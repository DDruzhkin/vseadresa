<?php
/*
*	yii runner/execute
*/

namespace console\controllers;

use Yii;
use yii\helpers\Console;
use yii\helpers\ArrayHelper;
use common\models\Entity;
use common\models\Address;


class AddressesController extends \yii\console\Controller {
	
	public function actionSubscribing() {	
		print "Start...\r\n";

		$now = Entity::dbNow('Y-m-d');
		$fin = date('Y-m-d', strtotime($now.' +1day'));
		
		echo  $now.' - '.$fin."\r\n";
		$list = Address::find() -> andWhere(['between', 'subscribe_to', $now, $fin]);
		$list = $list -> all();
		foreach($list as $address) {
			$address -> publicate(true);
			echo $address -> address." -> продлена подписка\r\n";
		};
		
		print "Finish\r\n";
	}
	
	public function actionPaymentChecking() {	
		print "Start...\r\n";		
		$list = Address::find() -> andWhere('TO_DAYS(NOW()) - TO_DAYS(subscribe_to) >=15');
		echo $list -> count(); print "\r\n";
		$list = $list -> all();
		foreach($list as $address) {
			echo $address -> address.'. Подписка до '.$address -> subscribe_to.'. ';
			$invoice = $address -> getSubscribeInvoice();
			if (!$invoice) {
				echo 'Счет не выставлен.';
			} else {
				if ($address -> checkSubscribeInvoice()) {
					
					echo 'Неполаченный счет: '.$invoice -> caption();
				} else {
					echo 'Счет оплачен.';
				}
			}
			print "\r\n";
		};
		print "Finish\r\n";
	}
	
}