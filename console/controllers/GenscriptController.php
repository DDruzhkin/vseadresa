<?php
/*
*	yii genscript/filldb
*/

namespace console\controllers;

use Yii;
use yii\helpers\Console;
use yii\helpers\ArrayHelper;

class GenscriptController extends \yii\console\Controller {


	public $dataPath = '/var/www/portal/mgribov/data/'; // Путь размещения данных 

public function actionFilldb() {
	print "Start...\r\n";
  
//	$this -> fillTable('Okrug', ['abr']);
//	$this -> fillTable('Metro');
//	$this -> fillTable('Nalog', ['number']);
//	$this -> fillTable('Region');
	$this -> fillTable('Status', ['alias', 'table']);
	return 0;
}  


public function addStatusLink($status_from_id, $status_to_id){

	$values = ['from_id' => $status_from_id, 'to_id' => $status_to_id];
	return \yii\db\ActiveRecord::getDb()->schema->insert('adr_status_links', $values);
}

// Заполнить таблицу для класса $className. Идентификация производится по массиву $idNames
public function fillTable($className, $idNames = ['name']) {
	print "Filling the table of class ".$className."...\r\n";
	$fn = $this -> dataPath.strtolower($className).'.dat'; // имя файла, из которого будут загружаться данные, для каждой модели свой файл
	$ns = '\common\models\\'; // namespace моделей
	if (($f = fopen($fn, "r")) !== FALSE) {
		print "File is openned...\r\n";		
//		var_dump($row = fgetcsv($f, 1000, ";"));

		$fullClassName = $ns.$className;
		$data = []; $ids =[]; $c = 0;
		if ($row = fgetcsv($f, 1000, ";")){
			$titles = $row; // считываем первую строку с заголовками
			echo "======================================================\n";
			while ($row = fgetcsv($f, 1000, ";")){
				
				echo implode(',',$row)."\n";
				echo "======================================================\n";
				$obj = new $fullClassName();
				// формируем массив данных в формате key => value, где key - заголовок столбца
				foreach($titles as $i => $title) {
					$title = trim($title);
					if ($i < count($row)) {
						$value = trim($row[$i]);
					}  else $value = '';
					$data[$title] = $value;
					if (in_array($title, $idNames)) $ids[] = $title;
				}				
				
			//	if (!$data['table']) continue;
				if ((strtolower($className) == 'nalog') || (strtolower($className) == 'region')) {
					// для налоговых и регионов формируем ссылку на округ
					$okrugTitle = $data['okrug'];
	                                $cl = $ns.'Okrug';
					$data['okrug_id'] = $cl::addBy(['name' => $okrugTitle]) -> id;					
				}


				
				$obj = $fullClassName::addBy($data, $ids); // Производится поиск объекта, если найден - возвращается, если нет, то создается и возвращается
				

//var_dump($data); echo "<br/>";
//var_dump($ids); echo "<br/>";
//var_dump($obj -> attributes); echo "<br/>";
//exit;



				if (strtolower($className) == 'status') { // Для статусов обновляем связки
					// удаляем старые
					$command = Yii::$app->db->createCommand();
					$command->delete('adr_status_links', 'from_id=:fromId', ['fromId' => $obj -> id]);
					
					// добавляем из файла
					$links = $data['linksTo'];
					if ($links > '') {
						$links = explode(',', $links);
						foreach($links as $status_to) {

							$status_to = trim($status_to);
							if ($status_to > '') {
								$status_to_id = $fullClassName::addBy(['alias' => $status_to,'table' =>$data['table']]) -> id;
								$this -> addStatusLink($obj -> id, $status_to_id);
							}
						}
					}


				}


				$c++;
		//		print $c."\r\n";		
			}
		}


	} else {
		print "File error!\r\n";		
	}

        print "the end of table filling.\r\n";
	return;

	$f = fopen($fn, 'r');



	$row = fgetcsv($f, 1000, ";");
	var_dump($row);

	while (($row = fgetcsv($f, 1000, ";")) != FALSE){
		var_dump($row);
		print "\r\n";
	}
}


}