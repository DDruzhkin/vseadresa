<?php
/*
*	yii common/daily
*/

namespace console\controllers;

use Yii;
use yii\helpers\Console;
use yii\helpers\ArrayHelper;
use common\models\Entity;
use frontend\models\Order;
use frontend\models\Address;
use frontend\models\PhotoCalendar;


class CommonController extends \yii\console\Controller {
	
/*
*	Запускает фоновые процесс, которые должны выполняться  ежедневно
*/	
	
	protected static function start() {
		echo  "\r\n***** START: ".date('d.m.Y H:i:s', strtotime(Entity::dbNow()))."\r\n";
	}
	
	protected static function div() {
		echo "\r\n***\r\n";
	}
		
	protected static function fin() {
		echo "***** THE END: ".date('d.m.Y H:i:s', strtotime(Entity::dbNow()))."\r\n\r\n";
	}
	
	// Должно запускаться ежедневно
	public function actionDaily() {	
	
		echo "Check subscribing: \r\n";
		Address::subscribing();
		echo "Check subscribing - finished.\r\n";
		
		echo "Check subscribing payments: \r\n";
//		Address::paymentChecking();
		echo "Check subscribing payments - finished.\r\n";
	
	
		self::start();
		$c = Order::clearDrafts();
		echo "Deleted: ".$c."\r\n";
		self::div();
		
		$c = Address::mixPriorities();		
		self::div();
		
		echo "Удаление старых неоплаченных визитов фотографа \r\n";;
		$c = PhotoCalendar::removeOldUnpaiedVisits();
		echo "Deleted: ".$c."\r\n";
		self::fin();	
	}
	
	
	// Должно запускаться ежеполучасно
	public function actionHalfHourly() {	
		self::start();
		echo "Проверка и отправка уведомлений для срочных заказов в формировании более 3-х часов:\r\n\t";
		$c = Order::notifyFormation3Hour();
		
		if ($c) {
			echo "сформировано уведомлений: ".$c;
		} else {
			echo "нет уведомлений";
		}
		echo "\r\n";
		self::div();
		echo "Проверка и отправка уведомлений для заказов в формировании более 24-х часов:\r\n\t";		
		$c = Order::notifyFormation24Hour();
		
		if ($c) {
			echo "сформировано уведомлений: ".$c;
		}	else {
			echo "нет уведомлений";
		}
		echo "\r\n";
		self::div();
		self::fin();	
	
	}
	
}