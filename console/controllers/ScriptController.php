<?php
/*
*	yii runner/execute
*/

namespace console\controllers;

use Yii;
use yii\helpers\Console;
use yii\helpers\ArrayHelper;
use common\models\Entity;
use common\models\Order;


class ScriptController extends \yii\console\Controller
{

    public function actionFixOrder()
    {
        print "Start...\r\n";
        $list = Order::find()
            ->andWhere(['not', ['date_start' => null]])
            ->andWhere(['date_fin' => null])
            ->all();
        foreach ($list as $order) {
            print "order " . $order->id . "\r\n";
            if (strpos($order->duration, '11') !== false) {
                $order->date_fin = date('Y-m-d', strtotime($order->date_start . ' +11 month'));

            }
            if (strpos($order->duration, '6') !== false) {
                $order->date_fin = date('Y-m-d', strtotime($order->date_start . ' +6 month'));
            }
            $order->date_fin = date('Y-m-d', strtotime($order->date_fin . ' -1 days'));
            if ($order->defSave(false))
                print " fixed\r\n";
        };

        print "Finish\r\n";
    }


}