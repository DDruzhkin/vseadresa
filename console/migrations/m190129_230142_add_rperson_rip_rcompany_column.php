<?php

use yii\db\Migration;

class m190129_230142_add_rperson_rip_rcompany_column extends Migration
{
    public function up()
    {
        $this->addColumn('adr_rip', 'sign', $this->string(255));
        $this->addColumn('adr_rip', 'stamp', $this->string(255));
        $this->addColumn('adr_rcompany','sign', $this->string(255));
        $this->addColumn('adr_rcompany', 'stamp', $this->string(255));
        $this->addColumn('adr_rperson','sign', $this->string(255));
        $this->addColumn('adr_rperson', 'stamp', $this->string(255));
    }

    public function down()
    {

    }
}
