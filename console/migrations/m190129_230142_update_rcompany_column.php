<?php

use yii\db\Migration;

class m190129_230142_update_rcompany_column extends Migration
{
    public function up()
    {
        $this->alterColumn('adr_rcompany','name','varchar(50)  NULL DEFAULT ""');
    }

    public function down()
    {

    }
}
