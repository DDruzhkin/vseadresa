<?php

use yii\db\Migration;

class m190118_230142_add_address_column extends Migration
{
    public function up()
    {
        $this->addColumn('adr_adresses', 'reg_msk', $this->tinyInteger());
        $this->addColumn('adr_adresses', 'reg_mo', $this->tinyInteger());
        $this->addColumn('adr_adresses', 'reg_other', $this->tinyInteger());
    }

    public function down()
    {

    }
}
