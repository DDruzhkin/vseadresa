<?php

use yii\db\Migration;

class m190129_230142_update_rperson_column extends Migration
{
    public function up()
    {
        $this->alterColumn('adr_rperson','fname','varchar(50)  NULL DEFAULT ""');
        $this->alterColumn('adr_rperson','lname','varchar(50)  NULL DEFAULT ""');
    }

    public function down()
    {

    }
}
