<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'container' => [
        'definitions' => [
            'yii\widgets\LinkPager' => [
                'nextPageLabel'=>'<img src="/img/triangle_right.png" alt="">',
                'prevPageLabel'=>'<img src="/img/triangle_left.png">',
                'lastPageLabel' => "<img src='/img/last_page.png'>",
                'firstPageLabel' => "<img src='/img/first_page.png'>",
                'lastPageCssClass' => "pagination-last",
                'firstPageCssClass' => "pagination-first",
                'prevPageCssClass' => "pagination-prev",
                'nextPageCssClass' => "pagination-next",
                'maxButtonCount' => 5,
                "pageCssClass"=>"usual",
                "activePageCssClass"=>"active-pag"
            ]



        ],
        'singletons' => [

        ]
    ],

	'container' => [
        'definitions' => [
            'yii\widgets\LinkPager' => [
				'nextPageLabel'=>'<img src="/img/triangle_right.png" alt="">',
                'prevPageLabel'=>'<img src="/img/triangle_left.png">',
                'lastPageLabel' => "<img src='/img/last_page.png'>",
                'firstPageLabel' => "<img src='/img/first_page.png'>",
                'lastPageCssClass' => "pagination-last",
                'firstPageCssClass' => "pagination-first",
                'prevPageCssClass' => "pagination-prev",
                'nextPageCssClass' => "pagination-next",
                'maxButtonCount' => 5,
                "pageCssClass"=>"usual",
                "activePageCssClass"=>"active-pag"
			]
			
			
			
        ],
        'singletons' => [
     
        ]
	],
	
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'basic/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,            
            'rules' => [
							
				'GET page/<slug:[\w_\-]+>' => 'page/index',
				
				
				'GET addresses/<action:(okrug|nalog|metro|option)>/<code:\w+>' => 'address/<action>',
				'GET addresses' => 'address/index',
				'GET address/<id:\d+>' => 'address/view',				
                'address/<id:\d+>' => 'address/detail',
                'address' => 'address/index',
                'addresses' => 'address/index',

                'GET address/<id:\d+>/buy' => 'customer/order/buy',
                'POST address/<id:\d+>/buy' => 'customer/order/stored',
                'GET order/<id:\d+>/delivery' => 'customer/order/delivery',
                'POST order/<id:\d+>/delivery' => 'customer/order/delivery-stored',

				'order/create' => 'customer/order/create',
				'order/update/<id:\d*>/<step:\d*>' => 'customer/order/update',
				'order/confirm/<id:\d*>' => 'customer/order/confirm',
				
				'customer/order/delete/<id:\d+>' => 'customer/order/delete',
				'GET customer/user/default-profile/<id:\d+>/<rid:\d+>' => 'customer/user/default-profile',
				'customer/order/clone/<id:\d+>' => 'customer/order/clone',
				'GET customer/<controller:[\w_\-]+>/view/<id:\d+>' => 'customer/<controller>/view',				
				'customer/<controller:[\w_\-]+>/update/<id:\d+>' => 'customer/<controller>/update',							
				'customer/<action:(signup)>' => 'customer/user/<action>',			
				'customer' => 'customer/user',
				'customer/settlements' => 'customer/document/settlements',												
				'customer/document/view/<id:\d+>' => 'customer/document/view',
				'customer/order/document/<id:\d+>' => 'customer/order/document',
				'document/show-pdf/<id:\d+>' => 'document/show-pdf',
                'customer/requisite/<id:\d+>' => 'customer/user/requisite',

				'owner' => 'owner/user',							
				'owner/address/publicate/<id:\d*>' => 'owner/address/publicate',
				'owner/address/unpublicate/<id:\d*>' => 'owner/address/unpublicate',
				'owner/address/create/<id:\d*>/<step:\d*>' => 'owner/address/create',			
				'owner/address/update/<id:\d*>/<tab:[[\w_\-]*>' => 'owner/address/update',			
				'owner/service-price/update/<id:\d*>/<step:\d*>' => 'owner/service-price/update',
				'owner/service-price/ofaddress/<id:\d*>' => 'owner/service-price/ofaddress',			
				'owner/statistics/<by:(day|month|customer)>' => 'owner/statistics',				
				'owner/settlements' => 'owner/document/settlements',				
				'owner/psettlements' => 'owner/document/psettlements',
				'owner/payconfirm' => 'owner/document/payconfirm',								
				'GET owner/user/default-profile/<id:\d+>/<rid:\d+>' => 'owner/user/default-profile',
				'GET owner/<controller:[\w_\-]+>/view/<id:\d+>' => 'owner/<controller>/view',
				'GET owner/<controller:[\w_\-]+>/update/<id:\d+>' => 'owner/<controller>/update',			
				'POST owner/<controller:[\w_\-]+>/update/<id:\d+>' => 'owner/<controller>/update',
				'owner/<action:(signup)>' => 'owner/user/<action>',			
				'owner/order/document/<id:\d+>' => 'owner/order/document',
//				'owner/auction/<tab:[\w_\-]+>' => 'owner/auction',
				'owner/auction/rate/<id:\d+>' => 'owner/auction/rate',
				'owner/auction/rate-pay/<id:\d+>' => 'owner/auction/rate-pay',
				
				'GET admin/payment/<dir:(out|inp|eq)>' => 'admin/payment/',
				
				
				'r/create/<type:(rip|rperson|rcompany)>' => 'r/create/',
				
				'owner/address/get-stat/<by:(day|month)>/<group:(customer|address)>' => 'owner/address/get-stat/',
							
				'logout' => 'user/logout',				
				'login' => 'user/login',	
				'request-password-reset' => 'user/request-password-reset',
				'reset-password' => 'user/reset-password',				
				'GET profile' => 'user/profile',						
				'GET admin/' => 'admin/site/',		
			
				'/' => 'address/home',
				
				
				
				
				
				/// FOR DEVELOPERS		
				'GET maket/<name:[\w_\-]+>' => 'site/maket', 				
				'site/prices' => 'site/prices',
				/// /FOR DEVELOPERS		
				
				
				/*
                [
                    'pattern' => 'order/<id:\d+>',
                    'route' => 'order/index',
                    'defaults' => ['id' => null],
                ]
				*/
                //'order/<id:\d+>/<step:\d+>' => 'order/index',
				
				'admin/options' => 'admin/user-portal-options/',
            ],
        ],
        
    ],
    'params' => $params,
];
