$(document).ready(function () {
    console.log("ready!!");
	
    $('.visit_photo_calendar .dropDown-date').change(function () {			
        
		$('.visit_photo_calendar .grid-view').addClass('hidden');
		$('.visit_photo_calendar #photo-grid-view-' + $(this).val()).removeClass('hidden');
		
		
/*		
			var tableClass = $('.visit_photo_calendar .table');		
            tableClass.removeClass('active disabled');
            tableClass.each(function () {
                if ($(this).attr('data-id') == dataId) {
                    $(this).addClass('active');
                } else {
                    $(this).addClass('disabled');
                }
            });
*/			
    });


    $(".widget-filter select.metro").on("change", function () {
        if ($(this).val() == '') {
            document.location.href = $(this).attr('data-def-url');
        } else {
            document.location.href = $(this).attr('data-url') + '/' + $(this).val();
        }
    });

    //***** стилизация чекбоксов, описанных внутри label
    //***** декорация чекбоксов, описанных внутри label
    $("label input[type=checkbox]").each(function (index, chb) {

        $(chb).hide();
        chbD = $(chb).after('<div class="form-checkbox' + ($(chb).prop('checked') ? ' active-checkbox' : '') + '"></div>');
    });

    $( "div.form-checkbox").parent().on("click", function (e) {
        e.preventDefault();
    })
    $("label div.form-checkbox").on("click", function (e) {
        e.preventDefault();
        e.cancelBubble = true;
        e.stopPropagation();
        console.log($(this))
        chb = $(this).parent().find("input[type=checkbox]");
        if(chb.prop('checked') == true){
            chb.prop('checked', false)
        }else{
            chb.prop('checked', true)
        }
        chbD = $(this);
        console.log($(chb).prop('checked'), "XXX");
        if ($(chb).prop('checked')) chbD.addClass("active-checkbox"); else chbD.removeClass("active-checkbox");

        //	$(this).parent().find("input[name='ServicePrice[to_address]']").val($(chb).prop('checked') == true?1:0);

    });
	
	$("label input[type=radio]").each(function (index, chb) {
        $(chb).hide();		
        chbD = $(chb).after('<div class="form-checkbox' + ($(chb).prop('checked') ? ' active-checkbox' : '') + '"></div>');		
    });
	
	
	$("label input[type=radio]").parent().on("click", function () {        
        chb = $(this).find("input[type=radio]");
        chbD = $(this).find("div.form-checkbox");
        if ($(chb).prop('checked')) chbD.addClass("active-checkbox"); else chbD.removeClass("active-checkbox");        
			
    });
	
	
	$("label input[type=radio]").on("change", function () {	
		name = $(this).prop('name');		
		$("input[name='" + name + "']").each(function (index, chb) {
			if (!$(chb).parent().hasClass('busy')) {
				if ($(chb).prop('checked')) {
					$(this).parent().find('div.form-checkbox').addClass("active-checkbox");
				} else {
					$(this).parent().find('div.form-checkbox').removeClass("active-checkbox");
				}
			}
		});
	});
	
	
	
	
	
	
    //***** /декорация чекбоксов, описанных внутри label
    //***** /стилизация  чекбоксов, описанных внутри label

    /* checkbox */
    /* chb = $("input[type=checkbox]");

     $("input[type=checkbox]").each(function(index, chb){
         $(chb).hide();
         $(chb).parent().append('<div class="form-checkbox' + (($(chb).attr("checked") != undefined) ?' active-checkbox':'') + '"></div>');
     });
 */

    /* $("input[type=checkbox]").parent().find("div.form-checkbox").on("click", function () {
         chb = $(this).parent().find("input");
         $(this).toggleClass("active-checkbox");
         if ($(this).hasClass("active-checkbox")) chb.attr("checked","checked"); else chb.removeAttr('checked');
         console.log(chb.attr('checked'));

     });*/
    /* end checkbox */


    if ($(".has-sort").length) {
        var url_string = window.location.href;
        var url = new URL(url_string);
        var sort = url.searchParams.get("sort");
        if (sort !== undefined && sort !== null) {
            console.log(sort, "sort")
            var aTag = sort.replace("-", "");
            console.log(aTag, "aTag")
            console.log(sort.indexOf("-"), "index");
            if (sort.indexOf("-") == -1) {
                var data = "data-sort=-'" + sort + "'";
                console.log(data, 'dataaa')
                $("a[data-sort=-" + sort + "]").addClass("active-sort-asc");
            } else {
                console.log('in')
                sort = sort.replace("-", "");
                $("a[data-sort=" + sort + "]").addClass("active-sort-desc");
            }
        }

    }


    $("tbody .go-to-address").on("click", function () {
        var id = $(this).parent().attr("data-key");
        var loc = window.location.origin + "/index.php/address/" + id;
        location.href = loc;
        console.log(window.location, "ID")
    });


    if ($(".widget-separator-with-background").length) {
        if ($(window).width() < 768) {
            var widget = $(".widget-separator-with-background").clone();
            widget.addClass("absolute");
            widget.find(".left-padding").removeClass(".left-padding");
            $(".address-search-popup .container-to-insert").append(widget);
        }
    }


    function tabManipulator() {
        if ($(window).width() > 767) {
            $('.tab').on("click", function () {
                if (!$(this).hasClass('active-tab')) {
                    $('.tab').removeClass('active-tab');
                    $(this).addClass('active-tab');
                    $(".search-way").removeClass('display');
                    $("." + $(this).data('block')).addClass("display");
                }

            });
        }
        else {
            console.log("aa");
            var addressSearch = $(".address-search").clone();
            $(".address-mobile-section").append(addressSearch);
            $(".search-section .address-search").remove();

            $(".district-mobile-container").append($(".search-way.district").clone().removeClass("row"));
            $(".tax-mobile-container").append($(".search-way.tax").clone().removeClass("row"));
            $(".subway-mobile-container").append($(".search-way.subway").clone().removeClass("row"));
            $(".other-mobile-container").append($(".search-way.other").clone().removeClass("row"));

            $(".district-mobile-container .search-way.district").hide();
            $(".tax-mobile-container .search-way.tax").hide();
            $(".subway-mobile-container .search-way.subway").hide();
            $(".other-mobile-container .search-way.other").hide();
            $(".address-search .tab-content").html(""); //удаляем десктопные формы поиска
            $(".tab").removeClass("active-tab");
            $(".district-mobile-container .search-way.district").show();
            $(".distr-tab").on("click", function () {
                $(".tab-block .search-way:not(.search-way.district)").hide();
                $(".district-mobile-container .search-way.district").slideToggle("slow");
            });

            $(".tax-tab").on("click", function () {
                $(".tab-block .search-way:not(.search-way.tax)").hide();
                $(".tax-mobile-container .search-way.tax").slideToggle("slow");
            });

            $(".subway-tab").on("click", function () {
                $(".tab-block .search-way:not(.search-way.subway)").hide();
                $(".subway-mobile-container .search-way.subway").slideToggle("slow");
            });

            $(".other-tab").on("click", function () {
                $(".tab-block .search-way:not(.search-way.other)").hide();
                $(".other-mobile-container .search-way.other").slideToggle("slow");
            });
        }
    }

    tabManipulator();

    $(".order-page-two-btn").on("click", function (e) {
        console.log("order-address");
        e.preventDefault();
        $(".popup").removeClass("show-popup");
        $(".order-page-two").css("opacity", 0);
        $(".order-page-two").addClass("show-popup");
        opacityChange(".order-page-two");
    });
    $(".order-page-three-btn").on("click", function (e) {
        console.log("order-address");
        e.preventDefault();
        $(".popup").removeClass("show-popup");
        $(".order-page-three").css("opacity", 0);
        $(".order-page-three").addClass("show-popup");
        opacityChange(".order-page-three");
    });
    $(".order-page-fourth-btn").on("click", function (e) {
        console.log("order-address");
        e.preventDefault();
        $(".popup").removeClass("show-popup");
        $(".order-page-fourth").css("opacity", 0);
        $(".order-page-fourth").addClass("show-popup");
        opacityChange(".order-page-fourth");
    });


    /*review slider*/
    var reviewSl = $('.slider').bxSlider({
        pager: false,
        /* nextSelector: '#pronext',
         prevSelector: '#proprev',*/
    });
    $(".slide-2-left").on("click", function () {
        reviewSl.goToPrevSlide();
    });
    $(".slide-2-right").on("click", function () {
        reviewSl.goToNextSlide();
    });


   /* if ($(".lense-search").length) {
        var src = $(".lense-search").eq(0).find("img").attr("src");
        console.log(src, "srcsrc");
        $("#sld .image-to-change").find("img").attr("src", src).removeClass("hidden");
    }
    $(".lense-search").on("click", function () {
        var src = $(this).find("img").attr("src");
        console.log(src, "srcsrc");
        $("#sld .image-to-change").find("img").attr("src", src);
    });*/

    $(".open-profile-menu").on("click", function () {
        $(".cab-menu").css("display", "block");
        $(".open-profile-menu").css("display", "none");
        $(".profile-menu-closer").css("display", "block");
    });
    $(".profile-menu-closer").on("click", function () {
        $(".cab-menu").css("display", "none");
        $(".open-profile-menu").css("display", "block");
        $(".profile-menu-closer").css("display", "none");
    });


    $('.fancybox').fancybox({
        padding: 5
    });
    $('.order-fancybox').fancybox({
        padding: 5,
        afterLoad: function (current, previous) {
            $(".order-step").removeClass("show-popup");
            $(".order-step").css("opacity", 0);
            $(".order-step").addClass("show-popup");
            opacityChange(".order-step");
        },
        afterClose: function () {
            $(".popup").removeClass("show-popup");
        }
    });
    $("#uislider").slider({
        slide: function (event, ui) {
            $(".slider-current-value .value").text(ui.value);
        },
        min: 6,
        max: 11,
        value: 6
    });


    function opacityChange(popup) {
        $(popup).animate({opacity: 1}, 'fast');
    }

    $(".close-popup").on("click", function () {
        console.log("3")
        $(".popup").removeClass("show-popup");
    });

    if ($('.sl').length) {
        var getWidth = $(".sld").outerWidth();

        if ($(window).width() > 767) {
            var slidesCount = 4;
        } else {
            var slidesCount = 2;
        }
        var slideWidth = (getWidth - 40) / slidesCount
        $('.sl').bxSlider({
            pager: true,
            controls: false,
            slideWidth: slideWidth,
            slideMargin: 11,
            maxSlides: slidesCount,
            minSlides: slidesCount,
            touchEnabled: true,
            mouseDrag: true
            /* nextSelector: '#pronext',
             prevSelector: '#proprev',*/
        });

    }

var s;

    if ($('.view-sl').length) {
        var getWidth = $(".sld").outerWidth();

        if ($(window).width() > 767) {
            var slidesCount = 1;
        } else {
            var slidesCount = 1;
        }
        var slideWidth = (getWidth - 40) / slidesCount
        s = $('.view-sl').bxSlider({
            pager: false,
            controls: false,
            slideWidth: slideWidth,
            mouseDrag: true,
            //slideMargin: 11,
            maxSlides: slidesCount,
            minSlides: slidesCount,
            touchEnabled: true
            /* nextSelector: '#pronext',
             prevSelector: '#proprev',*/
        });

    }
    $(document).on("click", ".lense-search", function () {
        console.log("aa");
        var id = $(this).attr("data-id");
        s.goToSlide(id);
      /*  var src = $(this).find("img").attr("src");
        console.log(src, "srcsrc");
        $("#sld .image-to-change").find("img").attr("src", src);*/
    });

    /*questions*/


    $("#accordion").accordion({
        collapsible: true,
        active: false,
        heightStyle: 'panel',
        activate: function (event, ui) {
            var a = ui.newHeader.find("a.question");
            console.log(a.length);
            $("#accordion").find(".close-question").css("display", "none");
            $("#accordion").find("a.question").removeClass("opened-question");
            if (a.length != 0) {

                a.addClass("opened-question");
                if ($(window).length > 767) {
                    a.parent().find(".close-question").css("display", "block");
                }

            }
        }
    });
    $("#accordion2").accordion({
        collapsible: true,
        active: false,
        heightStyle: 'panel',
        activate: function (event, ui) {
            var a = ui.newHeader.find("a.question");
            console.log(a.length);
            $("#accordion").find(".close-question").css("display", "none");
            $("#accordion").find("a.question").removeClass("opened-question");
            if (a.length != 0) {

                a.addClass("opened-question");
                if ($(window).length > 767) {
                    a.parent().find(".close-question").css("display", "block");
                }

            }
        }
    });

    /*  $(".checkbox-item, .form-checkbox").on("click", function () {


           if($(this).hasClass("active-checkbox")){
               $(this).removeClass("active-checkbox");
               checked = false;

           }else{
               $(this).addClass("active-checkbox");
               $(this).find(".hidden-cb").prop('checked', true);
               checked = true;
           }

           $(this).parent().find('input').prop('checked', checked);

       });*/
    /*$(".price-filter-row select").on("change", function () {
        $(this).parents("form").submit();
       console.log($(this).val(), "val");
    });*/
    $(".cancel-filter").on("click", function () {
        location.href = "http://" + location.host + location.pathname
    });
    $(".sort-button").on("click", function () {
        var sortBy = $(this).attr("data-sort");
        var uri = UpdateQueryString("sort_by", sortBy);
        location.href = uri;
        console.log(sortBy, "sortBy");
    });
    $(".offerta-box").on("click", function () {
        if ($(this).hasClass("active-checkbox")) {
            $(this).removeClass("active-checkbox");
            $(this).parent().find(".hidden-cb").attr("value", 0).attr("checked", false);
        } else {
            $(this).addClass("active-checkbox");
            $(this).parent().find(".hidden-cb").attr("value", 1).attr("checked", true);
        }


    });
    $(".nds-box").on("click", function () {
        if ($(this).hasClass("active-checkbox")) {
            $(this).removeClass("active-checkbox");
            $(this).parent().find(".hidden-cb").attr("value", "");
        } else {
            $(this).addClass("active-checkbox");
            $(this).parent().find(".hidden-cb").attr("value", 1);
        }


    });


    $(".form-radio").on("click", function () {
        //$(this).parents(".checkbox-container").find(".checkbox-item").removeClass("active-checkbox");
        //$(this).parents(".checkbox-container").find(".checkbox-item .hidden-cb").prop('checked', false);
        $(".form-radio").removeClass("active-checkbox");
        $(this).addClass("active-checkbox");
    });


    /*$(".district-block .radio-value").on('click', function(event) {
        event.preventDefault();
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).parents(".district-block").find(".radio-btn").prop("checked", false);
        }else{
            $(this).parents(".district-block").find(".radio-value").removeClass('active');
            $(this).addClass('active');
            /!* Radio *!/
            $(this).parents(".district-block").find(".radio-btn").prop("checked", false);
            $(this).parent().find(".radio-btn").prop("checked", true);    
        }
        var ch = $(this).parents(".district-block").find("input:checked").val();
        console.log(ch, "Ch");
        submitFilter();

    });

    $(".tax-block .radio-value").on('click', function(event) {
        event.preventDefault();
        /!* UI *!/
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).parents(".tax-block").find(".radio-btn").prop("checked", false);
        }else{
            $(this).parents(".tax-block").find(".radio-value").removeClass('active');
            $(this).addClass('active');
            /!* Radio *!/
            $(this).parents(".tax-block").find(".radio-btn").prop("checked", false);
             $(this).parent().find(".radio-btn").prop("checked", true);
            
        }

        submitFilter();    
        
    });*/

//    $(".date").inputmask("99/99/9999", {"placeholder": "."});
    $(".time").inputmask("99/99/99", {"placeholder": "."});


    $(".filter-callout").on("click", function () {
        var className = $(".filter-pp");
        className.addClass("show-filter");
        className.animate({

            backgroundColor: 'rgba(0,0,0,0.6)'
        }, 500, function () {
            // Animation complete.
        });
       // var width = className.css({"display": "block", "width": 'auto'});
        var height = className.find('.cabinet-form').outerHeight();
        var width = className.find('.cabinet-form').outerWidth();
        className.find('.cabinet-form').css({height: 40 + "px", width: 40 + "px", left: 100 + "%", top: 0});
        console.log(height, "HEIGHT");

        className.find('.cabinet-form').animate({
            top: 50 + "%",
            left: 50 + "%",
            height: height,
            width: width
        }, 500, function () {
            // Animation complete.
        });
    });


    $(".filter-callout-reviews").on("click", function (e) {
        e.preventDefault();
        $('#load-modal').modal('show');
        $('#load-modal input[required=true]').prev('label').addClass('redstar');
        $('#load-modal textarea').prev('label').addClass('redstar');
    });

    $(".filter-pp, .close-filter-button img").on("click", function (e) {

        console.log($(this), "aa");
        $(".filter-pp .cabinet-form").animate({

            top: 0 + "%",
            left: 100 + "%",
            height: 40 + "px",
            width: 40 + "px"
        }, 500, function () {
            $(".filter-pp").removeClass("show-filter");
            $(".filter-pp .cabinet-form").removeAttr("style")
        });

    });

    $(".filter-pp-area .cabinet-form").on("click", function (e) {
        e.stopPropagation();
    });

    //gradient
    setGradient();

    $(window).on("resize", function () {
        console.log("test")
        setGradient();
    });

    /* $(".widget-filter select.metro").on("change", function(){
         submitFilter();
     });
 */
    /*if($("#order-payment_type").length){
        console.log($("#order-payment_type input[value='3']"));
        if($("#order-payment_type input[value='3']").is(":checked")){
            $(".by-count").addClass("active");
        }else if($("#order-payment_type input[value='2']").prop("checked")){

        }
    }*/

    $(".order-form.page-4 .form-radio").on("click", function () {
        $(".form-radio").removeClass("active");
        $(this).addClass("active");
        if ($(this).hasClass("by-card")) {
            $("#order-payment_type input[value='2']").prop("checked", true);
            console.log("in1");
        }
        if ($(this).hasClass("by-count")) {
            console.log("in2");
            $("#order-payment_type input[value='3']").prop("checked", true);
        }
    });


    $(".tax-block input.nalog-radio:checked").parents(".radio").find(".radio-value").addClass('active');

    $(".district-block input.district-radio:checked").parents(".radio").find(".radio-value").addClass('active');

    $(".burger-menu").on("click", function () {
        var mainMenu = $(".main-menu").clone();

        $(".mobile-navigation-menu").css("display", "block");
        /* open menu */

        $(".mobile-navigation-menu").append(mainMenu);
        /* insert nav list */

    });
    $(".close-menu").on("click", function () {

        $(".mobile-navigation-menu").css("display", "none");
        /* open menu */

        $(".mobile-navigation-menu .main-menu").remove();
        /* remove nav list */

    });

    $(".phone-format").inputmask();
    $(".bank-cor").inputmask();

    $(".mobile-address-search span").on("click", function () {
        console.log("aaa");
        $(".address-search-popup").css("display", "block");
        $(".mobile-address-search").css("display", "none");

    });
    $(".search-closer").on("click", function () {
        console.log("aaa");
        $(".address-search-popup").css("display", "none");
        $(".mobile-address-search").css("display", "flex");

    });


    /*    if($("div.addresses-list").length){
            if($(".address").length){
                var maxHeight = Math.max.apply(null, $(".address").map(function ()
                {
                    return $(this).height();
                }).get());

                console.log(maxHeight, "maxHeight");
                $.each($(".address"), function (i, x) {
                   $(this).height(maxHeight);
                });
            }
            if($(".address-icons").length){
                var maxHeight = Math.max.apply(null, $(".address-icons").map(function ()
                {
                    return $(this).height();
                }).get());

                console.log(maxHeight, "maxHeight");
                $.each($(".address-icons"), function (i, x) {
                   $(this).height(maxHeight);
                });
            }

        }*/

    $(".change-page").on("click", function () {
        var page = $(this).data("page");
        console.log(page, "page");
        var uri = UpdateQueryString("page", page);
        location.href = uri;
    });

    $(".by-time").on("click", function () {
        $(".by-time").removeClass("active");
        $(this).addClass("active");
    });


    $('#form-signup').on('afterValidate', function (e, messages) {
        //console.log(messages, 'xxxxxxxxxxxxxxxxxxxx');
        /* $.each(messages, function (i, v) {

             console.log($(this)[0], "AAA");
             console.log(i, "AAA");
             if($(this)[0] != undefined){
                 $("#"+i).parent().find("p.help-block-error").text($(this)[0]);
                 //$("#"+i).attr("placeholder", $(this)[0]).addClass("error");

             }
         });*/

    });
    $('#order, #person, #company, #ip').on('afterValidate', function (e, messages) {
        /*console.log(messages, 'xxxxxxxxxxxxxxxxxxxx');
        $.each(messages, function (i, v) {
            console.log($(this)[0], "AAA");
            console.log(i, "AAA");
            if($(this)[0] != undefined){

                $("#"+i).attr("placeholder", $(this)[0]).addClass("error");

            }
        });*/
    });
    $(".select-client-form-div select").on("change", function () {
        if ($(this).val() == 2) {
            $(".organisation-name-field").removeClass("hidden");
        } else {
            $(".organisation-name-field").addClass("hidden");
        }
    });

    /*ruslan*/
    if ($("#choose-your-status").length) {
        var val = $("#choose-your-status").val();
        console.log(val, "SEL VAL");
        changeOwnerForm(val);
    }
    $("#choose-your-status").on("change", function () {
        var val = $(this).val();
        changeOwnerForm(val);
    });
    /*alexey*/
    $('[name=defaultEnabled]').on('change', function () {
        if ($(this).is(':checked')) $('#notification-statuses').show();
        else $('#notification-statuses').hide();
    }).change();
    /*end*/

    $(".articles-reload").on("click", function () {
        location.reload();
    });


});

function changeOwnerForm(val) {
    if (val == 1) {
        $("form").removeClass("show");
        $("form#person").addClass("show");
    }
    else if (val == 2) {
        $("form").removeClass("show");
        $("form#company").addClass("show");
    }
    else if (val == 3) {
        $("form").removeClass("show");
        $("form#ip").addClass("show");
    }
}

function submitFilter() {
    $(".widget-separator-with-background form").submit();
}

function setSliderImage() {

}

function setGradient() {
    console.log($('.logo').offset().left, 'offst');
    /*if($(window).width() > 1400){
        logoLeft = -500;
    }*/
    //radial-gradient(circle 250px at 246px 98px, #fff, #E5E6E8)
    var logoLeft = $('.logo').offset().left;
    //var l = logoLeft +  $('.logo').width() / 2 - 25;
    var l = logoLeft + 120;
    console.log(l, "LLL");
    $(".white-bg").css('background', 'radial-gradient(circle 250px at ' + l + 'px 98px, #fff, #E5E6E8)');
}

function UpdateQueryString(key, value, url) {
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
        hash;

    if (re.test(url)) {
        if (typeof value !== 'undefined' && value !== null)
            return url.replace(re, '$1' + key + "=" + value + '$2$3');
        else {
            hash = url.split('#');
            url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
    }
    else {
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?';
            hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
        else
            return url;
    }


}