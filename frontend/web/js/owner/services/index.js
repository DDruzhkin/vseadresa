$(function () {
    // удаляем услугу
    $('.del-service').on('click', function () {
        if (confirm('Уалить услугу?')) {
            location.href = '?act=delete&id=' + $(this).data('id');
        }
        return false;
    });

    // редактируем услугу
    $('.edit-service').fancybox({
        type:'iframe'
    });

    // добавляем услугу
    $('#add-service').fancybox({
        type:'iframe'
    });

});