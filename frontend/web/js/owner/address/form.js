ymaps.ready(function () {
    var myMap = new ymaps.Map("map", {
            center: addressLocation,
            zoom: 14
        }, {
            searchControlProvider: 'yandex#search'
        }),

        // метка
        mark = new ymaps.Placemark(addressLocation, {
            balloonContent: 'место расположения объекта'
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6',
            draggable: true
        });

    // ставим метку
    myMap.geoObjects.add(mark);

    // при клике по карте перемещаем метку
    myMap.events.add('click', function (e) {
        var coords = e.get('coords');
        mark.geometry.setCoordinates(coords);
        document.getElementById('address-gps').value = coords.join(', ');
    });

    // перетаскиваем метку
    mark.events.add('dragend', function (e) {
        var coords = e.get('target').geometry.getCoordinates();
        document.getElementById('address-gps').value = coords.join(', ');
    });
});


$(function () {
    $('#address-metro').on('keyup', function () {
        var t = $(this),
            win = $('#metro-win'),
            ul = win.find('ul');
        $.post('/index.php/owner/address/create', {value: t.val()}, function (data) {

            if (win.length === 0) {
                t.after('<div id="metro-win" style="position: absolute; border: 1px solid #f00; width: 288px;' +
                    'min-height: 25px; background-color: #fff;z-index: 1000;"><ul></ul></div>');
            }
            ul.html('');

            $.each(data, function (i, d) {
                ul.append('<li>' + d + '</li>');
            });

            ul.find('li').on('click', function () {
                t.val($(this).html());

                win.remove();
            });

        }, 'json');

    });
});
