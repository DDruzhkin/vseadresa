$(function () {
    $('[name=demo]').on('change', function () {
        var t = $(this),
            isChecked = t.is(':checked') ? 1 : 0;
        $.post('/index.php/owner/address', {id: t.val(), value: isChecked});
    });
});