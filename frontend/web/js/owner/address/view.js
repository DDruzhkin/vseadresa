$(function () {
    var div = $('#map');
    if (div.length !== 0) {
        var addressLocation = [div.data('lat'),div.data('lng')];
        ymaps.ready(function () {
            var myMap = new ymaps.Map("map", {
                    center: addressLocation,
                    zoom: 14
                }, {
                    searchControlProvider: 'yandex#search'
                }),

                // метка
                mark = new ymaps.Placemark(addressLocation, {
                    balloonContent: 'место расположения объекта'
                }, {
                    preset: 'islands#icon',
                    iconColor: '#0095b6',
                    draggable: false
                });

            // ставим метку
            myMap.geoObjects.add(mark);
        });
    }


    // слайдер
    var items = $(".lense-search");
    if(items.length) {
        var src = items.eq(0).find("img").attr("src"),
            imgToChange = $('.image-to-change img');
        imgToChange.attr("src", src).removeClass("hidden");

        items.on("click", function () {
            var src = $(this).find("img").attr("src");
            imgToChange.attr("src", src);
        });
    }

});


