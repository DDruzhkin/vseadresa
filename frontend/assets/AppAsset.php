<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/bx/jquery.bxslider.min.css',
        'plugins/fa/css/font-awesome.min.css',
        /*'plugins/ui/jquery-ui.min.css',*/
        'plugins/fancy/source/jquery.fancybox.css',
        'css/site.css',

    ];
    public $js = [

        //'js/map.js',
        'plugins/bx/jquery.bxslider.js',
        'plugins/ui/jquery-ui.min.js',
        'plugins/fancy/source/jquery.fancybox.pack.js',
        'plugins/im/jquery.inputmask.bundle.js',
        'js/site.js',
        'js/bxslider-drag.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

	public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
	

}
