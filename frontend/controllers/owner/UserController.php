<?php

namespace frontend\controllers\owner;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\EntityHelper;
use common\models\RequisiteManager;
use common\models\Region;
use common\models\UserSearch;
use common\controllers\EntityController;
use frontend\models\EmailTemplate;
use frontend\models\User;
use frontend\models\ChangePassword;
use frontend\models\DeliveryAddress;
use frontend\models\Rcompany;
use frontend\models\Rperson;
use frontend\models\Rip;
use frontend\models\OwnerSignup;



/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex()

    {		
		$id = User::currentUser();
        $model = User::findOne($id);
		
//		
		if (isset(Yii::$app->request->get()['delivery_address'])) {           
		  $address = Yii::$app->request->get()['DeliveryAddress'];		  
		  //$model -> setDeliveryAddress($address);		  
		  $model -> delivery_address_as_array = $address;		  
		  $model -> save();		  
		}
	
        $passwordModel = new ChangePassword();

        if ($model->load(Yii::$app->request->post())) {           		
			if ($model->save()) {                       
				//return $this->redirect(['view', 'id' => $model->id]);
			}
		};
    
		return $this->render('update', [
			'model' => $model,
			'passwordModel' => $passwordModel,
		]);
    }


    public function actionSignup()
    {

        $model = new OwnerSignup();
        $model->role = User::ROLE_OWNER;

        $app = Yii::$app;
        $request = $app->request;
        $session = $app->session;

        if ($request->isAjax && $model->load($request->post())) {
            $model->email = $model->username;
            $app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }


        if ($request->post()) {
            $model->email = $model->username;
        }

        if ($model->load($request->post())) {

            if ($user = $model->signup()) {

                if ($app->getUser()->login($user)) {

                    $session->set('newRegisterOwner','true');

                    $subject = Yii::t('app', 'registeredSuccessfully');

                    $session->setFlash('success', Yii::t('app', 'registeredSuccessfully'));

                    return $this->redirect(["owner/user/signup-complete", 'step' => 2]);

                    //return $this->goHome();
                }

                //return $this->redirect(['site/register_success']);
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

	
	
public function actionSignupComplete(){

        $step = (int)@$_GET['step'];
        $user_id = Yii::$app->user->identity->id;
        $user = User::findOne($user_id);
        $deliveryAddress = new DeliveryAddress();

        //$region = new Region();
        //var_dump(Yii::$app->user); die();
        if($step == 2){ // создаём модель с реквизитами

                /*если выбрал физ. лицо*/
            $person = new Rperson();
            if (Yii::$app->request->isAjax && $person->load(Yii::$app->request->post())) { // Валидация модели "Физ. лицо ajax
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($person);
            }
			if ($person->load(Yii::$app->request->post()) && $deliveryAddress->load(Yii::$app->request->post())) {

                if($deliveryAddress->validate()){
                    ///die("valid");
                    $person->user_id = $user_id;
                    //$person->save(true);
					RequisiteManager::save($person);
                    //echo "<pre>", print_r($user); die();
                    $user->default_profile_id = $person->id;
                    //$user->setDeliveryAddress($deliveryAddress);
					$user->delivery_address_as_array = $deliveryAddress;
                    $user->info = $deliveryAddress->info;
                    $user->save();
                    return $this->redirect(['/owner']);
                }else{
                    $errors = $deliveryAddress->errors;
                    echo print_r($errors); die();
                }


            }
            /* end физ.лицо */

            /*если выбрал Юр. лицо*/
            $company = new Rcompany();
            if (Yii::$app->request->isAjax && $company->load(Yii::$app->request->post())) { // Валидация модели "Юр. лицо ajax
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($company);
            }
            if ($company->load(Yii::$app->request->post()) && $deliveryAddress->load(Yii::$app->request->post())) {

                if($deliveryAddress->validate()) {
                    $company->user_id = $user_id;
                    //$company->save(true);
					RequisiteManager::save($company);
                    //echo "<pre>", print_r($user); die();
                    $user->default_profile_id = $company->id;
                    //$user->setDeliveryAddress($deliveryAddress);
					$user->delivery_address_as_array = $deliveryAddress;
                    $user->info = $deliveryAddress->info;
                    $user->save();
                    return $this->redirect(['/owner']);
                }
            }
            /* end Юр.лицо */

            /*если выбрал Ип.*/
            $rip = new Rip();
            if (Yii::$app->request->isAjax && $rip->load(Yii::$app->request->post())) { // Валидация модели "Юр. лицо ajax
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($rip);
            }
            if ($rip->load(Yii::$app->request->post()) && $deliveryAddress->load(Yii::$app->request->post())) {
                $rip->user_id = $user_id;
                //$rip->save(true);
				RequisiteManager::save($rip);
                //echo "<pre>", print_r($user); die();
                $user->default_profile_id = $rip->id;
                //$user->setDeliveryAddress($deliveryAddress);
				$user->delivery_address_as_array = $deliveryAddress;
                $user->info = $deliveryAddress->info;
                $user->save();
                return $this->redirect(['/owner']);
            }
            /*end Ип.*/

            return $this->render('formdata', [
                'person' => $person,
                'company' => $company,
                'rip' => $rip,
                'deliveryAddress' => $deliveryAddress,
                'region' => new Region(),
            ]);
        }
    }	
	
	
}
