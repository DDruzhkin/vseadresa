<?php

namespace frontend\controllers\owner;

use Yii;
use backend\models\Rip;

/**
 * RipController implements the CRUD actions for Rip model.
 */
class RipController extends RController
{
      public function actionCreate()
    {
        $model = new Rip();
		$model -> integrate();
		return parent::create($model);
	}

}
