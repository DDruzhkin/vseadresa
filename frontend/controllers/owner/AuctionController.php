<?php

namespace frontend\controllers\owner;



use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use common\models\EntityHelper;
use common\models\PayManager;

use frontend\models\AuctionRateSearch;
use frontend\models\Auction;
use frontend\models\AuctionRate;
use frontend\models\AuctionSearch;
use frontend\models\Page;
use frontend\models\Payment;
use frontend\models\User;



class AuctionController extends \frontend\controllers\owner\BasicController
{
	public function actionRules()
	{
		$page = Page::bySlug('auction');		
        return $this->render('index', [
            'tab' => 'rules',					
        ]);
		
	}
	
	
/*
** На странице отображается список открытых аукционов, 
** а так же закрытые аукционы на даты до вчерашнего дня (последним отображается аукцион на текущую дату, который проводился вчера). 
** Для каждого аукциона отображается дата на которую проводиться аукцион, которая является ссылкой на страницу карточки аукциона.	
*/
	
    public function actionIndex()
    {	
//		$params = Yii::$app->request->queryParams;
			
		$list = Auction::integrateAuctionList();

        return $this->render('index', [
			'tab' => 'index',			
        ]);
		
    }

    public function actionArch()
    {			
		$list = Auction::integrateAuctionList();

        return $this->render('index', [
			'tab' => 'arch',			
        ]);
		
    }

	
	public function actionView($id)
    {				
		$model = $this -> findModel($id);
        return $this->render('view', [
            'model' => $model,

        ]);
		
    }

			
	public function actionRate($id)
    {						
		
		$auction = Auction::findOne([$id]);
		if ($auction -> status -> alias == 'noactive') {
			throw new NotFoundHttpException('Аукцион закрыт, невозможно сделать ставки');
		}

        return $this->render('auction_rate', [
            'auction' => $auction,
        ]);
		
    }
	
	//Страница Список ставок
	public function actionRateList()
    {						
		$searchModel = new AuctionRateSearch();		
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
			'tab' => 'rate-list',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
		
		
    }
	
	
	public function actionRatePay($id)
    {						

		$auction = Auction::findOne([$id]);
		$params = Yii::$app->request->queryParams;
		//$id = $params['id'];
		$summ = 0;
		if (isset($params['addrates'])) {
			$addrates = $params['addrates'];			
			foreach($addrates as $rateid => $amount) {
				$summ = $summ + $amount;
			}						
		}
		$amount = $summ;	
		if ($amount <= 0) {
			Yii::$app->session->setFlash('error', "Ставки не указаны! Укажите размеры ставок в колонке 'Ваша ставка'");			
			return $this -> redirect(Url::to('/owner/auction/rate/'.$id));
		} else {
			// Создаем оплату
			$payment = new Payment();
			$payment -> ptype = Payment::PTYPE_CARD;
			$payment -> value = $amount;
			$payment -> data_as_array = $params;		
			$payment -> save();
			$number = $payment -> id;
			
			$result = PayManager::payRegOrder([
				'number' => $number, 
				'amount' => $amount, 
				'returnUrl' => Url::base(true).Url::to(['/owner/auction/rate-pay-back/']),
			]);
			//echo "$number<br>";
			if (isset($result['errorCode']) && $result['errorMessage']) {
				throw new \Exception('Ошибка: ['.$result['errorCode'].'] '.$result['errorMessage']. ' №'.$number);
			} else {			
				$payId = $result['orderId'];
				$payment -> pay_id = $payId;
				$payment -> save();
					
				return $this -> redirect($result['formUrl']);
			}
		}
		
    }
	
	
	public function actionRatePayBack($orderId) {				
		$payId = $orderId;		
		$payment = Payment::findOne(['pay_id' => $payId]);		
		if (is_null($payment)) {
			throw new \Exception('Попытка проведения некорректного платежа');
		}		
		
		/* в рабочем режиме раскоментировать
		if ($payment -> confirmed) {
			throw new NotFoundHttpException('Попытка дублирования оплаты по карте');
		}
		*/
		// Подтверждаем платеж
		$payment -> confirmed = 1;
		$payment -> save();	
				
		// Формируем ставки
		$data = $payment -> data_as_array;
		
		$auctionId = $data['id'];
		$addrates = $data['addrates'];
		foreach($addrates as $addressId => $amount) {
			if (!$amount) continue;
			$rate = new AuctionRate();
			$rate -> auction_id = $auctionId;
			$rate -> address_id = $addressId;
			$rate -> owner_id  = User::currentUser();
			$rate -> amount = $amount;
			$rate -> save();
		}
		
		return $this -> redirect(Url::to(['/owner/auction/rate/'.$auctionId]));
		
	}
	
}