<?php

namespace frontend\controllers\owner;

use Yii;
use backend\models\ServicePrice;
use common\models\ServicePriceSearch;
use common\models\Entity;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ServicePriceController implements the CRUD actions for ServicePrice model.
 */
class ServicePriceController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ServicePrice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServicePriceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ServicePrice model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ServicePrice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ServicePrice();
		$model -> integrate();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		$model->load(Yii::$app->request->post()); $model->save();            
			return $this->redirect(['update', 'id' => $model->id, 'step' => 2]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'step' => 1
            ]);
        }
    }

    /**
     * Updates an existing ServicePrice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
		$get = Yii::$app->request->get();
		$step = (isset($get['step']) && ($get['step'] > '')&& ($get['step'] > 1))?$get['step']:1;
		$id = (isset($get['id']) && ($get['id'] > ''))?$get['id']:false;
		
		if ($id) $model = $this->findModel($id);
		$post = Yii::$app->request->post(ServicePrice::baseClassName());
		if ($post) {
			if ($id) {				
				$model->load(Yii::$app->request->post());
			} else {
				$model = Entity::getCurrentUser() -> addOwnerSerivcePrice($post);
			};	
						
			
			if ($model->save()) {                       
				if ($step > 2)  // Конец мастера - переходим на список услуг
					return $this->redirect(['owner/service-price']);
			}	
		};
		
        if (!$model) throw new Exception('Некорректный ID или модель не найдена');   
           
		return $this->render('update', [
			'model' => $model,
			'step' => $step             
		]);
    }

    /**
     * Deletes an existing ServicePrice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		
        $model = $this->findModel($id);		
		$model->delete();

        return $this->redirect(['index']);
    }

	
	// Справочник услуг адреса
	public function actionOfaddress($id) {
		$model = $this->findModel($id);
		
		return $this->render('ofaddress', [
			'model' => $model,			 
		]);
	}
	
	
}
