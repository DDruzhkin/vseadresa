<?php

namespace frontend\controllers\owner;

use Yii;
use backend\models\Rcompany;


/**
 * RcompanyController implements the CRUD actions for Rcompany model.
 */
class RcompanyController extends RController
{
    public function actionCreate()
    {
        $model = new Rcompany();
		$model -> integrate();
		return parent::create($model);
	}

}
