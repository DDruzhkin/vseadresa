<?php

namespace frontend\controllers\owner;


use common\controllers\EntityController;
use frontend\models\User;
use yii\filters\AccessControl;
use Yii;
use yii\filters\VerbFilter;
use frontend\components\AccessRule;
use yii\web\ForbiddenHttpException;


class BasicController extends \frontend\controllers\BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'ruleConfig' => [
                    'class' => AccessRule::class,
                ],

				
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@',User::ROLE_OWNER],
                    ],
                ],
				
				
                'denyCallback' => function() {
                    Yii::$app->session->setFlash('error', 'У вас нет доступа к этой странице');
                    $this->goHome();
                }
            ],

            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	
	
	public function beforeAction($action)
	{		
		$result = parent::beforeAction($action);
		$user = User::getCurrentUser();
		if ($result) {			

			if ((Yii::$app->user->isGuest || !$user -> checkRole(User::ROLE_OWNER)) && (!in_array($action -> id, ['signup', 'signup-complete']))) 
				throw new ForbiddenHttpException (self::EM_OWNER_ROLE_REQUIRED);

			$this -> layout = (in_array($action -> id, ['signup', 'signup-complete'])? 'main.php' : 'owner.php');		
		};		
		

		return $result;
	}
	
	

}