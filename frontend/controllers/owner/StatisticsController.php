<?php
namespace frontend\controllers\owner;


use Yii;
use \frontend\models\StatisticsSearch;
use \frontend\models\Stat;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

class StatisticsController extends BasicController {

    public function actionIndex() {
        $this -> layout = 'owner.php';
    
		if (Yii::$app->request->get('by'))  $by = Yii::$app->request->get('by'); else $by = 'month';
        return $this->render('index', [
            'by' => $by,            
        ]);
    }
	
    

}