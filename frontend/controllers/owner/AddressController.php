<?php

namespace frontend\controllers\owner;


use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\Entity;
use common\models\EntityHelper;
use frontend\models\Address;
use frontend\models\AddressSearch;
use frontend\models\PhotoCalendar;
use frontend\models\DocumentType;
use common\models\Nalog;


/**
 * AddressController implements the CRUD actions for Address model.
 */
class AddressController extends BasicController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Address models.
     * @return mixed
     */
    public function actionIndex()

    {
        $searchModel = new AddressSearch();
        $searchModel->demo = false;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }


    public function actionGetStat($by, $group)
    {
        $a = ['customer' => 'customer_r_id', 'address' => 'address_id'];
        $group = $a[$group];
        EntityHelper::createPivot($by, $group, Entity::currentUser());
    }

    /*
    public function actionStat()
    {

        return $this->render('index', [

        ]);

    }
    */


    /**
     * Снять адрес с публикации
     */
    public function actionUnpublicate($id)
    {
        $model = $this->findModel($id);
        $result = $model->unPublicate();

        return $this->redirect(['/owner/address']);
        //return $this->redirect(['/address/'.$model->id]);
    }

    /**
     * Опубликовать адрес
     */
    public function actionPublicate($id)
    {
        $model = $this->findModel($id);
        $result = $model->publicate();
        return $this->redirect(['/owner/address']);
        /*
                return $this->render('public', [
                    'model' => $model,
                    'result' => $model -> publicate(),
                ]);
        */

    }

    /**
     * Displays a single Address model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Мастер добавления адреса. Может быть передано два параметра id - id нового адреса и step - номер шага
     * Если  id не передан, то модель надо создать, иначе загрузить
     * Если step не передан, то step=1
     */
    public function actionCreate()
    {

        $app = Yii::$app;
        $request = $app->request;
        $session = $app->session;

        $get = $request->get();
        $step = (isset($get['step']) && ($get['step'] > '') && ($get['step'] > 1)) ? $get['step'] : 1;
        $id = (isset($get['id']) && ($get['id'] > '')) ? $get['id'] : false;
        $post = $request->post(Address::baseClassName());
        $model = $id ? Address::findOne([$id]) : new Address();

        $model->integrate();

        $calendarData = PhotoCalendar::getPhotoCalendarDataSinceToday();

        if ($model->load($request->post())) {

            if (isset($post['quickable']) && $post['quickable']) {
                $model->addQuickServicePrice(['description' => $post['service_description'], 'price' => $post['service_price']]);
            }
            // $model->deleteQuickServicePrice(); ///???


            if ($model->save()) {
                if (isset(Yii::$app->request->post($model->formName())['tax_agencies'])) {
                    foreach (Yii::$app->request->post($model->formName())['tax_agencies'] as $tax_agency) {
                        $model->link('taxAgencies', Nalog::findOne($tax_agency));
                    }
                }
            };


        };

        return $this->render('create', [
            'model' => $model,
            'step' => $step,
//            'calendarData' => $calendarData,
        ]);

    }

    /**
     * Updates an existing Address model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param int $id
     * @param string $tab
     *
     * @return mixed
     *
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id, $tab)
    {
        $app = Yii::$app;
        $request = $app->request;
        $session = $app->session;

        $get = $request->get();
        $model = $this->findModel($id);
        $tab = (isset($get['tab']) && ($get['tab'] > '')) ? $get['tab'] : false;

        if ($request->get()) {

            if ($tab == 'services') {

                $data = $request->get();
                if (!isset($data['serviceList'])) $data['serviceList'] = [];

                if (isset($data['serviceList']) && isset($data['price']) && isset($data['price6']) && isset($data['price11'])) {
                    $model->setServiceList($data['serviceList'], $data['price'], $data['price6'], $data['price11']);
                }
            };


        }

        if ($model->load(Yii::$app->request->post())) {
            $model->unlinkAll('taxAgencies',true);
            if (isset(Yii::$app->request->post($model->formName())['tax_agencies'])) {
                foreach (Yii::$app->request->post($model->formName())['tax_agencies'] as $tax_agency) {
                    $model->link('taxAgencies', Nalog::findOne($tax_agency));
                }
            }
            if ($model->save()) {
                $session->setFlash('success', 'Данные сохранены');
            }else{
                die(var_dump($model->errors));
            }
        };


        return $this->render('update', [
            'model' => $model,
            'tab' => $tab,
        ]);
    }


    public function actionDocument($id)
    {
        $params = Yii::$app->request->get();

        $all = false;
        if ($params) {
            if (isset($params['all'])) $all = true;
        }


        $model = $this->findModel($id);
        if (!$model) throw new NotFoundHttpException('Некорректный id адреса: ' . $id);

        $documents = $model->getInvoices($all);


        return $this->render('document', [
            'model' => $model,
            'documents' => $documents,
        ]);
    }


    /**
     * Deletes an existing Address model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Address model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Address the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        //if (($model = Address::find()->where(['id'=>$id])->with('taxAgencies')->one()) !== null) {
        if (($model = Address::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }

}
