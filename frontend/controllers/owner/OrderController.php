<?php

namespace frontend\controllers\owner;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\controllers\EntityController;
use frontend\models\Order;
use frontend\models\OrderSearch;
use frontend\models\User;
use frontend\models\DocumentType;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {			
	
        $searchModel = new OrderSearch();		
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	
		$dataProvider -> query -> andFilterWhere(['>', 'paid_at', 0]);
		$dataProvider -> query -> andFilterWhere(['owner_id' => User::currentUser()]);
	
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	
	
	/*
	* Перевод статуса заказа в Формирвоание
	*/
	public function actionFormation($id) {
		
		$model = Order::findOne($id);
		$model -> changeStatus('formation', true);
		Yii::$app->session->setFlash('success', Yii::t('app', 'Заказ '.$model -> number.' переведен в статус '.$model -> status -> name));
		return $this -> redirect(['/owner/order']);
	}	
	
	/*
	* Перевод статуса заказа в Доставка
	*/
	public function actionDelivery($id) {
		
		$model = Order::findOne($id);
		$model -> changeStatus('delivery', true);
		Yii::$app->session->setFlash('success', Yii::t('app', 'Заказ '.$model -> number.' переведен в статус '.$model -> status -> name));
		return $this -> redirect(['/owner/order']);
	}	
	
	
	public function actionDocument($id)
    {
		$model = $this->findModel($id);
		if (!$model) throw new NotFoundHttpException('Некорректный id заказа: '.$id);
		$rList = array_keys((User::getCurrentUser() -> requisiteList()));		
		
		$invoiceTypeId = DocumentType::byAlias('invoice');        
        $documents = $model -> getDocuments()-> where(['doc_type_id' => $invoiceTypeId, 'owner_id' => $rList]) -> all();		
        return $this->render('document', [
			'model' => $model,
            'documents' => $documents,
        ]);
    }
	
	
    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {           
			if ($model->save()) {                       
				//return $this->redirect(['view', 'id' => $model->id]);
			}
		};
           
           
		return $this->render('update', [
			'model' => $model,

		]);
    }

	
	// карточка заказа 
	public function actionView($id) {		
	
		$model = $this->findModel($id);
		return $this->render('view', [
			'model' => $model,						
		]);
		
	}
	
	


	 protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {			
            throw new NotFoundHttpException('Запрашиваемая модель не найдена.');
        }
    }	
	
	
}
