<?php

namespace frontend\controllers\owner;

use frontend\forms\owner\ServiceForm;
use frontend\models\Service;
use frontend\models\User;
use Yii;
use frontend\models\ServiceSearch;

/**
 */
class ServicesController extends BasicController
{

    public function actionIndex()
    {
        $app = Yii::$app;
        $request = $app->request;
        $post = $request->post();
        $get = $request->get();
        $session = $app->session;

        $userId = User::getUserId();
        $searchModel = new ServiceSearch();

        if (isset($get['act'])) {
            switch ($get['act']) {
                case 'delete':
                    try {
                        Service::deleteService($get['id'], $userId);
                    } catch (\Exception $e) {
                        $session->setFlash('error', $e->getMessage());
                    }
                    break;
            }
            return $this->redirect(['']);
        }


        $post['owner_id'] = $userId;
        $dataProvider = $searchModel->search($post);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {		
        $app = Yii::$app;
        $app->layout = null;
        $service = Service::findOne($id);
        $serviceForm = new ServiceForm();

        if ($app->request->isPost) {
            $post = $app->request->post();

            Service::updateService($id, $post);
        }


        return $this->render('update', [
            'service' => $service,
            'serviceForm' => $serviceForm,
        ]);
    }

    public function actionCreate()
    {
        $app = Yii::$app;
        $app->layout = null;
        $serviceForm = new ServiceForm();
        $service = new Service();

        if ($app->request->isPost) {
            $post = $app->request->post();

            Service::createService($post);
        }


        return $this->render('update', [
            'service' => $service,
            'serviceForm' => $serviceForm,
        ]);
    }

}
