<?php

namespace frontend\controllers\owner;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use common\models\SettlementSearch;
use frontend\models\OwnerSettlementSearch;
use common\models\PaymentSearch;
use common\models\Entity;
use common\models\EntityHelper;
use frontend\models\User;


class DocumentController extends \frontend\controllers\DocumentController {


	public function beforeAction($action)
	{
		$this -> cabinet = self::UC_OWNER;		
		$this -> layout = 'owner.php';
		
		if ((Yii::$app->user->isGuest || !User::getCurrentUser() -> checkRole(User::ROLE_OWNER)) && (!in_array($action -> id, ['signup', 'signup-complete']))) 
				throw new ForbiddenHttpException (self::EM_OWNER_ROLE_REQUIRED);
		
		$result = parent::beforeAction($action);
		return $result;
	}

	
	
	// Расчеты по адресам
    public function actionSettlements()
    {
        $searchModel = new OwnerSettlementSearch();
        //$searchModel->load(Yii::$app->request->post());
       // $searchModel->recipientId = $this->getRcipients();
       // $searchModel->payerId = $this->getPayers();

        //$searchModel -> load($this -> getSearchModelParams(), '');

        $dataProvider = $searchModel->search(Yii::$app->request->get());


        return $this->render('settlements', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }	
	
	
	// Возвращает список неоплаченных счетов для указания оплаты
	public function actionPayconfirm() {
		$searchModel = new SettlementSearch();
		$searchModel -> documentTypes = [SettlementSearch::SHOW_INVOICES];// только счета
		$searchModel -> recipientId = $this -> getRcipients();
		$params = Yii::$app->request->post();
		if (isset($params['po-date'])&&isset($params['po-number'])&&isset($params['po-summa'])) {
			EntityHelper::initPayments($params['po-date'], $params['po-number'], $params['po-summa']);
		}
		
		//$searchModel -> load(Yii::$app->request->get(), '');
        $dataProvider = $searchModel->search();
		$dataProvider -> query -> andWhere(['state' => 0, 'porder_id' => NULL]) -> andWhere(['not', ['order_id' => NULL]]);

		//echo 'sql: '.$dataProvider -> query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
		//var_dump($searchModel -> documentTypes);
		
		return $this->render('payconfirm', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
		
	}

	// Плательщик денег - кто угодно
	public function getPayers(){
		
			return [];
	}
	
	
	// Получатель денег - Я
	public function getRcipients(){		
		$list = null;
		
		$user = Entity::getCurrentUser();
		if ($user) {
			$list = array_keys($user -> requisiteList());
		}
		// return ['recipientId' => $list];
		return $list;
	}
	
	
	/*

	// Дополнительные ограничения, накладываемые на список документов
	protected function getSearchModelParams() {
		$user = Entity::getCurrentUser();
		$list = array_keys($user -> requisiteList());
		
		return ['recipientId' => $list];
	}
	
	*/
	
	public function hiddenFilter() {
		
		return [
			'documentTypes' => [SettlementSearch::SHOW_INVOICES],			
		];
	}
		
	
}