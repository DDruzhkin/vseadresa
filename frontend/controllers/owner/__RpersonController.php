<?php

namespace frontend\controllers\owner;

use Yii;
use backend\models\Rperson;


/**
 * RpersonController implements the CRUD actions for Rperson model.
 */
class RpersonController extends RController
{

    public function actionCreate()
    {
        $model = new Rperson();
		$model -> integrate();
		return parent::create($model);
	}

    
}
