<?php

namespace frontend\controllers\owner;

use frontend\forms\owner\OrderSearchForm;
use frontend\models\Service;
use frontend\models\ServiceSearch;
use frontend\models\StatusChange;
use Yii;
use backend\models\Order;
use frontend\models\OrderSearch;
use yii\web\NotFoundHttpException;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrdersController extends BasicController
{

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $app = Yii::$app;
        $request = $app->request;
        $session = $app->session;
        $searchModel = new OrderSearch();
        $params = [];
        $params['OrderSearch'] = $session->get('OrderSearch');
        $params['owner_id'] = (string)$app->user->id;

        if ($request->isPost) {
            $post = $request->post();
            $session->set('OrderSearch', $post['OrderSearchForm']);
            return $this->redirect(['']);
        }

        $dataProvider = $searchModel->search($params);


        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'orderSearchForm' => new OrderSearchForm(),
            'params' => empty($params['OrderSearch']) ? [] : $params['OrderSearch'],
        ]);
    }

    /**
     *
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'services' => ServiceSearch::searchByOrderId($id),
            'statusChanges' => StatusChange::searchByOrderId($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->load(Yii::$app->request->post());
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->load(Yii::$app->request->post());
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}
