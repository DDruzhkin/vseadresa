<?php

namespace frontend\controllers\owner;

use Yii;

use common\models\Entity;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RController общий предок для работы с реквизитами разного типа (ИП, физлица, юрлица)
 */
class RController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }



    public function create($model)
    {        
		$type = $model -> modelId();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		$model->load(Yii::$app->request->post()); $model->save();
            return $this->redirect(['/owner/user']);
        } else {
            return $this->render('@frontend/views/'.$type.'/create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Rperson model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
		$type = $model -> modelId();
		
        if ($model->load(Yii::$app->request->post())) {           
			if ($model->save()) {                       
				return $this->redirect(['/owner/user']);
			}
		};
           
           
		return $this->render('@frontend/views/'.$type.'/update', [
			'model' => $model,
		]);
    }

    /**
     * Deletes an existing Rperson model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }    
}
