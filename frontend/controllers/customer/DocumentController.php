<?php
namespace frontend\controllers\customer;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use common\models\SettlementSearch;
use common\models\PaymentSearch;
use common\models\Entity;
use frontend\models\User;

class DocumentController extends \frontend\controllers\DocumentController {


	public function beforeAction($action)
	{
		$this -> cabinet = self::UC_CUSTOMER;		
		$this -> layout = 'customer.php';
		
		if ((Yii::$app->user->isGuest || !User::getCurrentUser() -> checkRole(User::ROLE_CUSTOMER)) && (!in_array($action -> id, ['signup']))) 				
			throw new ForbiddenHttpException (self::EM_CUSTOMER_ROLE_REQUIRED);

				
		$result = parent::beforeAction($action);		
		return $result;
	}



	// Дополнительные ограничения, накладываемые на список документов
	protected function getSearchModelParams() {		
		$user = Entity::getCurrentUser();
		$list = array_keys($user -> requisiteList());
		
		return ['payerId' => $list];
	}
	
	
}