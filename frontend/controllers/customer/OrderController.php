<?php

namespace frontend\controllers\customer;


use common\models\Contract;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;

use common\models\User;
use common\models\PayManager;
use common\controllers\EntityController;


use frontend\models\Address;
use frontend\models\DocumentType;
use frontend\models\Order;
use frontend\models\OrderSearch;
use frontend\models\Page;
use yii\web\Request;


/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends BasicController
{
    const WIZARD_COUNT = 3;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    // перехватим проверку прав, чтобы запомнить адрес, который пользователь хотел заказать
    public function beforeAction($action)
    {
        if ($action->id == 'create') { // сохраняем в сессии id адреса, на который была попытка заказа
            $params = Yii::$app->request->get();
            if (isset($params['address_id'])) {
                Yii::$app->session['ordered_address_id'] = $params['address_id'];
            }
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['customer_id' => User::currentUser()]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionGuarantee()
    {

        if (Yii::$app->request->isPost) { // по посту идет подтверждение заказа
            $params = Yii::$app->request->post();

            $id = $params[Order::baseClassName()]['id'];
            if (!$id) throw new NotFoundHttpException('Не указан id заказа');
            $model = Order::findOne($id);
            if (is_null($model)) throw new NotFoundHttpException('Некорректный id заказа: ' . $id);
            $model->createGuarantee();
            return $this->redirect(['/customer/order/document/' . $id]); // отправляем на список счетов
        } else {
            $id = Yii::$app->request->get()['id'];
            if (!$id) throw new NotFoundHttpException('Не указан id заказа');
            $model = Order::findOne($id);
            if (is_null($model)) throw new NotFoundHttpException('Некорректный id заказа: ' . $id);
            $page = Page::bySlug('guarantee');
            if (is_null($page)) throw new NotFoundHttpException('Не найдена страница guarantee');
            return $this->render('guarantee', [
                'model' => $model,
                'page' => $page,
            ]);
        }
    }

    /*
    * Подтверждение заказа - выставляются счета
    */
    public function actionConfirm($id)
    {
        $model = Order::findOne($id);
        $model->confirm();
        return $this->redirect(['/customer/order/document/' . $id]); // отправляем на список счетов
    }

    /**
     * Buy a new address.
     *
     * @param $id
     *
     * @return string
     *
     * @throws NotFoundHttpException
     */
    public function actionBuy($id)
    {
        if (!$address = Address::findOne($id)) {
            throw new NotFoundHttpException('Адрес не найден!');
        }

        $order = new Order();
        $order->address_id = $address->id;
        $order->integrate();

        return $this->render('create', [
            'order' => $order,
            'address' => $address,
            'contract' => new Contract,
            'step' => 1,
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     *
     * @throws NotFoundHttpException
     */
    public function actionStored($id)
    {
        if (!$address = Address::findOne($id)) {
            throw new NotFoundHttpException('Адрес не найден!');
        }
        $request = Yii::$app->request;

        $input = $request->post(Order::baseClassName());

        $order = new Order();
        $order->address_id = $id;
        $order->save(false, null, false, false);

        $contract = new Contract([
            'scenario' => isset($input['scenario']) ? $input['scenario'] : Contract::SCENARIO_REGISTER
        ]);

        if ($request->isPost && $order->load($request->post())) {
            $order->address_id = $id;
            $order->address_price = $address->{$input['duration']};
            $order->duration = Address::getMeta($input['duration'], 'description');
            $order->save();

            if ($contract->load($request->post(), Order::baseClassName()) && $contract->validate()) {
                $contract->order_id = $order->id;
                $contract->save();
            }

            return $this->redirect(["/order/{$order->id}/delivery"]);
        }

        return $this->redirect([$request->referrer ?: $request->baseUrl]);
    }

    /**
     *
     *
     * @param string $id
     *
     * @return string|\yii\web\Response
     */
    public function actionDelivery($id)
    {
        $order = $this->findModel($id);

        return $this->render('update', [
            'order' => $order,
            'address' => $order->address,
            'contract' => new Contract,
            'step' => 2,
            'wizard_count' => self::WIZARD_COUNT,
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response | mixed
     */
    public function actionDeliveryStored($id)
    {
        $order = $this->findModel($id);

        $request = Yii::$app->request;

        if ($request->isPost && $order->load($request->post())) {
            $order->save();

            //return $this->redirect(['/customer/order']);
        }

        return $this->render('update', [
            'order' => $order,
            'address' => $order->address,
            'contract' => new Contract,
            'step' => 3,
            'wizard_count' => self::WIZARD_COUNT,
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @deprecated
     */
    public function actionCreate()
    {    // через GET должен быть переадан параметр address_id - идентификатор адреса, на который создается заказ
        $params = Yii::$app->request->get();
        if (!isset($params['address_id'])) throw new NotFoundHttpException('Не указан обязательный параметр address_id!');
        $address = Address::findOne($params['address_id']);
        if (!$address) throw new NotFoundHttpException('Адрес id=' . $params['address_id'] . ' не найден!');
        $params = Yii::$app->request->post();

        $model = new Order();
        $model->address_id = $address->id;
        $model->integrate();


        return $this->render('create', [
            'model' => $model,
            'address_id' => $address->id,
            'step' => 2,
            'wizard_count' => self::WIZARD_COUNT,
        ]);

    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {

        $get = Yii::$app->request->get();
        $step = (isset($get['step']) && ($get['step'] > '') && ($get['step'] > 1)) ? $get['step'] : 1;
        $id = (isset($get['id']) && ($get['id'] > '')) ? $get['id'] : false;

        if ($id) $model = $this->findModel($id);
        $post = Yii::$app->request->post(Order::baseClassName());
        if ($post) {
            if (!$id) {
                $model = new Order(); // Создаем новую модель
                $model->address_id = $post['address_id'];
                $model->save(); // Сохраняем сразу, т.к. дальнейшая работа возможна только с сохраненным заказом
            }

            //var_dump($post); exit;
            $model->load(Yii::$app->request->post()); // Загружаем данные из запроса в модель

            $model->duration = Address::getMeta($post['duration'], 'description');
            if ($model->save()) {
                if ($step > self::WIZARD_COUNT)  // Конец мастера - переходим на список заказов
                    return $this->redirect(['/customer/order/']);
                if(!isset($post['last_step']))
                    return $this->redirect(["/order/$id/delivery"]);
                else
                    return $this->redirect(['/customer/order/']);
            }
        };

        if (!$model) throw new \Exception('Некорректный ID или модель не найдена');

        //$model -> integrate();
        return $this->render('update', [
            'order' => $model,
            'step' => $step,
            'address' => $model->address,
            'contract' => new Contract,
            'wizard_count' => self::WIZARD_COUNT,
        ]);


    }

    public function actionDelete($id)
    {

        $this->findModel($id)->delete();

        return $this->redirect(['/customer/order/']);
    }


    // Кдлнировать заказ с $id
    public function actionClone($id)
    {
        $order = self::findModel($id);
        $model = new Order();
        $attrbutes = $order->attributes;
        unset($attrbutes['id']);
        unset($attrbutes['created_at']);
        unset($attrbutes['delivery_id']);
        unset($attrbutes['number']);
        //var_dump($attrbutes ); exit;
        $model->load($attrbutes, '');
        $model->save();
        $model->serviceIdList = $order->serviceIdList;
        $model->save();
        return $this->redirect(Url::toRoute(['update', 'id' => $model->id]));
    }


    // карточка заказа
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);

    }


    public function actionDocument($id)
    {
        $model = $this->findModel($id);
        if (!$model) throw new NotFoundHttpException('Некорректный id заказа: ' . $id);
        $invoiceTypeId = DocumentType::byAlias('invoice');
        $documents = $model->getDocuments()->andWhere(['doc_type_id' => $invoiceTypeId])->all();

        return $this->render('document', [
            'model' => $model,
            'documents' => $documents,
        ]);
    }


    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая модель не найдена.');
        }
    }


}
