<?php

namespace frontend\controllers\customer;

use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

use frontend\components\AccessRule;
use frontend\models\User;


class BasicController extends \frontend\controllers\BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'ruleConfig' => [
                    'class' => AccessRule::class,
                ],				
				'rules' => [
                    [
						'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
				
				
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_CUSTOMER],
                    ],
                ],
				
				
                'denyCallback' => function() {
                    Yii::$app->session->setFlash('error', 'У вас нет доступа к этой странице');
                    $this->goHome();
                }
            ],

            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
	
	
	public function beforeAction($action)
	{		
		
		
		
		
		$result = parent::beforeAction($action);
		if ($result) {
			
			$this -> layout = 'customer.php';					

			if ((Yii::$app->user->isGuest || !User::getCurrentUser() -> checkRole(User::ROLE_CUSTOMER)) && (!in_array($action -> id, ['signup']))) 				
					throw new ForbiddenHttpException (self::EM_CUSTOMER_ROLE_REQUIRED);
				
			if (in_array($action -> id, ['signup'])) $this -> layout = 'main.php';// регистрация 
			
			
			
		//if ($this -> id == 'customer/order' && in_array($action -> id,['create', 'update'])) $this -> layout = 'content.php';// оформление заказа
		};
		
		return $result;
				
		
	}
	
	

}