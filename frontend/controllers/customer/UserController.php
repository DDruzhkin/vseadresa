<?php

namespace frontend\controllers\customer;


use common\models\EntityHelper;
use common\models\NotificationManagerExt;
use frontend\models\ChangePassword;
use frontend\models\Rcompany;
use Yii;
use common\models\User;
use common\models\UserSearch;
use common\controllers\EntityController;
use common\models\NotificationManager;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\SignupForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex()
    {
        $id = User::currentUser();
        $model = User::findOne($id);
        $passwordModel = new ChangePassword();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                //return $this->redirect(['view', 'id' => $model->id]);
            }
        };

        return $this->render('update', [
            'model' => $model,
            'passwordModel' => $passwordModel,
        ]);
    }

    public function actionRequisite($id)
    {
        $requisite = Rcompany::find()
            ->where([
                'id' => $id,
                'user_id' => Yii::$app->user->identity->getId(),
            ])
            ->one();

        return $this->asJson($requisite);
    }

    // Установить профиль (реквизиты по умолчанию)
    public function actionDefaultProfile($id, $rid)
    {
        $model = $this->findModel($id);
        $model->default_profile_id = $rid;
        $model->save();
        $username = $model->id == $model->portal()->id ? '' : $model->username;
        return $this->redirect(Url::toRoute(['user']));
    }


    public function actionSignup()
    {

        $model = new SignupForm();

        $app = Yii::$app;
        $request = $app->request;
        $session = $app->session;

        $model->role = User::ROLE_CUSTOMER;
//		$model -> status_id = $model -> statusByAlias('active') -> id;

        if ($request->isAjax && $model->load($request->post())) {
            $model->email = $model->username;
            $app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load($request->post())) {
            if ($user = $model->signup()) {
                if ($app->getUser()->login($user)) {
                    $session->setFlash('success', Yii::t('app', 'registeredSuccessfully'));
					if (Yii::$app->session -> get('ordered_address_id')) { // Была попытка закза адреса без авторизации - перенаправляем на процесс заказа
						$address_id = Yii::$app->session['ordered_address_id'];
						Yii::$app->session -> remove('ordered_address_id');
						return $this->redirect(["/address/{$address_id}/buy"]);
						//return $this->redirect(['/order/create', 'address_id' => $address_id]);
					};
                    return $this->redirect(Url::to(['/customer']));

                }

            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

}
