<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Notification;
use frontend\models\User;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\StringHelper;


class NotificationController extends BasicController
{

	public function actionIndex() {
		$user = User::getCurrentUser();
		if (!$user) return false;
		
		$role = false;
		if ($user -> checkRole(User::ROLE_OWNER)) $role = 'owner';
		if ($user -> checkRole(User::ROLE_CUSTOMER)) $role = 'customer';
		if ($user -> checkRole(User::ROLE_ADMIN)) $role = 'admin';		
		if (!$role) return false;
		
		$this -> layout = $role;
		
		if (Yii::$app->request->get()) {		
			$params = Yii::$app->request->get(); 
			$user -> setNotifications(@$params['type'], @$params['statuses'], @$params['emails'], @$params['nnames']);
		}
		
		
		
		return $this->render('update', [
            'role' => $role,            
			'user' => $user,            
        ]);
		
		
	}


}