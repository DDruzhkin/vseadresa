<?php

namespace frontend\controllers;

use Yii;
use backend\models\Page;
use common\models\PageSearch;
use common\controllers\EntityController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex($slug)
    {
		
		$model = Page::bySlug($slug);
		if (is_null($model)) throw new NotFoundHttpException('Страница '.$slug.' не найдена');
		$this -> setModel($model);
		
		$breadcrumbs = [];
		
		$parents = $model -> getParents();
		foreach($parents as $page) {
			$breadcrumbs[] = [
				'label' => $page -> h1,
				'url' => $page -> getUrl(),
				'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
			];
			
		}
		
		$breadcrumbs = array_reverse($breadcrumbs);
		$breadcrumbs[] = [
				'label' => $this -> model -> h1,								
		];
		

		$viewName = $this -> model -> template;		// проверим существование шаблона по указанному шаблону
		if (!($viewName && $this -> checkView($viewName))) {
			$viewName = $this -> model -> type;		// проверим существование шаблона по типу						
			if (!($viewName && $this -> checkView($viewName))) {				
				$viewName = $this -> model -> slug;		// проверим существование шаблона по имени				
				if (!($viewName && $this -> checkView($viewName))) {
					$viewName = 'default';
				}
			};
		}
		
		// Используется имя Представления указанного шаблона для страницы, 
		// Если не указан или не существует, то совпадающим с именем типа страницы,
		// Если не существует, то совпадающим со slug страницы,
		// Если не существует, то default


        return $this->render($viewName, [
            'model' => $this -> model,
			'breadcrumbs' => $breadcrumbs,

        ]);
    }

}
