<?php

namespace frontend\controllers;

use Yii;

use common\models\Entity;
use common\models\User;
use common\models\Rip;
use common\models\Rcompany;
use common\models\Rperson;
use common\models\RequisiteManager;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;



/**
 * RController общий предок для работы с реквизитами разного типа (ИП, физлица, юрлица)
 */
class RController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


	
	
	public function beforeAction($action){
		$result = parent::beforeAction($action);
		
		if (User::getCurrentUser() -> checkRole(User::ROLE_OWNER)) {
			$this -> layout = 'owner';
		};
		if (User::getCurrentUser() -> checkRole(User::ROLE_CUSTOMER)) {
			$this -> layout = 'customer';
		}
		
		
		
		return $result;
	}

	

    public function create($model)
    {        
		$type = $model -> modelId();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		$model->load(Yii::$app->request->post()); $model->save();
            return $this->redirect(['/profile']);
        } else {
            return $this->render('@frontend/views/'.$type.'/create', [
                'model' => $model,
            ]);
        }
    }

	
	public function actionCreate($type)
    {		
		$className = Inflector::camelize($type);
		$model = (new \ReflectionClass('\common\models\\'.$className)) -> newInstance();				        
		$model -> integrate();
		return $this -> create($model);
	}
	
	
    /**
     * Updates an existing Rperson model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
		$type = $model -> modelId();
		if (Entity::getCurrentUser() ->checkRole(User::ROLE_OWNER)) {
			Yii::$app->session->setFlash('error', 'Изменение данных для договора потребует заключение дополнительного соглашения об изменении реквизитов договора в течении одной недели, в случае, если договор не будет перезаключен, будут восстановлены данные текущего договора. До момента подписания личный кабинет будет заблокирован.');
		}
		
        if ($model->load(Yii::$app->request->post())) {           
			$newModel = RequisiteManager::save($model);
			if ($newModel && ($newModel -> id != $model -> id)) {  
//				$newModel -> user -> ownerAgreement();
				if (Entity::getCurrentUser() ->checkRole(User::ROLE_OWNER)) {
					Yii::$app->session->setFlash('success', 'Внимание! Реквизиты были изменены, Вам необходимо подписать дополнительное соглашение на их изменение.');
				} else {
					Yii::$app->session->setFlash('success', 'Изменения сохранены.');
				}
				return $this->redirect(['/profile']);
			} else {
				Yii::$app->session->setFlash('success', 'Изменения сохранены.');
			}
		};
           
           		
		return $this->render('@frontend/views/'.$type.'/update', [
			'model' => $model,
		]);
    }

	
	
    /**
     * Deletes an existing Rperson model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }    
}
