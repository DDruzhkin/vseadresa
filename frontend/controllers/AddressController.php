<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Address;
use frontend\models\Okrug;
use frontend\models\Nalog;
use frontend\models\Metro;
use frontend\models\Option;

use frontend\models\AddressSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\StringHelper;

/**
 * AddressController implements the CRUD actions for Address model.
 */
class AddressController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),

            ],
        ];
    }
	
	public function beforeAction($action)
	{
		$this -> layout = 'addresses';
		return parent::beforeAction($action);	
	}

	

	
	public function actionView($id)
    {
		$model = $this -> findModel($id);
        $searchModel = new AddressSearch(['okrug_id'=>array(0=>$model->okrug_id), 'nalog_id'=>array($model->nalog_id), 'metro_id'=>$model->metro_id]);
		$this -> setModel($model);
		if (!$model) throw new NotFoundHttpException('Запрашиваемый адрес не найден');
		$vars = [];
		foreach(['okrug', 'nalog', 'metro', 'option'] as $name) {
			$vars[$name] = false;
		}
        $vars['searchModel'] = $searchModel;
		return $this->render('view', [
			'address' => $model,			
			'vars' => $vars
		]);
    }
	
	
	public function actionPrices() 
	{
		
		$this -> layout = 'main';
        Yii::$app->params['page'] = 'price';
		if (Address::portal() -> getOptions('address.pricelist.seo.h1')) $this -> h1 = Address::portal() -> getOptions('address.pricelist.seo.h1');		
		if (Address::portal() -> getOptions('address.pricelist.seo.title')) $this -> title = Address::portal() -> getOptions('address.pricelist.seo.title');		
		if (Address::portal() -> getOptions('address.pricelist.seo.description')) Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Address::portal() -> getOptions('address.pricelist.seo.description')], 'description');	
		
		
        $searchModel = new AddressSearch();
        $dataProvider = $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();

        return $this->render('prices', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
	}
	
	
	// Главная страница
	public function actionHome()
    {

		if (Address::portal() -> getOptions('home.seo.h1')) $this -> h1 = Address::portal() -> getOptions('home.seo.h1');		
		if (Address::portal() -> getOptions('home.seo.title')) $this -> title = Address::portal() -> getOptions('home.seo.title');
		
		if (Address::portal() -> getOptions('home.seo.description')) Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Address::portal() -> getOptions('home.seo.description')], 'description');
		if (Address::portal() -> getOptions('home.seo.keywords')) Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Address::portal() -> getOptions('home.seo.keywords')], 'keywords');
		
		
        Yii::$app->params['page'] = 'main';
        $searchModel = new AddressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('home', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);       
    }
	
	
	
	public function actionSearch()
    {
        $searchModel = new AddressSearch();
				
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('_search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);       
    }
	
    public function actionIndex()
    {
		if (Address::portal() -> getOptions('address.list.seo.h1')) $this -> h1 = Address::portal() -> getOptions('address.list.seo.h1');		
		if (Address::portal() -> getOptions('address.list.seo.title')) $this -> title = Address::portal() -> getOptions('address.list.seo.title');
		
		if (Address::portal() -> getOptions('address.list.seo.description')) Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Address::portal() -> getOptions('address.list.seo.description')], 'description');
		if (Address::portal() -> getOptions('address.list.seo.keywords')) Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Address::portal() -> getOptions('address.list.seo.keywords')], 'keywords');
	
		return $this -> show();
    }
	
	
	public function actionMetro($code)
    {
		$model = Metro::byAlias($code);
		$this -> setModel($model);
		if (!$model) throw new NotFoundHttpException();
        return $this -> show(['metro' => $model]);
    }
	
	public function actionOkrug($code)
    {
        $model = Okrug::byAlias($code);
		$this -> setModel($model);
		if (!$model) throw new NotFoundHttpException();
        return $this -> show(['okrug' => $model]);
    }
	
	public function actionNalog($code)
    {
		$model = Nalog::byNumber($code);
		$this -> setModel($model);
		if (!$model) throw new NotFoundHttpException();
        return $this -> show(['nalog' => $model]);
    }
	
	public function actionOption($code)
    {	
		$model = Option::byAlias($code);		
		$this -> setModel($model);
		if (!$model) throw new NotFoundHttpException('Не определен alias для фильтра');
        return $this -> show(['options' => $model]);
    }
	
	protected function show($vars = []) 
	{
		$searchModel = new AddressSearch();
		$params = Yii::$app->request->queryParams;

		$searchModel->load($params);
				
		foreach(['okrug', 'nalog', 'metro', 'options'] as $name) {
			if(!array_key_exists($name, $vars)) {
				$vars[$name] = false;				
			} else {
				if ($searchModel[$name.'_id']) {
					$searchModel[$name.'_id'] = array_merge($searchModel[$name.'_id'], [$vars[$name] -> id]);
				} else {
					$searchModel[$name.'_id'] = [$vars[$name] -> id];
				}
			}
		}		
        		
		$searchModel->demo = true;
		$dataProvider = $searchModel->search();
        //$dataProvider->query->where(['demo'=>1]);
		//echo $dataProvider->query->createCommand()->getRawSql(); echo "<br>";
		$dataProvider -> pagination -> pagesize = 9;

		$vars['searchModel'] = $searchModel;
        return $this->render('index', array_merge([
			'vars' => $vars,
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider
		], $vars)
		);
	}
	
}
