<?php

namespace frontend\controllers;

use Yii;

use common\models\Entity;
use common\models\EntityExt;
use common\models\PayManager;
use common\models\SettlementSearch;
use common\models\PaymentSearch;
use frontend\models\Document;
use frontend\models\Invoice;
use frontend\models\Payment;
use frontend\models\User;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class DocumentController extends BasicController
{
	
	const UC_CUSTOMER = 'customer';
	const UC_OWNER = 'owner';
	
	public $cabinet;

	public static function getCabinet() {
		$result = 'main';
		$cuser = User::getCurrentUser();
		if ($cuser) {
			if ($cuser -> checkrole(User::ROLE_CUSTOMER)) return self::UC_CUSTOMER;
			if ($cuser -> checkrole(User::ROLE_OWNER)) return self::UC_OWNER;
		};
		return $result;
	}
	
	
	
	// Сюда направляется платежная система после совершения оплаты по карте.
	// Здесь создается объект оплаты (Payment), изменяется статус заказа
	public function actionPayBack($orderId) {
		if (!User::getCurrentUser()) throw new ForbiddenHttpException('Страница не доступна для незарегистрированного пользователя');
		$this -> layout = self::getCabinet().'.php';
		$payId = $orderId;		
		$payment = Payment::findOne(['pay_id' => $payId]);
		if (is_null($payment)) {
			throw new \Exception('Попытка проведения некорректного платежа');
		}		
		
		if ($payment -> confirmed) {
			//throw new NotFoundHttpException('Попытка дублирования оплаты по карте');
		}

		//проверка статуса заказа
        $payCheck=PayManager::payCheck([
            'id'=>$payment->pay_id,
            'number'=>$payment->id
        ]);
        if(!$payCheck['actionCode']) {
            $payment->confirmed = 1; // Подтверждаем платеж, без этого флажка оплата считается не проведенной!
            $payment->save();
            $payment->invoice->balance(); // счет сам проверяет все платежи и выставляет себе состояние оплачен или нет
            $payMessage='Счет '.$payment->id.' оплачен успешно';
            $payStatus=true;
        }else{
            $payMessage='Ошибка оплаты: '.$payCheck['errorMessage'];
            $payStatus=false;
        }
		Yii::$app -> eventManager -> callEvent('afterPayment', ['payment' => $payment]); 
		
		return $this->render('paid', [
            'payment' => $payment,
            'payStatus'=>$payStatus,
            'message'=>$payMessage
        ]);
		
	}
	
	public function actionPayFail($orderId) {
		echo "PayFail";
		var_dump($orderId); 
	}
	
	
	
	// оплата по счету c $id
	public function actionPay($id) {
		if (!User::getCurrentUser()) throw new ForbiddenHttpException('Страница не доступна для незарегистрированного пользователя');
		$model = Document::findOne($id);
		
		if (is_null($model)) throw new NotFoundHttpException('Счет не найден id='.$id);
		if ($model -> state == Invoice::STATE_PAID) throw new NotFoundHttpException('Счет №'.$model -> number.' уже оплачен ');
		$amount = $model -> summa;		
		// Создаем оплату
		$payment = new Payment();
		$payment -> ptype = Payment::PTYPE_CARD;
		$payment -> value = $amount;
		$payment -> invoice_id = $id;
		$payment -> save();
		$number = $payment -> id;
		$result = $payment -> recipient -> payRegOrder([
			'number' => $number, 
			'amount' => $amount, 
			'returnUrl' => Url::base(true).Url::to(['/document/pay-back']),
		]);
		//echo "$number<br>";
		if (isset($result['errorCode']) && $result['errorMessage']) {
			throw new \Exception('Ошибка: ['.$result['errorCode'].'] '.$result['errorMessage']. ' №'.$number);
		} else {			
			$payId = $result['orderId'];
			$payment -> pay_id = $payId;
			$payment -> save();
				
			return $this -> redirect($result['formUrl']);
		}
	}
			
	
    public function actionSettlements()
    {
		if (!User::getCurrentUser()) throw new ForbiddenHttpException('Страница не доступна для незарегистрированного пользователя');
        $searchModel = new SettlementSearch();
        $searchModel->load(Yii::$app->request->get());
        $searchModel->recipientId = $this->getRcipients();
        $searchModel->payerId = $this->getPayers();

        //$searchModel -> load($this -> getSearchModelParams(), '');

        $dataProvider = $searchModel->search();


        return $this->render('@frontend/views/'.$this -> cabinet.'/document/settlements', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }


    public function actionPsettlements()
    {
		if (!User::getCurrentUser()) throw new ForbiddenHttpException('Страница не доступна для незарегистрированного пользователя');
        $searchModel = new SettlementSearch();
        $searchModel->load(Yii::$app->request->get());
		
        //$searchModel -> load($this -> getSearchModelParams(), '');

        $user = EntityExt::portal();
        $list = array_keys($user->requisiteList());
        $searchModel->recipientId = $list;

        $searchModel->payerId = $this->getRcipients();

        $dataProvider = $searchModel->search();
        return $this->render('@frontend/views/document/psettlements', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    // Плательщик денег - Я
    public function getPayers()
    {
        $user = Entity::getCurrentUser();
        $list = array_keys($user->requisiteList());

        return $list;
    }

    // Получатель денег - кто-угодно
    public function getRcipients()
    {
        return [];
    }

    // Просмотр документа
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('@frontend/views/document/view', [
            'model' => $model,
        ]);
    }

    // Просмотр Pdf документа
    public function actionShowPdf($id)
    {
        return $this->redirect(Document::findOne($id)->getPdf());
    }



}