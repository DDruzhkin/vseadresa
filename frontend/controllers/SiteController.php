<?php

namespace frontend\controllers;

use common\models\NotificationManager;
use common\models\NotificationManagerExt;
use common\models\StatusChange;
use common\models\Entity;
use frontend\models\NotificationsVsStatusesSettings;
use frontend\models\Status;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\OwnerSignup;
use frontend\models\ContactForm;
use frontend\models\DeliveryAddress;
use frontend\models\PhotoCalendar;

use common\models\Metro;
use frontend\models\User;
use common\models\Order;
use frontend\models\OrderSearch;
use common\models\Nalog;
use common\models\Okrug;
use common\models\ServiceName;
use frontend\models\Document;
use frontend\models\AddressSearch;
use frontend\models\Address;
use common\models\Rcompany;
use common\models\Rperson;
use common\models\Rip;
use common\models\Region;
use yii\data\ActiveDataProvider;
use yii\web\View;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;
use frontend\models\Invoice;
use frontend\models\Porder;

/**
 * Site controller
 */
class SiteController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'cab', 'apply_data', 'cab_owner', 'orders', 'document', 'request-password-reset', 'contracting'],

                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'cab', 'apply_data', 'cab_owner', 'orders', 'document', 'request-password-reset'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
            /*
                    'eauth' => [
                                    // required to disable csrf validation on OpenID requests
                                    'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                                    'only' => ['login'],
                                ],
            */

        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function successCallback($client)
    {
        $attributes = $client->getUserAttributes();
        // user login or signup comes here
    }


    public function onAuthSuccess($client)
    {
        $attributes = $client->getUserAttributes();

        /** @var Auth $auth */
        $auth = Auth::find()->where([
            'source' => $client->getId(),
            'source_id' => $attributes['id'],
        ])->one();

        if (Yii::$app->user->isGuest) {
            if ($auth) { // login
                $user = $auth->user;
                Yii::$app->user->login($user);
            } else { // signup
                if (User::find()->where(['email' => $attributes['email']])->exists()) {
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", ['client' => $client->getTitle()]),
                    ]);
                } else {
                    $password = Yii::$app->security->generateRandomString(6);
                    $user = new User([
                        'username' => $attributes['login'],
                        'email' => $attributes['email'],
                        'password' => $password,
                    ]);
                    $user->generateAuthKey();
                    $user->generatePasswordResetToken();
                    $transaction = $user->getDb()->beginTransaction();
                    if ($user->save()) {
                        $auth = new Auth([
                            'user_id' => $user->id,
                            'source' => $client->getId(),
                            'source_id' => (string)$attributes['id'],
                        ]);
                        if ($auth->save()) {
                            $transaction->commit();
                            Yii::$app->user->login($user);
                        } else {
                            print_r($auth->getErrors());
                        }
                    } else {
                        print_r($user->getErrors());
                    }
                }
            }
        } else { // user already logged in
            if (!$auth) { // add auth provider
                $auth = new Auth([
                    'user_id' => Yii::$app->user->id,
                    'source' => $client->getId(),
                    'source_id' => $attributes['id'],
                ]);
                $auth->save();
            }
        }
    }


    public function actionTest()	{
		
		//echo "PhotoCalendar";
		
		
		
//		echo \common\widgets\photocalendar\PhotoCalendar::widget(['date' => '20.07.2018']);
		
		
		
		$cal = PhotoCalendar::byDate('27.07.2018', 1);
		$state = $cal -> getState();
		var_dump($cal -> attributes); echo "<br>";
		echo 'state: '.$state; echo " для ".$cal -> date." - ".$cal -> getInterval()."<br>";
		var_dump(PhotoCalendar::SLOT_STATE_TITLES[(int)$state]);		
		
		exit;
		Yii::$app->cache -> set('counter', 0);		
		
		$order = Order::findOne([12008]);
		
		if ($order -> duration == Order::DURATION_6) {
			$order -> duration = Order::DURATION_11;
			echo "*** 11 месяцев<br>";
		} else {
			$order -> duration = Order::DURATION_6;
			echo "*** 6 месяцев<br>";
		}
//		$order -> save();
		
		
/*
		//Yii::$app->cache -> clearObject($order -> id);
		$obj = Yii::$app->cache -> getObject($order -> id);
		if ($obj) {
			var_dump($obj -> diration);
		} 		else  {
			echo "----";
		}
		echo '+++<br>';
	
		Yii::$app->cache -> setObject($order);
*/	
//Yii::$app->cache -> setObject($order);
	//	$order -> save();
	
/*	
		foreach($order -> getServices() -> all() as $service) {								
			echo "!!!!: "; var_dump($service -> order -> duration); echo "===<br>";
		}
		
*/

		
		
		$order -> save();
		
		var_dump($order -> calculate());
		
exit;		
		
		
	//Entity::getCurrentUser() -> save();
	$user = Entity::getCurrentUser();
	
	var_dump($user -> getCaption("%contract_id% %contract_view%"));
	exit;
		
		
		/*
	$address = Address::findOne([11892]);
	echo $address -> status -> alias.'<br>';	
	var_dump($address -> balance()); echo '<br>';	
	echo $address -> status -> alias.'<br>';
*/

	$order = Order::findOne([11810]);
	
	echo $order -> getCaption();
	
	//$order -> status_id = 918;
	//$order -> save();
	
	//$order = Order::findOne([11810]);
	//echo $order -> status_view;
	
//	$order -> save();

exit;		
		$r = Rcompany::findOne([2711]);
		
		echo $r -> getHeaderText().'<br><br>';
		echo $r -> getFooterText().'<br>';
		exit;
		
		
		$address = Address::findOne([9282]);
		$address -> files_as_array = [['date' =>'11.2.88', 'name' => 'test'], ['date' =>'11.11.99', 'name' => 'test1']];
		$address -> save();
		var_dump($address);
		exit;
		//Yii::$app -> eventManager -> linkEvent('afterPayment', [$this, 'test']);
		//Yii::$app -> eventManager -> callEvent('afterPayment', ['test' => 'test1']);
		
		Yii::$app -> eventManager -> callEvent('afterPayment', ['test' => 'test1']);
		
		//var_dump(Yii::$app -> eventManager -> events);
		
		//var_dump(Yii::$app -> eventManager -> events);
			
		/*
			
		$invoice = Invoice::findOne([9111]);
		$invoice -> integrate();
		var_dump($invoice -> summa);
		exit;
		$order = Order::findOne([9108]);
		var_dump($order -> calculate());
		exit;
		*/
		
		/*
		$order = Porder::findOne([8729]);
		$order -> save();
		//$order -> bill();
		var_dump($order -> attributes);
		*/
		/*
		$order = Order::findOne([8723]);
		$order -> bill();
		var_dump($order -> attributes);
		*/
		
		/*
		$invoice = Document::findOne([9212]);
		//$invoice -> save();
		var_dump($invoice -> balance());
		*/
		
	}
	
	
// отображает макет с именем  $name
    public function actionMaket($name)
    {
        $person = User::findOne(User::currentUser());
        return $this->render($name, [
            'person' => $person,
        ]);
    }


    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        //echo "<pre>", var_dump(Yii::$app->user->identity), "</pre>" ;
echo 12345;
        $addressSearchModel = new AddressSearch();
        $metro = new Metro();// список станций метро для формы поиска
        $taxes = new Nalog(); // список налоговых для формы поиска
        $districts = new Okrug(); // список округов для формы поиска
        $services = new ServiceName(); // список услуг
        $document_cnt = Document::find()->where(['doc_type_id' => 3])->count();
        //var_dump($document_cnt); die();
        //echo "<pre>", print_r($taxes); die();
        Yii::$app->params['page'] = 'main';


        function pre($data)
        {
            echo "<pre>", print_r($data), "</pre>";
            //die();
        }

        //pre($review_Provider->getModels());
        $owners_count = User::find()->where(['role' => 'Владелец'])->count(); //количество владельцев в системе
        $address_count = Address::find()->count(); //количество адресов в системе
        //pre($owners);
        $arr = array(
            "metro" => $metro,
            "searchModel" => $addressSearchModel,
            "taxes" => $taxes,
            "districts" => $districts,
            "services" => $services,
            "document_cnt" => $document_cnt,
            "owner_cnt" => $owners_count,
            "address_count" => $address_count,
        );

        return $this->render('index', $arr);
    }


    public function actionGenerateDocument($id = 2500)
    {
        $this->layout = 'empty';
        Document::findOne($id)->generatePdf();
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $app = Yii::$app;
        $request = $app->request;
        $session = $app->session;

        if (!$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load($request->post()) && $model->login()) {
            if ($backUrl = $app->request->get('back')) {
                if (strpos(@get_headers(Url::to([$backUrl]))[0], '404') === false) {
                    return $this->redirect(Url::to([$backUrl]));
                }
            }
            return $this->goBack();
        } else {
            if (!empty($model->errors['password']))
                $session->setFlash('error', 'Неверный пароль');
            return $this->render('auth', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    /*
    // Перенесен в контроллер UserController, доступно по урлу Url::to(['/logout/'])
   public function actionLogout()
   {
       Yii::$app->user->logout();

       return $this->goHome();
   }
   */

    /**
     * Displays contact page.
     *
     * @return mixed
     */

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }


    // Переход на профиль пользователя
    /*
    // Перенесен в контроллер UserController
    public function actionProfile()
    {


        $url = Url::to(['/']);
        $user = User::findOne(User::currentUser());

        if ($user) {
            if ($user -> checkRole(User::ROLE_OWNER)) {
                $url = Url::to('/owner/');
            };
            if ($user -> checkRole(User::ROLE_CUSTOMER)) {
                $url = Url::to('/customer/');
            };
        }

        return $this->redirect([$url]);
    }

    */


    public function actionPrices()
    {
        $model = [];
        $addressSearchModel = new AddressSearch();
        $metro = new Metro();// список станций метро для формы поиска
        $taxes = new Nalog(); // список налоговых для формы поиска
        $districts = new Okrug(); // список округов для формы поиска
        $arr = [];

        /*список адресов*/

        $perPage = 6;

        $searchModel = new AddressSearch();
        $currentPage = 1;
        if (isset($_GET['page']) && (int)$_GET['page'] > 0) {
            $currentPage = (int)$_GET['page'];
        }

        $query = Address::find();
        if (isset($_GET["AddressSearch"])) {
            if (isset($_GET["AddressSearch"]['okrug_id'])) {
                if (is_array($_GET["AddressSearch"]['okrug_id'])) { // если пришло с главной страницы
                    foreach ($_GET["AddressSearch"]['okrug_id'] as $okrug_id) {
                        $okrug_id = (int)$okrug_id;
                        $query->orWhere(['okrug_id' => $okrug_id]);
                    }
                } else { // со страницы списка адресов
                    if ((int)$_GET["AddressSearch"]['okrug_id'] > 0) {
                        $district = (int)$_GET["AddressSearch"]['okrug_id'];
                        $query->andWhere(['okrug_id' => $district]);
                    }
                }

            }
            if (isset($_GET["AddressSearch"]['metro_id'])) {
                if (is_array($_GET["AddressSearch"]['metro_id'])) { // если пришло с главной страницы
                    foreach ($_GET["AddressSearch"]['metro_id'] as $metro_id) {
                        $metro_id = (int)$metro_id;
                        $query->orWhere(['metro_id' => $metro_id]);
                    }
                } else { // со страницы списка адресов
                    if ((int)$_GET["AddressSearch"]['metro_id'] > 0) {
                        $metro_id = (int)$_GET["AddressSearch"]['metro_id'];
                        $query->andWhere(['metro_id' => $metro_id]);
                    }
                }

            }

            if (isset($_GET["AddressSearch"]['nalog_id'])) {
                if (is_array($_GET["AddressSearch"]['nalog_id'])) { // если пришло с главной страницы
                    foreach ($_GET["AddressSearch"]['nalog_id'] as $nalog_id) {
                        $nalog_id = (int)$nalog_id;
                        $query->orWhere(['nalog_id' => $nalog_id]);
                    }
                } else { // со страницы списка адресов
                    if ((int)$_GET["AddressSearch"]['nalog_id'] > 0) {
                        $nalog_id = (int)$_GET["AddressSearch"]['nalog_id'];
                        $query->andWhere(['nalog_id' => $nalog_id]);
                    }
                }
            }

        }

        $sort_arr = ["metro_id", "nalog_id", "okrug_id", "price6", "price11"];

        if (isset($_GET["sort_by"]) && in_array($_GET["sort_by"], $sort_arr)) {

            $sort_by = $_GET["sort_by"];
        } else {
            $sort_by = 'id';
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => $perPage, 'page' => $currentPage - 1],
            'sort' => [
                'defaultOrder' => [
                    $sort_by => SORT_DESC,
                ]
            ],
        ]);
        $totalCount = $dataProvider->getTotalCount();

        $arr = array(
            "metro" => $metro,
            "searchModel" => $addressSearchModel,
            "taxes" => $taxes,
            "districts" => $districts,
            "addresses" => $dataProvider->getModels(),
            "totalCount" => $totalCount,
            "totalPages" => ceil($totalCount / $perPage),
            "currentPage" => $currentPage
        );
        //var_dump($arr['addresses']);
        return $this->render('prices',
            $arr

        );
    }

    public function actionAddresses($id = null)
    {
        $addressSearchModel = new AddressSearch();
        $metro = new Metro();// список станций метро для формы поиска
        $taxes = new Nalog(); // список налоговых для формы поиска
        $districts = new Okrug(); // список округов для формы поиска
        $services = new ServiceName(); // список услуг
        $document_cnt = Document::find()->where(['doc_type_id' => 3])->count();
        //var_dump($document_cnt); die();
        //echo "<pre>", print_r($taxes); die();
        if (isset($_GET['AddressSearch'])) {

        }


        // Yii::$app->params['page'] = 'main';
        $arr = [];
        $arr = array(
            "metro" => $metro,
            "searchModel" => $addressSearchModel,
            "taxes" => $taxes,
            "districts" => $districts,
            "services" => $services,
            "document_cnt" => $document_cnt);

        if ((int)$id) {
            return $this->render('address-detail', $arr);
        } else {
            return $this->render('addresses', $arr);
        }

    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionWidgets()
    {
        $addressSearchModel = new AddressSearch();
        $metro = new Metro();// список станций метро для формы поиска
        $taxes = new Nalog(); // список налоговых для формы поиска
        $districts = new Okrug(); // список округов для формы поиска
        $services = new ServiceName(); // список услуг
        $document_cnt = Document::find()->where(['doc_type_id' => 3])->count();
        //var_dump($document_cnt); die();
        //echo "<pre>", print_r($taxes); die();
        if (isset($_GET['AddressSearch'])) {

        }


        Yii::$app->params['page'] = 'main';
        $arr = [];
        $arr = array(
            "metro" => $metro,
            "searchModel" => $addressSearchModel,
            "taxes" => $taxes,
            "districts" => $districts,
            "services" => $services,
            "document_cnt" => $document_cnt);

        return $this->render('widgets', $arr);
    }


    public function actionArticle()
    {
        return $this->render('article');
    }

    public function actionPrivacy()
    {
        return $this->render('static');
    }

    public function actionInfo()
    {
        return $this->render('info');
    }

    public function actionRule()
    {
        return $this->render('rule');
    }

    public function actionAdd_address()
    {
        return $this->render('add_address');
    }

    public function actionAdd_addressstep2()
    {
        return $this->render('add_address_step_two');
    }

    public function actionAdd_addressstep3()
    {
        return $this->render('add_address_step_three');
    }

    public function actionAddress_card()
    {
        return $this->render('address_card');
    }

    public function actionStatistics()
    {
        return $this->render('statistics');
    }

    public function actionServices()
    {
        return $this->render('services');
    }

    public function actionCab()
    {

        $user = User::findOne(Yii::$app->user->identity->id);

        return $this->render('cab', array("user" => $user));
    }

    public function actionCabCompany()
    {

        return $this->render('cab-company');
    }

    public function actionCounts()
    {
        return $this->render('counts');
    }

    public function actionDelivery()
    {
        /*доставка документов*/
        return $this->render('delivery');
    }

	
    public function actionOrders_card()
    {
        return $this->render('orders-card');
    }

    /* public function actionNotifications()
     {
         return $this->render('notifications');
     }*/
    public function actionArticles()
    {
        return $this->render('article-list');
    }

    public function actionRegister_success()
    {
        return $this->render('register-success');
    }


    /**
     * Signs user up.
     *
     * @return mixed
     */
    /*public function actionSignup()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }*/

    public function actionClientSignup()
    {

        $model = new SignupForm();

        $model->role = "Заказчик";
        /* if(Yii::$app->request->post()){
             $model->email = $model->username;
         }*/
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->email = $model->username;
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {


            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }

                //return $this->redirect(['site/register_success']);
            }
        }

        return $this->render('client-signup', [
            'model' => $model,
        ]);
    }

    public function actionOwnerSignup()
    {
        $app = Yii::$app;
        $request = $app->request;
        $post = $request->post();
        $model = new OwnerSignup();

        $model->role = User::OWNER_ROLE;
        $session = $app->session;

        if ($request->isAjax && $model->load($post)) {
            $model->email = $model->username;
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }


        if ($request->isPost) {
            if ($model->load($post)) {
                $model->email = $model->username;
                if ($user = $model->signup()) {
                    if ($app->getUser()->login($user)) {
                        $session->setFlash('success', 'Вы благополучно зарегистрировались в системе');
                        return $this->goHome();
                    }
                }
            }
            $session->setFlash('error', 'Что-то пошло не так');
        }

        return $this->render('owner-signup', [
            'model' => $model,
        ]);
    }

    public function actionApply_data()
    {
        $app = Yii::$app;
        $request = $app->request;
        $session = $app->session;

        if ($session->get('newRegisterOwner') == TRUE) {
            $session->setFlash('success', Yii::t('app', 'registeredSuccessfully'));
            unset($session['newRegister']);
        }


        $step = (int)@$_GET['step'];
        $user_id = Yii::$app->user->identity->id;
        $user = User::findOne($user_id);
        $deliveryAddress = new DeliveryAddress();
        $region = new Region();
        //var_dump(Yii::$app->user); die();
        if ($step == 2) { // создаём модель с реквизитами

            /*если выбрал физ. лицо*/
            $person = new Rperson();
            if (Yii::$app->request->isAjax && $person->load(Yii::$app->request->post())) { // Валидация модели "Физ. лицо ajax
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($person);
            }
            if ($person->load(Yii::$app->request->post()) && $deliveryAddress->load(Yii::$app->request->post()) && $region->load(Yii::$app->request->post())) {

                if ($deliveryAddress->validate()) {
                    ///die("valid");
                    $person->user_id = $user_id;
                    $person->save(true);
                    //echo "<pre>", print_r($user); die();
                    $user->default_profile_id = $person->id;
                    $user->address = $deliveryAddress->index . " " . $deliveryAddress->district . " " . $deliveryAddress->destination . " " . $deliveryAddress->address;
                    $user->info = $deliveryAddress->info;
                    $user->save();
                    return $this->redirect(['site/cab_owner']);
                } else {
                    $errors = $deliveryAddress->errors;
                    echo print_r($errors);
                    die();
                }


            }

            /* end физ.лицо */

            /*если выбрал Юр. лицо*/
            $company = new Rcompany();
            if (Yii::$app->request->isAjax && $company->load(Yii::$app->request->post())) { // Валидация модели "Юр. лицо ajax
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($company);
            }
            if ($company->load(Yii::$app->request->post()) && $deliveryAddress->load(Yii::$app->request->post()) && $region->load(Yii::$app->request->post())) {

                if ($deliveryAddress->validate()) {

                    $company->user_id = $user_id;
                    $company->save(true);
                    //echo "<pre>", print_r($user); die();
                    $user->default_profile_id = $company->id;
                    $user->address = $deliveryAddress->index . " " . $deliveryAddress->district . " " . $deliveryAddress->destination . " " . $deliveryAddress->address;
                    $user->info = $deliveryAddress->info;
                    $user->save();
                    return $this->redirect(['site/cab_owner']);
                }
            }
            /* end Юр.лицо */
            /*если выбрал Ип.*/
            $rip = new Rip();
            if (Yii::$app->request->isAjax && $rip->load(Yii::$app->request->post())) { // Валидация модели "Юр. лицо ajax
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($rip);
            }
            if ($rip->load(Yii::$app->request->post()) && $deliveryAddress->load(Yii::$app->request->post()) && $region->load(Yii::$app->request->post())) {
                $rip->user_id = $user_id;
                $rip->save(true);
                //echo "<pre>", print_r($user); die();
                $user->default_profile_id = $rip->id;
                $user->address = $deliveryAddress->index . " " . $deliveryAddress->district . " " . $deliveryAddress->destination . " " . $deliveryAddress->address;
                $user->info = $deliveryAddress->info;
                $user->save();
                return $this->redirect(['site/cab_owner']);
            }
            /*end Ип.*/

            return $this->render('formdata', [
                'person' => $person,
                'company' => $company,
                'rip' => $rip,
                'deliveryAddress' => $deliveryAddress,
                'region' => $region,
            ]);


        } else {

        }
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $app = Yii::$app;
        $request = $app->request;
        $post = $request->post();
        $session = $app->session;


        $model = new PasswordResetRequestForm();
        if ($request->isPost) {
            if ($model->load($post) && $model->validate()) {
                if ($model->sendEmail()) {
                    $session->setFlash('success', Yii::t('app','checkEmailForInstructions'));
                    return $this->goHome();
                } else {
                    $session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
                }
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }


    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionMetro()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new Metro();
        if ($model->load(Yii::$app->request->post()) && $model->metro()) {
            return $this->goBack();
        } else {
            return $this->render('metro', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('app','newPasswordSaved'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionNotifications()
    {
        $session = Yii::$app->session;
        $request = Yii::$app->request;
        if ($request->isPost) {
            try {
                NotificationsVsStatusesSettings::saveSettings($request->post());
            } catch (\Exception $e) {
                $session->setFlash('error', $e->getMessage());
            }
            return $this->redirect(['']);
        }

        $statusesSettings = [];
        try {
            $statusesSettings = NotificationsVsStatusesSettings::getSettingsByUserId();
        } catch (\Exception $e) {
            $session->setFlash('error', $e->getMessage());
        }

        return $this->render('notifications', [
            'user' => User::getById(),
            'statusesSettings' => $statusesSettings,
        ]);
    }
    /*

        public function actionLogin() {
            $serviceName = Yii::$app->getRequest()->getQueryParam('service');
            if (isset($serviceName)) {
                // @var $eauth \nodge\eauth\ServiceBase
                $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
                $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
                $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

                try {
                    if ($eauth->authenticate()) {
    //					var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes()); exit;

                        $identity = User::findByEAuth($eauth);
                        Yii::$app->getUser()->login($identity);

                        // special redirect with closing popup window
                        $eauth->redirect();
                    }
                    else {
                        // close popup window and redirect to cancelUrl
                        $eauth->cancel();
                    }
                }
                catch (\nodge\eauth\ErrorException $e) {
                    // save error to show it later
                    Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());

                    // close popup window and redirect to cancelUrl
    //				$eauth->cancel();
                    $eauth->redirect($eauth->getCancelUrl());
                }
            }

            // default authorization code through login/password ..
        }
    */
}
