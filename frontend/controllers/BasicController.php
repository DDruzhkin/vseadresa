<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Response;

use frontend\models\User;
use common\models\Err404;

class BasicController extends \common\controllers\EntityController{

	const EM_OWNER_ROLE_REQUIRED = 'Для совершения действия не достаточно прав. Требуются права Владельца. Зайдите под учетной записью Владельца или <a href="/owner/signup">Зарегистрируйтесь</a>';
	const EM_CUSTOMER_ROLE_REQUIRED = 'Для совершения действия не достаточно прав. Требуются права Заказчика. Зайдите под учетной записью Заказчика или <a href="/customer/signup">Зарегистрируйтесь</a>';
	const EM_ADMIN_ROLE_REQUIRED = 'Для совершения действия не достаточно прав. Требуются права Администратора. Зайдите под учетной записью Администратора или Зарегистрируйтесь';


	public $title;
	public $h1;
	public $model;
	
    public function beforeAction($action){
				
		// Проверка прохождения второго шага владельцем при регистрации
		
        if (!Yii::$app->user->isGuest) {                        
			$user = User::getCurrentUser();	
			
            if(!(int)($user->default_profile_id) && ($user->role == \frontend\models\User::ROLE_OWNER )){			
                if($this->id != 'owner/user' || $this->action->id != 'signup-complete'){ 	//
                    return $this->redirect(["owner/user/signup-complete", 'step'=>2]);
                }
            }
						
			if (( // Условие на запрет личного кабинета владельца для неактивных владельцев (без договора) кроме нескольких страниц
					$user->role == \frontend\models\User::ROLE_OWNER) 
					&& !$user -> active 
					&& (substr($this -> id, 0, 5) == 'owner') // запрет только в рамках личного кабинета владельца
					&& (!in_array($this -> id.'.'.$action -> id, [
						// куда можно ходить без договора
						'owner/user.index', 
						'owner/document.psettlements', 
						'owner/user.signup-complete'
					]))
				) 
			{
				Yii::$app->session->setFlash('error', "Данная операция доступна после заключения договора");
				return $this->redirect(["owner/user/"]);
				//throw new NotFoundHttpExceptionHttpException ("Данная операция доступна после заключения договора");
					
			}
        }
		
		
		
				
		
//		if ($this -> id == 'page' ) $this -> layout = 'content.php';// статические страницы 
//		$this -> layout = 'content.php';		
        return true;
    }


	// установка связанной с отображаемой страницей модели
	// устанавливаются мета-теги, соответствующие устанавливаемой модели
	public function setModel($model) {
		$this -> model = $model;				
		if ($this -> model) {			

			// Мета-теги
			if ($model -> hasAttribute('h1')) $this -> h1 = $model -> h1;
			//if (trim($this -> h1) == '') $this -> h1 = 'Пустой заголовок H1';
			
			if ($model -> hasAttribute('title')) $this -> title = $model -> title;
			if ($model -> hasAttribute('meta_keywords')) Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $model -> meta_keywords], 'keywords');
			if ($model -> hasAttribute('meta_description')) Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $model -> meta_description], 'description');
			
		}
		
		
	}
	
	// Вызвать, если пора перейти по урлу, на который патался перейти пользователь без прав
	public function redirect403() {

		if ($this -> get403()) {
			$url = $this -> get403();			
			$this -> set403(true);
			return $this->redirect($url);
		} else  {
			return $this->redirect(Url::to['/']);
		}
			
	}
	
	public function get403() {
		return Yii::$app->session -> get('lastUrl403');
	}
	
	public function set403($clear = false, $url = false) {		
		if ($clear) {
			Yii::$app->session -> remove('lastUrl403');			
		} else {			
			Yii::$app->session -> set('lastUrl403', $url?$url:Yii::$app->request->url);					
		}			
	}
	
	
	
	
	
	
	public function save404($exception) {
		// протоколирование ошибки
		$err = new Err404();
		
		$r = Yii::$app->request;
		
		$err -> url = urldecode($r -> url);
		$err -> ip = $r -> userIP;
		$err -> method = $r -> method;
		
		$err -> save();
	}
	
	
	public function actionError() {
		$exception = Yii::$app->errorHandler->exception;
		
		
        if ($exception !== null) {
            $statusCode = $exception->statusCode;
            $name = $exception->getName();
            $message = $exception->getMessage();
			$this -> layout = 'auth';
			switch ($statusCode) {
				case 403: 
					$this -> set403();	

					break;
				case 404:
					$this -> save404($exception);
					break;
				
			}
			
			
			
            return $this->render('error', [
                'exception' => $exception,
                'statusCode' => $statusCode,
                'name' => $name,
                'message' => $message
            ]);
        }
	}
}

?>