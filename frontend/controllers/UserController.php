<?php

namespace frontend\controllers;



use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use common\models\UserSearch;
use common\models\LoginForm;
use common\models\RequisiteManager;
use common\controllers\EntityController;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\ChangePassword;
use frontend\models\OwnerSignup;
use frontend\models\User;
use Exception;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


	

	
    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = User::findOne($id);
        if (!$model) throw new NotFoundHttpException(Yii::t('app', 'userNotFound'));
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
/*
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		$model->load(Yii::$app->request->post()); $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
*/
	

	public function actionLogout()
    {
        Yii::$app->user->logout();
		
        return $this->goHome();
    }	
	
	// Переход на профиль пользователя
	public function actionProfile()
	{
		$url = Url::to(['/']);

		$user = User::findOne(User::currentUser());

		if ($user) {
			if ($user -> checkRole(User::ROLE_ADMIN)) {
				$url = Url::to('/admin/');
			};
			if ($user -> checkRole(User::ROLE_OWNER)) {
				$url = Url::to('/owner/');
			};
			if ($user -> checkRole(User::ROLE_CUSTOMER)) {
				$url = Url::to('/customer/');
			};
		}

		return $this->redirect([$url]);
	}

	public function actionLogin()
    {		
        $this -> layout = 'auth';
        if (!Yii::$app->user->isGuest) {
			if (Yii::$app->request->post()) {
				$url = $this -> get403();
				Yii::$app->user->logout(); // Если пользователь залогинен, а данные пришли, то разлогиним его
				$this -> set403(false, $url);
	
			} else {
				return $this->redirect(['/profile']);
			}
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {

		
		
			if($model->login()) {


				if (Yii::$app->session -> get('ordered_address_id')) { // Была попытка закза адреса без авторизации - перенаправляем на процесс заказа
					$address_id = Yii::$app->session['ordered_address_id'];
					Yii::$app->session -> remove('ordered_address_id');
					return $this->redirect(["/address/{$address_id}/buy"]);
					//return $this->redirect(['/order/create', 'address_id' => $address_id]);
				};
			
				if ($this -> get403()) {
						return $this->redirect403();
				}
				if (Yii::$app->request->get('back')) return $this -> redirect(Yii::$app->request->get('back'));
				//pred(Yii::$app->user->identity->role);
				if(Yii::$app->user->identity->role == \common\models\User::attrLists()['role']['Заказчик']){
                    return $this->redirect(['/customer']);
                }
                elseif (Yii::$app->user->identity->role == \common\models\User::attrLists()['role']['Владелец']){
                    return $this->redirect(['/owner']);
                }
                else{
                    return $this->goBack();
                }

			} else {
				Yii::$app->session->setFlash('error', Yii::t('app', 'wrongPasswordOrEmail'));
			}
        }


        $this -> layout = 'auth';

        return $this->render('auth', [
             'model' => $model,
        ]);

    }

	 public function actionChangePassword()
    {
        $app = Yii::$app;
        $request = $app->request;
        $post = $request->post();
        $session = $app->session;

        $model = new ChangePassword();
        if ($request->isPost) {
            if ($model->load($post)) {
                if ($model->changePassword()) {
                    $session->setFlash('success', Yii::t('app', 'passwordChangedSuccess'));
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

	// Установить профиль (реквизиты по умолчанию)
	public function actionDefaultProfile($id, $rid){
		$model = $this->findModel($id);		
		if($model -> active)  { // Активный (подписавший договор) пользователь не может изменить реквизиты по умолчанию
			Yii::$app->session->setFlash('error', Yii::t('app','noChangeDefaultRequisites'));
		} else {
			$model -> setDefaultProfile($rid);
		};
		return $this->redirect(Url::to(['/profile']));
	
	}


    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $app = Yii::$app;
        $request = $app->request;
        $post = $request->post();
        $session = $app->session;
		$this -> layout = 'auth';
		

        $model = new PasswordResetRequestForm();
        if ($request->isPost) {
            if ($model->load($post) && $model->validate()) {
                if ($model->sendEmail()) {
                    $session->setFlash('success', Yii::t('app','checkEmailForInstructions'));
                    return $this->goHome();
                } else {
                    $session->setFlash('error', Yii::t('app','notUserWithEmail'));
                }
            }
        }
		
		
        return $this->render('request-password-reset-token', [
            'model' => $model,
        ]);
		
		
		
    }

	public function actionResetPassword($token)
    {


	
		//$this -> layout = 'message';
		//$this -> h1 = 'Восстановление пароля';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

		
		
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('app','newPasswordSaved'));

            return $this->goHome();
        }

        return $this->render('reset-password', [
            'model' => $model,
        ]);
    }

	
	public function actionTest() {
		echo __method__;
	}
	
	

}