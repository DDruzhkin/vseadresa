<?php

namespace frontend\controllers;

use backend\models\EmailTemplate;
use common\models\NotificationManagerExt;
use frontend\controllers\BasicController;
use frontend\models\Notification;
use Yii;
use frontend\models\Review;
use common\models\ReviewSearch;
use common\controllers\EntityController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\EntityHelper;


/**
 * ReviewController implements the CRUD actions for Review model.
 */
class ReviewController extends BasicController
{

    public function actionCreate()
    {
        $model = new Review();
        $app = Yii::$app;
        $request = $app->request;
        $session = $app->session;
        $post = $request->post(Review::baseClassName());
        if ($post) {
            if ($model->load($request->post())) {
                if ($model->save()) {
                    $session->setFlash('success', Yii::t('app','thanksForFeedback'));
                } else {
                    $session->setFlash('error', Yii::t('app','somethingWrong'));
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer);

    }


    protected function findModel($id)
    {
        if (($model = Review::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app','pageWasNotFound'));
        }
    }
}
