<?php

namespace frontend\controllers\admin;

use Yii;
use frontend\models\User;
use frontend\models\UserPortalOptions;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserPortalOptionsController extends BasicController
{


    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
		
		
        $username = User::portal() -> username;		
		
		if (!$username) throw new NotFoundHttpException('Не указан параметр username');
		$user = User::findOne(['username' => $username]);
		$model = new UserPortalOptions();
		if (!$model) throw new NotFoundHttpException('Не найден пользователь '.$username);
		$model -> user = &$user;		
        if ($model->load(Yii::$app->request->post())) {           				
			if ($model->save()) {                       	

			}
		};
		
           
		return $this->render('update', [
			'model' => $model,
			'user' => $user,

		]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


}
