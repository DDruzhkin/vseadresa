<?php
/* генерируемый файл, редактировать его не надо */


namespace backend\controllers;

use Yii;
use backend\models\Rip;
use common\models\RipSearch;
use backend\controllers\BasicController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * RipController implements the CRUD actions for Rip model.
 */
class RipController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rip models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RipSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rip model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Rip model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Rip();
		
		if (Yii::$app->request->get() && isset(Yii::$app->request->get()['data'])) {
				$data = Yii::$app->request->get()['data'];
				$model->load($data, '');
				$model->integrate();
		};


        if ($model->load(Yii::$app->request->post()) && $model->save()) {		
			Yii::$app->session->setFlash('success', "Данные Реквизитов сохранены");
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Rip model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$tab = isset($_GET['tab'])?$_GET['tab']:false;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

			if ($model->save()) {                       
				Yii::$app->session->setFlash('success', "Данные Реквизитов сохранены");
				//return $this->redirect(['view', 'id' => $model->id]);
			}
		};
           
           
		return $this->render('update', [
			'model' => $model,
			'tab' => $tab               
		]);
    }

    /**
     * Deletes an existing Rip model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Rip model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rip the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rip::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}
