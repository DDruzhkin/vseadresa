<?php

namespace frontend\controllers\admin;

use Yii;
use frontend\models\User;
use frontend\models\UserPortalOptions;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class RequisitesController extends BasicController
{


    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
		
		$get = Yii::$app->request->get();
		$id = false;
		if (isset($get['id'])) {
			$id = $get['id'];			
		};
		if (!$id) {		
			$id = User::portal() -> id;			
		}		
		
		$user = User::findOne($id);
		
		if (!$user) { // Попробуем найти по логину
			$user = User::findOne(['username' => $id]);
		}
				
		if (!$user) throw new NotFoundHttpException('Не найден пользователь '.$id);

/*		
		$model = new UserPortalOptions();		
		$model -> user = &$user;		
        if ($model->load(Yii::$app->request->post())) {           				
			if ($model->save()) {                       	

			}
		};
	*/	
           
		return $this->render('index', [
			'model' => $user,
//			'user' => $user,

		]);
		
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


}
