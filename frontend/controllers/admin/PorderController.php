<?php
namespace frontend\controllers\admin;

use Yii;
use frontend\models\Porder;
use common\models\PorderSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PorderController implements the CRUD actions for Porder model.
 */
class PorderController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Porder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PorderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$get = Yii::$app->request->get();
		$param = false;
		$param_value = null;
		if ($get) {
			if (isset($get['user_id'])) {
					$param = 'customer_id';
					$param_value = $get['user_id'];
			}
		};
		
		if ($param) {
			$dataProvider -> query -> andWhere([$param => $param_value]);				
		} else $param_value = null;
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'param' => $param,
			'param_value' => $param_value,
        ]);
    }

    /**
     * Displays a single Porder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Porder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Porder();
		
		if (Yii::$app->request->get() && isset(Yii::$app->request->get()['data'])) {
				$data = Yii::$app->request->get()['data'];
				$model->load($data, '');
				$model->integrate();
		};


        if ($model->load(Yii::$app->request->post()) && $model->save()) {		
			Yii::$app->session->setFlash('success', "Данные Заказа сохранены");
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Porder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$tab = isset($_GET['tab'])?$_GET['tab']:false;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {           
			if ($model->save()) {                       
				Yii::$app->session->setFlash('success', "Данные Заказа сохранены");
				//return $this->redirect(['view', 'id' => $model->id]);
			}
		};
           
           
		return $this->render('update', [
			'model' => $model,
			'tab' => $tab               
		]);
    }

    /**
     * Deletes an existing Porder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Porder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Porder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Porder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}
