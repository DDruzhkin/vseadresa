<?php
namespace frontend\controllers\admin;

use Yii;
use frontend\models\Faq;
use common\models\FaqSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FaqController implements the CRUD actions for Faq model.
 */
class FaqController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Faq models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FaqSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Faq model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Faq model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Faq();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		$model->load(Yii::$app->request->post()); $model->save();
			Yii::$app->session->setFlash('success', "Данные  сохранены");
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Faq model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$tab = isset($_GET['tab'])?$_GET['tab']:false;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {           
			if ($model->save()) {                       
				Yii::$app->session->setFlash('success', "Данные  сохранены");
				//return $this->redirect(['view', 'id' => $model->id]);
			}
		};
           
           
		return $this->render('update', [
			'model' => $model,
			'tab' => $tab               
		]);
    }

	
	public function actionToDisabled($id) {
		
		$model = $this->findModel($id);
		$model -> enabled = 0;
		$model -> save();
		return $this->redirect(['admin/faq/']);			
	}
	
	
	public function actionToEnabled($id) {
		
		$model = $this->findModel($id);
		$model -> enabled = 1;
		$model -> save();
		return $this->redirect(['admin/faq/']);			
	}
	
	
    /**
     * Deletes an existing Faq model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Faq model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Faq the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Faq::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}
