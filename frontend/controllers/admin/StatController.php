<?php

namespace frontend\controllers\admin;

use Yii;
use yii\web\NotFoundHttpException;

use frontend\models\OrderStatAdmin;
use frontend\models\PorderStatAdmin;



class StatController extends BasicController {
	
	
	
	public function actionIndex()
    {
       
    }
	
	/*
	*	количество и сумма заказов в каждом из статусов
	*/
	public function actionOrders()
    {
		$get = Yii::$app->request->get();
		$stat = new OrderStatAdmin();
		$stat -> period = isset($get['period'])?$get['period']:OrderStatAdmin::PERIOD_MONTH;
		$stat -> address_id = isset($get['address_id'])?$get['address_id']:'';
		$stat -> user_id = isset($get['user_id'])?$get['user_id']:'';
		
		$provider = $stat -> search();
        return $this->render('orders', [
			'dataProvider'=> $provider,
			'period' => $stat -> period,
			'user_id' => $stat -> user_id,
			'address_id' => $stat -> address_id,
        ]); 
    }
	
	
	/*
	*	количество и сумма заказов услуг портала (для каждой из услуг портала)
	*/
	public function actionPortalOrders()
    {
		$get = Yii::$app->request->get();
		$stat = new PorderStatAdmin();
		$stat -> period = isset($get['period'])?$get['period']:PorderStatAdmin::PERIOD_MONTH;
		$stat -> address_id = isset($get['address_id'])?$get['address_id']:'';
		$stat -> user_id = isset($get['user_id'])?$get['user_id']:'';
		
		$provider = $stat -> search();
        return $this->render('portal-orders', [
			'dataProvider'=> $provider,
			'period' => $stat -> period,
			'user_id' => $stat -> user_id,
			'address_id' => $stat -> address_id,
        ]); 
		/*
		$stat -> period = isset($get['period'])?$get['period']:OrderStatAdmin::PERIOD_MONTH;
		$provider = $stat -> search();
        return $this->render('portal-orders', [
			'dataProvider'=> $provider,
        ]); 
		*/
    }
	
	
}