<?php
namespace frontend\controllers\admin;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Response;
use common\models\Entity;
use frontend\models\User;

class BasicController extends \frontend\controllers\BasicController{


	
    public function beforeAction($action){

		$this -> layout = 'admin.php';	
		$user = User::getCurrentUser();	
		if (!$user || !$user->checkRole(\frontend\models\User::ROLE_ADMIN) )
			throw new ForbiddenHttpException(self::EM_ADMIN_ROLE_REQUIRED);
        return true;
    }

	// установка связанной с отображаемой страницей модели
	// устанавливаются мета-теги, соответствующие устанавливаемой модели
	public function setModel($model) {
		$this -> model = $model;
		
	}
	
	// Вызвать, если пора перейти по урлу, на который патался перейти пользователь без прав
	public function redirect403() {
				
		if ($this -> get403()) {
			$url = $this -> get403();			
			$this -> set403(true);
			return $this->redirect($url);
		} else  {
			return $this->redirect(Url::to['/']);
		}
			
	}
	
	// Изменить статус в указанный по алиасу для модели $id
	// Производится проверка возможности изменения
	protected function statusTo($id, $alias, $redirect = false) {
		if (!$redirect) $redirect = Yii::$app->request->referrer;
		$url = parse_url($redirect);
		$redirect = $url['path'];
		if (isset($url['query'])) $redirect = $redirect."?".$url['query'];
		if (isset($url['fragment'])) $redirect = $redirect."#".$url['fragment'];
		
//		var_dump($redirect); exit;

		if (!$redirect) $redirect = '/';
		$model = $this->findModel($id);
		
		$statuses = $model -> status -> getTo() -> all();
		$statuses = array_flip(Entity::objectsByTemplate($statuses, '%alias%'));
		if (!array_key_exists($alias, $statuses)) throw new NotFoundHttpException('Невозможно перевести статус из '.$model -> status -> alias.' в '.$alias);
		
		$model -> status_id = $model -> statusByAlias($alias) -> id;
		$model -> save();
		return $this->redirect([$redirect]);			
	}
	

}

?>