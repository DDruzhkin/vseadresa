<?php

namespace frontend\controllers\admin;

use Yii;
use frontend\models\Payment;
use frontend\models\Import;
use frontend\models\ImportOut;
use frontend\models\PaymentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class ImportController extends PaymentController
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        // ...set `$this->enableCsrfValidation` here based on some conditions...
        // call parent method that will check CSRF if such property is true.

            # code...
            //$this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $data = [];
		$res = false;
		
        if(Yii::$app->request->isPost){			
            /** Request method is POST, check file*/			
            if(!isset($_FILES['excel'])) throw new NotFoundHttpException('Ошибка загрузки файла');			
            $response = Import::validateImportedFile($_FILES['excel']);  /** Validating file */			
			
            if($response['success']){
				$data = Import::getData($_FILES['excel']);
			}	                
        };
			return $this->render('index', 
				[
					'mode' => 'view',
					'data' => $data
				]
			);
        //return ($res)? $this->redirect(array('payment/index')):$this->render('index', array("data" => []));
    }
	
	public function actionLoad() {
		$out = false;
		if(Yii::$app->request->isPost){			
				$data = Yii::$app->request->post('data');
				
				$out = isset($data['out']);
				if ($out){
					$data = ImportOut::loadData($data);
				} else {
					$data = Import::loadData($data);
				}
				
		} else {
			$data = [];
		}
		
		if ($out) {
			return $this->render('out', 
				[
					'mode' => 'loading',
					'data' => $data
				]
			);
		} else {
			return $this->render('index', 
				[
					'mode' => 'loading',
					'data' => $data
				]
			);
		}
	}
	
    public function actionOut()
    {
	    $data = [];
		$res = false;
		
        if(Yii::$app->request->isPost){			
            /** Request method is POST, check file*/			
            if(!isset($_FILES['excel'])) throw new NotFoundHttpException('Ошибка загрузки файла');			
            $response = ImportOut::validateImportedFile($_FILES['excel']);  /** Validating file */			
			
            if($response['success']){
				$data = ImportOut::getData($_FILES['excel']);
			}	                
        };
			return $this->render('out', 
				[
					'mode' => 'view',
					'data' => $data
				]
			);	
		
    }



    /**
     * Creates a new Payment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       // echo "<pre>", print_r($_POST); die();
        if(isset($_POST['data'])){
            Import::importAndModify();
        }

        $this->redirect(array('payment/index'));

    }

    public function actionCreateOutcome()
    {

        if(isset($_POST['data'])){
            Import::importAndModifyOutcome();
        }

        $this->redirect(array('payment/index'));

    }


}
