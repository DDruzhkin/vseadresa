<?php
namespace frontend\controllers\admin;

use Yii;
use frontend\models\Address;
use common\models\AddressSearch;
use common\models\Entity;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use frontend\models\Nalog;


/**
 * AddressController implements the CRUD actions for Address model.
 */
class AddressController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
	
	
	// Скрыть/показать в каталоге (флаг Demo)
	public function actionDemo($id, $value = false)  {
		$redirect = Yii::$app->request->referrer;
		$url = parse_url($redirect);
		$redirect = $url['path'];
		if (isset($url['query'])) $redirect = $redirect."?".$url['query'];
		if (isset($url['fragment'])) $redirect = $redirect."#".$url['fragment'];
		
		if (!$redirect) $redirect = '/';
		
		
		$model =  $this->findModel($id);
		$model -> demo = $value;
		Yii::$app->session->setFlash($value?'success':'error', ($value?"Разрешено":"Запрещено")." отображение адреса ".$model -> address." в каталоге");
		$model -> save();
		
		return $this->redirect([$redirect]);	
	}
	
	
	
	// Установить приоритет адреса 
	public function actionPriority($id, $priority = 9999) {
		$model =  $this->findModel($id);
		$model -> priority = $priority;
		$model -> save();
		Address::recalcPriorities(); // пересчет приоритетов
		//return $this->redirect(['admin/address/']);
		return $this -> toBack();
	}
	 
	 
	 
	public function actionRestore($id)  {
		$model = $this->findModel($id);	
		
		if ($model -> unDelete()) {			
			Yii::$app->session->setFlash('success', "Адрес восстановлен");
			return $this->redirect(['/admin/address/view', 'id' => $id]);	
		}
	}
	
	
	protected function statusTo($id, $alias, $redirect = false) {
		
		 $result = parent::statusTo($id, $alias);
		 if ($result) 
			Yii::$app->session->setFlash('success', "Статус Адреса изменен");
		 
		 return $result;
	}	
	
	
	public function actionPreparation($id) {		
		$model = $this->findModel($id);					
		return $this -> statusTo($id, 'preparation');
	}
	
	public function actionReady($id) {
		$model = $this->findModel($id);					
		return $this -> statusTo($id, 'ready');
	}
	
	public function actionFiles()
	{
		$get = Yii::$app->request->get();
		$id = $get['id'];
		
		$model = $this->findModel($id);	
			
		$files = $model -> files_as_array;		
		if (Yii::$app->request->isPost){			
			$post = Yii::$app->request->post();
			$data =  $post[Address::baseClassName()];
			
			$numbers = isset($data['numbers'])?$data['numbers']:[];			
			$dates = isset($data['dates'])?$data['dates']:[];			
			$names = isset($data['names'])?$data['names']:[];
			$removes = isset($data['removes'])?$data['removes']:[];			
			$filenames = isset($data['filenames'])?$data['filenames']:[];
			
			$files = [];
			foreach($numbers as $no => $value) {
				if (isset($removes[$no])) {
					//  файл удаляем физически
					
					FilesController::removeFile('documents', 'uploaded', $filenames[$no]);
					
					
				} else {
					$files[] = [
						'number' => ArrayHelper::getValue($numbers, $no), 
						'date' => ArrayHelper::getValue($dates, $no), 
						'name' => ArrayHelper::getValue($names, $no), 
						'filename' => ArrayHelper::getValue($filenames, $no)
					];
				}
			}
            $newFiles = FilesController::uploadFiles('Address[files]');
			if (is_array($newFiles) && count($newFiles)) {
				$loaded = 0;			
				foreach($newFiles as $name => $data) {
					if ($data['error'] > 0) {
						Yii::$app->session->setFlash('error', "Ошибка загрузки файла: ".$name);
					} else {
	//					Yii::$app->session->setFlash('success', "Файл загружен: ".$name);
						$loaded++;
						$files[] = ['date' => Entity::dbNow(), 'name' => $name, 'filename' => $data['filename']];
					}				
				}
						
				Yii::$app->session->setFlash('success', "Загружено файлов: ".$loaded);
			} else Yii::$app->session->setFlash('success', "Изменения сохранены");
			
			$model -> files_as_array = $files;			
			$model -> save();
		}
		
		
		return $this->render('files', [
			'files' => $files,
            'model' => $model,
			
        ]);
	}		
	


public function actionDeleted()
    {
		
        $searchModel = new AddressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider -> query -> andWhere(['enabled' => 0]);
		
		$get = Yii::$app->request->get();
		$param = false;
		$param_value = null;
		if ($get) {
			if (isset($get['owner_id'])) {
					$param = 'owner_id';
					$param_value = $get[$param];
			};
						
		};
		
		if ($param) {
			
			$dataProvider -> query -> andWhere([$param => $param_value]);				
		} else $param_value = null;

		
		
        return $this->render('deleted', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'param' => $param,
			'param_value' => $param_value,
        ]);
    }



	
	
	

    /**
     * Lists all Address models.
     * @return mixed
     */
    public function actionIndex()
    {
		
        $searchModel = new AddressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider -> query -> andWhere(['enabled' => 1]);
		
		$get = Yii::$app->request->get();
		$param = false;
		$param_value = null;
		if ($get) {
			if (isset($get['owner_id'])) {
					$param = 'owner_id';
					$param_value = $get[$param];
			};
						
		};
		
		if ($param) {
			
			$dataProvider -> query -> andWhere([$param => $param_value]);				
		} else $param_value = null;

		
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'param' => $param,
			'param_value' => $param_value,
        ]);
    }

    /**
     * Displays a single Address model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Address model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
/*	 
    public function actionCreate()
    {
        $model = new Address();
		
		if (Yii::$app->request->get() && isset(Yii::$app->request->get()['data'])) {
				$data = Yii::$app->request->get()['data'];
				$model->load($data, '');
				$model->integrate();
		};


        if ($model->load(Yii::$app->request->post()) && $model->save()) {		
			Yii::$app->session->setFlash('success', "Данные Адреса сохранены");
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
*/
    /**
     * Updates an existing Address model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {		
		$tab = isset($_GET['tab'])?$_GET['tab']:false;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->unlinkAll('taxAgencies',true);
            if (Yii::$app->request->post($model->formName())['tax_agencies']) {
                foreach (Yii::$app->request->post($model->formName())['tax_agencies'] as $tax_agency) {
                    $model->link('taxAgencies', Nalog::findOne($tax_agency));
                }
            }
			if ($model->save()) {           
				Address::recalcPriorities(); // пересчет приоритетов
				Yii::$app->session->setFlash('success', "Данные Адреса сохранены");
				//return $this->redirect(['view', 'id' => $model->id]);
			}
		};
           
           
		return $this->render('update', [
			'model' => $model,
			'tab' => $tab               
		]);
    }

    /**
     * Deletes an existing Address model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Address model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Address the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Address::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}
