<?php
namespace frontend\controllers\admin;

use Yii;
use common\models\EntityExt;
use common\models\PaymentSearch;
use frontend\models\Payment;
use frontend\models\User;
use frontend\models\Document;
use frontend\models\DocumentType;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class PaymentController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payment models.
     * @return mixed
     */
    public function actionIndex()
    {
		
        $searchModel = new PaymentSearch();
		
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider -> query -> andWhere(['confirmed' => 1]);
		$get = Yii::$app->request->get();
		$out =  false;
		$inp = false;
		$eq = false;
		if (isset($get['dir'])) {
			$out =  $get['dir'] == 'out';
			$inp = $get['dir'] == 'inp';
			$eq = $get['dir'] == 'eq';
		}
		
		$param = false;
		$param_value = null;
		$list = null;
		$porder_id = null;
		$order_id = null;
		$user_id = null;
		if ($get) {
			if (isset($get['payer_id'])) {
					$param = 'payer_id';
					$param_value = $get[$param];
			};
			if (isset($get['recipient_id'])) {
					$param = 'recipient_id';
					$param_value = $get[$param];
			};
			
			
			if ($param_value) {
				$user = User::findone([$param_value]);
				if ($user) {
					
					$list = $user -> requisiteList();
				} else {
					$param = false;
					$param_value = null;
				}
			}
			
			if (isset($get['porder_id'])) { // в таблице нет этого поля, поэтому обрабатываем отдельно
					$param = 'porder_id';
					$param_value = $get[$param];
					$porder_id = $param_value;
					
			} else
			
			if (isset($get['order_id'])) { // в таблице нет этого поля, поэтому обрабатываем отдельно
					$param = 'order_id';
					$param_value = $get[$param];
					$order_id = $param_value;
					
			} else
			
			if (isset($get['user_id'])) { // в таблице нет этого поля, поэтому обрабатываем отдельно
					$param = 'user_id';
					$param_value = $get[$param];
					$user_id = $param_value;
					
			};
			
			
			
		};
		
		
		$docTypeId = DocumentType::byAlias('invoice') -> id;
		$portalRList = array_keys(EntityExt::portal() -> requisiteList());
		
		if ($out) { // Исходящие платежи
			
			$dataProvider -> query -> andWhere(['payer_id' => $portalRList]);			
		} ;
		if ($inp) { // Входящие платежи
			
			$dataProvider -> query -> andWhere(['recipient_id' => $portalRList]);			
		} ;
		
		if ($eq) { // Платежи по эквайрингу от заказчиков владельцам
			
			$dataProvider -> query -> andWhere(['not',['recipient_id' => $portalRList]]);			
			$dataProvider -> query -> andWhere(['not',['payer_id' => $portalRList]]);						
			$dataProvider -> query -> andWhere(['ptype' => Payment::PTYPE_CARD]);						
		} ;
			
			
			
			if ($porder_id) {		
				$dataProvider -> query -> where('invoice_id in (select id from '.Document::tableName().' where doc_type_id = '.$docTypeId.' AND porder_id = '.$porder_id.')');
			} else
			if ($order_id) {		
				$dataProvider -> query -> where('invoice_id in (select id from '.Document::tableName().' where doc_type_id = '.$docTypeId.' AND order_id = '.$order_id.')');			
			} else
			if ($user_id) {		
				$user = User::findOne([$user_id]);
				if ($user) {
					$rList = array_keys($user -> requisiteList());
					$dataProvider -> query -> andWhere(['or', ['recipient_id' => $rList], ['payer_id' => $rList]]);			
				}
			} else
			if ($param) {
				
				$dataProvider -> query -> andWhere([$param => is_null($list)?$param_value:array_keys($list)]);				
			} else $param_value = null;
		
//		echo 'sql: '.$dataProvider -> query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
		
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'param' => $param,
			'param_value' => $param_value,
			'out' => $out,
			'inp' => $inp,
			'eq' => $eq,
        ]);
    }

    /**
     * Displays a single Payment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Payment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Payment();
		
		if (Yii::$app->request->get() && isset(Yii::$app->request->get()['data'])) {
				$data = Yii::$app->request->get()['data'];
				$model->load($data, '');
				$model->integrate();
		};


        if ($model->load(Yii::$app->request->post()) && $model->save()) {		
			Yii::$app->session->setFlash('success', "Данные Платежа сохранены");
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Payment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$tab = isset($_GET['tab'])?$_GET['tab']:false;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {           
			if ($model->save()) {                       
				Yii::$app->session->setFlash('success', "Данные Платежа сохранены");
				//return $this->redirect(['view', 'id' => $model->id]);
			}
		};
           
           
		return $this->render('update', [
			'model' => $model,
			'tab' => $tab               
		]);
    }

    /**
     * Deletes an existing Payment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Payment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}
