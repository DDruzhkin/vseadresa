<?php

namespace frontend\controllers\admin;

use common\models\UserSearch;

use frontend\models\Address;
use frontend\models\ChangePassword;
use frontend\models\User;
use frontend\models\SignupFormAdmin;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionList()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }	
	
	
	
	protected function statusTo($id, $alias, $redirect = false) {
		
		 $result = parent::statusTo($id, $alias, '/admin/user/list');
		 if ($result) 
			Yii::$app->session->setFlash('success', "Статус пользователя изменен");
		 
		 return $result;
	}
	
	
	public function actionToActive($id) {
		return $this -> statusTo($id, 'active');
	}
	
	public function actionToBlocked($id) {
		return $this -> statusTo($id, 'blocked');
	}
		
	
		
	// Установить приоритет пользователя
	public function actionPriority($id, $priority = 9999) {
		$model =  $this->findModel($id);
		$model -> priority = $priority;
		$model -> save();
		Address::recalcPriorities(); // пересчет приоритетов
		//return $this->redirect(['admin/address/']);
		return $this -> toBack();
	}
	 
	
		
	
    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex()
    {		
		$id = User::currentUser();		
		
        $model = User::findOne($id);		
        $passwordModel = new ChangePassword();

        if ($model->load(Yii::$app->request->post())) {           
			if ($model->save()) {                       
				//return $this->redirect(['view', 'id' => $model->id]);
			}
		};
           
		return $this->render('update', [
			'model' => $model,			
			'passwordModel' => $passwordModel,
		]);
    }

	
	public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }	
	
	
    public function actionEdit($id)
    {				
        $model = User::findOne($id);		
		if (!$model) throw new NotFoundHttpException('Неверно указан id: '.$id);
        
        if ($model->load(Yii::$app->request->post())) {           
			if ($model->save()) {                       
				//return $this->redirect(['view', 'id' => $model->id]);
			}
		};
           
		return $this->render('edit', [
			'model' => $model,						
		]);
    }	
	
	
	public function actionUpdate($id)
    {
		$tab = isset($_GET['tab'])?$_GET['tab']:false;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {           
			if ($model->save()) {  
				Address::recalcPriorities(); // пересчет приоритетов			
				Yii::$app->session->setFlash('success', "Данные Пользователя сохранены");
				//return $this->redirect(['view', 'id' => $model->id]);
			}
		};
           
           
		return $this->render('update', [
			'model' => $model,
			'tab' => $tab               
		]);
    }
	

	public function actionSignup()
    {

        $model = new SignupFormAdmin();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->email = $model->username;
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                return $this->redirect(['/admin']);
                
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
	
	
		// Установить профиль (реквизиты по умолчанию)
	public function actionDefaultProfile($id, $rid){
		$model = $this->findModel($id);		
		$model -> setDefaultProfile($rid);
		return $this->redirect(Url::to(['/admin/requisites']));
	
	}

	
}
