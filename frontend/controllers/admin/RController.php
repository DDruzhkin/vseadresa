<?php

namespace frontend\controllers\admin;

use Yii;

use common\models\Entity;
use common\models\EntityExt;
use common\models\RequisiteManager;
use frontend\models\User;
use frontend\models\Rip;
use frontend\models\Rcompany;
use frontend\models\Rperson;
use yii\web\UploadedFile;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;


/**
 * RController общий предок для работы с реквизитами разного типа (ИП, физлица, юрлица)
 */
class RController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function create($model)
    {
        $model->user_id = EntityExt::portal()->id;
        $type = $model->modelId();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->load(Yii::$app->request->post());
            RequisiteManager::save($model);
            return $this->redirect(['/admin/requisites']);
        } else {
            return $this->render('@frontend/views/admin/' . $type . '/create', [
                'model' => $model,
            ]);
        }
    }


    public function actionCreate($type)
    {
        $className = Inflector::camelize($type);
        $model = (new \ReflectionClass('\common\models\\' . $className))->newInstance();
        $model->integrate();

        return $this->create($model);
    }


    /**
     * Updates an existing Rperson model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $type = $model->modelId();

        //Yii::$app->session->setFlash('error', 'Внимание! Изменение реквизитов потребует подписание дополнительного соглашения.');
        $current_sign = $model->sign;
        $current_stamp = $model->stamp;
        if ($model->load(Yii::$app->request->post())) {
            $sign = UploadedFile::getInstance($model, 'sign');
            if (!empty($sign) && $sign->size !== 0) {
                $sign->saveAs(Yii::getAlias(Yii::$app->params['storage']['path']) . '/images/requisites/sign_' . hash("md5", $id) . '.' . $sign->extension);
                $model->sign = 'sign_' . hash("md5", $id) . '.' . $sign->extension;
            } else {
                $model->sign = $current_sign;
            }
            $stamp = UploadedFile::getInstance($model, 'stamp');
            if (!empty($stamp) && $stamp->size !== 0) {
                $stamp->saveAs(Yii::getAlias(Yii::$app->params['storage']['path']) . '/images/requisites/stamp_' . hash("md5", $id) . '.' . $stamp->extension);
                $model->stamp = 'stamp_' . hash("md5", $id) . '.' . $stamp->extension;
            } else {
                $model->stamp = $current_stamp;
            }
            if ($model->save()) {
//				$model -> user -> ownerAgreement();
//				Yii::$app->session->setFlash('success', 'Внимание! Реквизиты были изменены, Вам необходимо подписать дополнительное соглашение на их изменение.');
                return $this->redirect(Yii::$app->request->post('redirect')?:['/admin/requisites']);
            }
        };


        return $this->render('@frontend/views/admin/' . $type . '/update', [
            'model' => $model,
        ]);
    }


    /**
     * Deletes an existing Rperson model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
