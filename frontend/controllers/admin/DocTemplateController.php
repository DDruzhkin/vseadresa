<?php

namespace frontend\controllers\admin;

use common\models\DocTemplate;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;


class DocTemplateController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	// Редактирование шаблона
	public function actionIndex($name) {
		$model = DocTemplate::open($name);
		$templateList = $model -> list;
		if (!array_key_exists($name, $templateList)) {
				throw new NotFoundHttpException('Не найден шаблон '.$name);
		}
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
		};
	
		return $this->render('index', [            
			'model' => $model,            
        ]);
	}
	
	

	
}