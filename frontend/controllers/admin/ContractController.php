<?php
/* генерируемый файл, редактировать его не надо */


namespace frontend\controllers\admin;

use Yii;
use frontend\models\Document;
use common\models\ContractSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

/**
 * DocumentController implements the CRUD actions for Document model.
 */
class ContractController extends BasicController
{
    /**
     * @inheritdoc
     */
/*	 
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
*/
    /**
     * Lists all Document models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContractSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		$get = Yii::$app->request->get();
		$param = false;
		$param_value = null;
		if ($get) {
			if (isset($get['owner_id'])) {
					$param = 'owner_id';
					$param_value = $get[$param];
			};
			if (isset($get['recipient_id'])) {
					$param = 'recipient_id';
					$param_value = $get[$param];
			};
						
		};
		
		if ($param) {
			
			$dataProvider -> query -> andWhere([$param => $param_value]);				
		} else $param_value = null;

				
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'param' => $param,
			'param_value' => $param_value,
        ]);
    }
	
	
	// Изменить состояние договора (проверки на возможность не проводится)
	protected function stateTo($id, $value, $redirect = false) {
		if (!$redirect) $redirect = Yii::$app->request->referrer;
		$url = parse_url($redirect);
		$redirect = $url['path'];
		if (isset($url['query'])) $redirect = $redirect."?".$url['query'];
		if (isset($url['fragment'])) $redirect = $redirect."#".$url['fragment'];

		if (!$redirect) $redirect = '/';
		$model = $this->findModel($id);

		$model -> state = $value;
		$model -> save();
		$docTitle = $model -> doc_type -> alias == 'agreement'?'Доп.соглашения':'Договора';
		Yii::$app->session->setFlash('success', "Состояние $docTitle ".$model -> number." изменено");
		return $this->redirect([$redirect]);			
	}

	public function actionTosigning($id) {
		$this -> stateTo($id, Document::CONTRACT_STATE_SIGNING);
	}

	public function actionActivate($id) {
		$this -> stateTo($id, Document::CONTRACT_STATE_ACTIVE);
	}
	
	public function actionDeactivate($id) {
		$this -> stateTo($id, Document::CONTRACT_STATE_DEACTIVE);
	}

    /**
     * Displays a single Document model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Document model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Document the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Document::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}
