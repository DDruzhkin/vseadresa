<?php

use common\models\Review;
use yii\helpers\Html;
$model = new Review();

/* @var $this yii\web\View */
/* @var $model backend\models\Review */

$this->title = 'Добавление отзыва';
$this->params['breadcrumbs'][] = ['label' => Review::getMeta('','title'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-create">

    <h2><?= Html::encode($this->title) ?></h2>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
