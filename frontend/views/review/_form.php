<?php

if (YII_DEBUG) $startTime = microtime(true);
use frontend\models\Review;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$controls = [
    'name' => ['type' => Form::INPUT_TEXT, 'label' => Review::getMeta('name', 'label'), 'options' => ['required' => 'true']],
    'email' => ['type' => Form::INPUT_TEXT, 'label' => Review::getMeta('email', 'label'), 'options' => ['required' => 'true']],

    'company' => ['type' => Form::INPUT_TEXT, 'label' => Review::getMeta('company', 'label')],
    'content' => ['type' => Form::INPUT_TEXTAREA, 'label' => Review::getMeta('content', 'label'), 'options' => ['required' => 'true']],
    'actions' => ['type' => Form::INPUT_RAW, 'value' => '<div style="text-align: right; margin-top: 20px">' . Html::submitButton('Отправить', ['class' => 'btn btn-primary']) . '</div>'],
];


$layoutBase =
    [
        [
            'attributes' => [
                'name' => $controls['name'],
            ]
        ], [
        'attributes' => [
            'email' => $controls['email'],
        ],
    ], [
        'attributes' => [
            'company' => $controls['company'],
        ],
    ], [
        'attributes' => [
            'content' => $controls['content'],
        ],
    ], [
        'attributes' => [
            'actions' => $controls['actions'],
        ],
    ]
    ];

?>


<div class="review-form">

    <?php


    echo "<hr/>";


    $form = ActiveForm::begin([
        'action' => Url::to(['review/create'])
    ]);
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'rows' => $layoutBase,
    ]);

    ActiveForm::end();

    ?>

</div>
