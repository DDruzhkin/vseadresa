<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Rperson */

$this->title = 'Реквизиты физлица';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('@frontend/views/rperson/_form', [
    'model' => $model,
]) ?>
