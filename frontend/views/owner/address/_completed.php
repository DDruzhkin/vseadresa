<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<h2>Адрес добавлен</h2>
<p>
<?=Html::a('Перейти к списку адресов', Url::to(['owner/address']), ['class'=>"move-to-address"])?>
<!--
<?php if ($model -> getInvoiceCount):?>
<div>с Адресов связаны неоплаченные счета:</div>
<?=Html::a('Перейти к счетам', Url::to(['owner/address/document', 'id' => $model -> id]), ['class'=>"move-to-address"])?>
<?php endif?>
-->
<p>
<?=Html::a('Перейти к счетам по адресу', Url::to(['owner/address/document', 'id' => $model -> id]), ['class'=>"move-to-address"])?>

</p>
<?php

//var_dump($model -> attributes);

?>