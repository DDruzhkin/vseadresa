<?php

if (YII_DEBUG) $startTime = microtime(true);

use common\models\EntityExt;
use frontend\models\Address;
use frontend\models\Option;
use frontend\models\ServiceName;
use frontend\models\ServicePrice;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\AddressTaxAgency;
use yii\web\View;

?>

<?php
$tax_id_array=AddressTaxAgency::getAddressAgencyId($model->id);
$check_all=empty($tax_id_array);
$script = <<< JS
    /*if($check_all){
        $('#address-tax_agencies .checkbox input[type=checkbox]').attr('checked', true);
        $('#address-tax_agencies .checkbox .form-checkbox').addClass('active-checkbox');
    }*/
    $('.select_all').click(function () {
        console.log('select all');
        $('#address-tax_agencies .checkbox input[type=checkbox]').attr('checked', true);
        $('#address-tax_agencies .checkbox .form-checkbox').addClass('active-checkbox');
    })
    $('.unselect_all').click(function () {
        console.log('unselect all');
        $('#address-tax_agencies .checkbox input[type=checkbox]').attr('checked', false);
        $('#address-tax_agencies .checkbox .form-checkbox').removeClass('active-checkbox');
    })
JS;
$this->registerJs($script, View::POS_READY);
?>
<?php

include "controls.php";
$sections = [
    'basic' => 'Основные параметры',
    'services' => 'Справочник услуг',
    'photo' => 'Отображение адреса',
    'documents' => 'Документы адреса',
    'publication' => 'Параметры для публикации',
    'photographer' => 'Заказ фотографа',
];
/*
if ($tab == 'photographer') { ?>
    <h1><?= Yii::t('app','chooseTimeForPhotographerVisit')?></h1>
  <?php  echo $this->render('_photo_calendar', [
        'model' => $model,        
        'update' => true
    ]);
}else 

*/
{


    $layouts =
        [
            'basic' =>
                [
                    ['attributes' => ['adr_status' => controls('status_view', $model)]],
                    ['attributes' => ['adr_index' => controls('adr_index')]],
                    ['attributes' => ['address' => controls('address', $model)]],
                    ['attributes' => ['nalog_id' => controls('nalog_id', $model)]],
                    ['attributes' => ['okrug_id' => controls('okrug_id', $model)]],
                    ['attributes' => ['metro_id' => controls('metro_id')]],


                    ['attributes' => ['submit' => controls('submit', $model)]]
                ],
            'photo' =>
                [

                    [
                        'attributes' => [
                            'gps' => controls('gps'),
                        ]

                    ],
                    [
                        'attributes' => [
                            'submit' => controls('submit', $model),
                        ]

                    ]
                ],
            'photographer' =>
                [

                    [
                        'attributes' => [
                            'photographerInterval' => controls('photographerInterval'),
                            'photographerDate' => controls('photographerDate'),
                        ]

                    ],


                    [
                        'attributes' => [
                            'submit' => controls('submit', $model),
                        ]

                    ]
                ],

            'documents' =>
                [

                    [
                        'attributes' => [
                            'submit' => controls('submit', $model),
                        ]

                    ]
                ],
            'publication' =>
                [
                    [
                        'attributes' => [
                            'square' => controls('square'),
                        ],
                    ],

                    [
                        'attributes' => [
                            'action' => [
                                'type' => Form::INPUT_RAW,
                                'value' => '<h3>Стоимость первичной регистрации</h3>',
                            ]
                        ]
                    ],
                    [
                        'attributes' => [
                            'price_first_6' => controls('price_first_6'),
                        ],
                    ],
                    [
                        'attributes' => [
                            'price_first_11' => controls('price_first_11'),
                        ],
                    ],

                    [
                        'attributes' => [
                            'action' => [
                                'type' => Form::INPUT_RAW,
                                'value' => '<h3>Стоимость регистрации изменений</h3>',
                            ]
                        ]
                    ],
                    [
                        'attributes' => [
                            'price_change_6' => controls('price_change_6'),
                        ],
                    ],
                    [
                        'attributes' => [
                            'price_change_11' => controls('price_change_11'),
                        ],
                    ],

                    [
                        'attributes' => [
                            'action' => [
                                'type' => Form::INPUT_RAW,
                                'value' => '<h3>Стоимость пролонгации</h3>',
                            ]
                        ]
                    ],
                    [
                        'attributes' => [
                            'price_prolongation_6' => controls('price_prolongation_6'),
                        ],
                    ],
                    [
                        'attributes' => [
                            'price_prolongation_11' => controls('price_prolongation_11'),
                        ],
                    ],
                    ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<h2>Ограничение регистрации по ИФНС</h2>'],]],
                    ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<span class="span-hint">Если есть ограничения, отметьте ИФНС, для которых регистрация будет недоступна.</span>'],]],
                    ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<span class="select_all">Выбрать все</span> / <span class="unselect_all">Отменить выбор</span>'],]],
                    ['attributes' => ['tax_agencies' => controls('tax_agencies', $model)]],
                    ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<h2>Ограничение по месту регистрации исполнительного лица</h2>'],]],
                    ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<span class="span-hint">Если есть ограничения, отметьте субъекты, для которых регистрация будет недоступна.</span>'],]],
                    [
                        'attributes' => [
                            'reg_msk' => controls('reg_msk'),
                        ],
                    ],
                    [
                        'attributes' => [
                            'reg_mo' => controls('reg_mo'),
                        ],
                    ],
                    [
                        'attributes' => [
                            'reg_other' => controls('reg_other'),
                        ],
                    ],
                    [
                        'attributes' => [
                            'action' => [
                                'type' => Form::INPUT_RAW,
                                'value' => '<h3>Опции</h3>',
                            ]
                        ]
                    ],

                    [

                        'attributes' => [
                            'options' => controls('options_as_array'),
                        ],
                    ],


                    [
                        'attributes' => [
                            'submit' => controls('submit', $model),
                        ]

                    ]


                ],

        ];
    if (!$model->hasOption('new')) {
        array_unshift($layouts['basic'], [

            'attributes' => [
                'demo' => controls('demo'),
            ]
        ]);
    }

    ?>
    <div class="header-with-menu">
        <h1><?= $sections[$tab] ?></h1>
        <?php

        /*
    $sections = include "_form_sections.php";

    $menu = [];
    foreach($sections as $alias => $title) [
        $menu[] = [
            'label' => $title,
            'url' => Url::toRoute(['update', 'id' => $model -> id, 'tab' => $alias]),
        ]
    ];
    */

        echo NavX::widget([
            'options' => ['class' => 'nav nav-context menu-custom-icon'],
            'encodeLabels' => false,
            'items' => [
                [
                    'label' => Icon::show('bars'),
//                'items' => $menu,
                    'items' => $model->getOwnerMenuItems(),
                ],
            ],
        ]);


        ?>

    </div>
    <div class="mobile-cabinet-button"></div>

    <?php
    if (!$tab) {
        $tab = array_keys($layouts)[0];
    }

    if (in_array($tab, ['services'])) {
        echo $this->render('_' . $tab, [
            'model' => $model,
            'tab' => $tab,
        ]);
    } else {

        if ($tab == 'photographer') {
            echo \common\widgets\photocalendar\PhotoCalendar::widget([
                'model' => $model,
                'mode' => 'owner',
                'inputs' => ['interval' => 'address-photographerinterval', 'date' => 'address-photographerdate']
            ]);
        }

        if ($tab == 'photo') {


            echo \frontend\components\ImagesWidget::widget([
                'images' => $model->photos_as_array
            ]);

            ?>

            <div class="clear"></div>


            <div id="map-block">
                <?php

                $gps = $model->gps;
                $gps = explode(',', $gps);
                if (count($gps) < 2) {
                    $gps = ['55.751429', '37.618879'];
                }
                echo sergmoro1\yamap\widgets\Yamap::widget([
                    'points' => [
                        [

                            'lat' => $gps[0],
                            'lng' => $gps[1],
                            'icon' => '',
                            'header' => '',
                            'body' => $model->getCaption(),
                            'footer' => '',
                            'inputId' => 'address-gps'
                        ],
                    ],
                    'params' => ['visible' => true, 'zoom' => 15]

                ]);

                ?>

            </div>
            <div style="clear:both"></div>
            <?php
        };


        $form = ActiveForm::begin(['id' => 'form-address', 'options' => ['enctype' => 'multipart/form-data']]);
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'rows' => $layouts[$tab],
        ]);

        ActiveForm::end();
    }
}

if (YII_DEBUG) {
    $finTime = microtime(true);
    $delta = $finTime - $startTime;
    echo $delta . ' сек.';
}
?>
