
<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;

use frontend\models\Address;
use frontend\components\FilterWindow;
?>
<style>

.form-group label {
	width: 100%;	
}

.form-group select{
	width: 80%;
	float: left;
	margin: 0;
}


#AddressFilterForm .input-daterange {
	float: left;
	margin: 0;
	width: 90%;
}

#AddressFilterForm .input-daterange .form-control{
		width: 100%;
}

#AddressFilterForm .form-control {
	float: left;
	
	margin: 0;
}

#AddressFilterForm input.form-control {
	width: 90%;
}

#AddressFilterForm #ordersearch-address_id {
	width: 90%;
}

#AddressFilterForm .hint-block{
	display: block;
	float: left;
	width: 90%;
}

</style>
<?php

$layout = <<< HTML
    
    {input1}    	
    {separator}    
    {input2}
    <span class="input-group-addon kv-date-remove">
        <i class="glyphicon glyphicon-remove"></i>
    </span>
HTML;

$controls = [
	
	'address' => ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('address', 'label'), 'hint'=>'поиск по части адреса', 'options'=>[]],
	
	'nalog_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('nalog_id', 'label'), 'items' => Address::getExistsValues('nalog_id', false, true), 'hint'=>'', 'options'=>[]],
	'metro_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('metro_id', 'label'), 'items' => Address::getExistsValues('metro_id', false, true), 'hint'=>'', 'options'=>[]],						
	'okrug_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('okrug_id', 'label'), 'items' => Address::getExistsValues('okrug_id', false, true), 'hint'=>'', 'options'=>[]],	
	'status_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('status_id', 'label'), 'hint'=>'', 'items' => Address::getExistsValues('status_id', false, true)],			
	
	
	'actions'=>['type'=>Form::INPUT_RAW, 'value'=>  
		Html::submitButton('Применить фильтр', ['class'=>'reg-button', 'style'=>'float: right; margin-right: 20px;']).
		Html::button('Очистить фильтр', ['class'=>'reg-button reset', 'style'=>'float: right; margin-right: 20px;'])
		
	],            
];


$layout = [
[	
	'autoGenerateColumns' => false,
	'columns' => 3,
	'attributes' => [		
		'nalog_id' => $controls['nalog_id'],				
		'metro_id' => $controls['metro_id'],		
		'okrug_id' => $controls['okrug_id'],						
	],
],

[	
	'autoGenerateColumns' => false,
	'columns' => 2,
	'attributes' => [		
		'status_id' => $controls['status_id'],				
		'address' => $controls['address'],				
	],
],


[
	'columns' => 1,
	'attributes' => [				
		'actions' => FilterWindow::getActions(),								
		
		
	]
],



];




	echo FilterWindow::widget(
			[
				'options' => [
					'id' => 'AddressFilterForm', 					
					'method' => 'get',
				],
				'model' => $searchModel,
				'layout' => $layout,
					
			]);




/*

$form = ActiveForm::begin([
	'id' => 'AddressFilterForm', 
	'method' => 'get',
//	'action' => Url::to(['/address/prices']),
]);
echo FormGrid::widget([
    'model' => $searchModel,
    'form' => $form,
	//'autoGenerateColumns' => false,
    'rows' => $layout
	]);
	
ActiveForm::end();

*/