<?php




/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\models\Address;
use frontend\models\Option;
use frontend\models\AddressSearch;
use common\models\Entity;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Адреса';

$this->params['breadcrumbs'] =
    [

        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/owner'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,
        ],

    ];
//$this->params['breadcrumbs'][] = $this->title;


$query = Address::find()-> where(Address::getToCatalogCondition());
$list = $query -> all();
$publishedList = [];
foreach($list as $item) {
	$publishedList[] = $item -> id;
}


?>

<div class="address-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \frontend\models\Address::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	
	<div class="address-top-bar">
    <p class="mobile-cabinet-button">
        <?= Html::a('Добавить Адрес', ['create'], ['class' => 'public-address-button cabinet-button']) ?>
    </p>
	<div class="flex">
        <?=\frontend\components\FilterWindow::getFilterButton($searchModel)?>
	</div>

    </div>
	<?php
		$dataProvider -> query -> andFilterWhere(['owner_id'=>Address::currentUser()]);
//		publishedList
	?>




    <?= GridView::widget([
        'dataProvider' => $dataProvider,		
		'summary' => false,
		'tableOptions' => [
			'class' => 'table price-table counts-table has-sort',
			
        ],      

		'rowOptions' => function ($model, $key, $index, $grid) use ($publishedList) {
			return ['class' => in_array($model -> id, $publishedList)?'address-published':'address-normal'];
		}		,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					
					/*
					$sections = include "_form_sections.php";					
                    $menu = [];										
					$canPhotographer = $address -> getCanPhotographer();
					foreach($sections as $alias => $title) {
						if (!$canPhotographer && ($alias == 'photographer')) continue; // Пропускаем секцию с фотографом, если фотографа заказать невозможно
						$menu[] = [
							'label' => $title,
							'url' => Url::toRoute(['update', 'id' => $address -> id, 'tab' => $alias]),
						];
					};
					
					*/
					
					$address = Address::findOne($data['id']);
					
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'),
								'items' => $address -> getOwnerMenuItems(),
							],
						],
					]);
										
				}					
			],


            ['attribute' => 'id',
				'label' => 'Код',
				'contentOptions' => ['class' => 'col-id '],
				'filterOptions' => ['class' => 'col-id'],
				'headerOptions' => ['class' => 'col-id '],				
				'footerOptions' => ['class' => 'col-id '],
				
				
				
			],
            
            ['attribute' => 'address',
				'label' => Address::getMeta('address', 'label'),
				'contentOptions' => ['class' => 'col-address '],
				'filterOptions' => ['class' => 'col-address'],
				'headerOptions' => ['class' => 'col-address '],				
				'footerOptions' => ['class' => 'col-address '],
                'format' => 'html',
                'value' => function($data){
			        return "<a class='move-to-address' href='".Url::to(['/address/'.$data['id']])."'>".$data['address']."</a>";
                }
				
				
				
			],
            ['attribute' => 'status_id',
				'label' => Address::getMeta('status_id', 'label'),
				'contentOptions' => ['class' => 'col-status_id '],
				'filterOptions' => ['class' => 'col-status_id'],
				'headerOptions' => ['class' => 'col-status_id '],				
				'footerOptions' => ['class' => 'col-status_id '],
				'filter' => Address::getExistsValues('status_id'),	
				'value' => function($data){ 
					return $data['status_id_view'];
				},
				
			],
				
[
                'class' => 'yii\grid\ActionColumn',

                'buttons' => [
                    'delete' => function ($url, $model) {

                        return 
                            Html::a(
                                Icon::show('times'),
                                Url::toRoute(['delete', 'id' => $model->id]),
                             
                                [
                                    'title' => 'Удалить адрес',
                                    'class' => 'delete',
                                    'data-confirm' => 'Вы уверены, что хотите удалить этот адрес',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                    'data-params' => ['id' => $model->id],

                                ]
                            );

					}
                ],
                'template' => '{delete}',


            ],
//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

	<?= $this->render('_filter', [
			'searchModel' => $searchModel,
	]) ?>

