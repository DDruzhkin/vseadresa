<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
exit;

/* @var $this yii\web\View */
/* @var $model backend\models\Address */

/*
$this->title = "Адрес " . $model->getCaption();
$this->params['breadcrumbs'][] = ['label' => 'Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$attrList = $model->getAttributes();
$attributes = [];
foreach ($attrList as $colName => $value) {
    if (!in_array($colName, [
        ''
    ])) continue;
    $show = (int)$model->getMeta($colName, 'show');
    if (!$show || in_array($colName, ['owner_id'])) continue;
    $type = $model->getMeta($colName, 'type');
    $attributes[] = $colName . "_view" . ($type == 'text' ? ':ntext' : '');
}

$this->registerJsFile('/js/owner/address/view.js', ['position' => View::POS_END]);

$gps = explode(',', $model->gps);
$isMap = false;
if (count($gps) == 2) {
    $isMap = true;
    $lat = floatval($gps[0]);
    $lng = floatval($gps[1]);
}*/
?>

<?php /*
<section class="cabinet-section">
    <div class="container-fluid">
        <div class="col-md-3 add-search-block">
            <?php
            try {
                echo CabWidget::widget();
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            } ?>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 pull-right all-addresses-block">
                    <div class="col-md-10">

                        <?php
                        try {
                            echo AddressMenuWidget::widget(['addressId' => $model->id]);
                        } catch (\Exception $e) {
                            var_dump($e->getMessage());
                        } ?>


                        <h1><?= Html::encode($this->title) ?></h1>

                        <div class="row">
                            <?php
                            $photos = json_decode($model->photos, true);
                            if (is_array($photos) && count($photos)) { ?>
                                <div class="col-md-11">
                                    <div class="sld" id="sld">
                                        <div class="image-to-change add-padding-right">
                                            <?php $current = current($photos);
                                            echo Html::img($current['name'], ['class' => 'img-responsive']); ?>
                                        </div>
                                        <div class="address-slider">
                                            <ul class="sl">
                                                <?php foreach ($photos as $photo) { ?>
                                                    <li class="lense-search">
                                                        <?= Html::img($photo['name'],
                                                            ['title' => $photo['title'], 'class' => 'gh']) ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            <?php }

                            if ($isMap) { ?>
                                <div class="col-md-11" id="map-block">
                                    <div class="add-padding-right">
                                        <div id="map" data-lat="<?= $lat ?>" data-lng="<?= $lng ?>"></div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
*/?>


<?php
$this->title = "Адрес ".$model->getCaption();
$this->params['breadcrumbs'][] = ['label' => 'Адреса', 'url' => ['index'], 'template' => '<span class="b-link">{link}</span><span class="separate-links">\</span>' . PHP_EOL,];
$this->params['breadcrumbs'][] = $this->title;

$attrList = $model -> getAttributes(); 
$attributes = [];
	foreach ($attrList as $colName => $value) {		
		$type = $model -> getMeta($colName, 'type');
		$show = (int)$model -> getMeta($colName, 'show');
		if (!$show) continue;
		$attributes[] = $colName."_view" . ($type == 'text'?':ntext':'');
	}
?>
<div class="address-view">

    <h1><?= Html::encode($this->title) ?></h1>
    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

</div>

