<?php
/**
 * @var int $addressId
 * @var \frontend\models\Address $model
 */

use app\components\CabWidget;
use frontend\components\AddressMenuWidget;
use yii\helpers\Html;

$this->title = "Параметры для публикации адреса " . $model->getCaption();
$this->params['breadcrumbs'][] = ['label' => 'Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>


<section class="cabinet-section">
    <div class="container-fluid">
        <div class="col-md-3 add-search-block">
            <?php
            try {
                echo CabWidget::widget();
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            } ?>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 pull-right all-addresses-block">
                    <div class="col-md-10">

                        <?php
                        try {
                            echo AddressMenuWidget::widget(['addressId' => $model->id]);
                        } catch (\Exception $e) {
                            var_dump($e->getMessage());
                        } ?>


                        <h1><?= Html::encode($this->title) ?></h1>

                        <?= $this->render('_form', [
                            'model' => $model,
                            'step' => 4,
                        ]) ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>