<?php
use yii\helpers\Html;
use kartik\icons\Icon;


$this->title = 'Публикация адреса '.$model -> address;
$this->params['breadcrumbs'] =
    [
        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/owner'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
		[
            'label'     => 'Адреса',
            'url'       =>  ['/owner/address'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,
        ],

    ];
?>
<h1><?=$this -> title?></h1>
<div>
<?php
	if ($model-> status -> alias == 'published') {
		echo "Адрес опубликован";
		if (!IS_NULL($model-> subscribe_to) ) echo ' до '.$model-> subscribe_to_view;
		echo "!";
	} else {
		echo "Адрес НЕ опубликован!";
	}
?>

</div>
<div>
<?php
if (gettype($result) == 'object') { // есть неолаченный счет на подписку, надо об этом сообщить

		echo "есть неоплаченный счет на оплату подписки: ".$result -> getInvoice() -> getCaption();
};
		
?>
</div>

<div>
<?=Html::a(Icon::show('arrow-circle-left').' Назад', ['/owner/address'])?>
</div>	