<?php
/**
 * @var int $addressId
 * @var \frontend\models\Address $model
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use app\components\CabWidget;
use frontend\components\AddressMenuWidget;
use yii\helpers\Html;
use yii\grid\GridView;
use frontend\components\PagerWidget;
use frontend\models\DocumentType;

$this->title = "Документы адреса " . $model->getCaption();
$this->params['breadcrumbs'][] = ['label' => 'Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<section class="cabinet-section">
    <div class="container-fluid">
        <div class="col-md-3 add-search-block">
            <?php
            try {
                echo CabWidget::widget();
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            } ?>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 pull-right all-addresses-block">
                    <div class="col-md-10">

                        <?php
                        try {
                            echo AddressMenuWidget::widget(['addressId' => $model->id]);
                        } catch (\Exception $e) {
                            var_dump($e->getMessage());
                        } ?>


                        <h1><?= Html::encode($this->title) ?></h1>

                        <?php
                        $dataProvider->getPagination()->pageSize = 3;

                        try {
                            echo GridView::widget([
                                'dataProvider' => $dataProvider,
                                'tableOptions' => [
                                    'class' => 'table price-table counts-table',
                                ],
                                'showFooter' => true,
                                'layout' => "{items}\n{pager}",
                                'pager' => [
                                    'class' => PagerWidget::class,
                                ],
                                'columns' => [
                                    'number',
                                    'created_at',
                                    [
                                        'label' => 'Тип документа',
                                        'value' => function($data) {
                                            $docType = DocumentType::findOne($data->doc_type_id);
                                            return empty($docType) || empty($docType->name)
                                                ? '' : $docType->name;
                                        },
                                    ],
                                    [
                                        'label' => 'Ссылка',
                                        'value' => function($data) {
                                            return Html::a('pdf', '?act=pdf&id=' . $data->id,
                                                ['taget' => '_blank']);
                                        },
                                        'format' => 'raw',
                                    ]
                                ],
                            ]);
                        } catch (\Exception $e) {
                            var_dump($e->getMessage());
                        }

                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>