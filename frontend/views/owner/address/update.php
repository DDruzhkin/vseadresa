<?php

use yii\helpers\Html;

?>

<?php

/* @var $this yii\web\View */
/* @var $model backend\models\Address */

$this->title = 'Редактирование Адреса: ' . $model->getCaption();
//pred($model->id);
$this->params['breadcrumbs'] =
    [

        [
            'label' => 'Личный кабинет',
            'url' => ['/owner'],
            'template' => '<span class="b-link">{link}</span><span class="separate-links">\</span>' . PHP_EOL,
        ],
		[
            'label' => 'Адреса',
            'url' => ['/owner/address'],
            'template' => '<span class="b-link">{link}</span><span class="separate-links">\</span>' . PHP_EOL,
        ],
        [
            'label' => $model->getCaption(),
            'url' => ['/owner/address/update/'.$model->id.'/basic'],
            'template' => '<span class="b-link">{link}</span><span class="separate-links">\</span>' . PHP_EOL,
        ],
        [
            'label' => \Yii::t('app', "address-update-".$tab),
        ],

    ];

?>


<?php


if ($tab == 'documents') {
    echo "<div id='document-menu'>".\kartik\nav\NavX::widget([
        'options'=>['class'=>'nav nav-context menu-custom-icon'],
        'encodeLabels' => false,
        'items' => [
            [
                'label' => \kartik\icons\Icon::show('bars'),
//                'items' => $menu,
                'items' => $model -> getOwnerMenuItems(),
            ],
        ],
    ])."</div>";
	echo \frontend\components\AddressDocumentWidget::widget(['addressId' => $model->id]);
	
} else {
    echo $this->render('_form', [
        'model' => $model,
        'tab' => $tab,
    ]);
}


?>
