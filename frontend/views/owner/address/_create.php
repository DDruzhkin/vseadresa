<?php
if (YII_DEBUG) $startTime = microtime(true);

use frontend\models\Address;
use frontend\models\Option;
use frontend\models\ServiceName;
use frontend\models\ServicePrice;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>


<!------- Мастер --------->
<?php
// Получим информацию об услуге срочного оформления для заполнения формы
$service = null;
if (!$model->getIsNewRecord()) $service = $model->quickServicePrice();

if (!$service) {
    $sn = ServiceName::byAlias('quick_order');
    if (is_null($sn)) { // такая услуга должна существовать в справочнике всегда
        $sn = new ServiceName();
        $sn->load(['alias' => 'quick_order', 'undeleted' => 1, 'name' => 'Срочное формирование', 'stype' => ServiceName::STYPE_ONCE, 'code' => '01'], '');
        $sn->save();
    }

    $params['service_name_id'] = $sn->id;
    $params['owner_id'] = $model->currentUser();
    $service = ServicePrice::find()->andFilterWhere($params)->andWhere(['address_id' => NULL])->one();
}
$serviceData['description'] = $service ? $service->description : '';
$serviceData['price'] = $service ? $service->price : '';
//exit;
?>
<script>
    $(document).ready(function () {
        //$('#address-tax_agencies .checkbox input[type=checkbox]').attr('checked',true);
        //$('#address-tax_agencies .checkbox .form-checkbox').addClass('active-checkbox');
        $('.select_all').click(function () {
            console.log('select all');
            $('#address-tax_agencies .checkbox input[type=checkbox]').attr('checked',true);
            $('#address-tax_agencies .checkbox .form-checkbox').addClass('active-checkbox');
        })
        $('.unselect_all').click(function () {
            console.log('unselect all');
            $('#address-tax_agencies .checkbox input[type=checkbox]').attr('checked',false);
            $('#address-tax_agencies .checkbox .form-checkbox').removeClass('active-checkbox');
        })

        $("#address-service_description").val($("#address-service_period").val());
        $("#address-service_description").parent().parent().show();
        // Обработка нажатий кнопок мастера
        $('button.next').click(function () {
            console.log('next');
            $("#form-address").attr("action", "<?=Url::to(["/owner/address/create/" . (($model->getIsNewRecord()) ? "" : $model->id) . "/" . ($step + 1)])?>");
        });

        $('button.prior').click(function () {
            console.log('prior');
            $("#form-address").attr("action", "<?=Url::to(["/owner/address/create/" . (($model->getIsNewRecord()) ? "" : $model->id) . "/" . ($step - ($step > 1 ? 1 : 0))])?>");
        });

        // Заполнение услуги значениями поумолчанию
        <?php
        foreach ($serviceData as $name => $value) {
            echo '	if (!$("#address-service_' . $name . '").val()) $("#address-service_' . $name . '").val("' . $value . '");' . "\n";
        };

        ?>

        // Обновление контролов
        refreshContols();
        $("#address-quickable").change(function () {
            refreshContols();
        });

        $("#address-service_period").change(function () {
            $("#address-service_description").val($("#address-service_period").val());
        });

        function refreshContols() {
            //console.log('refreshContols: ' + $("#address-quickable").prop('checked'));
            if (!$("#address-quickable").prop('checked')) {
                $("#address-service_period").parent().parent().hide();
                $("#address-service_description").parent().parent().hide();
                $("#address-service_price").parent().parent().hide();
            } else {
                $("#address-service_period").parent().parent().show();
                //$("#address-service_description").parent().parent().show();
                $("#address-service_price").parent().parent().show();
            }

        }


    });
</script>
<!------- /Мастер --------->

<?php
/*
if ($step == 2) {
	
	echo $this->render('_photo_calendar', [
        'model' => $model,
		'step' => $step,
	]);

	
} else 
*/

{


    include "controls.php";


    $layouts =
        [
            1 =>
                [
                    ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<h2>Создание нового адреса</h2>'],]],
                    ['attributes' => ['adr_index' => controls('adr_index'),]],
                    ['attributes' => ['address' => controls('address', $model),],],
                    ['attributes' => ['nalog_id' => controls('nalog_id', $model),],],
                    [
                        'attributes' => [
                            'metro_id' => controls('metro_id'),
                        ],
                    ],
                    ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<h2>Ограничение регистрации по ИФНС</h2>'],]],
                    ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<span class="span-hint">Если есть ограничения, отметьте ИФНС, для которых регистрация будет недоступна.</span>'],]],
                    ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<span class="select_all">Выбрать все</span> / <span class="unselect_all">Отменить выбор</span>'],]],
                    ['attributes' => ['tax_agencies[]' => controls('tax_agencies', $model)]],
                    ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<h2>Ограничение по месту регистрации исполнительного лица</h2>'],]],
                    ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<span class="span-hint">Если есть ограничения, отметьте субъекты, для которых регистрация будет недоступна.</span>'],]],
                    [
                        'attributes' => [
                            'reg_msk' => controls('reg_msk'),
                        ],
                    ],
                    [
                        'attributes' => [
                            'reg_mo' => controls('reg_mo'),
                        ],
                    ],
                    [
                        'attributes' => [
                            'reg_other' => controls('reg_other'),
                        ],
                    ],
                    ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<h2>Дополнительно</h2>'],]],
                    [
                        'attributes' => [
                            'quickable' => controls('quickable'),
                        ],
                    ],
                     [
                    'attributes' => [
                        'service_period' => controls('service_period'),
                    ],
                ], [
                    'attributes' => [
                        'service_description' => controls('service_description'),
                    ],
                ], [
                    'attributes' => [
                        'service_price' => controls('service_price'),
                    ],
                ], [
                    'attributes' => [
                        'next' => controls('next', $model),
                    ],
                ],
                ],
            2 => [
                /*
                     [
                         'attributes' => [
                             'header' => ['type' => Form::INPUT_RAW, 'value' => '<h2>Заказ визита фотографа</h2>'],
                         ]

                     ],
        */
                [
                    'attributes' => [
                        'photographerInterval' => controls('photographerInterval'),
                        'photographerDate' => controls('photographerDate'),
                    ]

                ],


                [
                    'attributes' => [
                        'prior' => controls('prior', $model),
                        'next' => controls('next', $model),
                    ],
                ],
            ]
        ];

    ?>


    <?php


    if ($step == 2) {
        echo Html::tag('h1', 'Заказ визита фотографа');

        echo \common\widgets\photocalendar\PhotoCalendar::widget([
            'model' => $model,
            'mode' => 'owner',
            'inputs' => ['interval' => 'address-photographerinterval', 'date' => 'address-photographerdate']
        ]);
    }

    $form = ActiveForm::begin(['id' => 'form-address', 'options' => ['enctype' => 'multipart/form-data']]);
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'rows' => $layouts[$step]
    ]);

    ActiveForm::end();

}


if (YII_DEBUG) {
    $finTime = microtime(true);
    $delta = $finTime - $startTime;
    echo $delta . ' сек.';
}
?>

