<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use common\models\Entity;
use frontend\models\ServicePrice;
use frontend\models\User;

?>


<style>

tr.disabled td {
	color: silver;
}

tr.disabled td input[type=text]{
	display: none;
}
	
</style>

<script>
// добавление/удаление услуг
$(document).ready(function(){
	//$("label input[type=checkbox]").parent().on("click", function () {
	$(".form-checkbox").on("click", function () {
		active = !($(this).hasClass("active-checkbox"));
		row = $(this).parent().parent().parent().parent();
		if (active) {
			
			row.addClass('disabled')
		} else {
			row.removeClass('disabled');
		}

	});
});
</script>


<div class="raw">
<?php


//$list = $model -> serviceIdList;

//$model -> serviceIdList = $list;
//exit;

// формируем список услуг с иконками соответствующих опций
$ownerServiceList = $model -> getCurrentUser() -> servicePriceList -> all();
$ownerServices = [];
foreach($ownerServiceList as $service) {	
	$serviceName = $service -> service_name;
	$option = $serviceName? $serviceName -> option:null;
	$icon = $option?$option -> getCaption('<a title="Связана с опцией %name%">%icon%</a>'):'';;	
	$ownerServices[$service -> id] = '<span class="empty-icon">'.$icon.'</span>'.$service -> getCaption(' - %name%').(($serviceName? $serviceName -> undeleted:0) ? ' (неудаляемая)':'');	
}


/*

$controls = [
	'serviceIdList' => ['type'=>Form::INPUT_CHECKBOX_LIST, 'label'=>'', 'items' => $ownerServices, 'hint'=>'', 'options'=>['multiple' => true]],
	'submit'=>['type'=>Form::INPUT_RAW, 'value'=>  Html::submitButton('Изменить данные', ['class'=>'reg-button next'])]
];


$layout =
	[
		[		
			'attributes' => [
				'serviceIdList'=>$controls['serviceIdList'],		
			]
			
		],[		
			'attributes' => [
				'submit' => $controls['submit'],								
			],
		]
	];
	

$form = ActiveForm::begin(['id' => 'form-service-price']);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $layout
	]);	
ActiveForm::end();
*/

echo '<div class="cabinet-form" style="margin-top:20px;">';
echo '<div class="grid-view">';
echo '<form method="get">';
echo '<table class="table price-table">';
echo '<thead>';
echo '<tr><td></td><td>Услуга</td><td>Цена,<br/> руб</td>
		<td>Цена руб/мес при аренде<br/>адреса на 6 месяцев</td>
		<td>Цена руб/мес при аренде<br/>адреса на 11 месяцев</td><tr>';
echo '</thead>';
foreach($ownerServiceList as $ownerService) {
	
	$id = $ownerService -> id;
	$name = $ownerService -> name;
	$price = $ownerService -> price;
	$price6 = $ownerService -> price6;
	$price11 = $ownerService -> price11;
	$service = $ownerService -> getAddressServicePrice($model->id);
	if ($service) {
		$name = $service -> name;
		$price = $service -> price;
		$price6 = $service -> price6;
		$price11 = $service -> price11;
	}
	
	
	$active = in_array($id, $model -> serviceIdList);
	if ($ownerService -> stype == ServicePrice::STYPE_ONCE) {
		//$price = Yii::$app -> formatter -> asCurrency($service -> price);
		$price = '<input type="text" name="price['.$id.']" value="'.(int)($price).'">';
		$price6 = '<input type="text" name="price6['.$id.']" value="" disabled="1">';
		$price11 = '<input type="text" name="price11['.$id.']" value="" disabled="1">';		 
	} else {
		$price = '<input type="text" name="price['.$id.']" value="" disabled="1">';
		$price6 = '<input type="text" name="price6['.$id.']" value="'.(int)($price6).'">';
		$price11 = '<input type="text" name="price11['.$id.']" value="'.(int)($price11).'">';
		
	}
	
	
	echo '<tr data-id="'.$id.'" '.($active?'':'class="disabled"').'>';
	echo '<td>
		<div class="checkbox">
			<label>
				<input type="checkbox" '.($active?' checked="checked" ':'').'name="serviceList[]" value="'.$id.'"">
			</label>
		</div>
	</td>';
	echo '<td>'.$name.'</td>';
	echo '<td>'.$price.'</td>';
	echo '<td>'.$price6.'</td>';
	echo '<td>'.$price11.'</td>';
	
	
	echo '</tr>';

};

echo '</table>';
echo '<input type="submit" class="reg-button change-data" value="Сохранить">';
echo '</form>';

echo '</div>';
echo '</div>';

?>
</div>