<style>
.minus {
	color: red;
}
</style>
<?php
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\Document;
use frontend\models\Invoice;
use frontend\models\Order;

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;
use common\models\SettlementSearch;

$this->title = 'Расчеты по адресам';
$this->params['breadcrumbs'] =
    [

        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/owner'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,
        ],

    ];


?>






<div style="clear:both"></div>
<h1><?=$this->title?></h1>
<div class="row">
	<?=\frontend\components\FilterWindow::getFilterButton($searchModel)?>
</div>

<?php

//$searchModel -> documentTypes = [SettlementSearch::SHOW_PAYMENTS];
//$searchModel -> search();


echo GridView::widget([
    'dataProvider'=>$dataProvider,            
    'columns'=>[
//        ['class'=>'kartik\grid\SerialColumn'],

		[
            'attribute'=>'created_at',	
			'label' => 'Дата заказа',
			'value' => function($data) {							
				return Yii::$app->formatter->asDate($data['created_at']);	
			},
			
			'group' => true,
        ],
		
		/*
		[
            'attribute'=>'number',	
			'label' => 'Номер заказа',
			
			'group' => true,
        ],
		*/
		[
            'attribute'=>'order_id',			
			'label' => 'Заказ',			
			'format' => 'raw',
			'group' => true,
			'value' => function($data) {
				$order = \frontend\models\Order::findOne([$data['order_id']]);
				return Html::a($order -> number, Url::to(['/owner/order/view/'.$data['order_id']]));

			}
        ],
		

		[
            'attribute'=>'date',	
			'label' => 'Дата поступления',
			'format' => 'raw',
			'value' => function($data) {				
				if (!is_null($data['payment_order_date'])) {
					$date = $data['payment_order_date'];  // Если есть платежка, то показываем дату платежки (дата поступления)
				} else {
					$date = $data['pay_date']; 
				}
				return Yii::$app->formatter->asDate($date);									
			},
			
        ],
		
		[
            'attribute'=>'pay_value',	
			'label' => 'Сумма поступления',	
				'value' => function($data) {
					if ($data['pay_value']) {
						return Yii::$app -> formatter -> asCurrency($data['pay_value']);
					} else return '';
				}			
        ],
		
		[
            'attribute'=>'payment_order_number',	
			'label' => 'номер п/п',		
        ],
		
		
/*				
		[
            'attribute'=>'date',	
			'label' => 'Сумма поступления',
			'value' => function($data) {
				return Yii::$app->formatter->asCurrency($data['value']);	
			}
		],
		
		[
            'attribute'=>'payment_order_number',	
			'label' => 'Номер П/П',

		]
		
		*/
		

/*
		[
            'attribute'=>'id',	
			'label' => 'No',
			'format' => 'raw',
			'value' => function($data) {
				
				return (!$data['invoice_id'])?'<a href="'.Url::to(['/'.$this -> context -> cabinet.'/document/view/'.$data['id']]).'">'.$data['id'].'</a>':'';
			}
        ],
				
		
		[
            'attribute'=>'date',			
			'label' => 'дата заказа',
			'format' => 'datetime',
			
        ],
		
		[
            'attribute'=>'order_id',			
			'label' => 'Заказ',			
			'format' => 'raw',
			'group' => true,
			'value' => function($data) {
				$result = '-';
				if (isset($data['order_id']) && $data['order_id']) {
					$order = Order::findOne($data['order_id']);
					$result = $order -> number;
					$href= Url::to(['/'.$this -> context -> cabinet.'/order/view/'.$data['order_id']]);
					$result = Html::a($result, $href, array("class"=>"settlement-link"));
				}
				return $result;
			}
        ],
		
		
		[
            'attribute'=>'operation',	
			'label' => 'дата поступления',
			'format' => 'raw',
			'value' => function ($data) {
				if (isset($data['summa'])) {
					$value = Yii::$app->formatter->asCurrency($data['summa']);
					if (!$data['invoice_id']) {
						$value = Html::tag('span', '-'.$value, ['class' => 'minus']);
						
					}
					return $value;
				} else {
					return '';
				}
				
			}
			
        ],
	

		[
            'attribute'=>'operation',			
			'label' => 'вид операции',
        ],
		
		[
            'attribute'=>'summa',	
			'label' => 'сумма',
			'format' => 'raw',
			'value' => function ($data) {
				if (isset($data['summa'])) {
					$value = Yii::$app->formatter->asCurrency($data['summa']);
					if (!$data['invoice_id']) {
						$value = Html::tag('span', '-'.$value, ['class' => 'minus']);
						
					}
					return $value;
				} else {
					return '';
				}
				
			}
			
        ],
		
		[
            'attribute'=>'state',
			'label' => 'статус счета',		
			'value' => function($data) {
				if (is_null($data['state'])) {
					$value = '';
				} else {
					if ($data['state'] == Invoice::STATE_NOPAID) {
						$value = 'не оплачен ';
						
					} else {
						
						$value = 'оплачен';
					}
				};
				return $value;
			},
			
        ],
	
	
		[
            'attribute'=>'invoice_id',
			'label' => 'счет',						
			'value' => function($data) {
				$model = Document::findOne($data['invoice_id']);				
				return $model?$model -> getCaption('счет №%number% от %created_at% на сумму %summa% р.'):'';
				//return  'Счет №'.$data['invoice_number'].' на сумму '.(int)$data['invoice_summa'].'руб.';
			},
//			'group'=>true,
        ],
		
*/
		
    ],	
	
]);

/* */
?>
    </div>
</div>

<?=$this->render('_filter', [
        'searchModel' => $searchModel,
	
]) 
?>		
