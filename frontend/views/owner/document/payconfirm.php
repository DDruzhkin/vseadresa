<?php
use \kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\Document;

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;

$this->title = 'Подтверждение б/н оплаты';
$this->params['breadcrumbs'] =
    [

        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/owner'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,
        ],

    ];

/*
?>
<?=\frontend\components\FilterWindow::getFilterButton($searchModel)?>		
<?php

*/
?>	
<style>
.btn-toolbar textarea {
	display:none;
}

input.ingrid{
	width: 100%;
}
</style>	

<h1><?=$this -> title?></h1>

<div style="clear:both"></div>
<?php

//$form = ActiveForm::begin(['id' => 'payconfirm-form']);
//echo "<form id='payconfirm-form'>";

echo GridView::widget([
    'dataProvider'=>$dataProvider,        
	'layout'=>'<form id="payconfirm-form" method="post"> {items} {pager} '.Html::submitButton('Сохранить', ['class' => 'login-button']).'</form>',

    'columns'=>[
//        ['class'=>'kartik\grid\SerialColumn'],		
		
		[
            'attribute'=>'date',			
			'label' => 'дата и время',
			'format' => 'datetime',
			'group' => true,
        ],
		[
            'attribute'=>'order_id',			
			'label' => 'Заказ',			
			'format' => 'raw',
			'group' => true,
			'value' => function($data) {
				$order = \frontend\models\Order::findOne([$data['order_id']]);
				return Html::a($order -> number, Url::to(['/owner/order/view/'.$data['order_id']]));

			}
        ],
				
		[
            'attribute'=>'id',			
			'label' => 'номер счета',
			'format' => 'raw',
			'value' => function($data) {
				$invoice = \frontend\models\Document::findOne([$data['id']]);
//				return Html::a($invoice -> number, Url::to(['/owner/document/view/'.$data['id']]));
				return $invoice -> number;
			}
        ],
		
		[
            'attribute'=>'summa',	
			'label' => 'сумма по счету',
			'value' => function ($data) {
				if (isset($data['summa'])) {
					$value = Yii::$app->formatter->asCurrency($data['summa']);
					return $value;
				} else {
					return '';
				}
				
			}
        ],
			
		[            
			'label' => 'дата п/п',
			'format' => 'raw',
			'value' => function ($data) {				
				return '<input class="ingrid" name="po-date['.$data['id'].']"></input>';		
			}
        ],
		
		[         
			'label' => 'номер п/п',
			'format' => 'raw',
			'value' => function ($data) {
				return '<input class="ingrid" name="po-number['.$data['id'].']"></input>';		
			}
        ],		
		[         
			'label' => 'сумма п/п',
			'format' => 'raw',
			'value' => function ($data) {
				return '<input class="ingrid" name="po-summa['.$data['id'].']" value="'.(int)$data['summa'].'"></input>';		
			}
        ],	
	
		
    ],	
	
]);

//echo "</form>";

//	echo Html::submitButton('Сохранить', ['class' => 'login-button']);

//	ActiveForm::end();
 
 
 
 
/* 
?>
    </div>
</div>

<?=$this->render('_pfilter', [
        'searchModel' => $searchModel,
	
]) 
?>	
<?php 
*/
?>
	