
<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use frontend\models\Document;

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;

use frontend\components\FilterWindow;
?>
<style>
    #settlementFilterForm .input-daterange {
        float: left;
        margin: 0;
        width: 100%;
    }
/*
#settlementsearch-datefrom {
	width: 180px;
}
#settlementsearch-dateto {
	width: 180px;
}
#settlementsearch-summa{
	width: 120px;	
}





#settlementFilterForm .input-daterange {
	float: left;
	margin: 0;
	width: 90%;
}

#settlementFilterForm .input-daterange .form-control{
		width: 100%;
}

#settlementFilterForm .form-control {
	float: left;
	
	margin: 0;
}

#settlementFilterForm input.form-control {
	width: 90%;
}

#settlementFilterForm #ordersearch-address_id {
	width: 90%;
}

#settlementFilterForm .hint-block{
	display: block;
	float: left;
	width: 90%;
}
*/


</style>

<?php


$dateRangeLayout = <<< HTML
    
    {input1}    	
    {separator}    
    {input2}
    <span class="input-group-addon kv-date-remove">
        <i class="glyphicon glyphicon-remove"></i>
    </span>
HTML;

$controls = [
		'documentTypes' => ['type'=>Form::INPUT_CHECKBOX_LIST, 'label'=> 'показать', 'items' => [1 => 'счета', 2 => 'платежи'], 'hint'=>''],
		'dateFrom' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'kartik\widgets\DatePicker', 'label'=>'Период: ',
		'hint'=>'дата документа', 
		'options'=>[
			'layout' => $dateRangeLayout,
			'attribute' => 'dateFrom',
			'attribute2' => 'dateTo',
			'separator' => ' - ',
			'type' => kartik\widgets\DatePicker::TYPE_RANGE,
			'pluginOptions' => [
				'todayHighlight' => true, 
				'todayBtn' => true,				
				'autoclose'=>true,   			
				'endDate' => '0d',
				'format' => 'dd.mm.yyyy'
			]
		]
	],
						
						/*
						'dateRange' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\daterange\DateRangePicker', 'label'=>'Дата', 'hint'=>''],
						'dateFrom' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>'с', 'hint'=>'', 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
						'dateTo' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>'по', 'hint'=>'', 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
*/
						
		'summaCondition' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=> 'сумма', 'items' => [0 => '=', 1 => '<', 2 => '>'], 'options' => ['style'=>'width:60px; margin-left: 10px']],
		'summa' => ['type'=>Form::INPUT_WIDGET, 'label'=> false,  'widgetClass'=>'\yii\widgets\MaskedInput', 'options' => ['mask' => '9{1,12}',]],						
		'actions'=>['type'=>Form::INPUT_RAW, 'value'=>  Html::submitButton('Применить', ['class'=>'reg-button', 'style'=>'float: right;'])],            
];


$layout = [
[
//	'contentBefore'=>'Фильтр',
	'autoGenerateColumns' => false,
	'columns' => 1,
	'attributes' => [		
		'documentTypes' => $controls['documentTypes'],		
		
	],
],

[
	'columns' => 1,
	'attributes' => [		
		//'dateRange' => $controls['dateRange'],
		'dateFrom' => $controls['dateFrom'],		
		
	]
],

/*
[
	'columns' => 1,
	'attributes' => [		
		'dateTo' => $controls['dateTo'],
		
	]
],

*/

[
	'columns' => 2,
	'attributes' => [		
		'summaCondition' => $controls['summaCondition'],
		'summa' => $controls['summa'],
	]
],

[
	'columns' => 1,
	'attributes' => [	
		
		'actions' => $controls['actions'],
	]
]


];
	echo FilterWindow::widget(
			[
				'options' => [
					'id' => 'settlementFilterForm', 					
					'method' => 'get',
				],
				'model' => $searchModel,
				'layout' => $layout,
					
			]);