<?php
/*
 * @var $dataProvider
 * @var $searchModel


*/

use yii\grid\GridView;
use yii\helpers\Html;
use app\components\CabWidget;
use frontend\components\PagerWidget;
use yii\web\View;

$this->registerJsFile('/js/owner/services/index.js', ['position' => View::POS_END]);


$this->title = 'Услуги';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="cabinet-section">
    <div class="container-fluid">
        <div class="col-md-3 add-search-block">
            <?php
            try {
                echo CabWidget::widget();
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            } ?>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 pull-right all-addresses-block orders-search-form">
                    <div class="col-md-10">
                        <div class="cabinet-form">
                            <div class="col-md-9">
                                <div class="col-md-12">
                                    <h2><?= Html::encode($this->title) ?></h2>
                                </div>
                                <p>
                                    <?= Html::a('Добавить Услугу', ['/owner/services/create'],
                                        ['class' => 'public-address-button',
                                            'id' => 'add-service']) ?>
                                </p>

                            </div>
                        </div>

                        <?php
                        $dataProvider->getPagination()->pageSize = 3;
                        try {
                            echo GridView::widget([
                                'dataProvider' => $dataProvider,
                                'tableOptions' => [
                                    'class' => 'table price-table counts-table',
                                ],
                                'showFooter' => true,
                                'layout' => "{items}\n{pager}",
                                'pager' => [
                                    'class' => PagerWidget::class,
                                ],
                                'columns' => [
                                    'code',
                                    'name',
                                    'stype',
                                    [
                                        'label' => 'Системная / Пользовательская',
                                        'value' => function () {
                                            // TODO
                                            return 'Пользовательская';
                                        }
                                    ],
                                    [
                                        'class' => \yii\grid\CheckboxColumn::class,
                                        'header' => 'Добавлять в адрес по умолчанию',
                                    ],
                                    'price',
                                    [
                                        'label' => 'Редактировать',
                                        'value' => function ($data) {
                                            return Html::a(
                                                Html::button('Редактировать',
                                                    ['data-id' => $data['id'],
                                                        'class' => 'public-address-button']),
                                                ['/owner/services/update?id=' . $data->id],
                                                ['class' => 'edit-service iframe']
                                            );
                                        },
                                        'format' => 'raw',
                                    ],
                                    [
                                        'label' => 'Удалить',
                                        'value' => function ($data) {
                                            return Html::button('Удалить',
                                                ['data-id' => $data['id'],
                                                    'class' => 'del-service public-address-button']);
                                        },
                                        'format' => 'raw',
                                    ],
                                ],
                            ]);
                        } catch (\Exception $e) {
                            var_dump($e->getMessage());
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

