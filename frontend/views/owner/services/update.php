<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\Service;

/*
 * @var $service
 */


?>
<link href="/css/site.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="col-md-8 pull-right all-addresses-block orders-search-form">
            <div class="col-md-10">
                <div class="cabinet-form">
                    <?php
                    $form = ActiveForm::begin(['class' => 'flex']);
                    ?>
                    <div class="col-md-9">
                        <div class="col-md-9">
                            <div class="">
                                <div class="field-row flex al">
                                    <div class="form-input">
                                        <?= $form->field($service, 'name')
                                            ->textInput() ?>
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-input">
                                        <?= $form->field($service, 'stype')
                                            ->dropDownList(array_combine(Service::$stypes, Service::$stypes)) ?>
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-input">
                                        <?= $form->field($service, 'price')
                                            ->textInput() ?>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="col-md-2" id="submit-count-search-form">
                            <div class="publish-address">
                                <div class="public-address">
                                    <?= Html::submitButton('Сохранить',
                                        ['class' => 'public-address-button', 'id' => 'save-service']) ?>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    //document.getElementById('save-service').onclick = function (ev) { top.location.reload(); }

</script>
