<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\models\Address;
use frontend\models\Option;
use frontend\models\AddressSearch;
use common\models\Entity;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
//use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Список ставок аукционов';

$this->params['breadcrumbs'] =
    [

        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/owner'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,
        ],

    ];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flex auction-f" style="float: right; margin-bottom: 20px;">
    <?=\frontend\components\FilterWindow::getFilterButton($searchModel)?>
</div>
<div class="address-index">

	<div class="content">
	<?= \frontend\models\Address::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	
	

    
	<?php
		//$dataProvider -> query -> andFilterWhere(['owner_id'=>Address::currentUser()]);
		$dataProvider -> sort = false;
		$dataProvider -> query -> orderBy(['created_at' => SORT_DESC]);
	?>




    <?= GridView::widget([
        'dataProvider' => $dataProvider,		
		'summary' => false,
		'tableOptions' => [
			'class' => 'table price-table counts-table has-sort',
			
        ],        
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					
					$address = Address::findOne($data['id']);
/*					
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'),
								'items' => $address -> getOwnerMenuItems(),
							],
						],
					]);
*/										
				}					
			],

			
			['attribute' => 'created_at',
				'label' => 'Дата ставки',
				'contentOptions' => ['class' => 'col-created_at '],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at '],				
				'footerOptions' => ['class' => 'col-created_at '],
				
			],
			
            ['attribute' => 'auction_date',
				'label' => 'Аукцион',
				'contentOptions' => ['class' => 'col-auction_date '],
				'filterOptions' => ['class' => 'col-auction_date'],
				'headerOptions' => ['class' => 'col-auction_date '],				
				'footerOptions' => ['class' => 'col-auction_date '],
				'format' => 'raw',
				'value' => function($data){			        
					return Html::a($data -> auction -> date_view, Url::to(['owner/auction/view/'.$data['auction_id']]), ['class' => 'move-to-address']);
                }
				
			],

            
            
            ['attribute' => 'address_id',
				'label' => Address::getMeta('address', 'label'),
				'contentOptions' => ['class' => 'col-address '],
				'filterOptions' => ['class' => 'col-address'],
				'headerOptions' => ['class' => 'col-address '],				
				'footerOptions' => ['class' => 'col-address '],
                'format' => 'raw',
                'value' => function($data){
			        return Html::a($data['address_view'], Url::to(['/address/'.$data['address_id']]), ['class' => 'move-to-address', 'target' => '_blank']);
                }
				
				
				
			],
            ['attribute' => 'owner_id',
				'label' => Address::getMeta('owner_id', 'label'),
				'contentOptions' => ['class' => 'col-owner_id '],
				'filterOptions' => ['class' => 'col-owner_id'],
				'headerOptions' => ['class' => 'col-owner_id '],				
				'footerOptions' => ['class' => 'col-owner_id '],
				'filter' => Address::getExistsValues('owner_id'),	
				'format' => 'raw',
				'value' => function($data){ 

					return Html::a($data -> owner -> getCaption('%fullname%'), Url::to(['/user/view/'.$data['owner_id']]), ['class' => 'move-to-address', 'target' => '_blank']);
				},
				
			],
			
			['attribute' => 'amount_view',
				//'label' => Address::getMeta('amount', 'label'),
				'label' => 'Ставка',
				'contentOptions' => ['class' => 'col-amount '],
				'filterOptions' => ['class' => 'col-amount'],
				'headerOptions' => ['class' => 'col-amount '],				
				'footerOptions' => ['class' => 'col-amount '],

			],
				

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
	?>

</div>

	<?php
	
	echo	$this->render('_filter', [
			'searchModel' => $searchModel,
	]) 
	
	?>

