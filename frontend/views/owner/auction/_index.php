<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

use frontend\models\Auction;
use frontend\models\AuctionSearch;
use frontend\models\User;
use kartik\nav\NavX;
use kartik\icons\Icon;


$searchModel = new AuctionSearch();	
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider -> sort = false;
$dataProvider -> query -> orderBy(['date' => SORT_ASC]);


	if ($tab == 'arch') { // показать архивные аукционы
		$statusId = Auction::statusByAlias('noactive') -> id;
		$sortDirect = SORT_DESC; // т.к. архив, то более поздние даты сверху - ближе к сегодня
	} else { // показать открытые аукционы
		$statusId = Auction::statusByAlias('active') -> id;
		$sortDirect = SORT_ASC;
	};
	
	$dataProvider -> query -> andWhere(['status_id' => $statusId]) -> orderBy(['date' => $sortDirect]); 


?>
    <div class="flex auction-f" style="float: right; margin-bottom: 20px;">
        <?=\frontend\components\FilterWindow::getFilterButton($searchModel)?>

    </div>
	<div class="content">
	<?= \backend\models\Auction::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	

    <div class="grid-view long-table">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'tableOptions' => [
			'class' => 'table price-table counts-table has-sort',
        ],
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								//'items' => backend\models\Auction::getListViewMenu($data),
								'items' => $data -> getMenu()
							],
						],
					]);
				}					
			],


            ['attribute' => 'date',
				'label' => Auction::getMeta('date', 'label'),
				'contentOptions' => ['class' => 'col-date adate'],
				'filterOptions' => ['class' => 'col-date'],
				'headerOptions' => ['class' => 'col-date adate'],				
				'footerOptions' => ['class' => 'col-date adate'],
				'format' => 'raw',
				'value' => function($data) {
					return Html::a($data -> date_view, Url::to(['/owner/auction/view/'.$data -> id]), ['title' => 'Перейти на карточку аукциона']);
				}
				
				
			],

			
            ['attribute' => 'address_id',
				'label' => Auction::getMeta('address_id', 'label'),
				'contentOptions' => ['class' => 'col-address_id '],
				'filterOptions' => ['class' => 'col-address_id'],
				'headerOptions' => ['class' => 'col-address_id '],				
				'footerOptions' => ['class' => 'col-address_id '],
				'filter' => Auction::getExistsValues('address_id'),	
				'value' => function($data){ return $data['address_id_view']?$data['address_id_view']: '-';},
				
			],

            ['attribute' => 'owner_id',
				'label' => Auction::getMeta('owner_id', 'label'),
				'contentOptions' => ['class' => 'col-owner_id '],
				'filterOptions' => ['class' => 'col-owner_id'],
				'headerOptions' => ['class' => 'col-owner_id '],				
				'footerOptions' => ['class' => 'col-owner_id '],
				'filter' => Auction::getExistsValues('owner_id'),	
				'value' => function($data){ 					
					return $data['owner_id']?$data -> owner -> fullname:'-';
				},
				
			],
			

            ['attribute' => 'status_id',
				'label' => Auction::getMeta('status_id', 'label'),
				'contentOptions' => ['class' => 'col-status_id '],
				'filterOptions' => ['class' => 'col-status_id'],
				'headerOptions' => ['class' => 'col-status_id '],				
				'footerOptions' => ['class' => 'col-status_id '],
				'filter' => Auction::getExistsValues('status_id'),	
				'value' => function($data){ return $data['status_id_view'];},
				
			],

            ['attribute' => 'rate_count',
				'label' => Auction::getMeta('rate_count', 'label'),
				'contentOptions' => ['class' => 'col-rate_count '],
				'filterOptions' => ['class' => 'col-rate_count'],
				'headerOptions' => ['class' => 'col-rate_count '],				
				'footerOptions' => ['class' => 'col-rate_count '],
				'value' => function($data){ return $data['rate_count']?$data['rate_count']:0;},
				
				
			],

            ['attribute' => 'amount',
				'label' => Auction::getMeta('amount', 'label'),
				'contentOptions' => ['class' => 'col-amount '],
				'filterOptions' => ['class' => 'col-amount'],
				'headerOptions' => ['class' => 'col-amount '],				
				'footerOptions' => ['class' => 'col-amount '],
				'value' => function($data){ return $data['amount']?$data['amount']:0;},
				
				'format' => ['decimal', 2],
			],

			[
                'class' => 'yii\grid\ActionColumn',

                'buttons' => [
                    'add' => function ($url, $model) {

                        return 
							
                            Html::a(
                                Icon::show('thumbs-o-up'),
                                Url::toRoute(['rate', 'id' => $model->id]),
                                [
									
                                    //'title' => ($model -> owner_id == User::currentUser()?'Просмотреть ставки':'Сделать ставки'),
									'title' => 'Сделать ставки',
									'class' => 'auction-rate-'.($model -> owner_id == User::currentUser()?'green':'red'),
                                    'data-method' => 'get',
                                    'data-pjax' => '0',
                                    'data-params' => ['id' => $model->id],
                                ]
                            );

                    }
                ],
                'template' => (($tab == 'arch') ? '':'{add}'),


            ],
/*																				
*/			
			
        ],
    ]); ?>
    </div>
	<?php
	
	echo	$this->render('_filter', [
			'searchModel' => $searchModel,
	]) 
	
	?>	
