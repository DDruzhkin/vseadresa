<?php
if (YII_DEBUG) $startTime = microtime(true);

use common\models\Entity;
use frontend\models\Address;
use frontend\models\AuctionRate;
use frontend\models\Status;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;


$this -> title = 'Сделать ставки на аукцион '.$auction -> date_view;

$this->params['breadcrumbs'] =
    [

        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/owner'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
		[
            'label'     => 'Аукционы',
            'url'       =>  ['/owner/auction'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,
        ],

    ];
?>
<h1><?=$this -> title?></h1>
<?php

$statusPublishedAddressId = Status::find() -> andWhere(['alias' => 'published', 'table' => Address::tableName()]) -> one() -> id;
$list = $auction -> getRelatedListValues('address_id', false, ['status_id' => $statusPublishedAddressId, 'owner_id' => Entity::currentUser()], true); // Список опубликованных адресов пользователя
$auctionAddresses = $auction -> getAuctionAddresses(); // здесь список адресов, участвующих в аукционе с характеристиками

///var_dump($auctionAddresses);

?>
<form class="auction-rate-form" action="<?=Url::to(['/owner/auction/rate-pay/'.$auction -> id])?>">
    <div class="grid-view">
	<table class="table price-table counts-table">
		<thead>
			<tr>
				<th>Адрес</th>
				<th>Налоговая</th>
				<th>Округ</th>
				<th>Ставка</th>
				<th>Место</th>
				<th>Ваша ставка</th>
			</tr>
		</thead>

		<?php		
        ///echo "<pre>",print_r($notifications), "</pre>";
		foreach($list as $addressId => $data) {
			//$rate = AuctionRate::findOne([$addressId]);
			
			$address = Address::findOne([$addressId]);
			if (is_null($address)) continue;
			
			if (array_key_exists($addressId, $auctionAddresses)) { // участвует в аукционе
				$rate = $auction -> getAddressRates($addressId, 1)[0];
			} else $rate = null;
						
			
			//$statusId = $id;
			/*
			if (array_key_exists($statusId, $notifications)) {
				$enabled = $notifications[$statusId] -> enabled;
				$email = (is_null($notifications[$statusId] -> email) ? Yii::$app->user->identity->email : $notifications[$statusId] -> email);
				//var_dump($email);
			} else {
				$email = Yii::$app->user->identity->email;
				$enabled = false;
			}	
			*/
			
			echo  '
			<tr>
			<td>'.
			$address -> address
			.'</td>
			<td>'.
			$address -> nalog_view
			.'</td>		
			<td>'.
			$address -> okrug_view
			.'</td>
			<td>'.($rate?Yii::$app -> formatter -> asCurrency($auctionAddresses[$addressId]['s']):'0').'</td>
			<td>'.($rate?$auctionAddresses[$addressId]['position']:'-').'</td>
			
			<td> <input name="addrates['.$addressId.']" class="form-control" value="0" type="number" min="0" step="100"/></td>
			
			</tr>
			';	
		}
		?>
		
		</table>
		<input type="submit" class="reg-button change-data" value="Сделать ставки"/>
    </div>
		</form>
		
<?php		
/*		


$controls = [
	'date' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>AuctionRate::getMeta('date', 'label'), 'hint'=>''.AuctionRate::getMeta('date', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
//	'auction_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>AuctionRate::getMeta('auction_id', 'label'), 'items' => AuctionRate::getListValues('auction_id'), 'hint'=>''.AuctionRate::getMeta('auction_id', 'comment'), 'options'=>[]],
	'address_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>AuctionRate::getMeta('address_id', 'label'), 'items' => $model -> getRelatedListValues('address_id', false, ['status_id' => $statusPublishedAddressId]), 'hint'=>''.AuctionRate::getMeta('address_id', 'comment'), 'options'=>[]],
//	'owner_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>AuctionRate::getMeta('owner_id', 'label'), 'items' => AuctionRate::getListValues('owner_id'), 'hint'=>''.AuctionRate::getMeta('owner_id', 'comment'), 'options'=>[]],
	'amount' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>AuctionRate::getMeta('amount', 'label'), 'hint'=>''.AuctionRate::getMeta('amount', 'comment'), 'options'=>['clientOptions' => ['alias' =>  'decimal', 'groupSeparator' => ' ','autoGroup' => true],]],
];


$data = [

				[
                    'attributes' => [
                        'address_id' => $controls['address_id'],
                    ],
                ], [
                    'attributes' => [
                        'amount' => $controls['amount'],
                    ]

                ]
				

	

];


?>

<div class="auction-rate-form">


<?php

$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

*/


if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
