
<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;

use frontend\models\Address;
use frontend\models\AuctionRate;

use frontend\components\FilterWindow;
?>
<style>


#OrderFilterForm .input-daterange {
	float: left;
	margin: 0;
	width: 90%;
}

#OrderFilterForm .input-daterange .form-control{
		width: 100%;
}

#OrderFilterForm .form-control {
	float: left;
	
	margin: 0;
}

#OrderFilterForm input.form-control {
	width: 90%;
}

#OrderFilterForm #ordersearch-address_id {
	width: 90%;
}

#OrderFilterForm .hint-block{
	display: block;
	float: left;
	width: 90%;
}

div.field-auctionratesearch-amount {
	width: 200px;
}

</style>
<?php

$list = AuctionRate::getExistsValues('amount');
$amountList = ['' => ''];
foreach($list as $value) {
	$amountList[$value] = $value;
};


$layout = <<< HTML
    
    {input1}    	
    {separator}    
    {input2}
    <span class="input-group-addon kv-date-remove">
        <i class="glyphicon glyphicon-remove"></i>
    </span>
HTML;

$controls = [
	
	'address_id' => ['type'=>Form::INPUT_TEXT, 'label'=>AuctionRate::getMeta('address_id', 'label'), 'hint'=>'поиск адреса по подстроке', 'options'=>[]],
	'owner_id' => ['type'=>Form::INPUT_TEXT, 'label'=>AuctionRate::getMeta('owner_id', 'label'), 'hint'=>'поиск владельца адреса по подстроке', 'options'=>[]],	
	'amount' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>AuctionRate::getMeta('amount', 'label'), 'items' => $amountList, 'hint'=>'', 'options'=>[]],	
		
		
		
	'rate_date_start' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'kartik\widgets\DatePicker', 'label'=>'Дата ставки: ',
		'hint'=>'Выберите период, в рамках которого необходимо найти ставку', 
		'options'=>[
			'layout' => $layout,
			'attribute' => 'rate_date_start',
			'attribute2' => 'rate_date_fin',
			'separator' => ' - ',
			'type' => kartik\widgets\DatePicker::TYPE_RANGE,
			'pluginOptions' => [
				'todayHighlight' => true, 
				'todayBtn' => true,				
				'autoclose'=>true,   			
				'endDate' => '+1d',
				'format' => 'dd.mm.yyyy'
			]
		]
	],
	
	'auction_date_start' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'kartik\widgets\DatePicker', 'label'=>'Дата аукциона: ',
		'hint'=>'Выберите период, в рамках которого необходимо найти аукцион', 
		'options'=>[
			'layout' => $layout,
			'attribute' => 'auction_date_start',
			'attribute2' => 'auction_date_fin',
			'separator' => ' - ',
			'type' => kartik\widgets\DatePicker::TYPE_RANGE,
			'pluginOptions' => [
				'todayHighlight' => true, 
				'todayBtn' => true,				
				'autoclose'=>true,   			
				'endDate' => '+1d',
				'format' => 'dd.mm.yyyy'
			]
		]
	],
						
	'actions'=>['type'=>Form::INPUT_RAW, 'value'=>
        '<div class="standart-btn-container">'.
		Html::submitButton('Применить фильтр', ['class'=>'reg-button', 'style'=>'float: right; margin-right: 20px;']).
		Html::button('Очистить фильтр', ['class'=>'reg-button reset', 'style'=>'float: right; margin-right: 20px;']).
        "</div>"
		
	], 
];


$layout = [
[
	'columns' => 1,
	'attributes' => [		
		
		'rate_date_start' => $controls['rate_date_start'],						
	]
],
[
	'columns' => 1,
	'attributes' => [		
		
		'auction_date_start' => $controls['auction_date_start'],						
	]
],
[	
	'columns' => 1,
	'attributes' => [		
		'address_id' => $controls['address_id'],						
	],
],

[	

	'columns' => 1,
	'attributes' => [		
		'owner_id' => $controls['owner_id'],										
	],
],

[	

	'columns' => 1,
	'attributes' => [		
		'amount' => $controls['amount'],
	],
],


[
	'columns' => 1,
	'attributes' => [				
		'actions' => FilterWindow::getActions(),								
		
		
	]
],





];




	echo FilterWindow::widget(
			[
				'options' => [
					'id' => 'OrderFilterForm', 					
					'method' => 'get',
				],
				'model' => $searchModel,
				'layout' => $layout,
					
			]);




/*

$form = ActiveForm::begin([
	'id' => 'OrderFilterForm', 
	'method' => 'get',
//	'action' => Url::to(['/address/prices']),
]);
echo FormGrid::widget([
    'model' => $searchModel,
    'form' => $form,
	//'autoGenerateColumns' => false,
    'rows' => $layout
	]);
	
ActiveForm::end();

*/