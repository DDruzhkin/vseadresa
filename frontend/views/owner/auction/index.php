<style>
	.auction-rate-red{
		color: red;
	}
	.auction-rate-green {
		color:	#33AA33;
	}
</style>
<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use common\models\AuctionSearch;
use frontend\models\Auction;
use frontend\models\User;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;





$sections = [
	'index' => 'Открытые аукционы',
	'arch' => 'Архив аукционов',
//	'rate-list' => 'Список ставок',	
	'rules' => 'Правила аукциона',	
	
];


if (!array_key_exists($tab, $sections)) throw new NotFoundHttpException('Страница аукционов на нейдена: '.$tab);
$this->title = $sections[$tab];
$this->params['breadcrumbs'] =
    [

        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/owner'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,
        ],

    ];
	

	
?>



<div class="auction-index" id="auc">
    <h1><?=$sections[$tab]?></h1>
    <?php

	$items = [];
	foreach ($sections as $name => $title) {
		
		$items[] = [
			'label' => $title,						
			'url' => Url::to(['/owner/auction/'.$name]),
		];
	}
	
    echo NavX::widget([
        'options'=>['class'=>'nav nav-context menu-custom-icon auction-menu', 'style' => 'width:100px'],
        'encodeLabels' => false,
        'items' => [
            [
                'label' => Icon::show('bars'),
//                'items' => $menu,
                'items' => $items,
            ],
        ],
    ]);

	
	$params = ['tab' => $tab];
	if ($tab == 'rate-list') {
		 $params['searchModel'] = $searchModel;
		 $params['dataProvider'] = $dataProvider;
	}
	
		echo $this->render(
			'_'.($tab=='arch'?'index':$tab),
			$params	
		);	
	
return;

    ?>
	
    </div>
 	
</div>
