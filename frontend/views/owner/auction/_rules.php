<?php
use frontend\models\Page;
use yii\web\NotFoundHttpException;


	$page = Page::bySlug('auction');	
	if (!$page) throw new NotFoundHttpException('Не найдена страница auction');
?>
<div class="content"><?=$page->content?></div>