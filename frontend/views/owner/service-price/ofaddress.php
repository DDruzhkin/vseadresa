<?php
if (YII_DEBUG) $startTime = microtime(true);
use frontend\models\ServicePrice;
use common\models\Entity;
//use backend\models\ServicePrice;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Редактирование услуги';
$this->params['breadcrumbs'][] = $this -> title;
?>



<script>
$(document).ready(function() {
	
	//***** декорация чекбоксов, описанных внутри label
	$("label input[type=checkbox]").each(function(index, chb){
		$(chb).hide();				
		chbD = $(chb).after('<div class="form-checkbox' +($(chb).prop('checked') ?' active-checkbox':'') +'"></div>');			
		console.log("!");
	});
	
			
	$("label input[type=checkbox]").parent().on("click", function () {				
		chb = $(this).find("input[type=checkbox]");
		chbD = $(this).find("div.form-checkbox");
		if ($(chb).prop('checked')) chbD.addClass("active-checkbox"); else chbD.removeClass("active-checkbox");		
		
	//	$(this).parent().find("input[name='ServicePrice[to_address]']").val($(chb).prop('checked') == true?1:0);
		
    });		
	//***** /декорация чекбоксов, описанных внутри label

	
	
	
});

</script>

<style>
div.form-checkbox {
	
	float: left;
	margin-right: 15px;
}
</style>
<h2>Справочник услуг адреса</h2>
<?php

$ownerServiceList = ServicePrice::find() -> andFilterWhere(['owner_id'=>Entity::currentUser()]) -> andWhere(['address_id' => NULL]) -> all();
$ownerServices = [];
foreach($ownerServiceList as $service) {
	$ownerServices[$service -> id] = $service -> getCaption('%name%');	
}

$controls = [
	'services' => ['type'=>Form::INPUT_CHECKBOX_LIST, 'label'=>'', 'items' => $ownerServices, 'hint'=>'', 'options'=>['multiple' => true]],
	'submit'=>['type'=>Form::INPUT_RAW, 'value'=>  Html::submitButton(($model -> getIsNewRecord()?'Добавить услугу':'Изменить данные'), ['class'=>'reg-button next'])]
];


$layout =
	[
		[		
			'attributes' => [
				'services'=>$controls['services'],		
			]
			
		],[		
			'attributes' => [
				'submit' => $controls['submit'],								
			],
		]
	];
	

$form = ActiveForm::begin(['id' => 'form-service-price']);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $layout
	]);	
ActiveForm::end();


if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>