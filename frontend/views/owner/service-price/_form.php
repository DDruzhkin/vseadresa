<?php
if (YII_DEBUG) $startTime = microtime(true);
use backend\models\ServicePrice;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$stepCount = 2;
?>

<!-- ********************************************************* -->
<!-- Код стилизации контролов. TODO: Необходимо вынести отсюда -->
<style>
<!-- Стилищация checkboxX -->
/*.cbx-icon {
	background-color: #fff;
}
.cbx-icon i{
	cursor: pointer;
    height: 20px;
    width: 20px;
    border-radius: 5px;    
    box-shadow: inset 1px 1px 10px 0px rgba(128,128,128,1);	
	border: 0;
    height: 20px;
    width: 20px;
	top: -2px; left: -2px;
    border-radius: 5px;
	background-color: #F93237;
	color:#F93237;
}

.cbx-container {
	margin-top: 8px;
	margin-bottom: 7px;
	
	width: 65%;
}*/


</style>

<!-- ********************************************************* -->
<!-- ***** Скрипт управления показом контролов ************** -->
<script>

function refreshControls() {
	
<?php 
if($step == 1):
?>	
	systemService = $('#serviceprice-service_name_id').val() > 0;	
//	console.log('serviceprice-service_name_id: ' + $('#serviceprice-service_name_id').val() + ' : ' + systemService);
	$('#serviceprice-name').attr('readonly', systemService);
	if (systemService || ($('#serviceprice-name').val==''))$('#serviceprice-name').val($("#serviceprice-service_name_id option:selected").text());	
<?php 
endif
?>	
<?php 
if($step == 2):
?>		
	stype = $('#serviceprice-stype').val();	
	if (stype == '<?=ServicePrice::STYPE_ONCE?>') {
		$('#serviceprice-price').parent().parent().show();
		$('#serviceprice-price6').parent().parent().hide();
		$('#serviceprice-price11').parent().parent().hide();
	} else {
		$('#serviceprice-price').parent().parent().hide();
		$('#serviceprice-price6').parent().parent().show();
		$('#serviceprice-price11').parent().parent().show();
	}
<?php 
endif
?>		
}

$(document).ready(function() {	
// Wizard
	$('button.next').click(function() {
		$("#form-service-price").attr("action", "<?=Url::to(["/owner/service-price/update/".(($model->getIsNewRecord())?"":$model->id)."/".($step + 1)])?>"); 
	});	
	<?php if((int)$step == 1): ?>
        $('button.prior').click(function(e) {
            e.preventDefault();
            console.log("aaa");
             window.location.href = '/owner/service-price';
        });
    <?php else: ?>

        $('button.prior').click(function() {
            $("#form-service-price").attr("action", "<?=Url::to(["/owner/service-price/update/".(($model->getIsNewRecord())?"":$model->id)."/".($step - ($step> 1?1:0))])?>");
        });
    <?php endif;?>


	refreshControls();
	$('#serviceprice-service_name_id').change(function(){		
		refreshControls();
	});
	
	$('#serviceprice-stype').change(function(){
		refreshControls();
	});
	
	
});
 </script>

<!-- ********************************************************* -->
<?php

$serviceNameList = ServicePrice::getListValues('service_name_id', '%name%'); // Список ServiceName доступных для создания услуг

$list = $model -> getCurrentUser() -> servicePriceList; // список услуг, которые уже присутствуют в справочнике - они будут исключены из списка
$list = $list ? $list -> andFilterWhere(['enabled' => 1]) -> all():[];
$list = $model -> objectsByTemplate($list, '%service_name_id%');
$ownerServiceNameList = array_flip($list);
$serviceNameList = array_diff_key($serviceNameList, $ownerServiceNameList);


//$serviceNameList = ArrayHelper::merge([0 => ''], $serviceNameList);

//var_dump($serviceNameList); exit;

// TODO: Необходимо запретить изменять ServiceName пользователю при редактировании услуги, после этого код по исключению вариантов можно открыть. Не забыть добавить пустое значение для выбора!


$controls = [
						'code_view' => ['type'=>Form::INPUT_TEXT, 'label'=>ServicePrice::getMeta('code_view', 'label'), 'hint'=>''.ServicePrice::getMeta('code_view', 'comment'), 'options'=>['readonly'=>'true', ]],						
						'stype' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>ServicePrice::getMeta('stype', 'label'), 'items' => ServicePrice::getList('stype'), 'hint'=>''.ServicePrice::getMeta('stype', 'comment'), 'options'=>[]],						
						'service_name_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>ServicePrice::getMeta('service_name_id', 'label'), 'items' => $serviceNameList, 'hint'=>ServicePrice::getMeta('service_name_id', 'comment'), 'options'=>[]],
						'service_name_view' => ['type'=>Form::INPUT_TEXT, 'label'=>ServicePrice::getMeta('service_name_id', 'label'), 'hint'=>'Услуга из справочника услуг, если услуга является системной', 'options'=>['readonly' => true]],
						'name' => ['type'=>Form::INPUT_TEXT, 'label'=>ServicePrice::getMeta('name', 'label'), 'hint'=>''.ServicePrice::getMeta('name', 'comment'), 'options'=>[]],
						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>ServicePrice::getMeta('created_at_view', 'label'), 'hint'=>''.ServicePrice::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'to_address' => ['type'=>Form::INPUT_CHECKBOX, 'label'=>ServicePrice::getMeta('to_address', 'label'), 'hint'=>''.ServicePrice::getMeta('to_address', 'comment'), 'options'=>['value' => 1]],
//						'owner_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>ServicePrice::getMeta('owner_id_view', 'label'), 'hint'=>ServicePrice::getMeta('owner_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'recipient_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>ServicePrice::getMeta('recipient_id', 'label'), 'items' => $model -> getRelatedListValues('recipient_id'), 'hint'=>''.ServicePrice::getMeta('recipient_id', 'comment'), 'options'=>[]],
//						'address_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>ServicePrice::getMeta('address_id', 'label'), 'items' => $model -> getRelatedListValues('address_id'), 'hint'=>ServicePrice::getMeta('address_id', 'comment'), 'options'=>[]],						
						'description' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>ServicePrice::getMeta('description', 'label'), 'hint'=>''.ServicePrice::getMeta('description', 'comment'), 'options'=>[]],								
						'type' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>ServicePrice::getMeta('type', 'label'), 'items' => ServicePrice::getList('type'), 'hint'=>''.ServicePrice::getMeta('type', 'comment'), 'options'=>[]],
						'price' => ['type'=>Form::INPUT_WIDGET, 'label'=>ServicePrice::getMeta('price', 'label'), 'widgetClass'=>'\yii\widgets\MaskedInput','hint'=>''.ServicePrice::getMeta('price', 'comment'), 'options'=>['mask' => ServicePrice::getMaskByAlias("money"),]],
						'price6' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>ServicePrice::getMeta('price6', 'label'), 'hint'=>''.ServicePrice::getMeta('price6', 'comment'), 'options'=>['mask' => ServicePrice::getMaskByAlias("money"),]],
						'price11' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>ServicePrice::getMeta('price11', 'label'), 'hint'=>''.ServicePrice::getMeta('price11', 'comment'), 'options'=>['mask' => ServicePrice::getMaskByAlias("money"),]],
						'prior'=>['type'=>Form::INPUT_RAW, 'value'=> Html::submitButton('Назад', ['class'=>'reg-button prior'])],
						'next'=>['type'=>Form::INPUT_RAW, 'value'=>  Html::submitButton(($model -> getIsNewRecord()?'Добавить услугу':'Далее'), ['class'=>'reg-button next'])],            
						'submit'=>['type'=>Form::INPUT_RAW, 'value'=>  Html::submitButton(($model -> getIsNewRecord()?'Добавить услугу':'Сохранить данные'), ['class'=>'reg-button next'])]
  
];








if ($model -> service_name_id) { // Если услуга системная, то не даем изменять stype
	$controls['stype']['type'] = Form::INPUT_TEXT;
	$controls['stype']['options']['readonly'] = 'true';	
}


$layouts =
[
1=>
	[
		[		
			'attributes' => [
				'header'=>['type'=>Form::INPUT_RAW, 'value'=>  '<h1>Описание услуги</h1>'],
			]
			
		],[		
			'attributes' => 
				($model -> getIsNewRecord()?
					['service_name_id' => $controls['service_name_id']]:								
					['service_name_view' => $controls['service_name_view']]),								
			
		],[
			'attributes' => [
				'name' => $controls['name'],								
			]
		],[
			'attributes' => [
				'to_address' => $controls['to_address'],								
			],
		],[
			'attributes' => [				
				'description' => $controls['description'],										
			],		
		],[
			'attributes' => [
				'prior' => $controls['prior'],
				'next' => $controls['next'],								
			],	
		]
	],
2 => 
	[
		[		
			'attributes' => [
				'header'=>['type'=>Form::INPUT_RAW, 'value'=>  '<h2>Ценообразование</h2>'],            	
			]
			
		],[
			'attributes' => [
				'stype' => $controls['stype'],											
			
			],
		
		],[
			'attributes' => [
				'price' => $controls['price'],								
			],	
		],[
			'attributes' => [
				'price6' => $controls['price6'],								
			],
		],[
			'attributes' => [
				'price11' => $controls['price11'],								
			],	
		],[
			'attributes' => [
				'recipient_id' => $controls['recipient_id'],								
			],
		],[
			'attributes' => [
				'prior' => $controls['prior'],
				'submit' => $controls['submit'],								
			],	
		]
	],	
			
];
	




/* @var $this yii\web\View */
/* @var $model backend\models\ServicePrice */
/* @var $form yii\widgets\ActiveForm */
?>


<?php
//pred($model);
$form = ActiveForm::begin(['id' => 'form-service-price']);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $layouts[$step]
	]);	
ActiveForm::end();


if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>