<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\ServicePrice;
use common\models\Entity;
use common\models\ServicePriceSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Ваши услуги';
$this->params['breadcrumbs'] =
    [	
        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/owner'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,			
        ]
    ];
?>
<h1>Услуги</h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="mobile-cabinet-button">
        <?= Html::a('Добавить услугу', ['create'], ['class' => 'public-address-button']) ?>
    </p>	
	<p class="hint-block show-block legend-block">
		
		<a title="Системная"><?=Icon::show('sticky-note')?></a> - Системная услуга<br/>
		<a title="Пользовательская"><?=Icon::show('sticky-note-o')?></a> - Пользовательская услуга<br/>
		<a title="Автоматически добавлять в адреса"><?=Icon::show('address-card')?></a> - Услуга будет автоматически добавляться в новый адреса<br/>
	</p>
	
	<?php 		
		$dataProvider -> query -> andFilterWhere(['owner_id'=>Entity::currentUser(), 'enabled' => 1]) -> andWhere(['address_id' => NULL]);
	?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'summary' => false,

		'tableOptions' => [
			'class' => 'table price-table counts-table has-sort',
        ],

        //'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					$model = ServicePrice::findOne($data['id']);
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 
								'items' => [
									['label' => 'Изменить', 'url' => Url::toRoute(['update', 'id' => $model -> id])],
								],
							],
						],
					]);
				}					
			],

			
						            

            ['attribute' => 'code',
				'label' => ServicePrice::getMeta('code', 'label'),
				'contentOptions' => ['class' => 'col-code '],
				'filterOptions' => ['class' => 'col-code'],
				'headerOptions' => ['class' => 'col-code '],				
				'footerOptions' => ['class' => 'col-code '],
				
				'value' => function($data)
				{ 
					$value = ($data['service_name_id_view'])?'<a title="Системная">'.Icon::show('sticky-note').' </a>':'<a title="Пользовательская">'.Icon::show('sticky-note-o').' </a>';
					$value = $value.(($data['to_address'])?'<a title="Автоматически добавлять в адреса">'.Icon::show('address-card').' </a>':'<a title="Не добавлять в адреса автоматически">'.Icon::show('address-card-o').' </a>');
					return $value.$data['code'];
				},	
				'format' => 'raw',
			],
            
			['attribute' => 'name',
				'label' => ServicePrice::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				
				
				
			],
			

			
			['attribute' => 'stype',
				'label' => ServicePrice::getMeta('stype', 'label'),
				'contentOptions' => ['class' => 'col-stype '],
				'filterOptions' => ['class' => 'col-stype'],
				'headerOptions' => ['class' => 'col-stype '],				
				'footerOptions' => ['class' => 'col-stype '],
				'filter' => ServicePrice::getList('stype'),	
				
				
			],
			
			
            ['attribute' => 'to_address',
				'label' => ServicePrice::getMeta('to_address', 'label'),
				'contentOptions' => ['class' => 'col-to_address hidden '],
				'filterOptions' => ['class' => 'col-to_address hidden'],
				'headerOptions' => ['class' => 'col-to_address hidden '],				
				'footerOptions' => ['class' => 'col-to_address hidden '],
				
				
				
			],
	
            
            ['attribute' => 'price',
				'label' => 'Цена, руб.',
				'contentOptions' => ['class' => 'col-price '],
				'filterOptions' => ['class' => 'col-price'],
				'headerOptions' => ['class' => 'col-price '],				
				'footerOptions' => ['class' => 'col-price '],
				'value' => function($data) {					
					
					$sp = ServicePrice::findOne($data['id']);
					$result = '';
					if ($sp -> stype == ServicePrice::STYPE_ONCE) {
						$p = (int)$sp -> price;
						if ($p > 0) {
							//$result = '<a title="Разовая стоимость услуги">'.$p.'</a>';
							$result = $p;
						};
					} else {
						$price6 = (int)$sp -> price6;
						$price11 = (int)$sp -> price11;
						if (($price6 + $price11) == 0) {
							$result = '-';
						} else if ($price6 == $price11){
							$result = $price6;
						} else {
							if ($price6 > 0) {
								$result = $result.$price6.'(при аренде на 6 мес)';
							};
							if ($price6 * $price11) {
								$result = $result.'<br>';
							}
							if ($price11 > 0) {
								$result = $result.$price11.'(при аренде на 11 мес)';
							};
						};
						
						//$result = '<a title="Цена за месяц при заказе на 6 месяцев">'.((int)$sp -> price6).'</a>/<a title="Цена за месяц при заказе на 11 месяцев">'.((int)$sp -> price11).'</a>';
						
					}
					return $result;
				},
				'format' => 'raw'
				
				
			],
			
            

            [
				'class' => 'yii\grid\ActionColumn',
			
				'buttons'=>[
                  'delete'=>function ($url, $model) {
						
					  return Html::a(
						  Icon::show('times'), 
						  Url::toRoute(['delete', 'id' => $model -> id]), 
						  [
								'title' => 'Удалить услугу из справочника',
								'class' => 'delete',
								'data-confirm'=> 'Вы уверены, что хотите удалить эту услугу из справочника?' ,
								'data-method'=> 'post' ,
								'data-pjax'=> '0',
						  
						  ]
					  );
				  }
				],
				'template'=>'{delete}',
			
			
			],
        ],
    ]); ?>

