<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ServicePrice */

$this->title = 'Добавление услуги';
$this->params['breadcrumbs'] =
    [
	
        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/owner'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,			
        ]
    ];
?>

    <?= $this->render('_form', [
        'model' => $model,
		'step' => $step,
    ]) ?>
