<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveField;
use common\models\User;
use common\models\Region;
use frontend\models\OwnerSignup;
?>
<section class="owner-form">
<div class="container-fluid">
    <div class="container">
        <form action="">
        <div class="form-group">
<select name="" id="choose-your-status" class="form-control">
<?php
$arr = OwnerSignup::attrLists()['form'];
foreach ($arr as $k=>$item) :?>
    <option value="<?=$k?>"><?=$item?></option>
<?php endforeach; ?>
</select>
        </div>
        </form>
    </div></div>





<?php $personForm = ActiveForm::begin([
    'id' => 'person',
    'enableAjaxValidation' => true,
    'enableClientValidation'=>true

]); ?>

    <div class="container-fluid reg-step">
        <div class="container">
            <div style="text-align: center">
                Физическое лицо
            </div>
            <div class="col-md-12">

                <div class="labels cabinet-label">
                    адрес доставки документов
                </div>
                <?php
                echo \frontend\components\DeliveryAddressWidget::widget(['form' => $personForm, 'data' => $deliveryAddress]);
                ?>


                <div class="labels cabinet-label">
                    паспортные данные
                </div>
                <fieldset>                
                    <div class="form-group field-user-fullname ">
                        <label class="control-label" for="user-fullname">фамилия
                        <sup>*</sup>
                        </label>
                        <?= $personForm->field($person, 'lname')->label(false)->textInput() ?>
                        <div class="help-block"></div>
                    </div>
			    </fieldset>
                <fieldset>                
                    <div class="form-group field-user-fullname ">
                        <label class="control-label" for="user-fullname">имя
                        <sup>*</sup>
                        </label>
                        <?= $personForm->field($person, 'fname')->label(false)->textInput() ?>
                        <div class="help-block"></div>
                    </div>
			    </fieldset>
                
                <fieldset>                
                    <div class="form-group field-user-fullname ">
                        <label class="control-label" for="user-fullname">отчество
                        <sup>*</sup>
                        </label>
                        <?= $personForm->field($person, 'mname')->label(false)->textInput() ?>
                        <div class="help-block"></div>
                    </div>
                </fieldset>
                <fieldset>                
                    <div class="form-group field-user-fullname ">
                        <label class="control-label" for="user-fullname">серия / номер паспорта
                        <sup>*</sup>
                        </label>
                        <?= $personForm->field($person, 'pasport_number')->label(false)->textInput() ?>
                        <div class="help-block"></div>
                    </div>
			    </fieldset>
                <fieldset>                
                    <div class="form-group field-user-fullname ">
                        <label class="control-label" for="user-fullname">дата выдачи паспорта
                        <sup>*</sup>
                        </label>
                        <?= $personForm->field($person, 'pasport_date')->label(false)->textInput() ?>
                        <div class="help-block"></div>
                    </div>
			    </fieldset>
                <fieldset>                
                    <div class="form-group field-user-fullname ">
                        <label class="control-label" for="user-fullname">орган выдавший паспорт
                        <sup>*</sup>
                        </label>
                        <?= $personForm->field($person, 'pasport_organ')->label(false)->textInput() ?>
                        <div class="help-block"></div>
                    </div>
			    </fieldset>
                <fieldset>                
                    <div class="form-group field-user-fullname ">
                        <label class="control-label" for="user-fullname">адрес регистрации
                        <sup>*</sup>
                        </label>
                        <?= $personForm->field($person, 'address')->label(false)->textInput() ?>
                        <div class="help-block"></div>
                    </div>
			    </fieldset>
                
                




                <div class="labels cabinet-label">
                    Реквизиты банковского счета
                </div>
                <fieldset>                
                    <div class="form-group field-user-fullname ">
                        <label class="control-label" for="user-fullname">БИК
                        <sup>*</sup>
                        </label>
                        <?= $personForm->field($person, 'bank_bik')->label(false)->textInput() ?>
                        <div class="help-block"></div>
                    </div>
			    </fieldset>
                
                <fieldset>                
                    <div class="form-group field-user-fullname ">
                        <label class="control-label" for="user-fullname">номер счета
                        <sup>*</sup>
                        </label>
                        <?= $personForm->field($person, 'account')->label(false)->textInput() ?>
                        <div class="help-block"></div>
                    </div>
                </fieldset>
                <fieldset>                
                    <div class="form-group field-user-fullname ">
                        <label class="control-label" for="user-fullname">Название банка
                        <sup>*</sup>
                        </label>
                        <?= $personForm->field($person, 'bank_name')->label(false)->textInput() ?>
                        <div class="help-block"></div>
                    </div>
                </fieldset>
                <fieldset>                
                    <div class="form-group field-user-fullname ">
                        <label class="control-label" for="user-fullname">Кор.счет банка
                        <sup>*</sup>
                        </label>
                        <?php //pred(\common\models\Rperson::attrMeta()['bank_cor']); ?>
                        <?= $personForm->field($person, 'bank_cor')->label(false)->textInput(['class'=>'bank-cor form-control', 'data-inputmask'=>"'mask': '".\common\models\Rperson::attrMeta()['bank_cor']['mask']."'"]) ?>
                        <div class="help-block"></div>
                    </div>
                </fieldset>
                <fieldset>                
                    <div class="form-group field-user-fullname ">
                        <label class="control-label" for="user-fullname">владелец счета
                        <sup>*</sup>
                        </label>
                        <?= $personForm->field($person, 'account_owner')->label(false)->textInput() ?>
                        <div class="help-block"></div>
                    </div>
                </fieldset>
                <fieldset>                
                    <div class="form-group field-user-fullname ">
                        
                        <div class="form-group cant-modify attention">
                        изменение данных для договора потребует заключение дополнительного соглашения об изменении реквизитов договора в течении одной недели, в случае, если договор не будет перезаключен, будут восстановлены данные текущего договора. Вы уверены что хотите изменить данные договора?
                        </div>
                        
                    </div>
			    </fieldset>
                
                <!-- <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">

                        </label>
                    </div>
                    <div class="form-input">
                        <div class="cant-modify attention">
                            изменение данных для договора потребует заключение дополнительного соглашения об изменении реквизитов договора в течении одной недели, в случае, если договор не будет перезаключен, будут восстановлены данные текущего договора. Вы уверены что хотите изменить данные договора?
                        </div>
                    </div>
                </div> -->


                <fieldset>                
                    <div class="form-group field-user-fullname ">
                        
                        <div class="form-group">
                        <?= Html::submitButton('Завершить регистрацию', ['class' => 'reg-button', 'name' => 'signup-button']) ?>
                        </div>
                        
                    </div>
			    </fieldset>

                <!-- <div class="field-row flex al"> -->
                    <!--<a href="" class="reg-button">
                        изменить данные
                    </a>-->
                    <?php //echo Html::submitButton('Завершить регистрацию', ['class' => 'reg-button', 'name' => 'signup-button']) ?>
                <!-- </div> -->
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>



<?php $companyForm = ActiveForm::begin([
    'id' => 'company',
    'enableAjaxValidation' => true,
    'enableClientValidation'=>true

]); ?>

<div class="container-fluid reg-step">
    <div class="container">
        <div style="text-align: center;">
            Юридическое лицо
        </div>
        <div class="col-md-12">

            <div class="labels cabinet-label">
                адрес доставки документов
            </div>
            <?php
                echo \frontend\components\DeliveryAddressWidget::widget(['form' => $companyForm, 'data' => $deliveryAddress]);
            ?>


            <div class="labels cabinet-label">
                Реквизиты юридического лица
            </div>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">Полное наименование организации
                    <sup>*</sup>
                    </label>
                    <?= $companyForm->field($company, 'full_name')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>

            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">ИНН
                    <sup>*</sup>
                    </label>
                    <?= $companyForm->field($company, 'inn')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">КПП
                    <sup>*</sup>
                    </label>
                    <?= $companyForm->field($company, 'kpp')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>

            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">ОГРН
                    <sup>*</sup>
                    </label>
                    <?= $companyForm->field($company, 'ogrn')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">Юридический адрес
                    <sup>*</sup>
                    </label>
                    <?= $companyForm->field($company, 'address')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            
            

            
            <div class="labels cabinet-label">
                Для организаций
            </div>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">Фамилия Имя Отчество лица подписывающего договор
                    <sup>*</sup>
                    </label>
                    <?= $companyForm->field($company, 'dogovor_signature')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">Договор подписывается в лице
                    <sup>*</sup>
                    </label>
                    <?= $companyForm->field($company, 'dogovor_face')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">Подписант договора действует на основании
                    <sup>*</sup>
                    </label>
                    <?= $companyForm->field($company, 'dogovor_osnovanie')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>



            <div class="labels cabinet-label">
                Реквизиты банковского счета
            </div>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">БИК
                    <sup>*</sup>
                    </label>
                    <?= $companyForm->field($company, 'bank_bik')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">номер счета
                    <sup>*</sup>
                    </label>
                    <?= $companyForm->field($company, 'account')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">Название банка
                    <sup>*</sup>
                    </label>
                    <?= $companyForm->field($company, 'bank_name')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">Кор.счет банка
                    <sup>*</sup>
                    </label>
                    <?php //pred(\common\models\Rperson::attrMeta()['bank_cor']); ?>
                    <?= $companyForm->field($company, 'bank_cor')->label(false)->textInput(['class'=>'bank-cor form-control', 'data-inputmask'=>"'mask': '".\common\models\Rcompany::attrMeta()['bank_cor']['mask']."'"]) ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">владелец счета
                    <sup>*</sup>
                    </label>
                    <?= $companyForm->field($company, 'account_owner')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">Используется система налогообложения, в которой используется НДС
                    </label>
                    <div class="form-input nds-input-form">
                                <div class="nds-box">

                                </div>
                            <?= $companyForm->field($company, 'tax_system_vat')->checkbox(['uncheck' => null, 'value' =>"", 'class'=>'hidden-cb', 'checked ' => ''], false)->label(false); ?>
                    </div>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            
            
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    
                    <div class="form-group cant-modify attention">
                    изменение данных для договора потребует заключение дополнительного соглашения об изменении реквизитов договора в течении одной недели, в случае, если договор не будет перезаключен, будут восстановлены данные текущего договора. Вы уверены что хотите изменить данные договора?
                    </div>
                    
                </div>
            </fieldset>


            <fieldset>                
                <div class="form-group field-user-fullname ">
                    
                    <div class="form-group">
                    <?= Html::submitButton('Завершить регистрацию', ['class' => 'reg-button', 'name' => 'signup-button']) ?>
                    </div>
                    
                </div>
            </fieldset>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>


<?php $ipForm = ActiveForm::begin([
    'id' => 'ip',
    'enableAjaxValidation' => true,
    'enableClientValidation'=>true

]); ?>

<div class="container-fluid reg-step">
    <div class="container">
        <div style="text-align: center;">
            Индивидуальный Предприниматель
        </div>
        <div class="col-md-12">

            <div class="labels cabinet-label">
                адрес доставки документов
            </div>
            <?php
            echo \frontend\components\DeliveryAddressWidget::widget(['form' => $ipForm, 'data' => $deliveryAddress]);
            ?>

            <div class="labels cabinet-label">
                паспортные данные
            </div>

             <fieldset>                
                <div class="form-group field-user-fullname">
                    <label class="control-label" for="user-fullname">фамилия
                    <sup>*</sup>
                    </label>
                    <?= $ipForm->field($rip, 'lname')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">имя
                    <sup>*</sup>
                    </label>
                    <?= $ipForm->field($rip, 'fname')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="">отчество
                    <sup>*</sup>
                    </label>
                    <?= $ipForm->field($rip, 'mname')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="">серия / номер паспорта
                    <sup>*</sup>
                    </label>
                    <?= $ipForm->field($rip, 'pasport_number')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="">дата выдачи паспорта
                    <sup>*</sup>
                    </label>
                    <?= $ipForm->field($rip, 'pasport_date')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="">орган выдавший паспорт
                    <sup>*</sup>
                    </label>
                    <?= $ipForm->field($rip, 'pasport_organ')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="">адрес регистрации
                    <sup>*</sup>
                    </label>
                    <?= $ipForm->field($rip, 'address')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
           
            
            

            <div class="labels cabinet-label">
                Реквизиты ИП
            </div>
            <?php

            /*
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">Название ИП</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($company, 'name')->label(false)->textInput() ?>
                </div>
            </div> */
            ?>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="">ИНН
                    <sup>*</sup>
                    </label>
                    <?= $ipForm->field($rip, 'inn')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            
<!--			
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">КПП</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($rip, 'kpp')->label(false)->textInput() ?>
                </div>
            </div>
-->
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="">ОГРНИП
                    <sup>*</sup>
                    </label>
                    <?= $ipForm->field($rip, 'ogrnip')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            
			<fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">Серия свидетельства
                    <sup></sup>
                    </label>
                    <?= $ipForm->field($rip, 'sv_seriya')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">Номер свидетельства
                    <sup></sup>
                    </label>
                    <?= $ipForm->field($rip, 'sv_nomer')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">Дата свидетельства
                    <sup></sup>
                    </label>
                    <?= $ipForm->field($rip, 'sv_date')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>




            <div class="labels cabinet-label">
                Реквизиты банковского счета
            </div>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="">БИК
                    <sup>*</sup>
                    </label>
                    <?= $ipForm->field($rip, 'bank_bik')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="">номер счета
                    <sup>*</sup>
                    </label>
                    <?= $ipForm->field($rip, 'account')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="">владелец счета
                    <sup>*</sup>
                    </label>
                    <?= $ipForm->field($rip, 'account_owner')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">Название банка
                    <sup>*</sup>
                    </label>
                    <?= $ipForm->field($rip, 'bank_name')->label(false)->textInput() ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">Кор.счет банка
                    <sup>*</sup>
                    </label>
                    <?php //pred(\common\models\Rperson::attrMeta()['bank_cor']); ?>
                    <?= $ipForm->field($rip, 'bank_cor')->label(false)->textInput(['class'=>'bank-cor form-control', 'data-inputmask'=>"'mask': '".\common\models\Rip::attrMeta()['bank_cor']['mask']."'"]) ?>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    <label class="control-label" for="user-fullname">Используется система налогообложения, в которой используется НДС
                    </label>
                    <div class="form-input nds-input-form">
                                <div class="nds-box">

                                </div>
                            <?= $ipForm->field($rip, 'tax_system_vat')->checkbox(['uncheck' => null, 'value' =>"", 'class'=>'hidden-cb', 'checked ' => ''], false)->label(false); ?>
                    </div>
                    <div class="help-block"></div>
                </div>
            </fieldset>
            
            <fieldset>                
                <div class="form-group field-user-fullname ">
                    
                    <div class="form-group cant-modify attention">
                    изменение данных для договора потребует заключение дополнительного соглашения об изменении реквизитов договора в течении одной недели, в случае, если договор не будет перезаключен, будут восстановлены данные текущего договора. Вы уверены что хотите изменить данные договора?
                    </div>
                    
                </div>
            </fieldset>




            <div class="field-row flex al">
                <!--<a href="" class="reg-button">
                    изменить данные
                </a>-->
                <?= Html::submitButton('Завершить регистрацию', ['class' => 'reg-button', 'name' => 'signup-button']) ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

</section>