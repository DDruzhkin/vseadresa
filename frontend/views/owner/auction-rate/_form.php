<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\AuctionRate;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$controls = [
	'date' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>AuctionRate::getMeta('date', 'label'), 'hint'=>''.AuctionRate::getMeta('date', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
//	'auction_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>AuctionRate::getMeta('auction_id', 'label'), 'items' => AuctionRate::getListValues('auction_id'), 'hint'=>''.AuctionRate::getMeta('auction_id', 'comment'), 'options'=>[]],
	'address_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>AuctionRate::getMeta('address_id', 'label'), 'items' => $model -> getRelatedListValues('address_id'), 'hint'=>''.AuctionRate::getMeta('address_id', 'comment'), 'options'=>[]],
//	'owner_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>AuctionRate::getMeta('owner_id', 'label'), 'items' => AuctionRate::getListValues('owner_id'), 'hint'=>''.AuctionRate::getMeta('owner_id', 'comment'), 'options'=>[]],
	'amount' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>AuctionRate::getMeta('amount', 'label'), 'hint'=>''.AuctionRate::getMeta('amount', 'comment'), 'options'=>['clientOptions' => ['alias' =>  'decimal', 'groupSeparator' => ' ','autoGroup' => true],]],
];


$data = [

				[
                    'attributes' => [
                        'address_id' => $controls['address_id'],
                    ],
                ], [
                    'attributes' => [
                        'amount' => $controls['amount'],
                    ]

                ]
				
/*				
		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		*/
	

];




/* @var $this yii\web\View */
/* @var $model backend\models\AuctionRate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auction-rate-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => AuctionRate::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
