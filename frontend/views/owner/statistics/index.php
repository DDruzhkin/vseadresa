
<?php
use yii\data\SqlDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use kartik\grid\GridView;
use kartik\icons\Icon;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;

use common\models\Entity;
use common\models\EntityHelper;
use frontend\models\Document;
use frontend\models\Order;
use frontend\models\User;


$this->title = 'Статистика';
$this->params['breadcrumbs'] =
    [

        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/owner'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,
        ],

    ];

?>

<style>
table.stat {
	/*font-size: 16pt;	*/
}
table.stat th{
    text-align: center;
	padding: 5px 15px;
}

</style>

<h1 class="statistics-filter">Статистика</h1>
<div class="grid-view">
<table class='stat table  price-table counts-table' >
    <thead>
<tr>
<th class='cross'></th><th>по дням</td><th>по месяцам</th>
</tr>
</thead>
<tbody> 
<tr>
<td class="helper-td">по адресам</td>
<td align="center"><?=Html::a(Icon::show('file-excel-o'), Url::to(['/owner/address/get-stat/day/address']))?></td>
<td align="center"><?=Html::a(Icon::show('file-excel-o'), Url::to(['/owner/address/get-stat/month/address']))?></td>
</tr>
<tr>
<td class="helper-td">по заказчикам</td>
<td align="center"><?=Html::a(Icon::show('file-excel-o'), Url::to(['/owner/address/get-stat/day/customer']))?></td>
<td align="center"><?=Html::a(Icon::show('file-excel-o'), Url::to(['/owner/address/get-stat/month/customer']))?></td>
</tr>
</tbody>  
</table>
</div>


