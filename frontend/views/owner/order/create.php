<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Order */

$this->title = 'Добавление Заказа';
$this->params['breadcrumbs'][] = ['label' => 'Личный кабинет', 'url' => ['/owner'],'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL];
$this->params['breadcrumbs'][] = ['label' => \backend\models\Order::getMeta('','title'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
