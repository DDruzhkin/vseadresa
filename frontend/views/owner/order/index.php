<?php




/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\models\Order;
use common\models\Entity;
use frontend\models\OrderSearch;
use frontend\models\Status;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Inflector;


$this->title = 'Заказы';
$this->params['breadcrumbs'] =
    [

        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/owner'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,
        ],

    ];
?>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	
	<?php 		
		$dataProvider -> query -> andFilterWhere(['owner_id'=>Entity::currentUser(), 'address_id' => NULL]);		
	?>
<h1><?= $this -> title?></h1>
    <div class="row order-filter">

        <?=\frontend\components\FilterWindow::getFilterButton($searchModel)?>
	</div>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'summary' => false,

		'tableOptions' => [
			'class' => 'table price-table counts-table has-sort',
        ],
        
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					$order = Order::findOne($data['id']);
					if (!$order) return '';
					
					$items = $order -> getOwnerMenuItems();
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
                                'label' => Icon::show('bars'), 'url' => '#',
                                'items' => $items,

                            ],
						],
					]);
				}					
			],


            
            ['attribute' => 'number',
				'label' => Order::getMeta('number', 'label'),
				'contentOptions' => ['class' => 'col-number '],
				'filterOptions' => ['class' => 'col-number'],
				'headerOptions' => ['class' => 'col-number '],				
				'footerOptions' => ['class' => 'col-number '],
                'format' => 'html',
                'value' => function($data){
			        return "<a class='order-rel' href='".Url::toRoute(['/owner/order/view', 'id' => $data['id']])."'>".$data['number']."</a>";
                },
				
				
			],
			['attribute' => 'created_at',
				'label' => Order::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at '],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at '],				
				'footerOptions' => ['class' => 'col-created_at '],
				'value' => function($data) {
					if ($data['created_at']) {
						return Yii::$app -> formatter -> asDateTime($data['created_at']);
					} else return '';
				}
				
				
			],
			
			
            ['attribute' => 'date_fin',
                'label' => Order::getMeta('date_fin', 'label'),
                'contentOptions' => ['class' => 'col-date_fin '],
                'filterOptions' => ['class' => 'col-date_fin'],
                'headerOptions' => ['class' => 'col-date_fin '],
                'footerOptions' => ['class' => 'col-date_fin '],

            ],			
			
            ['attribute' => 'status_id',
				'label' => Order::getMeta('status_id', 'label'),
				'contentOptions' => ['class' => 'col-status_id '],
				'filterOptions' => ['class' => 'col-status_id'],
				'headerOptions' => ['class' => 'col-status_id '],				
				'footerOptions' => ['class' => 'col-status_id '],
				'filter' => Order::getExistsValues('status_id'),	
				'format' => "html",
				'value' => function($data){ 
					
				//	return "<a class='customet-rel' href='".Url::toRoute(['/user/view', 'id' => $data['customer_id']])."'>".$data['status_id_view']."</a>";
				
					$status = Status::findOne([$data['status_id']]) -> name;
					return "<a class='customet-rel' href='".Url::toRoute(['/user/view', 'id' => $data['customer_id']])."'>".$status."</a>";
				},
				
			],
            
            ['attribute' => 'summa',
				'label' => Order::getMeta('summa', 'label'),
				'contentOptions' => ['class' => 'col-summa '],
				'filterOptions' => ['class' => 'col-summa'],
				'headerOptions' => ['class' => 'col-summa '],				
				'footerOptions' => ['class' => 'col-summa '],
				'value' => function($data) {
					if ($data['summa']) {
						return Yii::$app -> formatter -> asCurrency($data['summa']);
					} else return '';
				}
							
			]/*,
            ['attribute' => 'owner_id',
                'label' => "Заказчик",
                'contentOptions' => ['class' => 'col-created_at '],
                'filterOptions' => ['class' => 'col-created_at'],
                'headerOptions' => ['class' => 'col-created_at '],
                'footerOptions' => ['class' => 'col-created_at '],
                'format' => 'html',
                'value' => function($data){ return "<a class='customet-rel' href='".Url::toRoute(['/user/view', 'id' => $data['customer_id']])."'>".\frontend\models\User::findOne($data["customer_id"])->username."</a>";},


            ],*/


//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

	<?= $this->render('_filter', [
			'searchModel' => $searchModel,
	]) ?>
