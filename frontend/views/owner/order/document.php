<?php
/**
 * Created by PhpStorm.
 * User: FS04
 * Date: 4/9/2018
 * Time: 11:33 AM
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use frontend\models\Document;


$this->title = 'Cчета по заказу №'.$model -> number;
$this->params['breadcrumbs'] =
    [

        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/owner'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
		
		[
            'label'     => 'Заказы',
            'url'       =>  ['/owner/order'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
		
		
        [
            'label' => $this -> title,
        ],

    ];
$i = 0;
?>
<h1><?=$this->title?></h1>
<?php if ($documents): ?>
    <div class="row">
        <div class="row">
            <div class="col-md-11 table-cnt">
                <div class="table-self">
                    <table class="table price-table counts-table">
                        <thead>
                        <th></th>
                        <th><?= Document::getMeta('number', 'label') ?></th>
<!--                        <th><?= Document::getMeta('doc_type_id', 'label') ?></th> -->
						<th><?= Document::getMeta('info', 'label') ?></th>
                        <th><?= Document::getMeta('summa', 'label') ?></th>
                        <th width="80px" align="right"></th>
                        </thead>
                        <tbody>
                        <?php foreach ($documents as $document):
                            ?>
                            <tr>
                                <td><?= ++$i ?></td>
                                <td class="document-link"> 
								<!--<?= Html::a($document->number, Url::to(['owner/document/view/'.$document -> id])) ?> -->
								<?=$document->number?>
								</td>
<!--                                <td><?= $document->doc_type_id_view ?></td> -->
                                <td><?= $document->info?></td>
                                <td><?= Yii::$app -> formatter -> asCurrency($document->summa) ?> </td>

                                <td>
								<?= Html::a('<i class="fa fa-download""></i>', ['document/show-pdf', 'id' => $document->id], ['target' => '_blank', 'title' => 'Скачать PDF']) ?>
								
								</td>
							
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="hr">
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>

