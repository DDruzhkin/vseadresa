<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;



$this->params['breadcrumbs'] = $breadcrumbs;


?>

<section class="addresses">
    <div class="container-fluid cloud-bg">

        <div class="container">

            <div class="row">
                <div class="col-md-3 add-search-block">
                    <div class="row widget-filter">
                        <div class="col-md-12">
                            <div class="addresses-promo-block">
                                <div class="col-md-12 promo-information">
                                    <div class="pi-container">
                                        <div class="icon">
                                            <?= Html::img('@web/img/diamond.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                        </div>
                                        <div class="pi-txt">
                                            надежность
                                        </div>
                                        <div class="pi-message">
                                            все адреса  проверены экспертами
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 promo-information">
                                    <div class="pi-container">
                                        <div class="icon">
                                            <?= Html::img('@web/img/banknote.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                        </div>
                                        <div class="pi-txt">
                                            реальная цена!
                                        </div>
                                        <div class="pi-message">
                                            многие собственники предоставили  <br> эксклюзивные скидки для продаж
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 promo-information">
                                    <div class="pi-container">
                                        <div class="icon">
                                            <?= Html::img('@web/img/like.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                        </div>
                                        <div class="pi-txt">
                                            чистота сделок
                                        </div>
                                        <div class="pi-message">
                                            и максимальная скорость
                                            платежей <br> гарантированы
                                            администрацией портала
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-8 col-lg-offset-1 col-md-8 col-md-offset-0 col-sm-12 all-addresses-block">
                    <div class="col-lg-12 bc nop-left">
                        <?= Breadcrumbs::widget([
                            'homeLink'      =>  [
                                'label'     => 'Главная',
                                'url'       =>  ['/'],
                                'class'     =>  'home',
                                'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
                            ],
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]) ?>
                        <?php // Alert::widget();?>
                    </div>
                    <div class="article-item">
                        <div class="col-md-11">
                            <h1 class="article-header">
                                <?=Yii::$app->controller -> h1?>
                            </h1>
                        </div>
                        <div class="col-md-1">

                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="article-item">
                        <div class="col-md-11 col-xs-13">
                            <?php foreach($model -> getRubrics() as $rubruc):?>
                                <div class="article-line">
                                    <a href="<?=$rubruc -> getUrl()?>"><?=$rubruc -> h1?></a>
                                </div>
                            <?php endforeach?>


                        </div>

                    </div>
                    <div class="clear"></div>
                    <div class="article-item">
                        <div class="col-md-11">
                            <?php foreach($model -> getPosts() as $post):?>
                                <div class="article-line">
                                    <a href="<?=$post -> getUrl()?>"><?=$post -> h1?></a>
                                </div>
                            <?php endforeach?>


                        </div>

                    </div>
                    <div class="clear"></div>
                    <div class="article">
                        <div class="row">
                            <hr class="article-hr" />
                        </div>
                    </div>
                    <div class="articles-widget">
                        <div class="col-md-12 pull-right">
                            <div class="row">
                                <div class="">

                                    <?=\frontend\components\SeoTextWidget::widget([
                                        'options' => ["class" => "col-md-4 promo-text-block"]
                                    ]);?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>










            </div>
        </div>
    </div>
</section>


