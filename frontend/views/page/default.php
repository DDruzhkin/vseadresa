<?php
use app\components\ArticlesWidget;
use frontend\models\Page;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

$this->params['breadcrumbs'] = $breadcrumbs;

?>


<section class="addresses">

    <div class="container-fluid cloud-bg">

        <div class="container">

                <div class="row">
                    <div class="article">
                        <div class="col-md-3 col-sm-12 add-search-block">
                                <div class="row widget-filter">
                                    <div class="col-md-12">
                                        <div class="addresses-promo-block">
                                            <div class="col-md-12 promo-information">
                                                <div class="pi-container">
                                                    <div class="icon">
                                                        <?= Html::img('@web/img/diamond.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                                    </div>
                                                    <div class="pi-txt">
                                                        надежность
                                                    </div>
                                                    <div class="pi-message">
                                                        все адреса  проверены экспертами
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 promo-information">
                                                <div class="pi-container">
                                                    <div class="icon">
                                                        <?= Html::img('@web/img/banknote.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                                    </div>
                                                    <div class="pi-txt">
                                                        реальная цена!
                                                    </div>
                                                    <div class="pi-message">
                                                        многие собственники предоставили  <br> эксклюзивные скидки для продаж
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 promo-information">
                                                <div class="pi-container">
                                                    <div class="icon">
                                                        <?= Html::img('@web/img/like.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                                    </div>
                                                    <div class="pi-txt">
                                                        чистота сделок
                                                    </div>
                                                    <div class="pi-message">
                                                        и максимальная скорость
                                                        платежей <br> гарантированы
                                                        администрацией портала
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                        </div>
                        <div class="col-lg-8 col-lg-offset-1 col-md-8 col-md-offset-0 col-sm-12 col-sm-offset-0">
                            <div class="col-lg-12 bc nop-left">
                                <?= Breadcrumbs::widget([
                                    'homeLink'      =>  [
                                        'label'     => 'Главная',
                                        'url'       =>  ['/'],
                                        'class'     =>  'home',
                                        'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
                                    ],
                                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                ]) ?>
                                <?php // Alert::widget();?>
                            </div>
                            <div class="col-md-12">
                                <div class="text-with-white">
                                    <div class="text-contents">
                            <div class="article-item">

                                    <h1 class="article-header">
                                        <?=Yii::$app->controller -> h1?>
                                    </h1>

                                <div class="" >
                                    <img src="/img/reload.png" class="pull-right articles-reload" alt="">
                                </div>
                            </div>
							<?php if (($model -> type == Page::TYPE_POST) && $model -> created_at):?>
                            <div class="article-date article-item">
                                    <?=$model -> created_at?>
                            </div>
							<?php endif?>

                            <div class="article-text article-item">
                                <?=$model -> content?>
                            </div>
                            <div class="soc-block">
                                    <div class="article">
                                            <div class="share-soc">
                                                <a class="socs al">
                                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                                </a>
                                                <a class="socs al">
                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                </a>
                                                <a class="socs al">
                                                    <i class="fa fa-vk" aria-hidden="true"></i>
                                                </a>
                                                <a class="socs al">
                                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                                </a>
                                                <a class="socs al">
                                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                    </div>
                            </div>
                            <div class="clear"></div>
                            <div class="soc-block">
                                <div class="col-md-12">
                                    <div class="article">
                                        <?php if(isset($_SERVER["HTTP_REFERER"])): ?>
                                            <a href="<?=$_SERVER["HTTP_REFERER"];?>">
                                                вернуться назад
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>

                            <div class="articles-widget">
                                <div class="col-md-12 pull-right">
                                    <div class="row">
                                        <div class="">

                                        <?=\frontend\components\SeoTextWidget::widget([
											'options' => ["class" => "col-md-4 promo-text-block"]
										]);?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>







                </div>
        </div>
    </div>
</section>



