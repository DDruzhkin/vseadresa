<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\models\User;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>


    <title><?= Yii::$app->controller->title ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>

<?php if (User::portal()->getOptions('top.banner.show')): ?>
    <div class="container-fluid promo-block">
        <div class="container">
            <div class="promo-text">
                <?= User::portal()->getOptions('top.banner.value') ?>
            </div>
        </div>
    </div>
<?php endif ?>


<section class="white-bg">


    <div class="container-fluid menu-block mobile-header">
        <div class="container">
            <div class="mobile-navigation-menu">
                <div class="close-menu"> <!-- burger (only mobile) -->
                    <img src="/img/close-menu.png" alt="">

                </div>
            </div>
            <div class="row username-part-menu">
                <div class="col-lg-3 col-lg-offset-9 col-md-4 col-md-offset-8 col-sm-12 col-sm-offset-0">
                    <div class="username-field">
                        <?php if (!Yii::$app->user->isGuest) {
                            //echo Yii::$app->user->identity->fullname;

                            if (Yii::$app->user->identity->role == \common\models\User::attrLists()['role']['Заказчик']) {
                                echo Html::a(Yii::$app->user->identity->fullname, ['/customer']);
                            } elseif (Yii::$app->user->identity->role == \common\models\User::attrLists()['role']['Владелец']) {
                                echo Html::a(Yii::$app->user->identity->fullname, ['/owner']);
                            }
                        } ?>

                    </div>


                </div>
            </div>
            <div class="row upper-menu-part ">

                <div class="mobile-top-menu col-xs-12"> <!-- only mobile block  -->
                    <div class="burger-menu visible-xs"> <!-- burger (only mobile) -->
                        <img src="/img/burger-menu.png" alt="">
                    </div>
                    <div class="phone-email"> <!-- contacts (only mobile) -->
                        <!-- <div class="phone">
                             <div>+7 (499) 713-0194</div>
                             <span class="multichannel">многоканальный</span>
                         </div>-->

                        <div class="email">
                            info@vseadresa.ru
                        </div>
                    </div>
                    <div class=""> <!-- Auth status (only mobile) -->
                        <div class="person">
                            <!--<img src="/img/person.png" alt="person" class="open-form fancybox"  href="#login" >-->
                            <?php if (Yii::$app->user->identity != null): ?>
                                <div>
                                    <a href="<?= Url::to(['/profile/']) ?>">
                                        <img src="/img/person.png" alt="person" class="open-form">
                                    </a>
                                </div>
                                <div class="logout-icon-div">
                                    <a href="<?= Url::to(["/logout"]) ?>">
                                        <img src="/img/logout.png" alt="logout" class="open-form logout-icon">
                                    </a>
                                </div>
                            <?php else: ?>
                                <a title="Вход в систему" href="<?= Url::to(["/login"]) ?>">
                                    <img src="/img/locked.png" alt="person" class="open-form">
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>


                </div>
                <div class="mobile-logotype col-xs-12 visible-xs"> <!-- logotype (only mobile) -->
                    <div class="header-padding-left">
                        <a href="<?= Url::to(["/"]); ?>">
                            <div class="logo">
                                <span class="main-page-link">vseadresa</span><span class="pro">.pro</span>
                            </div>
                        </a>
                        <div class="under-logo-text">
                            портал юридических адресов
                        </div>

                    </div>
                </div>
                <div class="col-md-3 col-lg-4 hidden-xs"> <!-- logo (desktop) -->
                    <div class="header-padding-left">
                        <a href="<?= Url::to(["/"]); ?>">
                            <div class="logo">
                                <span class="main-page-link">vseadresa</span><span class="pro">.pro</span>
                            </div>
                        </a>
                        <div class="under-logo-text">
                            портал юридических адресов
                        </div>

                    </div>
                </div>
                <div class="col-md-6 col-lg-5 no-padding-menu">
                    <!-- <div class="navbar-header">
                         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false" aria-controls="navbar">
                             <span class="sr-only">Toggle navigation</span>
                             <span class="icon-bar"></span>
                             <span class="icon-bar"></span>
                             <span class="icon-bar"></span>
                         </button>
                     </div>-->

                    <nav class="navbar-collapse collapse main-menu">
                        <div class="">

                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a class="dropdown-toggle custom-dropdown" data-toggle="dropdown" href="#">адреса
                                    </a>
                                    <?= \frontend\components\OptionLinksWidget::widget(['options' => ['class' => "dropdown-menu address-li"]]) ?>
                                </li>
                                <?= \frontend\components\MenuWidget::widget(['menu' => 'top', 'options' => ['ul' => "none"]]) ?>


                            </ul>
                        </div>
                    </nav>
                    <div class="phone-email hidden-xs">
                        <!-- <div class="phone">
                             +7 (499) 713-0194 <span class="multichannel">многоканальный</span>
                         </div>-->

                        <div class="email">
                            <a href="mailto:info@vseadresa.ru">info@vseadresa.ru</a>
                        </div>
                    </div>

                </div>
                <div class="col-md-3 col-lg-3 hidden-xs">

                    <div class="publish-address
                        <?php if (Yii::$app->user->identity == null): ?>
                            fix-padding
                        <?php endif; ?>
                        ">
                        <div class="public-address">
                            <?= Html::a('опубликовать <br> адрес', ['/owner/address/create'], ['class' => 'public-address-button']) ?>

                        </div>


                        <div class="person hidden-xs">


                            <!--<img src="/img/person.png" alt="person" class="open-form"  >-->

                            <?php if (Yii::$app->user->identity != null): ?>
                                <div class="person-div">
                                    <a href="<?= Url::to(['/profile/']) ?>">
                                        <img src="/img/person.png" alt="person" class="open-form">
                                    </a>
                                </div>
                                <div class="logout-icon-div">
                                    <a href="<?= Url::to(["/logout"]) ?>">
                                        <img src="/img/logout.png" alt="logout" class="open-form logout-icon">
                                    </a>
                                </div>
                            <?php else: ?>
                                <a title="Вход в систему" href="<?= Url::to(["/login"]) ?>">
                                    <img src="/img/locked.png" alt="person" class="open-form">
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>


                </div>
            </div>
            <?php //var_dump(Yii::$app->params['page']); ?>
            <?php if (isset(Yii::$app->params['page']) && Yii::$app->params['page'] == 'main'): ?>

                <div class="bottom-part">
                    <div class="col-md-4 hidden-xs">
                        <div class="row">
                            <div class="header-label">
                                самая большая площадка в интернете <br>
                                по сделкам с юридическими адресами
                            </div>
                        </div>
                    </div>
                    <div class="row visible-xs">
                        <div class="col-md-4 visible-xs">
                            <div class="header-label">
                                самая большая площадка в интернете <br>
                                по сделкам с юридическими адресами
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 hidden-xs no-padding-menu">
                        <h1 class="pick-address customize-header">
                            <?= Yii::$app->controller->h1 ?>
                            <!--выберите нужный вам адрес:-->

                        </h1>
                        <?= Alert::widget(); ?>

                    </div>
                    <div class="col-xs-12 visible-xs">
                        <div class="publish-address">
                            <div class="public-address">
                                <?= Html::a('опубликовать <br> адрес', ['/owner/address/create'], ['class' => 'public-address-button']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row visible-xs">
                        <h1 class="pick-address customize-header">
                            <?= Yii::$app->controller->h1 ?>
                            <!--выберите нужный вам адрес:-->
                        </h1>
                        <?= Alert::widget(); ?>
                    </div>


                </div>
            <?php else: ?>
                <div class="bottom-part">
                    <div class="row visible-xs">
                        <div class="col-md-4 visible-xs">
                            <div class="header-label">
                                самая большая площадка в интернете <br>
                                по сделкам с юридическими адресами
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 visible-xs">
                        <div class="publish-address">
                            <div class="public-address">
                                <?= Html::a('опубликовать <br> адрес', ['/owner/address/create'], ['class' => 'public-address-button']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>


    </div>

    <?php if (isset(Yii::$app->params['page']) && Yii::$app->params['page'] == 'main'): ?>
    <div class="wrap">
        <?php else: ?>
        <div class="pwrap">
            <?php endif; ?>



            <?= $content ?>
        </div>
</section>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="footer-first row">
                    <div class="col-md-3 col-xs-3">
                        <div class="year pull-left">

                            &copy;2017-<?= date('Y') ?>
                        </div>


                    </div>
                    <div class="col-md-9 col-xs-9 visible-xs">
                        <div class="mobile-logo-cnt">
                            <div class="footer-logo">
                                <div class="website footer-l">
                                    vseadresa.pro
                                </div>
                                <div class="website law-address">
                                    портал юридических адресов
                                </div>
                            </div>
                            <div class="">
                                <div class="dis-block">
                                    <div class="fntw">
                                        <?= Html::img('@web/img/fntw.png', ['alt' => 'fntw']); ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-xs-8 hidden-xs">
                        <div class="footer-logo pull-left">
                            <div class="website footer-l">
                                vseadresa.pro
                            </div>
                            <div class="website law-address">
                                портал юридических адресов
                            </div>
                        </div>
                    </div>

                </div>
                <div class="footer-second row visible-xs">
                    <div class="col-xs-10 col-xs-offset-1">
                        <div class="col-md-3 col-xs-2">
                            <div class="year pull-left">
                                <i class="fa fa-map-marker location" aria-hidden="true"></i>
                            </div>

                        </div>
                        <div class="col-md-9 col-xs-10">
                            <!--
                            <div class="footer-logo address-mobile pull-left">
                                <div class="website">


                                </div>
                                <div class="website">

                                </div>
                            </div>
                            -->
                        </div>

                    </div>
                </div>
                <?php /*
                    <div class="footer-second row hidden-xs">

                        <div class="col-md-3 col-xs-3">
                            <div class="year pull-left">
                                <i class="fa fa-map-marker location" aria-hidden="true"></i>
                            </div>

                        </div>
                        <div class="col-md-9 col-xs-6">
                            <div class="footer-logo pull-left">
                                <div class="website">
                                    105066, ул. Спартаковская, д. 19, стр. 3А

                                </div>
                               <!-- <div class="website">
                                    +7 (499) 713-0194
                                </div>-->
                            </div>
                        </div>


                    </div>
 */
                ?>
                <div class="row mobile-footer-soc visible-xs">
                    <div class="col-xs-10 col-xs-offset-1">
                        <div class="col-xs-10 col-xs-offset-2">
                            <div class="soc">
                                <a href="">
                                    <?= Html::img('@web/img/fb.png', ['alt' => 'facebook']); ?>
                                </a>
                                <a href="">
                                    <?= Html::img('@web/img/tw.png', ['alt' => 'twitter']); ?>
                                </a>
                                <a href="">
                                    <?= Html::img('@web/img/env.png', ['alt' => 'envelope']); ?>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-third row hidden-xs">
                    <div class="col-md-3">
                        <div class="year pull-left">
                            <i class="fa fa-star-o star" aria-hidden="true"></i>
                        </div>

                    </div>
                    <div class="col-md-9">
                        <?= \frontend\components\MenuWidget::widget(['menu' => 'conditions', 'options' => ['class' => "footer-logo pull-left"]]) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-7 col-xs-10 col-xs-offset-2 col-md-offset-0 col-sm-offset-0 footer-menu-block">
                <div class="col-md-4 col-sm-4 col-xs-7">
                    <div class="menu1">
                        <div class="address">

                        </div>

                        <?= \frontend\components\OptionLinksWidget::widget(['options' => ['class' => "bottom-menu"]]) ?>

                        <div class="soc hidden-xs">
                            <a href="">
                                <?= Html::img('@web/img/fb.png', ['alt' => 'facebook']); ?>
                            </a>
                            <a href="">
                                <?= Html::img('@web/img/tw.png', ['alt' => 'twitter']); ?>
                            </a>
                            <a href="">
                                <?= Html::img('@web/img/env.png', ['alt' => 'envelope']); ?>
                            </a>

                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-5">
                    <div class="menu2">
                        <?= \frontend\components\MenuWidget::widget(['menu' => 'main', 'options' => ['class' => "bottom-menu"]]) ?>

                    </div>
                </div>
                <div class="col-md-3 col-md-offset-1 col-sm-offset-0 col-sm-4 hidden-xs">
                    <div class="dis-block">
                        <div class="fntw">
                            <?= Html::img('@web/img/fntw.png', ['alt' => 'fntw']); ?>
                        </div>
                        <div class="fntw-text">
                            дизайн сайта <br>
                            студия fntw.ru
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-third visible-xs">
                <div class="mobile-confidential">
                    <div class="col-xs-10 col-xs-offset-1 cnf-container">
                        <div class="col-md-3 col-xs-2">
                            <div class="year pull-left">
                                <i class="fa fa-star-o star" aria-hidden="true"></i>
                            </div>

                        </div>
                        <div class="col-md-9 col-xs-10 cnf">
                            <?= \frontend\components\MenuWidget::widget(['menu' => 'conditions', 'options' => ['class' => "footer-logo pull-left"]]) ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header' => '',
    'id' => 'load-modal',
    'options' => ['class' => 'fade-scale'],
]);
?>
<div id="modal-content">

    <?php
    if (!empty($this->params['modal_view'])) {
        echo $this->render($this->params['modal_view']);
    }
    ?>
</div>
<?php
Modal::end();
?>


<?php $this->endBody() ?>
<?php //if (!User::currentUser()): ?>
<!-- counters -->
<?= (User::portal()->getOptions('common.seo.counters')) ?>
<!-- /counters -->
<?php //endif?>
<!— BEGIN JIVOSITE CODE {literal} —>
<script type="text/javascript">
    $(document).ready(function () {
            var widget_id = "8Ohpxbc3li";
            var d = document;
            var w = window;

            function l() {
                var s = document.createElement("script");
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://code.jivosite.com/script/widget/" + widget_id;
                var ss = document.getElementsByTagName("script")[0];
                ss.parentNode.insertBefore(s, ss);
            }

            if (d.readyState == "complete") {
                l();
            } else {
                if (w.attachEvent) {
                    w.attachEvent("onload", l);
                } else {
                    w.addEventListener("load", l, false);
                }
            }
        }
    )
</script>
<!— {/literal} END JIVOSITE CODE —>
</body>

</html>
<?php $this->endPage() ?>
