<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php $this->beginContent('@frontend/views/layouts/main.php'); ?>
<div class="container-fluid popup order-address-popup order-step order-page-one cloud-bg">

    <div class="container">
        <div >            
            <div class="row order-logo">
                <div class="col-md-4">
                    <div class="header-padding-left">
                        <div class="title">
                            <h1><?= Yii::$app->controller-> h1?></h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="flex">
                        <div class="popup-page">
                           
                        </div>

                    </div>

                </div>
            </div>
		<div class="row">
            <div class="col-md-3 col-lg-3">
                <ul class="error-fix-variant">
                    <li><?=Html::a('Зарегистрироваться как Заказчик', Url::to(['/customer/signup']))?></li>
                    <li><?=Html::a('Зарегистрироваться как Владелец', Url::to(['/owner/signup']))?></li>
                </ul>
            </div>
             <div class="col-md-offset-1 col-md-6">
                 <div class="col-md-12">
					<?=$content?>
                 </div>
			</div>
		</div>
		
	</div>
</div>

<?php $this->endContent(); ?>