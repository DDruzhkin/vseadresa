<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\Search_promoWidget;
use frontend\models\Address;
    use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

$currentPage = 1; $totalPages = 10;
?>
<?php $this->beginContent('@frontend/views/layouts/main.php');

?>
<?php if(isset(Yii::$app->params['page']) &&  (Yii::$app->params['page'] == 'main' || Yii::$app->params['page'] = 'price')): ?>
    <?=$content?>
<?php else: ?>
    <section class="mobile-address-search">
        <span>поиск адреса</span>
    </section>
    <section class="addresses">
        <div class="container">
            <div class="col-lg-offset-4 col-lg-8 col-md-offset-3 col-md-8 bc">
                <?= Breadcrumbs::widget([
                    'homeLink'      =>  [
                        'label'     => 'Главная',
                        'url'       =>  ['/'],
                        'class'     =>  'home',
                        'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget();?>
            </div>
        </div>
        <div class="address-search-popup">
            <div class="search-closer"><img src="/img/close-menu.png" alt=""></div>
            <div class="container-fluid">
                <div class="container container-to-insert">

                </div>
            </div>

        </div>
        <div class="container-fluid cloud-bg">

        <?=$content?>


        </div>

    </section>
<?php endif; ?>
<?php $this->endContent(); ?>