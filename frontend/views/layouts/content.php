<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
?>
<?php $this->beginContent('@frontend/views/layouts/main.php'); ?>
<div class="container-fluid popup order-address-popup order-step order-page-one">

    <div class="container">
        <div >            
            <div class="row order-logo">
                <div class="col-md-4">
                    <div class="header-padding-left">
                        <div class="title">
                            <h1><?= Yii::$app->controller-> h1?></h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="flex">
                        <div class="popup-page">
                           
                        </div>

                    </div>

                </div>
            </div>
		<div class="row">
                <div class="col-md-6 col-md-offset-3">
                <div class="col-lg-8 col-md-8 bc">
                    <?= Breadcrumbs::widget([
                        'homeLink'      =>  [
                            'label'     => 'Главная',
                            'url'       =>  ['/'],
                            'class'     =>  'home',
                            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
                        ],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= Alert::widget();?>
                </div>
<?=$content?>

			</div>
		</div>
		
	</div>
</div>

<?php $this->endContent(); ?>