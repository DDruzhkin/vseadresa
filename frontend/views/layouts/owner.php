<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use frontend\models\User;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
?>
<?php $this->beginContent('@frontend/views/layouts/main.php'); ?>
<section class="cabinet-section">
    <div class="container-fluid cloud-bg">

        <div class="container">
            <div class="row">
                <div class="col-md-2 col-lg-3 add-search-block">
                    <div class="cab-menu-block">
                        <?php
						$user = User::getCurrentUser();
                        $links = [
                                Url::to(['/owner/user']) => 'профиль',
                        ];
						if ($user -> active) {
							$links = ArrayHelper::merge($links, [
								Url::to(['/notification']) => 'уведомления',
                                Url::to(['/owner/address']) => 'адреса',
                                Url::to(['/owner/service-price']) => 'услуги',
								Url::to(['/owner/payconfirm']) => 'подтверждение б/н оплаты',
                                Url::to(['/owner/order']) => 'заказы',
								Url::to(['/owner/statistics']) => 'статистика',
                                Url::to(['/owner/settlements']) => 'расчеты по адресам',
							]);
						}
						
						$links[Url::to(['/owner/psettlements'])] = 'расчеты по услугам портала';
						if ($user -> hasPubleshedAddress()) {
							$links = ArrayHelper::merge($links, [
                                Url::to(['/owner/auction']) => 'аукцион',
							]);
						}
						
						

                        if (count($links)) {
                            foreach ($links as $link => $label) {
                                echo Html::a($label, $link,
                                    ['class' => 'cab-menu-item']);
                            }
                        }

                        ?>

                    </div>


                </div>
                <div class="col-md-9 col-lg-8 col-md-offset-1 all-addresses-block orders-search-form">
                    <div class="cabinet-breadcrumb">
                        <div class="col-lg-12 col-md-12 bc">
                            <?= Breadcrumbs::widget([
                                'homeLink'      =>  [
                                    'label'     => 'Главная',
                                    'url'       =>  ['/'],
                                    'class'     =>  'home',
                                    'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
                                ],
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            ]) ?>
                            <?= Alert::widget();?>
                        </div>
                    </div>
                    <div class="col-md-12 no-pads-mobile">
                        <div class="cabinet-form">
<?=$content?>

						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php $this->endContent(); ?>