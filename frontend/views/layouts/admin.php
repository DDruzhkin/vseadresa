<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\models\User;
use common\widgets\Alert;
use kartik\nav\NavX;

AppAsset::register($this);
$this->registerCssFile('/css/admin.css');


       $menuItems = [
	[
	
		'label' => 'Управление',
		'items' => [

			[
				'label' => 'Порталом', 
				'items' => [					
					['label' => 'Справочник услуг', 'url' => Url::to(['/admin/service-portal/'])],
					['label' => 'Реквизиты', 'url' => Url::to(['/admin/requisites/'])],					
					['label' => 'Параметры', 'url' => Url::to(['/admin/options/'])],					
					['label' => 'Статистика 404 ошибки', 'url' => Url::to(['/admin/err404/'])],					
					[
						'label' => 'Статистика по заказам',
						'items' => [					
							['label' => 'Владельцам', 'url' => Url::to(['/admin/stat/orders'])],
							['label' => 'Порталу', 'url' => Url::to(['/admin/stat/portal-orders'])],					
						]
					],					
//					['label' => 'Шаблоны писем', 'url' => Url::to(['/admin/email-template'])],					
				],
			],

			[
			'label' => 'Расчетами', 
				'items' => [					
					
					['label' => 'Входящие платежи - Список', 'url' => Url::to(['/admin/payment/inp'])],
					['label' => 'Входящие платежи - Импорт', 'url' => Url::to(['/admin/import/'])],
					['label' => 'Исходящие платежи - Список', 'url' => Url::to(['/admin/payment/out'])],					
					['label' => 'Исходящие платежи - Импорт', 'url' => Url::to(['/admin/import/out/'])],
					'<li class="divider"></li>',
					['label' => 'Платежи по эквайрингу за адреса', 'url' => Url::to(['/admin/payment/eq'])],
					
				],
			],
			
		
			[
				'label' => 'Контентом',
                'items' => [
                    ['label' => 'Отзывы', 'url' => Url::to(['/admin/review/'])],
					['label' => 'ЧаВо', 'url' => Url::to(['/admin/faq/'])],
					['label' => 'Страницы', 'url' => Url::to(['/admin/page/'])],
//					['label' => 'SEO', 'url' => Url::to(['/admin/seo/'])],
				],
			],
			


			

			[
				'label' => 'Аукционами', 
				'items' => [					
					['label' => 'Акционы', 'url' => Url::to(['/admin/auction'])],					
					['label' => 'Ставки', 'url' => Url::to(['/admin/auction-rate'])],					
				],
			],
			
			[
				'label' => 'Документами', 
				'items' => [
					['label' => 'Договоры', 	'url' => Url::to(['/admin/contract'])],
					['label' => 'Шаблоны документов', 	'url' => Url::to(['/admin/doc-template?name=agreement'])],
			
				]
			],
			
			[
				'label' => 'Уведомлениями', 
				'items' => [
					['label' => 'Настройка', 	'url' => Url::to(['/notification'])],
					['label' => 'Шаблоны', 	'url' => Url::to(['/admin/email-template'])],					
				]
				
			],

            [
                'label' => 'Заказами',
                'url' => Url::to(['/admin/order/'])
            ],

            [
                'label' => 'Доставкой',
                'url' => Url::to(['/admin/delivery/'])
            ],
			
			
			[
				'label' => 'Пользователями', 
				'url' => Url::to(['/admin/user/list'])
			],
			
			
			
			
			
			
			

		],
    ],
	
	
	
	[

                'label' => 'Адреса',
                'items' => [
					['label' => 'Список адресов', 'url' => Url::to(['/admin/address/'])],									
					[
						'label' => 'Фотограф', 
						'items' => [
							['label' => 'Список визитов','url' => Url::to(['/admin/photo-calendar/'])],
							['label' => 'Управление расписанием','url' => Url::to(['/admin/photo-calendar/manage'])],
						],
					],
					'<li class="divider"></li>',
					['label' => 'Налоговые инспекции', 'url' => Url::to(['/admin/nalog/'])],				
                    ['label' => 'Округи', 'url' => Url::to(['/admin/okrug/'])],
                    ['label' => 'Районы', 'url' => Url::to(['/admin/region/'])],
                    ['label' => 'Метро', 'url' => Url::to(['/admin/metro/'])],                    
					'<li class="divider"></li>',
//					['label' => 'Способы доставки',	'url' => Url::to(['/admin/delivery/'])],		
					['label' => 'Опции адресов',	'url' => Url::to(['/admin/option/'])],		
//					['label' => 'Типы документов', 'url' => Url::to(['/admin/document-type/'])],
					'<li class="divider"></li>',
					['label' => 'Удаленные адреса', 'url' => Url::to(['/admin/address/deleted'])],				

					
                ],

	],	
	
			
	[
                'label' => 'Услуги',
                'items' => [
					['label' => 'Услуги Владельцев', 'url' => Url::to(['/admin/service-name/'])],
					['label' => 'Услуги Портала', 'url' => Url::to(['/admin/service-portal/'])],
					['label' => 'Заказанные услуги Портала', 'url' => Url::to(['/admin/porder/'])],

					
                ],


    ],

		
	
	
	
	

	[

                'label' => 'Справочники',
                'items' => [					
					['label' => 'Налоговые инспекции', 'url' => Url::to(['/admin/nalog/'])],				
                    ['label' => 'Округи', 'url' => Url::to(['/admin/okrug/'])],
                    ['label' => 'Районы', 'url' => Url::to(['/admin/region/'])],
                    ['label' => 'Метро', 'url' => Url::to(['/admin/metro/'])],                    
					'<li class="divider"></li>',
//					['label' => 'Способы доставки',	'url' => Url::to(['/admin/delivery/'])],		
					['label' => 'Опции адресов',	'url' => Url::to(['/admin/option/'])],		
//					['label' => 'Типы документов', 'url' => Url::to(['/admin/document-type/'])],

					'<li class="divider"></li>',
					['label' => 'Услуги портала', 'url' => Url::to(['/admin/service-portal/'])],										
                    ['label' => 'Услуги владельцев', 'url' => Url::to(['/admin/service-name/'])],					
					
                ],

	],	
	

	];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>


	<title><?= Yii::$app->controller-> title?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <?php $this->head() ?>

</head>
<body id="admin">
<?php $this->beginBody() ?>


<div class="container-fluid promo-block">
    <div class="container">
        <div class="promo-text">
            Система администрирования
        </div>
    </div>
</div>




    <section class="white-bg">


        <div class="container-fluid menu-block mobile-header">
            <div class="container">
                <div class="mobile-navigation-menu">
                    <div class="close-menu"> <!-- burger (only mobile) -->
                        <img src="/img/close-menu.png" alt="">

                    </div>
                </div>
                <div class="row username-part-menu">
                    <div class="col-lg-3 col-lg-offset-9 col-md-4 col-md-offset-8 col-sm-12 col-sm-offset-0">
                        <div class="username-field">
                        <?php if(!Yii::$app->user->isGuest){

                            if (Yii::$app->user->identity->role == \common\models\User::attrLists()['role']['Администратор']){
                                //echo "<a href='/index.php/owner'>", Yii::$app->user->identity->fullname, "</a>";
								echo Html::a(Yii::$app->user->identity->fullname, ['/admin/profile']);
                            }
                        } ?>

                        </div>


                    </div>
                </div>
                <div class="row upper-menu-part ">

                    <div class="mobile-top-menu col-xs-12"> <!-- only mobile block  -->
                        <div class="burger-menu visible-xs"> <!-- burger (only mobile) -->
                            <img src="/img/burger-menu.png" alt="">
                        </div>

                        <div class=""> <!-- Auth status (only mobile) -->
                            <div class="person">
                                <!--<img src="/img/person.png" alt="person" class="open-form fancybox"  href="#login" >-->
                                <?php if(Yii::$app->user->identity != null): ?>
                                    <div>
                                         <a href="<?=Url::to(['/profile/'])?>">
                                        <img src="/img/person.png" alt="person" class="open-form" >
                                    </a>
                                    </div>
                                    <div class="logout-icon-div">
                                        <a href="<?=Url::to(["/logout"])?>">
                                            <img src="/img/logout.png" alt="logout" class="open-form logout-icon" >
                                        </a>
                                    </div>
                                <?php else:  ?>
                                    <a title="Вход в систему" href="<?=Url::to(["/login"])?>">
                                        <img src="/img/locked.png" alt="person" class="open-form" >
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>



                    </div>
                    <div class="mobile-logotype col-xs-12 visible-xs"> <!-- logotype (only mobile) -->
                        <div class="header-padding-left">
                            <a href="<?=Url::to(["/"]); ?>">
                                <div class="logo">
                                    <span class="main-page-link">vseadresa</span><span class="pro">.pro</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-4 hidden-xs"> <!-- logo (desktop) -->
                        <div class="header-padding-left">
                            <a href="<?=Url::to(["/"]); ?>">
                                <div class="logo">
                                    <span class="main-page-link">vseadresa</span><span class="pro">.pro</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-5 no-padding-menu">
                        <!-- <div class="navbar-header">
                             <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false" aria-controls="navbar">
                                 <span class="sr-only">Toggle navigation</span>
                                 <span class="icon-bar"></span>
                                 <span class="icon-bar"></span>
                                 <span class="icon-bar"></span>
                             </button>
                         </div>-->

						 	<?php
							echo NavX::widget([
								'options' => ['class' => 'nav nav-pills'],
								'items' => $menuItems,
								'encodeLabels' => false
								]);
							?>
						 
						 
                        


                    </div>
                    <div class="col-md-3 col-lg-3 hidden-xs">

                        <div class="publish-address
                        <?php if(Yii::$app->user->identity == null): ?>
                            fix-padding
                        <?php endif;?>
                        ">


                            <div class="person hidden-xs">



                                <!--<img src="/img/person.png" alt="person" class="open-form"  >-->

                                <?php if(Yii::$app->user->identity != null): ?>
                                    <div class="person-div">
                                        <a href="<?=Url::to(['/profile/'])?>">
                                        <img src="/img/person.png" alt="person" class="open-form" >
                                    </a>
                                    </div>
                                    <div class="logout-icon-div">
                                        <a href="<?=Url::to(["/logout"])?>">
                                            <img src="/img/logout.png" alt="logout" class="open-form logout-icon" >
                                        </a>
                                    </div>
                                <?php else:  ?>
                                    <a title="Вход в систему" href="<?=Url::to(["/login"])?>">
                                        <img src="/img/locked.png" alt="person" class="open-form" >
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>




                    </div>
                </div>
                <?php //var_dump(Yii::$app->params['page']); ?>
                <?php if(isset(Yii::$app->params['page']) && Yii::$app->params['page'] == 'main'): ?>

                    <div class="bottom-part">
                        <div class="col-md-4 hidden-xs">
                            <div class="row">
                                <div class="header-label">
                                    самая большая площадка в интернете <br>
                                    по сделкам с юридическими адресами
                                </div>
                            </div>
                        </div>
                        <div class="row visible-xs">
                            <div class="col-md-4 visible-xs">
                                <div class="header-label">
                                    самая большая площадка в интернете <br>
                                    по сделкам с юридическими адресами
                                </div>
                            </div>
                        </div>

                    </div>
                <?php else: ?>
                    <div class="bottom-part">
                        <div class="row visible-xs">
                            <div class="col-md-4 visible-xs">
                                <div class="header-label">
                                    самая большая площадка в интернете <br>
                                    по сделкам с юридическими адресами
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 visible-xs">
                            <div class="publish-address">
                                <div class="public-address">
                                    <?= Html::a('опубликовать <br> адрес', ['/owner/address/create'], ['class' => 'public-address-button']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>



        </div>

        <?php if(isset(Yii::$app->params['page']) && Yii::$app->params['page'] == 'main'): ?>
        <div class="wrap">
        <?php else:  ?>
            <div class="pwrap">
        <?php endif; ?>

		
		<div class="site-index">
			
			<div class="jumbotron">
			
			<div class="row">
			<?= Breadcrumbs::widget([
                        'homeLink'      =>  [
                            'label'     => 'Главная',
                            'url'       =>  ['/'],
                            'class'     =>  'home',
                            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
                        ],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= Alert::widget();?>
			</div>
			<h1><?=Yii::$app->controller->h1?></h1>
			
				<?= $content ?>
			</div>
		</div>
        </div>
    </section>
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="footer-first row">
                        <div class="col-md-3 col-xs-3">
                            <div class="year pull-left">

                                &copy;2017-<?=date('Y')?>
                            </div>


                        </div>
                        <div class="col-md-9 col-xs-9 visible-xs">
                            <div class="mobile-logo-cnt">
                                <div class="footer-logo">
                                    <div class="website footer-l">
                                        vseadresa.pro
                                    </div>
                                    <div class="website law-address">
                                        портал юридических адресов
                                    </div>
                                </div>
                                <div class="">
                                    <div class="dis-block">
                                        <div class="fntw">
                                            <?= Html::img('@web/img/fntw.png', ['alt'=>'fntw']);?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-xs-8 hidden-xs">
                            <div class="footer-logo pull-left">
                                <div class="website footer-l">
                                    vseadresa.pro
                                </div>
                                <div class="website law-address">
                                    портал юридических адресов
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="footer-second row visible-xs">
                        <div class="col-xs-10 col-xs-offset-1">
                            <div class="col-md-3 col-xs-2">
                                <div class="year pull-left">
                                    <i class="fa fa-map-marker location" aria-hidden="true"></i>
                                </div>

                            </div>
                            <div class="col-md-9 col-xs-10">
                                <div class="footer-logo address-mobile pull-left">
                                    <div class="website">
                                        129344, Москва, ул. Енисейская 1, стр.3

                                    </div>
                                    <div class="website">
                                        +7 (499) 713-0194
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="footer-second row hidden-xs">

                        <div class="col-md-3 col-xs-3">
                            <div class="year pull-left">
                                <i class="fa fa-map-marker location" aria-hidden="true"></i>
                            </div>

                        </div>
                        <div class="col-md-9 col-xs-6">
                            <div class="footer-logo pull-left">
                                <div class="website">
                                    105066, ул. Спартаковская, д. 19, стр. 3А

                                </div>
                               <!-- <div class="website">
                                    +7 (499) 713-0194
                                </div>-->
                            </div>
                        </div>


                    </div>
                    <div class="row mobile-footer-soc visible-xs">
                        <div class="col-xs-10 col-xs-offset-1">
                            <div class="col-xs-10 col-xs-offset-2">
                                <div class="soc">
                                    <a href="">
                                        <?= Html::img('@web/img/fb.png', ['alt'=>'facebook']);?>
                                    </a>
                                    <a href="">
                                        <?= Html::img('@web/img/tw.png', ['alt'=>'twitter']);?>
                                    </a>
                                    <a href="">
                                        <?= Html::img('@web/img/env.png', ['alt'=>'envelope']);?>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer-third row hidden-xs">
                        <div class="col-md-3">
                            <div class="year pull-left">
                                <i class="fa fa-star-o star" aria-hidden="true"></i>
                            </div>

                        </div>
                        <div class="col-md-9">
                            <?=\frontend\components\MenuWidget::widget(['menu' =>'conditions', 'options' => ['class'=>"footer-logo pull-left"]])?>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-xs-10 col-xs-offset-2 col-md-offset-0 col-sm-offset-0 footer-menu-block">
                    <div class="col-md-4 col-sm-4 col-xs-7">
                        <div class="menu1">
                            <div class="address">
                                адрес
                            </div>

							<?=\frontend\components\OptionLinksWidget::widget(['options' => ['class'=>"bottom-menu"]])?>

                            <div class="soc hidden-xs">
                                <a href="">
                                    <?= Html::img('@web/img/fb.png', ['alt'=>'facebook']);?>
                                </a>
                                <a href="">
                                    <?= Html::img('@web/img/tw.png', ['alt'=>'twitter']);?>
                                </a>
                                <a href="">
                                    <?= Html::img('@web/img/env.png', ['alt'=>'envelope']);?>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-5">
                        <div class="menu2">
							<?=\frontend\components\MenuWidget::widget(['menu' =>'main', 'options' => ['class'=>"bottom-menu"]])?>

                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-1 col-sm-offset-0 col-sm-4 hidden-xs">
                        <div class="dis-block">
                            <div class="fntw">
                                <?= Html::img('@web/img/fntw.png', ['alt'=>'fntw']);?>
                            </div>
                            <div class="fntw-text">
                                дизайн сайта <br>
                                студия fntw.ru
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-third visible-xs">
                    <div class="mobile-confidential">
                        <div class="col-xs-10 col-xs-offset-1 cnf-container">
                            <div class="col-md-3 col-xs-2">
                                <div class="year pull-left">
                                    <i class="fa fa-star-o star" aria-hidden="true"></i>
                                </div>

                            </div>
                            <div class="col-md-9 col-xs-10 cnf">
								<?=\frontend\components\MenuWidget::widget(['menu' =>'conditions', 'options' => ['class'=>"footer-logo pull-left"]])?>						
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php
Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'header' => '',
    'id' => 'load-modal',
    'options'=>['class'=>'fade-scale'],
]);
?>
<div  id="modal-content">

    <?php
    if(!empty($this->params['modal_view'])) {
        echo $this->render($this->params['modal_view']);
    }
    ?>
</div>
<?php
Modal::end();
?>


    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
