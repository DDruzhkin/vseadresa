<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

//$this->title = 'Вход';
//$this->params['breadcrumbs'][] = $this->title;
$this -> context -> title = $this->title;
?>
<?php $this->beginContent('@frontend/views/layouts/main.php');?>
<div class="container-fluid popup" id="login" >
    <div class="container">

        <!-- <div class="row login-logo">
            <div class="col-md-12">
                <div class="header-padding-left">
                    <div class="login-title">
                        авторизация:
                    </div>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-3 login-tutorial">
                <div class="flex column">
                    <div>
                        <a href="<?=Url::to(["/request-password-reset"])?>">восстановить <br> пароль</a>
                    </div>


                    <div>
                        <a href="<?=Url::to(["customer/signup"])?>" class="reg-popup">регистрация <br>
                            заказчика адреса</a>
                    </div>
                    <div>
                        <a href="<?=Url::to(["owner/signup"])?>" class="reg-popup">регистрация <br>
                            владельца адреса</a>
                    </div>
                </div>

            </div>

            <div class="col-md-offset-1 col-md-6">
                <div class="cabinet-breadcrumb">
                    <div class="col-lg-8 col-md-8 bc">
                        <?= Breadcrumbs::widget([
                            'homeLink'      =>  [
                                'label'     => 'Главная',
                                'url'       =>  ['/'],
                                'class'     =>  'home',
                                'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
                            ],
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]) ?>
                        <?= Alert::widget();?>
                    </div>
                </div>
                <div class="col-md-12">
                    <?=$content?>
                </div>

            </div>
        </div>
        <div class="row hidden">
            <div class="flex">
                <div class="success-action-block">
                    <div class="success-message">
                        авторизация <br>
                        прошла <br>
                        успешно!
                    </div>
                    <div class="thanks">
                        спасибо.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>
