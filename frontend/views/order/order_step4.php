<?php
use frontend\models\Order;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveField;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $form = ActiveForm::begin([
    'id' => 'order',
    //'enableAjaxValidation' => true,
    'enableClientValidation'=>true

]); ?>
    <div class="container-fluid popup order-address-popup page-4 order-form"   >

        <div class="container">
            <div style="">

                <div class="row order-logo">
                    <div class="col-md-4">
                        <div class="header-padding-left">
                            <div class="title">
                                оформление заказа адреса
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="flex">
                            <div class="popup-page">
                                4
                            </div>

                        </div>

                    </div>
                </div>
                <div class="row">

                    <div class="col-md-6 col-md-offset-3">
                        <?php
                        $order->payment_type = array_search($order->payment_type, Order::attrLists()['payment_type']);
                         echo $form->field($order, 'payment_type')->radioList(Order::attrLists()["payment_type"]);
                        ?>
                        <div class="field-row flex al radio">
                            <div class="form-label">
                                <label for="" class="flex">
                                    <div class="label-content">По счету</div>
                                </label>
                            </div>
                            <div class="form-input">
                                <?php

                                //var_dump($order->payment_type);
                                /*
                                var_dump( Order::attrLists()["payment_type"]);

                                print_r(Order::attrLists()["payment_type"]);*/ ?>
                                <?php //echo $form->field($order, 'payment_type')->radio(['uncheck'=>null, "required"=>false, 'value' => Order::attrLists()["payment_type"][3], 'class'=>'hidden-cb radio-btn payment-radio', 'id'=>null], false)->label(false); ?>                     <div class="form-radio
                                <?php
                                    if($order->payment_type == 3){
                                        echo "active";
                                    }
                                ?>
                                by-count">

                                </div>
                            </div>
                        </div>

                        <div class="field-row flex al radio">
                            <div class="form-label">
                                <label for="" class="flex">
                                    <div class="label-content">По карте</div>
                                </label>
                            </div>
                            <div class="form-input">
                                <?php  //echo $form->field($order, 'payment_type')->radio(['uncheck'=>null, "required"=>false, 'value' => Order::attrLists()["payment_type"][2], 'class'=>'hidden-cb radio-btn payment-radio', 'id'=>null], false)->label(false); ?>                    <div class="form-radio by-card
<?php
                                if($order->payment_type == 2){
                                    echo "active";
                                }
                                ?>">

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="row page-rule">
                    <div class="col-md-10 ">
                        <div class="field-row flex al order-master-padding">
                            <a href="<?=Url::to(["/order/{$order->address_id}?step=3"]); ?>" class="order-button-back">
                                вернуться назад
                            </a>
                            <?= Html::submitButton('далее', ['class' => 'order-button', 'name' => 'order-button']) ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>