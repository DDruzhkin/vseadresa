<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Order */


$this->title = 'Заказ ' . $model->getCaption();
$this->params['breadcrumbs'][] = ['label' => \backend\models\Order::getMeta('','title'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this -> title;

?>
<div class="container-fluid popup order-address-popup order-step order-page-one">

    <div class="container">
        <div >
            <div class="row order-logo">
                <div class="col-md-3 col-lg-4">
                    <div class="header-padding-left">
                        <div class="title">
                            оформление заказа адреса
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5">
                    <div class="flex">
                        <div class="popup-page">
                           <?=$step?>
                        </div>

                    </div>

                </div>
            </div>
		<div class="row">
                <div class="col-lg-6 col-lg-offset-4 col-md-5 col-md-offset-3">

                    <div class="offset-padding">
<!--    <h1><?= Html::encode($this->title) ?></h1> -->
<?php
	if ($step < 4)  {
		echo $this->render('_form', [
			'model' => $model,
			'step' => $step,
			'address_id' => $address_id,
		]);


	} else {
		$model -> changeStatus('new', true);
		echo $this->render('_completed', [
			'model' => $model,
		]);
	}
?>
			</div>
		</div>

	</div>
</div>
