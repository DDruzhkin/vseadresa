	<style>
#order-payment_type {
	display: block;
}
</style>
<?php
if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Order;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$controls = [

						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('created_at_view', 'label'), 'hint'=>''.Order::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'number' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('number', 'label'), 'hint'=>''.Order::getMeta('number', 'comment'), 'options'=>[]],
						'customer_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('customer_id', 'label'), 'items' => Order::getListValues('customer_id'), 'hint'=>'<a title="Перейти на карточку владельца" href="'.Url::to([$model -> getCaption('user/update/%customer_id%')]).'">'.Icon::show('link').'</a>'.Order::getMeta('customer_id', 'comment'), 'options'=>[]],
						'customer_r_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('customer_r_id', 'label'), 'items' => $model -> getRelatedListValues('customer_r_id'), 'hint'=>'<a title="Перейти" href="'.Order::getMeta('customer_r_id', 'comment'), 'options'=>[]],
						'status_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('status_id', 'label'), 'hint'=>''.Order::getMeta('status_id', 'comment'), 'items' => $model -> getStatusList()],
						'customer_r_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('customer_r_id_view', 'label'), 'hint'=>'<a title="Перейти на карточку владельца" href="'.Url::to([$model -> getCaption('cobject/update/%customer_r_id_view%')]).'">'.Icon::show('link').'</a>'.Order::getMeta('customer_r_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'owner_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('owner_id_view', 'label'), 'hint'=>'<a title="Перейти на карточку владельца" href="'.Url::to([$model -> getCaption('user/update/%owner_id_view%')]).'">'.Icon::show('link').'</a>'.Order::getMeta('owner_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],

						'address_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('address_id', 'label'), 'items' => Order::getListValues('address_id'), 'hint'=>'<a title="Перейти на карточку владельца" href="'.Url::to([$model -> getCaption('address/update/%address_id%')]).'">'.Icon::show('link').'</a>'.Order::getMeta('address_id', 'comment'), 'options'=>[]],
						'address_price_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('address_price_view', 'label'), 'hint'=>''.Order::getMeta('address_price_view', 'comment'), 'options'=>['readonly'=>'true', ]],

						'duration' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('duration', 'label'), 'items' => Order::getList('duration'), 'hint'=>''.Order::getMeta('duration', 'comment'), 'options'=>[]],
                        'delivery_type_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('delivery_type_id', 'label'), 'items' => Order::getListValues('delivery_type_id', false, false), 'hint'=>''.Order::getMeta('delivery_type_id', 'comment'), 'options'=>[]],
						'delivery_contacts' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('delivery_contacts', 'label'), 'hint'=>''.Order::getMeta('delivery_contacts', 'comment'), 'options'=>[]],
						'delivery_address' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('delivery_address', 'label'), 'hint'=>''.Order::getMeta('delivery_address', 'comment'), 'options'=>[]],
						'payment_type' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('payment_type', 'label'), 'items' => Order::getList('payment_type'), 'hint'=>''.Order::getMeta('payment_type', 'comment'), 'options'=>[]],
						'summa_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('summa_view', 'label'), 'hint'=>''.Order::getMeta('summa_view', 'comment'), 'options'=>['readonly'=>'true', ]],

				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW,
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) .
                        '</div>'
                ],

];
////////////////////////////////////////////////////
// Подготовка контролов для формы
?>


<?php
// HTML списка услуг
$ServiceListHTML = '';
if ($step == 1) { // Ограничение, чтобы не перпсчитывать на каждом шаге
	$slist = $model -> getServices() -> all();
	$list = [];

	for($i=0; $i < count($slist);$i++) {
		$v = $slist[$i] -> service_price_id;

		if (!in_array($v, $list)) $list[] = $v;
	};

	$all = $model -> address -> getServicePrices() -> all();
	//echo "<pre>", print_r($all);


	$i = 0;
	$ServiceListHTML = '
					<div class="field-row flex al">
                        <div class="form-label service-list">
                            набор сопутствующих услуг:
                        </div>
                    </div>										
					';


	foreach ($all as $servicePrice)	{
		$checked = in_array($servicePrice -> id, $list); // есть ли эта услуга в заказе или нет
		$ServiceListHTML = $ServiceListHTML .'
                    <div class="field-row flex al">
                        <div class="form-label">
                            <label for="" class="flex">
                                <div class="label-content">'.$servicePrice -> name.'</div>
                            </label>
                        </div>
                        <div class="form-input">
                            <input type="checkbox" class="hidden-cb" name="Order[services][]" '.($checked?' checked ':'').' value="'.$servicePrice -> id.'" />
                            <div class="form-checkbox'.($checked?' active-checkbox':'').'">

                            </div>
                        </div>

                    </div>
					';


	};


	/*$ServiceListHTML = $ServiceListHTML.'
			<!--</div>-->
		';*/

};

function getHTMLValue($label, $value) { // Вернуть код для отображения значения поля $attrName

	return '
						<div class="field-row flex al">
                            <div class="form-label">
                                <label for="" class="flex">
                                    <div class="label-content">'.$label.'</div>
                                </label>
                            </div>
                            <div class="form-input">
                               '.$value.'
                            </div>
                        </div>
			';
}

//Остальные контролы для каждого шага

$data = [
1 =>
	[

		[
			'attributes' => [
				'customer_r_id' => $controls['customer_r_id'],
			],
		],
		[
			'attributes' => [
				'duration' => $controls['duration'],
			],
		],

		[

			'attributes' => [
				'actions'=>[    //
                    'type'=>Form::INPUT_RAW,
                    'value'=>  $ServiceListHTML,
                ],
			]
		],



	],

2 => [
		[
			'attributes' => [
				'delivery_type_id' => $controls['delivery_type_id'],
			],
		],
		[
			'attributes' => [
				'delivery_address' => $controls['delivery_address'],
			],
		],

		[
			'attributes' => [
				'delivery_contacts' => $controls['delivery_contacts'],
			],
		],
	],

3 => [

		[
			'attributes' => [
				'address_price'=>[
					'type'=>Form::INPUT_RAW,
						'value'=>  getHTMLValue($model -> getMeta('address_price', 'label'), $model -> address_price),
				]
			],
		],
		[
			'attributes' => [
				'service_summa'=>[
					'type'=>Form::INPUT_RAW,
						'value'=>  getHTMLValue('стоимость набора дополнительных услуг', (float)$model -> summa - (float)$model -> address_price),
				]
			],
		],
		[
			'attributes' => [
				'address_price'=>[
					'type'=>Form::INPUT_RAW,
						'value'=>  getHTMLValue($model -> getMeta('delivery_address', 'label'), $model -> delivery_address),
				]
			],
		],

		[
			'attributes' => [
				'address_price'=>[
					'type'=>Form::INPUT_RAW,
						'value'=>  getHTMLValue($model -> getMeta('delivery_contacts', 'label'), $model -> delivery_contacts),
				]
			],
		],


	],
	/* С Иваном решили не указывать в заказе способ оплаты
4 => [
		[
			'attributes' => [
				'payment_type' => $controls['payment_type'],
			],
		],
	],
	*/

];


$url = ["/order/update/{$model->id}", 'step'=> $step + ($step < 4?1:0)];

if (!$model -> id) $url['address_id'] = $address_id;
$url = Url::to($url);

$formdata = $data[$step];

$formdata[] =

	[
		'attributes' => [
				'actions'=>[
					'type'=>Form::INPUT_RAW,
						'value'=>  '
						<div class="clear"></div>
						<div class="row">
						<div class="col-md-12">
				<div class="field-row flex al radio">
                            '.(
							($step <= 1)?'':
                            Html::submitButton('Назад', ['class' => 'order-button-back', 'name' => 'order-button-back', 'onClick' => '$("#form-order").attr("action", "'.Url::to(["/order/update/{$model->id}",  'step'=> $step - ($step > 1?1:0)]).'");'])
							).
							(
							Html::submitButton('далее', ['class' => 'order-button', 'name' => 'order-button', 'onClick' => '$("#form-order").attr("action", "'.$url.'");'])
							).'                       
                </div></div></div>
				',
				],
		],
	];



?>



<?php



$form = ActiveForm::begin(['id' => 'form-order']);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $formdata
	]);

ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true);
	$delta = $finTime - $startTime;
	//echo $delta . ' сек.';
}
?>


