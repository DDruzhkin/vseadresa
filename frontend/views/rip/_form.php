<?php
if (YII_DEBUG) $startTime = microtime(true);
use frontend\models\Rip;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;



$controls = [
	'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('created_at_view', 'label'), 'hint'=>''.Rip::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
	'inn' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rip::getMeta('inn', 'label'), 'hint'=>''.Rip::getMeta('inn', 'comment'), 'options'=>['mask' => 999999999999,]],
	'kpp' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rip::getMeta('kpp', 'label'), 'hint'=>''.Rip::getMeta('kpp', 'comment'), 'options'=>['mask' => 999999999,]],
	'ogrnip' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rip::getMeta('ogrnip', 'label'), 'hint'=>''.Rip::getMeta('ogrnip', 'comment'), 'options'=>['mask' => 999999999999999,]],
	'fname' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('fname', 'label'), 'hint'=>''.Rip::getMeta('fname', 'comment'), 'options'=>[]],
	'mname' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('mname', 'label'), 'hint'=>''.Rip::getMeta('mname', 'comment'), 'options'=>[]],
	'lname' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('lname', 'label'), 'hint'=>''.Rip::getMeta('lname', 'comment'), 'options'=>[]],
	'bdate' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Rip::getMeta('bdate', 'label'), 'hint'=>''.Rip::getMeta('bdate', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
	'pasport_number' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rip::getMeta('pasport_number', 'label'), 'hint'=>''.Rip::getMeta('pasport_number', 'comment'), 'options'=>['mask' => Rip::getMaskByAlias("pasport"),]],
	'pasport_date' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Rip::getMeta('pasport_date', 'label'), 'hint'=>''.Rip::getMeta('pasport_date', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 
		'pluginOptions' => [
			'todayHighlight' => true, 
			'autoclose'=>true,   			
			'endDate' => '0d',
			'format' => 'dd.mm.yyyy'
		], 
	]],
	'pasport_organ' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('pasport_organ', 'label'), 'hint'=>''.Rip::getMeta('pasport_organ', 'comment'), 'options'=>[]],
	'address' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('address', 'label'), 'hint'=>''.Rip::getMeta('address', 'comment'), 'options'=>[]],
	'bank_bik' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rip::getMeta('bank_bik', 'label'), 'hint'=>''.Rip::getMeta('bank_bik', 'comment'), 'options'=>['mask' => 999999999,]],
	'account' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rip::getMeta('account', 'label'), 'hint'=>''.Rip::getMeta('account', 'comment'), 'options'=>['mask' => Rip::getMaskByAlias("account"),]],
	'account_owner' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('account_owner', 'label'), 'hint'=>''.Rip::getMeta('account_owner', 'comment'), 'options'=>[]],	
	'sv_seriya' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('sv_seriya', 'label'), 'hint'=>''.Rip::getMeta('sv_seriya', 'comment'), 'options'=>[]],
	'sv_nomer' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('sv_nomer', 'label'), 'hint'=>''.Rip::getMeta('sv_nomer', 'comment'), 'options'=>[]],
	'sv_date' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Rip::getMeta('sv_date', 'label'), 'hint'=>''.Rip::getMeta('sv_date', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],					
	'bank_name' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('bank_name', 'label'), 'hint'=>''.Rip::getMeta('bank_name', 'comment'), 'options'=>[]],
	'bank_cor' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rip::getMeta('bank_cor', 'label'), 'hint'=>''.Rip::getMeta('bank_cor', 'comment'), 'options'=>['mask' => '99999999999999999999',]],
	'tax_system_vat' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>Rip::getMeta('tax_system_vat', 'label'), 'hint'=>''.Rip::getMeta('tax_system_vat', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],]],
	
	'actions'=>['type'=>Form::INPUT_RAW, 'value'=>  '<div style="text-align: right; margin-top: 20px">' .Html::submitButton('Изменить данные', ['class'=>'reg-button']) . '</div>'],	
];

$layout = [
	
		[
			'attributes' => [
				'header'=>['type'=>Form::INPUT_RAW, 'value'=>  '<h2>Паспортные данные</h2>'],		
			]
		],[
			'attributes' => [
				'fname' => $controls['fname'],
			],
		],[
			'attributes' => [
				'mname' => $controls['mname'],
			],
		],[
			'attributes' => [
				'lname' => $controls['lname'],
			],			
		],[
			'attributes' => [
				'pasport_number' => $controls['pasport_number'],
			],			
		],[
			'attributes' => [
				'pasport_date' => $controls['pasport_date'],
			],			
		],[
			'attributes' => [
				'pasport_organ' => $controls['pasport_organ'],
			],	
		],[
			'attributes' => [
				'address' => $controls['address'],
			],					
		],[
			'attributes' => [
				'header'=>['type'=>Form::INPUT_RAW, 'value'=>  '<h2>Реквизиты ИП</h2>'],		
			]
		],[
			'attributes' => [
				'inn' => $controls['inn'],
			],					
		],[
			'attributes' => [
				'ogrnip' => $controls['ogrnip'],
			],					
		],[
			'attributes' => [
				'sv_seriya' => $controls['sv_seriya'],
			],					
		],[
			'attributes' => [
				'sv_nomer' => $controls['sv_nomer'],
			],					
		],[
			'attributes' => [
				'sv_date' => $controls['sv_date'],
			],					
		],[
			'attributes' => [
				'header'=>['type'=>Form::INPUT_RAW, 'value'=>  '<h2>Реквизиты банковского счета</h2>'],		
			]
		],[
			'attributes' => [
				'bank_bik' => $controls['bank_bik'],
			],
		],[
			'attributes' => [
				'bank_name' => $controls['bank_name'],
			],
		],[
			'attributes' => [
				'bank_cor' => $controls['bank_cor'],
			],
		],[
			'attributes' => [
				'account' => $controls['account'],
			],					
		],[
			'attributes' => [
				'account_owner' => $controls['account_owner'],
			],					
		],[
			'attributes' => [
				'header'=>['type'=>Form::INPUT_RAW, 'value'=>  '<h2>Система налогообложения</h2>'],		
			]
		],[
			'attributes' => [
				'tax_system_vat' => $controls['tax_system_vat'],
			],					
		],[
			'attributes' => [
				'actions' => $controls['actions'],
			],					
		],
	
		
];




/* @var $this yii\web\View */
/* @var $model backend\models\Rip */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$form = ActiveForm::begin();
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $layout
	]);
ActiveForm::end();
	
if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
