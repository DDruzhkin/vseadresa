<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Rip */

$this->title = "Реквизиты ИП ".$model->getCaption();
$this->params['breadcrumbs'][] = ['label' => 'Rips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$attrList = $model -> getAttributes(); 
$attributes = [];
	foreach ($attrList as $colName => $value) {		
		$type = $model -> getMeta($colName, 'type');
		$show = (int)$model -> getMeta($colName, 'show');
		if (!$show) continue;
		$attributes[] = $colName."_view" . ($type == 'text'?':ntext':'');
	}


?>
<div class="rip-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
	
	//$this->theme->applyTo('view_custom');
	$viewFile = $this->findViewFile('view_custom');
	var_dump($viewFile);
	
	exit;
	$customView = $this->render('index', [
			'model' => $model,
			'user' => $user,
	]);
	if ($customView) {
		echo $customView;
	} else {
	
		echo DetailView::widget([
			'model' => $model,
			'attributes' => $attributes,
		]) 
	}
	?>
	
	DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) 
	
	?>
	
	

</div>
