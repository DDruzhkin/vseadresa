<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Rip */

$this->title = 'Добавление Реквизитов ИП';
$this->params['breadcrumbs'][] = $this->title;
?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
