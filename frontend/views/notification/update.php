<?php

use common\models\Entity;
use common\models\Status;
use common\models\NotificationManagerExt;

use frontend\models\Address;
use frontend\models\Order;
use frontend\models\User;
use frontend\models\Notification;

use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$this->title = 'Уведомления';
//$this->params['breadcrumbs'][] = $this->title;
//if ($user -> checkRole(User::ROLE_OWNER)) $role = 'owner';
//if ($user -> checkRole(User::ROLE_CUSTOMER)) $role = 'customer';
$this->params['breadcrumbs'] =
    [

        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/'.$role],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,
        ],

    ];


echo \frontend\components\NotificationWidget::widget(['notificationType' => Notification::NT_ORDER_STATUS, 'caption' => 'Изменение статусов заказа', 'user' => $user]);

if (in_array($role,['admin', 'owner'])) {
	echo \frontend\components\NotificationWidget::widget(['notificationType' => Notification::NT_ADDRESS_STATUS, 'caption' => 'Изменение статусов адреса', 'user' => $user]);
};

if ($role == 'admin') {

	
	echo \frontend\components\NotificationWidget::widget(['notificationType' => Notification::NT_USER_STATUS, 'caption' => 'Изменение статусов пользователя', 'user' => $user]);
?>	
	<h2>Другие события</h2>
	<form class="notification-form" action="<?=Url::to(['/notification'])?>">
		<table class="table price-table counts-table"><thead><tr><th>Событие</th><th>Email</th></tr></thead>

		<?php
		
		//$nnames = ['registration' => 'registration_admin'];
		$titles = [
			'registration_admin' => 'Регистрация пользователя', 
			'new_review_admin' => 'Новый отзыв',
			NotificationManagerExt::ORDER_FORMATION24.'_admin' => 'Заказ на Формирвоании более 24 часов',
			NotificationManagerExt::ORDER_FORMATION3.'_admin' => 'Срочный заказ на Формирвоании более 3 часов',
		];
		
		foreach($titles as $nname => $title) {			
			//$title = $titles[$type];
			$n = Notification::findOne(['nname' => $nname, 'user_id' => $user -> id]);			
			
			if ($n) {
				$enabled = $n -> enabled;
				$email = (is_null($n -> email) ? Yii::$app->user->identity->email : $n -> email);				
			} else {
				$email = Yii::$app->user->identity->email;
				$enabled = false;
			}	
		
		echo  '
		
		
		<tr>
		<td>
		<label><input type="checkbox" name="statuses[]" value="'.$nname.'"'.($enabled?' checked':'').'>'.$title.'</label>
		</td>
		<td><input name="emails['.$nname.']" class="form-control" type="text" value="'.$email.'"/></td>
		<td><input type="hidden" name="nnames['.$nname.']" class="form-control" type="text" value="'.$nname.'"/></td>
		</tr>
		';	
		}

		?>
		<input type="hidden" name="type" value="other"/>
		</table>
		<input type="submit" class="reg-button change-data" value="Сохранить"/>
		</form>
<?php
}

