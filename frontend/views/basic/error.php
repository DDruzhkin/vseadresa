<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="error-block">
    <div class="errormessage">
        <div>Ошибка</div>
    </div>
    <div class="message">
    <?=$message?>
    </div>
</div>
 <?php //echo 'url: '.Yii::$app->controller->get403(); ?>

<?php if (in_array($statusCode, [403])): ?>

<?php
		echo $this->render($statusCode, [
                'exception' => $exception,
                'statusCode' => $statusCode,
                'name' => $name,
                'message' => $message
        ]);
?>

<?php endif?>


