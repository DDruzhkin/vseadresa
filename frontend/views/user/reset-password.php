<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;


$this->title = Yii::t('app','resetPassword');
//$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('.help-block.help-block-error{display:block;    margin-left: 0px;}');


$controls = [
	'password' => ['type'=>Form::INPUT_PASSWORD, 'options'=>[]],
	'actions'=>['type'=>Form::INPUT_RAW, 'value'=>  Html::submitButton('Восстановить пароль	', ['class'=>'reg-button', 'style'=>'float: right;'])],
];


$layout = [
	[
		
		'autoGenerateColumns' => false,
		'columns' => 1,
		'attributes' => [		
			'password' => $controls['password'],		
			'password_repeat' => $controls['password'],		
			
		],
	],

	[
		
		'columns' => 1,
		'attributes' => [	
			
			'actions' => $controls['actions'],
		]
	]
];
?>

<div id="site-reset-password-token" class="">
    <div class="container">
    <div class="row">
    <div class="col-md-12" style="margin: 15px 0;">
		

<?php


$form = ActiveForm::begin(['id' => 'reset-password-form']);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $layout
	]);	
ActiveForm::end();
		

?>
    </div>

<?php
/*
?>
        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

				<div class="form-input">
					<?= $form->field($model, 'password')->passwordInput()->passwordInput(['autofocus' => true]) ?> <br/>
                </div>
				<div class="form-input">
					<?= $form->field($model, 'password_repeat')->passwordInput() ?>
				</div>
				

				
				<div class="field-row flex al">                    
                        <?= Html::submitButton(Yii::t('app','save'), ['class' => 'login-button', 'name' => 'login-button']) ?>                    
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
		
<?php
*/
?>		
		
		    </div>
    </div>
</div>
