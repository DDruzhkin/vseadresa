<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>                
				
				<?php $form = ActiveForm::begin(['id' => 'login-form', 'action' => Url::to(['/login'])]); ?>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">e-mail</div>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(false); ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">пароль</div>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $form->field($model, 'password')->passwordInput()->label(false); ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <!--<a href="" class="login-button">-->
                        <?= Html::submitButton('Войти', ['class' => 'login-button', 'name' => 'login-button']) ?>
                    <!--</a>-->
                </div>
                <?php ActiveForm::end(); ?>