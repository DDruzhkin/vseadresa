<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;

$this->title = Yii::t('app','requestPasswordReset');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('.help-block.help-block-error{display:block;    margin-left: 0px;}');
//$this -> context -> title = $this->title;

$controls = [
	'email' => ['type'=>Form::INPUT_TEXT, 'options'=>['type' => 'email']],
	'actions'=>['type'=>Form::INPUT_RAW, 'value'=>  Html::submitButton('Восстановить пароль', ['class'=>'reg-button', 'style'=>'float: right;'])],
];


$layout = [
	[
		
		'autoGenerateColumns' => false,
		'columns' => 1,
		'attributes' => [		
			'email' => $controls['email'],								
		],
	],

	[
		
		'columns' => 1,
		'attributes' => [	
			
			'actions' => $controls['actions'],
		]
	]
];


?>



<div id="site-reset-password-token" class="">
    <div class="row">
    <div class="col-md-12" style="margin: 15px 0;">

<?php

		

$form = ActiveForm::begin(['id' => 'reset-password-form']);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $layout
	]);	
ActiveForm::end();
		

?>

	</div>
	</div>
</div>

