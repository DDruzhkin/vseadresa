<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use frontend\models\User;
use frontend\models\Address;


/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index'], 'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL];
$this->params['breadcrumbs'][] = $this->title;

$attrList = $model -> getAttributes(); 
$attributes = [];
	foreach ($attrList as $colName => $value) {		
		$type = $model -> getMeta($colName, 'type');
		$show = (int)$model -> getMeta($colName, 'show');
		if (!$show) continue;
		$attributes[] = $colName."_view" . ($type == 'text'?':ntext':'');
	}



	//echo "<pre>", print_r($model);
?>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">


                    <h1 class="user-card-title"><?= Html::encode($this->title) ?></h1>
                    <div class="user-card-container">
					<!--
                        <div class="flex user-card-row">
                            <div class="user-card-label">
                                <?php echo User::getMeta('fullname', 'label'); ?>
                            </div>
                            <div class="user-card-value">
                                <?php echo $model->fullname; ?>
                            </div>
                        </div>
						-->
	<?php if (User::getCurrentUser() && User::getCurrentUser() -> checkRole(User::ROLE_ADMIN)): ?>
                        <div class="flex user-card-row">
                            <div class="user-card-label">
                                <?php echo User::getMeta('username', 'label'); ?>
                            </div>
                            <div class="user-card-value">
                                <?php echo $model->username; ?>
                            </div>
                        </div>
                        <div class="flex user-card-row">
                            <div class="user-card-label">
                                <?php echo User::getMeta('role', 'label'); ?>
                            </div>
                            <div class="user-card-value">
                                <?php echo $model->role; ?>
                            </div>
                        </div>
                        <div class="flex user-card-row">
                            <div class="user-card-label">
                                <?php echo User::getMeta('status_id', 'label'); ?>
                            </div>
                            <div class="user-card-value">
                                <?php echo $model->status->getCaption(); ?>
                            </div>
                        </div>
                        <div class="flex user-card-row">
                            <div class="user-card-label">
                                <?php echo User::getMeta('created_at', 'label'); ?>
                            </div>
                            <div class="user-card-value">
                                <?php echo $model->created_at; ?>
                            </div>
                        </div>
                        <div class="flex user-card-row">
                            <div class="user-card-label">
                                <?php echo User::getMeta('phone', 'label'); ?>
                            </div>
                            <div class="user-card-value">
                                <?php echo $model->phone; ?>
                            </div>
                        </div>
	<?php endif?>
						
                    </div>
                    <?php if($model->role ==  User::attrLists()['role']['Владелец']): ?>
                        <div class="user-card-addresses">
                            <h2>
                                Список адресов
                            </h2>
                        </div>
                    <div class="grid-table-container">
                        <?php
						
//						$statuses = [Address::statusByAlias('published') -> id]; // статусы, в которых оторажается адрес			

//                        $searchModel= $model->getAddresses() -> andWhere(['status_id' => $statuses]); // ограничение по статусам;
						
						$searchModel= $model->getAddresses() -> andWhere(Address::getToCatalogCondition());
						
                        $dataProvider = new ArrayDataProvider([
                            'allModels' => $searchModel->all(),
                            'sort' => [
                                'attributes' => ['id', 'username', 'email'],
                            ],
                            'pagination' => [
                                'pageSize' => 10,
                            ],
                        ]);
                        ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'summary' => false,
                            'tableOptions' => [
                                'class' => 'table price-table counts-table',

                            ],

                            'filterModel' => $searchModel,
                            'columns' => [
                                /*['class' => 'yii\grid\SerialColumn'],*/
                                [
                                    'class' => 'yii\grid\Column',
                                ],


                                ['attribute' => 'id',
                                    'label' => 'Код',
                                    'contentOptions' => ['class' => 'col-id '],
                                    'filterOptions' => ['class' => 'col-id'],
                                    'headerOptions' => ['class' => 'col-id '],
                                    'footerOptions' => ['class' => 'col-id '],



                                ],

                                ['attribute' => 'address',
                                    'label' => Address::getMeta('address', 'label'),
                                    'contentOptions' => ['class' => 'col-address '],
                                    'filterOptions' => ['class' => 'col-address'],
                                    'headerOptions' => ['class' => 'col-address '],
                                    'footerOptions' => ['class' => 'col-address '],



                                ],
                                ['attribute' => 'status_id',
                                    'label' => Address::getMeta('status_id', 'label'),
                                    'contentOptions' => ['class' => 'col-status_id '],
                                    'filterOptions' => ['class' => 'col-status_id'],
                                    'headerOptions' => ['class' => 'col-status_id '],
                                    'footerOptions' => ['class' => 'col-status_id '],
                                    'filter' => Address::getExistsValues('status_id'),
                                    'value' => function($data){ return $data['status_id_view'];},

                                ],


//            ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
                    <?php endif; ?>

                </div>
            </div>

        </div>
    </div>
