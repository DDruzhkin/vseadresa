<?php

use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

?>

<?php

$owner = $model->owner;
$recipient = $model->recipient;

?>

<?php if ($owner && $recipient): ?>
    <div class="document-content" style="max-width:1200px;margin: 0 auto;">
        <div class="header-blog row" style="    padding-bottom: 20px; padding-top: 20px;">
            <div class="col-md-6 header-padding-left">
            </div>
            <div class="col-md-6">
                <div class="text-right">
                    <h2>ООО "Kомпания"</h2>
                    <div><?= $owner->address ?></div>
                    <div>
                        <?php if ($owner->inn || $owner->kpp): ?>
                            ИНН/КПП <?= $owner->inn; ?>
                            <?php if ($owner->kpp): ?> /
                                <?= $owner->kpp ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <div>
                        <?php if ($owner->ogrnip): ?>
                            ОГРН <?= $owner->ogrnip ?>
                        <?php endif; ?>
                    </div>
                    <div>
                        <?= $owner->phone ?>
                    </div>
                </div>
            </div>
        </div>

        <div style="border: 2px solid #616161; width: 100%;margin-bottom: 20px;margin-top: 10px;"></div>

        <div><h4 class="text-center"><strong>Общество с ограниченной ответственностью "Kомпания"</h4> </strong></div>

        <table class="owner-recipient-date" style=" width: 100%;border: 1px solid #020202;margin-bottom: 20px;">
            <thead>
            <tr>
                <th style="padding:5px;width:50%">ООО "Kомпания"</th>
                <th style="padding:5px;width:50%">ООО "Покупатель"</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td style="padding:5px;width:50%"><?= $owner->address ?></td>
                <td style="padding:5px;width:50%"><?= $recipient->address ?></td>
            </tr>
            <tr>
                <td style="padding:5px;width:50%">
                    <?php if ($owner->inn || $owner->kpp): ?>
                        ИНН/КПП <?= $owner->inn; ?>
                        <?php if ($owner->kpp): ?> /
                            <?= $owner->kpp ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
                <td style="padding:5px;width:50%">
                    <?php if ($recipient->inn || $recipient->kpp): ?>
                        ИНН/КПП <?= $recipient->inn; ?>
                        <?php if ($recipient->kpp): ?> /
                            <?= $recipient->kpp ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td style="padding:5px;width:50%"> ОГРН <?= $owner->ogrnip ?></td>
                <td style="padding:5px;width:50%"> ОГРН <?= $recipient->ogrnip ?></td>
            </tr>
            <tr>
                <td style="padding:5px;width:50%">БИК <?= $owner->bank_bik ?></td>
                <td style="padding:5px;width:50%">БИК <?= $recipient->bank_bik ?></td>
            </tr>

            <tr>
                <td style="padding:5px;width:50%"><?= $owner->account ?></td>
                <td style="padding:5px;width:50%"><?= $recipient->account ?></td>
            </tr>
            <tr>
                <td style="padding:5px;width:50%"><?= $owner->account_owner ?></td>
                <td style="padding:5px;width:50%"><?= $recipient->account_owner ?></td>
            </tr>

            <tr>
                <td style="padding:5px;width:50%">Телефон <?= $owner->phone ?></td>
                <td style="padding:5px;width:50%">Телефон <?= $recipient->phone ?></td>
            </tr>
            </tbody>
        </table>


        <?php
        if ($services): ?>
            <div><h4 class="text-center">
                    <strong>
                        СЧЕТ № <?= $model->number ?> от <?= date_format(date_create($model->created_at), "d F Y") ?>
                    </strong>
                </h4></div>
            <div class="text-right"><?= $model->info ?></div>
            <table class="table table-striped document-service-table" style="    border: 1px solid #dedede;">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Товар / Услуги</th>
                    <th>Цена,руб.</th>
                    <th>Кол-во</th>
                    <th>Ед.изм</th>
                    <th>НДС</th>
                    <th>Сумма,руб.</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sumPrice = $sumSumma = 0;
                foreach ($services as $key => $service):
                    $price = round($service->price / ($service->nds / 100 + 1) * $service->nds / 100, 2);
                    $sumPrice += $price;
                    $sumSumma += $service->summa;
                    ?>
                    <tr>
                        <td style="border: 1px solid #e0dede;"><?= $key + 1 ?></td>
                        <td style="border: 1px solid #e0dede;"><?= $service->name ?></td>
                        <td style="border: 1px solid #e0dede;"><?= number_format($service->price, 2, ".", ""); ?></td>
                        <td style="border: 1px solid #e0dede;">1</td>
                        <td style="border: 1px solid #e0dede;">шт</td>
                        <td style="border: 1px solid #e0dede;"><?= $service->nds ?>%</td>
                        <td style="border: 1px solid #e0dede;"><?= number_format($service->summa, 2, ".", ""); ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td style="border: 1px solid #e0dede;" colspan="6" class="text-right">Итого в т.ч. НДС
                        (<?= $service->nds ?>%)
                        = <?= number_format($sumPrice, 2, ".", ""); ?> руб
                    </td>
                    <td style="border: 1px solid #e0dede;"
                        colspan="1"><?= number_format($sumSumma, 2, ".", ""); ?> </td>
                </tr>
                <tr>
                    <td style="border: 1px solid #e0dede;" colspan="7">
                        <div>Оплата по счету № <?= $model->number ?>
                            от <?= date_format(date_create($model->created_at), "d F Y") ?> за обслуживание
                            вычислительной техники в т.ч. НДС (<?= $service->nds ?>%)
                            = <?= number_format($sumPrice, 2, ".", ""); ?> руб
                        </div>
                        <div class="text-right" style="align:right;">Назначение платежа</div>
                    </td>
                </tr>
                </tbody>
            </table>
        <?php endif; ?>

        <!-- <div class="row">
             <div class="col-md-9">
                 <div class="heads-organization">
                     <div>
                         Руководители организации
                     </div>
                     <div class="line" style="width: 200px"></div>
                     <div class="line" style="width: 200px"></div>
                 </div>

                 <div class="chief-accountant">
                     <div>
                         Главные бухгалтер
                     </div>
                     <div class="line" style="width: 200px"></div>
                     <div class="line" style="width: 200px"></div>
                 </div>
             </div>
             <div class="col-md-3">
             </div>
         </div>
 -->
    </div>

<?php endif; ?>