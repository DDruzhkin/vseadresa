<?php
use app\components\CabWidget;
?>
<?php $this->params['breadcrumbs'] =
    [


        [
            'label' => 'Username',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => 'профиль',
        ],

    ];


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<section class="cabinet-section">
    <div class="container-fluid cloud-bg no-padding-top-bg">

        <div class="container">
            <div class="row">
                <div class="col-md-2 col-lg-3 add-search-block">

                    <?php echo CabWidget::widget(); ?>

                </div>
                <div class="col-md-8 col-md-offset-1 all-addresses-block">
                    <div class="offset-padding">
                    <div class="cabinet-form">
                        <form class="flex" action="">
                            <div class="col-md-9">
                                <div class="row">
                                    <h2>Расчёты</h2>

                                </div>
                                <div class="row flex">
                                    <div class="">
                                        <div class="find-counts-cnt">
                                            найти платежи
                                            или счета по
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="dates-from-to">
                                            <div class="field-row from-date-cnt flex al">
                                                <div class="form-label">
                                                    <label for="" class="flex">
                                                        <div class="label-content">датам с</div>
                                                    </label>
                                                </div>
                                                <div class="form-input">
                                                    <input  class="date" placeholder=".. / .. / ...." type="text">
                                                </div>
                                            </div>
                                            <div class="field-row flex al">
                                                <div class="form-label">
                                                    <label for="" class="flex">
                                                        <div class="label-content">до</div>
                                                    </label>
                                                </div>
                                                <div class="form-input">
                                                    <input  class="date" placeholder=".. / .. / ...." type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="count-search-subdata">
                                            <div class="field-row flex al count-search-subdata-input">

                                                <div class="form-input ">
                                                    <input  type="text" placeholder="Сумма">
                                                </div>
                                            </div>
                                            <div class="field-row  flex al count-search-subdata-select">

                                                <div class="form-input ">
                                                    <select name="" id="">
                                                        <option value="">точно</option>
                                                        <option value="">точно</option>
                                                        <option value="">точно</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-9 col-md-offset-3">
                                    <div class="count-search-checkbox-subdata">
                                        <div class="form-label checkbox-part-one">
                                            <label for="" class="flex">
                                                <div class="label-content">показать</div>
                                            </label>
                                        </div>
                                        <div class="field-row flex al count-search-checkbox checkbox-part-two">
                                            <div class="form-input">
                                                <input type="checkbox" class="hidden-cb">
                                                <div class="form-checkbox">

                                                </div>
                                            </div>
                                            <div class="form-label">
                                                <label for="" class="flex">
                                                    <div class="label-content">только счета</div>
                                                </label>
                                            </div>

                                        </div>
                                        <div class="field-row flex al count-search-checkbox checkbox-part-three">
                                            <div class="form-input">
                                                <input type="checkbox" class="hidden-cb">
                                                <div class="form-checkbox">

                                                </div>
                                            </div>
                                            <div class="form-label">
                                                <label for="" class="flex">
                                                    <div class="label-content">только оплаты</div>
                                                </label>
                                            </div>

                                        </div>

                                    </div>

                                </div>


                    </div>
                            <div class="col-md-2" id="submit-count-search-form">
                                <div class="publish-address">
                                    <div class="public-address">
                                        <a href="">
                                            <div class="public-address-button">
                                                искать
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-11 table-cnt">
                                <div class="table-self">
                                <table class="table price-table counts-table">
                                    <thead>
                                        <th> номер операции</th>
                                        <th>дата</th>
                                        <th>вид</th>
                                        <th>сумма</th>
                                        <th>статус</th>
                                        <th>связанный счет</th>
                                        <th></th> <!-- for orders -->
                                    </thead>
                                    <tbody>
                                    <tr>

                                        <td>253464</td>
                                        <td>15.10.2016</td>
                                        <td>счет</td>
                                        <td>25000</td>
                                        <td>
                                            оплачен
                                        </td>
                                        <td>4 000</td>

                                        <td>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>ЦАО</td>
                                        <td>2</td>
                                        <td>счет</td>
                                        <td>25000</td>
                                        <td>

                                        </td>
                                        <td>4 000</td>

                                        <td>
                                            <a href="" class="book-it">оплатить</a>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>ЦАО</td>
                                        <td>2</td>
                                        <td>счет</td>
                                        <td>25000</td>
                                        <td>

                                        </td>
                                        <td>4 000</td>

                                        <td>

                                        </td>
                                    </tr>
                                    <tr>

                                        <td>ЦАО</td>
                                        <td>2</td>
                                        <td>счет</td>
                                        <td>25000</td>
                                        <td>

                                        </td>
                                        <td>4 000</td>

                                        <td>
                                            <a href="" class="book-it">оплатить</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="hr">
                                    <hr>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="pag">
                                    <a><div class="pagination-arrow pagination-arrow-left">
                                            <img src="/img/triangle_left.png">
                                        </div></a>
                                    <div class="pag-stripe"></div>
                                    <div class="pag-page">
                                        <span class="active-page">1</span><span class="of">&nbsp;/ 15</span>
                                    </div>
                                    <div class="pag-stripe"></div>
                                    <a><div class="pagination-arrow pagination-arrow-right">
                                            <img src="/img/triangle_right.png">
                                        </div></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>
