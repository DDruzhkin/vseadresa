<?php
use app\components\CabWidget;
?>
<?php $this->params['breadcrumbs'] =
    [


        [
            'label' => 'Компания',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => 'профиль',
        ],

    ];


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<section class="cabinet-section">
    <div class="container-fluid cloud-bg no-padding-top-bg">

        <div class="container">
            <div class="row">
                <div class="col-md-3 add-search-block">

                    <?php echo CabWidget::widget(); ?>

                </div>
                <div class="col-md-8 col-md-offset-1 all-addresses-block statistics">
                    <div class="row">
                        <div class="col-md-11">
                            <div class="offset-padding">
                            <div class="dates-from-to">

                                <div class="flex-all by-time-filter">
                                    <div class="by-time active">
                                        по дням
                                    </div>
                                    <div class="by-time">
                                        по месяцам
                                    </div>
                                </div>
                                <div class="field-row from-date-cnt flex al">
                                    <div class="form-label period">
                                        <label for="" class="flex">
                                            <div class="label-content">в период с</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input  class="date" placeholder=".. / .. / ...." type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">до</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input  class="date" placeholder=".. / .. / ...." type="text">
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11">
                            <div class="labels cabinet-label stat-head">
                                Статистика по адресам
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-11 table-cnt">
                                <div class="table-self">
                                    <table class="table price-table counts-table">
                                        <thead>
                                            <th>адрес</th>
                                            <th>количество созданных заказов</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>ул. саратовская</td>
                                                <td>5002</td>
                                            </tr>
                                            <tr>
                                                <td>ул. саратовская</td>
                                                <td>5002</td>
                                            </tr>
                                            <tr>
                                                <td>ул. саратовская</td>
                                                <td>5002</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="hr">
                                        <hr>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="field-row flex al">
                            <a href="" class="reg-button notiications-button">
                                загрузить в <br> XLS
                            </a>
                        </div>

                        <div class="">
                            <div class="col-md-11">
                                <div class="labels stat-head cabinet-label">
                                    Статистика по адресам
                                </div>
                            </div>
                        </div>
                            <div class="row">
                                <div class="col-md-11 table-cnt">
                                    <div class="table-self">
                                        <table class="table price-table counts-table">
                                            <thead>
                                                <th>адрес</th>
                                                <th>количество созданных заказов</th>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>ул. саратовская</td>
                                                <td>5002</td>
                                            </tr>
                                            <tr>
                                                <td>ул. саратовская</td>
                                                <td>5002</td>
                                            </tr>
                                            <tr>
                                                <td>ул. саратовская</td>
                                                <td>5002</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div class="hr">
                                            <hr>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="field-row flex al">
                                <a href="" class="reg-button notiications-button">
                                    загрузить в <br> XLS
                                </a>
                            </div>

                </div>
            </div>


        </div>
    </div>
</section>
