<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveField;
use common\models\User;
use common\models\Region;
use frontend\models\OwnerSignup;
?>
<section class="owner-form">
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-9 col-lg-offset-4 col-md-offset-3">
                <div class="col-md-12">
<select name="" id="choose-your-status" class="metro">
                </div>
            </div>
        </div>
<?php
$arr = OwnerSignup::attrLists()['form'];
foreach ($arr as $k=>$item) :?>
    <option value="<?=$k?>"><?=$item?></option>
<?php endforeach; ?>
</select>
    </div></div>





<?php $personForm = ActiveForm::begin([
    'id' => 'person',
    'enableAjaxValidation' => true,
    'enableClientValidation'=>true

]); ?>

    <div class="container-fluid">
        <div class="container">
            <div class="row registrant-type" style="text-align: left">
                <div class="">
                Физическое лицо
                </div>
            </div>
            <div class="row">
            <div class="col-md-3 col-lg-4 reg-tutorial">
                <div class="flex row">
                    <sup>*</sup>
                    <div>помечены поля, <br>
                        обязательные для заполнения</div>
                </div>

            </div>
            <div class="col-md-9 col-lg-8">

                <div class="labels cabinet-label">
                    адрес доставки документов
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">индекс</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($deliveryAddress, 'index')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">область / край</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?php $region_array = Region::find()->all(); ?>
                        <?php $items = ArrayHelper::map($region_array,'id','name');

                        $params = [
                            /*'prompt' => 'Регион',*/
                            'class' => 'metro',
                            'label' => '',
                            'id'=>null
                        ];
                        echo $personForm->field($region, 'metro_id')->label(false)->dropDownList($items,$params);?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">район</div>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($deliveryAddress, 'district')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">Нас. пункт</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($deliveryAddress, 'destination')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">адрес</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($deliveryAddress, 'address')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">примечания</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($deliveryAddress, 'info')->label(false)->textarea(['rows' => '10', "cols"=>'30']) ?>
                    </div>
                </div>



                <div class="labels cabinet-label">
                    паспортные данные
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">фамилия</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($person, 'lname')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">имя</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($person, 'fname')->label(false)->textInput() ?>
                    </div>
                </div>

                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">отчество</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($person, 'mname')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">серия / номер паспорта</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($person, 'pasport_number')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">дата выдачи паспорта</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($person, 'pasport_date')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">орган выдавший паспорт</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($person, 'pasport_organ')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">адрес регистрации</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($person, 'address')->label(false)->textInput() ?>
                    </div>
                </div>




                <div class="labels cabinet-label">
                    Реквизиты банковского счета
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">БИК</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($person, 'bank_bik')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">номер счета</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($person, 'account')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">владелец счета</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $personForm->field($person, 'account_owner')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">

                        </label>
                    </div>
                    <div class="form-input">
                        <div class="cant-modify attention">
                            изменение данных для договора потребует заключение дополнительного соглашения об изменении реквизитов договора в течении одной недели, в случае, если договор не будет перезаключен, будут восстановлены данные текущего договора. Вы уверены что хотите изменить данные договора?
                        </div>
                    </div>
                </div>




                <div class="field-row flex al">
                    <!--<a href="" class="reg-button">
                        изменить данные
                    </a>-->
                    <?= Html::submitButton('Завершить регистрацию', ['class' => 'reg-button', 'name' => 'signup-button']) ?>
                </div>
            </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>



<?php $companyForm = ActiveForm::begin([
    'id' => 'company',
    'enableAjaxValidation' => true,
    'enableClientValidation'=>true

]); ?>

<div class="container-fluid">
    <div class="container">
        <div class="row registrant-type" style="text-align: left">
            <div class="">
                Юридическое лицо
            </div>
        </div>
        <div class="row">
        <div class="col-md-3 col-lg-4 reg-tutorial">
            <div class="flex row">
                <sup>*</sup>
                <div>помечены поля, <br>
                    обязательные для заполнения</div>
            </div>

        </div>
        <div class="col-md-9 col-lg-8">

            <div class="labels cabinet-label">
                адрес доставки документов
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">индекс</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($deliveryAddress, 'index')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">область / край</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?php $region_array = Region::find()->all(); ?>
                    <?php $items = ArrayHelper::map($region_array,'id','name');

                    $params = [
                        /*'prompt' => 'Регион',*/
                        'class' => 'metro',
                        'label' => '',
                        'id'=>null
                    ];
                    echo $companyForm->field($region, 'metro_id')->label(false)->dropDownList($items,$params);?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">район</div>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($deliveryAddress, 'district')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">Нас. пункт</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($deliveryAddress, 'destination')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">адрес</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($deliveryAddress, 'address')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">примечания</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($deliveryAddress, 'info')->label(false)->textarea(['rows' => '10', "cols"=>'30']) ?>
                </div>
            </div>



            <div class="labels cabinet-label">
                Реквизиты юридического лица
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">Название организации</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($company, 'name')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">ИНН</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($company, 'inn')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">КПП</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($company, 'kpp')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">ОГРН</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($company, 'ogrn')->label(false)->textInput() ?>
                </div>
            </div>

            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">Юридический адрес</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($company, 'address')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="labels cabinet-label">
                Для организаций
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">Фамилия Имя Отчество лица подписывающего договор</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($company, 'dogovor_signature')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">Договор подписывается в лице* (по умолчанию Генерального директора)</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($company, 'dogovor_face')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">Договор подписывается в лице* (по умолчанию Генерального директора)</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($company, 'dogovor_face')->label(false)->textInput() ?>
                </div>
            </div>



            <div class="labels cabinet-label">
                Реквизиты банковского счета
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">БИК</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($company, 'bank_bik')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">номер счета</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($company, 'account')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">владелец счета</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $companyForm->field($company, 'account_owner')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">

                    </label>
                </div>
                <div class="form-input">
                    <div class="cant-modify attention">
                        изменение данных для договора потребует заключение дополнительного соглашения об изменении реквизитов договора в течении одной недели, в случае, если договор не будет перезаключен, будут восстановлены данные текущего договора. Вы уверены что хотите изменить данные договора?
                    </div>
                </div>
            </div>




            <div class="field-row flex al">
                <!--<a href="" class="reg-button">
                    изменить данные
                </a>-->
                <?= Html::submitButton('Завершить регистрацию', ['class' => 'reg-button', 'name' => 'signup-button']) ?>
            </div>
        </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>


<?php $ipForm = ActiveForm::begin([
    'id' => 'ip',
    'enableAjaxValidation' => true,
    'enableClientValidation'=>true

]); ?>

<div class="container-fluid">
    <div class="container">
        <div class="row registrant-type" style="text-align: left">
            <div class="">
                Индивидуальный предприниматель
            </div>
        </div>
        <div class="row">
        <div class="col-md-3 col-lg-4 reg-tutorial">
            <div class="flex row">
                <sup>*</sup>
                <div>помечены поля, <br>
                    обязательные для заполнения</div>
            </div>

        </div>
        <div class="col-md-9 col-lg-8">

            <div class="labels cabinet-label">
                адрес доставки документов
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">индекс</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($deliveryAddress, 'index')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">область / край</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?php $region_array = Region::find()->all(); ?>
                    <?php $items = ArrayHelper::map($region_array,'id','name');

                    $params = [
                       /* 'prompt' => 'Регион',*/
                        'class' => 'metro',
                        'label' => '',
                        'id'=>null
                    ];
                    echo $ipForm->field($region, 'metro_id')->label(false)->dropDownList($items,$params);?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">район</div>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($deliveryAddress, 'district')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">Нас. пункт</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($deliveryAddress, 'destination')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">адрес</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($deliveryAddress, 'address')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">примечания</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($deliveryAddress, 'info')->label(false)->textarea(['rows' => '10', "cols"=>'30']) ?>
                </div>
            </div>

            <div class="labels cabinet-label">
                паспортные данные
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">фамилия</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($rip, 'lname')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">имя</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($rip, 'fname')->label(false)->textInput() ?>
                </div>
            </div>

            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">отчество</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($rip, 'mname')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">серия / номер паспорта</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($rip, 'pasport_number')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">дата выдачи паспорта</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($rip, 'pasport_date')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">орган выдавший паспорт</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($rip, 'pasport_organ')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">адрес регистрации</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($rip, 'address')->label(false)->textInput() ?>
                </div>
            </div>

            <div class="labels cabinet-label">
                Реквизиты ИП
            </div>
            <?php

            /*
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">Название ИП</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($company, 'name')->label(false)->textInput() ?>
                </div>
            </div> */
            ?>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">ИНН</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($rip, 'inn')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">КПП</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($rip, 'kpp')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">ОГРНИП</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($rip, 'ogrnip')->label(false)->textInput() ?>
                </div>
            </div>





            <div class="labels cabinet-label">
                Реквизиты банковского счета
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">БИК</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($rip, 'bank_bik')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">номер счета</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($rip, 'account')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">
                        <div class="label-content">владелец счета</div>
                        <sup>*</sup>
                    </label>
                </div>
                <div class="form-input">
                    <?= $ipForm->field($rip, 'account_owner')->label(false)->textInput() ?>
                </div>
            </div>
            <div class="field-row flex al">
                <div class="form-label">
                    <label for="" class="flex">

                    </label>
                </div>
                <div class="form-input">
                    <div class="cant-modify attention">
                        изменение данных для договора потребует заключение дополнительного соглашения об изменении реквизитов договора в течении одной недели, в случае, если договор не будет перезаключен, будут восстановлены данные текущего договора. Вы уверены что хотите изменить данные договора?
                    </div>
                </div>
            </div>




            <div class="field-row flex al">
                <!--<a href="" class="reg-button">
                    изменить данные
                </a>-->
                <?= Html::submitButton('Завершить регистрацию', ['class' => 'reg-button', 'name' => 'signup-button']) ?>
            </div>
        </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
</section>