<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app','resetPassword');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('.help-block.help-block-error{display:block;    margin-left: 0px;}');
?>
<div class="container">
    <div class="site-reset-password">
        <h1><?= Html::encode($this->title) ?></h1>

        <p><?= Yii::t('app','chooseNewPassword')?></p>

        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                <?= $form->field($model, 'password', ['options' => ['class' => 'clearfix']])->passwordInput(['autofocus' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app','save'), ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
