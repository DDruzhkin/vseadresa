<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app','requestPasswordReset');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('.help-block.help-block-error{display:block;    margin-left: 0px;}')
?>
<div class="container">
    <div class="site-request-password-reset">
        <h1><?= Html::encode($this->title) ?></h1>
        <p><?= Yii::t('app','resetPasswordEmailText') ?></p>


        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'email', ['options' => ['class' => 'clearfix']])->textInput(['autofocus' => true]
                ) ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app','save'), ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
