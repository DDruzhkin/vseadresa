    <?php
use app\components\CabWidget;
?>
<?php $this->params['breadcrumbs'] =
    [


        [
            'label' => 'Username',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => 'профиль',
        ],

    ];


use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\Order;
use backend\models\Status;
use common\models\OrderSearch;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<section class="cabinet-section cloud-bg no-padding-top-bg">
    <div class="container-fluid">

        <div class="container">
            <div class="row">
                <div class="col-md-2 col-lg-3 add-search-block">
                    <?php echo CabWidget::widget(); ?>
                </div>
                <div class="col-md-8 col-md-offset-1 all-addresses-block orders-search-form">
                    <div class="offset-padding">

                    <div class="cabinet-form">
                        <div class="col-md-11">
                            <div class="row">
                                <div class="flex al">
                                <h2>Ваши заказы</h2>
                                <div class="filter-callout  <?php if(isset($_GET["OrderSearch"])){
                                    echo "active-filter";
                                } ?>">
                                    <i class="fa fa-filter"></i>
                                </div>
                                </div>
                            </div>
                            <!--<div class="order-flex row">
                                <div class="order-flex-left">
                                    <div class="find-counts-cnt">
                                        найти заказ по
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>

                        <?php /* $form = ActiveForm::begin(['action' =>['site/orders'], 'method' => 'get', 'options' => [
                            'class' => 'flex'
                        ]]);?>
                            <div class="col-md-9">
                                <div class="row flex al">
                                    <h2>Ваши заказы</h2>

                                </div>
                                <div class="order-flex row">
                                    <div class="order-flex-left">
                                        <div class="find-counts-cnt">
                                            найти заказ по
                                        </div>
                                    </div>
                                    <div class="rder-flex-right">
                                        <div class="">
                                            <div class="field-row flex al">

                                                <div class="form-input">
                                                    <?php echo $form->field($searchModel, 'number')->label(false)->textInput(["placeholder"=>"по номеру"])?>
                                                </div>
                                            </div>
                                            <div class="field-row flex al">

                                                <div class="form-input">
                                                    <?php
                                                        foreach (Order::find()->where(["customer_id"=>Yii::$app->user->identity->id])->all() as $order){
                                                            $addresses[] = $order->address;
                                                            //echo "<pre>", print_r($order->attributes);
                                                        }
                                                   // echo "<pre>", print_r($addresses);

                                                    ?>
                                                    <?php
                                                    $params = [
                                                        'prompt' => 'по адресу',
                                                        'label' => '',
                                                        'id'=>null
                                                    ];
                                                    echo $form->field($searchModel, 'address_id')->label(false)->dropDownList(ArrayHelper::map($addresses, 'id', 'address'), $params); ?>
                                                    <!--<input   type="text" placeholder="по адресу">-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="count-search-subdata">

                                            <div class="field-row  flex al">

                                                <div class="form-input ">
                                                    <?php
                                                    //$statuses = Status::find()->where(["table"=>"adr_orders"])->all();
                                                    $items = Order::getExistsValues('status_id');

                                                    $params = [
                                                        'prompt' => 'по статусу',
                                                        'label' => '',
                                                        'id'=>null
                                                    ];
                                                    echo $form->field($searchModel, 'status_id')->label(false)->dropDownList($items, $params);
                                                    ?>

                                                   <!-- <select name="" id="">
                                                        <option value="">статусу</option>
                                                        <option value="">точно</option>
                                                        <option value="">точно</option>
                                                    </select>-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dates-from-to">
                                            <div class="field-row from-date-cnt flex al">
                                                <div class="form-label">
                                                    <label for="" class="flex">
                                                        <div class="label-content">датам с</div>
                                                    </label>
                                                </div>
                                                <div class="form-input">
                                                    <input name="start_date" value="<?=trim(strip_tags(@$_GET['start_date']));?>" class="date" placeholder="../../...." type="text">
                                                </div>
                                            </div>
                                            <div class="field-row flex al">
                                                <div class="form-label">
                                                    <label for="" class="flex">
                                                        <div class="label-content">до</div>
                                                    </label>
                                                </div>
                                                <div class="form-input">
                                                    <input name="end_date" value="<?=trim(strip_tags(@$_GET['end_date']));?>"  class="date" placeholder="../../...." type="text">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                            </div>
                            <div class="col-md-2" id="submit-count-search-form">
                                <div class="publish-address">
                                    <div class="public-address">
                                        <!--<a href="">
                                            <div class="public-address-button">
                                                искать
                                            </div>
                                        </a>-->
                                        <?php echo Html::submitButton("искать", ['class' => 'public-address-button']); ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); */?>
                    </div>

                    <div class="row">
                        <div class="row">
                            <div class="col-md-11 table-cnt">
                                <div class="table-self">
                                <table class="table price-table counts-table">
                                    <thead>
                                    <th> номер заказа</th>
                                    <th>дата создания</th>
                                    <th>статус</th>
                                    <th>сумма</th>
                                    <th>операция</th>
                                    <th></th> <!-- for orders -->
                                    </thead>
                                    <tbody>
                                    <?php foreach($dataProvider->getModels() as $order):
                                        //echo "<pre>", print_r($order); die();
                                        //echo "<pre>", print_r($order); die();
                                        //$order = $order->find()->all();
                                        ?>
                                        <tr>

                                            <td><?=$order->number;?></td>
                                            <td><?=date("d.m.Y", strtotime($order->created_at));?></td>
                                            <td><?=$order->status->name;?></td>
                                            <td><?=$order->summa;?></td>
                                            <td>

                                            </td>

                                            <td>
                                                <a href="" class="book-it">удалить</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <!--<tr>

                                        <td>ЦАО</td>
                                        <td>2</td>
                                        <td>счет</td>
                                        <td>25000</td>
                                        <td>

                                        </td>

                                        <td>
                                            <a href="" class="book-it">удалить</a>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>ЦАО</td>
                                        <td>2</td>
                                        <td>счет</td>
                                        <td>25000</td>
                                        <td>

                                        </td>

                                        <td>
                                            <a href="" class="book-it">удалить</a>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>ЦАО</td>
                                        <td>2</td>
                                        <td>счет</td>
                                        <td>25000</td>
                                        <td>

                                        </td>

                                        <td>
                                            <a href="" class="book-it">удалить</a>
                                        </td>
                                    </tr>-->

                                    </tbody>
                                </table>
                                <div class="hr">
                                    <hr>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="pag">
                                    <hr class="pagination-line">
                                    <a class="change-page" data-page="1">
                                        <div class="pagination-arrow pagination-arrow-left">
                                            <img src="/img/triangle_left.png">
                                            <img src="/img/triangle_left.png">
                                        </div>
                                    </a>
                                    <a class="change-page pag-move-left" data-page="1">
                                        <div class="pagination-arrow pagination-arrow-left pagination-space-left">
                                            <img src="/img/triangle_left.png">
                                        </div>
                                    </a>
                                    <!--<div class="pag-stripe"></div>-->
                                    <div class="pager-numbers">
                                        <a class="pag-number">
                                            <span>2</span>
                                        </a>
                                        <a class="pag-number">
                                            <span>3</span>
                                        </a>
                                        <div class="pag-page">

                                            <span class="active-page">1<!--</span><span class="of">&nbsp;/--> </span>
                                        </div>
                                        <a class="pag-number">
                                            <span>2</span>
                                        </a>
                                        <a class="pag-number">
                                            <span>3</span>
                                        </a>
                                    </div>

                                    <!--<div class="pag-stripe"></div>-->
                                    <a class="change-page pag-move-right" data-page="2">
                                        <div class="pagination-arrow pagination-arrow-right">
                                            <img src="/img/triangle_right.png">
                                        </div>
                                    </a>
                                    <a class="change-page" data-page="2">
                                        <div class="pagination-arrow pagination-arrow-right">
                                            <img src="/img/triangle_right.png">
                                            <img src="/img/triangle_right.png">
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>
<div class="filter-pp">
        <div class="filter-pp-area">

        <div class="cabinet-form">
            <div class="close-filter-button">
                <img src="/img/close-menu.png" alt="">
            </div>

            <?php $form = ActiveForm::begin(['action' =>['site/orders'], 'method' => 'get', 'options' => [
                'class' => 'flex'
            ]]);?>
            <div class="col-md-12">

                <div class="order-flex row">

                    <div class="order-flex-right">
                        <div class="">
                            <div class="field-row flex al">

                                <div class="form-input">
                                    <?php echo $form->field($searchModel, 'number')->label(false)->textInput(["placeholder"=>"по номеру"])?>
                                </div>
                            </div>
                            <div class="field-row flex al">

                                <div class="form-input">
                                    <?php
                                    foreach (Order::find()->where(["customer_id"=>Yii::$app->user->identity->id])->all() as $order){
                                        $addresses[] = $order->address;
                                        //echo "<pre>", print_r($order->attributes);
                                    }
                                    // echo "<pre>", print_r($addresses);

                                    ?>
                                    <?php
                                    $params = [
                                        'prompt' => 'по адресу',
                                        'label' => '',
                                        'id'=>null
                                    ];
                                    echo $form->field($searchModel, 'address_id')->label(false)->dropDownList(ArrayHelper::map($addresses, 'id', 'address'), $params); ?>
                                    <!--<input   type="text" placeholder="по адресу">-->
                                </div>
                            </div>
                        </div>
                        <div class="">

                            <div class="field-row  flex al">

                                <div class="form-input ">
                                    <?php
                                    //$statuses = Status::find()->where(["table"=>"adr_orders"])->all();
                                    $items = Order::getExistsValues('status_id');

                                    $params = [
                                        'prompt' => 'по статусу',
                                        'label' => '',
                                        'id'=>null
                                    ];
                                    echo $form->field($searchModel, 'status_id')->label(false)->dropDownList($items, $params);
                                    ?>

                                    <!-- <select name="" id="">
                                         <option value="">статусу</option>
                                         <option value="">точно</option>
                                         <option value="">точно</option>
                                     </select>-->
                                </div>
                            </div>
                        </div>
                        <div class="dates-from-to">
                            <div class="field-row from-date-cnt flex al">
                                <div class="form-label">
                                    <label for="" class="flex">
                                        <div class="label-content">датам с</div>
                                    </label>
                                </div>
                                <div class="form-input">
                                    <input name="start_date" value="<?=trim(strip_tags(@$_GET['start_date']));?>" class="date" placeholder="../../...." type="text">
                                </div>
                            </div>
                            <div class="field-row flex al">
                                <div class="form-label">
                                    <label for="" class="flex">
                                        <div class="label-content">до</div>
                                    </label>
                                </div>
                                <div class="form-input">
                                    <input name="end_date" value="<?=trim(strip_tags(@$_GET['end_date']));?>"  class="date" placeholder="../../...." type="text">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
            <div class="col-md-12" id="submit-count-search-form">
                <div class="filter-apply-block">
                    <div class="publish-address ">
                        <div class="public-address">
                            <!--<a href="">
                                <div class="public-address-button">
                                    искать
                                </div>
                            </a>-->
                            <?php echo Html::submitButton("искать", ['class' => 'public-address-button']); ?>
                        </div>

                    </div>
                    <div class="publish-address ">
                        <div class="clear-filter">
                            <a href="<?=Url::to("/index.php/site/prices"); ?>" class="public-address-button">убрать фильтр</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end();?>
        </div>

        </div>
</div>