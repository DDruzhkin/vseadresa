<?php
use app\components\CabWidget;
?>
<?php $this->params['breadcrumbs'] =
    [


        [
            'label' => 'Username',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => 'профиль',
        ],

    ];


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<section class="cabinet-section">
    <div class="container-fluid cloud-bg no-padding-top-bg">

        <div class="container">
            <div class="row">
                <div class="col-md-2 col-lg-3 add-search-block">
                    <?php echo CabWidget::widget(); ?>
                </div>
                <div class="col-md-8 col-md-offset-1 all-addresses-block">
                    <div class="offset-padding">
                        <h2>Услуги</h2>

                    </div>
                    <div class="row">

                        <div class="row">
                            <div class="col-md-11 table-cnt">
                                <div class="table-self">
                                    <table class="table price-table counts-table">
                                        <thead>
                                        <th> код</th>
                                        <th>название</th>
                                        <th class="flag-th">с</th>
                                        <th class="flag-th">п</th>
                                        <th class="flag-th">р</th>
                                        <th class="flag-th">е</th>
                                        <th style="text-align: center">добавить</th>
                                        <th>цена</th>
                                        <th>ред.</th>
                                        <th>уд.</th>
                                        </thead>
                                        <tbody>
                                        <tr>

                                            <td>253464</td>
                                            <td>15.10.2016</td>
                                            <td></td>
                                            <td class="flag-icon"><i class="fa fa-flag"></i></td>
                                            <td></td>
                                            <td class="flag-icon"><i class="fa fa-flag"></i></td>
                                            <td >
                                                <div class="table-checkbox">
                                                    <div class="form-checkbox"></div>
                                                </div>
                                            </td>
                                            <td>
                                                2000
                                            </td>
                                            <td>
                                                <div class="red-circle">
                                                    <i class="fa fa-pencil"></i>
                                                </div>

                                            </td>
                                            <td>
                                                <div class="red-circle">
                                                    <i class="fa fa-close"></i>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td>253464</td>
                                            <td>15.10.2016</td>
                                            <td class="flag-icon"><i class="fa fa-flag"></i></td>
                                            <td></td>

                                            <td></td>
                                            <td class="flag-icon"><i class="fa fa-flag"></i></td>
                                            <td >
                                                <div class="table-checkbox">
                                                    <div class="form-checkbox"></div>
                                                </div>
                                            </td>
                                            <td>
                                                2000
                                            </td>
                                            <td>
                                                <div class="red-circle">
                                                    <i class="fa fa-pencil"></i>
                                                </div>

                                            </td>
                                            <td>
                                                <div class="red-circle">
                                                    <i class="fa fa-close"></i>
                                                </div>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                    <div class="hr">
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex service-submit">
                            <div class="field-row flex al">
                                <a href="" class="reg-button service-button">
                                    сохранить <br> изменения
                                </a>
                            </div>
                            <div class="field-row flex al add-service-button">
                                <a href="" class="reg-button service-button">
                                    добавить услугу
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>
</section>
