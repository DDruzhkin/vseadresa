<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveField;
use common\models\User;

use dektrium\user\widgets\Connect;




$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="site-signup">
    <h1><?/*= Html::encode($this->title) */?></h1>



    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php /*$form = ActiveForm::begin(['id' => 'form-signup']); */?>

                <?/*= $form->field($model, 'username')->textInput(['autofocus' => true]) */?>

                <?/*= $form->field($model, 'email') */?>

                <?/*= $form->field($model, 'password')->passwordInput() */?>

                <div class="form-group">
                    <?/*= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) */?>
                </div>

            <?php /*ActiveForm::end(); */?>
        </div>
    </div>

</div>
-->
<div class="container-fluid"  id="reg" >

    <div class="container">
        <div style="">
            <!--<div class="row reg-logo">
                <div class="col-md-3">
                    <div class="header-padding-left">
                        <div class="logo">
                            vseadresa<span class="pro">.pro</span>
                        </div>
                        <div class="under-logo-text">
                            портал юридических адресов
                        </div>

                    </div>
                </div>
                <div class="col-md-8">
                    <img src="/img/close.png" class="pull-right close-popup" alt="">
                </div>
            </div>-->
            <div class="row reg-logo">
                <div class="col-md-3">
                    <div class="header-padding-left">
                        <div class="registration-title">
                            регистрация <br>
                            заказчика адреса:
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="flex">
                    <div class="success-action-block">
                        <div class="success-message">
                            регистрация <br>
                            прошла <br>
                            успешно!
                        </div>
                        <div class="thanks">
                            спасибо.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
/*$this->registerJsFile(
    "$('#form-signup').on('afterValidate', function (e, messages) {
    console.log(messages, 'xxxxxxxxxxxxxxxxxxxx');
});",
    \yii\web\View::POS_END,
);*/
?>

