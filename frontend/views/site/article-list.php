<?php $this->params['breadcrumbs'] =
    [

        [
            'label' => 'информация',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => 'статьи',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],


    ];


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>

<section class="addresses">
    <div class="container-fluid">

        <div class="container">

            <div class="row">
                <div class="col-md-2 col-lg-3 add-search-block">
                    <div class="row">
                        <div class="row widget-filter">
                            <div class="col-md-12">
                                <div class="addresses-promo-block">
                                    <div class="col-md-12 promo-information">
                                        <div class="pi-container">
                                            <div class="icon">
                                                <?= Html::img('@web/img/diamond.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                            </div>
                                            <div class="pi-txt">
                                                надежность
                                            </div>
                                            <div class="pi-message">
                                                все адреса  проверены экспертами
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 promo-information">
                                        <div class="pi-container">
                                            <div class="icon">
                                                <?= Html::img('@web/img/banknote.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                            </div>
                                            <div class="pi-txt">
                                                реальная цена!
                                            </div>
                                            <div class="pi-message">
                                                многие собственники предоставили  <br> эксклюзивные скидки для продаж
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 promo-information">
                                        <div class="pi-container">
                                            <div class="icon">
                                                <?= Html::img('@web/img/like.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                            </div>
                                            <div class="pi-txt">
                                                чистота сделок
                                            </div>
                                            <div class="pi-message">
                                                и максимальная скорость
                                                платежей <br> гарантированы
                                                администрацией портала
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>




                <div class="col-md-8 col-md-offset-1">
                    <div class="offset-padding">
                    <div class="">
                        <div class="all-addresses-block">
                            <div class="row article-item">
                                <div class="col-md-11">
                                    <div class="article-header">
                                        Cтатьи
                                    </div>
                                </div>
                                <div class="col-md-1">

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row article-item">
                        <div class="col-md-11">
                            <div class="article-line">
                                <a href="">Регистрация юридического адреса по месту жительства учредителя или директора</a>
                            </div>
                            <div class="article-line">
                                <a href="<?=Url::to("/index.php/site/article")?>">Что делать, если в юридическом адресе появилась ошибка
                                </a>
                            </div>
                            <div class="article-line">
                                <a href="">Изменение юридического адреса в 2017 году
                                </a>
                            </div>
                            <div class="article-line">
                                <a href="">Договор аренды с предоставлением юридического адреса. Образец
                                </a>
                            </div>
                            <div class="article-line">
                                <a href="">Новая форма Р13001. Образец заполнения при смене юридического адреса. </a>
                            </div>
                            <div class="article-line">
                                <a href="">Информационное письмо о смене юридического адреса. Образец
                                </a>
                            </div>
                            <div class="article-line">
                                <a href="">Решение о смене юридического адреса ООО. Образец

                                </a>
                            </div>
                            <div class="article-line">
                                <a href="">Смена юридического адреса со сменой налоговой</a>
                            </div>
                            <div class="article-line">
                                <a href="">Смена юридического адреса. Изменения в устав ООО. Образец</a>
                            </div>
                            <div class="article-line">
                                <a href="">Документы для регистрации нового юридического адреса ООО</a>
                            </div>
                            <div class="article-line">
                                <a href="">Как проверить адрес массовой регистрации юридических лиц</a>
                            </div>
                            <div class="article-line">
                                <a href="">Смена юридического адреса</a>

                            </div>
                            <div class="article-line">
                                <a href="">Смена юридического адреса: пошаговая инструкция</a>
                            </div>
                            <div class="article-line">
                                <a href="">Что такое юридический адрес? </a>
                            </div>
                            <div class="line-header uppercase">
                                надо знать про юридический адрес
                            </div>
                            <div class="article-line">
                                <a href="">Протокол общего собрания собственников ООО при смене юридического адреса</a>
                            </div>
                            <div class="article-line">
                                <a href="">Как правильно написать юридический адрес
                                </a>
                            </div>
                            <div class="article-line">
                                <a href="">ФНС России сообщила об исключении из ЕГРЮЛ более 230 тысяч недостоверных компаний за 1-ое полугодие 2017 г.
                                </a>
                            </div>
                            <div class="article-line">
                                <a href="">Подделка или реальный адрес </a>
                            </div>
                            <div class="line-header uppercase">
                                как выбрать юридический адрес
                            </div>
                            <div class="article-line">
                                <a href="">Как проверить надежность массового адреса
                                </a>
                            </div>
                            <div class="article-line">
                                <a href="">Как проверить адрес массовой регистрации юридических лиц
                                </a>
                            </div>
                        </div>

                    </div>
                    </div>
                </div>



                <div class="row">
                <div class="col-md-8 col-md-offset-4 col-xs-12">
                    <div class="offset-padding">
                    <div class="article">
                        <div class="">
                            <!--<div class="row">-->
                                <hr class="article-hr" />
                            <!--</div>-->
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>

