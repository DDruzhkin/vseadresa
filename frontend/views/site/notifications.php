<!-- ruslan -->
<?php
use app\components\CabWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\NotificationsVsStatusesSettings;
?>
<?php $this->params['breadcrumbs'] =
    [


        [
            'label' => 'Username',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => 'профиль',
        ],

    ];



?>
<section class="cabinet-section">
    <div class="container-fluid cloud-bg no-padding-top-bg">
        <?php $form = ActiveForm::begin(['method' => 'post']); ?>
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-lg-3 add-search-block">

                    <?php // echo CabWidget::widget(); ?>

                </div>
                <div class="col-md-8 col-md-offset-1 all-addresses-block">
                    <div class="offset-padding">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="">
                                    <h2>Настройки уведомлений</h2>
                                    <div class="field-row flex al" id="notification-email">
                                        <div class="form-label">
                                            <label for="" class="flex">
                                                <div class="label-content">email</div>
                                            </label>
                                        </div>
                                        <div class="form-input">
                                            <?= Html::input('text', 'defaultEmail',
                                                $user['notification_email'])?>
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label">
                                            <label for="" class="flex">
                                        <span class="label-content">
                                            высылать уведомления?
                                        </span>
                                            </label>
                                        </div>
                                        <div class="form-input hide-input">
                                            <div class="form-checkbox
                                            <?php  if($user['notification_enabled'] == 'yes'): ?>
                                                        active-checkbox
                                            <?php endif; ?>
"></div>
                                            <?= Html::checkbox('defaultEnabled',
                                                $user['notification_enabled'] == 'yes')?>
                                        </div>
                                    </div>
                                    <div class="labels cabinet-label">
                                        Уведомления по заказам
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">

                                <div class="col-md-11 table-cnt">
                                    <div class="table-self">
                                        <table class="table price-table counts-table">
                                            <thead>
                                            <th>Статус</th>
                                            <th style="text-align: center">уведолмения включены</th>
                                            <th>email для уведомлений</th>

                                            </thead>
                                            <tbody>

                                            <?php foreach ($statusesSettings as $settings) {

                                                if (in_array($settings['name'],
                                                    NotificationsVsStatusesSettings::$exceptionStatusNames))
                                                    continue;
                                                $id = $settings['status_id']; ?>
                                                <tr>
                                                <td>
                                                    <?= $settings['name']?>
                                                </td>
                                                    <td class="table-checkbox">


                                                    <div class="form-checkbox
                                                    <?php  if($settings['enabled'] == 'yes'): ?>
                                                        active-checkbox
                                                    <?php endif; ?>

                                                    "></div>




                                                        <?= Html::checkbox("enabled[$id]",
                                                            $settings['enabled'] == 'yes')?>
                                                    </td>


                                                    <td class="td-notification-email"><?= Html::input('text', "email[$id]",
                                                            $settings['email'])?></td>
                                                </tr>
                                            <?php }?>



                                           <!-- <tr>

                                                <td>Счет выставлен</td>
                                                <td class="table-checkbox"><div class="form-checkbox"></div></td>
                                                <td class="td-notification-email">qwerty@mail.ru</td>

                                            </tr>
                                            <tr>

                                                <td>Частично оплачен</td>
                                                <td class="table-checkbox"><div class="form-checkbox"></div></td>
                                                <td class="td-notification-email">qwerty@mail.ru</td>

                                            </tr>
                                            <tr>

                                                <td>Оплачен</td>
                                                <td class="table-checkbox"><div class="form-checkbox"></div></td>
                                                <td class="td-notification-email">qwerty@mail.ru</td>

                                            </tr>
                                            <tr>

                                                <td>Формирование</td>
                                                <td class="table-checkbox"><div class="form-checkbox"></div></td>
                                                <td class="td-notification-email">qwerty@mail.ru</td>

                                            </tr>-->
                                            </tbody>
                                        </table>
                                        <div class="hr">
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="">
                                    <div class="labels cabinet-label">
                                        Уведомления по адресам
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <div class="col-md-11 table-cnt">
                                    <div class="table-self">
                                    <table class="table price-table counts-table">
                                        <thead>
                                        <th>Статус</th>
                                        <th style="text-align: center">уведолмения включены</th>
                                        <th>email для уведомлений</th>

                                        </thead>
                                        <tbody>
                                        <tr>

                                            <td>Счет выставлен</td>
                                            <td class="table-checkbox"><div class="form-checkbox"></div></td>
                                            <td class="td-notification-email">qwerty@mail.ru</td>

                                        </tr>
                                        <tr>

                                            <td>Частично оплачен</td>
                                            <td class="table-checkbox"><div class="form-checkbox"></div></td>
                                            <td class="td-notification-email">qwerty@mail.ru</td>

                                        </tr>
                                        <tr>

                                            <td>Оплачен</td>
                                            <td class="table-checkbox"><div class="form-checkbox"></div></td>
                                            <td class="td-notification-email">qwerty@mail.ru</td>

                                        </tr>
                                        <tr>

                                            <td>Формирование</td>
                                            <td class="table-checkbox"><div class="form-checkbox"></div></td>
                                            <td class="td-notification-email">qwerty@mail.ru</td>

                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="hr">
                                        <hr>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field-row flex al">
                            <!--<a href="" class="reg-button notiications-button">
                                сохранить
                            </a>-->
                            <?= Html::submitButton('сохранить', ['class' => 'reg-button notiications-button', 'name' => 'login-button']) ?>

                        </div>
                    </div>

                </div>
            </div>


        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <!--alexey-->
<?php /*
<section class="cabinet-section">
    <div class="container-fluid">

        <div class="container">
            <div class="row">
                <div class="col-md-2 col-lg-3 add-search-block">

                    <?php try {
                        echo CabWidget::widget();
                    } catch (\Exception $e) {
                        echo $e->getMessage();
                    }?>

                </div>
                <div class="col-md-8  col-md-offset-1 all-addresses-block">

                    <div class="col-md-10">
                        <div class="cabinet-form">

                            <?php $form = ActiveForm::begin(['method' => 'post']); ?>
                            <div class="field-row flex al">
                                <div class="form-label">
                                    <label for="" class="flex">
                                        <span class="label-content">
                                            email для уведомлений по умолчанию
                                        </span>
                                    </label>
                                </div>
                                <div class="form-input">
                                    <?= Html::input('text', 'defaultEmail',
                                        $user['notification_email'])?>
                                </div>
                            </div>
                            <div class="field-row flex al">
                                <div class="form-label">
                                    <label for="" class="flex">
                                        <span class="label-content">
                                            высылать уведомления?
                                        </span>
                                    </label>
                                </div>
                                <div class="form-input">
                                    <?= Html::checkbox('defaultEnabled',
                                        $user['notification_enabled'] == 'yes')?>
                                </div>
                            </div>
                            <div id="notification-statuses" class="field-row flex al">
                                <div class="form-label">
                                    <label for="" class="flex">
                                        <span class="label-content">статусы</span>
                                    </label>
                                </div>
                                <div class="form-input">
                                    <?php foreach ($statusesSettings as $settings) {
                                        if (in_array($settings['name'],
                                            NotificationsVsStatusesSettings::$exceptionStatusNames))
                                            continue;
                                        $id = $settings['status_id']; ?>
                                        <div>
                                            <?= $settings['name']?>
                                            <?= Html::checkbox("enabled[$id]",
                                                $settings['enabled'] == 'yes')?>
                                            <?= Html::input('text', "email[$id]",
                                                $settings['email'])?>
                                        </div>
                                    <?php }?>
                                </div>
                            </div>
                            <div class="field-row flex al">
                                <div class="form-label">
                                    <label for="" class="flex">
                                        <?= Html::submitButton('Сохранить')?>
                                    </label>
                                </div>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 */?>

<!--end alexey-->
