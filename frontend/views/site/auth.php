<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid popup" id="login" >
    <div class="container">
        <!--<div class="close-popup-circle">
            <img src="/img/close.png" class="close-popup" alt="">
        </div>-->
        <!--<div class="row login-logo">
            <div class="col-md-3">
                <div class="header-padding-left">
                    <div class="logo">
                        vseadresa<span class="pro">.pro</span>
                    </div>
                    <div class="under-logo-text">
                        портал юридических адресов
                    </div>

                </div>
            </div>
            <div class="col-md-8">
                <img src="/img/close.png" class="pull-right close-popup" alt="">
            </div>
        </div>-->
        <div class="row login-logo">
            <div class="col-md-3">
                <div class="header-padding-left">
                    <div class="login-title">
                        авторизация:
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-lg-4 login-tutorial">
                <div class="flex column">
                    <!--<div>
                        <a  class="restore-popup fancybox" href="#restore-pwd">восстановить <br> пароль</a>
                    </div>
                    <div>
                        <a href="#reg" class="reg-popup fancybox">регистрация <br>
                            заказчика адреса</a>
                    </div>
                    <div>
                        <a href="#reg" class="reg-popup fancybox">регистрация <br>
                            заказчика адреса</a>
                    </div>-->

                    <div>
                        <a  href="<?=Url::to("/index.php/site/restore_pwd")?>">восстановить <br> пароль</a>
                    </div>
                    <div>
                        <a href="<?=Url::to("/index.php/site/client-signup")?>" class="reg-popup">регистрация <br>
                            заказчика адреса</a>
                    </div>
                    <div>
                        <a href="<?=Url::to("/index.php/site/owner-signup")?>" class="reg-popup">регистрация <br>
                            владельца адреса</a>
                    </div>
                </div>

            </div>
            <div class="col-md-5 col-lg-6">

                <div class="col-md-12">
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">e-mail</div>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(false); ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">пароль</div>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $form->field($model, 'password')->passwordInput()->label(false); ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <!--<a href="" class="login-button">-->
                        <?= Html::submitButton('Войти', ['class' => 'login-button', 'name' => 'login-button']) ?>
                    <!--</a>-->
                </div>
                <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
        <div class="row hidden">
            <div class="flex">
                <div class="success-action-block">
                    <div class="success-message">
                        авторизация <br>
                        прошла <br>
                        успешно!
                    </div>
                    <div class="thanks">
                        спасибо.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>