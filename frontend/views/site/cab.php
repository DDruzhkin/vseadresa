<?php
use app\components\CabWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveField;
use yii\helpers\Html;
?>
<?php $this->params['breadcrumbs'] =
    [


        [
            'label' => 'Username',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => 'профиль',
        ],

    ];



?>
<section class="cabinet-section">
        <div class="container-fluid cloud-bg no-padding-top-bg">

            <div class="container">
                <div class="row">
                    <div class="col-md-3 add-search-block">

                        <?php echo CabWidget::widget(); ?>

                    </div>
                    <div class="col-md-8 desc-right all-addresses-block">
                        <div class="offset-padding">
                        <div class="cab-owner-cnt">
                            <div class="cabinet-form">
                                <?php $form = ActiveForm::begin([
                                    'id' => 'form-signup',
                                    'enableAjaxValidation' => true,
                                    'enableClientValidation'=>true

                                ]); ?>
                                <div class="flex al forms-head">
                                    <h2>Профиль</h2>

                                        <div class="reg-tutorial cab-text-info">
                                            <div class="flex">
                                                <sup>*</sup>
                                                <div>помечены поля, <br>
                                                    обязательные для заполнения</div>
                                            </div>

                                        </div>
                                </div>
                                    <div class="field-row flex al">
                                        <div class="form-label">
                                            <label for="" class="flex">
                                                <div class="label-content">фамилия, имя</div>
                                                <sup>*</sup>
                                            </label>
                                        </div>
                                        <div class="form-input">
                                            <?php echo $form->field($user, 'fullname')->label(false)->textInput(['value' => $user->fullname]) ?>
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label">
                                            <label for="" class="flex">
                                                <div class="label-content">email</div>
                                                <sup>*</sup>
                                            </label>
                                        </div>
                                        <div class="form-input">
                                            <?php echo $form->field($user, 'username')->label(false)->textInput(['value' => $user->username]) ?>
                                        </div>
                                        <div class="field-subrow">
                                            является уникальным
                                            идентификатором пользователя
                                            в системе и используется
                                            для авторизации
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label">
                                            <label for="" class="flex">
                                                <div class="label-content">телефон</div>
                                                <sup>*</sup>
                                            </label>
                                        </div>
                                        <div class="form-input">
                                            <?php echo $form->field($user, 'phone')->label(false)->textInput(['value' => $user->phone]) ?>
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label">
                                            <label for="" class="flex">
                                                <div class="label-content">правовой статус</div>
                                                <sup>*</sup>
                                            </label>
                                        </div>
                                        <div class="form-input">
                                            <!-- правовой статус -->
                                            <?=$user->form?>
                                        </div>
                                    </div>

                                    <div class="field-row flex al">
                                        <div class="form-label">
                                            <label for="" class="flex">
                                                <div class="label-content">название организации</div>
                                                <sup>*</sup>
                                            </label>
                                        </div>
                                        <div class="form-input">
                                            <input type="text">
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label">
                                            <label for="" class="flex">
                                                <div class="label-content">название ИП</div>
                                                <sup>*</sup>
                                            </label>
                                        </div>
                                        <div class="form-input">
                                            <input type="text">
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <a href="" class="reg-button">
                                            изменить данные
                                        </a>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label">
                                            <label for="" class="flex">
                                                <div class="label-content">старый пароль</div>
                                            </label>
                                        </div>
                                        <div class="form-input">
                                            <input type="text">
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label">
                                            <label for="" class="flex">
                                                <div class="label-content">новый пароль</div>
                                            </label>
                                        </div>
                                        <div class="form-input">
                                            <input type="text">
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label">
                                            <label for="" class="flex">
                                                <div class="label-content">новый пароль повторно</div>
                                            </label>
                                        </div>
                                        <div class="form-input">
                                            <input type="text">
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <a href="" class="reg-button">
                                            изменить данные
                                        </a>
                                    </div>
                                <?php ActiveForm::end(); ?>
                            </div>

                        </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</section>
