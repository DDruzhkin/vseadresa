<?php
use app\components\CabWidget;
?>
<?php $this->params['breadcrumbs'] =
    [


        [
            'label' => 'Username',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => 'профиль',
        ],

    ];


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<section class="cabinet-section">
    <div class="container-fluid cloud-bg no-padding-top-bg">

        <div class="container">
            <div class="row">
                <div class="col-md-3 add-search-block">

                    <?php echo CabWidget::widget(); ?>

                </div>
                <div class="col-md-8 col-md-offset-1 all-addresses-block address-publish-card">
                    <div class="offset-padding">
                    <div class="col-md-10 row">
                        <div class="flex">
                            <div class="address-detail-head">
                                Малый Харитоньевский пер., д.7 стр.1
                            </div>

                        </div>

                        <div class="cabinet-form">
                            <form action="">
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">местонахождение</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">индекс</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">номер налоговой</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <select  class="form-control" aria-required="true" aria-invalid="false">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">станция метро</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <select  class="form-control" aria-required="true" aria-invalid="false">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="field-row more-button flex al">
                                    <a href="" class="reg-button">
                                        сохранить
                                    </a>
                                </div>
                            </form>


                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11">
                            <div class="sld" id="sld">
                                    <div class="image-to-change add-padding-right">
                                        <img class="img-responsive" src="/img/house.png" />
                                    </div>
                                    <div class="address-slider">
                                        <ul class="sl">
                                            <li class="lense-search"><img class="gh" src="/img/sl1.png"></li>
                                            <li class="lense-search"><img class="gh" src="/img/sl2.png"></li>
                                            <li class="lense-search"><img class="gh" src="/img/sl3.png"></li>
                                            <li class="lense-search"><img class="gh" src="/img/sl4.png"></li>
                                            <li class="lense-search"><img class="gh" src="/img/sl1.png"></li>
                                            <li class="lense-search"><img class="gh" src="/img/sl2.png"></li>
                                            <li class="lense-search"><img class="gh" src="/img/sl3.png"></li>
                                            <li class="lense-search"><img class="gh" src="/img/sl4.png"></li>
                                        </ul>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-11" id="map-block">
                            <div class="add-padding-right">
                                <div id="map" data-lat="55.766530" data-lng="37.652377"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11">
                            <div class="labels cabinet-label">
                                адрес доставки документов
                            </div>
                        </div>
                    </div>

                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 desc-right all-addresses-block address-publish-card">
                <div class="row">
                    <div class="col-md-11 no-pl table-cnt">
                        <div class="table-self">
                            <table class="table price-table address-card-table">
                                <thead>
                                <tr><th> No</th>
                                    <th>дата</th>
                                    <th>название</th>
                                    <th>статус</th>
                                    <th>скачать</th>
                                </tr></thead>
                                <tbody>



                                <tr>

                                    <td>ЦАО</td>
                                    <td>2</td>
                                    <td>счет</td>
                                    <td>25000</td>
                                    <td>
                                        <a href="" class="download-it">pdf</a>
                                    </td>


                                </tr>
                                </tbody>
                            </table>
                            <div class="hr">
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-8 col-xs-12 pull-right all-addresses-block address-publish-card documentsaddress">
                    <div class="offset-padding">

                    <div class="row">
                        <div class="col-md-11">
                            <div class="labels cabinet-label">
                                документы адреса
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <div class="cabinet-form">
                                <form action="">
                                    <div class="field-row flex al">
                                        <div class="form-label">
                                            <label for="" class="flex no-checkbox">
                                                <div class="label-content">6 месяцев</div>
                                            </label>
                                        </div>
                                        <div class="form-input flex al">
                                            <input type="text">
                                            <div class="address-card-valute">
                                                <i class="fa fa-rub" aria-hidden="true"></i>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label">
                                            <label for="" class="flex no-checkbox">
                                                <div class="label-content">11 месяцев</div>
                                            </label>
                                        </div>
                                        <div class="form-input flex al">
                                            <input type="text">
                                            <div class="address-card-valute">
                                                <i class="fa fa-rub" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label">
                                            <label for="" class="flex no-checkbox">
                                                <div class="label-content">площадь адреса</div>
                                            </label>
                                        </div>
                                        <div class="form-input flex al">
                                            <input type="text">
                                            <div class="address-card-valute">
                                                <span>м</span> <sup>2</sup>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label flex">
                                            <div class="form-checkbox"></div>
                                            <label for="" class="flex">
                                                <div class="label-content">новый</div>
                                            </label>
                                        </div>

                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label flex">
                                            <div class="form-checkbox"></div>
                                            <label for="" class="flex">
                                                <div class="label-content">уборка помещения</div>
                                            </label>
                                        </div>
                                        <div class="form-icon flex">
                                            <img src="/img/cleaning.png" alt="">
                                        </div>
                                        <div class="form-input flex al has-icon">
                                            <input type="text">
                                            <div class="address-card-valute">
                                                <i class="fa fa-rub" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label flex">
                                            <div class="form-checkbox"></div>
                                            <label for="" class="flex">
                                                <div class="label-content">охрана</div>
                                            </label>
                                        </div>
                                        <div class="form-icon flex">
                                            <img src="/img/guard.png" alt="">
                                        </div>
                                        <div class="form-input flex al has-icon">
                                            <input type="text">
                                            <div class="address-card-valute">
                                                <i class="fa fa-rub" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label flex al">
                                            <div class="form-checkbox"></div>
                                            <label for="" class="flex">
                                                <div class="label-content">почтовое <br> обслуживание</div>
                                            </label>
                                        </div>
                                        <div class="form-icon flex">
                                            <img src="/img/email_copy.png" alt="">
                                        </div>
                                        <div class="form-input flex al has-icon">
                                            <input type="text">
                                            <div class="address-card-valute">
                                                <i class="fa fa-rub" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label flex al">
                                            <div class="form-checkbox"></div>
                                            <label for="" class="flex">
                                                <div class="label-content">рабочее место</div>
                                            </label>
                                        </div>
                                        <div class="form-icon flex">
                                            <img src="/img/home_copy.png" alt="">
                                        </div>
                                        <div class="form-input flex al has-icon">
                                            <input type="text">
                                            <div class="address-card-valute">
                                                <i class="fa fa-rub" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label flex al">
                                            <div class="form-checkbox"></div>
                                            <label for="" class="flex">
                                                <div class="label-content">оплата по безналу</div>
                                            </label>
                                        </div>
                                        <div class="form-icon flex">
                                            <img src="/img/no_cash.png" alt="">
                                        </div>
                                        <div class="form-input flex al has-icon">
                                            <input type="text">
                                            <div class="address-card-valute">
                                                <i class="fa fa-rub" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label flex al">
                                            <div class="form-checkbox"></div>
                                            <label for="" class="flex">
                                                <div class="label-content">подтверждение</div>
                                            </label>
                                        </div>
                                        <div class="form-icon flex">
                                            <img src="/img/hand-icon.png" alt="">
                                        </div>
                                        <div class="form-input flex al has-icon">
                                            <input type="text">
                                            <div class="address-card-valute">
                                                <i class="fa fa-rub" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field-row flex al">
                                        <div class="form-label flex al">
                                            <div class="form-checkbox"></div>
                                            <label for="" class="flex">
                                                <div class="label-content">принятие банков-<br>
                                                    ских проверок
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-icon flex">
                                            <img src="/img/card_copy.png" alt="">
                                        </div>
                                        <div class="form-input flex al has-icon">
                                            бесплатно
                                        </div>
                                    </div>
                                    <div class="field-row flex al icon-row">
                                        <div class="form-label flex al">
                                            <div class="form-checkbox"></div>
                                            <label for="" class="flex">
                                                <div class="label-content">гарантия<br>
                                                    от собственника
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-icon flex">
                                            <img src="/img/warrant_copy.png" alt="">
                                        </div>

                                    </div>
                                    <div class="field-row flex al icon-row">
                                        <div class="form-label flex al">
                                            <div class="form-checkbox"></div>
                                            <label for="" class="flex">
                                                <div class="label-content">юридический адрес
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-icon flex">
                                            <img src="/img/legal_icon.png" alt="">
                                        </div>

                                    </div>
                                    <div class="field-row flex al icon-row">
                                        <div class="form-label flex al">
                                            <div class="form-checkbox"></div>
                                            <label for="" class="flex">
                                                <div class="label-content">адрес не <br>
                                                    является массовым
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-icon flex">
                                            <img src="/img/compass_small.png" alt="">
                                        </div>

                                    </div>
                                    <div class="field-row flex al icon-row">
                                        <div class="form-label flex al">
                                            <div class="form-checkbox"></div>
                                            <label for="" class="flex">
                                                <div class="label-content">пролонгация <br>
                                                    договора
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-icon flex">
                                            <img src="/img/prolong.png" alt="">
                                        </div>

                                    </div>
                                    <div class="field-row flex al icon-row">
                                        <div class="form-label flex al">
                                            <div class="form-checkbox"></div>
                                            <label for="" class="flex">
                                                <div class="label-content">содействие <br>
                                                    собственника <br>
                                                    в решении <br>
                                                    вопрос ИФНС
                                                </div>
                                            </label>
                                        </div>
                                        <div class="form-icon flex">
                                            <img src="/img/prate.png" alt="">
                                        </div>

                                    </div>

                                    <div class="field-row flex al">
                                        <div class="form-label flex al">
                                            <a href="" class="reg-button service-button pay-button">
                                                оплатить <br> публикацию адреса
                                            </a>
                                        </div>


                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    </div>

                </div>
            </div>


        </div>
    </div>
</section>
<?php $this->registerJsFile("/js/map.js", ["position"=>\yii\web\View::POS_END]); ?>