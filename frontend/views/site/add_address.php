<?php
use app\components\CabWidget;
?>
<?php $this->params['breadcrumbs'] =
    [


        [
            'label' => 'Username',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => 'профиль',
        ],

    ];

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<section class="cabinet-section">
    <div class="container-fluid  cloud-bg no-padding-top-bg">

        <div class="container">
            <div class="row">
                <div class="col-md-3 add-search-block">

                    <?php echo CabWidget::widget(); ?>

                </div>
                <div class="col-md-8 col-md-offset-1 all-addresses-block address-add-page">
                    <div class="col-md-10">
                        <div class="address-add-page-title">
                            <span class="address-add-step-color">1</span> создание нового адреса
                        </div>
                        <div class="cabinet-form">
                            <form action="">
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">местонахождение</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">индекс</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">номер налоговой</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <select id="signupform-form" class="form-control"  aria-required="true" aria-invalid="false">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">станция метро</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <select id="signupform-form" class="form-control"  aria-required="true" aria-invalid="false">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="field-row more-button flex al">
                                    <a href="<?=Url::to("/index.php/site/add_addressstep2");?>" class="reg-button">
                                        далее
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>
