<?php
/* @var $this yii\web\View */
use frontend\components\NewsWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
$this->title = 'My Yii Application';
?>
<div class="mobile-accordeon-block">

</div>
<section class="visible-xs mobile-address-wrapper">
    <div class="container-fluid">
        <div class="container address-mobile-section">

        </div>
    </div>
</section>
<section class="search-section">
    <div class="container-fluid">
        <div class="container">
            <div class="address-search">
                <div class="tab-block row">
                    <div class="col-md-3 no-padding switch-left">
                        <div class="tab active-tab distr-tab" data-block="district">
                            по округу
                        </div>
                        <div class="district-mobile-container"></div>
                    </div>
                    <div class="col-md-3 no-padding switch-mode">
                        <div class="tab tax-tab" data-block="tax">
                            по номеру налоговой
                        </div>
                        <div class="tax-mobile-container"></div>
                    </div>
                    <div class="col-md-3 no-padding switch-mode">
                        <div class="tab subway-tab" data-block="subway">
                            по метро
                        </div>
                        <div class="subway-mobile-container"></div>
                    </div>
                    <div class="col-md-3 no-padding switch-right">
                        <div class="tab other-tab" data-block="other">
                            другие параметры
                        </div>
                        <div class="other-mobile-container"></div>
                    </div>
                </div>
                <div class="tab-content">
                <div class="row search-way district display">
                    <?php $form = ActiveForm::begin(['action' =>['address/index'], 'method' => 'get']);?>
                    <div class="flex search-form">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="checkbox-block row">
                            <div class="checkbox-container">
                            <?php $district_array = $districts::find()->all();
                            $district_array = ArrayHelper::map($district_array,'id','abr');
                                //echo "<pre>", print_r($district_array); die();
                            ?>
                            <?php foreach ($district_array as $key=>$district): ?>

                                <div class="col-md-3 col-xs-4 check-box">
                                    <div class="checkbox">
                                        <div class="checkbox-item">
                                            <?php echo $form->field($searchModel, 'okrug_id[]')->checkbox(['uncheck'=>null, "required"=>false, 'value' =>$key, 'class'=>'hidden-cb', 'id'=>null], false)->label(false)?>
                                        </div>
                                        <div class="checkbox-value">

                                            <?php echo $district; ?>
                                        </div>

                                    </div>
                                </div>
                            <?php endforeach; ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="checkbox-block">
                        <div class="public-address">
                            <?php echo Html::submitButton("найти", ['class' => 'find']); ?>
                        </div>
                        </div>
                    </div>
                    </div>
                    <?php ActiveForm::end();?>
                </div>
                <div class="row search-way tax">
                    <?php $form = ActiveForm::begin(['action' =>['address/index'], 'method' => 'get']);?>
                    <div class="flex search-form">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="checkbox-block row">
                            <div class="checkbox-container">
                                <?php $tax_array = $taxes::find()->all();
                                $tax_array = ArrayHelper::map($tax_array,'id','number');
                               //  echo "<pre>", print_r($searchModel);
                                ?>
                                <?php foreach ($tax_array as $key=>$tax): ?>
                                    <div class="col-md-3 col-xs-4 check-box">
                                        <div class="checkbox">
                                            <div class="checkbox-item">
                                               <?php echo $form->field($searchModel, 'nalog_id[]')->checkbox(['uncheck'=>null, "required"=>false, 'value' => $key, 'class'=>'hidden-cb', 'id'=>null], false)->label(false)?>
                                            </div>
                                            <div class="checkbox-value">

                                                <?=$tax?>
                                            </div>

                                        </div>
                                    </div>
                                 <?php endforeach; ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="checkbox-block">
                            <div class="public-address">
                                <?php echo Html::submitButton("найти", ['class' => 'find']); ?>
                            </div>
                        </div>
                    </div>
                    </div>
                    <?php ActiveForm::end();?>
                </div>
                <div class="row search-way subway">
                    <?php $form = ActiveForm::begin(['action' =>['address/index'], 'method' => 'get']);?>
                    <div class="col-md-8 col-md-offset-1">
                        <div class="checkbox-block row">

                            <?php $metro_array = $metro::find()->all(); ?>
                            <?php $items = ArrayHelper::map($metro_array,'id','name');

                            $params = [
                                'prompt' => 'станция метро',
                                'class' => 'metro',
                                'label' => '',
                                'id'=>null
                            ];
                            echo $form->field($searchModel, 'metro_id')->label(false)->dropDownList($items,$params);
                            ?>


                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="checkbox-block">
                            <div class="public-address">
                                <?php echo Html::submitButton("найти", ['class' => 'find']); ?>
                                <!--<div class="find">
                                    найти
                                </div>-->
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end();?>
                </div>
                <div class="row search-way other">
                    <?php $form = ActiveForm::begin(['action' =>['address/index'], 'method' => 'get']);?>
                    <div class="flex search-form">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="checkbox-block row">

                            <select name="other[option][1184]" id="rent" class="metro" style="margin-top: 0">
                                <option value="0">Возможность реальной аренды</option>
                                <option value="on">С реальной арендой</option>
                                <option value="off">Без реальной аренды</option>
                            </select>
                            <select name="other[option][1173]"  class="metro">
                                <option value="0">Наличие почтового обслуживания</option>
                                <option value="on">С почтовый обслуживанием</option>
                                <option value="off">Без почтового обслуживания</option>
                            </select>
                            <select name="other[square]"  class="metro">
                                <option value="0">площадь</option>
                                <option value="0-20">0..20 кв.м</option>
                                <option value="20-50">20..50 кв.м</option>
                                <option value="50-100">50..100 кв.м</option>
                                <option value="100">больше 100 кв.м</option>
                            </select>

                        </div>
                    </div>
                        <div class="col-md-3">
                            <div class="checkbox-block">
                                <div class="public-address">
                                    <?php echo Html::submitButton("найти", ['class' => 'find']); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end();?>
                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid promo-infoblock">
        <div class="container narrow">
            <div class="row">
                <div class="col-md-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/shop.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            <?=$address_count;?>
                        </div>
                        <div class="pi-message">
                            адресов <br> в системе
                        </div>
                    </div>
                </div>
                <div class="col-md-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/user.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            <?=$owner_cnt?>
                        </div>
                        <div class="pi-message">
                            владельцев адресов <br> в системе
                        </div>
                    </div>
                </div>
                <div class="col-md-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/note.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            <?= $document_cnt; ?>
                        </div>
                        <div class="pi-message">
                            заключенных <br> договоров
                        </div>
                    </div>
                </div>
            </div>
            <div class="row second-promo-row">
                <div class="col-md-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/diamond.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            надежность
                        </div>
                        <div class="pi-message">
                            все адреса <br> проверены <br> экспертами
                        </div>
                    </div>
                </div>

                <div class="col-md-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/banknote.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            реальная цена!
                        </div>
                        <div class="pi-message">
                            многие собственники <br> предоставили эксклюзивные <br> скидки для продаж <br> на этом портале
                        </div>
                    </div>
                </div>
                <div class="col-md-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/like.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            чистота сделок
                        </div>
                        <div class="pi-message">
                            и максимальная скорость <br>
                            платежей гарантированы  <br>
                            администрацией портала <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


   <?= \frontend\components\ReviewsWidget::widget()?>


    <div class="container-fluid">
        <div class="container">
            <div class="public-review">
                <a href="">
                    <div class="public-answer-question">
                        еще вопросы <br> и ответы на них
                    </div>
                </a>
            </div>
        </div>
    </div>

       <?= NewsWidget::widget();?>
</section>