<?php
use app\components\Search_promoWidget;
?>
<?php $this->params['breadcrumbs'] =
    [

        [
            'label' => 'виджеты',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links"></span>'.PHP_EOL,
        ],


    ];


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>

<section class="addresses">
    <div class="container-fluid">
        <div class="col-md-3 add-search-block">
            <div class="add-searcher">
                <span>поиск адреса:</span>

            </div>
            <?php echo Search_promoWidget::widget(['arr' => array(
                "metro"=>$metro,
                "searchModel"=>$searchModel,
                "taxes"=>$taxes,
                "districts"=>$districts,
                "services"=>$services,
                "document_cnt"=>$document_cnt)]); ?>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flex">
                        <div class="widget-small-block"></div>
                        <div class="widget-large-block"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>