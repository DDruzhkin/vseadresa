<?php
use app\components\ArticlesWidget;
?>
<?php $this->params['breadcrumbs'] =
		[

			[
				'label' => 'информация',
                'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
			],
            [
                'label' => 'статьи',
                'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
            ],
            [
                'label' => 'что делать если ...',
            ],
		
		        ];


		        use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>

<section class="addresses">
    <div class="container-fluid">

        <div class="container">

                <div class="row">
                    <div class="col-md-2 col-lg-3 add-search-block">
                        <div class="row">
                            <div class="row widget-filter">
                                <div class="col-md-12">
                                    <div class="addresses-promo-block">
                                        <div class="col-md-12 promo-information">
                                            <div class="pi-container">
                                                <div class="icon">
                                                    <?= Html::img('@web/img/diamond.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                                </div>
                                                <div class="pi-txt">
                                                    надежность
                                                </div>
                                                <div class="pi-message">
                                                    все адреса  проверены экспертами
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 promo-information">
                                            <div class="pi-container">
                                                <div class="icon">
                                                    <?= Html::img('@web/img/banknote.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                                </div>
                                                <div class="pi-txt">
                                                    реальная цена!
                                                </div>
                                                <div class="pi-message">
                                                    многие собственники предоставили  <br> эксклюзивные скидки для продаж
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 promo-information">
                                            <div class="pi-container">
                                                <div class="icon">
                                                    <?= Html::img('@web/img/like.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                                </div>
                                                <div class="pi-txt">
                                                    чистота сделок
                                                </div>
                                                <div class="pi-message">
                                                    и максимальная скорость
                                                    платежей <br> гарантированы
                                                    администрацией портала
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="article">
                        <div class="col-md-8 col-md-offset-1 all-addresses-block">
                            <div class="offset-padding">
                            <div class="row article-item">
                                <div class="col-md-11">
                                    <div class="article-header">
                                        Что делать, если в юридическом адресе
                                        появилась ошибка?
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <img src="/img/reload.png" class="pull-right articles-reload" alt="">
                                </div>
                            </div>
                            <div class="row article-date article-item">
                                <div class="col-md-11">
                                    16/06/2012
                                </div>
                            </div>
                            <div class="row article-text article-item">
                                <div class="col-md-11">
                                При оформлении юридического адреса сотрудник налоговой инспекции может ошибиться, неправильно набрать текст адреса или личные данные заявителя. А получатель, в свою очередь, может пропустить ошибку при проверке. Даже одна опечатка может привести к нежелательным последствиям, поэтому мы рекомендуем Вам внимательно проверять всю документацию по несколько раз. Но если ошибка все же допущена – ее можно исправить.
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="article">
                        <div class="col-md-8 pull-right all-addresses-block">
                            <div class="offset-padding">
                            <div class="row article-item">
                                <div class="col-md-11">
                                    <div class="article-title uppercase">
                                        Какие типичные ошибки возможны в написании юридического адреса?
                                    </div>
                                </div>

                            </div>

                            <div class="row article-text article-item">
                                <div class="col-md-11">
                                    Ошибка может быть орфографической или пунктуационной, допущенной по вине представителя ООО или сотрудника налоговой службы, заполнявшего документы. В большинстве случаев ошибки представляют собой опечатки: перестановку букв или их пропуск, неверно написанные цифры.
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div class="article">
                        <div class="col-md-8 pull-right all-addresses-block">
                            <div class="offset-padding">
                            <div class="row article-item">
                                <div class="col-md-11">
                                    <div class="article-title uppercase">
                                        К каким последствиям может привести ошибка в юридическом адресе?
                                    </div>
                                </div>

                            </div>

                            <div class="row article-text article-item">
                                <div class="col-md-11">
                                    Если в официальных документах организации указано неверное написание улицы, фамилии учредителей и т.д., то возможны:
                                </div>
                            </div>
                            <div class="row article-text article-item">
                                <div class="col-md-11">
                                    Ошибка может быть орфографической или пунктуационной, допущенной по вине представителя ООО или сотрудника налоговой службы, заполнявшего документы. В большинстве случаев ошибки представляют собой опечатки: перестановку букв или их пропуск, неверно написанные цифры.
                                </div>
                            </div>
                            <div class="row article-text article-item">
                                <div class="col-md-11">
                                    <ul>
                                        <li><span>\</span>дальнейшие разночтения в договорах,</li>
                                        <li><span>\</span>проблемы при сверке данных со сведениями ЕГРЮЛ,</li>
                                        <li><span>\</span>сложности с открытием банковского счета,</li>
                                        <li><span>\</span>недопущения ООО к участию в различных тендерах,</li>
                                        <li><span>\</span>проблемы в разных ситуациях, где требуется предъявлять уставные документы</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row article-text article-item">
                                <div class="col-md-11">
                                    Недостоверные сведения в ЕГРЮЛ могут стать причиной для отказа со стороны налоговой инспекции при последующих изменениях. Также наличие недостоверных сведений в базе грозит административной ответственностью организации в соответствии со ст. 14.25 КоАП РФ.
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="article">
                        <div class="col-md-8 pull-right all-addresses-block">
                            <div class="offset-padding">
                                <div class="row article-item">
                                    <div class="col-md-11">
                                        <div class="article-title uppercase">
                                            Порядок действий при обнаружении ошибки в юридическом адресе
                                        </div>
                                    </div>

                                </div>

                                <div class="row article-text article-item">
                                    <div class="col-md-11 article-paragraph">
                                        Обнаружение ошибки в налоговой инспекции при оформлении ООО
                                    </div>
                                </div>
                                <div class="row article-text article-item">
                                    <div class="col-md-11">
                                        Если ошибка обнаружена сразу после получения документов, на нее нужно немедленно указать специалисту налоговой службы в том окне, где выдавались выписки. После чего инспектор заполнит специальную карточку замечаний, которая будет передана заявителю. В карточке будет указана дата, когда можно получить корректный вариант (обычно – через 4-6 рабочих дней).
                                    </div>
                                </div>
                                <div class="row article-text article-item">
                                    <div class="col-md-11 article-paragraph">
                                        Обнаружение ошибки через 5 дней или позже, с момента получения документов
                                    </div>
                                </div>
                                <div class="row article-text article-item">
                                    <div class="col-md-11">
                                        Если техническая ошибка в указании организационно-правовой формы в ЕГРЮЛ обнаружена спустя несколько дней или даже месяцев с момента получения документов в налоговой, то процесс ее исправления может затянуться на несколько месяцев и даже на год.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="article">
                        <div class="col-md-8 pull-right all-addresses-block">
                            <div class="offset-padding">
                            <div class="row article-item">
                                <div class="col-md-11">
                                    <div class="article-title uppercase">
                                        Порядок действий при обнаружении ошибки в юридическом адресе
                                    </div>
                                </div>

                            </div>

                            <div class="row article-text article-item">
                                <div class="col-md-11">
                                    Обнаружение ошибки в налоговой инспекции при оформлении ООО
                                </div>
                            </div>
                            <div class="row article-text article-item">
                                <div class="col-md-11">
                                    Если ошибка обнаружена сразу после получения документов, на нее нужно немедленно указать специалисту налоговой службы в том окне, где выдавались выписки. После чего инспектор заполнит специальную карточку замечаний, которая будет передана заявителю. В карточке будет указана дата, когда можно получить корректный вариант (обычно – через 4-6 рабочих дней).
                                </div>
                            </div>
                            <div class="row article-text article-item">
                                <div class="col-md-11">
                                    Обнаружение ошибки через 5 дней или позже, с момента получения документов
                                </div>
                            </div>
                            <div class="row article-text article-item">
                                <div class="col-md-11">
                                    Если техническая ошибка в указании организационно-правовой формы в ЕГРЮЛ обнаружена спустя несколько дней или даже месяцев с момента получения документов в налоговой, то процесс ее исправления может затянуться на несколько месяцев и даже на год.
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="article">
                        <div class="col-md-8 pull-right all-addresses-block">
                            <div class="offset-padding">
                            <div class="row article-item">
                                <div class="col-md-11">
                                    <div class="article-title uppercase">
                                        КАК ИСПРАВИТЬ ОШИБКУ?
                                    </div>
                                </div>
                            </div>
                            <div class="row article-text article-item">
                                <div class="col-md-11">
                                    Для исправления ошибки нужно обратиться в канцелярию регистрирующего органа с заявлением по форме р14001. В форме Р14001 заполняется Заявление о внесении изменений в сведения о юридическом лице, содержащиеся в ЕГРЮЛ (первый лист). В пункте 2 ставится значение 2 и указывается государственный регистрационный номер (ГРН) записи, где сделана ошибка. Правка будет осуществляться за счет организации-заявителя. Исправленные документы будут отправлены почтой на юридический адрес компании. В центральную инспекцию можно также обратиться в электронном виде, с просьбой об исправлении ошибки. Для этого надо заполнить форму на сайте налоговой инспекции. Если в местной налоговой инспекции отказались исправлять Вашу ошибку и признавать свою вину, Вы можете обратиться с жалобой в центральную инспекцию, оформив заявление в электронном виде.  Сайт Управления ФНС по г.Москве: <a href="http://www.r77.nalog.ru">www.r77.nalog.ru</a>
                                </div>
                            </div>
                            <div class="row article-text article-item">
                                <div class="col-md-11">
                                    Заметим, что отправка документов по электронной почте сильно затягивает сроки регистрации ООО. Налоговые инспекции рассматривают обращения, присланные в интерактивном режиме, достаточно долго, с приостановками в 30 дней и проверками. Если же компании надо начать работать уже сейчас – в ее интересах ускорить процесс посредством личного контакта.
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="article">
                        <div class="col-md-8 pull-right all-addresses-block">
                            <div class="offset-padding">
                            <div class="row article-item">
                                <div class="col-md-11">
                                    <div class="article-title">
                                        Заключение: как избежать ошибок при оформлении юр. адреса
                                    </div>
                                </div>
                            </div>
                            <div class="row article-text article-item">
                                <div class="col-md-11">
                                    Совет один: нужно очень тщательно, сверяя каждую букву, проверять названия, адреса, фамилии и другие данные – буквально каждое слово, прочитав внимательно документы не менее двух раз. Все документы из налоговой, как правило, получают курьеры, а не гендиректора – следовательно, это должны быть очень ответственные, грамотные люди. Во избежание упущения ошибки мы рекомендуем учредителю компании лично приехать за готовыми документами.
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>


                        <div class="col-md-8 soc-block">
                            <div class="offset-padding">
                                <div class="article">
                                <div class="row">
                                    <div class="share-soc">
                                        <div class="socs al">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </div>
                                        <div class="socs al">
                                            <i class="fa fa-twitter" aria-hidden="true"></i>
                                        </div>
                                        <div class="socs al">
                                            <i class="fa fa-vk" aria-hidden="true"></i>
                                        </div>
                                        <div class="socs al">
                                            <i class="fa fa-google-plus" aria-hidden="true"></i>
                                        </div>
                                        <div class="socs al">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                        <div class="col-md-8 soc-block">
                            <div class="offset-padding">
                            <div class="article">
                                <div class="row">
                                    <a href="">
                                        вернуться назад
                                    </a>
                                </div>
                            </div>
                            </div>
                        </div>


                        <div class="col-md-8 pull-right">
                            <div class="offset-padding">
                            <div class="article">
                            <div class="row">
                                <div class="row">
                                    <hr class="article-hr" />
                                </div>
                            </div>
                            </div>
                        </div>
                            <div class="row articles-widget">
                                <div class="offset-padding">
                                <div class="col-md-12 pull-right">
                                    <div class="row">
                                        <div class="">
                                        <?php echo ArticlesWidget::widget();?>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                    </div>
                </div>
        </div>
    </div>
</section>


