<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid"  id="restore-pwd" >
    <div class="container">
        <!--<div class="row restore-logo">
            <div class="col-md-3">
                <div class="header-padding-left">
                    <div class="logo">
                        vseadresa<span class="pro">.pro</span>
                    </div>
                    <div class="under-logo-text">
                        портал юридических адресов
                    </div>

                </div>
            </div>
            <div class="col-md-8">
                <img src="/img/close.png" class="pull-right close-popup" alt="">
            </div>
        </div>-->
        <div class="row restore-logo">
            <div class="col-md-3">
                <div class="header-padding-left">
                    <div class="login-title">
                        восстановление пароля:
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 login-tutorial">
                <div class="flex column">

                </div>

            </div>
            <div class="col-md-6">
                <div class="offset-padding">
                    <div class="field-row flex al">
                        <div class="form-label">
                            <label for="" class="flex restore-password-email">
                                <div class="label-content">e-mail</div>
                            </label>
                        </div>
                        <div class="form-input">
                            <input type="text" />
                        </div>
                    </div>

                    <div class="field-row flex al">
                        <a href="" class="login-button">
                            восстановить <br> пароль
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="field-row flex">
                    <div class="restore-information">
                        в базе есть <br> пользователь <br> с указанным <br> e-mail
                    </div>
                </div>
            </div>

        </div>-->
    </div>
</div>