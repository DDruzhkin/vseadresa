<?php
use app\components\ArticlesWidget;
use common\models\Metro;
use common\models\Okrug;
use common\models\Nalog;
use common\models\Option;
use yii\helpers\Url;
?>

<?php $this->params['breadcrumbs'] =
		[

			[
				'label' => 'цены',
			],
		
		        ];


		        use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$optionList = Option::find()->all();

function highlightSort($sortBy){
    if(isset($_GET['sort_by']) && $_GET['sort_by'] == $sortBy){
        echo '<img src="/img/strelka.png">';
    }else{
        echo '<img src="/img/white_triangle.png">';
    }
}



?>


<section class="addresses" id="prices">
	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="page-info">
					стоимость юридического адреса
					с предоставлением рабочего места
					на период проверок:
					</div>
				</div>
			</div>
            <div class="row">
                <div class="col-md-11 flex put-to-right  filter-callout  <?php if(isset($_GET["AddressSearch"])){
                    echo "active-filter";
                } ?>">
                    <i class="fa fa-filter"></i>
                </div>
            </div>
			<div class="row table-cnt">
                <div class="table-self">
				<table class="table price-table">
					<thead>
						<th></th> <!-- for new addresses -->
						<th><div class="th-wrapper">
                                <div>округ</div>
                                <div class="sort-button" data-sort="okrug_id">
                                    <?php highlightSort('okrug_id'); ?>
                                </div>
                            </div></th>
						<th>
                            <div class="th-wrapper">
                                <div>налог</div>
                                <div class="sort-button" data-sort="nalog_id">
                                    <?php highlightSort('nalog_id'); ?>
                                </div>
                            </div>
                        </th>
						<th class="pth">
								адрес
						</th>
						<th>
                            <div class="th-wrapper">
                                <div>метро</div>
                                <div class="sort-button" data-sort="metro_id">
                                    <?php highlightSort('metro_id'); ?>
                                </div>
                            </div>
                        </th>
						<th class="pth">опции</th>
						<th>индекс</th>
						<th>

                            <div class="th-wrapper">
                                <div>6 мес</div>
                                <div class="sort-button" data-sort="price6">
                                    <?php highlightSort('price6'); ?>
                                </div>
                            </div>
                        </th>
						<th>

                            <div class="th-wrapper">
                                <div>11 мес</div>
                                <div class="sort-button" data-sort="price11">
                                    <?php highlightSort('price11'); ?>
                                </div>
                            </div>
                        </th>
						<th></th> <!-- for orders -->
					</thead>
					<tbody>

                        <?php
                        foreach ($addresses as $address): ?>
                            <tr>
                                <td>
                                    <a class="new-label">
                                        new
                                    </a>
                                </td>
                                <td><?=@Okrug::findOne($address->okrug_id)->name;?></td>
                                <td><?=@Nalog::findOne($address->nalog_id)->number;?></td>
                                <td><?=$address->address;?></td>
                                <td><?=@Metro::findOne($address->metro_id)->name;?></td>
                                <td>
                                    <div class="icons-div flex al">
                                        <!--<img src="/img/anchors.png" class="address-icon" />
                                        <i class="fa fa-envelope address-icon suspended" aria-hidden="true"></i>
                                        <i class="fa fa-home address-icon" aria-hidden="true"></i>
                                        <i class="fa fa-credit-card-alt address-icon" aria-hidden="true"></i>
                                        <img src="/img/column.png" class="address-icon"/>
                                        <img src="/img/compass.png" class="address-icon" />-->
                                        <?=$address -> getOptions('<a title="%name%">%icon%</a>')?>
                                    </div>
                                </td>
                                <td><?=@$address->adr_index;?></td>
                                <td><?=$address->price6;?></td>
                                <td><?=$address->price11;?></td>
                                <td>
                                    <a href="" class="book-it">заказать</a>
                                </td>
                            </tr>
                        <?php endforeach;  ?>
                    </tbody>
                </table>
						<!--
						<table class="table price-table">
					<thead>
						<th></th>
                    <th><div class="th-wrapper">
                            <div>округ</div>
                            <div class="sort-button">
                                <img src="/img/strelka.png">
                            </div>
                        </div></th>
                    <th>офис</th>
                    <th class="pth">
                        <div class="th-wrapper">
                            <div>адрес</div>
                            <div class="sort-button">
                                <img src="/img/white_triangle.png">
                            </div>
                        </div>

                    </th>
                    <th>метро</th>
                    <th class="pth">опции</th>
                    <th>е/м аренда</th>
                    <th>цена 6 мес</th>
                    <th>цена 11 мес</th>
                    <th></th>
                    </thead>
                    <tbody>

						<tr>
							<td>
                                <a class="new-label">
                                    new
                                </a>
                            </td>
							<td>ЦАО</td>
							<td>2</td>
							<td>105066, ул. Спартаковская</td>
							<td>Спортивная</td>
							<td>
								<div class="icons-div flex al">

									<img src="/img/anchors.png" class="address-icon" />
									<i class="fa fa-envelope address-icon suspended" aria-hidden="true"></i>
									<i class="fa fa-home address-icon" aria-hidden="true"></i>
									<i class="fa fa-credit-card-alt address-icon" aria-hidden="true"></i>
									<img src="/img/column.png" class="address-icon"/>
									<img src="/img/compass.png" class="address-icon" />
								</div>
							</td>
							<td>4 000</td>
							<td>19 000</td>
							<td>25 000</td>
							<td>
								<a href="" class="book-it">заказать</a>
							</td>
						</tr>
                        <tr>
                            <td>

                            </td>
                            <td>ЦАО</td>
                            <td>2</td>
                            <td>105066, ул. Спартаковская</td>
                            <td>Спортивная</td>
                            <td>
                                <div class="icons-div flex al">
                                    <img src="/img/anchors.png" class="address-icon" />
                                    <i class="fa fa-envelope address-icon suspended" aria-hidden="true"></i>
                                    <i class="fa fa-home address-icon" aria-hidden="true"></i>
                                    <i class="fa fa-credit-card-alt address-icon" aria-hidden="true"></i>
                                    <img src="/img/column.png" class="address-icon"/>
                                    <img src="/img/compass.png" class="address-icon" />
                                </div>
                            </td>
                            <td>4 000</td>
                            <td>19 000</td>
                            <td>25 000</td>
                            <td>
                                <a href="" class="book-it">заказать</a>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td>ЦАО</td>
                            <td>2</td>
                            <td>105066, ул. Спартаковская</td>
                            <td>Спортивная</td>
                            <td>
                                <div class="icons-div flex al">
                                    <img src="/img/anchors.png" class="address-icon" />
                                    <i class="fa fa-envelope address-icon suspended" aria-hidden="true"></i>
                                    <i class="fa fa-home address-icon" aria-hidden="true"></i>
                                    <i class="fa fa-credit-card-alt address-icon" aria-hidden="true"></i>
                                    <img src="/img/column.png" class="address-icon"/>
                                    <img src="/img/compass.png" class="address-icon" />
                                </div>
                            </td>
                            <td>4 000</td>
                            <td>19 000</td>
                            <td>25 000</td>
                            <td>
                                <a href="" class="book-it">заказать</a>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td>ЦАО</td>
                            <td>2</td>
                            <td>105066, ул. Спартаковская</td>
                            <td>Спортивная</td>
                            <td>
                                <div class="icons-div flex al">
                                    <img src="/img/anchors.png" class="address-icon" />
                                    <i class="fa fa-envelope address-icon suspended" aria-hidden="true"></i>
                                    <i class="fa fa-home address-icon" aria-hidden="true"></i>
                                    <i class="fa fa-credit-card-alt address-icon" aria-hidden="true"></i>
                                    <img src="/img/column.png" class="address-icon"/>
                                    <img src="/img/compass.png" class="address-icon" />
                                </div>
                            </td>
                            <td>4 000</td>
                            <td>19 000</td>
                            <td>25 000</td>
                            <td>
                                <a href="" class="book-it">заказать</a>
                            </td>
                        </tr>
					</tbody>
				</table>-->
                <div class="hr">
                    <hr>
                </div>
                </div>
			</div>
            <div class="row">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pag">
                            <hr class="pagination-line">

                            <div class="change-page" data-page="1">
                                <div class="pagination-arrow pagination-arrow-left">
                                    <img src="/img/triangle_left.png" alt="">                <img src="/img/triangle_left.png" alt="">            </div>
                            </div>

                            <div class="change-page" data-page="2">
                                <div class="pagination-arrow pagination-arrow-left pagination-space-left">
                                    <img src="/img/triangle_left.png" alt="">            </div>
                            </div>

                            <div class="pager-numbers">
                                <div class="pag-number change-page" data-page="1">
                                    <span>1</span>
                                </div>
                                <div class="pag-number change-page" data-page="2">
                                    <span>2</span>
                                </div>
                                <div class="pag-page">
                                    <span class="active-page">3</span>
                                </div>
                                <div class="pag-number change-page" data-page="4">
                                    <span>4</span>
                                </div>
                                <div class="pag-number change-page" data-page="5">
                                    <span>5</span>
                                </div>
                            </div>

                            <div class="change-page" data-page="4">
                                <div class="pagination-arrow pagination-arrow-right">
                                    <img src="/img/triangle_right.png" alt="">            </div>
                            </div>

                            <div class="change-page" data-page="10">
                                <div class="pagination-arrow pagination-arrow-right">
                                    <img src="/img/triangle_right.png" alt="">                <img src="/img/triangle_right.png" alt="">            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>

        <div class="container-fluid bottom-text">
            <div class="container narrow">
                <?php // echo ArticlesWidget::widget();?>
                <div class="row">
                    <div class="col-md-3">
                        <span class="bottom-text-block">Аренда офиса в Москве</span> — не проблема с актуальными предложениями на портале office.ru. Ежедневно не менее 14000 актуальных вариантов по аренде офисов по более чем 9000 адресам Москвы. Все предложения напрямую от собственников. Все предложения без комиссии. Важную роль играет район аренды, ведь это не только финансовая операция, но и работа над имиджем компании. Здесь месторасположение офиса важно потому, что конкуренция в большом городе огромна и клиент зачастую отдаст предпочтение тем, кого просто и быстро отыскать в офисе.

                    </div>
                    <div class="col-md-3">
                        <span class="bottom-text-block">Офисы бывают двух типов: </span> в специализированных бизнес-центрах и на первых этажах жилых домов, вторых этажах магазинов и в прочих, зданиях. Но данная услуга представляет собой не только поиск помещения. Это процесс состоящий из нескольких этапов: запрос от арендатора, составления заявки, вдумчивый поиск по нашей специализированной базе данных, низкую комиссию, офис для Вас и тщательное оформление договора аренды офиса или нежилого помещения по всем юридическим и правовым нормам.

                    </div>
                    <div class="col-md-3">

                        Кроме того, <span class="bottom-text-block">обратившись в специализированную фирму,</span> предоставляющую услугу «аренда офисов в Москве», заказчик уже работает с одним человеком, которому объясняет всё, что ему требуется, а не решает одновременно множество дел – аренда офиса, подготовка к переезду, свои текущие дела и т.п. Агент по аренде офиса самостоятельно объезжает объекты и представляет на суд заказчику самые подходящие варианты. Цена за подобную услугу, помимо адреса и наличие мест для парковки.

                    </div>
                    <div class="col-md-3">

                        <span class="bottom-text-block">Цена за подобную услугу,</span> помимо адреса и наличие мест для парковки, формируется из таких факторов как удаление от метро/остановок общественного транспорта, площади помещения, наличия ремонта (в ряде случаев возможно снять офисное помещение без внутренней отделки или в разной степени её готовности), а также предполагаемого срока найма. Неспециалисту непросто всё это учесть и тогда поиск подходящего помещения может превратиться в затяжной кошмар для любой организации.

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<div class="filter-pp">
    <div class="filter-pp-area">
        <div class="cabinet-form">
            <div class="close-filter-button">
                <img src="/img/close-menu.png" alt="">
            </div>
            <div class="price-filter-row">
                <?php $form = ActiveForm::begin(['action' =>['site/prices'], 'method' => 'get']); ?>
                <div class="col-md-12 col-xs-12 flex al between">
                    <div class="price-filter-block filter-bar col-md-12 flex al">
                        <div class="price-filter-word flex al">
                            фильтр
                        </div>
                    </div>

                    <div class="select-bar flex al">

                        <?php $districts_array = $districts::find()->all(); ?>
                        <?php $items = ArrayHelper::map($districts_array,'id','abr');
                        // echo "<pre>", print_r($items);
                        $params = [
                            'prompt' => 'по округу',
                            'class' => 'metro',
                            'label' => '',
                            'options'=>[@(int)$_GET['AddressSearch']['okrug_id']=>['Selected'=>true]]
                        ];
                        echo $form->field($searchModel, 'okrug_id')->label(false)->dropDownList($items,$params);
                        ?>


                    </div>

                    <div class="select-bar flex al">

                        <?php $taxes_array = $taxes::find()->all(); ?>
                        <?php $items = ArrayHelper::map($taxes_array,'id','number');
                        // echo "<pre>", print_r($items);
                        $params = [
                            'prompt' => 'по налоговой',
                            'class' => 'metro',
                            'label' => '',
                            'options'=>[@(int)$_GET['AddressSearch']['nalog_id']=>['Selected'=>true]]
                        ];
                        echo $form->field($searchModel, 'nalog_id')->label(false)->dropDownList($items,$params);
                        ?>


                    </div>

                    <div class="select-bar flex al">

                        <?php $metro_array = $metro::find()->all(); ?>
                        <?php $items = ArrayHelper::map($metro_array,'id','name');
                        // echo "<pre>", print_r($items);
                        $params = [
                            'prompt' => 'станция метро',
                            'class' => 'metro',
                            'label' => '',
                            'options'=>[@(int)$_GET['AddressSearch']['metro_id']=>['Selected'=>true]]
                        ];
                        echo $form->field($searchModel, 'metro_id')->label(false)->dropDownList($items,$params);
                        ?>


                    </div>
                </div>
                <div class="col-md-12" id="submit-count-search-form">
                    <div class="publish-address filter-apply-block">

                        <div class="clear-filter">
                            <a href="<?=Url::to("/index.php/site/orders"); ?>" class="public-address-button">убрать фильтр</a>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end();?>

            </div>

        </div>
    </div>
</div>