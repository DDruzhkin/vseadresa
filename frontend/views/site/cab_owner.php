<?php
use app\components\Cab_ownerWidget;
?>
<?php $this->params['breadcrumbs'] =
    [


        [
            'label' => 'Company',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => 'профиль',
        ],

    ];


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<section class="cabinet-section">
    <div class="container-fluid cloud-bg no-padding-top-bg">

        <div class="container">
            <div class="row">
                <div class="col-md-3 add-search-block">

                    <?php echo Cab_ownerWidget::widget(); ?>

                </div>
                <div class="col-md-8 col-md-offset-1 all-addresses-block">
                    <div class="offset-padding">
                    <div class="cab-owner-cnt">
                        <div class="cabinet-form">
                            <div class="flex al forms-head">
                                <h2>Профиль</h2>

                                <div class="reg-tutorial cab-text-info">
                                    <div class="flex">
                                        <sup>*</sup>
                                        <div>помечены поля, <br>
                                            обязательные для заполнения</div>
                                    </div>

                                </div>
                            </div>
                            <form action="">
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">фамилия, имя</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">email</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                    <div class="field-subrow">
                                        является уникальным
                                        идентификатором пользователя
                                        в системе и используется
                                        для авторизации
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">телефон</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">правовой статус</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">название организации</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <div class="cant-modify">Company</div>

                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">название ИП</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <a href="" class="reg-button">
                                        изменить данные
                                    </a>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">старый пароль</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">новый пароль</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">новый пароль повторно</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <a href="" class="reg-button">
                                        изменить данные
                                    </a>
                                </div>
                                <div class="labels cabinet-label">
                                    адрес доставки документов
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">индекс</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">область / край</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">район</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">населенный пункт</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">адрес</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">примечания</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <textarea name="" id="" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <a href="" class="reg-button">
                                        изменить данные
                                    </a>
                                </div>


                                <!--passport-->



                                <div class="labels cabinet-label">
                                    паспортные данные
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">фамилия</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">имя</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">район</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">отчество</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">серия / номер паспорта</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">дата выдачи паспорта</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">орган выдавший паспорт</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">адрес регистрации</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">

                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <div class="cant-modify attention">
                                        изменение данных для договора потребует заключение дополнительного соглашения об изменении реквизитов договора в течении одной недели, в случае, если договор не будет перезаключен, будут восстановлены данные текущего договора. Вы уверены что хотите изменить данные договора?
                                        </div>
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <a href="" class="reg-button">
                                        изменить данные
                                    </a>
                                </div>


                                <!-- Rekvizit -->

                                <div class="labels cabinet-label">
                                    Реквизиты юридического лица для договора
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">название организации</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">название ИП</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">название ИНН</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">название КПП</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">название ОГРН</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">юридический адрес</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">

                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">

                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <div class="cant-modify attention">
                                            изменение данных для договора потребует заключение дополнительного соглашения об изменении реквизитов договора в течении одной недели, в случае, если договор не будет перезаключен, будут восстановлены данные текущего договора. Вы уверены что хотите изменить данные договора?
                                        </div>
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <a href="" class="reg-button">
                                        изменить данные
                                    </a>
                                </div>


                                <!-- Реквизиты банковского счета -->


                                <div class="labels cabinet-label">
                                    Реквизиты банковского счета
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">БИК</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">номер счета</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">владелец счета</div>
                                            <sup>*</sup>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">

                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <div class="cant-modify attention">
                                            изменение данных для договора потребует заключение дополнительного соглашения об изменении реквизитов договора в течении одной недели, в случае, если договор не будет перезаключен, будут восстановлены данные текущего договора. Вы уверены что хотите изменить данные договора?
                                        </div>
                                    </div>
                                </div>

                                <div class="field-row flex al">
                                    <a href="" class="reg-button">
                                        изменить данные
                                    </a>
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="row labels cabinet-label">
                        <div class="col-md-11">
                            договора
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 table-cnt">
                            <div class="table-self">
                            <table class="table price-table counts-table company-table">
                                <thead>
                                <th>No</th>
                                <th>дата</th>
                                <th>название</th>
                                <th>статус</th>
                                <th>скачать</th>

                                </thead>
                                <tbody>
                                <tr>

                                    <td>253464</td>
                                    <td>15.10.2016</td>
                                    <td>выполнен</td>
                                    <td>заказчик</td>
                                    <td><a href="" class="book-it">pdf</a></td>
                                </tr>
                                <tr>

                                    <td>ЦАО</td>
                                    <td>2</td>
                                    <td>счет</td>
                                    <td>25000</td>
                                    <td><a href="" class="book-it">pdf</a></td>
                                </tr>
                                <tr>

                                    <td>ЦАО</td>
                                    <td>2</td>
                                    <td>счет</td>
                                    <td>25000</td>
                                    <td><a href="" class="book-it">pdf</a></td>
                                </tr>
                                <tr>

                                    <td>ЦАО</td>
                                    <td>2</td>
                                    <td>счет</td>
                                    <td>25000</td>
                                    <td><a href="" class="book-it">pdf</a></td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="hr">
                                <hr>
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
