<?php
use app\components\CabWidget;
?>
<?php $this->params['breadcrumbs'] =
    [


        [
            'label' => 'Username',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => 'профиль',
        ],

    ];


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<section class="cabinet-section">
    <div class="container-fluid cloud-bg no-padding-top-bg">

        <div class="container">
            <div class="row">
                <div class="col-md-2 col-lg-3 add-search-block">

                    <?php echo CabWidget::widget(); ?>

                </div>
                <div class="col-md-8 col-md-offset-1 all-addresses-block orders-search-form">
                    <div class="">
                        <div class="offset-padding">

                                <h2>Заказ № 125 203</h2>
                                <div class="get-order-address">
                                    Большой Тишинский пер.
                                </div>
                                <div class="labels">
                                    выбранный период
                                </div>
                                <div class="order-value">
                                    <span>15/10/2017</span> - <span>14/10/2018</span>
                                </div>
                                <div class="labels">
                                    стоимость аренды адреса
                                </div>
                                <div class="order-value">
                                    <span>25 000</span> <i class="fa fa-rub" aria-hidden="true"></i>/месяц
                                </div>
                                <div class="labels">
                                    текущий статус заказа
                                </div>
                                <div class="order-value">
                                    <span>черновик</span>
                                </div>
                                <div class="labels">
                                    дата и время создания
                                </div>
                                <div class="order-value">
                                    <span>01/10/2017 10:59</span>
                                </div>
                                <div class="labels">
                                    дата последнего изменения
                                </div>
                                <div class="order-value">
                                    <span>01/10/2017 10:59</span>
                                </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-11 table-cnt">
                                <div class="table-self">
                                    <table class="table price-table counts-table">
                                        <thead>
                                        <th>Код транзакции</th>
                                        <th>дата и время транзакции</th>
                                        <th>новый статус</th>
                                        <th>кто осуществил</th>

                                        </thead>
                                        <tbody>
                                        <tr>

                                            <td>253464</td>
                                            <td>15.10.2016</td>
                                            <td>выполнен</td>
                                            <td>заказчик</td>

                                        </tr>
                                        <tr>

                                            <td>ЦАО</td>
                                            <td>2</td>
                                            <td>счет</td>
                                            <td>25000</td>

                                        </tr>
                                        <tr>

                                            <td>ЦАО</td>
                                            <td>2</td>
                                            <td>счет</td>
                                            <td>25000</td>

                                        </tr>
                                        <tr>

                                            <td>ЦАО</td>
                                            <td>2</td>
                                            <td>счет</td>
                                            <td>25000</td>

                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="hr">
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="pag">
                                    <a><div class="pagination-arrow pagination-arrow-left">
                                            <img src="/img/triangle_left.png">
                                        </div></a>
                                    <div class="pag-stripe"></div>
                                    <div class="pag-page">
                                        <span class="active-page">1</span><span class="of">&nbsp;/ 15</span>
                                    </div>
                                    <div class="pag-stripe"></div>
                                    <a><div class="pagination-arrow pagination-arrow-right">
                                            <img src="/img/triangle_right.png">
                                        </div></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>
