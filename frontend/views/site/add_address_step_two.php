<?php
use app\components\CabWidget;
use yii\helpers\Url;
?>
<?php $this->params['breadcrumbs'] =
    [


        [
            'label' => 'Username',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => 'профиль',
        ],

    ];


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<section class="cabinet-section">
    <div class="container-fluid cloud-bg no-padding-top-bg">

        <div class="container">
            <div class="row">
                <div class="col-md-3 add-search-block">

                    <?php echo CabWidget::widget(); ?>

                </div>
                <div class="col-md-8 col-md-offset-1 all-addresses-block address-add-page">
                    <div class="col-md-10">
                        <div class="address-add-page-title">
                            <span class="address-add-step-color">2</span> заказ визита фотографа
                        </div>
                        <div class="add-address-form-description">
                            укажите желаемую дату и время
                            визита фотографа по этому адресу.
                        </div>
                        <div class="cabinet-form">
                            <form action="">
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">дата</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input  class="date" placeholder=".. / .. / ...." type="text">
                                    </div>
                                </div>
                                <div class="field-row flex al">
                                    <div class="form-label">
                                        <label for="" class="flex">
                                            <div class="label-content">время</div>
                                        </label>
                                    </div>
                                    <div class="form-input">
                                        <input  class="date" placeholder=".. / .. / .." type="text">
                                    </div>
                                </div>

                                <div class="field-row flex al add-address-submit">
                                    <div class="field-row more-button flex al back-add-address">
                                        <a href="<?=Url::to("/index.php/site/add_address");?>" class="reg-button">
                                            вернуться
                                        </a>
                                    </div>
                                    <div class="field-row more-button flex al more-add-address">
                                        <a href="<?=Url::to("/index.php/site/add_addressstep3");?>" class="reg-button">
                                            далее
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>
