<?php
use app\components\CabWidget;
?>
<?php $this->params['breadcrumbs'] =
    [


        [
            'label' => 'Username',
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => 'профиль',
        ],

    ];


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>
<section class="cabinet-section">
    <div class="container-fluid cloud-bg cloud-bg no-padding-top-bg">

        <div class="container">
            <div class="row">
                <div class="col-md-3 add-search-block">

                    <?php echo CabWidget::widget(); ?>

                </div>
                <div class="col-md-8 col-md-offset-1 all-addresses-block orders-search-form">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-11 flex">
                                <h2>Адрес доставки документов</h2>
                                <div class="flex put-to-right  filter-callout  <?php if(isset($_GET["AddressSearch"])){
                                    echo "active-filter";
                                } ?>">
                                    <i class="fa fa-filter"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-11 table-cnt">
                                <div class="table-self">
                                    <table class="table price-table counts-table">
                                        <thead>
                                        <th> код</th>
                                        <th>название</th>
                                        <th>статус</th>
                                        <th>операции</th>
                                        <th>заказы</th>
                                        <th>расчеты</th> <!-- for orders -->
                                        </thead>
                                        <tbody>
                                        <tr>

                                            <td>253464</td>
                                            <td>15.10.2016</td>
                                            <td>счет</td>
                                            <td>25000</td>
                                            <td>

                                            </td>

                                            <td>

                                            </td>
                                        </tr>
                                        <tr>

                                            <td>ЦАО</td>
                                            <td>2</td>
                                            <td>счет</td>
                                            <td>25000</td>
                                            <td>

                                            </td>

                                            <td>

                                            </td>
                                        </tr>
                                        <tr>

                                            <td>ЦАО</td>
                                            <td>2</td>
                                            <td>счет</td>
                                            <td>25000</td>
                                            <td>

                                            </td>

                                            <td>

                                            </td>
                                        </tr>
                                        <tr>

                                            <td>ЦАО</td>
                                            <td>2</td>
                                            <td>счет</td>
                                            <td>25000</td>
                                            <td>

                                            </td>

                                            <td>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="hr">
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="pag">
                                    <hr class="pagination-line">

                                    <div class="change-page" data-page="1">
                                        <div class="pagination-arrow pagination-arrow-left">
                                            <img src="/img/triangle_left.png" alt="">                <img src="/img/triangle_left.png" alt="">            </div>
                                    </div>

                                    <div class="change-page" data-page="2">
                                        <div class="pagination-arrow pagination-arrow-left pagination-space-left">
                                            <img src="/img/triangle_left.png" alt="">            </div>
                                    </div>

                                    <div class="pager-numbers">
                                        <div class="pag-number change-page" data-page="1">
                                            <span>1</span>
                                        </div>
                                        <div class="pag-number change-page" data-page="2">
                                            <span>2</span>
                                        </div>
                                        <div class="pag-page">
                                            <span class="active-page">3</span>
                                        </div>
                                        <div class="pag-number change-page" data-page="4">
                                            <span>4</span>
                                        </div>
                                        <div class="pag-number change-page" data-page="5">
                                            <span>5</span>
                                        </div>
                                    </div>

                                    <div class="change-page" data-page="4">
                                        <div class="pagination-arrow pagination-arrow-right">
                                            <img src="/img/triangle_right.png" alt="">            </div>
                                    </div>

                                    <div class="change-page" data-page="10">
                                        <div class="pagination-arrow pagination-arrow-right">
                                            <img src="/img/triangle_right.png" alt="">                <img src="/img/triangle_right.png" alt="">            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>

<div class="filter-pp">
    <div class="filter-pp-area">

<div class="cabinet-form">
    <div class="close-filter-button">
        <img src="/img/close-menu.png" alt="">
    </div>
    <form class="flex" action="">
        <div class="">

            <div class="order-flex column">
                <div class="order-flex-left">
                    <div class="find-counts-cnt">
                        найти адрес по
                    </div>
                </div>
                <div class="order-flex-right">
                    <div class="">
                        <div class="field-row flex al">

                            <div class="form-input">
                                <input   type="text" placeholder="Улице">
                            </div>
                        </div>
                        <div class="field-row al">

                            <div class="form-input ">
                                <select name="" id="">
                                    <option value="">номеру налоговой</option>
                                    <option value="">точно</option>
                                    <option value="">точно</option>
                                </select>
                            </div>
                        </div>
                        <div class="field-row  al">

                            <div class="form-input ">
                                <select name="" id="">
                                    <option value="">округу</option>
                                    <option value="">точно</option>
                                    <option value="">точно</option>
                                </select>
                            </div>
                        </div>
                        <div class="field-row al">

                            <div class="form-input ">
                                <select name="" id="">
                                    <option value="">станции метро</option>
                                    <option value="">точно</option>
                                    <option value="">точно</option>
                                </select>
                            </div>
                        </div>
                        <div class="field-row al">

                            <div class="form-input ">
                                <select name="" id="">
                                    <option value="">статусу адреса</option>
                                    <option value="">точно</option>
                                    <option value="">точно</option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="delivery-form-submit">
                        <div class="publish-address">
                            <div class="public-address">
                                <a href="">
                                    <div class="public-address-button">
                                        искать
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="publish-address">
                            <div class="public-address">
                                <a href="">
                                    <div class="public-address-button">
                                        добавить адрес
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="publish-address filter-apply-block">

                            <div class="clear-filter">
                                <a href="<?=Url::to("/index.php/site/orders"); ?>" class="public-address-button">убрать фильтр</a>
                            </div>
                        </div>
                    </div>


                </div>
            </div>


        </div>

    </form>
</div>
</div>
</div>