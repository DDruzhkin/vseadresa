<div class="contacts">
    <div class="contact-block">
        <span class="contact-name">
            Телефон:
        </span>
        <span class="contact-value">+7 (499) 713-0194</span>
    </div>
    <div class="contact-block">
        <span class="contact-name">
            Email:
        </span>
        <span class="contact-value">info@vseadresa.ru</span>
    </div>
    <div class="contact-block">
        <span class="contact-name">
            ИП:
        </span>
        <span class="contact-value">
            Ивашкова Елена Владимировна
        </span>
    </div>
    <div class="contact-block">
        <span class="contact-name">
            Адрес:
        </span>
        <span class="contact-value">
           129594, г. Москва, ул. Марьиной Рощи 4-ая, д. 17, кв. 132
        </span>
    </div>
</div>