<?php
use backend\models\Order;
use backend\models\Address;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;




//echo "<pre>", print_r(Yii::$app->user->identity->id); die();
//echo "<pre>", print_r($orders); die();
$orders = Order::find()->where(["customer_id"=>Yii::$app->user->identity->id]);


$addresses = [];
foreach ($orders->all() as $order){
    $addresses[] = $order->address;
    //echo "<pre>", print_r($order->attributes);
}
//die();
$controls = [

    'address' => ['type'=>Form::INPUT_DROPDOWN_LIST, "items"=> ArrayHelper::map($addresses, 'id', 'address'), 'label'=>Address::getMeta('address', 'label'), 'hint'=>'', 'options'=>[]],
];




$form = ActiveForm::begin(['id' => 'form-order']);
$data =
        [
            [
                'attributes' => [
                    'address' => $controls['address'],
                ],
            ],




        ];
$formdata = $data;

$formdata[] =

    [
        'attributes' => [
            'actions'=>[
                'type'=>Form::INPUT_RAW,
                'value'=>  '
						<div class="clear"></div>
						<div class="row">
						<div class="col-md-12">
                            <div class="field-row flex al radio">
                                        '.

                                    (
                                    Html::submitButton('далее', ['class' => 'order-button', 'name' => 'order-button', 'onClick' => '$("#form-order").attr("action", "");'])
                                    ).'                       
                            </div></div></div>
				',
            ],
        ],
    ];

//var_dump($model); die();
$o = new Order();
echo FormGrid::widget([
    'model' => $o,
    'form' => $form,
    'rows' => $data
]);



    ActiveForm::end();
?>

