<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveField;
use common\models\User;
?>

<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'enableAjaxValidation' => true,
    'enableClientValidation'=>true

]); ?>

    <div class="container-fluid">
        <div class="container">

            <div class="col-md-12">

                <div class="labels cabinet-label">
                    адрес доставки документов
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">индекс</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <input type="text">
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">область / край</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <input type="text">
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">район</div>
                        </label>
                    </div>
                    <div class="form-input">
                        <input type="text">
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">населенный пункт</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <input type="text">
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">адрес</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <input type="text">
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">примечания</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <textarea name="" id="" cols="30" rows="10"></textarea>
                    </div>
                </div>



                <div class="labels cabinet-label">
                    паспортные данные
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">фамилия</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $form->field($person, 'lname')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">имя</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $form->field($person, 'fname')->label(false)->textInput() ?>
                    </div>
                </div>

                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">отчество</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $form->field($person, 'mname')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">серия / номер паспорта</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $form->field($person, 'pasport_number')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">дата выдачи паспорта</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $form->field($person, 'pasport_date')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">орган выдавший паспорт</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $form->field($person, 'pasport_organ')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">адрес регистрации</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $form->field($person, 'address')->label(false)->textInput() ?>
                    </div>
                </div>




                <div class="labels cabinet-label">
                    Реквизиты банковского счета
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">БИК</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $form->field($person, 'bank_bik')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">номер счета</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $form->field($person, 'account')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">
                            <div class="label-content">владелец счета</div>
                            <sup>*</sup>
                        </label>
                    </div>
                    <div class="form-input">
                        <?= $form->field($person, 'account_owner')->label(false)->textInput() ?>
                    </div>
                </div>
                <div class="field-row flex al">
                    <div class="form-label">
                        <label for="" class="flex">

                        </label>
                    </div>
                    <div class="form-input">
                        <div class="cant-modify attention">
                            изменение данных для договора потребует заключение дополнительного соглашения об изменении реквизитов договора в течении одной недели, в случае, если договор не будет перезаключен, будут восстановлены данные текущего договора. Вы уверены что хотите изменить данные договора?
                        </div>
                    </div>
                </div>




                <div class="field-row flex al">
                    <!--<a href="" class="reg-button">
                        изменить данные
                    </a>-->
                    <?= Html::submitButton('Завершить регистрацию', ['class' => 'reg-button', 'name' => 'signup-button']) ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>