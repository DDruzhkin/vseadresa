<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'fullname') ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'role') ?>

    <?= $form->field($model, 'status_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'registration_ip') ?>

    <?php // echo $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'password_hash') ?>

    <?php // echo $form->field($model, 'password_reset_token') ?>

    <?php // echo $form->field($model, 'auth_key') ?>

    <?php // echo $form->field($model, 'psw_recovery_code') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'role_admin') ?>

    <?php // echo $form->field($model, 'notifications') ?>

    <?php // echo $form->field($model, 'form_mode') ?>

    <?php // echo $form->field($model, 'date_visit') ?>

    <?php // echo $form->field($model, 'used') ?>

    <?php // echo $form->field($model, 'new_id') ?>

    <?php // echo $form->field($model, 'auth') ?>

    <?php // echo $form->field($model, 'psw_recovery_date') ?>

    <?php // echo $form->field($model, '_status_') ?>

    <?php // echo $form->field($model, 'notification_email') ?>

    <?php // echo $form->field($model, 'notification_enabled') ?>

    <?php // echo $form->field($model, 'default_profile_id') ?>

    <?php // echo $form->field($model, 'options') ?>

    <?php // echo $form->field($model, 'offerta_confirm') ?>

    <?php // echo $form->field($model, 'info') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
