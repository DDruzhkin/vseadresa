<style>

    /* Реквизиты */
    /*div.card {
        color: #333;
        width: 70%;
        float: left;
        padding: 10px 2%;
        margin: 10px 2%;
        border: 1px solid #777;
        border-radius: 5px;
        background: #FFF;
    }

    div.card div.header {
            font-size: 14pt;
            margin-bottom: 10px;
            background: #EEE;
            padding: 5px 15px;
    }

    div.card div.header span.caption{
        width: 100%;
        font-weight: bold;
        color: #333;
        padding: 0 5px;
        font-size: 11pt;
    }

    div.card div.value {
        margin: 0;
        font-size: 12pt;
        width: 100%;
        text-align: center;
        padding: 0 5px;

    }

    div.card div.undervalue {
        margin: 0;
        width: 100%;
        text-align: center;
        color: #777;
        font-size: 8pt;
        width: 100%;
        margin: 0;
        font-weight: normal;
        padding: 0 5px 15px 5px;
        border-top: 1px dashed #AAA;
    }*/
    /* /Реквизиты */

</style>
<?php
if (YII_DEBUG) $startTime = microtime(true);
use common\models\User;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$controls = [

    'fullname' => ['type' => Form::INPUT_TEXT, 'label' => User::getMeta('fullname', 'label'), 'hint' => '' . User::getMeta('fullname', 'comment'), 'options' => ['required' => 'true',]],
    'username' => ['type' => Form::INPUT_TEXT, 'label' => User::getMeta('username', 'label'), 'hint' => '' . User::getMeta('username', 'comment'), 'options' => ['readonly' => 'true', 'clientOptions' => ['readonly' => true]]],
    'role_as_array' => ['type' => Form::INPUT_CHECKBOX_LIST, 'label' => User::getMeta('role', 'label'), 'items' => User::getList('role'), 'hint' => '' . User::getMeta('role', 'comment'), 'options' => ['multiple' => true]],
    'status_id' => ['type' => Form::INPUT_DROPDOWN_LIST, 'label' => User::getMeta('status_id', 'label'), 'hint' => '' . User::getMeta('status_id', 'comment'), 'items' => $model->getStatusList()],
    'created_at_view' => ['type' => Form::INPUT_TEXT, 'label' => User::getMeta('created_at_view', 'label'), 'hint' => '' . User::getMeta('created_at_view', 'comment'), 'options' => ['readonly' => 'true',]],
    //'password' => ['type'=>Form::INPUT_TEXT, 'label'=>User::getMeta('password', 'label'), 'hint'=>''.User::getMeta('password', 'comment'), 'options'=>[]],
    //'email' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => User::getMeta('email', 'label'), 'hint' => '' . User::getMeta('email', 'comment'), 'options' => ['clientOptions' => ['readonly' => true, 'alias' => 'email'], ]],
	'email' => ['type' => Form::INPUT_TEXT, 'label' => User::getMeta('email', 'label'), 'hint' => '' . User::getMeta('email', 'comment'), 'options' => ['readonly' => 'true', 'clientOptions' => ['readonly' => true]]],
    'phone' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => User::getMeta('phone', 'label'), 'hint' => '' . User::getMeta('phone', 'comment'), 'options' => ['mask' => User::getMaskByAlias("phone"),]],
    'notification_email' => ['type' => Form::INPUT_TEXT, 'label' => User::getMeta('notification_email', 'label'), 'hint' => '' . User::getMeta('notification_email', 'comment'), 'options' => []],
    'notification_enabled' => ['type' => Form::INPUT_DROPDOWN_LIST, 'label' => User::getMeta('notification_enabled', 'label'), 'items' => User::getList('notification_enabled'), 'hint' => '' . User::getMeta('notification_enabled', 'comment'), 'options' => []],
    'offerta_confirm' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\checkbox\CheckboxX', 'label' => User::getMeta('offerta_confirm', 'label'), 'hint' => '' . User::getMeta('offerta_confirm', 'comment'), 'options' => []],
    'info' => ['type' => Form::INPUT_TEXTAREA, 'label' => User::getMeta('info', 'label'), 'hint' => '' . User::getMeta('info', 'comment'), 'options' => []],
    'actions' => ['type' => Form::INPUT_RAW, 'value' => '<div class="change-data">' . Html::submitButton('Изменить данные', ['class' => 'reg-button change-data']) . '</div>'],
    'passwordOld' => ['type' => Form::INPUT_TEXT, 'label' => 'Старый пароль', 'hint' => 'Введите пароль, который Вы использовали для входа', 'options' => ['type' => 'password']],
    'passwordNew' => ['type' => Form::INPUT_TEXT, 'label' => 'Новый пароль', 'hint' => 'Введите новый пароль, который Вы хотите установить', 'options' => ['type' => 'password']],
    'passwordNewAgain' => ['type' => Form::INPUT_TEXT, 'label' => 'Новый пароль повторно', 'hint' => 'Введите новый пароль повторно', 'options' => ['type' => 'password']],
];


$layoutBase =
    [
        [
            'attributes' => [
                'fullname' => $controls['fullname'],
            ],
        ], [
        'attributes' => [
            'email' => $controls['email'],
        ]
    ], [
        'attributes' => [
            'phone' => $controls['phone'],
        ],
    ], [
        'attributes' => [
            'actions' => $controls['actions'],
        ],
    ]
    ];


$layoutPsw =
    [
        [
            'attributes' => [
                'oldPassword' => $controls['passwordOld'],
            ],
        ], [
        'attributes' => [
            'newPassword' => $controls['passwordNew'],
        ]
    ], [
        'attributes' => [
            'retypePassword' => $controls['passwordNewAgain'],
        ],
    ], [
        'attributes' => [
            'actions' => $controls['actions'],
        ],
    ]
    ];


/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $passwordModel frontend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">


    <h2>Основные данные</h2>
    <?php
    $form = ActiveForm::begin();
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'rows' => $layoutBase
    ]);
    ActiveForm::end();
    ?>
    <h2 id="requisites">Реквизиты</h2>
    <div class="rekvizit-container">

        <?php

        echo NavX::widget([
            'options' => ['class' => 'dd-button'],
            'items' => [
                [
                    'label' => 'Добавить реквизиты',
                    'items' => [
                        ['label' => 'Физического лица', 'url' => Url::to(['/r/create/rperson'])],
                        ['label' => 'Индивидуального предпринимателя', 'url' => Url::to(['/r/create/rip'])],
                        ['label' => 'Юридического лица', 'url' => Url::to(['/r/create/rcompany'])],
                    ],
                ],
            ],
        ]);

        $list = $model->requisiteList();
        if ($list) {
            foreach ($list as $id => $item) {
                $requisite = $model->byId($id);
                if (!$requisite) continue;
                echo
                $this->render('@frontend/views/' . $requisite->modelId() . '/view_one', [
                    'model' => $requisite,                    
                ]);
            }
        } else {
        }

        ?>
    </div>

    <div class="clear"></div>
    <h2>Пароль</h2>

    <?php
    $form = ActiveForm::begin(['action' => 'user/change-password']);
    echo FormGrid::widget([
        'model' => $passwordModel,
        'form' => $form,
        'rows' => $layoutPsw
    ]);
    ActiveForm::end();
    ?>





    <?php

    if (YII_DEBUG) {
        $finTime = microtime(true);
        $delta = $finTime - $startTime;
        echo $delta . ' сек.';
    }
    ?>

</div>
