<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveField;
use frontend\models\User;
use frontend\models\SignupForm;

use dektrium\user\widgets\Connect;




$this->title = 'Регистрация заказчика';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="site-signup">
    <h1><?/*= Html::encode($this->title) */?></h1>



    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php /*$form = ActiveForm::begin(['id' => 'form-signup']); */?>

                <?/*= $form->field($model, 'username')->textInput(['autofocus' => true]) */?>

                <?/*= $form->field($model, 'email') */?>

                <?/*= $form->field($model, 'password')->passwordInput() */?>

                <div class="form-group">
                    <?/*= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) */?>
                </div>

            <?php /*ActiveForm::end(); */?>
        </div>
    </div>

</div>
-->
<div class="container-fluid"  id="reg" >

    <div class="container">
        <div style="">
            <!--<div class="row reg-logo">
                <div class="col-md-3">
                    <div class="header-padding-left">
                        <div class="logo">
                            vseadresa<span class="pro">.pro</span>
                        </div>
                        <div class="under-logo-text">
                            портал юридических адресов
                        </div>

                    </div>
                </div>
                <div class="col-md-8">
                    <img src="/img/close.png" class="pull-right close-popup" alt="">
                </div>
            </div>-->
            <!-- <div class="row reg-logo">
                <div class="col-md-3">
                    <div class="header-padding-left">
                        <div class="registration-title">
                            регистрация <br>
                            заказчика адреса:
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="row">
                <?php
                    //echo "<pre>", print_r($model->getErrors()), "</pre>";

                    if(count($model->getErrors())){
                        foreach ($model->getErrors() as $error){
                            echo "<p style='text-align: center'>".$error[0]."</p>";

                        }
                    }
                ?>
                <?php $form = ActiveForm::begin([
                        'id' => 'form-signup',
                    'enableAjaxValidation' => true,
                    'enableClientValidation'=>true,
	

                ]); ?>
                <div class="col-md-3 reg-tutorial">
                    <div class="flex">
                        <sup>*</sup>
                        <div>помечены поля, <br>
                            обязательные для заполнения</div>
                    </div>

                </div>
                <div class="col-md-6 col-md-offset-1">
                    <div class="col-md-12">
                        <h2>Регистрация
                            заказчика адреса</h2>
                        <div class="field-row flex al">
                            <div class="form-label">
                                <label for="" class="flex">
                                    <div class="label-content">фамилия, имя</div>
                                    <sup>*</sup>
                                </label>
                            </div>
                            <div class="form-input">
                                <?= $form->field($model, 'fullname')->label(false)->textInput() ?>
                            </div>
                        </div>
                        <div class="field-row flex al">
                            <div class="form-label">
                                <label for="" class="flex">
                                    <div class="label-content">email</div>
                                    <sup>*</sup>
                                </label>
                            </div>
                            <div class="form-input">
                                <?= $form->field($model, 'username')->label(false)->textInput() ?>
                            </div>
                            <div class="field-subrow">
                                является уникальным
                                идентификатором пользователя
                                в системе и используется
                                для авторизации
                            </div>
                        </div>
                        <div class="field-row flex al">
                            <div class="form-label">
                                <label for="" class="flex">
                                    <div class="label-content">пароль</div>
                                    <sup>*</sup>
                                </label>
                            </div>
                            <div class="form-input">

                                <?php echo $form->field($model, 'password')->label(false)->passwordInput() ?>
                            </div>
                        </div>
                        <div class="field-row flex al">
                            <div class="form-label">
                                <label for="" class="flex">
                                    <div class="label-content">подтверждение пароля</div>
                                    <sup>*</sup>
                                </label>
                            </div>
                            <div class="form-input">

                                <?= $form->field($model, 'password_repeat')->label(false)->passwordInput() ?>
                            </div>
                        </div>
                        <div class="field-row flex al">
                            <div class="form-label">
                                <label for="" class="flex">
                                    <div class="label-content">телефон</div>
                                    <sup>*</sup>
                                </label>
                            </div>
                            <div class="form-input">
                                <?= $form->field($model, 'phone')->label(false)->textInput(['class'=>"phone-format", 'data-inputmask'=>"'mask': '".\common\models\Entity::getMaskAliasList()['phone']."'"]) ?>
                            </div>
                        </div>
                        <div class="field-row flex al">
                            <div class="form-label">
                                <label for="" class="flex">
                                    <div class="label-content">правовой статус</div>
                                    <sup>*</sup>
                                </label>
                            </div>
                            <div class="form-input select-client-form-div">
                                <?php echo $form->field($model , 'form')->label(false)->dropDownList(SignupForm::attrLists()['form'],[]); ?>
                                <!--<select name="user_role" id="">-->


                                <?php
                               // var_dump(SignupForm::attrLists()['form']);
                                ?><?php
                               /* foreach(SignupForm::attrLists()['form'] as $k=>$v):*/?><!--
                                    <option value="<?/*=$k*/?>"><?/*=$v;*/?></option>

                                --><?php /*endforeach; */?>
                                <!-- </select> -->
                            </div>
                        </div>
                        <div class="field-row flex al organisation-name-field hidden">
                            <div class="form-label">
                                <label for="" class="flex">
                                    <div class="label-content">название организации</div>
                                    <sup>*</sup>
                                </label>
                            </div>
                            <div class="form-input">
                                <?= $form->field($model, 'full_name')->label(false)->textInput() ?>
                            </div>

                        </div>
                        <div class="field-row flex field-checkbox-row">
                            <div class="form-label">
                                <label for="" class="flex">
                                    <div class="label-content">подтверждение <?=Html::a('договора оферты',['page/offerta'], ['target'=>'_blank', 'title' => 'Просмотреть договор оферты'])?></div>
                                </label>
                            </div>
                            <div class="form-input">
                                <div class="offerta-box">

                                </div>
                                <!--<input type="checkbox" class="hidden-cb"  />-->
                                <?= $form->field($model, 'offerta_confirm')->checkbox(['uncheck' => null, 'value' =>0, 'class'=>'hidden-cb offerta'], false)->label(false); ?>

                            </div>
                        </div>
                        <div class="field-row flex al">
                            <!--<a href="" class="reg-button">
                                зарегистрироваться
                            </a>-->
                            <?= Html::submitButton('зарегистрироваться', ['class' => 'reg-button', 'name' => 'signup-button']) ?>
                        </div>

                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

        </div>
    </div>
</div>
<?php
/*$this->registerJsFile(
    "$('#form-signup').on('afterValidate', function (e, messages) {
    console.log(messages, 'xxxxxxxxxxxxxxxxxxxx');
});",
    \yii\web\View::POS_END,
);*/
?>

