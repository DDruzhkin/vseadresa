<?php
if (YII_DEBUG) $startTime = microtime(true);

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use frontend\models\Address;
use frontend\models\Order;
use frontend\models\Delivery;
use backend\models\Rperson;
use backend\models\Rcompany;
use backend\models\Rip;
use common\models\AddressTaxAgency;
use yii\web\View;
use common\models\ServicePortal;

//die((string)$order->id);
?>

<script>
    function refreshControls() {
        refreshDelivary();
    }

    function refreshDelivary() {
        if ($("#order-delivery_type_id").val() ==<?=Delivery::byAlias('pickup')->id?>) {
            $("fieldset#w2").hide();
            if (!$("#order-delivery_address").val()) $("#order-delivery_address").val('-');
        } else {
            $("fieldset#w2").show();
            if ($("#order-delivery_address").val() == '-') $("#order-delivery_address").val('');
        }
    }

    function durationChanged() {
        var $select = $('#order-duration');

        if ($select.length) {
            var $option = $select.find('option:selected');

            var params = $option.val().split('_');
            var type = params[1] === 'first' ? 'register' : 'change';

            $('#order-scenario').val(type);

            $.each(['company_inn', 'company_ogrn', 'company_kpp'], function (index, value) {
                var $element = $('.field-order-' + value);

                if (params[1] === 'change') {
                    $element.show();
                }
                else {
                    $element.hide();
                }
            });
        }
    }

    var availableInn = [77, 78];

    $(document).ready(function () {
        /*$('#form-order').on('beforeValidateAttribute', function (event, attribute, messages) {
            attribute.validate = function (attribute, value, messages, deferred, $form) {
                if (attribute.name === 'company_inn' && value.length === 10) {
                    var region = parseInt(value.substring(0, 2));
                    //var department = parseInt(value.substring(2, 4));

                    if (availableInn.indexOf(region) === -1) {
                        $form.yiiActiveForm('updateAttribute', attribute.id, [
                            'Владелец адреса ограничил регистрацию для ИНН зарегистрированных в вашем регионе.'
                        ]);
                    }
                }
            };

            return true;
        });*/

        /*$("#order-company_inn").on('change.yii',function (e) {
            var inn = $(e.target).val();

            if (inn.length === 10) {
                var region = inn.substring(0, 2);
                var department = inn.substring(2, 4);

                console.log('region: ' + region + ' department: ' + department)
            }

            $('#form-order').yiiActiveForm('updateAttribute', '#order-company_inn', ["I have an error..."]);
        });*/

// Изменение способа доставки		

        $("#order-delivery_type_id").change(function () {
            refreshDelivary();
        });

        durationChanged();

        $('#order-duration').change(function () {
            durationChanged();
        });

// Wizard
        $('button.next').click(function () {
            $("#form-order").attr("action", "<?php echo Url::to(["/address/$address->id/delivery"])?>");
        });

        $('button.prior').click(function (e) {
            e.preventDefault();
            location.href =<?php echo Url::to(["customer/order/update/$order->id"])?>
            //$("#form-order").attr("action", "<?php// echo Url::to(["/address/$address->id/buy"])?>");
        });


        refreshControls();

    });
</script>

<?php


function controls($name, $address = null, $order = null)
{
    $model = false;

    switch ($name) {
        case 'add_guarantee':
            $guarantee=ServicePortal::byAlias('guarantee');
            return [
                'type' => Form::INPUT_CHECKBOX,
                'label' => '<p class="service-title">Добавить Гарантию</p><p class="service-descr">Цена: '.number_format($guarantee->price,2,',',' ').' рублей</p><p class="service-descr">Услуга гарантирует успешное прохождение всех административных процедур</p>',
            ];

        case 'guarantee_info':
            return [    // embed raw HTML content
                'type' => Form::INPUT_RAW,
                'value' => '<div style="text-align: right; margin-top: 20px;">' .
                    Html::a('Подробнее о гарантии...', ['/page/guarantee']) .
                    '</div>'
            ];

        case 'created_at_view':
            return ['type' => Form::INPUT_TEXT, 'label' => Order::getMeta('created_at_view', 'label'), 'hint' => '' . Order::getMeta('created_at_view', 'comment'), 'options' => ['readonly' => 'true',]];
        case 'first_step':
            return ['type' => Form::INPUT_HIDDEN, 'label' => 'first_step'];
            case 'last_step':
        return ['type' => Form::INPUT_HIDDEN, 'label' => 'last_step'];
        case 'number':
            return ['type' => Form::INPUT_TEXT, 'label' => Order::getMeta('number', 'label'), 'hint' => '' . Order::getMeta('number', 'comment'), 'options' => []];
        case 'customer_id':
            return ['type' => Form::INPUT_DROPDOWN_LIST, 'label' => Order::getMeta('customer_id', 'label'), 'items' => Order::getListValues('customer_id'), 'hint' => '' . Order::getMeta('customer_id', 'comment'), 'options' => []];
        case 'customer_r_id':
            return ['type' => Form::INPUT_DROPDOWN_LIST, 'label' => Order::getMeta('customer_r_id', 'label'), 'items' => $model->getRelatedListValues('customer_r_id', false, false, true), 'hint' => '', 'options' => []];
        case 'services':
            $services = $address->getServicePrices()->andFilterWhere(['enabled' => 1])->with('serviceName')->asArray()->all();
            //var_dump($services);
            foreach ($services as $service) {
                $str = '<p class="service-title">' . $service['name'] . '</p><p class="service-descr">';
                $str .= $service['price'] ? 'Цена: ' . number_format($service['price'], 2, ',', ' ') . ' рублей</p>' : ($service['price11'] ? 'Цена: ' . number_format($service['price11'], 2, ',', ' ') . ' рублей</p>' : 'Бесплатно<p>');
                if (isset($service['serviceName']['description']) && strlen($service['serviceName']['description']))
                    $str .= '<p  class="service-descr">' . $service['serviceName']['description'] . '</p>';
                $result[$service['id']] = $str;
            }
            return [
                'type' => Form::INPUT_CHECKBOX_LIST,
                'label' => '',
                'items' => $result,//Order::objectsByTemplate($address->getServicePrices()->andFilterWhere(['enabled' => 1])->all(), '<p>%name%</p><p>Цена: %price%рублей</p>'),
                'hint' => '',
                'options' => ['multiple' => true]
            ];

//						'status_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('status_id', 'label'), 'hint'=>''.Order::getMeta('status_id', 'comment'), 'items' => $model -> getStatusList()],
        case 'customer_r_id_view':
            return ['type' => Form::INPUT_TEXT, 'label' => Order::getMeta('customer_r_id_view', 'label'), 'hint' => '<a title="Перейти на карточку владельца" href="' . Url::to([$model->getCaption('cobject/update/%customer_r_id_view%')]) . '">' . Icon::show('link') . '</a>' . Order::getMeta('customer_r_id_view', 'comment'), 'options' => ['readonly' => 'true',]];
        case 'requisites':
            $label = 'Реквизиты для договора аренды; <small>Название организации (полное и сокращенное) ФИО Генерального ди⁠ректора (должность) ИНН, КПП, ОГРН р/с в банке, БИК, к/с Примечания, пожелания к договору</small>';
            return ['type' => Form::INPUT_TEXTAREA, 'label' => $label, 'hint' => '' . Order::getMeta('requisites', 'comment'), 'options' => ['rows' => 10]];

        case 'requisite_type':
            return [
                'type' => Form::INPUT_DROPDOWN_LIST,
                'label' => 'Реквизиты для договора аренды',
                //'items' => Yii::$app->user->identity->requisiteList(),
                'items' => [
                    'ИП' => 'Индивидуальный предприниматель',
                    'Юрлицо' => 'Юридическое лицо',
                    'Физлицо' => 'Физическое лицо'
                ]
            ];
            break;

        case 'owner_id_view':
            return ['type' => Form::INPUT_TEXT, 'label' => Order::getMeta('owner_id_view', 'label'), 'hint' => '<a title="Перейти на карточку владельца" href="' . Url::to([$model->getCaption('user/update/%owner_id_view%')]) . '">' . Icon::show('link') . '</a>' . Order::getMeta('owner_id_view', 'comment'), 'options' => ['readonly' => 'true',]];

        //case 'address_id': return ['type'=>Form::INPUT_HIDDEN, 'value' => $address_id];
        case 'address_price_view':
            return ['type' => Form::INPUT_TEXT, 'label' => Order::getMeta('address_price_view', 'label'), 'hint' => '' . Order::getMeta('address_price_view', 'comment'), 'options' => ['readonly' => 'true',]];

        case 'duration':
            {
                //$address = Address::findOne([$address_id]);
                $list = [];

                foreach (['price_first_6', 'price_first_11', 'price_change_6', 'price_change_11'] as $key) {
                    if ($address->{$key} > 0) {
                        $list[$key] = Address::getMeta($key, 'description') . ' (' . round($address->{$key}) . ' руб.)';
                    }
                }

                return [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'label' => Order::getMeta('duration', 'label'),
                    'items' => $list,
                    'hint' => Order::getMeta('duration', 'comment'), 'options' => []
                ];
            }
        case 'delivery_type_id':
            {
                return ['type' => Form::INPUT_DROPDOWN_LIST, 'label' => Order::getMeta('delivery_type_id', 'label'), 'items' => Order::getListValues('delivery_type_id', false, true), 'hint' => '' . Order::getMeta('delivery_type_id', 'comment'), 'options' => []];
            }
        case 'delivery_description':
            {
                $delivery = Delivery::find()->select('alias, price')->indexBy('alias')->asArray()->all();
                $html = '<p>Наша компания осуществляет доставку документов с помощью собственной команды курьеров. Ниже представлены расценки и условия:</p><p>' . number_format($delivery['metro']['price'], 2, ',', '') . ' рублей – доставка документов до станции метро в течение дня с 9-00 до 18-00</p>
<p>' . number_format($delivery['address']['price'], 2, ',', '') . ' рублей – доставка документов в течение дня с 9-00 до 18-00 по точному адресу</p>
<p>' . number_format($delivery['address-time']['price'], 2, ',', '') . ' рублей - доставка документов по точному адресу к определенному согласованному времени с 9-00 до 21-00.</p>
<p>Доставка осуществляется в пределах станций московского метрополитена , только в рабочие дни.</p>';
                return $html;
            }
        case 'delivery_contacts':
            return ['type' => Form::INPUT_TEXT, 'label' => Order::getMeta('delivery_contacts', 'label'), 'hint' => '' . Order::getMeta('delivery_contacts', 'comment'), 'options' => []];
        case 'delivery_address':
            return ['type' => Form::INPUT_TEXT, 'label' => Order::getMeta('delivery_address', 'label'), 'hint' => '' . Order::getMeta('delivery_address', 'comment'), 'options' => []];
        case 'payment_type':
            return ['type' => Form::INPUT_DROPDOWN_LIST, 'label' => Order::getMeta('payment_type', 'label'), 'items' => Order::getList('payment_type'), 'hint' => '' . Order::getMeta('payment_type', 'comment'), 'options' => []];
        case 'summa_view':
            return ['type' => Form::INPUT_TEXT, 'label' => Order::getMeta('summa_view', 'label'), 'hint' => '' . Order::getMeta('summa_view', 'comment'), 'options' => ['readonly' => 'true',]];

        case 'comment':
            return ['type' => Form::INPUT_TEXTAREA, 'label' => 'Комментарий', 'hint' => '', 'options' => []];

        case 'tax_system_vat':
            return ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\checkbox\CheckboxX', 'label' => Rcompany::getMeta('tax_system_vat', 'label'), 'hint' => '' . Rcompany::getMeta('tax_system_vat', 'comment'), 'options' => ['pluginOptions' => ['threeState' => false],]];

        case 'prior':
            return ['type' => Form::INPUT_RAW, 'value' => Html::a('Назад', "/customer/order/update/$order->id?update=1", ['class' => 'reg-button prior'])];
            return ['type' => Form::INPUT_RAW, 'value' => Html::submitButton('Назад', ['class' => 'reg-button prior'])];
        case 'next':
            return ['type' => Form::INPUT_RAW, 'value' => Html::submitButton('Далее', ['class' => 'reg-button next submit_btn'])];
        case 'submit':
            return ['type' => Form::INPUT_RAW, 'value' => Html::submitButton('Сохранить', ['class' => 'reg-button next'])];


        case 'actions':
            return [    // embed raw HTML content
                'type' => Form::INPUT_RAW,
                'value' => '<div style="text-align: right; margin-top: 20px">' .
                    Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) .
                    '</div>'
            ];

        default:
            return [];

    }
}

$reg_msk = (int)$address->reg_msk;
$reg_mo = (int)$address->reg_mo;
$reg_other = (int)$address->reg_other;
$name_arr = [];
$reg_msk ? $name_arr[] = 'Москва' : null;
$reg_mo ? $name_arr[] = 'Московская область' : null;
$reg_other ? $name_arr[] = 'Другие регионы РФ' : null;

$requisites = [
    // Только для Юрлица
    'ogrn' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rcompany::getMeta('ogrn', 'label'), 'hint' => Rcompany::getMeta('ogrn', 'comment'), 'options' => ['mask' => 9999999999999,]],
    'name' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('name', 'label'), 'hint' => Rcompany::getMeta('name', 'comment'), 'options' => []],
    'full_name' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('full_name', 'label'), 'hint' => Rcompany::getMeta('full_name', 'comment'), 'options' => []],
    'dogovor_osnovanie' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('dogovor_osnovanie', 'label'), 'hint' => Rcompany::getMeta('dogovor_osnovanie', 'comment'), 'options' => []],
    'dogovor_signature' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('dogovor_signature', 'label'), 'hint' => Rcompany::getMeta('dogovor_signature', 'comment'), 'options' => []],
    'dogovor_face' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('dogovor_face', 'label'), 'hint' => Rcompany::getMeta('dogovor_face', 'comment'), 'options' => []],
    'dogovor_city' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('dogovor_city', 'label'), 'hint' => Rcompany::getMeta('dogovor_city', 'comment'), 'options' => []],
    'dogovor_address' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('dogovor_address', 'label'), 'hint' => Rcompany::getMeta('dogovor_address', 'comment'), 'options' => []],

    // ИП
    'ogrnip' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rip::getMeta('ogrnip', 'label'), 'hint' => '' . Rip::getMeta('ogrnip', 'comment'), 'options' => ['mask' => 999999999999999,]],
    'sv_seriya' => ['type' => Form::INPUT_TEXT, 'label' => Rip::getMeta('sv_seriya', 'label'), 'hint' => '' . Rip::getMeta('sv_seriya', 'comment'), 'options' => []],
    'sv_nomer' => ['type' => Form::INPUT_TEXT, 'label' => Rip::getMeta('sv_nomer', 'label'), 'hint' => '' . Rip::getMeta('sv_nomer', 'comment'), 'options' => []],
    'sv_date' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\date\DatePicker', 'label' => Rip::getMeta('sv_date', 'label'), 'hint' => '' . Rip::getMeta('sv_date', 'comment'), 'options' => ['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose' => true, 'format' => 'dd.mm.yyyy'],]],
    'kpp' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rip::getMeta('kpp', 'label'), 'hint' => '' . Rip::getMeta('kpp', 'comment'), 'options' => ['mask' => 999999999,]],
    // Физлицо
    'bdate' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\date\DatePicker', 'label' => Rperson::getMeta('bdate', 'label'), 'hint' => '' . Rperson::getMeta('bdate', 'comment'), 'options' => ['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose' => true, 'format' => 'dd.mm.yyyy'],]],

    // Юрлицо, Физлицо и ИП
    'inn' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rperson::getMeta('inn', 'label'), 'hint' => '' . Rperson::getMeta('inn', 'comment'), 'options' => ['mask' => 999999999999,]],
    'fname' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('fname', 'label'), 'hint' => '' . Rperson::getMeta('fname', 'comment'), 'options' => []],
    'mname' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('mname', 'label'), 'hint' => '' . Rperson::getMeta('mname', 'comment'), 'options' => []],
    'lname' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('lname', 'label'), 'hint' => '' . Rperson::getMeta('lname', 'comment'), 'options' => []],
    'pasport_number' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rperson::getMeta('pasport_number', 'label'), 'hint' => '' . Rperson::getMeta('pasport_number', 'comment'), 'options' => ['mask' => Rperson::getMaskByAlias("pasport"),]],
    'pasport_date' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\date\DatePicker', 'label' => Rperson::getMeta('pasport_date', 'label'), 'hint' => '' . Rperson::getMeta('pasport_date', 'comment'), 'options' => ['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['todayHighlight' => true, 'autoclose' => true, 'endDate' => '0d', 'format' => 'dd.mm.yyyy'],]],
    'pasport_organ' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('pasport_organ', 'label'), 'hint' => '' . Rperson::getMeta('pasport_organ', 'comment'), 'options' => []],
    'address' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('address', 'label'), 'hint' => '' . Rperson::getMeta('address', 'comment'), 'options' => []],
    'bank_bik' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rperson::getMeta('bank_bik', 'label'), 'hint' => '' . Rperson::getMeta('bank_bik', 'comment'), 'options' => ['mask' => 999999999,]],
    'account' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rperson::getMeta('account', 'label'), 'hint' => '' . Rperson::getMeta('account', 'comment'), 'options' => ['mask' => Rperson::getMaskByAlias("account"),]],
    'account_owner' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('account_owner', 'label'), 'hint' => '' . Rperson::getMeta('account_owner', 'comment'), 'options' => []],
    'bank_name' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('bank_name', 'label'), 'hint' => '' . Rperson::getMeta('bank_name', 'comment'), 'options' => []],
    'bank_cor' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rperson::getMeta('bank_cor', 'label'), 'hint' => '' . Rperson::getMeta('bank_cor', 'comment'), 'options' => ['mask' => '99999999999999999999',]],
];

$controls = [
    'scenario' => ['type' => Form::INPUT_HIDDEN, 'options' => ['value' => \common\models\Contract::SCENARIO_REGISTER]],
    'requisite_company_name' => ['type' => Form::INPUT_TEXT, 'label' => $contract->getAttributeLabel('company_name')],
    'requisite_inn' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Order::getMeta('label', 'requisite_inn'), 'options' => ['mask' => 9999999999]],
    'requisite_ogrn' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Order::getMeta('label', 'requisite_ogrn'), 'options' => ['mask' => 9999999999999]],
    'requisite_kpp' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Order::getMeta('requisite_kpp', 'label'), 'options' => ['mask' => 999999999]],
    'requisite_bank_name' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('requisite_bank_name', 'label')],
    'requisite_bank_id' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Order::getMeta('requisite_bank_id', 'label'), 'options' => ['mask' => 999999999]],
    'requisite_bank_account' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Order::getMeta('requisite_bank_account', 'label'), 'options' => ['mask' => Rperson::getMaskByAlias('account')]],
    'requisite_bank_correspondent_account' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Order::getMeta('requisite_bank_correspondent_account', 'label'), 'options' => ['mask' => Rperson::getMaskByAlias('account')]],
    'company_name' => ['type' => Form::INPUT_TEXT, 'label' => $contract->getAttributeLabel('company_name')],
    'company_full_name' => ['type' => Form::INPUT_TEXT, 'label' => $contract->getAttributeLabel('company_full_name')],
    'company_inn' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => $contract->getAttributeLabel('company_inn'), 'options' => ['mask' => 9999999999]],
    'company_ogrn' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => $contract->getAttributeLabel('company_ogrn'), 'options' => ['mask' => 9999999999999]],
    'company_kpp' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => $contract->getAttributeLabel('company_kpp'), 'options' => ['mask' => 999999999]],
    'execution_officer_full_name' => ['type' => Form::INPUT_TEXT, 'label' => $contract->getAttributeLabel('execution_officer_full_name')],
    'execution_officer_envoy_ship' => ['type' => Form::INPUT_TEXT, 'label' => $contract->getAttributeLabel('execution_officer_envoy_ship')],
    'execution_officer_recording_city' => ['type' => Form::INPUT_TEXT, 'label' => $contract->getAttributeLabel('execution_officer_recording_city')],
    'execution_officer_recording_address' => ['type' => Form::INPUT_TEXT, 'label' => $contract->getAttributeLabel('execution_officer_recording_address')],
    'tax_system_vat' => ['type' => Form::INPUT_CHECKBOX, 'label' => Rcompany::getMeta('tax_system_vat', 'label')],
];

////////////////////////////////////////////////////
// Подготовка контролов для формы
//Остальные контролы для каждого шага

$layouts = [
    1 => [
        ['attributes' => ['duration' => controls('duration', $address)]],
        ['attributes' => ['scenario' => $controls['scenario']]],

        ['attributes' => ['header' => ['type' => Form::INPUT_RAW,
            'value' => '<h2>Реквизиты для оформления договора аренды</h2>'
        ]]],

        ['attributes' => ['company_name' => $controls['company_name']]],
        ['attributes' => ['company_full_name' => $controls['company_full_name']]],
        ['attributes' => ['execution_officer_full_name' => $controls['execution_officer_full_name']]],
        ['attributes' => ['execution_officer_envoy_ship' => $controls['execution_officer_envoy_ship']]],
        ['attributes' => ['execution_officer_recording_city' => $controls['execution_officer_recording_city']]],
        ['attributes' => ['execution_officer_recording_address' => $controls['execution_officer_recording_address']]], ['attributes' => ['header' => ['type' => Form::INPUT_RAW,
            'value' => '<span class="loc_error" style="display:none">Регистрация недоступна для следующих субъектов: ' . implode(', ', $name_arr) . '</span>'
        ]]],
        ['attributes' => ['company_inn' => $controls['company_inn']]],
        ['attributes' => ['company_ogrn' => $controls['company_ogrn']]],
        ['attributes' => ['company_kpp' => $controls['company_kpp']]],
        ['attributes' => ['header' => ['type' => Form::INPUT_RAW,
            'value' => '<span class="kpp_error" style="display:none">Регистрация недоступна для ИФНС №' . implode(', №', AddressTaxAgency::getAddressAgencies($address->id)) . '</span>'
        ]]],
        ['attributes' => ['comment' => controls('comment')]],

        ['attributes' => ['header' => ['type' => Form::INPUT_RAW,
            'value' => '<h2>Реквизиты для оформления платёжных документов</h2>'
        ]]],

        ['attributes' => ['requisite_company_name' => $controls['requisite_company_name']]],
        ['attributes' => ['requisite_inn' => $controls['requisite_inn']]],
        ['attributes' => ['requisite_ogrn' => $controls['requisite_ogrn']]],
        ['attributes' => ['requisite_kpp' => $controls['requisite_kpp']]],
        ['attributes' => ['requisite_bank_name' => $controls['requisite_bank_name']]],
        ['attributes' => ['requisite_bank_id' => $controls['requisite_bank_id']]],
        ['attributes' => ['requisite_bank_account' => $controls['requisite_bank_account']]],
        ['attributes' => ['requisite_bank_correspondent_account' => $controls['requisite_bank_correspondent_account']]],
        ['attributes' => ['tax_system_vat' => $controls['tax_system_vat']]],
        ['attributes' => ['add_guarantee' => controls('add_guarantee')]],
        ['attributes' => ['serviceIdList' => controls('services', $address)]],
        ['attributes' => ['next' => controls('next')]]
    ],

    2 => [
        [
            'attributes' => [
                'last_step' => controls('last_step'),
            ],
        ],
        [
            'attributes' => [
                'delivery_type_id' => controls('delivery_type_id'),
            ],
        ],
        [
            'attributes' => [
                'delivery_address' => controls('delivery_address'),
            ],
        ],

        [
            'attributes' => [
                'delivery_contacts' => controls('delivery_contacts'),
            ],
        ],
        ['attributes' => ['header' => ['type' => Form::INPUT_RAW,
            'value' => controls('delivery_description'),
        ]]],
        [
            'attributes' => [
                'prior' => controls('prior', null, $order),
                'next' => controls('next'),
            ],
        ],
    ],

    3 => [
        /*
                [
                    'attributes' => [
                        'address_price'=>[
                            'type'=>Form::INPUT_RAW,
                                'value'=>  getHTMLValue($model -> getMeta('address_price', 'label'), $model -> address_price),
                        ]
                    ],
                ],
                [
                    'attributes' => [
                        'service_summa'=>[
                            'type'=>Form::INPUT_RAW,
                                'value'=>  getHTMLValue('стоимость набора дополнительных услуг', (float)$model -> summa - (float)$model -> address_price),
                        ]
                    ],
                ],
                [
                    'attributes' => [
                        'address_price'=>[
                            'type'=>Form::INPUT_RAW,
                                'value'=>  getHTMLValue($model -> getMeta('delivery_address', 'label'), $model -> delivery_address),
                        ]
                    ],
                ],

                [
                    'attributes' => [
                        'address_price'=>[
                            'type'=>Form::INPUT_RAW,
                                'value'=>  getHTMLValue($model -> getMeta('delivery_contacts', 'label'), $model -> delivery_contacts),
                        ]
                    ],
                ],

        */
    ],


];
/*

$url = ["/order/update/{$model->id}", 'step'=> $step + ($step < 4?1:0)];


if (!$model -> id) $url['address_id'] = $address_id;
$url = Url::to($url);

$formdata = $data[$step];

$formdata[] =

	[
		'attributes' => [
				'actions'=>[
					'type'=>Form::INPUT_RAW,
						'value'=>  '
						<div class="clear"></div>
				<div class="field-row flex al radio">
                            '.(
							($step <= 1)?'':
                            Html::submitButton('Назад', ['class' => 'order-button-back', 'name' => 'order-button-back', 'onClick' => '$("#form-order").attr("action", "'.Url::to(["/order/update/{$model->id}",  'step'=> $step - ($step > 1?1:0)]).'");'])
							).
							(
							Html::submitButton('далее', ['class' => 'order-button', 'name' => 'order-button', 'onClick' => '$("#form-order").attr("action", "'.$url.'");'])
							).'
                </div>
				',
				],
		],
	];

*/

?>


<script>

</script>
<?php


$taxAgencies = json_encode(AddressTaxAgency::getAddressAgencies($address->id));

$script = <<< JS
    let agencies=$taxAgencies,
    reg_msk=$reg_msk,
    reg_mo=$reg_mo,
    reg_other=$reg_other,
    can_reg=true;
    $('#order-company_kpp').change(function () {
        let tax_number = parseInt($(this).val().slice(2, 4), 10).toString();
        if (agencies.indexOf(tax_number) != -1) {
            $('.kpp_error').fadeIn();
            $('.submit_btn').addClass('submit_btn-disabled');
            $('.submit_btn').prop('disabled', true);
        } else {
            $('.kpp_error').fadeOut();
            $('.submit_btn').removeClass('submit_btn-disabled');
            $('.submit_btn').prop('disabled', false);
        }
    })
    $('#order-execution_officer_recording_address, #order-execution_officer_recording_city').change(function () {
            let city = $('#order-execution_officer_recording_city').val() ? $('#order-execution_officer_recording_city').val() : '',
                addr = $('#order-execution_officer_recording_address').val() ? $('#order-execution_officer_recording_address') : '';
            $.get('https://geocode-maps.yandex.ru/1.x/?format=json&geocode=' + city + ', ' + addr, function (data) {
                    let addr_obj = data.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components,
                        locality = '';
                    for (let component of addr_obj) {
                        if (component.kind == 'province') {
                            locality = component.name;
                        }
                    }
                    if (reg_msk || reg_mo || reg_other) {
                        can_reg = true;
                        switch (locality) {
                            case 'Москва':
                                if (reg_msk) {
                                    can_reg = !can_reg;
                                }
                                break;
                            case 'Московская область':
                                if (reg_mo) {
                                    can_reg = !can_reg;
                                }
                                break;
                            default:
                                if (reg_other) {
                                    can_reg = !can_reg;
                                }
                                break;
                        }
                    }
                    console.log(can_reg)
                    if (!can_reg) {
                        $('.loc_error').fadeIn();
                        $('.submit_btn').addClass('submit_btn-disabled');
                        $('.submit_btn').prop('disabled', true);
                    } else {
                        $('.loc_error').fadeOut();
                        $('.submit_btn').removeClass('submit_btn-disabled');
                        $('.submit_btn').prop('disabled', false);
                    }
                }
            )
        }
    )
JS;
$this->registerJs($script, View::POS_READY);

$form = ActiveForm::begin(['id' => 'form-order']);

echo FormGrid::widget([
    'model' => $order,
    'form' => $form,
    'rows' => $layouts[$step]
]);

ActiveForm::end();

if (YII_DEBUG) {
    $finTime = microtime(true);
    $delta = $finTime - $startTime;
    echo $delta . ' сек.';
}
?>


