<?php


/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */


use common\models\Entity;
use frontend\models\Order;
use frontend\models\OrderSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Inflector;

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?=$this->title;?></h1>
<div class="row">
    <?= \frontend\components\FilterWindow::getFilterButton($searchModel) ?>
</div>
<!--
                <div class="col-md-11 flex put-to-right  filter-callout  <?php if (isset($_GET["AddressSearch"])) {
    echo "active-filter";
} ?>">
                    <i class="fa fa-filter"></i>
                </div>
            </div>

-->

<?php
$dataProvider->query->andFilterWhere(['customer_id' => Entity::currentUser(), 'address_id' => NULL]); // заказы только от текущего заказчика


	

?>

<div class="grid-table-container">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,

        'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],

        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
            [
                'class' => 'yii\grid\Column',
                'content' => function ($data) {
					$order = Order::findOne($data['id']);
					if (!$order) return '';
					
					$items = $order -> getCustomerMenuItems();										
                    return NavX::widget([
                        'options' => ['class' => 'nav nav-context'],
                        'encodeLabels' => false,
						'items' => [
                            [
                                'label' => Icon::show('bars'), 'url' => '#',
                                'items' => $items,

                            ],

                        ],
						
/*						
						'items' => [
                            [
                                'label' => Icon::show('bars'), 'url' => '#',
                                'items' => [
                                    [
                                        'label' => 'Карточка заказа',
                                        'url' => Url::toRoute(['view', 'id' => $data['id']]),
                                    ],

                                    [
                                        'label' => 'Счета',
                                        'url' => Url::toRoute(['document', 'id' => $data['id']]),
                                    ],

                                    '<li class="divider"></li>',
                                    [
                                        'label' => 'Изменить',
                                        'url' => Url::toRoute(['update', 'id' => $data['id']]),
                                    ],
                                    [
                                        'label' => 'Новый',
                                        'url' => Url::toRoute(['clone', 'id' => $data['id']]),
                                    ],
                                ],

                            ],

                        ],
						*/
						
                    ]);
                }
            ],


            ['attribute' => 'number',
                'label' => Order::getMeta('number', 'label'),
                'contentOptions' => ['class' => 'col-number '],
                'headerOptions' => ['class' => 'col-number '],
                'footerOptions' => ['class' => 'col-number '],
				'value'  => function($data) {
					$g = $data['guaratee_id'].' ';
					return $g.Html::a($data['number'], Url::to(['customer/order/view/'.$data['id']]), array("class"=>"table-link"));
				},
				'format' => 'raw'
            ],
            ['attribute' => 'created_at',
                'label' => Order::getMeta('created_at', 'label'),
                'contentOptions' => ['class' => 'col-created_at '],
                'filterOptions' => ['class' => 'col-created_at'],
                'headerOptions' => ['class' => 'col-created_at '],
                'footerOptions' => ['class' => 'col-created_at '],


            ],
            ['attribute' => 'date_fin',
                'label' => Order::getMeta('date_fin', 'label'),
                'contentOptions' => ['class' => 'col-date_fin '],
                'filterOptions' => ['class' => 'col-date_fin'],
                'headerOptions' => ['class' => 'col-date_fin '],
                'footerOptions' => ['class' => 'col-date_fin '],

            ],			
            ['attribute' => 'status_id',
                'label' => Order::getMeta('status_id', 'label'),
                'contentOptions' => ['class' => 'col-status_id '],
                'filterOptions' => ['class' => 'col-status_id'],
                'headerOptions' => ['class' => 'col-status_id '],
                'footerOptions' => ['class' => 'col-status_id '],
                'filter' => Order::getExistsValues('status_id'),
                'value' => function ($data) {
					
					return  Html::a($data['status_id_view'], Url::to(['customer/order/view/'.$data['id']]), array("class"=>"table-link"));
                },
				'format' => 'raw'
            ],

            ['attribute' => 'summa',
                'label' => Order::getMeta('summa', 'label'),
                'contentOptions' => ['class' => 'col-summa '],
                'filterOptions' => ['class' => 'col-summa'],
                'headerOptions' => ['class' => 'col-summa '],
                'footerOptions' => ['class' => 'col-summa '],


                'format' => ['decimal', 2],
            ],

            [
                'class' => 'yii\grid\ActionColumn',

                'buttons' => [
                    'delete' => function ($url, $model) {

                        return in_array($model->status->alias, ['new', 'draft']) ?            // не во всех статусах можно удалять заказ


                            Html::a(
                                Icon::show('times'),
                                Url::toRoute(['delete', 'id' => $model->id]),

                                //['/customer/order/delete'],
                                [

                                    'title' => 'Удалить заказ',
                                    'class' => 'delete',
                                    'data-confirm' => 'Вы уверены, что хотите удалить этот заказ',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                    'data-params' => ['id' => $model->id],

                                ]
                            ) : '';

                    }
                ],
                'template' => '{delete}',


            ],
        ],
    ]); ?>
</div>


<?= $this->render('_filter', [
    'searchModel' => $searchModel,
]) ?>

