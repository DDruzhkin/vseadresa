<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

							
	<div class="article-item">
		<h1 class="article-header">
			<?=$page -> h1?>
		</h1>
		<h2>на адрес <?=$model -> address -> address?></h2>

		
	</div>							
							
	<div class="article-text article-item">
		<?=$page -> content?>
	</div>
	
	<div class="article-text">
	<?php $form = ActiveForm::begin(['method' => 'post', 'action' => Url::to(['/customer/order/guarantee'])]) ?>	
	
	<?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
	<?= Html::submitButton('Заказать услугу гарантии', ['class' => 'reg-button change-data']);?>			
			
	
	
	<?php ActiveForm::end(); ?>
		
	</div>