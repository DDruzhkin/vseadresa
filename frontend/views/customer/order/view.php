<?php

use common\models\Entity;
use frontend\components\OrderServicesWidget;
use frontend\components\OrderWidget;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model backend\models\Order */

$this->title = "Заказ " . $model->getCaption();
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => Url::to(['customer/order']), 'template' => '<span class="b-link">{link}</span><span class="separate-links">\</span>' . PHP_EOL];
$this->params['breadcrumbs'][] = $this->title;

$attrList = $model->getAttributes();
$attributes = [];
foreach ($attrList as $colName => $value) {
    $type = $model->getMeta($colName, 'type');
    $show = (int)$model->getMeta($colName, 'show');
    if (!$show) continue;
    $attributes[] = $colName . "_view" . ($type == 'text' ? ':ntext' : '');
}

?>


<div class="order-view">
    <div class="row">
        <div class="col-md-9">
            <h2>   <?= $model->getCaption() ?></h2>

            <div class="get-order-address">
                <?= $model->address_view ?>
            </div>


            <?php if ($model->guarantee_id):?>
                <div class="delivery_method">

                    <div class="labels">
                        Гарантия
                    </div>
                    <div class="order-value">
                        <?php
                        if ($model->guarantee_id) {
                            echo $model->guarantee_view;
                        };
                        ?>
                    </div>
                </div>
            <?php endif;?>
            <div class="requisites">
                <div class="labels">
                    Реквизиты для оформления адреса
                </div>
                <ul class="list-unstyled">
                    <li>Сокращённое название организации: <?= $model->company_name; ?></li>
                    <li>Полное название организации: <?= $model->company_full_name; ?></li>
                    <li>ФИО исполнительного лица: <?= $model->execution_officer_full_name; ?></li>
                    <li>Должность исполнительного лица: <?= $model->execution_officer_envoy_ship; ?></li>
                    <li>Город регистрации исполнительного
                        лица: <?= $model->execution_officer_recording_city; ?></li>
                    <li>Адрес регистрации исполнительного
                        лица: <?= $model->execution_officer_recording_address; ?></li>
                    <li>ИНН огранизации: <?= $model->company_inn; ?></li>
                    <li>ОГРН: <?= $model->company_ogrn; ?></li>
                    <li>КПП: <?= $model->company_kpp; ?></li>
                </ul>
            </div>

            <div class="requisites">
                <div class="labels">
                    Комментарий
                </div>
                <div class="order-value">
                    <?= $model->comment; ?>
                </div>
            </div>

            <?php if ($model->delivery_type_id_view): ?>

                <div class="delivery_method">

                    <div class="labels">
                        Способ доставки
                    </div>
                    <div class="order-value">
                        <?= $model->delivery_type_id_view; ?>
                    </div>
                </div>
            <?php endif; ?>


            <?php if ($model->duration): ?>
                <div class="labels">
                    выбранный период
                </div>

                <div class="order-value">
                    <span><?= $model->duration ?></span>
                </div>

                <?php if ($model->date_start && $model->date_fin): ?>

                    <div class="order-value">
                        <span><?= $model->date_start_view ?></span> - <span><?= $model->date_fin_view ?></span>
                    </div>
                <?php endif; ?>

            <?php endif; ?>

            <?php if ($model->addressPrice()): ?>
                <div class="labels">
                    стоимость аренды адреса
                </div>
                <div class="order-value">
                    <span><?= (int)$model->addressPrice() ?></span> <i class="fa fa-rub" aria-hidden="true"></i>
                </div>
            <?php endif; ?>

            <?php if ($model->status_view): ?>
                <div class="labels">
                    текущий статус заказа
                </div>
                <div class="order-value">
                    <span><?= $model->status->getCaption() ?></span>
                    <?php echo ($model->status->alias == 'partpaid') ? '(' . Yii::$app->formatter->asCurrency($model->balance()) . ')' : '' ?>
                </div>
            <?php endif; ?>

            <?php if ($model->created_at): ?>
                <div class="labels">
                    дата и время создания
                </div>
                <div class="order-value">
                    <span><?= Yii::$app->formatter->asDateTime($model->created_at); ?></span>


                </div>
            <?php endif; ?>

            <?php if ($model->updated_at): ?>
                <div class="labels">
                    дата последнего изменения
                </div>
                <div class="order-value">
                    <span><?= Yii::$app->formatter->asDateTime($model->updated_at); ?></span>
                </div>
            <?php endif; ?>

            <div>
                <div class="labels">
                    Реквизиты, на которые оформлен заказ
                </div>

                <ul class="list-unstyled">
                    <li>Сокращённое название организации: <?= $model->requisite_company_name; ?></li>
                    <li>ИНН: <?= $model->requisite_inn; ?></li>
                    <li>ОГРН: <?= $model->requisite_ogrn; ?></li>
                    <li>КПП: <?= $model->requisite_kpp; ?></li>
                    <li>Наименование банка: <?= $model->requisite_bank_name; ?></li>
                    <li>БИК: <?= $model->requisite_bank_id; ?></li>
                    <li>Расчётный счёт: <?= $model->requisite_bank_account; ?></li>
                    <li>Корреспондентский счёт: <?= $model->requisite_bank_correspondent_account; ?></li>
                </ul>
            </div>

            <div class="">

                <div class="labels">
                    Данные для формирвания документов
                </div>

                <?php
                $id = $model->owner->default_profile_id;
                $r = Entity::byId($id);
                if ($r) echo $r->getFooterText();
                ?>

            </div>


            <div class="labels">
                Общая стоимость заказа
            </div>

            <div class="order-value">
                <span><?= (int)$model->summa ?> <i class="fa fa-rub" aria-hidden="true"></i></span>
            </div>

        </div>
    </div>

    <?= OrderServicesWidget::widget(['services' => $model->getServices()->all(), 'guarantee'=>isset($model->guarantee)?$model->guarantee:null]) ?>


    <?= OrderWidget::widget(['id' => $model->id, 'limit' => 20]); ?>


</div>
