<?php


use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $order backend\models\Order */

$this->title = 'Заказ ' . $order->getCaption();
$this->params['breadcrumbs'][] = ['label' => \backend\models\Order::getMeta('','title'), 'url' => Url::to(['customer/order']),'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL];
$this->params['breadcrumbs'][] = $this -> title;



/*
 *
	<h1>
        Заказ адреса <?= Html::a(ArrayHelper::getValue($order->address,'address'), Url::toRoute('address/'.ArrayHelper::getValue($order->address,'id')),['target' => '_blank']) ?>
    </h1>
*/
?>


<?php

	if ($step < $wizard_count)  {
		$h1 = "Заказ адреса ".Html::a(ArrayHelper::getValue($order->address,'address'), Url::toRoute('address/'.ArrayHelper::getValue($order->address,'id')),['target' => '_blank']);
	}else {
		$h1 = "Заказ адреса ".Html::a(ArrayHelper::getValue($order->address,'address'), Url::toRoute('address/'.ArrayHelper::getValue($order->address,'id')),['target' => '_blank'])." оформлен";
	}
	$this -> context = $h1;
//	echo $h1;
            
	if ($step < $wizard_count)  {
	    
		echo $this->render('_form', [
			'order' => $order,
			'address' => $address,
			'contract' => $contract,
			'step' => $step,
			'address_id' => $address->id,
		]);		

	} else {
    
		$order -> changeStatus('new', true);
		echo $this->render('_completed', [
			'model' => $order,
		]);
	}
?>
