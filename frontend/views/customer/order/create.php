<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model backend\models\Order */

$this->title = 'Новый заказ';
//$this->params['breadcrumbs'][] = ['label' => \backend\models\Order::getMeta('','title'), 'url' => ['index'], 'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>
<div class="order-create">

    <h1>
        Заказ адреса <?= Html::a($address->address, Url::toRoute('address/'.$address->id, ['target' => '_blank'])) ?>
    </h1>

    <?php echo $this->render('_form', compact('order', 'address', 'contract', 'step')) ?>
</div>
