<?php

use common\models\Entity;
use frontend\components\OrderServicesWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

    <style>
        .order-md-6 {
            width: 100%;
        }
    </style>

<?php $form = ActiveForm::begin([
    'id' => 'order',
    //'enableAjaxValidation' => true,
    'enableClientValidation' => true

]);


?>
    <div class="popup order-address-popup order-page-three">

    <div class="">
    <div style="">

    <div class="row order-logo">
        <!--<div class="col-md-4">
            <div class="header-padding-left">
                <div class="title">
                    Заказ оформлен
                </div>
            </div>
        </div>-->
        <!--
                            <div class="col-md-6">
                                <div class="flex">
                                    <div class="popup-page">
                                        3
                                    </div>

                                </div>

                            </div>
        -->
    </div>
    <div class="row">

    <div class="col-md-6 order-md-6">

<?php

//$model -> save();
//var_dump($model -> attributes);
$calculation = $model->calculate();


$attrList = $model->getAttributes();
$attributes = [];
foreach ($attrList as $colName => $value) {
    $type = $model->getMeta($colName, 'type');
    $show = (int)$model->getMeta($colName, 'show');
    if (!$show) continue;
    $attributes[] = $colName . "_view" . ($type == 'text' ? ':ntext' : '');
}

?>


    <div class="order-view">
        <div class="row">
            <div class="col-md-9">
                <h2>   <?= $model->getCaption() ?></h2>

                <div class="get-order-address">
                    <?= $model->address_view ?>
                </div>

<?php if ($model->guarantee_id):?>
                <div class="delivery_method">

                    <div class="labels">
                        Гарантия
                    </div>
                    <div class="order-value">
                        <?php
                        if ($model->guarantee_id) {
                            echo $model->guarantee_view;
                        };
                        ?>
                    </div>
                </div>
<?php endif;?>
                <div class="requisites">
                    <div class="labels">
                        Реквизиты для оформления адреса
                    </div>
                    <ul class="list-unstyled">
                        <li>Сокращённое название организации: <?= $model->company_name; ?></li>
                        <li>Полное название организации: <?= $model->company_full_name; ?></li>
                        <li>ФИО исполнительного лица: <?= $model->execution_officer_full_name; ?></li>
                        <li>Должность исполнительного лица: <?= $model->execution_officer_envoy_ship; ?></li>
                        <li>Город регистрации исполнительного
                            лица: <?= $model->execution_officer_recording_city; ?></li>
                        <li>Адрес регистрации исполнительного
                            лица: <?= $model->execution_officer_recording_address; ?></li>
                        <?php if ($model->company_inn): ?>
                            <li>ИНН огранизации: <?= $model->company_inn; ?></li>
                        <?php endif; ?>
                        <?php if ($model->company_ogrn): ?>
                            <li>ОГРН: <?= $model->company_ogrn; ?></li>
                        <?php endif; ?>
                        <?php if ($model->company_kpp): ?>
                            <li>КПП: <?= $model->company_kpp; ?></li>
                        <?php endif; ?>
                    </ul>
                </div>

                <div class="requisites">
                    <div class="labels">
                        Комментарий
                    </div>
                    <div class="order-value">
                        <?= $model->comment; ?>
                    </div>
                </div>

                <?php if ($model->delivery_type_id_view): ?>

                    <div class="delivery_method">

                        <div class="labels">
                            Способ доставки
                        </div>
                        <div class="order-value">
                            <?= $model->delivery_type_id_view; ?>
                        </div>
                    </div>
                <?php endif; ?>


                <?php if ($model->duration): ?>
                    <div class="labels">
                        выбранный период
                    </div>

                    <div class="order-value">
                        <span><?= $model->duration ?></span>
                    </div>

                    <?php if ($model->date_start && $model->date_fin): ?>

                        <div class="order-value">
                            <span><?= $model->date_start_view ?></span> - <span><?= $model->date_fin_view ?></span>
                        </div>
                    <?php endif; ?>

                <?php endif; ?>

                <?php if ($model->addressPrice()): ?>
                    <div class="labels">
                        стоимость аренды адреса
                    </div>
                    <div class="order-value">
                        <span><?= (int)$model->addressPrice() ?></span> <i class="fa fa-rub" aria-hidden="true"></i>
                    </div>
                <?php endif; ?>

                <?php if ($model->status_view): ?>
                    <div class="labels">
                        текущий статус заказа
                    </div>
                    <div class="order-value">
                        <span><?= $model->status->getCaption() ?></span>
                        <?php echo ($model->status->alias == 'partpaid') ? '(' . Yii::$app->formatter->asCurrency($model->balance()) . ')' : '' ?>
                    </div>
                <?php endif; ?>

                <?php if ($model->created_at): ?>
                    <div class="labels">
                        дата и время создания
                    </div>
                    <div class="order-value">
                        <span><?= Yii::$app->formatter->asDateTime($model->created_at); ?></span>


                    </div>
                <?php endif; ?>

                <?php if ($model->updated_at): ?>
                    <div class="labels">
                        дата последнего изменения
                    </div>
                    <div class="order-value">
                        <span><?= Yii::$app->formatter->asDateTime($model->updated_at); ?></span>
                    </div>
                <?php endif; ?>

                <div>
                    <div class="labels">
                        Реквизиты, на которые оформлен заказ
                    </div>

                    <ul class="list-unstyled">
                        <li>Сокращённое название организации: <?= $model->requisite_company_name; ?></li>
                        <li>ИНН: <?= $model->requisite_inn; ?></li>
                        <li>ОГРН: <?= $model->requisite_ogrn; ?></li>
                        <li>КПП: <?= $model->requisite_kpp; ?></li>
                        <li>Наименование банка: <?= $model->requisite_bank_name; ?></li>
                        <li>БИК: <?= $model->requisite_bank_id; ?></li>
                        <li>Расчётный счёт: <?= $model->requisite_bank_account; ?></li>
                        <li>Корреспондентский счёт: <?= $model->requisite_bank_correspondent_account; ?></li>
                    </ul>
                </div>

                <div class="">

                    <div class="labels">
                        Данные для формирвания документов
                    </div>

                    <?php
                    $id = $model->owner->default_profile_id;
                    $r = Entity::byId($id);
                    if ($r) echo $r->getFooterText();
                    ?>

                </div>


                <div class="labels">
                    Общая стоимость заказа
                </div>

                <div class="order-value">
                    <span><?= (int)$model->summa ?> <i class="fa fa-rub" aria-hidden="true"></i></span>
                </div>


            </div>


        </div>

        <?= OrderServicesWidget::widget(['services' => $model->getServices()->all(), 'guarantee'=>isset($model->guarantee)?$model->guarantee:null]) ?>


        <?= ''//OrderWidget::widget(['id' => $model->id, 'limit' => 20]);    ?>


    </div>

    <div style="clear:both;">
    <div>
    <div class="field-row flex al">

        <div class="field-row flex al">
            <a href="<?= Url::to(["/order/update/" . $model->id . "/2"]); ?>" class="reg-button prior"
               style="float: left; margin-left: 0;">
                Изменить заказ
            </a>

            <a href="<?= Url::to(["/order/confirm/" . $model->id]); ?>" class="reg-button next"
               style="float: right; margin-left: 25%;" data-page="order-page-two">
                Подтвердить заказ
            </a>

        </div>

    </div>

<?php ActiveForm::end(); ?>