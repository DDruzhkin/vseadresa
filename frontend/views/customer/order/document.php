<?php
/**
 * Created by PhpStorm.
 * User: FS04
 * Date: 4/9/2018
 * Time: 11:33 AM
 */

use yii\bootstrap\Html;
use yii\helpers\Url;
use frontend\models\Document;
use frontend\models\Invoice;


$this->title = 'Счета';
$this->params['breadcrumbs'] =
    [

        [
            'label' => 'Личный кабинет',
            'url' => ['/customer'],
            'template' => '<span class="b-link">{link}</span><span class="separate-links">\</span>' . PHP_EOL,
        ],
        [
            'label' => 'Заказы',
            'url' => ['/customer/order'],
            'template' => '<span class="b-link">{link}</span><span class="separate-links">\</span>' . PHP_EOL,
        ],
        [
            'label' => 'Заказ №' . $model->number,
            'url' => ['/customer/order/view/' . $model->id],
            'template' => '<span class="b-link">{link}</span><span class="separate-links">\</span>' . PHP_EOL,
        ],
        [
            'label' => $this->title,
        ],

    ];
$i = 0;
?>
<h1><?= $this->title ?></h1>
<?php if ($documents): ?>
    <div class="row">
        <div class="row">
            <div class="col-md-12 table-cnt">
                <div class="table-self">
                    <table class="table price-table counts-table nom-table">
                        <thead>
                        <th></th>
                        <th><?= Document::getMeta('number', 'label') ?></th>
                        <!--                        <th><?= Document::getMeta('doc_type_id', 'label') ?></th> -->
                        <th><?= Document::getMeta('info', 'label') ?></th>
                        <th><?= Document::getMeta('summa', 'label') ?></th>
                        <th width="80px" align="right"></th>
                        </thead>
                        <tbody>
                        <?php foreach ($documents as $document):
                            ?>
                            <tr>
                                <td><?= ++$i ?></td>
                                <td><?= $document->number ?></td>
                                <!--                                <td><?= $document->doc_type_id_view ?></td> -->
                                <td><?= $document->info ?></td>
                                <td><?= Yii::$app->formatter->asCurrency($document->summa) ?> </td>
                                <td>
                                    <?= Html::a('<i class="fa fa-download""></i>', ['document/show-pdf', 'id' => $document->id], ['target' => '_blank', 'title' => 'Скачать PDF']) ?>
                                    <?php if ($document->state != Invoice::STATE_PAID): ?>
                                        <?= Html::a('<i class="fa fa-credit-card" style="margin-left: 5px;"></i> ', ['/document/pay', 'id' => $document->id], ['title' => 'Оплатить картой', 'target' => '_blank']) ?>
                                    <?php endif ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="hr">
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<div class="payment-tooltip row">
    <p>Для физического лица возможна оплата банковской картой либо оплата по квитанции в банке.</p>
    <p>Для юридического лица или ИП возможна оплата картой либо на расчетный счет.</p>
    <p>В случае оплаты по счету, вам необходимо нажать на иконку с изображением документы, где, по указанным вами
        реквизитам, будет выставлене счет.</p>
    <p> В случае оплаты банковской картой, необходимо нажать на иконку с изображением карты, произойдет переход на
        страницу эквайринга, где вы можете ввести данные карты и сделать оплату.</p>
    <p> После подтверждения поступления денежных средств для вас будут подготовлены документы для оформления
        юридического адреса и отправлены выбранным способом доставки.</p>
</div>
