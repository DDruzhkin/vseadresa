
<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;

use frontend\models\Address;
use common\models\Order;

use frontend\components\FilterWindow;


?>
<style>

#OrderFilterForm .input-daterange {
	float: left;
	margin: 0;
	width: 90%;
}

#OrderFilterForm .input-daterange .form-control{
		width: 100%;
}

#OrderFilterForm .form-control {
	float: left;
	
	margin: 0;
}

#OrderFilterForm input.form-control {
	width: 90%;
}

#OrderFilterForm #ordersearch-address_id {
	width: 90%;
}

#OrderFilterForm .hint-block{
	display: block;
	float: left;
	width: 90%;
}

</style>
<?php

$layout = <<< HTML
    
    {input1}    	
    {separator}    
    {input2}
    <span class="input-group-addon kv-date-remove">
        <i class="glyphicon glyphicon-remove"></i>
    </span>
HTML;

$controls = [
	'number' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('number', 'label'), 'hint'=>'часть номера'.Order::getMeta('number', 'comment'), 'options'=>[]],
	'address_id' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('address_id', 'label'), 'hint'=>'поиск по части заказанного адреса', 'options'=>[]],
	'status_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('status_id', 'label'), 'hint'=>'', 'items' => Order::getExistsValues('status_id', false, true)],			
	
	
	'date_start' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'kartik\widgets\DatePicker', 'label'=>'Период: ',
		'hint'=>'дата оформления заказа', 
		'options'=>[
			'layout' => $layout,
			'attribute' => 'date_start',
			'attribute2' => 'date_fin',
			'separator' => ' - ',
			'type' => kartik\widgets\DatePicker::TYPE_RANGE,
			'pluginOptions' => [
				'todayHighlight' => true, 
				'todayBtn' => true,				
				'autoclose'=>true,   			
				'endDate' => '0d',
				'format' => 'dd.mm.yyyy'
			]
		]
	],
						
	'actions'=>['type'=>Form::INPUT_RAW, 'value'=>
        '<div class="standart-btn-container">'.
		Html::submitButton('Применить фильтр', ['class'=>'reg-button', 'style'=>'float: right; margin-right: 20px;']).
		Html::button('Очистить фильтр', ['class'=>'reg-button reset', 'style'=>'float: right; margin-right: 20px;']).
        "</div>"
	],            
];


$layout = [
[	
	'columns' => 2,
	'attributes' => [		
		'number' => $controls['number'],				
		'status_id' => $controls['status_id'],										
	],
],

[	

	'columns' => 2,
	'attributes' => [		
		'date_start' => $controls['date_start'],										
	],
],


[	
	'columns' => 1,
	'attributes' => [				
		'address_id' => $controls['address_id'],										
	],
],


[
	'columns' => 1,
	'attributes' => [		
		
		'actions' => $controls['actions'],						
	]
],


];




	echo FilterWindow::widget(
			[
				'options' => [
					'id' => 'OrderFilterForm', 					
					'method' => 'get',
				],
				'model' => $searchModel,
				'layout' => $layout,				
					
			]);




/*

$form = ActiveForm::begin([
	'id' => 'OrderFilterForm', 
	'method' => 'get',
//	'action' => Url::to(['/address/prices']),
]);
echo FormGrid::widget([
    'model' => $searchModel,
    'form' => $form,
	//'autoGenerateColumns' => false,
    'rows' => $layout
	]);
	
ActiveForm::end();

*/