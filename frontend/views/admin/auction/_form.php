<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Auction;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'date' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Auction::getMeta('date', 'label'), 'hint'=>''.Auction::getMeta('date', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
						'address_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Auction::getMeta('address_id', 'label'), 'items' => Auction::getListValues('address_id'), 'hint'=>''.Auction::getMeta('address_id', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'owner_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Auction::getMeta('owner_id', 'label'), 'items' => Auction::getListValues('owner_id'), 'hint'=>''.Auction::getMeta('owner_id', 'comment'), 'options'=>[]],
						'status_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Auction::getMeta('status_id', 'label'), 'hint'=>''.Auction::getMeta('status_id', 'comment'), 'items' => $model -> getStatusList()],	
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'rate_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Auction::getMeta('rate_id', 'label'), 'items' => Auction::getListValues('rate_id'), 'hint'=>''.Auction::getMeta('rate_id', 'comment'), 'options'=>[]],
						'rate_count' => ['type'=>Form::INPUT_TEXT, 'label'=>Auction::getMeta('rate_count', 'label'), 'hint'=>''.Auction::getMeta('rate_count', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'amount' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Auction::getMeta('amount', 'label'), 'hint'=>''.Auction::getMeta('amount', 'comment'), 'options'=>['clientOptions' => ['alias' =>  'decimal', 'groupSeparator' => ' ','autoGroup' => true],]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Auction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auction-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Auction::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
