<?php
use common\models\Entity;
use frontend\models\Address;
use frontend\models\AuctionRate;
use frontend\models\Status;
use frontend\models\User;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\helpers\Url;



$this -> title = 'Аукцион '.$model -> date_view;

$this->params['breadcrumbs'] =
    [
/*
        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/owner'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
		*/
		[
            'label'     => 'Аукционы',
            'url'       =>  ['/admin/auction'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,
        ],

    ];



$finish = strtotime($model -> date) - 86400 + 17*3600;


?>
<style>
tr.owner td {
	font-weight: bold;
}

div.auction-clock {
	color:green;
}

div.auction-status {
	font-weight: bold;
}

div.auction-status-active {
	color:green;
}

div.auction-status-noactive {
	color:red;
}

div.no-auction-rates {
	color:red;
	margin-top: 10px;
	font-weight: bold;
}
</style>
<?php if ($model -> status -> alias == 'active'):?>

<script>

var deadline = '<?=date('Y-m-d H:i:00', $finish);?>';

function getTimeRemaining(endtime){
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor( (t/1000) % 60 );
  var minutes = Math.floor( (t/1000/60) % 60 );
  var hours = Math.floor( (t/(1000*60*60)) % 24 );
  var days = Math.floor( t/(1000*60*60*24) );
  return {
   'total': t,
   'days': days,
   'hours': hours,
   'minutes': minutes,
   'seconds': seconds
  };
};
function initializeClock(endtime){
  
  var timeinterval = setInterval(function(){
		showClock(endtime);
  },1000);
};

// в пределах 100
function getMetrics(val, metrics) {
	var m = {
		'день':{1:'дня', 2:'дней'},
		'час':{1:'часа', 2:'часов'},
		'минута':{1:'минуты', 2:'минут'},
		'секунда':{1:'секунды', 2:'секунд'},
	};
	
	i = 0; dval = val % 10;
	mm = m[metrics];
	if ((val >= 5 && val <= 20) || (dval >=5 && dval < 9) || (dval == 0)) {
		return mm[2];
	} else if (dval == 1){
		return metrics;
	} else {
		return mm[1];
	}
	
}

function showClock(endtime) {
	var t = getTimeRemaining(endtime);
	var clock = document.getElementById('clockdiv');
	
   clock.innerHTML = t.days + ' ' + getMetrics(t.days, 'день') + ' '
+	t.hours + ' ' + getMetrics(t.hours, 'час') + ' '
+	t.minutes + ' ' + getMetrics(t.minutes, 'минута') + ' '
+	t.seconds + ' ' + getMetrics(t.seconds, 'секунда');
	if (t.seconds == 0) { // раз в минуту обновляем страницу
		document.location.href=document.location.href;
	}
	
   if(t.total<=0){
    clearInterval(timeinterval);
   }	
}

$(document).ready(function() {
	showClock(deadline);
	initializeClock(deadline);	
});



</script>

<?php endif?>

<h1>Аукцион <?=Yii::$app -> formatter -> asDate($model -> date) ?></h1>
<div class="auction-status auction-status-<?=$model -> status -> alias?>"><?=$model -> status -> name ?></div>
<?php 
	if ($model -> status -> alias == 'active') {
		?>
		<div class="auction-clock">До завершения осталось: <span id="clockdiv"></span></div>
		<?php
	};

	
if ($model -> rate_count == 0)	 {
	echo "<div class='no-auction-rates'>Нет ставок</div>";
} else {
	$auctionAddresses = $model -> getAuctionAddresses(); // здесь список адресов, участвующих в аукционе с характеристиками
	?>

	<table class="table price-table counts-table">
			<thead>
				<tr>
					<th>Место</th>				
					<th>Адрес</th>
					<th>Владелец адреса</th>
					<th>Число ставок</th>
					<th>Cтавка</th>				
				</tr>
			</thead>

			<?php		
			///echo "<pre>",print_r($notifications), "</pre>";
			foreach($auctionAddresses as $addressId => $data) {
				//$rate = AuctionRate::findOne([$addressId]);
				//var_dump($data); exit;
				$address = Address::findOne([$addressId]);
				$owner = User::findOne([$data['owner_id']]);
				
				if (is_null($address)) continue;
				
				$rate = $model -> getAddressRates($addressId, 1)[0];		
				
				
				
				echo  '
				<tr'.($address -> owner_id == Entity::currentUser()?' class="owner"':'').'>
				<td>'.($rate?$auctionAddresses[$addressId]['position']:'-').'</td>
				<td>'.
				Html::a($address -> address, Url::to(['/address/'.$address -> id]), ['class' => 'move-to-address', 'title' => 'Перейти на карточку адреса'])
				.'</td>
				<td>'.
				Html::a($owner -> fullname, Url::to(['user/view/'.$owner -> id]), ['class' => 'move-to-address'])
				.'</td>		
				<td>'.
				$data['c']
				.'</td>
				<td>'.($rate?Yii::$app -> formatter -> asCurrency($auctionAddresses[$addressId]['s']):'0').'</td>
				

				</tr>
				';	
			}
			?>
			
			</table>			
		</div>

	<?php
};