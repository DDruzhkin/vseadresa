<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Porder */

$this->title = 'Добавление Заказа';
$this->params['breadcrumbs'][] = ['label' => \backend\models\Porder::getMeta('','title'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="porder-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
