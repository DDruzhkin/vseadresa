<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Porder;
use common\models\Entity;
use common\models\PorderSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Заказы портала';

$this->params['breadcrumbs'][] = $this->title;
$this->context -> h1 = $this->title;
?>
<div class="porder-index">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,



		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => $data -> getAdminMenuItems(),
							],
						],
					]);
				}					
			],	


            ['attribute' => 'id',
				'label' => Porder::getMeta('id', 'label'),
				'contentOptions' => ['class' => 'col-id '],
				'filterOptions' => ['class' => 'col-id'],
				'headerOptions' => ['class' => 'col-id '],				
				'footerOptions' => ['class' => 'col-id '],
			],
/*			
            ['attribute' => 'number',
				'label' => Porder::getMeta('number', 'label'),
				'contentOptions' => ['class' => 'col-number '],
				'filterOptions' => ['class' => 'col-number'],
				'headerOptions' => ['class' => 'col-number '],				
				'footerOptions' => ['class' => 'col-number '],
				
				
				
			],
*/			
            ['attribute' => 'created_at',
				'label' => Porder::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'service_name',
				'label' => Porder::getMeta('service_name', 'label'),
				'contentOptions' => ['class' => 'col-service_name '],
				'filterOptions' => ['class' => 'col-service_name'],
				'headerOptions' => ['class' => 'col-service_name '],				
				'footerOptions' => ['class' => 'col-service_name '],
				'value' => 
					function($data)
					{ 
						$value = Html::a($data -> service_name,['/admin/'.Inflector::camel2id('ServicePortal').'/update/', 'id'=>$data -> service_id], ['title' => 'Системная услуга: '.$data -> service_view]);
								
						return $value;
					},
				'format' => 'raw',
				
				
			],
            ['attribute' => 'info',
				'label' => Porder::getMeta('info', 'label'),
				'contentOptions' => ['class' => 'col-info hidden '],
				'filterOptions' => ['class' => 'col-info hidden'],
				'headerOptions' => ['class' => 'col-info hidden '],				
				'footerOptions' => ['class' => 'col-info hidden '],
				
				
				
			],
/*			
            ['attribute' => 'service_id',
				'label' => Porder::getMeta('service_id', 'label'),
				'contentOptions' => ['class' => 'col-service_id '],
				'filterOptions' => ['class' => 'col-service_id'],
				'headerOptions' => ['class' => 'col-service_id '],				
				'footerOptions' => ['class' => 'col-service_id '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['service_id_view'],[Inflector::camel2id('ServicePortal').'/update/'.$data['service_id']]);
				return $value;
			},
				'format' => 'raw',
			],
			
			
*/
/*			
            ['attribute' => 'customer_id',
				'label' => Porder::getMeta('customer_id', 'label'),
				'contentOptions' => ['class' => 'col-customer_id '],
				'filterOptions' => ['class' => 'col-customer_id'],
				'headerOptions' => ['class' => 'col-customer_id '],				
				'footerOptions' => ['class' => 'col-customer_id '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data -> customer -> fullname,[Inflector::camel2id('User').'/update/'.$data['customer_id']]);
				return $value;
			},
				'format' => 'raw',
			],
*/			
			
            ['attribute' => 'customer_r_id',
				'label' => 'Контрагент',
				'contentOptions' => ['class' => 'col-customer_r_id  '],
				'filterOptions' => ['class' => 'col-customer_r_id '],
				'headerOptions' => ['class' => 'col-customer_r_id  '],				
				'footerOptions' => ['class' => 'col-customer_r_id '],
				
				'value' => 
			function($data)
			{ 	
				//return $data -> customer_r -> title;				
				return Html::a($data -> customer_r -> title, Url::to(['/admin/user/view', 'id' => $data -> customer_id]), ['title' => 'Перейти на карточку пользователя']);
			},
				'format' => 'raw',
			],
            ['attribute' => 'recipient_id',
				'label' => Porder::getMeta('recipient_id', 'label'),
				'contentOptions' => ['class' => 'col-recipient_id hidden '],
				'filterOptions' => ['class' => 'col-recipient_id hidden'],
				'headerOptions' => ['class' => 'col-recipient_id hidden '],				
				'footerOptions' => ['class' => 'col-recipient_id hidden '],
				
				'value' => function($data){ return $data -> recipient -> title;},
				
			],
            ['attribute' => 'summa',
				'label' => Porder::getMeta('summa', 'label'),
				'contentOptions' => ['class' => 'col-summa '],
				'filterOptions' => ['class' => 'col-summa'],
				'headerOptions' => ['class' => 'col-summa '],				
				'footerOptions' => ['class' => 'col-summa '],
				'value' => function($data)  {
					return Yii::$app -> formatter -> asCurrency($data -> summa);
				}
				
				
			],
            ['attribute' => 'invoice_id',
				'label' => Porder::getMeta('invoice_id', 'label'),
				'contentOptions' => ['class' => 'col-invoice_id hidden '],
				'filterOptions' => ['class' => 'col-invoice_id hidden'],
				'headerOptions' => ['class' => 'col-invoice_id hidden '],				
				'footerOptions' => ['class' => 'col-invoice_id hidden '],
				
				
				
			],

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
