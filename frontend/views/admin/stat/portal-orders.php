<?php

use common\models\EntityHelper;
use frontend\models\Address;
use frontend\models\User;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$user = false;
$address = false;
if ($user_id) {
	$user = User::findone($user_id);
};
if ($address_id){
	$address = Address::findone([$address_id]);
}


$title = 'Статистика по заказам порталу';

if ($user_id || $address_id) {
$this->params['breadcrumbs'][] =	
			[
				'label'     => $title,
                'url'       =>  ['/admin/stat/portal-orders'],
                'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
			];
			
if ($user) {
	$this->params['breadcrumbs'][] = $user -> fullname;
};
			
if ($address) {
	$this->params['breadcrumbs'][] = $address -> address;
};
			
} else {
$this->params['breadcrumbs'][] = $this->title;	
}

$this->title = $title;
$this->context -> h1 = $this->title;

?>

<div class="stat-order">

	<div class="content">
	Статистический отчет по заказам Порталу 
<?php	
			
			if ($user) {
				
				$role = 'Пользователя';
					if ($user -> checkRole(User::ROLE_OWNER)) $role = 'Владельца';
					if ($user -> checkRole(User::ROLE_CUSTOMER))$role = 'Закачика';
					
				if($user) {
					echo ' от '.$role.' '.Html::a($user -> fullname, ['admin/user/view', 'id' => $user -> id]);
				};
				
			};
		
			if ($address){
				
				if($address) {
					echo ' по Адресу '.Html::a($address -> address, ['admin/address/view', 'id' => $address -> id]);
				};
			}
		
		
?>.
	</p>
	<p>
	
	<form method="get">
	<label>
	Период:
	<select name="period" id="period-selector"  onchange="this.form.submit()">
		<option value="year"<?=($period == 'year')?' selected="1"':''?>>за год</option>
		<option value="month"<?=($period == 'month')?' selected="1"':''?>>за месяц</option>
		<option value="day"<?=($period == 'day')?' selected="1"':''?>>за день</option>
	</select>
	</label>
	<input type="hidden" name="user_id" value="<?=$user_id?>">
	<input type="hidden" name="address_id" value="<?=$address_id?>">
	<form>
	</p>
	</div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
		'dataProvider'=>$dataProvider,       

		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],
		
	]);
	?>
		
		
		
</div>		