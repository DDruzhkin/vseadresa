<?php




/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Region;
use common\models\RegionSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Районы Москвы';
$this->params['breadcrumbs'][] = $this->title;
$this -> context -> h1 = $this->title;
?>
<div class="region-index">
    
	<div class="content">

	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="controls">
       <?= Html::a('Добавить', ['create'], ['class' => 'reg-button']) ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,

		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],

        'filterModel' => $searchModel,
        'columns' => [

			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => $data -> getAdminMenuItems(),
							],
						],
					]);
				}					
			],
		
            ['attribute' => 'name',
				'label' => Region::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name'],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name'],				
				'footerOptions' => ['class' => 'col-name'],
				'value' => function ($data) {					
					return Html::a($data -> name, ['/admin/okrug/update/', 'id' => $data -> id], ['title' => 'Изменить элемент']);
				},
				'format' => 'raw'					
				
				
			],
            ['attribute' => 'okrug_id',
				'label' => Region::getMeta('okrug_id', 'label'),
				'contentOptions' => ['class' => 'col-okrug_id'],
				'filterOptions' => ['class' => 'col-okrug_id'],
				'headerOptions' => ['class' => 'col-okrug_id'],				
				'footerOptions' => ['class' => 'col-okrug_id'],
				'filter' => Region::getExistsValues('okrug_id'),	
				'value' => function($data){ return $data['okrug_id_view'];},
				
			],

            [
                'class' => 'yii\grid\ActionColumn',

                'buttons' => [
                    'delete' => function ($url, $model) {

                        return ($model->deletable) ?           


                            Html::a(
                                Icon::show('times'),
                                Url::toRoute(['delete', 'id' => $model->id]),
                                
                                [
                                    'title' => 'Удалить элемент',
                                    'class' => 'delete',
                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                    'data-params' => ['id' => $model->id],

                                ]
                            ) : '';

                    }
                ],
                'template' => '{delete}',


            ],
        ],
    ]); ?>
</div>
