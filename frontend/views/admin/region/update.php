<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Region */

$this->title = 'Редактирование района ' . $model->getCaption();
$this->params['breadcrumbs'][] = [
	'label' => \frontend\models\Okrug::getMeta('','title'), 
	'url' => ['index'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];
$this->params['breadcrumbs'][] = $this -> title;
$this->context -> h1 = $this -> title;
?>
<div class="region-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
