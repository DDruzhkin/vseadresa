<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\PhotoCalendar;
use common\models\PhotoCalendarSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\grid\GridView;
//use kartik\export\ExportMenu;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Список визитов фотографа';
$this->params['breadcrumbs'][] = $this->title;
$this -> context -> h1 = $this->title;
?>
<div class="photo-calendar-index">

    
	<div class="content">
	
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<!--
    <p>
        <?= Html::a('Добавить ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
-->	


	<div class="talbe-border-add">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'exportConfig' => [	
			GridView::EXCEL => [
			'label' => 'Excel',
			'icon' => 'floppy-remove',
			],
		],
		'layout' => '<div class="pull-right">{export}</div>{summary}<br><br>{items}',


		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],

//        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => $data -> getAdminMenuItems(),
							],
						],
					]);
				}
			],


            ['attribute' => 'address_id',
				//'label' => PhotoCalendar::getMeta('address_id', 'label'),
				'label' => 'Заказанный адрес',
				'contentOptions' => ['class' => 'col-address_id '],
				'filterOptions' => ['class' => 'col-address_id'],
				'headerOptions' => ['class' => 'col-address_id '],
				'footerOptions' => ['class' => 'col-address_id '],

				'value' => function($data){ 
					$address = $data -> address;
					$paid = $address -> photo_order?$address -> photo_order -> isPaied:false;
					if ($address) $value = Html::a($data -> address -> getCaption(), Url::to(['/admin/address/view', 'id' => $data -> address_id]), ['title' => 'Перейти на карточку Адреса']);
					else $value = '';
					return Icon::show($paid?'money':'hand-lizard-o').' '.$value;
				},
				'format' => 'raw',

			],
			
		
			
            ['attribute' => 'user_id',
				'label' => 'Заказчик',
				'contentOptions' => ['class' => 'col-user_id '],
				'filterOptions' => ['class' => 'col-user_id'],
				'headerOptions' => ['class' => 'col-user_id '],
				'footerOptions' => ['class' => 'col-user_id '],

				'value' => function($data){ 
					return Html::a($data -> user -> getCaption("%fullname%<br>%email%<br>%phone%"), Url::to(['/admin/user/view', 'id' => $data -> user_id]), ['title' => 'Перейти на карточку Заказчика']);
				},
				'format' => 'raw',

			],
            ['attribute' => 'date',
				'label' => PhotoCalendar::getMeta('date', 'label'),
				'contentOptions' => ['class' => 'col-date date'],
				'filterOptions' => ['class' => 'col-date'],
				'headerOptions' => ['class' => 'col-date date'],
				'footerOptions' => ['class' => 'col-date date'],
				'value' => function($data)
                    {
						//$interval = ArrayHelper::getValue(\frontend\models\PhotoCalendar::getInterval(),$data->interval);
						//if ($interval > '') $interval = '['.$interval.']';
                        //return $data -> date_view.' '.$interval;
						return $data -> date_view;
                    },


			],
			
			
            ['attribute' => 'interval',
				'label' => PhotoCalendar::getMeta('interval', 'label'),
				'contentOptions' => ['class' => 'col-interval '],
				'filterOptions' => ['class' => 'col-interval'],
				'headerOptions' => ['class' => 'col-interval '],
				'footerOptions' => ['class' => 'col-interval '],
                'value' => function($data)
                    {
                        return ArrayHelper::getValue(\frontend\models\PhotoCalendar::getIntervals(),$data->interval);
                    },


			],

			
			
/*
            ['class' => 'yii\grid\ActionColumn'],
		*/
        ],
    ]); ?>
	</div>
</div>
