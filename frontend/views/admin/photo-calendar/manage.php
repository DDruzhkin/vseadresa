<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PhotoCalendar */

$this->title = 'Управление расписанием фотографа';

$this->params['breadcrumbs'][] = [
	'label' => \backend\models\PhotoCalendar::getMeta('','title'), 
	'url' => ['index'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];

$this->params['breadcrumbs'][] = $this -> title;
$this -> context -> h1 = $this -> title;
?>
<div class="photo-calendar-manage">

<?=\common\widgets\photocalendar\PhotoCalendar::widget(['curMonth' => $curMonth])?>

</div>
