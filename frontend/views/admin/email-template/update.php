<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Faq */

$this->title = 'Редактирование  шаблона '.$model->name;
$this -> context -> h1 = $this->title;

$this->params['breadcrumbs'][] = [
	'label' => 'Настройка уведомлений', 
	'url' => ['/admin/email-template/'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];
$this->params['breadcrumbs'][] = $this -> title;

?>
<div class="email-template-update">
    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
