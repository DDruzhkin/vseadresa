<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


use yii\grid\GridView;
use yii\helpers\Html;
use kartik\icons\Icon;
use common\models\EntityHelper;

$this->title = 'Настройка уведомлений';
$this->params['breadcrumbs'][] = $this->title;
$this -> context -> h1 = $this->title;


?>
<div class="email-index">

<!--
        <?= Html::a('Добавить шаблон', ['create'], ['class' => 'reg-button']) ?>
-->		
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		
		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],
        'columns' => [
		
		
		
		
			[
                'header' => Yii::t('app','Category'),
                'attribute' => 'category',
				'filter' => $searchModel -> getValueList('category'),

            ],
			[
                'header' => Yii::t('app','Role'),
                'attribute' => 'role',
//				'filter' => $searchModel -> getValueList('role'),

            ],
            [
                'header' => Yii::t('app','Title'),
                'attribute' => 'title',
				'value' => function($data) {
					return $data['title'];
				}

            ],
		
            [
                'header' => Yii::t('app','Name'),
                'attribute' => 'name',

            ],
/*
            [
                'header' => Yii::t('app','Type'),
                'attribute' => 'type',

            ],
*/


            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => [],
                'header' => '',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a(Icon::show('edit'), ['update', 'name' => $model['name']],
                            ['title' => Yii::t('app', 'Update')]);
                    },
                    'delete' => function ($url, $model) {
               
                            return Html::a(Icon::show('times'), ['delete', 'name' => $model['name']],
                                [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data' => [
                                        'confirm' => 'Вы уверены, что хотите удалить элемент?',
                                        'method' => 'post',
                                    ],
                                ]
                            );
                        
                    },
                ]
            ],
			
        ],
    ]); ?>
</div>
