<?php
if (YII_DEBUG) $startTime = microtime(true);

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model backend\models\Document */
/* @var $form yii\widgets\ActiveForm */
?>

<script src="https://cdn.ckeditor.com/4.9.1/full-all/ckeditor.js"></script>

<style>
.field-emailtemplate-content label {
	display: none;
}


</style>


<div class="email-template-form">


    <?php $form = ActiveForm::begin(['id' => 'email-template-update-form']); ?>

	<?= $form->field($model, 'category', ['options' => ['class' => 'clearfix']])->textInput() ?>
	<?= $form->field($model, 'role', ['options' => ['class' => 'clearfix']])->dropDownList(['Администратор' => 'Администратор','Владелец'=>'Владелец', 'Заказчик'=>'Заказчик','Пользователь' => 'Пользователь']) ?>
	<?= $form->field($model, 'title', ['options' => ['class' => 'clearfix']])->textInput() ?>
    <?= $form->field($model, 'name', ['options' => ['class' => 'clearfix']])->textInput() ?>

    <?= $form->field($model, 'template', ['options' => ['class' => 'clearfix']])->dropDownList(ArrayHelper::map($model->viewFileList, 'name', 'name'), array(
		
	
        /*'onchange' => '$.post("' . Url::to(['/email-template/put-template-into-editor']) . '",{ name: $(this).val() },
                  function(data) {
                         CKEDITOR.instances["emailtemplate-content"].setData(data);
                  }
           );',
        'csrf' => true,*/
    )) ?>

    <?= $form->field($model, 'content')->textarea(['autofocus' => true, ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'save'), ['class' => 'reg-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>

<script>
    $('.default-template').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: '<?= Url::to(['/email-template/put-default-template-into-editor']) ?>',
            type: 'post',
            data: {name: $(this).attr('data-name')},
            success: function (data) {
                var obj = JSON.parse(data);
                $("#emailtemplate-name").val(obj.name);
                $("#emailtemplate-template").val(obj.view);
                CKEDITOR.instances["emailtemplate-content"].setData(obj.content);
            }
        });
    });

    CKEDITOR.replace('EmailTemplate[content]');

</script>