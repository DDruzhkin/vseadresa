<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\models\ServiceName;
use common\models\ServiceNameSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Справочник услуг Владельцев';
$this->params['breadcrumbs'][] = $this->title;
$this->context -> h1 = $this->title;
?>
<div class="service-name-index">


	<div class="content">

	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="controls">
       <?= Html::a('Добавить', ['create'], ['class' => 'reg-button']) ?>
    </div>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,

		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',								
								'items' => [
									[
										'label' => 'Изменить',
										'url' => Url::to(['/admin/service-name/update', 'id' => $data -> id]),
									],
/*									
									[
										'label' => 'Удалить',
										'url' => Url::to(['/admin/service-name/delete', 'id' => $data -> id]),
									]
*/									
								]
								,							
							],
						],
					]);
				}					
			],


            ['attribute' => 'id',
				'label' => ServiceName::getMeta('id', 'label'),
				'contentOptions' => ['class' => 'col-id '],
				'filterOptions' => ['class' => 'col-id'],
				'headerOptions' => ['class' => 'col-id '],				
				'footerOptions' => ['class' => 'col-id '],

				
			],
			
            ['attribute' => 'code',
				'label' => ServiceName::getMeta('code', 'label'),
				'contentOptions' => ['class' => 'col-code '],
				'filterOptions' => ['class' => 'col-code'],
				'headerOptions' => ['class' => 'col-code '],				
				'footerOptions' => ['class' => 'col-code '],
				
				
				
			],
            ['attribute' => 'name',
				'label' => ServiceName::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				'value' => function ($data) {					
					return Html::a($data -> name, ['/admin/service-name/update/', 'id' => $data -> id], ['title' => 'Изменить элемент']);
				},
				'format' => 'raw'	
				
				
			],
            ['attribute' => 'stype',
				'label' => ServiceName::getMeta('stype', 'label'),
				'contentOptions' => ['class' => 'col-stype '],
				'filterOptions' => ['class' => 'col-stype'],
				'headerOptions' => ['class' => 'col-stype '],				
				'footerOptions' => ['class' => 'col-stype '],
				'filter' => ServiceName::getList('stype'),	
				
				
			],
/*			
            ['attribute' => 'to_address',
				'label' => ServiceName::getMeta('to_address', 'label'),
				'contentOptions' => ['class' => 'col-to_address hidden '],
				'filterOptions' => ['class' => 'col-to_address hidden'],
				'headerOptions' => ['class' => 'col-to_address hidden '],				
				'footerOptions' => ['class' => 'col-to_address hidden '],
				
				
				
			],
            ['attribute' => 'description',
				'label' => ServiceName::getMeta('description', 'label'),
				'contentOptions' => ['class' => 'col-description hidden '],
				'filterOptions' => ['class' => 'col-description hidden'],
				'headerOptions' => ['class' => 'col-description hidden '],				
				'footerOptions' => ['class' => 'col-description hidden '],
				
				
				
			],
            ['attribute' => 'option_id',
				'label' => ServiceName::getMeta('option_id', 'label'),
				'contentOptions' => ['class' => 'col-option_id '],
				'filterOptions' => ['class' => 'col-option_id'],
				'headerOptions' => ['class' => 'col-option_id '],				
				'footerOptions' => ['class' => 'col-option_id '],
				
				'value' => function($data){ return $data['option_id_view'];},
				
			],
            ['attribute' => 'undeleted',
				'label' => ServiceName::getMeta('undeleted', 'label'),
				'contentOptions' => ['class' => 'col-undeleted hidden '],
				'filterOptions' => ['class' => 'col-undeleted hidden'],
				'headerOptions' => ['class' => 'col-undeleted hidden '],				
				'footerOptions' => ['class' => 'col-undeleted hidden '],
				
				
				
			],
            ['attribute' => 'alias',
				'label' => ServiceName::getMeta('alias', 'label'),
				'contentOptions' => ['class' => 'col-alias hidden '],
				'filterOptions' => ['class' => 'col-alias hidden'],
				'headerOptions' => ['class' => 'col-alias hidden '],				
				'footerOptions' => ['class' => 'col-alias hidden '],
				
				
				
			],
*/			
               [
                'class' => 'yii\grid\ActionColumn',

                'buttons' => [
                    'delete' => function ($url, $model) {

                        return ($model->deletable) ?           


                            Html::a(
                                Icon::show('times'),
                                Url::toRoute(['delete', 'id' => $model->id]),
                                
                                [
                                    'title' => 'Удалить элемент',
                                    'class' => 'delete',
                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                    'data-params' => ['id' => $model->id],

                                ]
                            ) : '';

                    }
                ],
                'template' => '{delete}',


            ],
        

        ],
    ]); ?>
</div>
