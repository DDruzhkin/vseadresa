<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\ServiceName;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$addition = is_null($model) || is_null($model -> id);
$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'code' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>ServiceName::getMeta('code', 'label'), 'hint'=>''.ServiceName::getMeta('code', 'comment'), 'options'=>['mask' => '99',]],
						'name' => ['type'=>Form::INPUT_TEXT, 'label'=>ServiceName::getMeta('name', 'label'), 'hint'=>''.ServiceName::getMeta('name', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],

		
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
						'stype' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>ServiceName::getMeta('stype', 'label'), 'items' => ServiceName::getList('stype'), 'hint'=>''.ServiceName::getMeta('stype', 'comment'), 'options'=>[]],		
						'alias' => ['type'=>Form::INPUT_TEXT, 'label'=>ServiceName::getMeta('alias_view', 'label'), 'hint'=>''.ServiceName::getMeta('alias_view', 'comment'), 'options'=>['readonly'=>!$addition ]],
		
		
				],
			],				
		],				
	],
	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						
						'undeleted' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>ServiceName::getMeta('undeleted_view', 'label'), 'hint'=>''.ServiceName::getMeta('undeleted_view', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],'readonly'=>'true', ]],						
						'to_address' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>ServiceName::getMeta('to_address', 'label'), 'hint'=>''.ServiceName::getMeta('to_address', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],]],
		
				],
			],				
		],				
	],



	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'description' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>ServiceName::getMeta('description', 'label'), 'hint'=>''.ServiceName::getMeta('description', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	



	
	

		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'public-address-button']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\ServiceName */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-name-form">


<?php


$form = ActiveForm::begin([
	'enableAjaxValidation' => true,
	'enableClientValidation'=>true
]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
