<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ServiceName */

$this->title = 'Добавление услуги';

$this -> context -> h1 = $this->title;
$this->params['breadcrumbs'][] = [
	'label' => \frontend\models\ServiceName::getMeta('','title'), 
	'url' => ['index'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];
$this->params['breadcrumbs'][] = $this -> title;
?>
<div class="service-name-create">
    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
