<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Rcompany */

$this->title = 'Добавление Реквизитов Юридичекого лица';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('@frontend/views/rcompany/_form', [
    'model' => $model,
    'admin' => 1
]) ?>

	
	    