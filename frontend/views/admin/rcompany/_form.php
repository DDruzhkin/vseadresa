<?php
if (YII_DEBUG) $startTime = microtime(true);

use backend\models\Rcompany;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;


$controls = [
    'created_at_view' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('created_at_view', 'label'), 'hint' => '' . Rcompany::getMeta('created_at_view', 'comment'), 'options' => ['readonly' => 'true',]],
    'inn' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rcompany::getMeta('inn', 'label'), 'hint' => '' . Rcompany::getMeta('inn', 'comment'), 'options' => ['mask' => 9999999999,]],
    'kpp' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rcompany::getMeta('kpp', 'label'), 'hint' => '' . Rcompany::getMeta('kpp', 'comment'), 'options' => ['mask' => 999999999,]],
    'ogrn' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rcompany::getMeta('ogrn', 'label'), 'hint' => '' . Rcompany::getMeta('ogrn', 'comment'), 'options' => ['mask' => 9999999999999,]],

    'name' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('name', 'label'), 'hint' => '' . Rcompany::getMeta('name', 'comment'), 'options' => []],
    'full_name' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('full_name', 'label'), 'hint' => '' . Rcompany::getMeta('full_name', 'comment'), 'options' => []],

    'address' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('address', 'label'), 'hint' => '' . Rcompany::getMeta('address', 'comment'), 'options' => []],
    'bank_bik' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rcompany::getMeta('bank_bik', 'label'), 'hint' => '' . Rcompany::getMeta('bank_bik', 'comment'), 'options' => ['mask' => 999999999,]],
    'account' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rcompany::getMeta('account', 'label'), 'hint' => '' . Rcompany::getMeta('account', 'comment'), 'options' => ['mask' => Rcompany::getMaskByAlias("account"),]],
    'account_owner' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('account_owner', 'label'), 'hint' => '' . Rcompany::getMeta('account_owner', 'comment'), 'options' => []],

    'dogovor_osnovanie' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('dogovor_osnovanie', 'label'), 'hint' => '' . Rcompany::getMeta('dogovor_osnovanie', 'comment'), 'options' => []],
    'dogovor_signature' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('dogovor_signature', 'label'), 'hint' => '' . Rcompany::getMeta('dogovor_signature', 'comment'), 'options' => []],
    'dogovor_face' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('dogovor_face', 'label'), 'hint' => '' . Rcompany::getMeta('dogovor_face', 'comment'), 'options' => []],
    'bank_name' => ['type' => Form::INPUT_TEXT, 'label' => Rcompany::getMeta('bank_name', 'label'), 'hint' => '' . Rcompany::getMeta('bank_name', 'comment'), 'options' => []],
    'bank_cor' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rcompany::getMeta('bank_cor', 'label'), 'hint' => '' . Rcompany::getMeta('bank_cor', 'comment'), 'options' => ['mask' => '99999999999999999999',]],
    'tax_system_vat' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\checkbox\CheckboxX', 'label' => Rcompany::getMeta('tax_system_vat', 'label'), 'hint' => '' . Rcompany::getMeta('tax_system_vat', 'comment'), 'options' => ['pluginOptions' => ['threeState' => false],]],
    'sign' => ['type' => Form::INPUT_FILE, 'label' => Rip::getMeta('sign', 'label'), 'hint' => '' . Rip::getMeta('sign', 'comment'), 'options' => ['accept' => "image/png"]],
    'stamp' => ['type' => Form::INPUT_FILE, 'label' => Rip::getMeta('stamp', 'label'), 'hint' => '' . Rip::getMeta('stamp', 'comment'), 'options' => ['accept' => "image/png"]],
    'sign_sample' => ['type' => Form::INPUT_RAW, 'value' => '<div><img class="img_sample" ' . (isset($model->sign) ? 'src="/files/images/requisites/' . $model->sign . '"' : '') . '  id="sign_sample"></div>'],
    'stamp_sample' => ['type' => Form::INPUT_RAW, 'value' => '<div><img class="img_sample" ' . (isset($model->stamp) ? 'src="/files/images/requisites/' . $model->stamp . '"' : '') . ' id="stamp_sample"></div>'],

    'actions' => ['type' => Form::INPUT_RAW, 'value' => '<div style="text-align: right; margin-top: 20px">' . Html::submitButton('Изменить данные', ['class' => 'reg-button']) . '</div>'],
];

$layout = [

    [
        'attributes' => [
            'header' => ['type' => Form::INPUT_RAW, 'value' => '<h2>Реквизиты юридического лица</h2>'],
        ]
    ], [
        'attributes' => [
            'name' => $controls['name'],
        ],
    ], [
        'attributes' => [
            'full_name' => $controls['full_name'],
        ],
    ], [
        'attributes' => [
            'inn' => $controls['inn'],
        ],
    ], [
        'attributes' => [
            'kpp' => $controls['kpp'],
        ],
    ], [
        'attributes' => [
            'ogrn' => $controls['ogrn'],
        ],
    ], [
        'attributes' => [
            'address' => $controls['address'],
        ],
    ], [
        'attributes' => [
            'dogovor_signature' => $controls['dogovor_signature'],
        ],
    ], [
        'attributes' => [
            'dogovor_face' => $controls['dogovor_face'],
        ],
    ], [
        'attributes' => [
            'dogovor_osnovanie' => $controls['dogovor_osnovanie'],
        ],
    ], [
        'attributes' => [
            'header' => ['type' => Form::INPUT_RAW, 'value' => '<h2>Реквизиты банковского счета</h2>'],
        ]
    ], [
        'attributes' => [
            'bank_bik' => $controls['bank_bik'],
        ],
    ], [
        'attributes' => [
            'bank_name' => $controls['bank_name'],
        ],
    ], [
        'attributes' => [
            'bank_cor' => $controls['bank_cor'],
        ],
    ], [
        'attributes' => [
            'account' => $controls['account'],
        ],
    ], [
        'attributes' => [
            'account_owner' => $controls['account_owner'],
        ],
    ], [
        'attributes' => [
            'header' => ['type' => Form::INPUT_RAW, 'value' => '<h2>Система налогообложения</h2>'],
        ]
    ], [
        'attributes' => [
            'tax_system_vat' => $controls['tax_system_vat'],
        ],
    ], [
        'attributes' => [
            'sign' => $controls['sign'],
            'stamp' => $controls['stamp'],
        ],
    ], [
        'attributes' => [
            'sign' => $controls['sign_sample'],
            'stamp' => $controls['stamp_sample'],
        ],
    ], [
        'attributes' => [
            'actions' => $controls['actions'],
        ],
    ],

];


/* @var $this yii\web\View */
/* @var $model backend\models\Rcompany */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$form = ActiveForm::begin();
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $layout
]);
ActiveForm::end();


if (YII_DEBUG) {
    $finTime = microtime(true);
    $delta = $finTime - $startTime;
    echo $delta . ' сек.';
}
?>

</div>
<script>
    $(document).ready(function () {
        function readURL(input, type) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#' + type + '_sample').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#rip-sign").change(function () {
            readURL(this, 'sign');
        });
        $("#rip-stamp").change(function () {
            readURL(this, 'stamp');
        });
    })
</script>
