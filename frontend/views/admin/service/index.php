<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\models\Service;
use common\models\Entity;
use common\models\ServiceSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Заказанные услуги';
$this->params['breadcrumbs'][] = $this->title;
$this->context -> h1 = $this->title;
?>
<div class="service-index">
    
	<div class="content">
	<?= Service::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<!--
    <p>
        <?= Html::a('Добавить ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
-->	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Service::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'code',
				'label' => Service::getMeta('code', 'label'),
				'contentOptions' => ['class' => 'col-code '],
				'filterOptions' => ['class' => 'col-code'],
				'headerOptions' => ['class' => 'col-code '],				
				'footerOptions' => ['class' => 'col-code '],
				
				
				
			],
            ['attribute' => 'created_at',
				'label' => Service::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'service_price_id',
				'label' => Service::getMeta('service_price_id', 'label'),
				'contentOptions' => ['class' => 'col-service_price_id '],
				'filterOptions' => ['class' => 'col-service_price_id'],
				'headerOptions' => ['class' => 'col-service_price_id '],				
				'footerOptions' => ['class' => 'col-service_price_id '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['service_price_id_view'],[Inflector::camel2id('ServicePrice').'/update/'.$data['service_price_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'order_id',
				'label' => Service::getMeta('order_id', 'label'),
				'contentOptions' => ['class' => 'col-order_id '],
				'filterOptions' => ['class' => 'col-order_id'],
				'headerOptions' => ['class' => 'col-order_id '],				
				'footerOptions' => ['class' => 'col-order_id '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['order_id_view'],[Inflector::camel2id('Order').'/update/'.$data['order_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'date_start',
				'label' => Service::getMeta('date_start', 'label'),
				'contentOptions' => ['class' => 'col-date_start date'],
				'filterOptions' => ['class' => 'col-date_start'],
				'headerOptions' => ['class' => 'col-date_start date'],				
				'footerOptions' => ['class' => 'col-date_start date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'date_start',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'date_fin',
				'label' => Service::getMeta('date_fin', 'label'),
				'contentOptions' => ['class' => 'col-date_fin date'],
				'filterOptions' => ['class' => 'col-date_fin'],
				'headerOptions' => ['class' => 'col-date_fin date'],				
				'footerOptions' => ['class' => 'col-date_fin date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'date_fin',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'address_id',
				'label' => Service::getMeta('address_id', 'label'),
				'contentOptions' => ['class' => 'col-address_id '],
				'filterOptions' => ['class' => 'col-address_id'],
				'headerOptions' => ['class' => 'col-address_id '],				
				'footerOptions' => ['class' => 'col-address_id '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['address_id_view'],[Inflector::camel2id('Address').'/update/'.$data['address_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'owner_id',
				'label' => Service::getMeta('owner_id', 'label'),
				'contentOptions' => ['class' => 'col-owner_id '],
				'filterOptions' => ['class' => 'col-owner_id'],
				'headerOptions' => ['class' => 'col-owner_id '],				
				'footerOptions' => ['class' => 'col-owner_id '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['owner_id_view'],[Inflector::camel2id('User').'/update/'.$data['owner_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'stype',
				'label' => Service::getMeta('stype', 'label'),
				'contentOptions' => ['class' => 'col-stype '],
				'filterOptions' => ['class' => 'col-stype'],
				'headerOptions' => ['class' => 'col-stype '],				
				'footerOptions' => ['class' => 'col-stype '],
				'filter' => Service::getList('stype'),	
				
				
			],
            ['attribute' => 'duration',
				'label' => Service::getMeta('duration', 'label'),
				'contentOptions' => ['class' => 'col-duration '],
				'filterOptions' => ['class' => 'col-duration'],
				'headerOptions' => ['class' => 'col-duration '],				
				'footerOptions' => ['class' => 'col-duration '],
				'filter' => Service::getList('duration'),	
				
				
			],
            ['attribute' => 'price',
				'label' => Service::getMeta('price', 'label'),
				'contentOptions' => ['class' => 'col-price '],
				'filterOptions' => ['class' => 'col-price'],
				'headerOptions' => ['class' => 'col-price '],				
				'footerOptions' => ['class' => 'col-price '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'summa',
				'label' => Service::getMeta('summa', 'label'),
				'contentOptions' => ['class' => 'col-summa '],
				'filterOptions' => ['class' => 'col-summa'],
				'headerOptions' => ['class' => 'col-summa '],				
				'footerOptions' => ['class' => 'col-summa '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'invoice_id',
				'label' => Service::getMeta('invoice_id', 'label'),
				'contentOptions' => ['class' => 'col-invoice_id hidden '],
				'filterOptions' => ['class' => 'col-invoice_id hidden'],
				'headerOptions' => ['class' => 'col-invoice_id hidden '],				
				'footerOptions' => ['class' => 'col-invoice_id hidden '],
				
				'value' => function($data){ return $data['invoice_id_view'];},
				
			],
            ['attribute' => 'nds',
				'label' => Service::getMeta('nds', 'label'),
				'contentOptions' => ['class' => 'col-nds hidden '],
				'filterOptions' => ['class' => 'col-nds hidden'],
				'headerOptions' => ['class' => 'col-nds hidden '],				
				'footerOptions' => ['class' => 'col-nds hidden '],
				
				
				
			],
/*
            ['class' => 'yii\grid\ActionColumn'],
*/			
        ],
    ]); ?>
</div>
