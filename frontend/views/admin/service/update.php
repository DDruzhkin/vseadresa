<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Service */

$this->title = 'Редактирование  ' . $model->getCaption();
$this->params['breadcrumbs'][] = ['label' => \backend\models\Service::getMeta('','title'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this -> title;
?>
<div class="service-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'tab' => $tab  
    ]) ?>

</div>
