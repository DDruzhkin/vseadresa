<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use common\models\Err404;
use common\models\Err404Search;
use common\models\EntityHelper;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Статистика по ошибкам 404';
$this->params['breadcrumbs'][] = $this->title;
$this->context -> h1 = $this->title;

?>
<div class="err404-index">

    
	<div class="content">
	<?= \common\models\Err404::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],
        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/



            ['attribute' => 'created_at',
				'label' => Err404::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'ip',
				'label' => Err404::getMeta('ip', 'label'),
				'contentOptions' => ['class' => 'col-ip '],
				'filterOptions' => ['class' => 'col-ip'],
				'headerOptions' => ['class' => 'col-ip '],				
				'footerOptions' => ['class' => 'col-ip '],
				
				
				
			],
            ['attribute' => 'method',
				'label' => Err404::getMeta('method', 'label'),
				'contentOptions' => ['class' => 'col-method '],
				'filterOptions' => ['class' => 'col-method'],
				'headerOptions' => ['class' => 'col-method '],				
				'footerOptions' => ['class' => 'col-method '],
				'filter' => Err404::getExistsValues('method'),	
				
				
			],
            ['attribute' => 'url',
				'label' => Err404::getMeta('url', 'label'),
				'contentOptions' => ['class' => 'col-url'],
				'filterOptions' => ['class' => 'col-url'],
				'headerOptions' => ['class' => 'col-url'],				
				'footerOptions' => ['class' => 'col-url'],
				'value' => function($data){
					$url = $data -> url;
					$text = EntityHelper::mCutText($url);
					return $url == $text? $url:"<a title='$url'>$text</a>";
					
				},
				'format' => 'raw'
				
				
				
			],
        ],
    ]); ?>
</div>
