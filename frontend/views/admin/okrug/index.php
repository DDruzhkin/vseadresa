<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\models\Okrug;
use common\models\OkrugSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Административные округа Москвы';
$this->params['breadcrumbs'][] = $this->title;
$this -> context -> h1 = $this->title;
?>
<div class="okrug-index">

   
	<div class="content">

	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="controls">
       <?= Html::a('Добавить', ['create'], ['class' => 'reg-button']) ?>
    </div>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],

        'filterModel' => $searchModel,
        'columns' => [

			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => $data -> getAdminMenuItems(),
							],
						],
					]);
				}					
			],
			['attribute' => 'id',
				'label' => Okrug::getMeta('id', 'label'),
				'contentOptions' => ['class' => 'col-id '],
				'filterOptions' => ['class' => 'col-id'],
				'headerOptions' => ['class' => 'col-id '],				
				'footerOptions' => ['class' => 'col-id '],
				

			],
            ['attribute' => 'alias',
				'label' => Okrug::getMeta('alias', 'label'),
				'contentOptions' => ['class' => 'col-alias '],
				'filterOptions' => ['class' => 'col-alias'],
				'headerOptions' => ['class' => 'col-alias '],				
				'footerOptions' => ['class' => 'col-alias '],
				

			],
            ['attribute' => 'name',
				'label' => Okrug::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				'value' => function ($data) {					
					return Html::a($data -> name, ['/admin/okrug/update/', 'id' => $data -> id], ['title' => 'Изменить элемент']);
				},
				'format' => 'raw'	
				
				
			],
            ['attribute' => 'abr',
				'label' => Okrug::getMeta('abr', 'label'),
				'contentOptions' => ['class' => 'col-abr '],
				'filterOptions' => ['class' => 'col-abr'],
				'headerOptions' => ['class' => 'col-abr '],				
				'footerOptions' => ['class' => 'col-abr '],
				
				
				
			],
            ['attribute' => 'title',
				'label' => Okrug::getMeta('title', 'label'),
				'contentOptions' => ['class' => 'col-title hidden '],
				'filterOptions' => ['class' => 'col-title hidden'],
				'headerOptions' => ['class' => 'col-title hidden '],				
				'footerOptions' => ['class' => 'col-title hidden '],
				
				
				
			],
            ['attribute' => 'meta_description',
				'label' => Okrug::getMeta('meta_description', 'label'),
				'contentOptions' => ['class' => 'col-meta_description hidden '],
				'filterOptions' => ['class' => 'col-meta_description hidden'],
				'headerOptions' => ['class' => 'col-meta_description hidden '],				
				'footerOptions' => ['class' => 'col-meta_description hidden '],
				
				
				
			],
            ['attribute' => 'h1',
				'label' => Okrug::getMeta('h1', 'label'),
				'contentOptions' => ['class' => 'col-h1 hidden '],
				'filterOptions' => ['class' => 'col-h1 hidden'],
				'headerOptions' => ['class' => 'col-h1 hidden '],				
				'footerOptions' => ['class' => 'col-h1 hidden '],
				
				
				
			],
            ['attribute' => 'seo_text',
				'label' => Okrug::getMeta('seo_text', 'label'),
				'contentOptions' => ['class' => 'col-seo_text hidden '],
				'filterOptions' => ['class' => 'col-seo_text hidden'],
				'headerOptions' => ['class' => 'col-seo_text hidden '],				
				'footerOptions' => ['class' => 'col-seo_text hidden '],
				
				
				
			],
			[
                'class' => 'yii\grid\ActionColumn',

                'buttons' => [
                    'delete' => function ($url, $model) {

                        return ($model->deletable) ?           


                            Html::a(
                                Icon::show('times'),
                                Url::toRoute(['delete', 'id' => $model->id]),
                                
                                [
                                    'title' => 'Удалить элемент',
                                    'class' => 'delete',
                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                    'data-params' => ['id' => $model->id],

                                ]
                            ) : '';

                    }
                ],
                'template' => '{delete}',


            ],
        ],
    ]); ?>
</div>
