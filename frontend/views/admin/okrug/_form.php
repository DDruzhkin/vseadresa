<?php

if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Okrug;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$isNew = !isset($model) || is_null($model -> id);

$readonly = !$isNew;



$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'alias' => ['type'=>Form::INPUT_TEXT, 'label'=>Okrug::getMeta('alias_view', 'label'), 'hint'=>''.Okrug::getMeta('alias_view', 'comment'), 'options'=>['readonly'=>$readonly, ]],
						'name' => ['type'=>Form::INPUT_TEXT, 'label'=>Okrug::getMeta('name_view', 'label'), 'hint'=>''.Okrug::getMeta('name_view', 'comment'), 'options'=>['readonly'=>$readonly, ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'abr' => ['type'=>Form::INPUT_TEXT, 'label'=>Okrug::getMeta('abr_view', 'label'), 'hint'=>''.Okrug::getMeta('abr_view', 'comment'), 'options'=>['readonly'=>$readonly, ]],

		
				],
			],				
		],				
	],

		[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'title' => ['type'=>Form::INPUT_TEXT, 'label'=>Okrug::getMeta('title', 'label'), 'hint'=>''.Okrug::getMeta('title', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'meta_description' => ['type'=>Form::INPUT_TEXT, 'label'=>Okrug::getMeta('meta_description', 'label'), 'hint'=>''.Okrug::getMeta('meta_description', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],

	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'h1' => ['type'=>Form::INPUT_TEXT, 'label'=>Okrug::getMeta('h1', 'label'), 'hint'=>''.Okrug::getMeta('h1', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'seo_text' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Okrug::getMeta('seo_text', 'label'), 'hint'=>''.Okrug::getMeta('seo_text', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'public-address-button']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Okrug */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="okrug-form">


<?php



$form = ActiveForm::begin([
	'enableAjaxValidation' => true,
	'enableClientValidation'=>true
]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
