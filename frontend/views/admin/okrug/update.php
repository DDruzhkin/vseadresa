<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Okrug */

$this->title = 'Редактирование округа ' . $model->getCaption();
$this->params['breadcrumbs'][] = [
	'label' => \frontend\models\Okrug::getMeta('','title'), 
	'url' => ['index'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];
$this->params['breadcrumbs'][] = $this -> title;
$this->context -> h1 = $this -> title;
?>
<div class="okrug-update">

  
    <?= $this->render('_form', [
        'model' => $model,
		'tab' => $tab  
    ]) ?>

</div>
