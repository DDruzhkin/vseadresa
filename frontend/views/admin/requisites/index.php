<?php

use yii\helpers\Html;
use kartik\nav\NavX;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\UserPortalOptions */
/* @var $user backend\models\User */


$this->title = 'Реквизиты '.(($model -> username == 'portal')?'Портала':('пользователя ' . $model->fullname));
$this->params['breadcrumbs'][] = $this -> title;
$this -> context -> h1 = $this->title;
?>

<style>
div.card {	
	color: #333;
	width: 28%;
	float: left;
	padding: 10px 2%;
	margin: 10px 2%;
	border: 1px solid #777;
	border-radius: 5px;
}

div.card div.header {
		font-size: 14pt;
		margin-bottom: 10px;
		background: #EEE;
		padding: 5px 15px;
}

div.card div.header span.caption{
	width: 100%;
	font-weight: bold;		
	color: #333;
	padding: 0 5px;
	font-size: 11pt;
}

div.card div.value {
	margin: 0;	
	font-size: 12pt;
	width: 100%;
	text-align: center;
	padding: 0 5px;

}

div.card div.undervalue {
	margin: 0;
	width: 100%;
	text-align: center;
	color: #777;
	font-size: 8pt;
	width: 100%;
	margin: 0;
	font-weight: normal;
	padding: 0 5px 15px 5px;
	border-top: 1px dashed #AAA;	
}
</style>

<div class="user-update">

    
    <div class="rekvizit-container">
        <?php

        echo NavX::widget([
            'options' => ['class' => 'dd-button'],
            'items' => [
                [
                    'label' => 'Добавить реквизиты',
                    'items' => [
                        ['label' => 'Физического лица', 'url' => Url::to(['/admin/r/create', 'type' => 'rperson'])],
                        ['label' => 'Индивидуального предпринимателя', 'url' => Url::to(['/admin/r/create', 'type' => 'rip'])],
                        ['label' => 'Юридического лица', 'url' => Url::to(['/admin/r/create', 'type' => 'rcompany'])],
                    ],
                ],
            ],
        ]);

		echo '<div class="clear"></div>';
		
        $list = $model->requisiteList();
        if ($list) {
            foreach ($list as $id => $item) {
                $requisite = $model->byId($id);
                if (!$requisite) continue;				
                echo
                $this->render('@frontend/views/admin/' . $requisite->modelId() . '/view_one', [
                    'model' => $requisite,
                    'editable'=>true
                ]);
            }
        } else {
        }

        ?>
		<div class="clear"></div>
    </div>

	
	
</div>
