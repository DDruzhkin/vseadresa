<?php

if (YII_DEBUG) $startTime = microtime(true);

use frontend\models\Address;
use frontend\models\Nalog;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'number' => ['type'=>Form::INPUT_TEXT, 'label'=>Nalog::getMeta('number_view', 'label'), 'hint'=>''.Nalog::getMeta('number_view', 'comment'), 'options'=>['enableAjaxValidation' => "true", 'readonly'=>!$addition, 'type' => 'number', 'min' => 1]],
						'okrug_id' => 
						
							['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('okrug_id', 'label'), 'items' => Address::getListValues('okrug_id'), 'hint'=>Address::getMeta('okrug_id', 'comment'), 'options'=>['disabled'=>!$addition]],
													
		
				],
			],				
		],				
	],

	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'title' => ['type'=>Form::INPUT_TEXT, 'label'=>Nalog::getMeta('title', 'label'), 'hint'=>''.Nalog::getMeta('title', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],
	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'meta_description' => ['type'=>Form::INPUT_TEXT, 'label'=>Nalog::getMeta('meta_description', 'label'), 'hint'=>''.Nalog::getMeta('meta_description', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'h1' => ['type'=>Form::INPUT_TEXT, 'label'=>Nalog::getMeta('h1', 'label'), 'hint'=>''.Nalog::getMeta('h1', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'seo_text' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Nalog::getMeta('seo_text', 'label'), 'hint'=>''.Nalog::getMeta('seo_text', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'public-address-button']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Nalog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nalog-form">


<?php




$form = ActiveForm::begin([
	'enableAjaxValidation' => true,
	'enableClientValidation'=>true
]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
