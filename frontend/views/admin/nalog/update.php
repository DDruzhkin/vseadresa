<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Nalog */

$this->title = 'Редактирование Налоговой инспекции ' . $model->getCaption();
$this->context -> h1 = $model->getCaption();

$this->params['breadcrumbs'][] = [
	'label' => \backend\models\Nalog::getMeta('','title'), 
	'url' => ['index'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];

$this->params['breadcrumbs'][] = $this -> title;



?>
<div class="nalog-update">


    <?= $this->render('_form', [
        'model' => $model,
		'addition' => false
    ]) ?>

</div>
