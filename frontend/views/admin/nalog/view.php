<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Nalog */

$this->title = 'Просмотр Налоговой инспекции ' . $model->getCaption();
$this->context -> h1 = $model->getCaption();

$this->params['breadcrumbs'][] = [
	'label' => \backend\models\Nalog::getMeta('','title'), 
	'url' => ['index'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];

$this->params['breadcrumbs'][] = $this -> title;

$attrList = $model -> getAttributes(); 
$attributes = [];
	foreach ($attrList as $colName => $value) {		
		$type = $model -> getMeta($colName, 'type');
		$show = (int)$model -> getMeta($colName, 'show');
		if (!$show) continue;
		$attributes[] = $colName."_view" . ($type == 'text'?':ntext':'');
	}


?>
<div class="nalog-view">
    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

</div>
