	<?php
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\models\Nalog;
use common\models\NalogSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Список налоговых инспекций';
$this->params['breadcrumbs'][] = $this->title;
$this -> context -> h1 = $this->title;
?>
<div class="nalog-index">

   
	<div class="content">
	
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<div class="controls">
       <?= Html::a('Добавить', ['create'], ['class' => 'reg-button']) ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		
        'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],
        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/

			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => $data -> getAdminMenuItems(),
							],
						],
					]);
				}					
			],




            ['attribute' => 'number',
				'label' => Nalog::getMeta('number', 'label'),
				'contentOptions' => ['class' => 'col-number '],
				'filterOptions' => ['class' => 'col-number'],
				'headerOptions' => ['class' => 'col-number '],				
				'footerOptions' => ['class' => 'col-number '],
				
				'value' => function ($data) {
					return Html::a($data -> number, ['/admin/nalog/update/', 'id' => $data -> id], ['title' => 'Изменить элемент']);
				},
				'format' => 'raw'
				
			],
            ['attribute' => 'okrug_id',
				'label' => Nalog::getMeta('okrug_id', 'label'),
				'contentOptions' => ['class' => 'col-okrug_id '],
				'filterOptions' => ['class' => 'col-okrug_id'],
				'headerOptions' => ['class' => 'col-okrug_id '],				
				'footerOptions' => ['class' => 'col-okrug_id '],
				'filter' => Nalog::getExistsValues('okrug_id'),	
				'value' => function($data){ return $data['okrug_id_view'];},
				
			],
            ['attribute' => 'title',
				'label' => Nalog::getMeta('title', 'label'),
				'contentOptions' => ['class' => 'col-title hidden '],
				'filterOptions' => ['class' => 'col-title hidden'],
				'headerOptions' => ['class' => 'col-title hidden '],				
				'footerOptions' => ['class' => 'col-title hidden '],
				
				
				
			],
            ['attribute' => 'meta_description',
				'label' => Nalog::getMeta('meta_description', 'label'),
				'contentOptions' => ['class' => 'col-meta_description hidden '],
				'filterOptions' => ['class' => 'col-meta_description hidden'],
				'headerOptions' => ['class' => 'col-meta_description hidden '],				
				'footerOptions' => ['class' => 'col-meta_description hidden '],
				
				
				
			],
            ['attribute' => 'seo_text',
				'label' => Nalog::getMeta('seo_text', 'label'),
				'contentOptions' => ['class' => 'col-seo_text hidden '],
				'filterOptions' => ['class' => 'col-seo_text hidden'],
				'headerOptions' => ['class' => 'col-seo_text hidden '],				
				'footerOptions' => ['class' => 'col-seo_text hidden '],
				
				
				
			],
            ['attribute' => 'h1',
				'label' => Nalog::getMeta('h1', 'label'),
				'contentOptions' => ['class' => 'col-h1 hidden '],
				'filterOptions' => ['class' => 'col-h1 hidden'],
				'headerOptions' => ['class' => 'col-h1 hidden '],				
				'footerOptions' => ['class' => 'col-h1 hidden '],
				
				
				
			],

[
                'class' => 'yii\grid\ActionColumn',

                'buttons' => [
                    'delete' => function ($url, $model) {

                        return ($model->deletable) ?           


                            Html::a(
                                Icon::show('times'),
                                Url::toRoute(['delete', 'id' => $model->id]),
                                
                                [
                                    'title' => 'Удалить элемент',
                                    'class' => 'delete',
                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                    'data-params' => ['id' => $model->id],

                                ]
                            ) : '';

                    }
                ],
                'template' => '{delete}',


            ],
        ],
    ]); ?>
</div>
