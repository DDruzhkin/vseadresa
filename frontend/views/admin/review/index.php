<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Review;
use common\models\ReviewSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Отзывы';
$this->params['breadcrumbs'][] = $this->title;
$this -> context -> h1 = $this->title;
?>
<div class="review-index">
    
	<div class="content">
	
	</div>
  
<!--
    <p>
        <?= Html::a('Добавить отзыв', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	
-->	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,

		

		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
			
        ],
		
		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = ($model -> status -> alias == 'active'?'black':'silver');
			return ['style' => 'color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' =>$data -> getAdminMenuItems(),
							],
						],
					]);
				}					
			],

            ['attribute' => 'id',
				'label' => Review::getMeta('id', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				'value' => function($data) {
					return '<a title="Статус отзыва: '.$data -> status -> name.'">'.(Icon::show($data -> status -> alias == 'active'?'eye':'eye-slash')).'</a> '.$data -> id;
				},
				'format' => 'raw'
				
				
			],
			
			
            ['attribute' => 'created_at',
				'label' => Review::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'name',
				'label' => Review::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				
				
				
			],
            ['attribute' => 'email',
				'label' => Review::getMeta('email', 'label'),
				'contentOptions' => ['class' => 'col-email '],
				'filterOptions' => ['class' => 'col-email'],
				'headerOptions' => ['class' => 'col-email '],				
				'footerOptions' => ['class' => 'col-email '],
				
				
				
			],
            ['attribute' => 'company',
				'label' => Review::getMeta('company', 'label'),
				'contentOptions' => ['class' => 'col-company '],
				'filterOptions' => ['class' => 'col-company'],
				'headerOptions' => ['class' => 'col-company '],				
				'footerOptions' => ['class' => 'col-company '],
				
				
				
			],
            ['attribute' => 'author_position',
				'label' => Review::getMeta('author_position', 'label'),
				'contentOptions' => ['class' => 'col-author_position hidden '],
				'filterOptions' => ['class' => 'col-author_position hidden'],
				'headerOptions' => ['class' => 'col-author_position hidden '],				
				'footerOptions' => ['class' => 'col-author_position hidden '],
				
				
				
			],
/*			
            ['attribute' => 'status_id',
				'label' => Review::getMeta('status_id', 'label'),
				'contentOptions' => ['class' => 'col-status_id '],
				'filterOptions' => ['class' => 'col-status_id'],
				'headerOptions' => ['class' => 'col-status_id '],				
				'footerOptions' => ['class' => 'col-status_id '],
				'filter' => Review::getExistsValues('status_id'),	
				'value' => function($data){ return $data['status_id_view'];},
				
			],
*/			
            ['attribute' => 'content',
				'label' => Review::getMeta('content', 'label'),
				'contentOptions' => ['class' => 'col-content hidden '],
				'filterOptions' => ['class' => 'col-content hidden'],
				'headerOptions' => ['class' => 'col-content hidden '],				
				'footerOptions' => ['class' => 'col-content hidden '],
				
				
				
			],
			[
                'class' => 'yii\grid\ActionColumn',

                'buttons' => [
                    'delete' => function ($url, $model) {

                        return ($model->deletable) ?           


                            Html::a(
                                Icon::show('times'),
                                Url::toRoute(['delete', 'id' => $model->id]),
                                
                                [
                                    'title' => 'Удалить элемент',
                                    'class' => 'delete',
                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                    'data-params' => ['id' => $model->id],

                                ]
                            ) : '';

                    }
                ],
                'template' => '{delete}',


            ],			
        ],
    ]); ?>
</div>
