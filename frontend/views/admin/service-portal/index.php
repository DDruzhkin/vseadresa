<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\ServicePortal;
use common\models\ServicePortalSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Справочник услуг портала';
$this->params['breadcrumbs'][] = $this->title;
$this->context -> h1 = $this->title;
?>
<div class="service-portal-index">

	<div class="content">

	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="controls">
       <?= Html::a('Добавить', ['create'], ['class' => 'reg-button']) ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			

			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => $data -> getAdminMenuItems(),
							],
						],
					]);
				}					
			],			
/*			
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => [
									[
										'label' => 'Изменить',
										'url' => Url::to(['/admin/service-portal/update', 'id' => $data -> id]),
									],
								
								]
								,
							],
						],
					]);
				}					
			],
*/

            ['attribute' => 'id',
				'label' => ServicePortal::getMeta('id', 'label'),
				'contentOptions' => ['class' => 'col-id '],
				'filterOptions' => ['class' => 'col-id'],
				'headerOptions' => ['class' => 'col-id '],				
				'footerOptions' => ['class' => 'col-id '],
				
				
				
			],
            ['attribute' => 'name',
				'label' => ServicePortal::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				
				'value' => function ($data) {					
					return Html::a($data -> name, ['/admin/service-portal/update/', 'id' => $data -> id], ['title' => 'Изменить элемент']);
				},
				'format' => 'raw'	
				
			],
            ['attribute' => 'alias',
				'label' => ServicePortal::getMeta('alias', 'label'),
				'contentOptions' => ['class' => 'col-alias hidden '],
				'filterOptions' => ['class' => 'col-alias hidden'],
				'headerOptions' => ['class' => 'col-alias hidden '],				
				'footerOptions' => ['class' => 'col-alias hidden '],
				
				
				
			],
            ['attribute' => 'for',
				'label' => ServicePortal::getMeta('for', 'label'),
				'contentOptions' => ['class' => 'col-for '],
				'filterOptions' => ['class' => 'col-for'],
				'headerOptions' => ['class' => 'col-for '],				
				'footerOptions' => ['class' => 'col-for '],
				'filter' => ServicePortal::getList('for'),	
				
				
			],
            ['attribute' => 'price',
				'label' => ServicePortal::getMeta('price', 'label'),
				'contentOptions' => ['class' => 'col-price '],
				'filterOptions' => ['class' => 'col-price'],
				'headerOptions' => ['class' => 'col-price '],				
				'footerOptions' => ['class' => 'col-price '],
				
				'value' => function ($data) {					
					return Yii::$app -> formatter -> asCurrency($data -> price);
				},
				
				
				
			],
            ['attribute' => 'price_rules',
				'label' => ServicePortal::getMeta('price_rules', 'label'),
				'contentOptions' => ['class' => 'col-price_rules hidden '],
				'filterOptions' => ['class' => 'col-price_rules hidden'],
				'headerOptions' => ['class' => 'col-price_rules hidden '],				
				'footerOptions' => ['class' => 'col-price_rules hidden '],
				
				
				
			],
            ['attribute' => 'undeleted',
				'label' => ServicePortal::getMeta('undeleted', 'label'),
				'contentOptions' => ['class' => 'col-undeleted hidden '],
				'filterOptions' => ['class' => 'col-undeleted hidden'],
				'headerOptions' => ['class' => 'col-undeleted hidden '],				
				'footerOptions' => ['class' => 'col-undeleted hidden '],
				
				
				
			],
            ['attribute' => 'recipient_id',
				'label' => ServicePortal::getMeta('recipient_id', 'label'),
				'contentOptions' => ['class' => 'col-recipient_id hidden '],
				'filterOptions' => ['class' => 'col-recipient_id hidden'],
				'headerOptions' => ['class' => 'col-recipient_id hidden '],				
				'footerOptions' => ['class' => 'col-recipient_id hidden '],
				
				'value' => function($data){ return $data['recipient_id_view'];},
				
			],
			[
                'class' => 'yii\grid\ActionColumn',

                'buttons' => [
                    'delete' => function ($url, $model) {

                        return ($model->deletable) ?           


                            Html::a(
                                Icon::show('times'),
                                Url::toRoute(['delete', 'id' => $model->id]),
                                
                                [
                                    'title' => 'Удалить элемент',
                                    'class' => 'delete',
                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                    'data-params' => ['id' => $model->id],

                                ]
                            ) : '';

                    }
                ],
                'template' => '{delete}',


            ],		
        ],
    ]); ?>
</div>
