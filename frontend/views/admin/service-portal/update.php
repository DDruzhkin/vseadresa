<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ServicePortal */

$this->title = 'Редактирование услуги ' . $model->getCaption();
$this -> context -> h1 = $this->title;
$this->params['breadcrumbs'][] = [
	'label' => \backend\models\ServicePortal::getMeta('','title'), 
	'url' => ['index'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];
$this->params['breadcrumbs'][] = $this -> title;
?>
<div class="service-portal-update">

    <?= $this->render('_form', [
        'model' => $model,
		'tab' => $tab  
    ]) ?>

</div>
