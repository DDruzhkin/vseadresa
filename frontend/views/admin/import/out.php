<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Импорт исходящих платежей';
$this->context -> h1 = $this->title;
$this->params['breadcrumbs'][] = $this->title;


?>

    <?php
	$i = 0;
    if($data && is_array($data) && count($data)) : ?>
    <form action="<?php echo Yii::$app->urlManager->createUrl(['admin/import/load']);?>" method="POST">
        <input type="hidden" name="_csrf-backend" value="<?=Yii::$app->request->getCsrfToken()?>" />
		
        <table class="table">
		<tr class="head">
            <th>Дата платежа</th>
            <th>Сумма</th>
			<th>П/п</th>            
            <th>Плательщик</th>            
			<th>Получатель</th>            
			 <th>Информация</th>
		</tr>	
		
		
        <?php foreach ( $data as $line): 
		
					
			$class = isset($line['messageType'])?('line-'.$line['messageType']):false;
			$class = $class?' class="'.$class.'"':'';
			?>
			
		<input type="hidden" name="data[out]" value="out"/>
		<tr<?=$class?>>
                <td>
                    <?=$line['pmDate']; ?>
					<?php if ($mode == 'view'):?>
					<input type="hidden" name="data[<?=$i?>][pmDate]" value="<?=$line['pmDate']?>"/>
					<?php endif?>
                </td>
                <td>
                     <?=Yii::$app -> formatter -> asCurrency($line['summ'])?>
					<?php if ($mode == 'view'):?>
					<input type="hidden" name="data[<?=$i?>][summ]" value="<?=$line['summ']?>"/>
					<?php endif?>
                </td>
                <td>
                    <?='№'.$line['ppNumber'].'<br>от '.$line['ppDate'];?>
					<?php if ($mode == 'view'):?>
						<input type="hidden" name="data[<?=$i?>][ppNumber]" value="<?=$line['ppNumber']?>"/>
						<input type="hidden" name="data[<?=$i?>][ppDate]" value="<?=$line['ppDate']?>"/>
					<?php endif?>
                </td>
            
				<td>
                    <?=$line['payer'];?>
                </td>   
				<td>
                    <?=$line['recipient'];?>
					<?php if ($mode == 'view'):?>
						<input type="hidden" name="data[<?=$i?>][recipient]" value="<?=$line['recipientId']?>"/>
					<?php endif?>
                </td>    				
				<td>	
					<?=$line['message']?>
					
				</td>
				
                <?php $i++; ?>

            </tr>
        <?php endforeach; ?>
        </table>
		<?php if ($mode == 'view'):?>
        <button type="submit" class="public-address-button" style="margin-bottom: 20px;">Загрузить</button>
		<?php endif ?>
		<input type="hidden" name="_csrf-backend" value="<?=Yii::$app->request->getCsrfToken()?>" />
    </form>
    <?php endif; ?>
	
	
		<div class="content">
		Выберите файл MS Excel с платежами порталу, которые необходимо загрузить в систему.
		</div>
        <form enctype="multipart/form-data" action="<?php echo Yii::$app->urlManager->createUrl(['admin/import/out']);?>" method="POST">
            <div class="form-group">                
                <input type="file" onchange="form.submit();" name="excel" class="form-control" id="file">
                <input type="hidden" name="_csrf-backend" value="<?=Yii::$app->request->getCsrfToken()?>" />
            </div>            
        </form>
 