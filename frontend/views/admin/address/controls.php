<?php
use kartik\builder\Form;
use backend\models\Address;
use common\models\Nalog;
use common\models\AddressTaxAgency;
use backend\models\ServicePrice;
use backend\models\Option;
use yii\helpers\Html;

function controls($name, $model = false) {
	if ($name == 'options') {
		$options = Option::listAll(Address::getMeta('options', 'itemtemplate'));
		if (!$model -> canNew()) {
			$new_id = Option::byAlias('new') -> id;
			unset($options[$new_id]);
		}
	}

	switch ($name) {
		case 'gps' : return 		
			[
							'type'=>Form::INPUT_WIDGET, 
							'widgetClass'=>'\common\widgets\ymap\Ymap', 
							//'label'=>Address::getMeta('gps', 'label'), 
							'label'=>'Местоположение адреса', 
							'hint'=>''.Address::getMeta('gps', 'comment'), 
								'options' => [
									'options' => ['style' => 'width:100%; height: 500px;margin-bottom: 20px;'],
									'params' => ['zoom' => 15]								
								]
			];
		

		case 'adr_index' : return ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('adr_index', 'label'), 'hint'=>''.Address::getMeta('adr_index', 'comment'), 'options'=>['mask' => '999999',]];
		case 'is_new' : return ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta($name, 'label'), 'hint'=>''.Address::getMeta($name, 'comment'), 'options'=>['readonly'=>'true', 'value' => $model -> $name, ]];		
		case 'address' : return ['type'=>Form::INPUT_TEXTAREA, 'label'=>Address::getMeta('address', 'label'), 'hint'=>''.Address::getMeta('address', 'comment'), 'options'=>[]];
		//case 'status_id' : return ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('status_id', 'label'), 'hint'=>''.Address::getMeta('status_id', 'comment'), 'items' => $model -> getStatusList(), 'options'=>['readonly'=>'true', ]];
		case 'status_view' : return ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('status_id', 'label'), 'hint'=>''.Address::getMeta('status_id', 'comment'), 'options'=>['readonly'=>'true', 'value' => $model -> status_id_view, ]];		
		case 'nalog_id' : return ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('nalog_id', 'label'), 'items' => Address::getListValues('nalog_id'), 'hint'=>Address::getMeta('nalog_id', 'comment'), 'options'=>[]];
		case 'okrug_id' : return ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('okrug_id', 'label'), 'items' => Address::getListValues('okrug_id'), 'hint'=>Address::getMeta('okrug_id', 'comment'), 'options'=>[]];
		case 'metro_id' : return ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('metro_id', 'label'), 'items' => Address::getListValues('metro_id'), 'hint'=>Address::getMeta('metro_id', 'comment'), 'options'=>[]];
		case 'region_id' : return ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('region_id', 'label'), 'items' => Address::getListValues('region_id'), 'hint'=>Address::getMeta('region_id', 'comment'), 'options'=>[]];
        case 'reg_msk' :
            return ['type' => Form::INPUT_CHECKBOX, 'label' => Address::getMeta('reg_msk', 'label'), 'hint' => '' . Address::getMeta('reg_msk', 'comment'), 'options' => []];
        case 'reg_mo' :
            return ['type' => Form::INPUT_CHECKBOX, 'label' => Address::getMeta('reg_mo', 'label'), 'hint' => '' . Address::getMeta('reg_mo', 'comment'), 'options' => []];
        case 'reg_other' :
            return ['type' => Form::INPUT_CHECKBOX, 'label' => Address::getMeta('reg_other', 'label'), 'hint' => '' . Address::getMeta('reg_other', 'comment'), 'options' => []];
        case 'tax_agencies':
            $opts = ['multiple' => true, 'class'=>['cb_container-wide']];
            if ($model->id) {
                $opts["value"] = AddressTaxAgency::getAddressAgencyId($model->id);
            }
            return [
                'type' => Form::INPUT_CHECKBOX_LIST,
                'label' => '',
                'items' => Nalog::listAll('ИФНС №%number%'),
                'hint' => Address::getMeta('nalog_id', 'comment'),
                'options' => $opts,

            ];
        case 'okrug_id_view' : return ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('okrug_id_view', 'label'), 'hint'=>Address::getMeta('okrug_id_view', 'comment'), 'options'=>['readonly'=>'true', ]];
		case 'demo' : {
			
			if (!$model || in_array($model -> status -> alias, ['new'])) { // в каких статусах не показывать флажок Демо
				return ['type'=>Form::INPUT_RAW,'value'=> ''];
			} else {
				//return ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>Address::getMeta('demo', 'label'), 'hint'=>''.Address::getMeta('demo', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false,],]];
				return ['type'=>Form::INPUT_CHECKBOX, 'label'=>Address::getMeta($name, 'label'), 'hint'=>''.Address::getMeta($name, 'comment'), 'options'=>[]];
			}
		}
		
		case 'quickable' : return ['type'=>Form::INPUT_CHECKBOX, 'label'=>Address::getMeta($name, 'label'), 'hint'=>''.Address::getMeta($name, 'comment'), 'options'=>[]];
		
		case 'square' : return ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('square', 'label'), 'hint'=>''.Address::getMeta('square', 'comment'), 'options'=>['clientOptions' => ['alias' =>  'decimal', 'groupSeparator' => ' ','autoGroup' => true],]];
		case 'options_as_array' : return ['type'=>Form::INPUT_CHECKBOX_LIST, 'label'=>'', 'items' => Option::listAll(Address::getMeta('options', 'itemtemplate')), 'hint'=>''.Address::getMeta('options', 'comment'), 'options'=>['multiple' => true]];
		case 'options' : return ['type'=>Form::INPUT_CHECKBOX_LIST, 'label'=>'', 'items' => $options, 'hint'=>''.Address::getMeta('options', 'comment'), 'options'=>['multiple' => true]];
		case 'photo_date' : return ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\datetime\DateTimePicker', 'label'=>Address::getMeta('photo_date', 'label'), 'hint'=>''.Address::getMeta('photo_date', 'comment'), 'options'=>['type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
			'pluginOptions' => [
					'todayHighlight' => true, 
					'autoclose'=>true,   			
					'startDate' => '0d',
					'format' => 'dd-mm-yyyy hh:ii'
			],
		]];
		case 'service_period' : return ['type'=>Form::INPUT_DROPDOWN_LIST, 'items' => ['4 часа' => '4 часа', '8 часа' => '8 часа', '12 часа' => '12 часа', '24 часа' => '24 часа'],'label'=>'Срок', 'hint'=>'Срок оформления заказа', 'options'=>[]];
		case 'info' : return ['type'=>Form::INPUT_TEXTAREA, 'label'=>Address::getMeta('info', 'label'), 'hint'=>''.Address::getMeta('info', 'comment'), 'options'=>[]];

		case 'price6' : return ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price6', 'label'), 'hint'=>''.Address::getMeta('price6', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]];
		case 'price11' : return ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price11', 'label'), 'hint'=>''.Address::getMeta('price11', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]];

        case 'price_first_6': return ['type' => Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label' => 'Стоимость первичной регистрации адреса на 6 месяцев', 'hint' => Address::getMeta('price_first_6', 'comment'), 'options' => ['mask' => Address::getMaskByAlias('money'),]];
        case 'price_first_11': return ['type' => Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label' => 'Стоимость первичной регистрации адреса на 11 месяцев', 'hint' => Address::getMeta('price_first_11', 'comment'), 'options' => ['mask' => Address::getMaskByAlias('money'),]];
        case 'price_change_6': return ['type' => Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label' => 'Стоимость регистрации изменений на 6 месяцев', 'hint' => Address::getMeta('price_change_6', 'comment'), 'options' => ['mask' => Address::getMaskByAlias('money'),]];
        case 'price_change_11': return ['type' => Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label' => 'Стоимость регистрации изменений на 11 месяцев', 'hint' => Address::getMeta('price_change_11', 'comment'), 'options' => ['mask' => Address::getMaskByAlias('money'),]];
        case 'price_prolongation_6': return ['type' => Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label' => 'Стоимость пролонгации на 6 месяцев', 'hint' => Address::getMeta('price_prolongation_6', 'comment'), 'options' => ['mask' => Address::getMaskByAlias('money'),]];
        case 'price_prolongation_11': return ['type' => Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label' => 'Стоимость пролонгации на 11 месяцев', 'hint' => Address::getMeta('price_prolongation_11', 'comment'), 'options' => ['mask' => Address::getMaskByAlias('money'),]];

		case 'price6min' : return ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price6min', 'label'), 'hint'=>''.Address::getMeta('price6min', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]];
		case 'price11min' : return ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price11min', 'label'), 'hint'=>''.Address::getMeta('price11min', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]];
		case 'price6max' : return ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price6max', 'label'), 'hint'=>''.Address::getMeta('price6max', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]];
		case 'price11max' : return ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price11max', 'label'), 'hint'=>''.Address::getMeta('price11max', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]];
		case 'priority' : return ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('priority', 'label'), 'hint'=>''.Address::getMeta('priority', 'comment'), 'options'=>['type' => 'number', 'max' => 9999]];		
		
		case 'service_description' : return ['type'=>Form::INPUT_TEXTAREA, 'label'=>'Условия', 'hint'=>'Условия срочного оформления заказа', 'options'=>[]];
		case 'service_price' : return ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>'Стоимость', 'hint'=>'Стоимость срочного оформления заказа', 'options'=>['mask' => ServicePrice::getMaskByAlias("money"),]];
		

		case 'h1' : return ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta($name, 'label'), 'hint'=>''.Address::getMeta($name, 'comment'), 'options'=>[]];
		case 'title' : return ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta($name, 'label'), 'hint'=>''.Address::getMeta($name, 'comment'), 'options'=>[]];
		case 'meta_description' : return ['type'=>Form::INPUT_TEXTAREA, 'label'=>Address::getMeta($name, 'label'), 'hint'=>''.Address::getMeta($name, 'comment'), 'options'=>[]];		
		case 'seo_text' : return ['type'=>Form::INPUT_TEXTAREA, 'label'=>Address::getMeta($name, 'label'), 'hint'=>''.Address::getMeta($name, 'comment'), 'options'=>['style' => 'height: 300px;']];		
		
		case 'photos': return ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\common\widgets\photos\PhotoList', 'label'=>Address::getMeta('photos', 'label'), 'hint'=>''.Address::getMeta('photos', 'comment'), 'options'=>[]];
		
		case 'prior' : return ['type'=>Form::INPUT_RAW, 'value'=> Html::submitButton('Назад', ['class'=>'reg-button prior'])];
		case 'next' : return isset($model)?['type'=>Form::INPUT_RAW, 'value'=>  Html::submitButton(($model -> getIsNewRecord()?'Добавить адрес':'Далее'), ['class'=>'reg-button next'])]:false;
		case 'submit' : return isset($model)?['type'=>Form::INPUT_RAW, 'value'=> Html::submitButton(($model -> getIsNewRecord()?'Добавить':'Сохранить').' данные', ['class'=>'reg-button next', 'style' => 'margin-bottom: 10px'])]:false;
		
		

		default: return false;
		
	}
	
	
}


?>