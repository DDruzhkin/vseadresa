<?php
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\models\Address;
use frontend\models\Option;
use frontend\models\User;
use common\models\AddressSearch;
use common\models\Entity;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Адреса';
$this->params['breadcrumbs'][] = $this->title;
$this->context -> h1 = $this->title;


?>
<div class="address-index">
    
	<div class="content">

		<?php
		
	
		switch ($param) {
		case 'owner_id': {
			$user = User::findone([$param_value]);
			if($user) {
				echo 'Адреса Владельца '.Html::a($user -> fullname, ['admin/user/view', 'id' => $user -> id]).'<br>';
			};
			break;
		};
		
		
		
	}
	
	?>
	
	</div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,

		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],
        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					$items = $data -> getAdminMenuItems();
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => $items,
							],
						],
					]);
				}					
			],


            ['attribute' => 'id',
				'label' => Address::getMeta('id', 'label'),
				'contentOptions' => ['class' => 'col-id '],
				'filterOptions' => ['class' => 'col-id'],
				'headerOptions' => ['class' => 'col-id '],				
				'footerOptions' => ['class' => 'col-id '],
				
				
				
			],

            ['attribute' => 'address',
				'label' => Address::getMeta('address', 'label'),
				'contentOptions' => ['class' => 'col-address '],
				'filterOptions' => ['class' => 'col-address'],
				'headerOptions' => ['class' => 'col-address '],				
				'footerOptions' => ['class' => 'col-address '],
			'value' => 
			function($data)
			{ 
				$demoIcon = $data -> demo?"check-circle-o":"circle-o";
				$value = 
					Html::a(Icon::show($demoIcon), Url::to(['/admin/address/demo/', 'id' => $data -> id, 'value' => !$data -> demo]), ['title' => 'Отображение в каталоге '.($demoIcon = $data -> demo?'разрешено':'запрещено')])
					.' '
					.Html::a($data -> address,['/admin/address/view', 'id' => $data -> id], ['title' => 'Перейти на карточку адреса']);
				return $value;
			},
				'format' => 'raw',
				
				
			],
            ['attribute' => 'status_id',
				'label' => Address::getMeta('status_id', 'label'),
				'contentOptions' => ['class' => 'col-status_id '],
				'filterOptions' => ['class' => 'col-status_id'],
				'headerOptions' => ['class' => 'col-status_id '],				
				'footerOptions' => ['class' => 'col-status_id '],
				'filter' => Address::getExistsValues('status_id'),	
				'value' => function($data){ return $data['status_id_view'];},
				
			],
            ['attribute' => 'owner_id',
				'label' => Address::getMeta('owner_id', 'label'),
				'contentOptions' => ['class' => 'col-owner_id '],
				'filterOptions' => ['class' => 'col-owner_id'],
				'headerOptions' => ['class' => 'col-owner_id '],				
				'footerOptions' => ['class' => 'col-owner_id '],
				
				'value' => 
			function($data)
			{ 
				//$value = Html::a($data['owner_id_view'],[Inflector::camel2id('User').'/update/'.$data['owner_id']]);
				
				$value = Html::a($data -> owner -> fullname,['/admin/user/view', 'id' => $data -> owner_id], ['title' => 'Перейти на карточку владельца']);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'nalog_id',
				'label' => Address::getMeta('nalog_id', 'label'),
				'contentOptions' => ['class' => 'col-nalog_id '],
				'filterOptions' => ['class' => 'col-nalog_id'],
				'headerOptions' => ['class' => 'col-nalog_id '],				
				'footerOptions' => ['class' => 'col-nalog_id '],
				'filter' => Address::getExistsValues('nalog_id'),	
				
				'value' => 
			function($data)
			{ 
				//$value = Html::a($data['nalog_id_view'],[Inflector::camel2id('Nalog').'/update/'.$data['nalog_id']]);
				
				$value = ($data -> nalog_id) ? $data -> nalog_view: '';
				return $value;
			},
				'format' => 'raw',
				
				
			],
            ['attribute' => 'metro_id',
				'label' => Address::getMeta('metro_id', 'label'),
				'contentOptions' => ['class' => 'col-metro_id'],
				'filterOptions' => ['class' => 'col-metro_id'],
				'headerOptions' => ['class' => 'col-metro_id'],				
				'footerOptions' => ['class' => 'col-metro_id'],
				'filter' => Address::getExistsValues('metro_id'),	
				
				'value' => 
			function($data)
			{ 
				//$value = Html::a($data['metro_id_view'],[Inflector::camel2id('Metro').'/update/'.$data['metro_id']]);
				
				$value = ($data -> metro_id) ? $data -> metro_view: '';
				return $value;
			},
				'format' => 'raw',
				
			],
			
		
			
            ['attribute' => 'okrug_id',
				'label' => Address::getMeta('okrug_id', 'label'),
				'contentOptions' => ['class' => 'col-okrug_id '],
				'filterOptions' => ['class' => 'col-okrug_id'],
				'headerOptions' => ['class' => 'col-okrug_id '],				
				'footerOptions' => ['class' => 'col-okrug_id '],
				'filter' => Address::getExistsValues('okrug_id'),	
				
				'value' => 
			function($data)
			{ 
				//$value = Html::a($data['okrug_id_view'],[Inflector::camel2id('Okrug').'/update/'.$data['okrug_id']]);
				
				$value = ($data -> okrug_id) ? $data -> okrug_view: '';
				return $value;
			},
				'format' => 'raw',
				
			],
			


            ['attribute' => 'priority',
				'label' => Address::getMeta('priority', 'label'),
				'contentOptions' => ['class' => 'col-priority'],
				'filterOptions' => ['class' => 'col-priority'],
				'headerOptions' => ['class' => 'col-priority'],				
				'footerOptions' => ['class' => 'col-priority'],
				'value' => function($data) {
					return $data -> priority?$data -> priority:'0';
				}
				
				
			],
			
			[
                'class' => 'yii\grid\ActionColumn',

                'buttons' => [
                    'delete' => function ($url, $model) {

                        return ($model->deletable) ?           


                            Html::a(
                                Icon::show('times'),
                                Url::toRoute(['delete', 'id' => $model->id]),
                                
                                [
                                    'title' => 'Удалить элемент',
                                    'class' => 'delete',
                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                    'data-params' => ['id' => $model->id],

                                ]
                            ) : '';

                    }
                ],
                'template' => '{delete}',


            ],
        ],
    ]); ?>
</div>
