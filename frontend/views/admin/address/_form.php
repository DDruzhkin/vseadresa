<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);

use backend\models\Address;
use backend\models\Option;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\AddressTaxAgency;
use yii\web\View;


if (!isset($tab) || !$tab) {
    $tab = 'base';
};


$tabItems = $model->getAdminTabItems();
$title = '';
foreach ($tabItems as $item) {
    if ($item['tab'] == $tab) {
        $title = $item['label'];
        break;
    }
}
echo Html::tag('h2', $title);

include "controls.php";

$data = [];
switch ($tab) {
    case 'map'     :
        {

            $data = [

                [
                    'attributes' => [
                        'gps' => controls('gps', $model),
                    ]

                ],

                [
                    'attributes' => [
                        'submit' => controls('submit', $model),
                    ]

                ]
            ];
            break;
        }

    case 'options'     :
        {

            $data = [

                [
                    'attributes' => [
                        'options' => controls('options', $model),
                    ]

                ],

                [
                    'attributes' => [
                        'submit' => controls('submit', $model),
                    ]

                ]
            ];
            break;
        }

    case 'base'    :
        {

            $data = [


                [

                    'attributes' => [

                        'adr_index' => controls('adr_index'),
                        'metro_id' => controls('metro_id'),
                    ]
                ],

                [
                    'attributes' => [
                        'nalog_id' => controls('nalog_id'),
                        'okrug_id' => controls('okrug_id'),
                    ],
                ],
                ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<label class="control-label">Ограничение регистрации по ИФНС</label>'],]],
                ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<span class="span-hint">Если есть ограничения, отметьте ИФНС, для которых регистрация будет недоступна.</span>'],]],
                ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<span class="select_all">Выбрать все</span> / <span class="unselect_all">Отменить выбор</span>'],]],

                [
                    'attributes' => [
                        'tax_agencies' => controls('tax_agencies', $model)
                    ]
                ],
                ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<label class="control-label">Ограничение по месту регистрации исполнительного лица</label>'],]],
                ['attributes' => ['header' => ['type' => Form::INPUT_RAW, 'value' => '<span class="span-hint">Если есть ограничения, отметьте субъекты, для которых регистрация будет недоступна.</span>'],]],
                [
                    'attributes' => [
                        'reg_msk' => controls('reg_msk'),
                        'reg_other' => controls('reg_other'),
                    ],
                ],
                [
                    'attributes' => [
                        'reg_mo' => controls('reg_mo'),
                    ]
                ],
                [
                    'attributes' => [
                        'address' => controls('address'),
                    ]
                    ,
                ],


                /*
                                    [
                                        'attributes' => [
                                            'square' => controls('square'),
                                            'status_view' => controls('status_view', $model),
                                        ],
                                    ],
                */


                [

                    'attributes' => [
                        'demo' => controls('demo', $model),
                        'quickable' => controls('quickable', $model),
                    ]

                ],
                [

                    'attributes' => [
                        'priority' => controls('priority'),
                        'square' => controls('square'),

                    ]

                ],
                /*[
                    'attributes' => [
                        'price6' => controls('price6'),
                        'price11' => controls('price11'),
                    ],
                ], */
                ['attributes' => [
                    'price_first_6' => controls('price_first_6'),
                    'price_first_11' => controls('price_first_11'),
                ]],
                ['attributes' => [
                    'price_change_6' => controls('price_change_6'),
                    'price_change_11' => controls('price_change_11'),
                ]],
                ['attributes' => [
                    'price_prolongation_6' => controls('price_prolongation_6'),
                    'price_prolongation_11' => controls('price_prolongation_11'),
                ]],
                [
                    'attributes' => [
                        'price6min' => controls('price6min'),
                        'price11min' => controls('price11min'),
                    ],
                ],
                [
                    'attributes' => [
                        'price6max' => controls('price6max'),
                        'price11max' => controls('price11max'),
                    ],
                ],
                [
                    'attributes' => [
                        'info' => controls('info', $model),
                    ]

                ],
                [
                    'attributes' => [
                        'submit' => controls('submit', $model),
                    ]

                ]


            ];

            break;
        };

    case 'seo'    :
        {

            $data = [

                [
                    'attributes' => [
                        'h1' => controls('h1'),
                    ],
                ],
                [
                    'attributes' => [
                        'title' => controls('title'),
                    ],
                ],
                [
                    'attributes' => [
                        'meta_description' => controls('meta_description'),
                    ],
                ],

                [
                    'attributes' => [
                        'seo_text' => controls('seo_text'),
                    ]

                ],
                [
                    'attributes' => [
                        'submit' => controls('submit', $model),
                    ]

                ]
            ];

            break;
        };
    case 'photos'    :
        {
            $data = [

                [
                    'attributes' => [
                        'photos' => controls('photos', $model),
                    ]

                ],
                [
                    'attributes' => [
                        'submit' => controls('submit', $model),
                    ]

                ]

            ];
            break;
        }

}

/*	
	
	$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
						'gps' => [
							'type'=>Form::INPUT_WIDGET, 
							'widgetClass'=>'\common\widgets\ymap\Ymap', 
							'label'=>Address::getMeta('gps', 'label'), 
							'hint'=>''.Address::getMeta('gps', 'comment'), 
								'options' => [
									'options' => ['style' => 'width:100%; height: 300px'],
									'params' => ['zoom' => 15]								
								]
							],
						
//						'gps' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Address::getMeta('gps', 'label'), 'hint'=>''.Address::getMeta('gps', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('created_at_view', 'label'), 'hint'=>''.Address::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'adr_index' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('adr_index', 'label'), 'hint'=>''.Address::getMeta('adr_index', 'comment'), 'options'=>['mask' => '999999',]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'address' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Address::getMeta('address', 'label'), 'hint'=>''.Address::getMeta('address', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'status_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('status_id', 'label'), 'hint'=>''.Address::getMeta('status_id', 'comment'), 'items' => $model -> getStatusList()],	
						'owner_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('owner_id', 'label'), 'items' => Address::getListValues('owner_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('user/update/%owner_id%')]).'">'.Icon::show('link').'</a>'.Address::getMeta('owner_id', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'nalog_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('nalog_id', 'label'), 'items' => Address::getListValues('nalog_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('nalog/update/%nalog_id%')]).'">'.Icon::show('link').'</a>'.Address::getMeta('nalog_id', 'comment'), 'options'=>[]],
						'metro_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('metro_id', 'label'), 'items' => Address::getListValues('metro_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('metro/update/%metro_id%')]).'">'.Icon::show('link').'</a>'.Address::getMeta('metro_id', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'region_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('region_id', 'label'), 'items' => Address::getListValues('region_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('region/update/%region_id%')]).'">'.Icon::show('link').'</a>'.Address::getMeta('region_id', 'comment'), 'options'=>[]],
						'okrug_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('okrug_id_view', 'label'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('okrug/update/%okrug_id%')]).'">'.Icon::show('link').'</a>'.Address::getMeta('okrug_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'demo' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>Address::getMeta('demo', 'label'), 'hint'=>''.Address::getMeta('demo', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],]],
						'square' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('square', 'label'), 'hint'=>''.Address::getMeta('square', 'comment'), 'options'=>['clientOptions' => ['alias' =>  'decimal', 'groupSeparator' => ' ','autoGroup' => true],]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'options_as_array' => ['type'=>Form::INPUT_CHECKBOX_LIST, 'label'=>Address::getMeta('options', 'label'), 'items' => Option::listAll(Address::getMeta('options', 'itemtemplate')), 'hint'=>''.Address::getMeta('options', 'comment'), 'options'=>['multiple' => true]],
						'photo_date' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Address::getMeta('photo_date', 'label'), 'hint'=>''.Address::getMeta('photo_date', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'subscribe_to' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Address::getMeta('subscribe_to', 'label'), 'hint'=>''.Address::getMeta('subscribe_to', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
						'quickable' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>Address::getMeta('quickable', 'label'), 'hint'=>''.Address::getMeta('quickable', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'info' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Address::getMeta('info', 'label'), 'hint'=>''.Address::getMeta('info', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'price6' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price6', 'label'), 'hint'=>''.Address::getMeta('price6', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]],
						'price11' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price11', 'label'), 'hint'=>''.Address::getMeta('price11', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'price6min' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price6min', 'label'), 'hint'=>''.Address::getMeta('price6min', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]],
						'price11min' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price11min', 'label'), 'hint'=>''.Address::getMeta('price11min', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'price6max' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price6max', 'label'), 'hint'=>''.Address::getMeta('price6max', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]],
						'price11max' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price11max', 'label'), 'hint'=>''.Address::getMeta('price11max', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
//						'photos' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\common\widgets\photos\PhotoList', 'label'=>Address::getMeta('photos', 'label'), 'hint'=>''.Address::getMeta('photos', 'comment'), 'options'=>[]],
//						'files' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\common\widgets\photos\PhotoList', 'label'=>Address::getMeta('files', 'label'), 'hint'=>''.Address::getMeta('files', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'range' => ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('range', 'label'), 'hint'=>''.Address::getMeta('range', 'comment'), 'options'=>[]],
						'h1' => ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('h1', 'label'), 'hint'=>''.Address::getMeta('h1', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'title' => ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('title', 'label'), 'hint'=>''.Address::getMeta('title', 'comment'), 'options'=>[]],
						'meta_description' => ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('meta_description', 'label'), 'hint'=>''.Address::getMeta('meta_description', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'seo_text' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Address::getMeta('seo_text', 'label'), 'hint'=>''.Address::getMeta('seo_text', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'enabled' => ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('enabled', 'label'), 'hint'=>''.Address::getMeta('enabled', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]

*/


/* @var $this yii\web\View */
/* @var $model backend\models\Address */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="address-form">
    <div class="address-status">
        <?php
        echo NavX::widget([
            'options' => ['class' => 'nav nav-context menu-custom-icon'],
            'encodeLabels' => false,
            'items' => [
                [
                    'label' => Icon::show('bars'),
                    'items' => $tabItems
                ],
            ],
        ]);
        ?>

        Текущий статус адреса: <strong><?= $model->status_view ?></strong>
        <?php if ($model->status->alias == 'preparation'): ?>
            <a href="<?= Url::to(['/admin/address/ready', 'id' => $model->id]) ?>" class="public-address-button">Готов к
                публикации</a>
            <!--
	<form name="change-status" method="get" action="<?= Url::to(['/admin/address/ready', 'id' => $model->id]) ?>">
	<button type="submit" class="public-address-button">Готов к публикации</button>
	</form>
-->
        <?php endif ?>
    </div>
    <?php
    $tax_id_array = AddressTaxAgency::getAddressAgencyId($model->id);
    $check_all = (int)empty($tax_id_array);
    $script = <<< JS
    /*if($check_all){
        $('#address-tax_agencies .checkbox input[type=checkbox]').attr('checked', true);
        $('#address-tax_agencies .checkbox .form-checkbox').addClass('active-checkbox');
    }*/
    $('.select_all').click(function () {
        console.log('select all');
        $('#address-tax_agencies .checkbox input[type=checkbox]').attr('checked', true);
        $('#address-tax_agencies .checkbox .form-checkbox').addClass('active-checkbox');
    })
    $('.unselect_all').click(function () {
        console.log('unselect all');
        $('#address-tax_agencies .checkbox input[type=checkbox]').attr('checked', false);
        $('#address-tax_agencies .checkbox .form-checkbox').removeClass('active-checkbox');
    })
JS;
    $this->registerJs($script, View::POS_READY);
    ?>
    <!--
    <div style="margin-top: 20px">
    <label>
    <input
        type="checkbox"
        id="address_is_new"
        name="address_is_new"
        value="1"
        checked=""
        style="display: none;"/>

        Адрес является новым
    </label>
    </div>
    -->
    <?php


    //echo "<hr/>";


    $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'rows' => $data
    ]);

    ActiveForm::end();

    if (YII_DEBUG) {
        $finTime = microtime(true);
        $delta = $finTime - $startTime;
        echo $delta . ' сек.';
    }
    ?>

</div>
