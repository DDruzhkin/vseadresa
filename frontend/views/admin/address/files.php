<?php

use frontend\models\Address;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;
use kartik\nav\NavX;

$this->title = "Документы";


$this->params['breadcrumbs'][] = [
	'label' => Address::getMeta('','title'), 
	'url' => ['index'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];

$this->params['breadcrumbs'][] = [
	'label' => $model -> address, 
	'url' => ['update', 'id' => $model -> id],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];


$this->params['breadcrumbs'][] = $this->title;
$this -> context -> h1 = $this->title;


$tabItems = $model -> getAdminTabItems();
$title = '';
foreach($tabItems as $item) {
	if ($item['tab'] == 'files') {
		$title = $item['label'];
		break;
	}
}

		echo NavX::widget([
			'options'=>['class'=>'nav nav-context menu-custom-icon'],
			'encodeLabels' => false,
			'items' => [
				[
					'label' => Icon::show('bars'),			
					'items' => $tabItems
				],
			],
		]);

?>
<div class="address-view">



</div>

<?php	

?>
<div style="clear:both"></div>
<?php 
$form = ActiveForm::begin(['id' => 'address-files-form', 'action' => Url::to(['admin/address/files', 'id' => $model -> id]), 'method' => 'post', 'options' => ['enctype' => 'multipart/form-data']]);

if (is_array($files) && count($files)) {
?>
<table class="table table-striped table-bordered"><thead><tr><th>Номер</th><th>Дата</th><th>Название</th><th></th><th style="text-align:center;">Удалить</th></tr></thead>
<?php
$index = 1;
foreach($files as $file) {
//	var_dump($file); exit;
		$number = ArrayHelper::getValue($file, 'number');
		$date = ArrayHelper::getValue($file, 'date');
		if ($date) {
			$date = Yii::$app -> formatter -> asDate(strtotime($date));
		}
		$fn = ArrayHelper::getValue($file, 'filename');
		echo  '
		<tr>
		<td>
		'.$form->field($model, 'numbers['.$index.']')->textInput(['value' => $number]) -> label(false).'
		</td>
		<td>'.$form->field($model, 'dates['.$index.']')->textInput(['value' => $date]) -> label(false).'</td>
		<td>'.$form->field($model, 'names['.$index.']')->textInput(['value' => ArrayHelper::getValue($file, 'name')]) -> label(false).'</td>		
		<td style="text-align:center;">'.
			$form->field($model, 'filenames['.$index.']')->hiddenInput(['value' => $fn])->label(false).
			Html::a(Icon::show('download'), Url::to(['/files/documents/uploaded/'.$fn]), ['title' => 'Скачать файл']).		
		'
		</td>
		<td style="text-align:center; padding-top:20px;"><input type="checkbox" name="'.Address::baseClassName().'[removes]['.$index.']"/></td>
		</tr>
		';	
	$index++;
}

?>

</table>
	<?= Html::submitButton('Сохранить', ['class' => 'reg-button'])?>
<?php
} else {
	echo "Нет документов";
}
?>

<h2> Добавить документы</h2>

    <?= $form->field($model, 'files[]')->fileInput(['multiple' => true]) -> label(false) ?> 
 
<?php ActiveForm::end() ?>



<?php
$script = <<< JS
	$('#address-files-form input[type=file]').change(function(){		
		$('#address-files-form').submit();
	});

JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>

