<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\components\Search_promoWidget;
use frontend\models\Metro;
use frontend\models\Okrug;
use frontend\models\Nalog;
use frontend\models\User;
use frontend\models\ServicePrice;
use frontend\components\RelatedAddressWidget;
use frontend\models\PhotoCalendarSearch;
use frontend\models\PhotoCalendar;


$this->title = 'Аренда юридического адреса';

?>
<?php 

	$list = [
				'label'     => 'адреса',
                'url'       =>  ['/admin/address'],
                'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
			];

			
	$this->params['breadcrumbs'] = 
		[
			$list
			,
			[
				'label' => $model -> address,
			],
		
		];
        ?>

		<div class="container">
			<div class="row">
			
                <?php //var_dump($model -> hasOption('new')); ?>
				<div class="col-lg-8 col-md-9 col-lg-offset-1 col-sm-offset-0 col-md-offset-0 col-sm-12 address-one">
                    <div class="col-md-12">
                    <div class="all-addresses-block">
					<?php if (!$model -> enabled):?>
					<div class="add-header row">
						<div class="alarm">
							<div class="caption">Внимание! Адрес удален.</div>												
							<div>Возможно восстановление адреса. Статус адреса также будет восстановлен в тот, в котором находился адрес при удалении.</div>
							<a href="<?=Url::to(['/admin/address/restore', 'id' => $model -> id])?>" style="margin-top: 15px" class="reg-button">Восстановить адрес</a>
						</div>
						
					</div>					
					<?php endif ?>
					
					
					
						<div class="alarm">
							<div class="caption">Внимание! На этот адрес требуется визит фотографа.</div>																										
							<div>
					<?php		
							$searchModel = new PhotoCalendarSearch();
							$visit = $searchModel -> search([]) -> query -> andFilterWhere(['address_id' => $model -> id]) -> one();							
							if ($visit) {
					?>		
							<div>
							Дата визита: <?=$visit -> date?> 
							</div><div>
							Интервал времени для визита:<?=ArrayHelper::getValue(PhotoCalendar::getIntervals(), $visit -> interval)?>
							</div><div>
							Телефон владельца:<?=$model -> owner -> phone?><br>
							</div>
					<?php 	}?>							
							</div>
						</div>
					
						
					
					<div class="add-header row">
						<div class="col-md-7 col-lg-8">
							<div class="flex al">
								<?php if ($model -> hasOption('new')): ?>
								<div class="new-button al">
									new
								</div>
								<?php endif?>
								
								<h1 class="address-detail-head">
									<?=Yii::$app->controller -> h1?Yii::$app->controller -> h1:$model -> address?>
								</h1>
								
							</div>
						</div>
						<div class="col-md-3 col-md-offset-1 no-padding-d">
							<div class="icons-div flex al">
								
								<?=$model -> getOptions('<a title="%name%">%icon%</a>')?>
								
							</div>



						</div>
					</div>
					<div class="address-text row">
						<div class="col-md-8 col-lg-7">
							<div class="flex address-first-row">
								<div class="address-text-district">								
									<?=$model -> okrug_id?Html::a($model -> okrug_view, Url::to(['/addresses/okrug/'.$model -> okrug->alias]), ['class' => "card-filter"]):'-'?>
								</div>
								<div class="middot">
									&middot;
								</div>
								<div class="address-text-tax">								
									<?=$model -> nalog_id?Html::a($model -> nalog_view, Url::to(['/addresses/nalog/'.$model -> nalog -> alias]), ['class' => "card-filter"]):'-'?>
                                    

                                </div>


								<div class="middot">
									&middot;
								</div>
								<div class="address-text-area">
									<div class="flex">
									<?=$model -> square?$model -> square." м <sup>2</sup>":''?>
									</div>
								</div>
							</div>
							<div class="flex second-row-address-info">
								<div class="address-text-index">
									<span>индекс</span>
								</div>
								<div class="address-text-index-value">
                                    <?=$model -> adr_index?>
								</div>
								<div class="address-text-subway">
									<div class="flex">
										<span>метро</span>
									</div>
								</div>
								<div class="address-text-subway-value">								
									<?=$model -> metro_id?Html::a($model -> metro_view, Url::to(['/addresses/metro/'.$model -> metro->alias]), ['class' => "card-filter"]):'-'?>
								</div>
							</div>
						</div>

					</div>
					
					<a title="Изменить данные адреса" class="reg-button" href="<?=Url::to(['admin/address/update', 'id' => $model->id])?>">Изменить</a>						
					
                    <div class="row">
						<?=\frontend\components\ImagesWidget::widget([
							'images' => $model -> photos_as_array
						]);?>
                    </div>
                    <div class="clear"></div>
					<div class="row">
						<div class="col-md-11">
							<div class="address-prelocation-info">
								<?=$model -> info?>
							</div>
                            <?php $latlng = explode(',', $model['gps']); ?>
							<div id="map" data-lat="<?=trim($latlng[0])?>" data-lng="<?=@trim($latlng[1])?>"></div>
							<div class="address-postlocation-info">

								<?=$model -> seo_text?>
								
							</div>

						</div>

					</div>
					<div class="row address-owner-info address-full-info">
						<div class="col-md-8 col-md-offset-1">
							<div class="add-owner labels">владелец адреса</div>
						</div>
					</div>
					<div class="row address-owner-info">
                        <div class="col-md-12 flex-all">
							<div class="icon">
                            	<img class="pi-icon" src="/img/user.png" alt="some">
                            </div>
                            
							<div class="owner-name">
                                <?=Html::a($model -> owner -> fullname, Url::to(['user/view', 'id' => $model -> owner_id]), ['title' => 'Перейти на карточку владельца'])?>
                            </div>
                        </div>

					</div>
		
					
					
					<div class="row address-owner-title">
						<div class="col-md-8 col-md-offset-1">
							<div class="add-owner labels">Подписка</div>
						</div>
					</div>
					<div class="row ">
                        <div class="col-md-12 address-owner-info">
							<div class="icon">
								<!-- <img class="pi-icon" src="/img/banknote.png" alt="some"> -->
							</div>
							<div class="flexAll column address-extra-content">
								Подписка на публикацию адреса оформлена до <?=$model -> subscribe_to_view?>
							</div>
						</div>
					</div>
					
					<div class="row address-owner-title">
						<div class="col-md-8 col-md-offset-1">
							<div class="add-owner labels">цены</div>
						</div>
					</div>
					<div class="row ">
                        <div class="col-md-12 address-owner-info">
                        <div class="icon">
                            <img class="pi-icon" src="/img/banknote.png" alt="some">
                        </div>
                        <div class="flexAll column address-extra-content">
						<?php if ($model -> price6 > 0): ?>
                            <div class="flex-all">

                                <div class="service-price">
                                    <?=round($model -> price6)?> <i class="fa fa-rub" aria-hidden="true"></i>
                                </div>
                                <div class="service-name">
                                    Аренда адреса на 6 месяцев
                                </div>
                            </div>
						<?php endif ?>	
						<?php if ($model -> price11 > 0): ?>
                            <div class="flex-all">
                                <div class="service-price">
                                    <?=round($model -> price11)?> <i class="fa fa-rub" aria-hidden="true"></i>
                                </div>
                                <div class="service-name">
                                    Аренда адреса на 11 месяцев
                                </div>
                            </div>
						<?php endif ?>	

<?php


$modelServiceList = $model -> serviceList -> all();
$modelServices = [];
$modelAddServices = [];

foreach($modelServiceList as $service) {
	
	//echo $service -> service_name_view.' - '.$service -> getPriceValue()."<br>";
	if (!$service -> getPriceValue()) continue; // Если цена не указана, то услугу не отображаем
	$serviceName = $service -> service_name;
	//$option = $serviceName? $serviceName -> option:null;
	//$icon = $option?$option -> getCaption('<a title="Связана с опцией %name%">%icon%</a>'):'';	
	if ($service -> stype == ServicePrice::STYPE_ONCE) {
		$priceValue = (int)$service -> price.'<i class="fa fa-rub" aria-hidden="true"></i>';	
	} else {
		$priceValue = '';
		$price6 = (int)$service -> price6;
		$price11 = (int)$service -> price11;
		if ($price6 == $price11) {
			$priceValue = $price6.' <i class="fa fa-rub" aria-hidden="true"></i> в месяц';
		} else {
			if (($price6) > 0) {
				$priceValue = $price6.' <i class="fa fa-rub" aria-hidden="true"></i> в месяц при аренде на 6 месяцев';
			};
			
			if ($price6 * $price11) $priceValue = $priceValue.'<br>';
			if ($price11 > 0) {
				$priceValue = $priceValue.$price11.' <i class="fa fa-rub" aria-hidden="true"></i> в месяц при аренде на 11 месяцев';
			};
											
		}
		
	};
	
	
	 $srv = [
										//'label' => '<span class="empty-icon">'.$icon.'</span>'.$service -> getCaption('%name%'),
										'label' => $service -> getCaption('%name%'),
										
										
										'price' => 	$priceValue,
										'stype' => $service -> stype,
									];
	if ($serviceName) $modelServices[$service -> id] = $srv; else $modelAddServices[$service -> id] = $srv;


}

?>

<?php 			foreach($modelServices as $id => $data): ?>
                            <div class="flex-all">
                                <div class="service-price">
                                    <?=$data['price']?>
									</div>
                                <div class="service-name">
                                    <?=$data['label']?>
                                </div>

                            </div>
<?php 			endforeach?>

                        </div>
                        </div>
					</div>





<?php			if (count($modelAddServices)):	?>
					<div class="row address-owner-title">
						<div class="col-md-8 col-md-offset-1">
							<div class="add-owner labels">дополнительные услуги</div>
						</div>
					</div>
                    <div class="row">
                        <div class="col-md-12 address-owner-info">

                        <div class="icon">
                            <img class="pi-icon" src="/img/pin.png" alt="some">
                        </div>
                        <div class="address-extra-content">
<?php 			foreach($modelAddServices as $id => $data): ?>
                            <div class="flex-all">
                                <div class="service-price">
                                    <?=$data['price']?>
                                </div>
                                <div class="service-name">
                                    <?=$data['label']?>
                                </div>

                            </div>
<?php 			endforeach?>

                        </div>

                        </div>
                    </div>

<?php			endif; ?>


					
				</div>
			</div>
		</div>


<?php
    $this->registerJsFile("/js/map.js", array("depends"=>[], "position"=>\yii\web\view::POS_END));
?>


