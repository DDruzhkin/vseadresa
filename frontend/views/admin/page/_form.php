<?php

if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Page;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'parent_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Page::getMeta('parent_id', 'label'), 'items' => $model -> getRelatedListValues('parent_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('page/update/%parent_id%')]).'">'.Icon::show('link').'</a>'.Page::getMeta('parent_id', 'comment'), 'options'=>[]],
						'created_at' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Page::getMeta('created_at', 'label'), 'hint'=>''.Page::getMeta('created_at', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		

						'h1' => ['type'=>Form::INPUT_TEXT, 'label'=>Page::getMeta('h1', 'label'), 'hint'=>''.Page::getMeta('h1', 'comment'), 'options'=>[]],
						'label' => ['type'=>Form::INPUT_TEXT, 'label'=>Page::getMeta('label', 'label'), 'hint'=>''.Page::getMeta('label', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'content' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Page::getMeta('content', 'label'), 'hint'=>''.Page::getMeta('content', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'menu_as_array' => ['type'=>Form::INPUT_CHECKBOX_LIST, 'label'=>Page::getMeta('menu', 'label'), 'items' => Page::getList('menu'), 'hint'=>''.Page::getMeta('menu', 'comment'), 'options'=>['multiple' => true]],
						'title' => ['type'=>Form::INPUT_TEXT, 'label'=>Page::getMeta('title', 'label'), 'hint'=>''.Page::getMeta('title', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'meta_description' => ['type'=>Form::INPUT_TEXT, 'label'=>Page::getMeta('meta_description', 'label'), 'hint'=>''.Page::getMeta('meta_description', 'comment'), 'options'=>[]],
		
				],
			],
				
		],		
	],
		
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'seo_text' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Page::getMeta('seo_text', 'label'), 'hint'=>''.Page::getMeta('seo_text', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'slug' => ['type'=>Form::INPUT_TEXT, 'label'=>Page::getMeta('slug', 'label'), 'hint'=>''.Page::getMeta('slug', 'comment'), 'options'=>[]],
						'template' => ['type'=>Form::INPUT_TEXT, 'label'=>Page::getMeta('template', 'label'), 'hint'=>''.Page::getMeta('template', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'type' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Page::getMeta('type', 'label'), 'items' => Page::getList('type'), 'hint'=>''.Page::getMeta('type', 'comment'), 'options'=>[]],
						'visible' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>Page::getMeta('visible', 'label'), 'hint'=>''.Page::getMeta('visible', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			

						'enabled' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>Page::getMeta('enabled', 'label'), 'hint'=>''.Page::getMeta('enabled', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'reg-button']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">
<script src="https://cdn.ckeditor.com/4.9.1/full-all/ckeditor.js"></script>
<style>
.field-page-content label {
	display: none;
}

</style>

<?php



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
<script>
$(document).ready(function() {	
    CKEDITOR.replace('Page[content]');	
});

</script>