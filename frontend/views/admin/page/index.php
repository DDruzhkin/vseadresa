<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Page;
use common\models\Entity;

use common\models\PageSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
$this -> context -> h1 = $this->title;


?>
<div class="page-index">
    
	<div class="content">
		Страницы могут быть трех типов:
		<ul>
			<li><?=Icon::show('folder-open-o')?> - рубрика. Теоретических ограничений на вложенность нет. Но интерфейс портала предполагает, что должно быть не более 2-х уровней вложенности</li>
			<li><?=Icon::show('file-o')?> - статическая страница.</li>
			<li><?=Icon::show('file-text-o')?> - пост. Как статическая страница, но для нее будет отображаться еще и дата. Например, в новостях лучше использовать этот тип страницы.</li>
		</ul>
		
		Раздел не возможно удалить ( <?=Icon::show('times')?>), если он содержит хотя бы одну статью или раздел. Поэтому для удаления раздела необходимо удалить все его содержимое.
		
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    

    <div class="controls">
       <?= Html::a('Добавить', ['create'], ['class' => 'reg-button']) ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,

		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/


			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => $data -> getAdminMenuItems(),
							],
						],
					]);
				}					
			],
			
            ['attribute' => 'created_at',
				'label' => Page::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],

            ['attribute' => 'h1',
				'label' => Page::getMeta('h1', 'label'),
				'contentOptions' => ['class' => 'col-h1 '],
				'filterOptions' => ['class' => 'col-h1'],
				'headerOptions' => ['class' => 'col-h1 '],				
				'footerOptions' => ['class' => 'col-h1 '],
				'value' => function($data) {
					$pageTypeIcons = ['rubric' => 'folder-open-o', 'static' => 'file-o', 'post' => 'file-text-o'];
					
					$icon = isset($pageTypeIcons[$data -> type])?$pageTypeIcons[$data -> type]:'question';
					return '<span class="icon">'.($icon?Icon::show($icon).'':'').'</span> '.Html::a($data -> h1, ['/admin/page/update/', 'id' => $data -> id], ['title' => 'Изменить элемент']);;
				},
				'format' => 'raw'
				
				
				
			],			
            ['attribute' => 'parent_id',
				'label' => Page::getMeta('parent_id', 'label'),
				'contentOptions' => ['class' => 'col-parent_id '],
				'filterOptions' => ['class' => 'col-parent_id'],
				'headerOptions' => ['class' => 'col-parent_id '],				
				'footerOptions' => ['class' => 'col-parent_id '],
				'filter' => Page::getExistsValues('parent_id'),	
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['parent_id_view'],[Inflector::camel2id('Page').'/update/'.$data['parent_id']]);
				return $value;
			},
				'format' => 'raw',
			],


            ['attribute' => 'label',
				'label' => Page::getMeta('label', 'label'),
				'contentOptions' => ['class' => 'col-label hidden '],
				'filterOptions' => ['class' => 'col-label hidden'],
				'headerOptions' => ['class' => 'col-label hidden '],				
				'footerOptions' => ['class' => 'col-label hidden '],
				
				
				
			],
            ['attribute' => 'content',
				'label' => Page::getMeta('content', 'label'),
				'contentOptions' => ['class' => 'col-content hidden '],
				'filterOptions' => ['class' => 'col-content hidden'],
				'headerOptions' => ['class' => 'col-content hidden '],				
				'footerOptions' => ['class' => 'col-content hidden '],
				
				
				
			],
/*						
            ['attribute' => 'menu',
				'label' => Page::getMeta('menu', 'label'),
				'contentOptions' => ['class' => 'col-menu '],
				'filterOptions' => ['class' => 'col-menu'],
				'headerOptions' => ['class' => 'col-menu '],				
				'footerOptions' => ['class' => 'col-menu '],
				'filter' => Page::getList('menu'),	
				'value' => function($data) {
					return $data['menu']?$data['menu']:'-';
				}			
				
			],

            ['attribute' => 'title',
				'label' => Page::getMeta('title', 'label'),
				'contentOptions' => ['class' => 'col-title hidden '],
				'filterOptions' => ['class' => 'col-title hidden'],
				'headerOptions' => ['class' => 'col-title hidden '],				
				'footerOptions' => ['class' => 'col-title hidden '],
				
				
				
			],
            ['attribute' => 'meta_description',
				'label' => Page::getMeta('meta_description', 'label'),
				'contentOptions' => ['class' => 'col-meta_description hidden '],
				'filterOptions' => ['class' => 'col-meta_description hidden'],
				'headerOptions' => ['class' => 'col-meta_description hidden '],				
				'footerOptions' => ['class' => 'col-meta_description hidden '],
				
				
				
			],
            ['attribute' => 'seo_text',
				'label' => Page::getMeta('seo_text', 'label'),
				'contentOptions' => ['class' => 'col-seo_text hidden '],
				'filterOptions' => ['class' => 'col-seo_text hidden'],
				'headerOptions' => ['class' => 'col-seo_text hidden '],				
				'footerOptions' => ['class' => 'col-seo_text hidden '],
				
				
				
			],
*/
/*			
            ['attribute' => 'slug',
				'label' => Page::getMeta('slug', 'label'),
				'contentOptions' => ['class' => 'col-slug '],
				'filterOptions' => ['class' => 'col-slug'],
				'headerOptions' => ['class' => 'col-slug '],				
				'footerOptions' => ['class' => 'col-slug '],
				'contentOptions' => ['style' => 'width: 300px; max-width: 400px; word-wrap: break-word;'],
				'value' => function($data) {
					return $data['slug']?$data['slug']:'';
				}
		
				
			],
*/			
			
/*			
            ['attribute' => 'template',
				'label' => Page::getMeta('template', 'label'),
				'contentOptions' => ['class' => 'col-template hidden '],
				'filterOptions' => ['class' => 'col-template hidden'],
				'headerOptions' => ['class' => 'col-template hidden '],				
				'footerOptions' => ['class' => 'col-template hidden '],
				
				
				
			],
*/
/*			
            ['attribute' => 'type',
				'label' => Page::getMeta('type', 'label'),
				'contentOptions' => ['class' => 'col-type '],
				'filterOptions' => ['class' => 'col-type'],
				'headerOptions' => ['class' => 'col-type '],				
				'footerOptions' => ['class' => 'col-type '],
				'filter' => Page::getList('type'),	
				
				
			],
*/ 			
			
/*			
            ['attribute' => 'visible',
				'label' => Page::getMeta('visible', 'label'),
				'contentOptions' => ['class' => 'col-visible hidden '],
				'filterOptions' => ['class' => 'col-visible hidden'],
				'headerOptions' => ['class' => 'col-visible hidden '],				
				'footerOptions' => ['class' => 'col-visible hidden '],
				
				
				
			],

            ['attribute' => 'enabled',
				'label' => Page::getMeta('enabled', 'label'),
				'contentOptions' => ['class' => 'col-enabled hidden '],
				'filterOptions' => ['class' => 'col-enabled hidden'],
				'headerOptions' => ['class' => 'col-enabled hidden '],				
				'footerOptions' => ['class' => 'col-enabled hidden '],
				
				
				
			],
			
*/			

			[
                'class' => 'yii\grid\ActionColumn',

                'buttons' => [
                    'delete' => function ($url, $model) {

                        return ($model->deletable) ?           


                            Html::a(
                                Icon::show('times'),
                                Url::toRoute(['delete', 'id' => $model->id]),
                                
                                [
                                    'title' => 'Удалить элемент',
                                    'class' => 'delete',
                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                    'data-params' => ['id' => $model->id],

                                ]
                            ) : '';

                    }
                ],
                'template' => '{delete}',


            ],	
			
        ],
    ]); ?>
</div>
