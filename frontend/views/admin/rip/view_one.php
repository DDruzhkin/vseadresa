<?php
use kartik\icons\Icon;
use yii\helpers\Url;
use yii\helpers\Html;

$isDefault = ($model -> id == $model -> user -> default_profile_id);
//$editable = ($isDefault || !$model -> user -> active) && $model -> editable;

$template = '
<div class="card">	
<div class="header">'
.($isDefault?
	'<a title="Основные реквизиты">'.Icon::show('star').'</a>'	
	:(
		$editable
			?'<a title="Сделать основынми реквизитами" href="'.URL::to(['/admin/user/default-profile/', 'id' => $model -> user -> id, 'rid' => $model -> id]).'">'.Icon::show('star-o').'</a>'
			:Icon::show('star-o')
	)
)
.'<span class="caption">ИП</span>'

//.'<a title="Изменить реквизиты" style="float:right" href="'.URL::to(['/'.$model -> modelId().'/update/'.$model -> id]).'">'.Icon::show('edit').'</a>'
.($editable?Html::a(Icon::show('edit'), URL::to(['/admin/r/update/', 'id' => $model -> id]), ['title' => "Изменить реквизиты", 'style' => "float:right"]):'').'
</div>
<div class="value title">%form% %fname% %mname% %lname%</div>
<div class="undervalue">Полное название ИП</div>
<div class="value>
</div><div class="undervalue"></div>
'
.(($model -> inn)?'<div class="value">%inn%</div><div class="undervalue">ИНН предпринимателя</div>':'')
.(($model -> kpp)?'<div class="value">%kpp%</div><div class="undervalue">КПП</div>':'')
.(($model -> ogrnip)?'<div class="value">%ogrnip%</div><div class="undervalue">ОГРНИП</div>':'')
.(($model -> pasport_number || $model -> pasport_date || $model -> pasport_organ)?'<div class="value">%pasport_number% ':'')
.(($model -> pasport_date || $model -> pasport_organ)?'выдан %pasport_date% %pasport_organ%':'')
.(($model -> pasport_number || $model -> pasport_date || $model -> pasport_organ)?'</div><div class="undervalue">паспорт гражданина РФ</div>':'')
.(($model -> address)?'<div class="value">%address%</div><div class="undervalue">адрес регистрации</div>':'')
.(($model -> bank_bik)?'<div class="value">%bank_bik%</div><div class="undervalue">БИК банка</div>':'')
.(($model -> account)?'<div class="value">%account%</div><div class="undervalue">номер счета</div>':'')
.(($model -> account && $model -> account_owner)?'<div class="value">%account_owner%</div><div class="undervalue">владелец счета</div>':'')
.'</div>	
';

echo $model -> generateByTemplate($template, $model -> attributes);
?>	