<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\models\Address;
use frontend\models\Order;
use frontend\models\User;
use common\models\Entity;
use common\models\OrderSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
$this -> context -> h1 = $this->title;
?>
<div class="order-index">

	<div class="content">
	<?php
	
	switch ($param) {
		case 'address_id': {
			$model = Address::findone([$param_value]);
			if($model) {
				echo 'Заказы на Адрес '.Html::a($model -> address, ['admin/address/update', 'id' => $model -> id]).'<br>';
			};
			break;
		};
		
		case 'owner_id': {
			$model = User::findone([$param_value]);
			if($model) {
				echo 'Заказы Владельцу '.Html::a($model -> fullname, ['admin/user/edit', 'id' => $model -> id]).'<br>';
			};
			break;			
		}
		
		case 'customer_id': {
			$model = User::findone([$param_value]);
			if($model) {
				echo 'Заказы от Заказчика '.Html::a($model -> fullname, ['admin/user/edit', 'id' => $model -> id]).'<br>';
			};
			break;
		}
	}
	
	?>
	</div>
		
	
	<hr/>
	
	<?php
			if (Yii::$app->request->get('OrderSearch')) {
				echo Html::a(Icon::show('times'), '#', ['class' => 'cancel-filter', 'title' => 'Очистить фильтр', 'style' => 'float: right']);
			}
		?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],
        'filterModel' => $searchModel,
        'columns' => [
            
						
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					$items = $data -> getAdminMenuItems();
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => $items,
							],
						],
					]);
				}					
			],



            ['attribute' => 'created_at',
				'label' => Order::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at '],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at '],				
				'footerOptions' => ['class' => 'col-created_at '],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				'value' => function($data) {
					return Yii::$app -> formatter -> asDate($data -> created_at);
				}
				
				
			],
            ['attribute' => 'number',
				'label' => Order::getMeta('number', 'label'),
				'contentOptions' => ['class' => 'col-number '],
				'filterOptions' => ['class' => 'col-number'],
				'headerOptions' => ['class' => 'col-number '],				
				'footerOptions' => ['class' => 'col-number '],
				'format' => 'raw',
				'value' => 
				function($data)
				{ 
					$value = Html::a($data['number'],['admin/order/view/', 'id' => $data['id']]);
					
					return $value;
				},
				
				
			],
            ['attribute' => 'status_id',
				'label' => Order::getMeta('status_id', 'label'),
				'contentOptions' => ['class' => 'col-status_id '],
				'filterOptions' => ['class' => 'col-status_id'],
				'headerOptions' => ['class' => 'col-status_id '],				
				'footerOptions' => ['class' => 'col-status_id '],
				'filter' => Order::getExistsValues('status_id'),	
				'value' => function($data){ return $data['status_id_view'];},
				
			],
/*			
            ['attribute' => 'duration',
				'label' => Order::getMeta('duration', 'label'),
				'contentOptions' => ['class' => 'col-duration '],
				'filterOptions' => ['class' => 'col-duration'],
				'headerOptions' => ['class' => 'col-duration '],				
				'footerOptions' => ['class' => 'col-duration '],
				'filter' => Order::getList('duration'),	
				
				
			],
*/			
            ['attribute' => 'customer_id',
				'label' => Order::getMeta('customer_id', 'label'),
				'contentOptions' => ['class' => 'col-customer_id'.($param == 'customer_id'?' hidden':'')],
				'filterOptions' => ['class' => 'col-customer_id'.($param == 'customer_id'?' hidden':'')],
				'headerOptions' => ['class' => 'col-customer_id'.($param == 'customer_id'?' hidden':'')],
				'footerOptions' => ['class' => 'col-customer_id'.($param == 'customer_id'?' hidden':'')],
				
				'value' => 
				function($data)
				{ 
					//$value = Html::a($data['customer_id_view'],[Inflector::camel2id('User').'/update/'.$data['customer_id']]);
					$value = Html::a($data -> customer -> fullname,['/admin/user/view/', 'id' => $data -> customer_id]);				
					return $value;
				},
				'format' => 'raw',
			],
            
            ['attribute' => 'address_id',
				'label' => Order::getMeta('address_id', 'label'),
				'contentOptions' => ['class' => 'col-address_id'.($param == 'address_id'?' hidden':'')],
				'filterOptions' => ['class' => 'col-address_id'.($param == 'address_id'?' hidden':'')],
				'headerOptions' => ['class' => 'col-address_id'.($param == 'address_id'?' hidden':'')],
				'footerOptions' => ['class' => 'col-address_id'.($param == 'address_id'?' hidden':'')],
				
				'value' => 
				function($data)
				{ 
					//$value = Html::a($data['address_id_view'],[Inflector::camel2id('Address').'/update/'.$data['address_id']]);
					$value = Html::a($data -> address -> address,['/admin/address/view/', 'id' => $data -> address_id]);				
					return $value;
				},
				'format' => 'raw',
			],
            ['attribute' => 'owner_id',
				'label' => Order::getMeta('owner_id', 'label'),
				'contentOptions' => ['class' => 'col-owner_id'.($param == 'owner_id'?' hidden':'')],
				'filterOptions' => ['class' => 'col-owner_id'.($param == 'owner_id'?' hidden':'')],
				'headerOptions' => ['class' => 'col-owner_id'.($param == 'owner_id'?' hidden':'')],
				'footerOptions' => ['class' => 'col-owner_id'.($param == 'owner_id'?' hidden':'')],
				
				'value' => 
				function($data)
				{ 
					//$value = Html::a($data['owner_id_view'],[Inflector::camel2id('User').'/update/'.$data['owner_id']]);
					$value = Html::a($data -> owner -> fullname,['/admin/user/view/', 'id' => $data -> owner_id]);				
					return $value;
				},
				'format' => 'raw',
			],
			
/*			
            ['attribute' => 'date_start',
				'label' => Order::getMeta('date_start', 'label'),
				'contentOptions' => ['class' => 'col-date_start hidden date'],
				'filterOptions' => ['class' => 'col-date_start hidden'],
				'headerOptions' => ['class' => 'col-date_start hidden date'],				
				'footerOptions' => ['class' => 'col-date_start hidden date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'date_start',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'date_fin',
				'label' => Order::getMeta('date_fin', 'label'),
				'contentOptions' => ['class' => 'col-date_fin hidden date'],
				'filterOptions' => ['class' => 'col-date_fin hidden'],
				'headerOptions' => ['class' => 'col-date_fin hidden date'],				
				'footerOptions' => ['class' => 'col-date_fin hidden date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'date_fin',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
			
            ['attribute' => 'address_price',
				'label' => Order::getMeta('address_price', 'label'),
				'contentOptions' => ['class' => 'col-address_price '],
				'filterOptions' => ['class' => 'col-address_price'],
				'headerOptions' => ['class' => 'col-address_price '],				
				'footerOptions' => ['class' => 'col-address_price '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'is_quick',
				'label' => Order::getMeta('is_quick', 'label'),
				'contentOptions' => ['class' => 'col-is_quick hidden '],
				'filterOptions' => ['class' => 'col-is_quick hidden'],
				'headerOptions' => ['class' => 'col-is_quick hidden '],				
				'footerOptions' => ['class' => 'col-is_quick hidden '],
				
				
				
			],
            ['attribute' => 'delivery_id',
				'label' => Order::getMeta('delivery_id', 'label'),
				'contentOptions' => ['class' => 'col-delivery_id hidden '],
				'filterOptions' => ['class' => 'col-delivery_id hidden'],
				'headerOptions' => ['class' => 'col-delivery_id hidden '],				
				'footerOptions' => ['class' => 'col-delivery_id hidden '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['delivery_id_view'],[Inflector::camel2id('Porder').'/update/'.$data['delivery_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'delivery_type_id',
				'label' => Order::getMeta('delivery_type_id', 'label'),
				'contentOptions' => ['class' => 'col-delivery_type_id hidden '],
				'filterOptions' => ['class' => 'col-delivery_type_id hidden'],
				'headerOptions' => ['class' => 'col-delivery_type_id hidden '],				
				'footerOptions' => ['class' => 'col-delivery_type_id hidden '],
				
				'value' => function($data){ return $data['delivery_type_id_view'];},
				
			],
            ['attribute' => 'delivery_contacts',
				'label' => Order::getMeta('delivery_contacts', 'label'),
				'contentOptions' => ['class' => 'col-delivery_contacts hidden '],
				'filterOptions' => ['class' => 'col-delivery_contacts hidden'],
				'headerOptions' => ['class' => 'col-delivery_contacts hidden '],				
				'footerOptions' => ['class' => 'col-delivery_contacts hidden '],
				
				
				
			],
            ['attribute' => 'delivery_address',
				'label' => Order::getMeta('delivery_address', 'label'),
				'contentOptions' => ['class' => 'col-delivery_address hidden '],
				'filterOptions' => ['class' => 'col-delivery_address hidden'],
				'headerOptions' => ['class' => 'col-delivery_address hidden '],				
				'footerOptions' => ['class' => 'col-delivery_address hidden '],
				
				
				
			],
            ['attribute' => 'guarantee_id',
				'label' => Order::getMeta('guarantee_id', 'label'),
				'contentOptions' => ['class' => 'col-guarantee_id hidden '],
				'filterOptions' => ['class' => 'col-guarantee_id hidden'],
				'headerOptions' => ['class' => 'col-guarantee_id hidden '],				
				'footerOptions' => ['class' => 'col-guarantee_id hidden '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['guarantee_id_view'],[Inflector::camel2id('Porder').'/update/'.$data['guarantee_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'payment_type',
				'label' => Order::getMeta('payment_type', 'label'),
				'contentOptions' => ['class' => 'col-payment_type '],
				'filterOptions' => ['class' => 'col-payment_type'],
				'headerOptions' => ['class' => 'col-payment_type '],				
				'footerOptions' => ['class' => 'col-payment_type '],
				'filter' => Order::getList('payment_type'),	
				
				
			],
*/			
			
            ['attribute' => 'summa',
				'label' => Order::getMeta('summa', 'label'),
				'contentOptions' => ['class' => 'col-summa '],
				'filterOptions' => ['class' => 'col-summa'],
				'headerOptions' => ['class' => 'col-summa '],				
				'footerOptions' => ['class' => 'col-summa '],
				
				
				'format' => ['decimal', 2],
			],


        ],
    ]); ?>
</div>
