<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Metro */

$this->title = 'Добавление Станции метро ' . $model->getCaption();
$this -> context -> h1 = 'Новая станция метро';
$this->params['breadcrumbs'][] = [
	'label' => \backend\models\Metro::getMeta('','title'), 
	'url' => ['index'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];

$this->params['breadcrumbs'][] = $this -> title;
?>
<div class="metro-create">
    <?= $this->render('_form', [
        'model' => $model,
		'addition' => true,
    ]) ?>

</div>
