<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Metro;
use common\models\MetroSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Список станций метро';
$this->params['breadcrumbs'][] = $this->title;
$this -> context -> h1 = $this->title;
?>
<div class="metro-index">
    
	<div class="content">

	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'reg-button']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,

		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],
        'filterModel' => $searchModel,
        'columns' => [
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => $data -> getAdminMenuItems(),
							],
						],
					]);
				}					
			],



            ['attribute' => 'alias',
				'label' => Metro::getMeta('alias', 'label'),
				'contentOptions' => ['class' => 'col-alias '],
				'filterOptions' => ['class' => 'col-alias'],
				'headerOptions' => ['class' => 'col-alias '],				
				'footerOptions' => ['class' => 'col-alias '],
				
				
				
			],
            ['attribute' => 'name',
				'label' => Metro::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				
				
				
			],
            ['attribute' => 'title',
				'label' => Metro::getMeta('title', 'label'),
				'contentOptions' => ['class' => 'col-title hidden '],
				'filterOptions' => ['class' => 'col-title hidden'],
				'headerOptions' => ['class' => 'col-title hidden '],				
				'footerOptions' => ['class' => 'col-title hidden '],
				
				
				
			],
            ['attribute' => 'meta_descrition',
				'label' => Metro::getMeta('meta_descrition', 'label'),
				'contentOptions' => ['class' => 'col-meta_descrition hidden '],
				'filterOptions' => ['class' => 'col-meta_descrition hidden'],
				'headerOptions' => ['class' => 'col-meta_descrition hidden '],				
				'footerOptions' => ['class' => 'col-meta_descrition hidden '],
				
				
				
			],
            ['attribute' => 'seo_text',
				'label' => Metro::getMeta('seo_text', 'label'),
				'contentOptions' => ['class' => 'col-seo_text hidden '],
				'filterOptions' => ['class' => 'col-seo_text hidden'],
				'headerOptions' => ['class' => 'col-seo_text hidden '],				
				'footerOptions' => ['class' => 'col-seo_text hidden '],
				
				
				
			],
            ['attribute' => 'h1',
				'label' => Metro::getMeta('h1', 'label'),
				'contentOptions' => ['class' => 'col-h1 hidden '],
				'filterOptions' => ['class' => 'col-h1 hidden'],
				'headerOptions' => ['class' => 'col-h1 hidden '],				
				'footerOptions' => ['class' => 'col-h1 hidden '],
				
				
				
			],

			[
                'class' => 'yii\grid\ActionColumn',

                'buttons' => [
                    'delete' => function ($url, $model) {

                        return ($model->deletable) ?           


                            Html::a(
                                Icon::show('times'),
                                Url::toRoute(['delete', 'id' => $model->id]),
                                
                                [
                                    'title' => 'Удалить элемент',
                                    'class' => 'delete',
                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент',
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                    'data-params' => ['id' => $model->id],

                                ]
                            ) : '';

                    }
                ],
                'template' => '{delete}',


            ],
        ],
    ]); ?>
</div>
