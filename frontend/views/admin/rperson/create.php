<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Rperson */

$this->title = 'Добавление реквизитов физлица';
$this->params['breadcrumbs'][] = $this->title;
$model->integrate();
?>

<?= $this->render('@frontend/views/rperson/_form', [
    'model' => $model,
    'admin' => 1
]) ?>


