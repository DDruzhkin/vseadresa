<?php
if (YII_DEBUG) $startTime = microtime(true);

use backend\models\Rperson;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$controls = [
    'created_at_view' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('created_at_view', 'label'), 'hint' => '' . Rperson::getMeta('created_at_view', 'comment'), 'options' => ['readonly' => 'true',]],
    'inn' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rperson::getMeta('inn', 'label'), 'hint' => '' . Rperson::getMeta('inn', 'comment'), 'options' => ['mask' => 999999999999,]],
    'fname' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('fname', 'label'), 'hint' => '' . Rperson::getMeta('fname', 'comment'), 'options' => []],
    'mname' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('mname', 'label'), 'hint' => '' . Rperson::getMeta('mname', 'comment'), 'options' => []],
    'lname' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('lname', 'label'), 'hint' => '' . Rperson::getMeta('lname', 'comment'), 'options' => []],
    'bdate' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\date\DatePicker', 'label' => Rperson::getMeta('bdate', 'label'), 'hint' => '' . Rperson::getMeta('bdate', 'comment'), 'options' => ['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose' => true, 'format' => 'dd.mm.yyyy'],]],
    'pasport_number' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rperson::getMeta('pasport_number', 'label'), 'hint' => '' . Rperson::getMeta('pasport_number', 'comment'), 'options' => ['mask' => Rperson::getMaskByAlias("pasport"),]],
    'pasport_date' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\date\DatePicker', 'label' => Rperson::getMeta('pasport_date', 'label'), 'hint' => '' . Rperson::getMeta('pasport_date', 'comment'), 'options' => ['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND,
        'pluginOptions' => [
            'todayHighlight' => true,
            'autoclose' => true,
            'endDate' => '0d',
            'format' => 'dd.mm.yyyy'

        ],]],
    'pasport_organ' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('pasport_organ', 'label'), 'hint' => '' . Rperson::getMeta('pasport_organ', 'comment'), 'options' => []],
    'address' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('address', 'label'), 'hint' => '' . Rperson::getMeta('address', 'comment'), 'options' => []],
    'bank_bik' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rperson::getMeta('bank_bik', 'label'), 'hint' => '' . Rperson::getMeta('bank_bik', 'comment'), 'options' => ['mask' => 999999999,]],
    'account' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rperson::getMeta('account', 'label'), 'hint' => '' . Rperson::getMeta('account', 'comment'), 'options' => ['mask' => Rperson::getMaskByAlias("account"),]],
    'account_owner' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('account_owner', 'label'), 'hint' => '' . Rperson::getMeta('account_owner', 'comment'), 'options' => []],
    'bank_name' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('bank_name', 'label'), 'hint' => '' . Rperson::getMeta('bank_name', 'comment'), 'options' => []],
    'bank_cor' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rperson::getMeta('bank_cor', 'label'), 'hint' => '' . Rperson::getMeta('bank_cor', 'comment'), 'options' => ['mask' => '99999999999999999999',]],
    'bank_name' => ['type' => Form::INPUT_TEXT, 'label' => Rperson::getMeta('bank_name', 'label'), 'hint' => '' . Rperson::getMeta('bank_name', 'comment'), 'options' => []],
    'bank_cor' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\yii\widgets\MaskedInput', 'label' => Rperson::getMeta('bank_cor', 'label'), 'hint' => '' . Rperson::getMeta('bank_cor', 'comment'), 'options' => ['mask' => '99999999999999999999',]],
    'sign' => ['type' => Form::INPUT_FILE, 'label' => Rip::getMeta('sign', 'label'), 'hint' => '' . Rip::getMeta('sign', 'comment'), 'options' => ['accept' => "image/png"]],
    'stamp' => ['type' => Form::INPUT_FILE, 'label' => Rip::getMeta('stamp', 'label'), 'hint' => '' . Rip::getMeta('stamp', 'comment'), 'options' => ['accept' => "image/png"]],
    'sign_sample' => ['type' => Form::INPUT_RAW, 'value' => '<div><img class="img_sample" ' . (isset($model->sign) ? 'src="/files/images/requisites/' . $model->sign . '"' : '') . '  id="sign_sample"></div>'],
    'stamp_sample' => ['type' => Form::INPUT_RAW, 'value' => '<div><img class="img_sample" ' . (isset($model->stamp) ? 'src="/files/images/requisites/' . $model->stamp . '"' : '') . ' id="stamp_sample"></div>'],

    'actions' => ['type' => Form::INPUT_RAW, 'value' => '<div style="text-align: right; margin-top: 20px">' . Html::submitButton('Изменить данные', ['class' => 'reg-button']) . '</div>'],
];

$layout = [

    [
        'attributes' => [
            'header' => ['type' => Form::INPUT_RAW, 'value' => '<h2>Паспортные данные</h2>'],
        ]
    ], [
        'attributes' => [
            'fname' => $controls['fname'],
        ],
    ], [
        'attributes' => [
            'mname' => $controls['mname'],
        ],
    ], [
        'attributes' => [
            'lname' => $controls['lname'],
        ],
    ], [
        'attributes' => [
            'inn' => $controls['inn'],
        ],
    ], [
        'attributes' => [
            'pasport_number' => $controls['pasport_number'],
        ],
    ], [
        'attributes' => [
            'pasport_date' => $controls['pasport_date'],
        ],
    ], [
        'attributes' => [
            'pasport_organ' => $controls['pasport_organ'],
        ],
    ], [
        'attributes' => [
            'address' => $controls['address'],
        ],
    ], [
        'attributes' => [
            'header' => ['type' => Form::INPUT_RAW, 'value' => '<h2>Реквизиты банковского счета</h2>'],
        ]
    ], [
        'attributes' => [
            'bank_bik' => $controls['bank_bik'],
        ],
    ], [
        'attributes' => [
            'bank_name' => $controls['bank_name'],
        ],
    ], [
        'attributes' => [
            'bank_cor' => $controls['bank_cor'],
        ],
    ], [
        'attributes' => [
            'account' => $controls['account'],
        ],
    ], [
        'attributes' => [
            'account_owner' => $controls['account_owner'],
        ],
    ], [
        'attributes' => [
            'sign' => $controls['sign'],
            'stamp' => $controls['stamp'],
        ],
    ], [
        'attributes' => [
            'sign' => $controls['sign_sample'],
            'stamp' => $controls['stamp_sample'],
        ],
    ], [
        'attributes' => [
            'actions' => $controls['actions'],
        ],
    ],

];


/* @var $this yii\web\View */
/* @var $model backend\models\Rperson */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$form = ActiveForm::begin();
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $layout
]);

ActiveForm::end();

if (YII_DEBUG) {
    $finTime = microtime(true);
    $delta = $finTime - $startTime;
    echo $delta . ' сек.';
}
?>

</div>
<script>
    $(document).ready(function () {
        function readURL(input, type) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#' + type + '_sample').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#rip-sign").change(function () {
            readURL(this, 'sign');
        });
        $("#rip-stamp").change(function () {
            readURL(this, 'stamp');
        });
    })
</script>