<?php
if (YII_DEBUG) $startTime = microtime(true);

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model backend\models\Faq */

$templateList = $model -> list;


$this->title = 'Редактирование  шаблона '.$templateList[$model->name];
$this -> context -> h1 = $this->title;

$this->params['breadcrumbs'][] = [
	'label' => 'Настройка уведомлений', 
	'url' => ['/admin/email-template/'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];
$this->params['breadcrumbs'][] = $this -> title;


?>

<style>
.field-doctemplate-content label {
	display: none;
}

#admin .form-group textarea#doctemplate-content {
	height: 600px;

}


.cke_contents { height: 600px !important; }

</style>
<div class="doc-template-update">

<script src="https://cdn.ckeditor.com/4.9.1/full-all/ckeditor.js"></script>



<div class="email-template-form">
<select id="template-selector">
<?php
	foreach($templateList as $name => $title) {
		if ($name == $model -> name)  {
			echo Html::tag('option', $title, ['value' => $name, 'selected' => 'selected']);
		} else {
			echo Html::tag('option', $title, ['value' => $name]);
		}
	}
?>
</select>
    <?php $form = ActiveForm::begin(['id' => 'doc-template-update-form']); ?>
		
    <?= $form->field($model, 'content')->textarea(['autofocus' => true, ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'save'), ['class' => 'reg-button', 'style' => 'margin-top: 20px']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>


</div>
<script>
$(document).ready(function(){
	CKEDITOR.replace('DocTemplate[content]');
	$("#template-selector").change(function() {
		document.location.href="<?=Url::to(['/admin/doc-template'])?>?name=" + $(this).val();
	});
});
</script>