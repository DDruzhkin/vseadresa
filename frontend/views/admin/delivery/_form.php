<?php

if (YII_DEBUG) $startTime = microtime(true);

use backend\models\Okrug;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Delivery;

$isNew = !isset($model) || is_null($model->id);

$readonly = !$isNew;


$data = [
    [
        'attributes' => [
            'address_detail' => [
                'columns' => 2,
                'attributes' => [
                    'alias' => ['type' => Form::INPUT_TEXT, 'label' => Delivery::getMeta('alias', 'label'), 'hint' => '' . Delivery::getMeta('alias', 'comment'), 'options' => ['readonly' => $readonly,]],
                    'name' => ['type' => Form::INPUT_TEXT, 'label' => Delivery::getMeta('name', 'label'), 'hint' => '' . Delivery::getMeta('name', 'comment'), 'options' => []],
                ],
            ],
        ],
    ],
    [
        'attributes' => [
            'address_detail' => [
                'columns' => 2,
                'attributes' => [
                    'description' => ['type' => Form::INPUT_TEXT, 'label' => Delivery::getMeta('description', 'label'), 'hint' => '' . Delivery::getMeta('description', 'comment'), 'options' => []],
                    'price' => ['type' => Form::INPUT_TEXT, 'label' => Delivery::getMeta('price', 'label'), 'hint' => '' . Delivery::getMeta('price', 'comment'), 'options' => []],
                ],
            ],
        ],
    ],
    [
        'attributes' => [
            'actions' => [    // embed raw HTML content
                'type' => Form::INPUT_RAW,
                'value' => '<div style="text-align: right; margin-top: 20px">' .
                    Html::submitButton('Сохранить', ['class' => 'public-address-button']) .
                    '</div>'
            ],
        ]
    ]


]


/* @var $this yii\web\View */
/* @var $model backend\models\Okrug */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-form">


    <?php


    $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'enableClientValidation' => true
    ]);
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'rows' => $data
    ]);

    ActiveForm::end();

    if (YII_DEBUG) {
        $finTime = microtime(true);
        $delta = $finTime - $startTime;
        echo $delta . ' сек.';
    }
    ?>

</div>
