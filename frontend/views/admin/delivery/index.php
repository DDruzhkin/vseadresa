<?php

/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\models\Delivery;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Способы доставки';
$this->params['breadcrumbs'][] = $this->title;
$this->context->h1 = $this->title;
?>
<div class="delivery-index">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],

        'columns' => [

            ['attribute' => 'id',
                'label' => Delivery::getMeta('id', 'label'),
                'contentOptions' => ['class' => 'col-id '],
                'filterOptions' => ['class' => 'col-id'],
                'headerOptions' => ['class' => 'col-id '],
                'footerOptions' => ['class' => 'col-id '],


            ],
            ['attribute' => 'alias',
                'label' => Delivery::getMeta('alias', 'label'),
                'contentOptions' => ['class' => 'col-alias '],
                'filterOptions' => ['class' => 'col-alias'],
                'headerOptions' => ['class' => 'col-alias '],
                'footerOptions' => ['class' => 'col-alias '],


            ],
            ['attribute' => 'name',
                'label' => Delivery::getMeta('name', 'label'),
                'contentOptions' => ['class' => 'col-name '],
                'filterOptions' => ['class' => 'col-name'],
                'headerOptions' => ['class' => 'col-name '],
                'footerOptions' => ['class' => 'col-name '],
                'value' => function ($data) {
                    return Html::a($data->name, ['/admin/delivery/update/', 'id' => $data->id], ['title' => 'Изменить элемент']);
                },
                'format' => 'raw'


            ],
            ['attribute' => 'price',
                'label' => Delivery::getMeta('price', 'label'),
                'contentOptions' => ['class' => 'col-price'],
                'filterOptions' => ['class' => 'col-price'],
                'headerOptions' => ['class' => 'col-price'],
                'footerOptions' => ['class' => 'col-price'],
                'value' => function ($data) {
                    return number_format($data->price,2,',',' ').' рублей';
                },
            ],
        ],
    ]); ?>
</div>
