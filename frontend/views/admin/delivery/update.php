<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Delivery */

$this->title = 'Редактирование способа доставки ' . $model->getCaption();
$this->params['breadcrumbs'][] = [
	'label' => \frontend\models\Delivery::getMeta('','title'),
	'url' => ['index'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];
$this->params['breadcrumbs'][] = $this -> title;
$this->context -> h1 = $this -> title;
?>
<div class="delivery-update">

  
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
