<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'ptype') ?>

    <?= $form->field($model, 'value') ?>

    <?= $form->field($model, 'payment_order_id') ?>

    <?php // echo $form->field($model, 'invoice_id') ?>

    <?php // echo $form->field($model, 'info') ?>

    <?php // echo $form->field($model, 'data') ?>

    <?php // echo $form->field($model, 'loaded_at') ?>

    <?php // echo $form->field($model, 'payment_order_date') ?>

    <?php // echo $form->field($model, 'payment_order_number') ?>

    <?php // echo $form->field($model, 'payment_order_summa') ?>

    <?php // echo $form->field($model, 'payer_id') ?>

    <?php // echo $form->field($model, 'payer_name') ?>

    <?php // echo $form->field($model, 'invoice_date') ?>

    <?php // echo $form->field($model, 'invoice_number') ?>

    <?php // echo $form->field($model, 'invoice_summa') ?>

    <?php // echo $form->field($model, 'recipient_id') ?>

    <?php // echo $form->field($model, 'recipient_name') ?>

    <?php // echo $form->field($model, 'pay_id') ?>

    <?php // echo $form->field($model, 'confirmed') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
