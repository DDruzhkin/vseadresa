<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use common\models\Entity;
use common\models\EntityExt;
use frontend\models\User;
use frontend\models\Payment;


$isOut =  $model -> payer -> user_id == EntityExt::portal() -> id; // исходящий платеж, если плательщиком является портал


$this->title = ($isOut?'Ис':'В').'ходящий платеж от '.$model -> date;
$this->params['breadcrumbs'][] = ['label' => 'Список платежей', 'url' => ['/admin/payment'], 'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL];
$this->params['breadcrumbs'][] = $this->title;


$attrList = $model -> getAttributes(); 
$attributes = [];
	foreach ($attrList as $colName => $value) {		
		$type = $model -> getMeta($colName, 'type');
		$show = (int)$model -> getMeta($colName, 'show');
		if (!$show) continue;
		$attributes[] = $colName."_view" . ($type == 'text'?':ntext':'');
	}



	//echo "<pre>", print_r($model);
?>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">


                    <h1 class="user-card-title"><?= Html::encode($this->title) ?></h1>
                    <div class="user-card-container">
<?php
		$attrs = ['date', 'value', 'payer_name', 'invoice_date', 'invoice_number', 'recipient_name'];		
		if (!$isOut) $attrs[] = 'ptype';
		
		foreach($attrs as $attr) {
			if ($model->$attr) {
			?>
						<div class="flex user-card-row">
                            <div class="user-card-label">
                                <?php echo Payment::getMeta($attr, 'label'); ?>
                            </div>
                            <div class="user-card-value">
							
                                <?php echo $model->$attr?>
								<?php
								if ($attr == 'payer_name') {
									echo $model -> payer?'['.$model -> payer -> user -> getPortalRole().']':'';
								};
								if ($attr == 'recipient_name') {
									echo $model -> recipient?'['.$model -> recipient -> user -> getPortalRole().']':'';
								}
								
								?>
                            </div>
                        </div>
			<?php
			}
		}

?>

<?php
	$orderNumber = null;
	$service = null;
	$invoice = $model -> invoice;
	if ($invoice) {
		$services = [];
		if ($invoice -> porder) { // Портальный заказ
			$orderNumber = $invoice -> porder -> number;
			$services = [$invoice -> porder -> service_name];
			
		} else if ($invoice -> order) { // Обычный заказ
			$order = $invoice -> order;
			$orderNumber = $order -> number;
			if ($order -> address_id) $services[] = 'Аренда адреса';
			foreach($order -> getServices() -> all() as $service) {				
				$services[] = $service -> name;
			};
			
		}
	}
	
	
	if ($orderNumber) {
	
?>
  
						
                        <div class="flex user-card-row">
                            <div class="user-card-label">
                                Номер заказа
							</div>
                            <div class="user-card-value">
                                <?php echo $orderNumber ?>
                            </div>
                        </div>
<?php 
	};
	if ($service) {
	?>						
						<div class="flex user-card-row">
                            <div class="user-card-label">
                                за услуг<?=count($services) > 1?"и":"у"?>
							</div>
                            <div class="user-card-value">
							<ul>
                                <?php 
								foreach($services as $service) {
									echo Html::tag('li', $service);
								}?>
							</ul>								
                            </div>
                        </div>
<?php
	}
?>
						

						<div style="clear:both"></div>
						

						
                    </div>
					
					<div style="clear:both"></div>
				</div>
			</div>
		</div>
    </div>
