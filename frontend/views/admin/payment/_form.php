<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Payment;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'date' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Payment::getMeta('date', 'label'), 'hint'=>''.Payment::getMeta('date', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
						'ptype' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Payment::getMeta('ptype', 'label'), 'items' => Payment::getList('ptype'), 'hint'=>''.Payment::getMeta('ptype', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'value' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Payment::getMeta('value', 'label'), 'hint'=>''.Payment::getMeta('value', 'comment'), 'options'=>['clientOptions' => ['alias' =>  'decimal', 'groupSeparator' => ' ','autoGroup' => true],]],
						'invoice_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Payment::getMeta('invoice_id', 'label'), 'items' => Payment::getListValues('invoice_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('document/update/%invoice_id%')]).'">'.Icon::show('link').'</a>'.Payment::getMeta('invoice_id', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'info' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Payment::getMeta('info', 'label'), 'hint'=>''.Payment::getMeta('info', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'loaded_at' => ['type'=>Form::INPUT_TEXT, 'label'=>Payment::getMeta('loaded_at', 'label'), 'hint'=>''.Payment::getMeta('loaded_at', 'comment'), 'options'=>[]],
						'payer_name' => ['type'=>Form::INPUT_TEXT, 'label'=>Payment::getMeta('payer_name', 'label'), 'hint'=>''.Payment::getMeta('payer_name', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'invoice_date' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Payment::getMeta('invoice_date', 'label'), 'hint'=>''.Payment::getMeta('invoice_date', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
						'invoice_number' => ['type'=>Form::INPUT_TEXT, 'label'=>Payment::getMeta('invoice_number', 'label'), 'hint'=>''.Payment::getMeta('invoice_number', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'recipient_name' => ['type'=>Form::INPUT_TEXT, 'label'=>Payment::getMeta('recipient_name', 'label'), 'hint'=>''.Payment::getMeta('recipient_name', 'comment'), 'options'=>[]],

]




/* @var $this yii\web\View */
/* @var $model backend\models\Payment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Payment::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
