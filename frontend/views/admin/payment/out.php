<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\models\Payment;
use frontend\models\User;
use common\models\Entity;
use common\models\PaymentSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Исходящие Платежи';
$this->params['breadcrumbs'] =
    [


        [
            'label' => $this -> title,
        ],

    ];
	
?>
<div class="payment-index">


	<div class="content">
	<?php
	
		switch ($param) {
		
		case 'payer_id': {
			$model = User::findone([$param_value]);
			if($model) {
				echo 'Платежи от плательщика '.Html::a($model -> fullname, ['admin/user/view', 'id' => $model -> id]).'<br>';
			};
			break;
		}
		
		case 'recipient_id': {
			$model = User::findone([$param_value]);
			if($model) {
				echo 'Платежи получателю '.Html::a($model -> fullname, ['admin/user/view', 'id' => $model -> id]).'<br>';
			};
			break;
		}
		
		case 'user_id': {
			$model = User::findone([$param_value]);
			if($model) {
				echo 'Расчеты по пользователю '.Html::a($model -> fullname, ['admin/user/view', 'id' => $model -> id]).'<br>';
			};
			break;
		}
			
	}
	
	?>
	</div>
  
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'exportConfig' => [	
			GridView::EXCEL => [
			'label' => 'Excel',
			'icon' => 'floppy-remove',
			],
		],
		'layout' => '<div class="pull-right">{export}</div>{summary}<br><br>{items}',
        'filterModel' => $searchModel,

        'columns' => [
/*		        
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Payment::getListViewMenu($data),
							],
						],
					]);
				}					
			],

*/
            ['attribute' => 'date',
				'label' => Payment::getMeta('date', 'label'),
				'contentOptions' => ['class' => 'col-date date'],
				'filterOptions' => ['class' => 'col-date'],
				'headerOptions' => ['class' => 'col-date date'],				
				'footerOptions' => ['class' => 'col-date date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'date',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'ptype',
				'label' => Payment::getMeta('ptype', 'label'),
				'contentOptions' => ['class' => 'col-ptype '],
				'filterOptions' => ['class' => 'col-ptype'],
				'headerOptions' => ['class' => 'col-ptype '],				
				'footerOptions' => ['class' => 'col-ptype '],
				
				
				
			],
            ['attribute' => 'value',
				'label' => Payment::getMeta('value', 'label'),
				'contentOptions' => ['class' => 'col-value '],
				'filterOptions' => ['class' => 'col-value'],
				'headerOptions' => ['class' => 'col-value '],				
				'footerOptions' => ['class' => 'col-value '],								
				//'format' => ['decimal', 2],
				'value' => 
				function($data)
				{ 
					$value = Yii::$app -> formatter -> asCurrency($data -> value);
					$value = Html::a($value,['/admin/'.Inflector::camel2id('Payment').'/view/', 'id' => $data['id']], ['title' => 'Информация о платеже']);
					return $value;
				},
				'format' => 'raw',
			],
            ['attribute' => 'invoice_id',
				'label' => Payment::getMeta('invoice_id', 'label'),
				'contentOptions' => ['class' => 'col-invoice_id hidden '],
				'filterOptions' => ['class' => 'col-invoice_id hidden'],
				'headerOptions' => ['class' => 'col-invoice_id hidden '],				
				'footerOptions' => ['class' => 'col-invoice_id hidden '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['invoice_id_view'],[Inflector::camel2id('Document').'/update/'.$data['invoice_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'info',
				'label' => Payment::getMeta('info', 'label'),
				'contentOptions' => ['class' => 'col-info hidden '],
				'filterOptions' => ['class' => 'col-info hidden'],
				'headerOptions' => ['class' => 'col-info hidden '],				
				'footerOptions' => ['class' => 'col-info hidden '],
				
				
				
			],
            ['attribute' => 'loaded_at',
				'label' => Payment::getMeta('loaded_at', 'label'),
				'contentOptions' => ['class' => 'col-loaded_at hidden '],
				'filterOptions' => ['class' => 'col-loaded_at hidden'],
				'headerOptions' => ['class' => 'col-loaded_at hidden '],				
				'footerOptions' => ['class' => 'col-loaded_at hidden '],
				
				
				
			],
            ['attribute' => 'payer_name',
				'label' => Payment::getMeta('payer_name', 'label'),
				'contentOptions' => ['class' => 'col-payer_name '],
				'filterOptions' => ['class' => 'col-payer_name'],
				'headerOptions' => ['class' => 'col-payer_name '],				
				'footerOptions' => ['class' => 'col-payer_name '],
				'value' => function($model) {
					return $model -> payer_name.($model -> payer?'<br>['.$model -> payer -> user -> getPortalRole().']':'');
				},
				'format' => 'raw'
				
				
			],
            ['attribute' => 'invoice_date',
				'label' => Payment::getMeta('invoice_date', 'label'),
				'contentOptions' => ['class' => 'col-invoice_date date'],
				'filterOptions' => ['class' => 'col-invoice_date'],
				'headerOptions' => ['class' => 'col-invoice_date date'],				
				'footerOptions' => ['class' => 'col-invoice_date date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'invoice_date',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'invoice_number',
				'label' => Payment::getMeta('invoice_number', 'label'),
				'contentOptions' => ['class' => 'col-invoice_number '],
				'filterOptions' => ['class' => 'col-invoice_number'],
				'headerOptions' => ['class' => 'col-invoice_number '],				
				'footerOptions' => ['class' => 'col-invoice_number '],
				
				'value' => function($data) {
					return Html::a($data -> invoice_number, Url::to(['document/show-pdf/', 'id' => $data -> invoice_id]), ['target' => '_blank', 'title' => 'Скачать счет']);
				},
				'format' => 'raw',
				
			],
            ['attribute' => 'recipient_name',
				'label' => Payment::getMeta('recipient_name', 'label'),
				'contentOptions' => ['class' => 'col-recipient_name '],
				'filterOptions' => ['class' => 'col-recipient_name'],
				'headerOptions' => ['class' => 'col-recipient_name '],				
				'footerOptions' => ['class' => 'col-recipient_name '],
				'value' => function($model) {
					return $model -> recipient_name.($model -> recipient?'<br>['.$model -> recipient -> user -> getPortalRole().']':'');
				},
				'format' => 'raw'
				

			],
/*
            ['class' => 'yii\grid\ActionColumn'],
*/			
        ],
    ]); ?>
</div>
