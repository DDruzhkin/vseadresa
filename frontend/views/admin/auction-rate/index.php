<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\AuctionRate;
use common\models\AuctionRateSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;


$this -> title = 'Ставки';

$this->params['breadcrumbs'] =
    [
/*
        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/owner'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
		*/
		[
            'label'     => 'Аукционы',
            'url'       =>  ['/admin/auction'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,
        ],

    ];


?>
<div class="auction-rate-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\AuctionRate::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],
        'filterModel' => $searchModel,
        'columns' => [


            ['attribute' => 'created_at',
				'label' => AuctionRate::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at '],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at '],				
				'footerOptions' => ['class' => 'col-created_at '],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'auction_id',
				'label' => AuctionRate::getMeta('auction_id', 'label'),
				'contentOptions' => ['class' => 'col-auction_id '],
				'filterOptions' => ['class' => 'col-auction_id'],
				'headerOptions' => ['class' => 'col-auction_id '],				
				'footerOptions' => ['class' => 'col-auction_id '],
				
				'value' => function($data){ return $data['auction_id_view'];},
				
			],
            ['attribute' => 'address_id',
				'label' => AuctionRate::getMeta('address_id', 'label'),
				'contentOptions' => ['class' => 'col-address_id '],
				'filterOptions' => ['class' => 'col-address_id'],
				'headerOptions' => ['class' => 'col-address_id '],				
				'footerOptions' => ['class' => 'col-address_id '],
				
				'value' => function($data){ return $data['address_id_view'];},
				
			],
            ['attribute' => 'owner_id',
				'label' => AuctionRate::getMeta('owner_id', 'label'),
				'contentOptions' => ['class' => 'col-owner_id '],
				'filterOptions' => ['class' => 'col-owner_id'],
				'headerOptions' => ['class' => 'col-owner_id '],				
				'footerOptions' => ['class' => 'col-owner_id '],
				
				'value' => function($data){ return $data['owner_id_view'];},
				
			],
            ['attribute' => 'amount',
				'label' => AuctionRate::getMeta('amount', 'label'),
				'contentOptions' => ['class' => 'col-amount '],
				'filterOptions' => ['class' => 'col-amount'],
				'headerOptions' => ['class' => 'col-amount '],				
				'footerOptions' => ['class' => 'col-amount '],
				'filter' => AuctionRate::getExistsValues('amount'),	
				
				'format' => ['decimal', 2],
			],
/*
            ['class' => 'yii\grid\ActionColumn'],
			*/
        ],
    ]); ?>
</div>
