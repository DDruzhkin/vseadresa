<?php
/* генерируемый файл, редактировать его не надо */

use frontend\models\Faq;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Faq */

$this->title = 'Добавление ';
$this -> context -> h1 = $this->title;
$this->params['breadcrumbs'][] = [
	'label' => Faq::getMeta('','title'), 
	'url' => ['index'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];
$this->params['breadcrumbs'][] = $this -> title;
?>
<div class="faq-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
