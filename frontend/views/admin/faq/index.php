<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\models\Faq;
use common\models\FaqSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;



$this->title = Faq::getMeta('','title');
$this->params['breadcrumbs'][] = $this->title;
$this->context -> h1 = $this->title;
?>
<div class="faq-index">

	<div class="content">
	
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<div class="controls">
       <?= Html::a('Добавить', ['create'], ['class' => 'reg-button']) ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],


        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => $data -> getAdminMenuItems(),
							],
						],
					]);
				}					
			],


            ['attribute' => 'created_at',
				'label' => Faq::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'question',
				'label' => Faq::getMeta('question', 'label'),
				'contentOptions' => ['class' => 'col-question '],
				'filterOptions' => ['class' => 'col-question'],
				'headerOptions' => ['class' => 'col-question '],				
				'footerOptions' => ['class' => 'col-question '],
				
				
				
			],
            ['attribute' => 'answer',
				'label' => Faq::getMeta('answer', 'label'),
				'contentOptions' => ['class' => 'col-answer hidden '],
				'filterOptions' => ['class' => 'col-answer hidden'],
				'headerOptions' => ['class' => 'col-answer hidden '],				
				'footerOptions' => ['class' => 'col-answer hidden '],
				
				
				
			],
/*			
            ['attribute' => 'role',
				'label' => Faq::getMeta('role', 'label'),
				'contentOptions' => ['class' => 'col-role '],
				'filterOptions' => ['class' => 'col-role'],
				'headerOptions' => ['class' => 'col-role '],				
				'footerOptions' => ['class' => 'col-role '],
				'filter' => Faq::getList('role'),	
				
				
			],
*/			
            ['attribute' => 'enabled',
				'label' => 'Видимость',
				'contentOptions' => ['class' => 'col-enabled'],
				'filterOptions' => ['class' => 'col-enabled'],
				'headerOptions' => ['class' => 'col-enabled'],				
				'footerOptions' => ['class' => 'col-enabled'],
				'value' => function($data) {
					return $data -> enabled?'Отображается':'Скрыт';
				}
				
				
			],
/*
            ['class' => 'yii\grid\ActionColumn'],
*/			
        ],
    ]); ?>
</div>
