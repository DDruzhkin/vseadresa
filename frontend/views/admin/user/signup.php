<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveField;
use frontend\models\User;
use frontend\models\SignupForm;

use dektrium\user\widgets\Connect;




$this->title = 'Регистрация администратора';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid"  id="reg" >

    <div class="container">
        <div style="">
            <div class="row reg-logo">
                <div class="col-md-3">
                    <div class="header-padding-left">
                        <div class="registration-title">
                            регистрация <br>
                            Администратора:
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                    //echo "<pre>", print_r($model->getErrors()), "</pre>";

                    if(count($model->getErrors())){
                        foreach ($model->getErrors() as $error){
                            echo "<p style='text-align: center'>".$error[0]."</p>";

                        }
                    }
                ?>
                <?php $form = ActiveForm::begin([
                        'id' => 'form-signup',
                    'enableAjaxValidation' => true,
                    'enableClientValidation'=>true

                ]); ?>
                <div class="col-md-3 reg-tutorial">
                    <div class="flex">
                        <sup>*</sup>
                        <div>помечены поля, <br>
                            обязательные для заполнения</div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="field-row flex al">
                        <div class="form-label">
                            <label for="" class="flex">
                                <div class="label-content">фамилия, имя</div>
                                <sup>*</sup>
                            </label>
                        </div>
                        <div class="form-input">
                            <?= $form->field($model, 'fullname')->label(false)->textInput() ?>
                        </div>
                    </div>
                    <div class="field-row flex al">
                        <div class="form-label">
                            <label for="" class="flex">
                                <div class="label-content">email</div>
                                <sup>*</sup>
                            </label>
                        </div>
                        <div class="form-input">
                            <?= $form->field($model, 'username')->label(false)->textInput() ?>
                        </div>
                        <div class="field-subrow">
                            является уникальным
                            идентификатором пользователя
                            в системе и используется
                            для авторизации
                        </div>
                    </div>
                    <div class="field-row flex al">
                        <div class="form-label">
                            <label for="" class="flex">
                                <div class="label-content">пароль</div>
                                <sup>*</sup>
                            </label>
                        </div>
                        <div class="form-input">

                            <?php echo $form->field($model, 'password')->label(false)->passwordInput() ?>
                        </div>
                    </div>
                    <div class="field-row flex al">
                        <div class="form-label">
                            <label for="" class="flex">
                                <div class="label-content">подтверждение пароля</div>
                                <sup>*</sup>
                            </label>
                        </div>
                        <div class="form-input">

                            <?= $form->field($model, 'password_repeat')->label(false)->passwordInput() ?>
                        </div>
                    </div>

                    <div class="field-row flex al">
                        <!--<a href="" class="reg-button">
                            зарегистрироваться
                        </a>-->
                        <?= Html::submitButton('зарегистрироваться', ['class' => 'reg-button', 'name' => 'signup-button']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

        </div>
    </div>
</div>
<?php
/*$this->registerJsFile(
    "$('#form-signup').on('afterValidate', function (e, messages) {
    console.log(messages, 'xxxxxxxxxxxxxxxxxxxx');
});",
    \yii\web\View::POS_END,
);*/
?>

