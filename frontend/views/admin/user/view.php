<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use common\models\Entity;
use frontend\models\User;
use frontend\models\Address;


/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['list'], 'template' => '<span class="b-link">{link}</span><span class="separate-links">\</span>' . PHP_EOL];
$this->params['breadcrumbs'][] = $this->title;

$attrList = $model->getAttributes();
$attributes = [];
foreach ($attrList as $colName => $value) {
    $type = $model->getMeta($colName, 'type');
    $show = (int)$model->getMeta($colName, 'show');
    if (!$show) continue;
    $attributes[] = $colName . "_view" . ($type == 'text' ? ':ntext' : '');
}


//echo "<pre>", print_r($model);
?>
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">


                <h1 class="user-card-title"><?= Html::encode($this->title) ?></h1>
                <div class="user-card-container">
                    <!--
                        <div class="flex user-card-row">
                            <div class="user-card-label">
                                <?php echo User::getMeta('fullname', 'label'); ?>
                            </div>
                            <div class="user-card-value">
                                <?php echo $model->fullname; ?>
                            </div>
                        </div>
						-->


                    <?php if (User::getCurrentUser() && User::getCurrentUser()->checkRole(User::ROLE_ADMIN)): ?>


                        <div class="flex user-card-row">
                            <div class="user-card-label">
                                <?php echo User::getMeta('id', 'label'); ?>
                            </div>
                            <div class="user-card-value">
                                <?php echo $model->id; ?>
                            </div>
                        </div>


                        <div class="flex user-card-row">
                            <div class="user-card-label">
                                <?php echo User::getMeta('username', 'label'); ?>
                            </div>
                            <div class="user-card-value">
                                <?php echo $model->username; ?>
                            </div>
                        </div>
                        <div class="flex user-card-row">
                            <div class="user-card-label">
                                <?php echo User::getMeta('role', 'label'); ?>
                            </div>
                            <div class="user-card-value">
                                <?php echo $model->role; ?>
                            </div>
                        </div>
                        <div class="flex user-card-row">
                            <div class="user-card-label">
                                <?php echo User::getMeta('status_id', 'label'); ?>
                            </div>
                            <div class="user-card-value">
                                <?php echo $model->status ? $model->status->getCaption() : ''; ?>
                            </div>
                        </div>
                        <div class="flex user-card-row">
                            <div class="user-card-label">
                                <?php echo User::getMeta('created_at', 'label'); ?>
                            </div>
                            <div class="user-card-value">
                                <?php echo $model->created_at; ?>
                            </div>
                        </div>
                        <div class="flex user-card-row">
                            <div class="user-card-label">
                                <?php echo User::getMeta('phone', 'label'); ?>
                            </div>
                            <div class="user-card-value">
                                <?php echo $model->phone; ?>
                            </div>
                        </div>

                        <a title="Изменить данные пользователя" class="reg-button"
                           href="<?= Url::to(['admin/user/update', 'id' => $model->id]) ?>">Изменить</a>


                        <div style="clear:both"></div>


                    <?php endif ?>

                </div>

                <div style="clear:both"></div>


                <?php
                if ($model->checkRole(User::ROLE_OWNER)) {
                    ?>
                    <div class="user-card-addresses">
                        <h2>Договоры</h2>
                        <div class="grid-view">
                            <?php

                            echo \frontend\components\ContractsWidget::widget(['user' => $model]);

                            ?>
                        </div>
                    </div>

                    <?php
                };

                if (!is_null($model->delivery_address)) {
                    ?>
                    <div class="user-card-addresses">
                        <h2>Адрес доставки документов</h2>
                        <div class="rekvizit-container">
                            <ul class="requisites">
                                <?php

                                $props = $model->delivery_address_as_array;
                                if ($props && is_array($props)) {
                                    foreach ($props as $name => $value) {
                                        if ($value) echo Html::tag('li', "$value", ['class' => 'base']);
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <?php
                };
                ?>
                <div class="user-card-addresses">
                    <h2>Реквизиты</h2>

                    <div class="rekvizit-container">
                        <ul class="requisites">
                            <?php

                            $list = $model->requisiteList();

                            if ($list) {

                                $requisite = Entity::byId($model->default_profile_id);
                                echo Html::a('Договоры', Url::to(['/admin/contract', 'recipient_id' => $requisite->id])) . '<br>';
                                echo Html::tag("li", $requisite->getFooterText() .
                                    Html::a('<i class="fa fa-edit"></i>Изменить',
                                        Url::to(['/admin/r/update', 'id' => $requisite->id]))
                                    , ["class" => "base",
                                        "style" => "text-decoration:none"
                                    ]);

                                foreach ($list as $id => $item) {
                                    if ($model->default_profile_id == $id) continue;
                                    $requisite = $model->byId($id);
                                    if (!$requisite) continue;
                                    echo Html::a('Договоры', Url::to(['/admin/contract', 'recipient_id' => $requisite->id])) . '<br>';
                                    echo Html::tag("li", $requisite->getFooterText());

                                }
                            } else {
                                echo "Нет реквизитов";
                            }

                            ?>
                        </ul>
                    </div>
                </div>


                <?php if ($model->role == User::attrLists()['role']['Владелец']): ?>
                    <div class="user-card-addresses">
                        <h2>
                            Список адресов
                        </h2>
                    </div>
                    <div class="grid-table-container">
                        <?php

                        $statuses = [Address::statusByAlias('published')->id]; // статусы, в которых отображается адрес

                        $searchModel = $model->getAddresses()->andWhere(['status_id' => $statuses]); // ограничение по статусам;
                        $dataProvider = new ArrayDataProvider([
                            'allModels' => $searchModel->all(),
                            'sort' => [
                                'attributes' => ['id', 'username', 'email'],
                            ],
                            'pagination' => [
                                'pageSize' => 10,
                            ],
                        ]);
                        ?>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'summary' => false,
                            'tableOptions' => [
                                'class' => 'table price-table counts-table',

                            ],

                            'filterModel' => $searchModel,
                            'columns' => [

                                [
                                    'class' => 'yii\grid\Column',

                                ],


                                ['attribute' => 'id',
                                    'label' => 'Код',
                                    'contentOptions' => ['class' => 'col-id '],
                                    'filterOptions' => ['class' => 'col-id'],
                                    'headerOptions' => ['class' => 'col-id '],
                                    'footerOptions' => ['class' => 'col-id '],


                                ],

                                ['attribute' => 'address',
                                    'label' => Address::getMeta('address', 'label'),
                                    'contentOptions' => ['class' => 'col-address '],
                                    'filterOptions' => ['class' => 'col-address'],
                                    'headerOptions' => ['class' => 'col-address '],
                                    'footerOptions' => ['class' => 'col-address '],


                                ],
                                ['attribute' => 'status_id',
                                    'label' => Address::getMeta('status_id', 'label'),
                                    'contentOptions' => ['class' => 'col-status_id '],
                                    'filterOptions' => ['class' => 'col-status_id'],
                                    'headerOptions' => ['class' => 'col-status_id '],
                                    'footerOptions' => ['class' => 'col-status_id '],
                                    'filter' => Address::getExistsValues('status_id'),
                                    'value' => function ($data) {
                                        return $data['status_id_view'];
                                    },

                                ],

                            ],
                        ]); ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>

    </div>
</div>
