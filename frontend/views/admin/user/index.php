<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\User;
use common\models\UserSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
$this -> context -> h1 = $this->title;
?>
<div class="user-index">

	<div class="content">
	
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,

		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],
        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					$items = $data -> getAdminMenuItems();
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => $items,
							],
						],
					]);
				}					
			],
			['attribute' => 'id',
				'label' => User::getMeta('id', 'label'),
				'contentOptions' => ['class' => 'col-id '],
				'filterOptions' => ['class' => 'col-id'],
				'headerOptions' => ['class' => 'col-id '],				
				'footerOptions' => ['class' => 'col-fullname '],
				
				
				
			],
			['attribute' => 'created_at',
				//'label' => User::getMeta('created_at', 'label'),
				'label' => 'Регистрация',
				'contentOptions' => ['class' => 'col-id '],
				'filterOptions' => ['class' => 'col-id'],
				'headerOptions' => ['class' => 'col-id '],				
				'footerOptions' => ['class' => 'col-fullname '],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				'value' => function($data) {
					return Yii::$app -> formatter -> asDate($data -> created_at);
				}
								
				
				
			],
			

            ['attribute' => 'fullname',
				'label' => User::getMeta('fullname', 'label'),
				'contentOptions' => ['class' => 'col-fullname '],
				'filterOptions' => ['class' => 'col-fullname'],
				'headerOptions' => ['class' => 'col-fullname '],				
				'footerOptions' => ['class' => 'col-fullname '],
				'format' => 'raw',
				'value' => function($data){ 
					return Html::a($data['fullname'], Url::to(['/admin/user/view', 'id' => $data -> id]), ['title' => 'Перейти на карточку пользователя']);
				},
				
				
			],
			
            ['attribute' => 'username',
				'label' => User::getMeta('username', 'label'),
				'contentOptions' => ['class' => 'col-username '],
				'filterOptions' => ['class' => 'col-username'],
				'headerOptions' => ['class' => 'col-username '],				
				'footerOptions' => ['class' => 'col-username '],
				
				
				
			],
            ['attribute' => 'role',
				'label' => User::getMeta('role', 'label'),
				'contentOptions' => ['class' => 'col-role '],
				'filterOptions' => ['class' => 'col-role'],
				'headerOptions' => ['class' => 'col-role '],				
				'footerOptions' => ['class' => 'col-role '],
				'filter' => User::getList('role'),	
				
				
			],
            ['attribute' => 'status_id',
				'label' => User::getMeta('status_id', 'label'),
				'contentOptions' => ['class' => 'col-status_id '],
				'filterOptions' => ['class' => 'col-status_id'],
				'headerOptions' => ['class' => 'col-status_id '],				
				'footerOptions' => ['class' => 'col-status_id '],
				'filter' => User::getExistsValues('status_id'),	
				'value' => function($data){ return $data['status_id_view'];},
				
			],
			
/*			
            ['attribute' => 'created_at',
				'label' => User::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'password',
				'label' => User::getMeta('password', 'label'),
				'contentOptions' => ['class' => 'col-password hidden '],
				'filterOptions' => ['class' => 'col-password hidden'],
				'headerOptions' => ['class' => 'col-password hidden '],				
				'footerOptions' => ['class' => 'col-password hidden '],
				
				
				
			],
            ['attribute' => 'email',
				'label' => User::getMeta('email', 'label'),
				'contentOptions' => ['class' => 'col-email '],
				'filterOptions' => ['class' => 'col-email'],
				'headerOptions' => ['class' => 'col-email '],				
				'footerOptions' => ['class' => 'col-email '],
				
				
				
			],
            ['attribute' => 'phone',
				'label' => User::getMeta('phone', 'label'),
				'contentOptions' => ['class' => 'col-phone '],
				'filterOptions' => ['class' => 'col-phone'],
				'headerOptions' => ['class' => 'col-phone '],				
				'footerOptions' => ['class' => 'col-phone '],
				
				
				
			],
            ['attribute' => 'offerta_confirm',
				'label' => User::getMeta('offerta_confirm', 'label'),
				'contentOptions' => ['class' => 'col-offerta_confirm hidden '],
				'filterOptions' => ['class' => 'col-offerta_confirm hidden'],
				'headerOptions' => ['class' => 'col-offerta_confirm hidden '],				
				'footerOptions' => ['class' => 'col-offerta_confirm hidden '],
				
				
				
			],
            ['attribute' => 'info',
				'label' => User::getMeta('info', 'label'),
				'contentOptions' => ['class' => 'col-info hidden '],
				'filterOptions' => ['class' => 'col-info hidden'],
				'headerOptions' => ['class' => 'col-info hidden '],				
				'footerOptions' => ['class' => 'col-info hidden '],
				
				
				
			],
			*/
			
			['attribute' => 'priority',
				'label' => User::getMeta('priority', 'label'),
				'contentOptions' => ['class' => 'col-priority'],
				'filterOptions' => ['class' => 'col-priority'],
				'headerOptions' => ['class' => 'col-priority'],				
				'footerOptions' => ['class' => 'col-priority'],
				'value' => function($data) {
					return $data -> priority?$data -> priority:'0';
				}
				
				
			],
          
        ],
    ]); ?>
</div>
