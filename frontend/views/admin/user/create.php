<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = 'Добавление Пользователя';
$this -> context -> h1 = $this->title;
$this->params['breadcrumbs'][] = [
	'label' => \backend\models\User::getMeta('','title'), 
	'url' => ['list'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];

$this->params['breadcrumbs'][] = $this -> title;
?>
<div class="user-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
