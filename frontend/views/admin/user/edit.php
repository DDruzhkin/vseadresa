<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;
use yii\widgets\DetailView;

use common\models\Entity;

/* @var $this yii\web\View */
/* @var $model backend\models\User */


$this->title = 'Пользователь ' . $model->fullname;
$this -> context -> h1 = $this->title;
$this->params['breadcrumbs'][] = [
	'label' => \backend\models\User::getMeta('','title'), 
	'url' => ['list'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];

$this->params['breadcrumbs'][] = $this -> title;



?>


<div class="user-view">
    <div class="row">
        <div class="col-md-9">
		             
					
                <?php if ($model->status_view): ?>
                    <div class="labels">
                        Текущее состояние пользователя
                    </div>
                    <div class="order-value">
                        <span><?= $model -> status -> getCaption() ?></span>
						<?php echo ($model -> status -> alias == 'partpaid')?'('.Yii::$app -> formatter -> asCurrency($model -> balance()).')':''?>
                    </div>
                <?php endif; ?>

                <?php if ($model->created_at): ?>
                    <div class="labels">
                        дата регистрации
                    </div>
                    <div class="order-value">
                        <span><?= Yii::$app -> formatter -> asDate($model->created_at); ?></span>
                    </div>
                <?php endif; ?>

                <?php if ($model->updated_at): ?>
                    <div class="labels">
                        дата последнего изменения
                    </div>
                    <div class="order-value">
                        <span><?= Yii::$app -> formatter -> asDate($model->updated_at); ?></span>
                    </div>
                <?php endif; ?>
				
				<?php
				$attrList = ['id', 'fullname', 'username', 'phone', 'delivery_address', 'info'];
				
				foreach($attrList as $attrName) {
					$attrValue = $model[$attrName];
					$attrLabel = $model -> getMeta($attrName)['label'];
					
					if ($attrName == 'delivery_address') {
						$address = $model -> delivery_address_as_array;
						$attrValue = $address['index'].'<br>';
						if (isset($address['district']) && $address['district']) {
							$attrValue = $attrValue.'Область (край): '.$address['district'].'<br>';
						};
						if (isset($address['region']) && $address['region']) {
							$attrValue = $attrValue.'Район: '.$address['region'].'<br>';
						};
						if (isset($address['destination']) && $address['destination']) {
							$attrValue = $attrValue.'Населенный пункт: '.$address['destination'].'<br>';
						};
						
						if (isset($address['address']) && $address['address']) {
							$attrValue = $attrValue.$address['address'].'<br>';
						};
						if (isset($address['info']) && $address['info']) {
							$attrValue = $attrValue.'Дополнительная информация: '.$address['info'].'<br>';
						};
						
					}
					
					echo '
					<div class="labels">
                     '.$attrLabel.'
                    </div>
                    <div class="order-value">
                        <span>'.($attrValue?$attrValue:' - ').'</span>
                    </div>
					';

				}
				?>
				

				
<!--				
				<div class="">

                    <div class="labels">
                        Реквизиты, на которые оформлен заказ
                    </div>

                    <?php 
						
						//echo $model -> customer_r -> getFooterText();
					?>

                </div>
-->				
                <div class="">

                    <div class="labels">
                        Данные для формирвания документов
                    </div>

                    <?php 
						$id = $model -> default_profile_id;
						$r = Entity::byId($id);
						if ($r) echo $r -> getFooterText();
					?>

                </div>



        </div>
    </div>


