<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = 'Редактирование Пользователя ' . $model->getCaption();

$this -> context -> h1 = $this->title;
$this->params['breadcrumbs'][] = [
	'label' => \backend\models\User::getMeta('','title'), 
	'url' => ['list'],
	'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
];

$this->params['breadcrumbs'][] = $this -> title;
?>
<div class="user-update">

   
    <?= $this->render('_form', [
        'model' => $model,		
    ]) ?>

</div>
