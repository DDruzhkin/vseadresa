<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Option;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'icon' => ['type'=>Form::INPUT_RADIO_LIST, 'label'=>Option::getMeta('icon', 'label'), 'items' => Option::getIcon('icon'), 'hint'=>''.Option::getMeta('icon', 'comment'), 'options'=>[]],
						'alias' => ['type'=>Form::INPUT_TEXT, 'label'=>Option::getMeta('alias', 'label'), 'hint'=>''.Option::getMeta('alias', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'name' => ['type'=>Form::INPUT_TEXT, 'label'=>Option::getMeta('name', 'label'), 'hint'=>''.Option::getMeta('name', 'comment'), 'options'=>[]],
						'label' => ['type'=>Form::INPUT_TEXT, 'label'=>Option::getMeta('label', 'label'), 'hint'=>''.Option::getMeta('label', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,
					'attributes' => [			
		
						'title' => ['type'=>Form::INPUT_TEXT, 'label'=>Option::getMeta('title', 'label'), 'hint'=>''.Option::getMeta('title', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,
					'attributes' => [			
						'meta_description' => ['type'=>Form::INPUT_TEXT, 'label'=>Option::getMeta('meta_description', 'label'), 'hint'=>''.Option::getMeta('meta_description', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],
	
	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'h1' => ['type'=>Form::INPUT_TEXT, 'label'=>Option::getMeta('h1', 'label'), 'hint'=>''.Option::getMeta('h1', 'comment'), 'options'=>[]],
		
				],
			],
				
		],		
	],
		
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'seo_text' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Option::getMeta('seo_text', 'label'), 'hint'=>''.Option::getMeta('seo_text', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'public-address-button']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Option */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="option-form">


<?php



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
