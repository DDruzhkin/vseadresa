<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use frontend\models\ServicePrice;
use common\models\Entity;
use common\models\ServicePriceSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Прайс-лист на услуги';
$this->params['breadcrumbs'][] = $this->title;
$this->context -> h1 = $this->title;
?>
<div class="service-price-index">
    
	<div class="content">
	<?= ServicePrice::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<!--
    <p>
        <?= Html::a('Добавить услугу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,

		'tableOptions' => [
            'class' => 'table price-table counts-table has-sort',
        ],

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\ServicePrice::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'code',
				'label' => ServicePrice::getMeta('code', 'label'),
				'contentOptions' => ['class' => 'col-code '],
				'filterOptions' => ['class' => 'col-code'],
				'headerOptions' => ['class' => 'col-code '],				
				'footerOptions' => ['class' => 'col-code '],
				
				
				
			],
            ['attribute' => 'stype',
				'label' => ServicePrice::getMeta('stype', 'label'),
				'contentOptions' => ['class' => 'col-stype '],
				'filterOptions' => ['class' => 'col-stype'],
				'headerOptions' => ['class' => 'col-stype '],				
				'footerOptions' => ['class' => 'col-stype '],
				'filter' => ServicePrice::getList('stype'),	
				
				
			],
            ['attribute' => 'service_name_id',
				'label' => ServicePrice::getMeta('service_name_id', 'label'),
				'contentOptions' => ['class' => 'col-service_name_id '],
				'filterOptions' => ['class' => 'col-service_name_id'],
				'headerOptions' => ['class' => 'col-service_name_id '],				
				'footerOptions' => ['class' => 'col-service_name_id '],
				'filter' => ServicePrice::getExistsValues('service_name_id'),	
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['service_name_id_view'],[Inflector::camel2id('ServiceName').'/update/'.$data['service_name_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'name',
				'label' => ServicePrice::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				
				
				
			],
            ['attribute' => 'created_at',
				'label' => ServicePrice::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at hidden date'],
				'filterOptions' => ['class' => 'col-created_at hidden'],
				'headerOptions' => ['class' => 'col-created_at hidden date'],				
				'footerOptions' => ['class' => 'col-created_at hidden date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'to_address',
				'label' => ServicePrice::getMeta('to_address', 'label'),
				'contentOptions' => ['class' => 'col-to_address hidden '],
				'filterOptions' => ['class' => 'col-to_address hidden'],
				'headerOptions' => ['class' => 'col-to_address hidden '],				
				'footerOptions' => ['class' => 'col-to_address hidden '],
				
				
				
			],
            ['attribute' => 'owner_id',
				'label' => ServicePrice::getMeta('owner_id', 'label'),
				'contentOptions' => ['class' => 'col-owner_id '],
				'filterOptions' => ['class' => 'col-owner_id'],
				'headerOptions' => ['class' => 'col-owner_id '],				
				'footerOptions' => ['class' => 'col-owner_id '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['owner_id_view'],[Inflector::camel2id('User').'/update/'.$data['owner_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'recipient_id',
				'label' => ServicePrice::getMeta('recipient_id', 'label'),
				'contentOptions' => ['class' => 'col-recipient_id hidden '],
				'filterOptions' => ['class' => 'col-recipient_id hidden'],
				'headerOptions' => ['class' => 'col-recipient_id hidden '],				
				'footerOptions' => ['class' => 'col-recipient_id hidden '],
				
				'value' => function($data){ return $data['recipient_id_view'];},
				
			],
            ['attribute' => 'address_id',
				'label' => ServicePrice::getMeta('address_id', 'label'),
				'contentOptions' => ['class' => 'col-address_id '],
				'filterOptions' => ['class' => 'col-address_id'],
				'headerOptions' => ['class' => 'col-address_id '],				
				'footerOptions' => ['class' => 'col-address_id '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['address_id_view'],[Inflector::camel2id('Address').'/update/'.$data['address_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'description',
				'label' => ServicePrice::getMeta('description', 'label'),
				'contentOptions' => ['class' => 'col-description hidden '],
				'filterOptions' => ['class' => 'col-description hidden'],
				'headerOptions' => ['class' => 'col-description hidden '],				
				'footerOptions' => ['class' => 'col-description hidden '],
				
				
				
			],
            ['attribute' => 'type',
				'label' => ServicePrice::getMeta('type', 'label'),
				'contentOptions' => ['class' => 'col-type hidden '],
				'filterOptions' => ['class' => 'col-type hidden'],
				'headerOptions' => ['class' => 'col-type hidden '],				
				'footerOptions' => ['class' => 'col-type hidden '],
				
				
				
			],
            ['attribute' => 'price',
				'label' => ServicePrice::getMeta('price', 'label'),
				'contentOptions' => ['class' => 'col-price '],
				'filterOptions' => ['class' => 'col-price'],
				'headerOptions' => ['class' => 'col-price '],				
				'footerOptions' => ['class' => 'col-price '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'price6',
				'label' => ServicePrice::getMeta('price6', 'label'),
				'contentOptions' => ['class' => 'col-price6 '],
				'filterOptions' => ['class' => 'col-price6'],
				'headerOptions' => ['class' => 'col-price6 '],				
				'footerOptions' => ['class' => 'col-price6 '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'price11',
				'label' => ServicePrice::getMeta('price11', 'label'),
				'contentOptions' => ['class' => 'col-price11 '],
				'filterOptions' => ['class' => 'col-price11'],
				'headerOptions' => ['class' => 'col-price11 '],				
				'footerOptions' => ['class' => 'col-price11 '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'enabled',
				'label' => ServicePrice::getMeta('enabled', 'label'),
				'contentOptions' => ['class' => 'col-enabled hidden '],
				'filterOptions' => ['class' => 'col-enabled hidden'],
				'headerOptions' => ['class' => 'col-enabled hidden '],				
				'footerOptions' => ['class' => 'col-enabled hidden '],
				
				
				
			],
/*
            ['class' => 'yii\grid\ActionColumn'],
*/			
        ],
    ]); ?>
</div>
