<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\components\Search_promoWidget;
use common\models\Metro;
use common\models\Okrug;
use common\models\Nalog;
use common\models\User;
use yii\helpers\Url;
$this->title = 'All addresses';
?>
<?php $this->params['breadcrumbs'] = 
		[

			[
				'label'     => 'адреса',
                'url'       =>  ['/site/addresses'],
                'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
			],
			[
				'label' => $address['address'],
			],
		
		        ];
        ?>
<section class="mobile-address-search">
    <span>поиск адреса</span>
</section>
<section class="addresses">
    <div class="address-search-popup">
        <div class="search-closer"><img src="/img/close-menu.png" alt=""></div>
        <div class="container-fluid">
            <div class="container container-to-insert">

            </div>
        </div>

    </div>
	<div class="container-fluid cloud-bg">

		<div class="container">
			<div class="row">
                <div class="col-lg-3 col-md-2 add-search-block hidden-xs">
                    <div class="add-searcher">
                        <span>поиск адреса:</span>

                    </div>
                    <?php echo Search_promoWidget::widget(['arr' => array(
                        "metro"=>$metro,
                        "searchModel"=>$searchModel,
                        "taxes"=>$taxes,
                        "districts"=>$districts,
                        "services"=>$services,
                        "document_cnt"=>$document_cnt)]); ?>

                </div>
				<div class="col-lg-8 col-md-9 col-md-offset-1 all-addresses-block">
                    <div class="offset-padding">
					<div class="add-header row">
						<div class="col-md-7">
							<div class="flex al">
								<div class="new-button al">
									new
								</div>
								<div class="address-detail-head">
									<?=$address->address;?>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-lg-offset-2 col-md-2 col-md-offset-1  no-padding-d">
							<div class="icons-div flex al">
                                <?php
                                $optionList = Yii::$app->db->createCommand('SELECT * FROM adr_options')->queryAll();
                                function pre($data){
                                    echo "<pre>", print_r($data), "</pre>";
                                    //die();
                                }



                                /*foreach ($address['options'] as $option){
                                    foreach ($optionList as $optdata){
                                        if($optdata['id'] == $option){
                                            echo "<img alt='".$optdata['name']."' class='address-icon' src='http://mgbackportal.steigenhaus.com/images/icons/options/".$optdata['icon']."' />";
                                        }
                                    }
                                }*/

                                ?>
								<!--<img src="/img/anchors.png" class="address-icon" />
								<i class="fa fa-envelope address-icon suspended" aria-hidden="true"></i>
								<i class="fa fa-home address-icon" aria-hidden="true"></i>
								<i class="fa fa-credit-card-alt address-icon" aria-hidden="true"></i>
								<img src="/img/column.png" class="address-icon"/>
								<img src="/img/compass.png" class="address-icon" />
								<img src="/img/reload.png" class="reload-icon">-->
                                <div class="order-address button-mobile desc-right">
                                    <a class="order-fancybox" href="#order-popup">
                                        <div class="order-address-button detail-font">
                                            заказать <br> адрес
                                        </div>
                                    </a>
                                </div>
							</div>



						</div>
					</div>
					<div class="address-text row">
						<div class="col-md-11"> <!--col-md-7-->
							<div class="flex">
								<div class="address-text-district">
                                    <?=$address->address;?>
								</div>
								<div class="middot">
									&middot;
								</div>
								<div class="address-text-tax">ИФНС No<?php echo Nalog::findOne($address->nalog_id)->attributes['number'];?></div>


								<div class="middot">
									&middot;
								</div>
								<div class="address-text-area">
									<div class="flex">
									<?=@$address->square;?> м <sup>2</sup>
									</div>
								</div>
							</div>
							<div class="flex second-row-address-info">
								<div class="address-text-index">
									<span>индекс</span>
								</div>
								<div class="address-text-index-value">
                                    <?=@$address->adr_index;?>
								</div>
								<div class="address-text-subway">
									<div class="flex">
										<span>метро</span>
									</div>
								</div>
								<div class="address-text-subway-value">
                                    <?php echo Metro::findOne($address->metro_id)->attributes['name'];?>
								</div>
							</div>
						</div>
						<!--<div class="col-md-3 col-md-offset-1 no-padding-d centralize-mobile">
							<div class="order-address desc-right">
		                        <a class="order-fancybox" href="#order-popup">
		                            <div class="order-address-button">
		                                заказать <br> адрес
		                            </div>
		                        </a>
		                    </div>
						</div>	-->
					</div>
					<div class="sld row" id="sld">
						<div class="col-md-11">
							<div class="image-to-change">
								<img class="img-responsive hidden" src="" />
							</div>
							<div class="address-slider">
								<ul class="sl">
                                    <?php
                                    //print_r($photos); //die();
                                        foreach($photos as $photo){
                                            echo "<li class=\"lense-search\"><img class=\"gh\" alt='".$photo["title"]."' src='".Url::to($address->getImage($photo["name"]))."'/></li>";
                                        }
                                    ?>
									<!--<li class="lense-search"><img class="gh" src="/img/sl1.png"></li>
									<li class="lense-search"><img class="gh" src="/img/sl2.png"></li>
									<li class="lense-search"><img class="gh" src="/img/sl3.png"></li>
									<li class="lense-search"><img class="gh" src="/img/sl4.png"></li>
									<li class="lense-search"><img class="gh" src="/img/sl1.png"></li>
									<li class="lense-search"><img class="gh" src="/img/sl2.png"></li>
									<li class="lense-search"><img class="gh" src="/img/sl3.png"></li>
									<li class="lense-search"><img class="gh" src="/img/sl4.png"></li>-->
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-11">
							<div class="address-prelocation-info">
								Юридический адрес находится в ЦАО города Москвы, относится к ИФНС № 3 и представляет собой современное и полностью оборудованное офисное помещение общей площадью 44,3 кв.м.  Расположение офиса: 1 км пешком от станции метро Белорусская Кольцевой линии. Офис находится на первом этаже в жилом 7-ми этажном здании кирпичного типа. Вход в помещение осуществляется через отдельный вход.
							</div>
                            <?php $latlng = explode(',', $address->gps); ?>
							<div id="map" data-lat="<?=trim($latlng[0])?>" data-lng="<?=@trim($latlng[1])?>"></div>
							<div class="address-postlocation-info">
								Юридический адрес находится в ЦАО города Москвы, относится к ИФНС № 3 и представляет собой современное и полностью оборудованное офисное помещение общей площадью 44,3 кв.м.  Расположение офиса: 1 км пешком от станции метро Белорусская Кольцевой линии. Офис находится на первом этаже в жилом 7-ми этажном здании кирпичного типа. Вход в помещение осуществляется через отдельный вход.
							</div>
						</div>

					</div>
					<div class="row address-full-info">
						<div class="col-md-12 address-owner-info">
                            <div class="icon">
                            </div>
							<div class="add-owner labels">владелец адреса</div>
						</div>
					</div>
					<div class="row address-owner-info">
                        <div class="col-md-12 flex-all">
							<div class="icon">
                            	<img class="pi-icon" src="/img/user.png" alt="some">
                            </div>
                            
							<div class="owner-name">
                                <?php
                               //  echo "<pre>", print_r(User::findOne($address['owner_id'])->attributes);
                                echo User::findOne($address->owner_id)->attributes['fullname'];?>
                            </div>
                        </div>

					</div>
					<div class="row address-owner-title">
						<div class="col-md-12 address-owner-info">
                            <div class="icon">
                            </div>
							<div class="add-owner labels">цены</div>
						</div>
					</div>
					<div class="row ">
                        <div class="col-md-12 address-owner-info">
                        <div class="icon">
                            <img class="pi-icon" src="/img/banknote.png" alt="some">
                        </div>
                        <div class="flexAll column address-extra-content">
                            <div class="flex-all">

                                <div class="service-price">
                                    <?=@$address->price6;?> <i class="fa fa-rub" aria-hidden="true"></i>
                                </div>
                                <div class="service-name">
                                    6 месяцев
                                </div>
                            </div>
                            <div class="flex-all">
                                <div class="service-price">
                                    <?=@$address->price11;?> <i class="fa fa-rub" aria-hidden="true"></i>
                                </div>
                                <div class="service-name">
                                    11 месяцев
                                </div>
                            </div>


                            <!--<div class="flex-all">
                                <div class="service-price">
                                    4 000 <i class="fa fa-rub" aria-hidden="true"></i>/месяц
                                </div>
                                <div class="service-name">
                                    предоставление рабочего места
                                </div>

                            </div>
                            <div class="flex-all">
                                <div class="service-price">
                                    1 000 <i class="fa fa-rub" aria-hidden="true"></i>/месяц
                                </div>
                                <div class="service-name">
                                    почтовое обслуживание
                                </div>
                            </div>-->
                        </div>
                        </div>
					</div>
					<div class="row address-owner-title">
						<div class="col-md-12 address-owner-info">
                            <div class="icon">
                            </div>
							<div class="add-owner labels">специльное предложение</div>
						</div>
					</div>

					<div class="row special">
                        <div class="address-owner-info col-md-12 flex-all">
                            <div class="icon">
                                <img class="pi-icon" src="/img/megaphone.png" alt="some">
                            </div>
                            <div class="address-extra-content">
                                <?php
                                $i = 0;
                                foreach ($services as $service):

                                    ?>
                                    <?php if ($service['type'] == 'Акционная'):
                                    $i++;?>
                                    <div class="flex-all extra-service">
                                        <div class="service-price">
                                            <?=$service['price6']?> <i class="fa fa-rub" aria-hidden="true"></i>
                                        </div>
                                        <div class="service-name">
                                            6 месяцев
                                        </div>
                                    </div>
                                <?php endif; endforeach;
                                if($i == 0){
                                    echo "Нет специальных предложений";
                                }?>
                            </div>
                        </div>

					</div>
					<div class="row address-owner-title">
						<div class="col-md-12 address-owner-info">
                            <div class="icon">
                            </div>
							<div class="add-owner labels">дополнительные услуги</div>
						</div>
					</div>
                    <div class="row">
                        <div class="col-md-12 address-owner-info">

                            <div class="icon">
                                <img class="pi-icon" src="/img/pin.png" alt="some">
                            </div>
                            <div class="address-extra-content">
                                <?php
                                $i = 0;
                                foreach ($services as $service):
                                if($service['type'] == 'Обычная'):
                                    $i++;
                                ?>
                                    <div class="flex-all extra-service">

                                        <div class="service-price">
                                            <?=$service['price6']?> <i class="fa fa-rub" aria-hidden="true"></i>
                                        </div>
                                        <div class="clean">
                                            <?=$service['name']?> / 6 месяцев
                                        </div>
                                    </div>
                                <?php
                                    if($i == 0){
                                        echo "Нет дополнительных услуг";
                                    }
                                    endif;
                                    endforeach;
                                ?>
                                <!--<div class="flex-all">

                                    <div class="service-price">
                                        5 000 <i class="fa fa-rub" aria-hidden="true"></i>
                                    </div>
                                    <div class="clean">
                                        уборка помещения / месяц
                                    </div>
                                </div>
                                <div class="flex-all">
                                    <div class="service-price">
                                        10 000 <i class="fa fa-rub" aria-hidden="true"></i>
                                    </div>
                                    <div class="service-name">
                                        охрана / месяц
                                    </div>
                                </div>


                                <div class="flex-all">
                                    <div class="service-price">
                                        10 000 <i class="fa fa-rub" aria-hidden="true"></i>
                                    </div>
                                    <div class="service-name">
                                        заказ пиццы / ежедневно
                                    </div>

                                </div>
                                <div class="flex-all">
                                    <div class="service-price">
                                        1 500 000 <i class="fa fa-rub" aria-hidden="true"></i>
                                    </div>
                                    <div class="service-name">
                                        разработка промо-сайта
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    </div>
					<!--<div class="row address-owner-info">
						<div class="col-md-1">
							<div class="icon">
	                            <img class="pi-icon" src="/img/pin.png" alt="some">
	                        </div>
                        </div>
                        <div class="col-md-3">
							<div class="clean-6month-price ser-price">
                            	5 000 <i class="fa fa-rub" aria-hidden="true"></i>
                            </div>
                            <div class="sec-price ser-price">
                            	10 000 <i class="fa fa-rub" aria-hidden="true"></i>
                            </div>
                            <div class="pizza-price ser-price">
                            	10 000 <i class="fa fa-rub" aria-hidden="true"></i>
                            </div>
                            <div class="site-price ser-price">
                            	1 500 000 <i class="fa fa-rub" aria-hidden="true"></i>
                            </div>
						</div>
						<div class="col-md-5">
							<div class="clean">
                            	уборка помещения / месяц
                            </div>
                            <div class="sec">
                            	охрана / месяц
                            </div>
                            <div class="pizza">
                            	заказ пиццы / ежедневно
                            </div>
                            <div class="pizza">
                            	разработка промо-сайта
                            </div>
						</div>
					</div>-->

					<!--<div class="row address-owner-info order-count-for-address">
						<div class="col-md-1">
							<div class="icon">
                            	<img class="pi-icon" src="/img/note.png" alt="some">
                            </div>
                            
						</div>
						<div class="col-md-3">
							<div class="pi-number">
                            	125 084
                            </div>
                           
						</div>
						<div class="col-md-3">
							<div class="order-count-text">
                            	заказов исполненных
								по данному адресу
								за прошедший месяц
                            </div>
                            
						</div>
					</div>-->
                    <div class="row special">
                        <div class="col-md-12 address-owner-info">
                            <div class="icon">
                                <img class="pi-icon" src="/img/note.png" alt="some">
                            </div>
                            <div class="address-extra-content">
                                <div class="flex-all">
                            <div class="pi-number">
                                <?=$orders_count;?>
                            </div>
                            <div class="service-name">
                                <div class="order-count-text">
                                    заказов исполненных
                                    по данному адресу
                                    за прошедший месяц
                                </div>
                            </div>
                                </div>
                            </div>
                        </div>
                    </div>


					<div class="row">
						<div class="col-md-11">
							<div class="order-address pull-left">
		                        <a href="">
		                            <div class="order-address-button">
		                                заказать <br> адрес
		                            </div>
		                        </a>
		                    </div>
	                    </div>
					</div>
					<div class="row address-promo">
						<div class="col-md-4">
								<div class="address-card">
									<div class="address-img">
										<?= Html::img('@web/img/1.jpg', ['alt'=>'some', 'class'=>'img-responsive']);?>
										<div class="new-address-label">
											New
										</div>
									</div>
									<div class="address-info-cnt">
										<div class="address-price">
											17 000 <span><i class="fa fa-rub" aria-hidden="true"></i></span>
										</div>
										<div class="address">
											
												ул. Спартаковская <br>
												ЦАО <br>
												ИФНС No1
												<div class="address-area">
													79 <span>м<sup>2</sup></span>
												</div>	

											
										</div>
										<div class="address-icons">
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-home" aria-hidden="true"></i>
											<i class="fa fa-credit-card-alt" aria-hidden="true"></i>
											<i class="fa fa-envelope" aria-hidden="true"></i>
										</div>
										<div class="address-order">
											<a href="">заказать</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="address-card">
									<div class="address-img">
										<?= Html::img('@web/img/1.jpg', ['alt'=>'some', 'class'=>'img-responsive']);?>
										<div class="new-address-label">
											New
										</div>
									</div>
									<div class="address-info-cnt">
										<div class="address-price">
											17 000 <span><i class="fa fa-rub" aria-hidden="true"></i></span>
										</div>
										<div class="address">
											
												ул. Спартаковская <br>
												ЦАО <br>
												ИФНС No1
												<div class="address-area">
													79 <span>м<sup>2</sup></span>
												</div>	

											
										</div>
										<div class="address-icons">
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-home" aria-hidden="true"></i>
											<i class="fa fa-credit-card-alt" aria-hidden="true"></i>
											<i class="fa fa-envelope" aria-hidden="true"></i>
										</div>
										<div class="address-order">
											<a href="">заказать</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="address-card">
									<div class="address-img">
										<?= Html::img('@web/img/1.jpg', ['alt'=>'some', 'class'=>'img-responsive']);?>
										<div class="new-address-label">
											New
										</div>
									</div>
									<div class="address-info-cnt">
										<div class="address-price">
											17 000 <span><i class="fa fa-rub" aria-hidden="true"></i></span>
										</div>
										<div class="address">
											
												ул. Спартаковская <br>
												ЦАО <br>
												ИФНС No1
												<div class="address-area">
													79 <span>м<sup>2</sup></span>
												</div>	

											
										</div>
										<div class="address-icons">
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-home" aria-hidden="true"></i>
											<i class="fa fa-credit-card-alt" aria-hidden="true"></i>
											<i class="fa fa-envelope" aria-hidden="true"></i>
										</div>
										<div class="address-order">
											<a href="">заказать</a>
										</div>
									</div>
								</div>
							</div>
					</div>
                    </div>
				</div>
			</div>
		</div>
	</div>

</section>
<?php $this->registerJsFile("/js/map.js", ["position"=>\yii\web\View::POS_END]); ?>



