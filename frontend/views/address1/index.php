<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\components\Search_promoWidget;
use frontend\components\PagerWidget;
use yii\helpers\Url;
use common\models\Metro;
use common\models\Okrug;
use common\models\Nalog;
use common\models\Option;
$this->title = 'All addresses';
?>
<?php $this->params['breadcrumbs'] = 
		[

			[
				'label' => 'адреса',
			],
		
		        ];
        ?>
<section class="mobile-address-search">
    <span>поиск адреса</span>
</section>
<?php //echo PagerWidget::widget();?>
<section class="addresses">
    <div class="address-search-popup">
        <div class="search-closer"><img src="/img/close-menu.png" alt=""></div>
        <div class="container-fluid">
            <div class="container container-to-insert">

            </div>
        </div>

    </div>
	<div class="container-fluid cloud-bg">

		<div class="container">
			<div class="row">
                <div class="col-md-2 col-lg-3 add-search-block hidden-xs">
                    <div class="add-searcher">
                        <span>поиск адреса:</span>

                    </div>
                    <?php echo Search_promoWidget::widget(['arr' => array(
                        "metro"=>$metro,
                        "searchModel"=>$searchModel,
                        "taxes"=>$taxes,
                        "districts"=>$districts,
                        "services"=>$services,
                        "document_cnt"=>$document_cnt)]); ?>

                </div>
				<div class="col-md-9 col-lg-8 col-md-offset-1 all-addresses-block">
                    <div class="offset-padding">
					<div class="row">
						<div class="addresses-list">
                            <?php
                            //$optionList = Yii::$app->db->createCommand('SELECT * FROM adr_options')->queryAll();
                            $optionList = Option::find()->all();
                            function pre($data){
                                echo "<pre>", print_r($data), "</pre>";
                                //die();
                            }
                          // pre($optionList);

                                foreach($addresses as $address):
                                    //pre($address->photos_as_array);
                                    if(count($address->photos_as_array)){
                                        $mainPic = $address->photos_as_array[0];
                                    }



                            ?>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="address-card">
                                    <div class="address-img">
                                        <a href="<?=Url::to(["/address/{$address->id}"]); ?>">
                                            <?php if(isset($mainPic)) {
                                                //echo Html::img('@web/img/hm.png', ['alt' => 'some', 'class' => 'img-responsive']);
                                                // echo "<img src='http://mgbackportal.steigenhaus.com".$mainPic["name"]."'/>";
                                                echo "<img src='".$address::getImage(Url::to($mainPic["name"]))."'/>";
                                            }

                                            else{
                                                echo Html::img('@web/img/hm.png', ['alt'=>'some', 'class'=>'img-responsive']);
                                            }
                                            ?>
                                        </a>
                                    </div>
                                    <div class="address-info-cnt">
                                        <div class="address-price">
                                            17 000 <span><i class="fa fa-rub" aria-hidden="true"></i></span>
                                        </div>
                                        <div class="address">

                                            <div><?=$address->address?></div>
                                            <?php // echo Metro::findOne($address['metro_id'])->name; die(); ?>
                                            <div> <?php echo @Metro::findOne($address->metro_id)->name; ?> </div>
                                            <?php //die('in');?>
                                            <div> ИФНС No<?php echo @Nalog::findOne($address->nalog_id)->attributes['number'];?> </div>
                                            <?php if(isset($address->square) && !is_null($address->square)): ?>
                                                <div class="address-area">
                                                    <?= $address->square; ?> <span>м<sup>2</sup></span>
                                                </div>
                                            <?php endif; ?>


                                        </div>
                                        <div class="address-icons">
                                            <?php
                                                foreach ($address->options as $option){
                                                    foreach ($optionList as $optdata){
                                                        if($optdata['id'] == $option){
                                                            //echo "<img alt='".$optdata['name']."' src='http://mgbackportal.steigenhaus.com/images/icons/options/".$optdata['icon']."' />";
                                                            echo "<div><img  alt='".$optdata['name']."' src='".URL::to(["/storage/icons/options/".$optdata['icon']])."' ></div>";

                                                        }
                                                    }
                                                }
                                            ?>
                                            <!--<i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-home" aria-hidden="true"></i>
                                            <i class="fa fa-credit-card-alt" aria-hidden="true"></i>
                                            <i class="fa fa-envelope" aria-hidden="true"></i>-->
                                        </div>
                                        <div class="address-order order-address">
                                            <!--<a class="order-fancybox" href="#order-popup">заказать</a>-->
                                            <?php if(Yii::$app->user->identity == null): ?>
                                                <a class="order-fancybox" href="<?=Url::to(["/site/login?back=/order/{$address->id}"])?>">заказать</a>
                                            <?php else: ?>
                                                <a class="order-fancybox" href="<?=Url::to(["/address/{$address->id}/buy"/*"/order/create?address_id={$address->id}"*/])?>">заказать</a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>

                            <?php /*
							<div class="col-md-4">
								<div class="address-card">
									<div class="address-img">
                                        <a href="<?=Url::to("/index.php/site/addresses/1"); ?>">
										<?= Html::img('@web/img/hm.png', ['alt'=>'some', 'class'=>'img-responsive']);?>
                                        </a>
									</div>
									<div class="address-info-cnt">
										<div class="address-price">
											17 000 <span><i class="fa fa-rub" aria-hidden="true"></i></span>
										</div>
										<div class="address">
											
												<div>ул. Спартаковская</div>
												<div> ЦАО </div>
												<div> ИФНС No1 </div>
												<div class="address-area">
													79 <span>м<sup>2</sup></span>
												</div>	

											
										</div>
										<div class="address-icons">
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-home" aria-hidden="true"></i>
											<i class="fa fa-credit-card-alt" aria-hidden="true"></i>
											<i class="fa fa-envelope" aria-hidden="true"></i>
										</div>
										<div class="address-order order-address">
											<a class="order-fancybox" href="#order-popup">заказать</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="address-card">
									<div class="address-img">
                                        <a href="<?=Url::to("/index.php/site/addresses/1"); ?>">
										    <?= Html::img('@web/img/hm.png', ['alt'=>'some', 'class'=>'img-responsive']);?>
                                        </a>
										<div class="new-address-label">
											New
										</div>
									</div>
									<div class="address-info-cnt">
										<div class="address-price">
											17 000 <span><i class="fa fa-rub" aria-hidden="true"></i></span>
										</div>
										<div class="address">
											
												ул. Спартаковская <br>
												ЦАО <br>
												ИФНС No1
												<div class="address-area">
													79 <span>м<sup>2</sup></span>
												</div>	

											
										</div>
										<div class="address-icons">
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-home" aria-hidden="true"></i>
											<i class="fa fa-credit-card-alt" aria-hidden="true"></i>
											<i class="fa fa-envelope" aria-hidden="true"></i>
										</div>
										<div class="address-order order-address">
											<a class="order-fancybox" href="#order-popup">заказать</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="address-card">
									<div class="address-img">
                                        <a href="<?=Url::to("/index.php/site/addresses/1"); ?>">
										    <?= Html::img('@web/img/hm.png', ['alt'=>'some', 'class'=>'img-responsive']);?>
                                        </a>
										<div class="new-address-label">
											New
										</div>
									</div>
									<div class="address-info-cnt">
										<div class="address-price">
											17 000 <span><i class="fa fa-rub" aria-hidden="true"></i></span>
										</div>
										<div class="address">
											
												ул. Спартаковская <br>
												ЦАО <br>
												ИФНС No1
												<div class="address-area">
													79 <span>м<sup>2</sup></span>
												</div>	

											
										</div>
										<div class="address-icons">
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-home" aria-hidden="true"></i>
											<i class="fa fa-credit-card-alt" aria-hidden="true"></i>
											<i class="fa fa-envelope" aria-hidden="true"></i>
										</div>
										<div class="address-order order-address">
											<a class="order-fancybox" href="#order-popup">заказать</a>
										</div>
									</div>
								</div>
							</div>
                             */?>
						</div>
                    </div>
						<div>
                            <div class="row">
                            <div class="visible-xs">
                                <div class="col-md-12 left-padding">
                                    <div class="addresses-promo-block">
                                        <div class="col-md-12 promo-information">
                                            <div class="pi-container">
                                                <div class="icon">
                                                    <?= Html::img('@web/img/diamond.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                                </div>
                                                <div class="pi-txt">
                                                    надежность
                                                </div>
                                                <div class="pi-message">
                                                    все адреса  проверены экспертами
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 promo-information">
                                            <div class="pi-container">
                                                <div class="icon">
                                                    <?= Html::img('@web/img/banknote.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                                </div>
                                                <div class="pi-txt">
                                                    реальная цена!
                                                </div>
                                                <div class="pi-message">
                                                    многие собственники предоставили  <br> эксклюзивные скидки для продаж
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 promo-information">
                                            <div class="pi-container">
                                                <div class="icon">
                                                    <?= Html::img('@web/img/like.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                                </div>
                                                <div class="pi-txt">
                                                    чистота сделок
                                                </div>
                                                <div class="pi-message">
                                                    и максимальная скорость
                                                    платежей <br> гарантированы
                                                    администрацией портала
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
						<div class="col-md-12">

                            <?php
                                $widget = new PagerWidget();
                                $widget->pagination = $dataProvider->pagination;
                                echo $widget->run();
                            ?>

						</div>
                            </div>
						</div>
						<div class="extra-information">
							<div class="col-md-12">
								<div class="col-md-4 promo-text-block">
									<span class="bottom-text-block">Офисы бывают двух типов: </span> в специализированных бизнес-центрах и на первых этажах жилых домов, вторых этажах магазинов и в прочих, зданиях. Но данная услуга представляет собой не только поиск помещения. Это процесс состоящий из нескольких этапов: запрос от арендатора, составления заявки, вдумчивый поиск по нашей специализированной базе данных, низкую комиссию, офис для Вас и тщательное оформление договора аренды офиса или нежилого помещения по всем юридическим и правовым нормам.
								</div>
								<div class="col-md-4 promo-text-block">
									Кроме того, <span class="bottom-text-block">обратившись в специализированную фирму,</span> предоставляющую услугу «аренда офисов в Москве», заказчик уже работает с одним человеком, которому объясняет всё, что ему требуется, а не решает одновременно множество дел – аренда офиса, подготовка к переезду, свои текущие дела и т.п. Агент по аренде офиса самостоятельно объезжает объекты и представляет на суд заказчику самые подходящие варианты. Цена за подобную услугу, помимо адреса и наличие мест для парковки.
								</div>
								<div class="col-md-4 promo-text-block">
									<span class="bottom-text-block">Цена за подобную услугу,</span> помимо адреса и наличие мест для парковки, формируется из таких факторов как удаление от метро/остановок общественного транспорта, площади помещения, наличия ремонта (в ряде случаев возможно снять офисное помещение без внутренней отделки или в разной степени её готовности), а также предполагаемого срока найма. Неспециалисту непросто всё это учесть и тогда поиск подходящего помещения может превратиться в затяжной кошмар для любой организации.
								</div>
							</div>
						</div>
                </div>

				</div>
			</div>
		</div>
	</div>

</section>