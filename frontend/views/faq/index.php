<?php
use app\components\ArticlesWidget;
use frontend\models\Page;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


$this->params['breadcrumbs'][] = 'Часто задаваемые вопросы';



?>


<section class="addresses faq-page">
    <div class="container-fluid cloud-bg">

        <div class="container">

            <div class="row">
                <div class="article">
                    <div class="col-md-3 add-search-block">
                        <div class="row widget-filter">
                            <div class="col-md-12">
                                <div class="addresses-promo-block">
                                    <div class="col-md-12 promo-information">
                                        <div class="pi-container">
                                            <div class="icon">
                                                <?= Html::img('@web/img/diamond.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                            </div>
                                            <div class="pi-txt">
                                                надежность
                                            </div>
                                            <div class="pi-message">
                                                все адреса  проверены экспертами
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 promo-information">
                                        <div class="pi-container">
                                            <div class="icon">
                                                <?= Html::img('@web/img/banknote.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                            </div>
                                            <div class="pi-txt">
                                                реальная цена!
                                            </div>
                                            <div class="pi-message">
                                                многие собственники предоставили  <br> эксклюзивные скидки для продаж
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 promo-information">
                                        <div class="pi-container">
                                            <div class="icon">
                                                <?= Html::img('@web/img/like.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                            </div>
                                            <div class="pi-txt">
                                                чистота сделок
                                            </div>
                                            <div class="pi-message">
                                                и максимальная скорость
                                                платежей <br> гарантированы
                                                администрацией портала
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-md-8 col-md-offset-1 all-addresses-block">

                        <div class="article-item">
                            <div class="col-md-11">
                                <div class="article-header">
                                    <h1>Часто задаваемые вопросы</h1>
                                </div>
                            </div>
                            <div class="col-md-1" >
                                <img src="/img/reload.png" class="pull-right articles-reload" alt="">
                            </div>
                        </div>


                        <div class="article-text article-item">
                            <div class="col-md-11 col-xs-12">
                                <?=\frontend\components\FaqWidget::widget(['template' => 'list'])?>
                            </div>
                        </div>
                        <div class="soc-block">
                            <div class="article">
                                <div class="row">
                                    <div class="share-soc">
                                        <div class="socs al">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </div>
                                        <div class="socs al">
                                            <i class="fa fa-twitter" aria-hidden="true"></i>
                                        </div>
                                        <div class="socs al">
                                            <i class="fa fa-vk" aria-hidden="true"></i>
                                        </div>
                                        <div class="socs al">
                                            <i class="fa fa-google-plus" aria-hidden="true"></i>
                                        </div>
                                        <div class="socs al">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="soc-block">
                            <div class="article">
                                <div class="row">
                                    <?php if(isset($_SERVER["HTTP_REFERER"])): ?>
                                        <a href="<?=$_SERVER["HTTP_REFERER"];?>">
                                            вернуться назад
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="article">
                            <div class="row">
                                <hr class="article-hr" />
                            </div>
                        </div>
<!--					
                        <div class="articles-widget">
                            <div class="col-md-12 pull-right">
                                <div class="row">
                                    <div class="">
                                        <?php echo ArticlesWidget::widget();?>
                                    </div>
                                </div>
                            </div>
                        </div>
-->						
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>


