<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Faq;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Faq::getMeta('created_at_view', 'label'), 'hint'=>''.Faq::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],
				
		],		
	],
		
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'question' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Faq::getMeta('question', 'label'), 'hint'=>''.Faq::getMeta('question', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'answer' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Faq::getMeta('answer', 'label'), 'hint'=>''.Faq::getMeta('answer', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'role_as_array' => ['type'=>Form::INPUT_CHECKBOX_LIST, 'label'=>Faq::getMeta('role', 'label'), 'items' => Faq::getList('role'), 'hint'=>''.Faq::getMeta('role', 'comment'), 'options'=>['multiple' => true]],
						'enabled' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>Faq::getMeta('enabled', 'label'), 'hint'=>''.Faq::getMeta('enabled', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Faq */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Faq::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
