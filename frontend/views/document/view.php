<?php

use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use frontend\models\Document;
use common\models\Entity;

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;


$this->title = $model -> getCaption();
$this->params['breadcrumbs'] =
    [

        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/customer'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,
        ],

    ];


//var_dump($model -> baseClassName());exit;

//var_dump($model -> owner -> attributes);

?>

<h2><?=$model -> getCaption()?></h2>
<div>Плательшик: <?=$model -> recipient -> getCaption()?> [<?=$model -> recipient -> user -> fullname?>]</div>
<div>Получатель: <?=$model -> owner -> getCaption()?> [<?=$model -> owner -> user -> fullname?>]</div>
<div><?=$model -> info?></div>

<div>Сумма: <?=Yii::$app->formatter->asCurrency($model -> summa)?></div>

<?php
//$obj = Entity::byId($model -> owner_id);
//var_dump($obj -> getCaption());
?>