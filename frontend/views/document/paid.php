<?php
$order = $payment -> invoice -> order;

$this->title = 'Оплата по карте';
$path = [
		[
            'label'     => 'Личный кабинет',
            'url'       =>  ['/'.$this-> context -> getCabinet()],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
		[
            'label'     => 'Заказы',
            'url'       =>  ['/'.$this-> context -> getCabinet().'/order'],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
];

if (!is_null($order)) {
	$path[] = 
		[
            'label'     => 'Заказа №'.$order -> number,
            'url'       =>  ['/'.$this-> context -> getCabinet().'/order/view/'.$order -> id],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ];
	$path[] = 
		[
            'label'     => 'Счета',
            'url'       =>  ['/'.$this-> context -> getCabinet().'/order/document/'.$order -> id],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ];	
};

	$path[] = [
            'label' => $this -> title,
        ];
	
$this->params['breadcrumbs'] = $path;
	
?>

<h2>Оплата по карте</h2>
<?if($payStatus){?>
<p>Счет <?=$payment -> invoice -> number?> оплачен успешно</p>
<?}else{?>
    <p><?=$payMessage?></p>
<?}?>
