<?php
use \kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\Document;
use frontend\models\Invoice;
use frontend\models\User;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;


$this->title = 'Расчеты по услугам портала';
$user = User::getCurrentUser();
if ($user -> checkRole(User::ROLE_OWNER)) $role = 'owner';
if ($user -> checkRole(User::ROLE_CUSTOMER)) $role = 'customer';
$this->params['breadcrumbs'] =
    [

        [
            'label'     => 'Личный кабинет',
            'url'       =>  ['/'.$role],
            'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
        ],
        [
            'label' => $this -> title,
        ],

    ];


?>

<h1><?=$this->title?></h1>
<div class="alone-filter">
<?=\frontend\components\FilterWindow::getFilterButton($searchModel)?>
</div>
<div style="clear:both"></div>
<?php


echo GridView::widget([
    'dataProvider'=>$dataProvider,

//    'panel'=>['type'=>'primary', 'heading'=>$this->title],
    'columns'=>[
//        ['class'=>'kartik\grid\SerialColumn'],
		[
            'attribute'=>'id',	
			'label' => 'No',
        ],
				
		
		[
            'attribute'=>'date',			
			'label' => 'дата операции',
			'format' => 'datetime',
        ],
		
		[
            'attribute'=>'operation',			
			'label' => 'вид операции',
			'format' => 'raw',
			'value' => function($data) {
				if ($data['is_invoice']) { // Для счета указываем номер и ставим ссылку на скачивание
					$invoice = Invoice::findOne([$data['id']]);
					if ($invoice) {
						return Html::a($data['operation'].' №'.$invoice -> number, Url::to(['document/show-pdf', 'id' => $invoice -> id]));
					} else return '';
				} else return $data['operation'];
			}
        ],
		
		[
            'attribute'=>'summa',	
			'label' => 'сумма',
			'value' => function ($data) {
				if (isset($data['summa'])) {
					$value = Yii::$app->formatter->asCurrency($data['summa']);
					return $value;
				} else {
					return '';
				}
				
			}
        ],
		
		[
            'attribute'=>'state',
			'label' => 'статус счета',			
			'format' => 'raw',
			'value' => function($data) {
				if (is_null($data['state'])) {
					$value = '';
				} else {
					if ($data['state'] == Invoice::STATE_NOPAID) {
						$value = 'не оплачен '.					
						Html::a('<i class="fa fa-credit-card" style="margin-left: 5px;"></i> ', ['/document/pay', 'id' => $data['id']], ['title' => 'Оплатить картой']);
						
					} else {
						
						$value = 'оплачен';
					}
				};
				return $value;
			},
        ],
	
	
		[
            'attribute'=>'invoice_id',
			'label' => 'счет',						
			'value' => function($data) {
				$model = Document::findOne($data['invoice_id']);				
				return $model?$model -> getCaption('счет №%number% от %created_at% на сумму %summa% р.'):'';
				//return  'Счет №'.$data['invoice_number'].' на сумму '.(int)$data['invoice_summa'].'руб.';
			},
//			'group'=>true,
        ],
		
/*		
		[
			'attribute'=>'payment_date',
		],
		[
			'attribute'=>'payment_summa',
		],
*/		
		
    ],	
	
]);

/* */
?>
    </div>
</div>



<?=$this->render('_filter', [
        'searchModel' => $searchModel,
	
]) 
?>		