<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Metro;
use common\models\Okrug;
use common\models\Nalog;
use common\models\Option;
use frontend\models\Address;

$this->title = '';


?>
            <div class="address-search">

                <div class="tab-block row">
                    <div class="col-md-3 no-padding switch-left">
                        <div class="tab active-tab distr-tab" data-block="district">
                            по округу
                        </div>
                        <div class="district-mobile-container"></div>
                    </div>
                    <div class="col-md-3 no-padding switch-mode">
                        <div class="tab tax-tab" data-block="tax">
                            по номеру налоговой
                        </div>
                        <div class="tax-mobile-container"></div>
                    </div>
                    <div class="col-md-3 no-padding switch-mode">
                        <div class="tab subway-tab" data-block="subway">
                            по метро
                        </div>
                        <div class="subway-mobile-container"></div>
                    </div>
                    <div class="col-md-3 no-padding switch-right">
                        <div class="tab other-tab" data-block="other">
                            другие параметры
                        </div>
                        <div class="other-mobile-container"></div>
                    </div>
                </div>
                <div class="tab-content">
<?php $form = ActiveForm::begin(['action' =>['address/index'], 'method' => 'get']);?>															
                <div class="row search-way district display">                    
				
                    <div class="flex search-form">						
                    <div class="col-md-8 col-md-offset-1">
                        <div class="checkbox-block row">
						

						
<?php							
echo FormGrid::widget([
    'model' => $searchModel,
    'form' => $form,
    'rows' => [
			[
				'attributes' => [
					//'okrug_id' => ['type'=>Form::INPUT_RADIO_LIST, 'label'=>'', 'items' => Okrug::listAll(), 'hint'=>'', 'options'=>['class' => 'checkbox-list']],
						'okrug_id' => ['type'=>Form::INPUT_CHECKBOX_LIST, 'label'=>'', 'items' => Okrug::listAll(false, false, true), 'hint'=>'', 'options'=>['class' => 'checkbox-list']],
				],			
			]
		]
	]);							
?>							

                            </div>
                        </div>
						
					<div class="col-md-3">
                        <div class="checkbox-block fix-filter-button">
                        <div class="public-address">
                            <?php echo Html::submitButton("найти", ['class' => 'find']); ?>
                        </div>
                        </div>
                    </div>						
															
                    </div>
					

                    </div>
	                   								
                <div class="row search-way tax">
                    
                    <div class="flex search-form">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="checkbox-block row">
                            <div class="checkbox-container">
                                <?php 
								
echo FormGrid::widget([
    'model' => $searchModel,
    'form' => $form,
    'rows' => [
			[
				'attributes' => [
					'nalog_id' => ['type'=>Form::INPUT_CHECKBOX_LIST, 'label'=>'', 'items' => Nalog::listAll("%number%", false, true), 'hint'=>'', 'options'=>['multiple' => false, 'class' => 'checkbox-list']],
				],			
			]
		]
	]);								
	?>
																
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="checkbox-block fix-filter-button">
                            <div class="public-address">
                                <?php echo Html::submitButton("найти", ['class' => 'find']); ?>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                </div>
                <div class="row search-way subway">
                    
                    <div class="col-md-8 col-md-offset-1">
                        <div class="checkbox-block row">
						
<?php							
echo FormGrid::widget([
    'model' => $searchModel,
    'form' => $form,
    'rows' => [
			[
				'attributes' => [
					'metro_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>'', 'items' => Address::getListValues('metro_id'), 'hint'=>Address::getMeta('metro_id', 'comment'), 'options'=>[]],						
				],			
			]
		]
	]);							
?>													

						

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="checkbox-block fix-filter-button">
                            <div class="public-address">
                                <?php echo Html::submitButton("найти", ['class' => 'find']); ?>
                                <!--<div class="find">
                                    найти
                                </div>-->
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row search-way other">
                    
                    <div class="flex search-form">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="checkbox-block row">
<?php							
echo FormGrid::widget([
    'model' => $searchModel,
    'form' => $form,
    'rows' => [
			[
				'attributes' => [
					'options_id' => ['type'=>Form::INPUT_CHECKBOX_LIST, 'label'=>'', 'items' => Option::listAll(Address::getMeta('options', 'itemtemplate')), 'hint'=>''.Address::getMeta('options', 'comment'), 'options'=>['multiple' => true, 'class' => 'options-checkbox-list'] ],
				],			
			]
		]
	]);							
?>													


                        </div>
                    </div>
                        <div class="col-md-3">
                            <div class="checkbox-block fix-filter-button">
                                <div class="public-address">
                                    <?php echo Html::submitButton("найти", ['class' => 'find']); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
				
<?php ActiveForm::end();?>						
				</div>
			
				
                </div>           
        

