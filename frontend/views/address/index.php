<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
use app\components\Search_promoWidget;
use frontend\components\PagerWidget;
use yii\helpers\Url;
use common\models\Metro;
use common\models\Okrug;
use common\models\Nalog;
use common\models\Option;
use common\models\ServiceName;
use common\models\Document;
use frontend\models\Address;

$this->title = 'Все адреса';

$searchCaption = ($okrug?'в '.$okrug -> getCaption():'').($nalog?'по '.$nalog -> getCaption():'').($metro?'у '.$metro -> getCaption():'').($options?$options -> description:'');



$breadcrumbs[] = ['label' => 'адреса', 'url' => Url::toRoute('index'), 'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL];
if ($searchCaption > '') $breadcrumbs[] = ['label' => $searchCaption];
		
$this->params['breadcrumbs'] = $breadcrumbs;



 
$addresses = $dataProvider->getModels();

?>		


		
		


		<div class="container">
			<div class="row">
                <div class="col-md-3 col-lg-3 col-sm-12 add-search-block hidden-xs">
                    <div class="add-searcher">
                        <span>поиск адреса:</span>

                    </div>

					<div class="widget-separator-with-background">
                    <?php 

					echo $this->render('_link_filter', $vars);
					
					echo $this->render('_promo', []); 
					?>	
					</div>	
						
                </div>
				<div class="col-md-9 col-lg-8 col-lg-offset-1 col-md-offset-0 col-sm-offset-0 col-sm-12 all-addresses-block">
                    <div class="offset-padding">
                        <h1 class="address-detail-head">
                            <?=Yii::$app->controller -> h1?>
                        </h1>
					<div class="row">

						<div class="addresses-list">

                            <?php

                            $optionList = Option::find()->all();
                                //pre($optionList);
								//$addresses = Address::find() -> all();
								if(count($addresses)):
                                foreach($addresses as $address):
                            ?>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="address-card">
                                    <div class="address-img">

                                        <a href="<?=Url::to(['address/'.$address->id])?>">
                                            <?php 
												$images = $address->photos_as_array;
												$pic = ($images && count($images) > 0) ? $images[0] : false;
																						
												echo $pic
                                                    ?Html::img($address::getImage(Url::to($pic["name"])), ['alt' => $pic["title"], 'class'=>'img-responsive'])
                                                    :Html::img('@web/img/hm.png', ['alt'=>'Юридический адрес', 'class'=>'img-responsive']);
                                            ?>
                                        </a>
                                    </div>
                                    <div class="address-info-cnt">
                                        <?php if ($address->hasOption('new')): ?>
                                            <div class="new-address-label">
                                                New
                                            </div>
                                        <?php endif; ?>
                                        <div class="address-price">
                                            <?=round($address->getPriceDisplayed())?> <span><i class="fa fa-rub" aria-hidden="true"></i></span>
                                        </div>
                                        <div class="address">
                                            <a href="<?=Url::to(['address/'.$address->id])?>" class="card-filter"><?=$address->address?></a>
											<?=@$address->metro?Html::a($address->metro->getCaption(), Url::to(['addresses/metro/'.$address->metro->alias]), ['class'=>["card-filter"]]):''?>
                                            <?=@$address->nalog?Html::a($address->nalog->getCaption(), Url::to(['addresses/nalog/'.$address->nalog->number]), ['class'=>["card-filter"]]):''?>
                                            <?=@$address->okrug?Html::a($address->okrug->getCaption(), Url::to(['addresses/okrug/'.$address->okrug->alias]), ['class'=>["card-filter"]]):''?>
											<?=@$address->square?Html::tag('div', $address->square."<span>м<sup>2</sup></span>", ['class' => "address-area"]):''?>
                                                                          
                                        </div>
                                        <div class="address-icons"> <!--style="margin-bottom: 40px"-->
											<?=$address->getOptions('<a title="%name%">%icon%</a>')?>
                                        </div>										
                                        <div class="address-order order-address">
											<a class="order-fancybox" href="<?=Url::to(["/address/{$address->id}/buy"/*"/order/create?address_id={$address->id}"*/])?>">заказать</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            <?php else: ?>
                                <div class="address-not-found">
                                    Не найдено ни одного адреса
                                </div>
                            <?php endif; ?>

							<div style="clear:both"></div>
							<?=\yii\widgets\LinkPager::widget([
								'pagination' => $dataProvider->pagination,
								'nextPageLabel'=>'<img src="/img/triangle_right.png" alt="">',
								'prevPageLabel'=>'<img src="/img/triangle_left.png">',
                                'lastPageLabel' => "<img src='/img/last_page.png'>",
                                'firstPageLabel' => "<img src='/img/first_page.png'>",
                                'lastPageCssClass' => "pagination-last",
                                'firstPageCssClass' => "pagination-first",
                                'prevPageCssClass' => "pagination-prev",
                                'nextPageCssClass' => "pagination-next",
                                'maxButtonCount' => 5,
                                "pageCssClass"=>"usual",
                                "activePageCssClass"=>"active-pag",
                            ]);?>
							
						</div>
                    </div>
					
					
                    <div>
                        <div class="row">
                            <div class="visible-xs">
                                <div class="col-md-12 left-padding">
                                    <div class="addresses-promo-block">
                                        <div class="col-md-12 promo-information">
                                            <div class="pi-container">
                                                <div class="icon">
                                                    <?= Html::img('@web/img/diamond.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                                </div>
                                                <div class="pi-txt">
                                                    надежность
                                                </div>
                                                <div class="pi-message">
                                                    все адреса  проверены экспертами
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 promo-information">
                                            <div class="pi-container">
                                                <div class="icon">
                                                    <?= Html::img('@web/img/banknote.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                                </div>
                                                <div class="pi-txt">
                                                    реальная цена!
                                                </div>
                                                <div class="pi-message">
                                                    многие собственники предоставили  <br> эксклюзивные скидки для продаж
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 promo-information">
                                            <div class="pi-container">
                                                <div class="icon">
                                                    <?= Html::img('@web/img/like.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                                                </div>
                                                <div class="pi-txt">
                                                    чистота сделок
                                                </div>
                                                <div class="pi-message">
                                                    и максимальная скорость
                                                    платежей <br> гарантированы
                                                    администрацией портала
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">



                            </div>
                        </div>
                    </div>
                    <div class="extra-information">
                        <div class="col-md-12">

						<?=\frontend\components\SeoTextWidget::widget([
							'options' => ["class" => "col-md-4 promo-text-block"]
						]);?>

                        </div>
                    </div>
                </div>

				</div>
			</div>
		</div>

