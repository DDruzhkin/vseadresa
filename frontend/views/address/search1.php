<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Metro;
use common\models\Okrug;
use common\models\Nalog;
use common\models\Option;
use frontend\models\Address;

$this->title = '';
?>
<div class="mobile-accordeon-block">

</div>
<section class="visible-xs mobile-address-wrapper">
    <div class="container-fluid">
        <div class="container address-mobile-section">

        </div>
    </div>
</section>
<section class="search-section">
    <div class="container-fluid">
        <div class="container">
            <div class="address-search">
                <div class="tab-block row">
                    <div class="col-md-3 no-padding switch-left">
                        <div class="tab active-tab distr-tab" data-block="district">
                            по округу
                        </div>
                        <div class="district-mobile-container"></div>
                    </div>
                    <div class="col-md-3 no-padding switch-mode">
                        <div class="tab tax-tab" data-block="tax">
                            по номеру налоговой
                        </div>
                        <div class="tax-mobile-container"></div>
                    </div>
                    <div class="col-md-3 no-padding switch-mode">
                        <div class="tab subway-tab" data-block="subway">
                            по метро
                        </div>
                        <div class="subway-mobile-container"></div>
                    </div>
                    <div class="col-md-3 no-padding switch-right">
                        <div class="tab other-tab" data-block="other">
                            другие параметры
                        </div>
                        <div class="other-mobile-container"></div>
                    </div>
                </div>
                <div class="tab-content">
                <div class="row search-way district display">
                    <?php $form = ActiveForm::begin(['action' =>['address/index'], 'method' => 'get']);?>
                    <div class="flex search-form">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="checkbox-block row">
                            <div class="checkbox-container">
							 <?php 
							// перед выводом отсортируем список округов по аббревиатуре
							
							$list = Okrug::find() -> orderBy('abr')->all();							
							$listS = ArrayHelper::map($list, 'id', 'abr');
							$listC = ArrayHelper::map($list, 'id', 'alias');
							$list = Address::getExistsValues('okrug_id', '%name% округ'); // только округа, по которым есть адреса
							
							?>
                            
                            <?php foreach ($listS as $id => $abr): ?>
							<?php if ($abr):?>
                                <div class="col-md-3 col-xs-4 check-box">
                                    <div class="checkbox">
									<?php
										/*
										if (array_key_exists($id, $list)) {
											echo Html::a(Html::tag('div', Html::encode($abr), ['class' => 'radio-value'.($okrug && ($listC[$id] == $okrug -> alias)?' active':'')]),['/addresses/okrug/'.$listC[$id]],['title' => $list[$id]]);
										} else {
											echo Html::tag('div', Html::encode($abr), ['class' => 'radio-value']);
										}
											*/
									?>
									
									
                                        <div class="checkbox-item">
                                            <?php echo $form->field($searchModel, 'okrug_id[]')->checkbox(['uncheck'=>0, "required"=>true, 'value' =>$id, 'class'=>'hidden-cb', 'id'=>null], false)->label(false)?>
                                        </div>
                                        <div class="checkbox-value">

                                            <?=Html::encode($abr)?>
                                        </div>

                                    </div>
                                </div>
							<?php endif ?>		
                            <?php endforeach; ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="checkbox-block">
                        <div class="public-address">
                            <?php echo Html::submitButton("найти", ['class' => 'find']); ?>
                        </div>
                        </div>
                    </div>
                    </div>
                    <?php ActiveForm::end();?>
                </div>
                <div class="row search-way tax">
                    <?php $form = ActiveForm::begin(['action' =>['address/index'], 'method' => 'get']);?>
                    <div class="flex search-form">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="checkbox-block row">
                            <div class="checkbox-container">
                                <?php 
								// перед выводом отсортируем список налоговых по номеру
								$list = Nalog::find()->all();
								$listS = ArrayHelper::map($list, 'id', 'number');							
								asort($listS);							
								$list = Address::getExistsValues('nalog_id'); // только налоговые, по которым есть адреса
								?>
                                <?php foreach ($listS as $id => $number): ?>
								<?php if ($number):?>
                                    <div class="col-md-3 col-xs-4 check-box">
                                        <div class="checkbox">
										<?php
										/*
										if (array_key_exists($id, $list)) {
											echo Html::a(Html::tag('div', Html::encode($number), ['class' => 'radio-value'.($nalog && ($number == $nalog -> number)?' active':'')]),['/addresses/nalog/'.$number],['title' => $list[$id]]);
										} else {
											echo '<a title="Нет адресов">'.Html::tag('div', Html::encode($number), ['class' => 'radio-value']).'</a>';
										}
										*/
										?>

										
                                            <div class="checkbox-item">
                                               <?php echo $form->field($searchModel, 'nalog_id[]')->checkbox(['uncheck'=>null, "required"=>false, 'value' => $id, 'class'=>'hidden-cb', 'id'=>null], false)->label(false)?>
                                            </div>
                                            <div class="checkbox-value">

                                                <?=$number?>
                                            </div>

                                        </div>
                                    </div>
								<?php endif ?>	
								<?php endforeach; ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="checkbox-block">
                            <div class="public-address">
                                <?php echo Html::submitButton("найти", ['class' => 'find']); ?>
                            </div>
                        </div>
                    </div>
                    </div>
                    <?php ActiveForm::end();?>
                </div>
                <div class="row search-way subway">
                    <?php $form = ActiveForm::begin(['action' =>['address/index'], 'method' => 'get']);?>
                    <div class="col-md-8 col-md-offset-1">
                        <div class="checkbox-block row">
						<?php 
							// перед выводом отсортируем список метро по названию
							$list = Metro::find()-> orderBy('name')->all();
							$listS = ArrayHelper::map($list, 'id', 'name');
							$listC = ArrayHelper::map($list, 'id', 'alias');
                        ?>
						
						<select class="metro" data-url="<?=Url::to(['addresses/metro'])?>" data-def-url="<?=Url::to(['/addresses'])?>">					
						<option value=''></option>
						<?php foreach ($listS as $id => $name): ?>
							<?php if ($name) {
								$options = ['value' => $listC[$id]];
								if ($metro && ($listC[$id] == $metro -> alias)) $options['selected'] = 'selected';
								echo Html::tag('option', $name, $options);
							}	
							?>	
                        <?php endforeach; ?>
						</select>
						
						
                            <?php 
							/*
							$metro_array = $metro::find()->all(); 
                            $items = ArrayHelper::map($metro_array,'id','name');

                            $params = [
                                'prompt' => 'станция метро',
                                'class' => 'metro',
                                'label' => '',
                                'id'=>null
                            ];
                            echo $form->field($searchModel, 'metro_id')->label(false)->dropDownList($items,$params);
							*/
                            ?>


                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="checkbox-block">
                            <div class="public-address">
                                <?php echo Html::submitButton("найти", ['class' => 'find']); ?>
                                <!--<div class="find">
                                    найти
                                </div>-->
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end();?>
                </div>
                <div class="row search-way other">
                    <?php $form = ActiveForm::begin(['action' =>['address/index'], 'method' => 'get']);?>
                    <div class="flex search-form">
                    <div class="col-md-8 col-md-offset-1">
                        <div class="checkbox-block row">

                            <select name="other[option][1184]" id="rent" class="metro" style="margin-top: 0">
                                <option value="0">Возможность реальной аренды</option>
                                <option value="on">С реальной арендой</option>
                                <option value="off">Без реальной аренды</option>
                            </select>
                            <select name="other[option][1173]"  class="metro">
                                <option value="0">Наличие почтового обслуживания</option>
                                <option value="on">С почтовый обслуживанием</option>
                                <option value="off">Без почтового обслуживания</option>
                            </select>
                            <select name="other[square]"  class="metro">
                                <option value="0">площадь</option>
                                <option value="0-20">0..20 кв.м</option>
                                <option value="20-50">20..50 кв.м</option>
                                <option value="50-100">50..100 кв.м</option>
                                <option value="100">больше 100 кв.м</option>
                            </select>

                        </div>
                    </div>
                        <div class="col-md-3">
                            <div class="checkbox-block">
                                <div class="public-address">
                                    <?php echo Html::submitButton("найти", ['class' => 'find']); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end();?>
                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid promo-infoblock">
        <div class="container narrow">
            <div class="row">
                <div class="col-md-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/shop.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            125 084
                        </div>
                        <div class="pi-message">
                            адресов <br> в системе
                        </div>
                    </div>
                </div>
                <div class="col-md-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/user.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            8 753
                        </div>
                        <div class="pi-message">
                            владельцев адресов <br> в системе
                        </div>
                    </div>
                </div>
                <div class="col-md-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/note.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            <?= $document_cnt; ?>
                        </div>
                        <div class="pi-message">
                            заключенных <br> договоров
                        </div>
                    </div>
                </div>
            </div>
            <div class="row second-promo-row">
                <div class="col-md-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/diamond.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            надежность
                        </div>
                        <div class="pi-message">
                            все адреса <br> проверены <br> экспертами
                        </div>
                    </div>
                </div>

                <div class="col-md-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/banknote.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            реальная цена!
                        </div>
                        <div class="pi-message">
                            многие собственники <br> предоставили эксклюзивные <br> скидки для продаж <br> на этом портале
                        </div>
                    </div>
                </div>
                <div class="col-md-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/like.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            чистота сделок
                        </div>
                        <div class="pi-message">
                            и максимальная скорость <br>
                            платежей гарантированы  <br>
                            администрацией портала <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid review-block">
        <div class="container narrow">
            <div class="row slider-block">
                <div class="reviews">
                    отзывы
                </div>
                <div class="slider">
                    <?php
                    $count = 1;
                    foreach($reviews as $review):
                        $reviewData = $review->attributes;
                    ?>
                    <div class="slide-item">
                        <div class="row flex-row">
                            <div class="col-md-6 slide-left-part">
                                <div class="review-author">

                                    <div class="author">
                                        <div class="author-name">
                                            <?=$reviewData['name'];?>
                                        </div>
                                        <div class="author-role">
                                            <?=$reviewData['author_position'];?>
                                        </div>
                                    </div>
                                    <div class="review-counter">
                                        <img class="slide-2-left" src="/img/red-triangle-left.png" alt="">
                                        <div class="slider-hr">
                                            <div class="hr-self">

                                            </div>
                                        </div>
                                        <div class="counter">
                                            <div class="counter-numbers">
                                                <span class="active-slide-page"><?=$count++;?></span> / <?=$review_total_count?>
                                            </div>
                                        </div>
                                        <div class="slider-hr">
                                            <div class="hr-self"></div>
                                        </div>
                                        <img class="slide-2-right" src="/img/red-triangle-right.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 slider-right-part">
                                <div class="review-content-flex">

                                    <div class="review-content">


                                        <?=$reviewData['content'];?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php endforeach; ?>
                    <!--<div class="slide-item">
                        <div class="row flex-row">
                            <div class="col-md-6 slide-left-part">
                                <div class="review-author">

                                    <div class="author">
                                        <div class="author-name">
                                            владимир <br>
                                            мишин
                                        </div>
                                        <div class="author-role">
                                            бизнесмен
                                        </div>
                                    </div>
                                    <div class="review-counter">
                                        <img class="slide-2-left" src="/img/red-triangle-left.png" alt="">
                                        <div class="slider-hr">
                                            <div class="hr-self">

                                            </div>
                                        </div>
                                        <div class="counter">
                                            <div class="counter-numbers">
                                                <span class="active-slide-page">2</span> / 15
                                            </div>
                                        </div>
                                        <div class="slider-hr">
                                            <div class="hr-self"></div>
                                        </div>
                                        <img class="slide-2-right" src="/img/red-triangle-right.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 slider-right-part">
                                <div class="review-content-flex">

                                    <div class="review-content">


                                        Выражаю благодарность сотрудникам vseadresa.pro за помощь в сдаче офисного помещения на улице Бурденко! Быстро нашли клиентов, очень грамотно организовали показ, оформили документы и уже через неделю заехали арендаторы. Отмечаю высокий профессионализм сотрудников и слаженную работу всего коллектива! Спасибо за содействие, рекомендую всем!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="row flex-row">
                            <div class="col-md-6 slide-left-part">
                                <div class="review-author">

                                    <div class="author">
                                        <div class="author-name">
                                            владимир <br>
                                            мишин
                                        </div>
                                        <div class="author-role">
                                            бизнесмен
                                        </div>
                                    </div>
                                    <div class="review-counter">
                                        <img class="slide-2-left" src="/img/red-triangle-left.png" alt="">
                                        <div class="slider-hr">
                                            <div class="hr-self">

                                            </div>
                                        </div>
                                        <div class="counter">
                                            <div class="counter-numbers">
                                                <span class="active-slide-page">3</span> / 15
                                            </div>
                                        </div>
                                        <div class="slider-hr">
                                            <div class="hr-self"></div>
                                        </div>
                                        <img class="slide-2-right" src="/img/red-triangle-right.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 slider-right-part">
                                <div class="review-content-flex">

                                    <div class="review-content">


                                        Выражаю благодарность сотрудникам vseadresa.pro за помощь в сдаче офисного помещения на улице Бурденко! Быстро нашли клиентов, очень грамотно организовали показ, оформили документы и уже через неделю заехали арендаторы. Отмечаю высокий профессионализм сотрудников и слаженную работу всего коллектива! Спасибо за содействие, рекомендую всем!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="row flex-row">
                            <div class="col-md-6 slide-left-part">
                                <div class="review-author">

                                    <div class="author">
                                        <div class="author-name">
                                            владимир <br>
                                            мишин
                                        </div>
                                        <div class="author-role">
                                            бизнесмен
                                        </div>
                                    </div>
                                    <div class="review-counter">
                                        <img class="slide-2-left" src="/img/red-triangle-left.png" alt="">
                                        <div class="slider-hr">
                                            <div class="hr-self">

                                            </div>
                                        </div>
                                        <div class="counter">
                                            <div class="counter-numbers">
                                                <span class="active-slide-page">4</span> / 15
                                            </div>
                                        </div>
                                        <div class="slider-hr">
                                            <div class="hr-self"></div>
                                        </div>
                                        <img class="slide-2-right" src="/img/red-triangle-right.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 slider-right-part">
                                <div class="review-content-flex">

                                    <div class="review-content">


                                        Выражаю благодарность сотрудникам vseadresa.pro за помощь в сдаче офисного помещения на улице Бурденко! Быстро нашли клиентов, очень грамотно организовали показ, оформили документы и уже через неделю заехали арендаторы. Отмечаю высокий профессионализм сотрудников и слаженную работу всего коллектива! Спасибо за содействие, рекомендую всем!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    -->

                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="public-review">
                <a href="">
                <div class="public-review-button">
                    добавить <br> отзыв
                </div>
                </a>
            </div>
        </div>
    </div>
    <div class="container-fluid faq-fluid">
        <div class="container narrow">
            <div class="flex visible-xs" style="align-items: center">
                <div class="block-title">

                    ответы на частые вопросы
                </div>
            </div>
            <div class="faq">
                <div class="flex hidden-xs" style="align-items: center">
                    <div class="block-title">
                        
                        ответы <br> на частые <br> вопросы
                    </div>
                </div>
                <div class="block-faq">
                    <div id="accordion">

                        <div class="question-container">
                            <?= Html::img('@web/img/x.png', ['alt'=>'cross', 'class'=>'close-question', 'data-id'=>"1"]);?>

                            <a data-answer-id="1" class="question">Для деятельности небольшой фирмы нужен маленький офис 15-20 кв.м.</a>
                        </div>
                        <div class="question-answers answer-id-1">
                            Мы можем предложить Вам комфортные мини кабинеты от 10 кв.м., соответствующие нормам РФ по охране труда и полностью оборудованное под                                   рабочее место.
                        </div>

                        <div class="question-container">
                            <?= Html::img('@web/img/x.png', ['alt'=>'cross', 'class'=>'close-question', 'data-id'=>"1"]);?>

                            <a data-answer-id="2" class="question">Для деятельности небольшой фирмы нужен маленький офис 15-20 кв.м.</a>
                        </div>
                        <div class="question-answers answer-id-1">
                            Мы можем предложить Вам комфортные мини кабинеты от 10 кв.м., соответствующие нормам РФ по охране труда и полностью оборудованное под                                   рабочее место.
                        </div>

                        <div class="question-container">
                            <?= Html::img('@web/img/x.png', ['alt'=>'cross', 'class'=>'close-question', 'data-id'=>"1"]);?>

                            <a data-answer-id="2" class="question">Для деятельности небольшой фирмы нужен маленький офис 15-20 кв.м.</a>
                        </div>
                        <div class="question-answers answer-id-1">
                            Мы можем предложить Вам комфортные мини кабинеты от 10 кв.м., соответствующие нормам РФ по охране труда и полностью оборудованное под                                   рабочее место.
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="public-review">
                <a href="">
                    <div class="public-answer-question">
                        еще вопросы <br> и ответы на них
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container narrow main-news-container">
            <div class="block-title flex visible-xs">
                новости
            </div>
            <div class="news-block flex">
                <div class="first-news-block flex news-mobile">
                    <div class="block-title flex hidden-xs">
                        новости
                    </div>
                    <div class="news">
                        <div class="news-date">
                            <div class="date-number">16</div>
                            <div class="date-month">октября</div>
                        </div>
                        <div class="news-title">
                            передвижной дом для экстремаль-<br>ных условий
                        </div>
                        <div class="news-preview">
                            Великобритании разработали передвижной дом для экстремальных условий Дом можно установить в любой местности …
                        </div>
                    </div>
                </div>
                <div class="news second-news-block news-mobile">
                    <div class="news-date">
                        <div class="date-number">5</div>
                        <div class="date-month">октября</div>
                    </div>
                    <div class="news-title">
                        Время собирать <br> вещи
                    </div>
                    <div class="news-preview">
                        В жилом комплексе «Яуза Парк», расположенном на востоке Москвы …
                    </div>
                </div>
                <div class="news third-news-block news-mobile">
                    <div class="news-date">
                        <div class="date-number">22</div>
                        <div class="date-month">октября</div>
                    </div>
                    <div class="news-title">
                        Аналог парка Горько-<br>го
                        построят В Бутово

                    </div>
                    <div class="news-preview">
                        В жилом комплексе «Яуза Парк», расположенном на востоке Москвы …
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="public-review">
                <a href="<?=Url::to(["/site/articles"])?>">
                    <div class="public-answer-question public-news">
                        все новости
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="container-fluid bottom-text">
        <div class="container narrow">
            <div class="row">
                <div class="col-md-3 article-mobile-block">
                    <span class="bottom-text-block">Аренда офиса в Москве</span> — не проблема с актуальными предложениями на портале office.ru. Ежедневно не менее 14000 актуальных вариантов по аренде офисов по более чем 9000 адресам Москвы. Все предложения напрямую от собственников. Все предложения без комиссии. Важную роль играет район аренды, ведь это не только финансовая операция, но и работа над имиджем компании. Здесь месторасположение офиса важно потому, что конкуренция в большом городе огромна и клиент зачастую отдаст предпочтение тем, кого просто и быстро отыскать в офисе.

                </div>
                <div class="col-md-3 article-mobile-block">
                    <span class="bottom-text-block">Офисы бывают двух типов: </span> в специализированных бизнес-центрах и на первых этажах жилых домов, вторых этажах магазинов и в прочих, зданиях. Но данная услуга представляет собой не только поиск помещения. Это процесс состоящий из нескольких этапов: запрос от арендатора, составления заявки, вдумчивый поиск по нашей специализированной базе данных, низкую комиссию, офис для Вас и тщательное оформление договора аренды офиса или нежилого помещения по всем юридическим и правовым нормам.

                </div>
                <div class="col-md-3 article-mobile-block">

                    Кроме того, <span class="bottom-text-block">обратившись в специализированную фирму,</span> предоставляющую услугу «аренда офисов в Москве», заказчик уже работает с одним человеком, которому объясняет всё, что ему требуется, а не решает одновременно множество дел – аренда офиса, подготовка к переезду, свои текущие дела и т.п. Агент по аренде офиса самостоятельно объезжает объекты и представляет на суд заказчику самые подходящие варианты. Цена за подобную услугу, помимо адреса и наличие мест для парковки.

                </div>
                <div class="col-md-3 article-mobile-block">

                    <span class="bottom-text-block">Цена за подобную услугу,</span> помимо адреса и наличие мест для парковки, формируется из таких факторов как удаление от метро/остановок общественного транспорта, площади помещения, наличия ремонта (в ряде случаев возможно снять офисное помещение без внутренней отделки или в разной степени её готовности), а также предполагаемого срока найма. Неспециалисту непросто всё это учесть и тогда поиск подходящего помещения может превратиться в затяжной кошмар для любой организации.

                </div>

            </div>
         </div>
    </div>
</section>