<?php

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\models\Address;
use frontend\models\Okrug;
use frontend\models\Nalog;
use frontend\models\Metro;
use frontend\models\Option;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

$this->title = "Прайс лист";
$this->params['breadcrumbs'] =
    [

        [
            'label' => $this->title,
        ],

    ];

?>

<div class="container">
    <h1 class="article-header">
        <?= Yii::$app->controller->h1 ?>
    </h1>
    <div class="row">
        <div class="col-lg-12 bc nop-left">
            <?= Breadcrumbs::widget([
                'homeLink' => [
                    'label' => 'Главная',
                    'url' => ['/'],
                    'class' => 'home',
                    'template' => '<span class="b-link">{link}</span><span class="separate-links">\</span>' . PHP_EOL,
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget(); ?>
        </div>
    </div>


    <div class="row">
        <?= \frontend\components\FilterWindow::getFilterButton($searchModel) ?>
    </div>


    <div class="pricelist ">


        <?= GridView::widget([
            'dataProvider' => $dataProvider,


            'summary' => false,

            'tableOptions' => [
                'class' => 'table price-table counts-table has-sort',
            ],

//        'filterModel' => $searchModel,
            'columns' => [
                /*['class' => 'yii\grid\SerialColumn'],*/


                ['attribute' => 'adr_index',
                    'label' => Address::getMeta('adr_index', 'label'),
                    'contentOptions' => ['class' => 'col-adr_index go-to-address'],

                    'headerOptions' => ['class' => 'col-adr_index'],
                    'footerOptions' => ['class' => 'col-adr_index go-to-address'],
                ],

                ['attribute' => 'address',
                    'label' => Address::getMeta('address', 'label'),

                    'contentOptions' => ['class' => 'col-address go-to-address'],
                    'headerOptions' => ['class' => 'col-address'],
                    'footerOptions' => ['class' => 'col-address go-to-address'],
                ],

                ['attribute' => 'nalog_view',
                    'label' => Address::getMeta('nalog_id', 'label'),
                    'contentOptions' => ['class' => 'col-nalog_id go-to-address'],
                    'headerOptions' => ['class' => 'col-nalog_id'],
                    'footerOptions' => ['class' => 'col-nalog_id go-to-address'],
                    'filter' => Address::getExistsValues('nalog_id'),
                    'value' => function ($data) {
                        return $data->nalog_id ? $data->nalog->getCaption() : '';
                    }

                ],

                ['attribute' => 'metro_view',
                    'label' => Address::getMeta('metro_id', 'label'),
                    'contentOptions' => ['class' => 'col-metro_id go-to-address'],
                    'headerOptions' => ['class' => 'col-metro_id'],
                    'footerOptions' => ['class' => 'col-metro_id go-to-address'],
                    'value' => function ($data) {
                        return $data->metro_id ? $data->metro->getCaption() : '';

                    }

                ],


                ['attribute' => 'okrug_view',
                    'label' => Address::getMeta('okrug_id', 'label'),
                    'contentOptions' => ['class' => 'col-okrug_id go-to-address'],
                    'headerOptions' => ['class' => 'col-okrug_id'],
                    'footerOptions' => ['class' => 'col-okrug_id go-to-address'],
//				'filter' => Address::getExistsValues('okrug_id'),	
                    'value' => function ($data) {
                        return $data['okrug_id'] ? Okrug::findOne($data['okrug_id'])->getCaption() : '';
                    }

                ],

                ['attribute' => 'options',
                    'label' => Address::getMeta('options', 'label'),
                    'contentOptions' => ['class' => 'col-options go-to-address'],

                    'headerOptions' => ['class' => 'col-options'],
                    'footerOptions' => ['class' => 'col-options go-to-address'],

                    'value' =>
                        function ($data) {
                            $model = Address::findone($data['id']);
                            if ($model) {
                                return $model->getOptions('<a title="%name%">%icon%</a>');
                            }
                        },
                    'format' => 'raw',
                ],

                ['attribute' => 'price_first_6',
                    'label' => "цена 6 мес., первичная",//Address::getMeta('price_first_6', 'label'),

                    'contentOptions' => ['class' => 'col-price6 go-to-address'],
                    'headerOptions' => ['class' => 'col-price6'],
                    'footerOptions' => ['class' => 'col-price6 go-to-address'],
                    'value' => function ($data) {
                        $price = (int)$data['price_first_6'];
                        return ($price) ? number_format($price, 0, '', ' ') . ' <i class="fa fa-rub" aria-hidden="true"></i>' : '';
                    },
                    'format' => 'raw',


                ],
                ['attribute' => 'price_change_6',
                    'label' => "цена 6 мес, перерегистрация",// Address::getMeta('price_change_6', 'label'),

                    'contentOptions' => ['class' => 'col-price6 go-to-address'],
                    'headerOptions' => ['class' => 'col-price6'],
                    'footerOptions' => ['class' => 'col-price6 go-to-address'],
                    'value' => function ($data) {
                        $price = (int)$data['price_change_6'];
                        return ($price) ? number_format($price, 0, '', ' ') . ' <i class="fa fa-rub" aria-hidden="true"></i>' : '';
                    },
                    'format' => 'raw',


                ],
                ['attribute' => 'price_first_11',
                    'label' => "цена 11 мес., первичная",// Address::getMeta('price_first_11', 'label'),

                    'contentOptions' => ['class' => 'col-price11 go-to-address'],
                    'headerOptions' => ['class' => 'col-price11'],
                    'footerOptions' => ['class' => 'col-price11 go-to-address'],
                    'value' => function ($data) {
                        $price = (int)$data['price_first_11'];
                        return ($price) ? number_format($price, 0, '', ' ') . ' <i class="fa fa-rub" aria-hidden="true"></i>' : '';
                    },
                    'format' => 'raw',


                ],
                ['attribute' => 'price_change_11',
                    'label' => "цена 11 мес., перерегистрация",// Address::getMeta('price_change_11', 'label'),

                    'contentOptions' => ['class' => 'col-price11 go-to-address'],
                    'headerOptions' => ['class' => 'col-price11'],
                    'footerOptions' => ['class' => 'col-price11 go-to-address'],
                    'value' => function ($data) {
                        $price = (int)$data['price_change_11'];
                        return ($price) ? number_format($price, 0, '', ' ') . ' <i class="fa fa-rub" aria-hidden="true"></i>' : '';
                    },
                    'format' => 'raw',


                ],


                ['class' => 'yii\grid\ActionColumn',

                    'buttons' => [
                        'order' => function ($url, $model) {

                            return Html::a(
                                'Заказать',
                                Url::to(["/address/{$model->id}/buy"]),
                                //Url::to(['/order/create', 'address_id' => $model -> id]),
                                [
                                    'class' => 'book-it',
                                    'title' => 'Оформить заказ на адрес',
                                ]
                            );

                        }
                    ],
                    'template' => '{order}',

                ],
            ],
        ]); ?>

    </div>


    <div class="hr">
        <hr>
    </div>
</div>
<!--</div>

</div>-->

<div class="container-fluid bottom-text">
    <div class="container narrow">
        <div class="row seo-background">
            <?= \frontend\components\SeoTextWidget::widget([
                'isHome' => false,
                'options' => ["class" => "col-md-3"]
            ]); ?>


        </div>
    </div>
</div>


<?= $this->render('prices_filter', [
    'searchModel' => $searchModel,

])
?>
		
