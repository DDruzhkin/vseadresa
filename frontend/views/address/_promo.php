<?php
use yii\helpers\Html;

?>
<div class="hidden-xs">
        <div class="">
        <div class="addresses-promo-block">
            <div class="promo-information">
                <div class="pi-container">
                    <div class="icon">
                        <?= Html::img('@web/img/diamond.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                    </div>
                    <div class="pi-txt">
                        надежность
                    </div>
                    <div class="pi-message">
                        все адреса  проверены экспертами
                    </div>
                </div>
            </div>
            <div class="promo-information">
                <div class="pi-container">
                    <div class="icon">
                        <?= Html::img('@web/img/banknote.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                    </div>
                    <div class="pi-txt">
                        реальная цена!
                    </div>
                    <div class="pi-message">
                        многие собственники предоставили  <br> эксклюзивные скидки для продаж
                    </div>
                </div>
            </div>
            <div class="promo-information">
                <div class="pi-container">
                    <div class="icon">
                        <?= Html::img('@web/img/like.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                    </div>
                    <div class="pi-txt">
                        чистота сделок
                    </div>
                    <div class="pi-message">
                        и максимальная скорость
                        платежей <br> гарантированы
                        администрацией портала
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>