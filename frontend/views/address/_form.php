<?php
if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Address;
use backend\models\Option;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'gps' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Address::getMeta('gps', 'label'), 'hint'=>''.Address::getMeta('gps', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('created_at_view', 'label'), 'hint'=>''.Address::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'adr_index' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('adr_index', 'label'), 'hint'=>''.Address::getMeta('adr_index', 'comment'), 'options'=>['mask' => '999999',]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'address' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Address::getMeta('address', 'label'), 'hint'=>''.Address::getMeta('address', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'status_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('status_id', 'label'), 'hint'=>''.Address::getMeta('status_id', 'comment'), 'items' => $model -> getStatusList()],	
						'owner_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('owner_id', 'label'), 'items' => Address::getListValues('owner_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('user/update/%owner_id%')]).'">'.Icon::show('link').'</a>'.Address::getMeta('owner_id', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'nalog_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('nalog_id', 'label'), 'items' => Address::getListValues('nalog_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('nalog/update/%nalog_id%')]).'">'.Icon::show('link').'</a>'.Address::getMeta('nalog_id', 'comment'), 'options'=>[]],
						'metro_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('metro_id', 'label'), 'items' => Address::getListValues('metro_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('metro/update/%metro_id%')]).'">'.Icon::show('link').'</a>'.Address::getMeta('metro_id', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'region_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('region_id', 'label'), 'items' => Address::getListValues('region_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('region/update/%region_id%')]).'">'.Icon::show('link').'</a>'.Address::getMeta('region_id', 'comment'), 'options'=>[]],
						'okrug_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('okrug_id_view', 'label'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('okrug/update/%okrug_id%')]).'">'.Icon::show('link').'</a>'.Address::getMeta('okrug_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'demo' => ['type'=>Form::INPUT_CHECKBOX, 'label'=>Address::getMeta('demo', 'label'), 'hint'=>''.Address::getMeta('demo', 'comment'), 'options'=>[]],
						'square' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('square', 'label'), 'hint'=>''.Address::getMeta('square', 'comment'), 'options'=>['clientOptions' => ['alias' =>  'decimal', 'groupSeparator' => ' ','autoGroup' => true],]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'options_as_array' => ['type'=>Form::INPUT_CHECKBOX_LIST, 'label'=>Address::getMeta('options', 'label'), 'items' => Option::listAll(Address::getMeta('options', 'itemtemplate')), 'hint'=>''.Address::getMeta('options', 'comment'), 'options'=>['multiple' => true]],
						'photo_date' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Address::getMeta('photo_date', 'label'), 'hint'=>''.Address::getMeta('photo_date', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 
							'pluginOptions' => [
								'todayHighlight' => true, 
								'autoclose'=>true,   			
								'endDate' => '0d',
								'format' => 'dd.mm.yyyy'
							
							], 
						]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'quickable' => ['type'=>Form::INPUT_CHECKBOX, 'label'=>Address::getMeta('quickable', 'label'), 'hint'=>''.Address::getMeta('quickable', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],]],
		
				],
			],
				
		],		
	],
		
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'info' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Address::getMeta('info', 'label'), 'hint'=>''.Address::getMeta('info', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'price6' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price6', 'label'), 'hint'=>''.Address::getMeta('price6', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]],
						'price11' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price11', 'label'), 'hint'=>''.Address::getMeta('price11', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'price6min' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price6min', 'label'), 'hint'=>''.Address::getMeta('price6min', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]],
						'price11min' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price11min', 'label'), 'hint'=>''.Address::getMeta('price11min', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'price6max' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price6max', 'label'), 'hint'=>''.Address::getMeta('price6max', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]],
						'price11max' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Address::getMeta('price11max', 'label'), 'hint'=>''.Address::getMeta('price11max', 'comment'), 'options'=>['mask' => Address::getMaskByAlias("money"),]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'photos' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\common\widgets\photos\PhotoList', 'label'=>Address::getMeta('photos', 'label'), 'hint'=>''.Address::getMeta('photos', 'comment'), 'options'=>[]],
						'documents' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\common\widgets\photos\PhotoList', 'label'=>Address::getMeta('documents', 'label'), 'hint'=>''.Address::getMeta('documents', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'h1' => ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('h1', 'label'), 'hint'=>''.Address::getMeta('h1', 'comment'), 'options'=>[]],
						'title' => ['type'=>Form::INPUT_TEXT, 'label'=>Address::getMeta('title', 'label'), 'hint'=>''.Address::getMeta('title', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'meta_description' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Address::getMeta('meta_description', 'label'), 'hint'=>''.Address::getMeta('meta_description', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'seo_text' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Address::getMeta('seo_text', 'label'), 'hint'=>''.Address::getMeta('seo_text', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Address */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="address-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Address::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	




$gps = $model -> gps;
$gps = explode(',', $gps);
if (count($gps) < 2) {
	$gps = ['55.751429','37.618879'];
}
echo "<div class='row'>";
echo sergmoro1\yamap\widgets\Yamap::widget([
	'points' => [
		[
			'lat' => $gps[0],
			'lng' => $gps[1],
			'icon' => '',
			'header' => '',
			'body'=> $model -> getCaption(),
			'footer' => '',
			'inputId' => 'address-gps'
		],
	],
	'params' => ['visible' => true, 'zoom' => 15]
	
]);
echo "</div>";
$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
