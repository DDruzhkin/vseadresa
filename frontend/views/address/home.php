<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\models\User;
use common\models\Metro;
use common\models\Okrug;
use common\models\Nalog;
use common\models\Option;
use frontend\models\Address;
use frontend\models\Stat;
use frontend\components\NewsWidget;
use common\widgets\Alert;
$this->title = '';
?>




<div class="mobile-accordeon-block">


</div>
<section class="visible-xs mobile-address-wrapper">
    <div class="container-fluid">
        <div class="container address-mobile-section">
			
        </div>
    </div>
</section>
<section class="search-section">

	
	
    <div class="container-fluid">
	


        <div class="container">
	

	
			<?= $this->render('_search', [
				'searchModel' => $searchModel,
			]) ?>
        </div>
    </div>

    <div class="container-fluid promo-infoblock">
        <div class="container narrow">
		
		
		
		<?php if (User::portal() -> getOptions('home.stat.show')):?>
            <div class="row">
				<?php if (User::portal() -> getOptions('home.stat.address.count.show')):?>

                <div class="col-md-4 col-sm-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/shop.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            <?php
							//	echo Stat::getValue("address.count");
								echo User::portal() -> getOptions('home.stat.address.count.value');
							?>

                        </div>
                        <div class="pi-message">
                            адресов <br> в системе
                        </div>
                    </div>
                </div>
				<?php endif?>
				<?php if (User::portal() -> getOptions('home.stat.owner.count.show')):?>
                <div class="col-md-4 col-sm-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/user.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            <?php
							//	echo Stat::getValue("owner.count");
								echo User::portal() -> getOptions('home.stat.owner.count.value');
							?>

                        </div>
                        <div class="pi-message">
                            владельцев адресов <br> в системе
                        </div>
                    </div>
                </div>
				<?php endif?>
				<?php if (User::portal() -> getOptions('home.stat.contract.count.show')):?>

                <div class="col-md-4 col-sm-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/note.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                           <?php
							//	echo Stat::getValue("contract.count");
								echo User::portal() -> getOptions('home.stat.contract.count.value');
							?>

                        </div>
                        <div class="pi-message">
                            заключенных <br> договоров
                        </div>
                    </div>
                </div>
				<?php endif?>
            </div>
			<?php endif?>

            <div class="row second-promo-row">
                <div class="col-md-4 col-sm-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/diamond.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            надежность
                        </div>
                        <div class="pi-message">
                            все адреса <br> проверены <br> экспертами
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/banknote.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            реальная цена!
                        </div>
                        <div class="pi-message">
                            многие собственники <br> предоставили эксклюзивные <br> скидки для продаж <br> на этом портале
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 promo-information">
                    <div class="pi-container">
                        <div class="icon">
                            <?= Html::img('@web/img/like.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                        </div>
                        <div class="pi-number">
                            чистота сделок
                        </div>
                        <div class="pi-message">
                            и максимальная скорость <br>
                            платежей гарантированы  <br>
                            администрацией портала <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php if (User::portal() -> getOptions('home.reviews.show')):?>
	<?=\frontend\components\ReviewsWidget::widget(['limit' => User::portal() -> getOptions('home.reviews.count')])?>
	<?php endif ?>
	
	<?php if (User::portal() -> getOptions('home.faq.show')):?>
	<?=\frontend\components\FaqWidget::widget(['limit' => User::portal() -> getOptions('home.faq.count')])?>
	<?php endif ?>
	
	
	<?php if (User::portal() -> getOptions('home.news.show')):?>
	<?=\frontend\components\NewsWidget::widget()?>
	<?php endif ?>
	

	
    <div class="container-fluid bottom-text">
        <div class="container narrow">
            <div class="row seo-background">
				<?=\frontend\components\SeoTextWidget::widget([
						'isHome' => true,
						'options' => ["class" => "col-md-3 article-mobile-block"]
				]);?>

            </div>
        </div>
    </div>

</section>
