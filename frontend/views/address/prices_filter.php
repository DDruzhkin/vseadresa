
<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;

use frontend\models\Address;
use frontend\components\FilterWindow;


$controls = [
	'nalog_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('nalog_id', 'label'), 'items' => Address::getExistsValues('nalog_id', false, true), 'hint'=>'', 'options'=>[]],
	'metro_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('metro_id', 'label'), 'items' => Address::getExistsValues('metro_id', false, true), 'hint'=>'', 'options'=>[]],						
	'okrug_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Address::getMeta('okrug_id', 'label'), 'items' => Address::getExistsValues('okrug_id', false, true), 'hint'=>'', 'options'=>[]],	

												
	'actions'=>['type'=>Form::INPUT_RAW, 'value'=>  
		Html::submitButton('Применить фильтр', ['class'=>'reg-button', 'style'=>'float: right; margin-right: 20px;']).
		Html::button('Очистить фильтр', ['class'=>'reg-button reset', 'style'=>'float: right; margin-right: 20px;'])
		
	],            
];


$layout = [
[	
	'autoGenerateColumns' => false,
	'columns' => 3,
	'attributes' => [		
		'nalog_id' => $controls['nalog_id'],				
		'metro_id' => $controls['metro_id'],		
		'okrug_id' => $controls['okrug_id'],						
	],
],

[
	'columns' => 1,
	'attributes' => [				
		'actions' => FilterWindow::getActions(),								
		
		
	]
],



];




	echo FilterWindow::widget(
			[
				'options' => [
					'id' => 'AddressPriceFilterForm', 					
					'method' => 'get',
					
				],
				'model' => $searchModel,
				'layout' => $layout,
				'type' => 'price'
					
			]);



