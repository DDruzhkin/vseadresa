<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\components\Search_promoWidget;
use frontend\components\PagerWidget;
use yii\helpers\Url;
use common\models\Metro;
use common\models\Okrug;
use common\models\Nalog;
use common\models\Option;
use common\models\ServiceName;
use common\models\Document;
use frontend\models\Address;

//var_dump($searchModel -> attributes);
//die("Debug");
/*
		//$totalCount = $dataProvider->getTotalCount();
        $metro = new Metro();// список станций метро для формы поиска
        $taxes = new Nalog(); // список налоговых для формы поиска
        $districts = new Okrug(); // список округов для формы поиска
        $services = new ServiceName(); // список услуг
        $document_cnt = Document::find()->where(['doc_type_id' => 3])->count();
		*/
/*if(isset($_GET['AddressSearch']))
		echo "<pre>",print_r($_GET['AddressSearch']), "</pre>";*/
?>

<div class="widget-filter">
        <div class="">
            <div class="widget-form-container">
            <div class="">
                <div class="">
                    <div class="filter-by">
                        по округу
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="">
                <div class="">
                <div class="district-block">
                    <?php 
					/* //Вариант с диактивными ссылками, по которым нет адресов
							// перед выводом отсортируем список округов по аббревиатуре
							$list = Okrug::find() -> orderBy('abr')->all();							
							$listS = ArrayHelper::map($list, 'id', 'abr');
							$listC = ArrayHelper::map($list, 'id', 'alias');
							$list = Address::getExistsValues('okrug_id', '%name% округ'); // только округа, по которым есть адреса
                        ?>
                        <?php foreach ($listS as $id => $abr): ?>
						<?php if ($abr):?>

                            <div class="col-md-4 col-xs-4 radio-box">
                                <div class="radio">
                                    <?php

										$active = (is_array(@$searchModel -> okrug_id) ? (in_array($id, $searchModel -> okrug_id) ? ' active':'') : "");
										
										if (array_key_exists($id, $list)) { 
                                                echo Html::a(Html::tag('div', Html::encode($abr), ['class' => 'radio-value'.($okrug && ($listC[$id] == $okrug -> alias)?' active':'').$active]),['/addresses/okrug/'.$listC[$id]],['title' => $list[$id]]);
										} else {
                                                echo Html::tag('div', Html::encode($abr), ['class' => 'radio-value'.$active]);
										}										
									?>

                                </div>
                            </div>
						<?php endif ?>	
                        <?php endforeach; ?>
					<?php 
					*/
					?>	
						
					<?php 
												
							$list = Okrug::find() -> orderBy('abr')->all();	

                        ?>
                        <?php foreach ($list as $obj): ?>
						<?php if ($obj):?>

                            <div class="col-md-4 col-xs-4 radio-box">
                                <div class="radio">
                                    <?php
										$active = (is_array(@$searchModel -> okrug_id) ? (in_array($obj -> id, $searchModel -> okrug_id) ? ' active':'') : "");
										
                                        echo Html::a(
											Html::tag(
												'div', 
												Html::encode($obj -> abr), 
												['class' => 'radio-value'.($okrug && ($obj -> alias == $okrug -> alias)?' active':'').$active]
											),
											['/addresses/okrug/'.$obj -> alias],
											['title' => $obj -> getCaption('%name% округ')]
										);
									?>

                                </div>
                            </div>
						<?php endif ?>	
                        <?php endforeach; ?>	
						
							
						
                </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="">
                <div class="">
                    <div class="filter-by">
                        по номеру налоговой
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="">
                <div class="">
                    <div class="tax-block">
                        <?php 
						/*
							// перед выводом отсортируем список налоговых по номеру
							$list = Nalog::find()->all();
							$listS = ArrayHelper::map($list, 'id', 'number');							
							asort($listS);							
							$list = Address::getExistsValues('nalog_id'); // только налоговые, по которым есть адреса
                        ?>
                        <?php foreach ($listS as $id => $number): ?>
						<?php if ($number):?>

                            <div class="radio-box">
                                <div class="radio">
                                    <?php
                                        $active = (is_array(@$searchModel -> nalog_id) ? (in_array($id, $searchModel -> nalog_id) ? ' active':'') : "");

										if (array_key_exists($id, $list)) {
											echo Html::a(Html::tag('div', Html::encode($number), ['class' => 'radio-value'.($nalog && ($number == $nalog -> number)?' active':'').$active]),['/addresses/nalog/'.$number],['title' => $list[$id]]);
										} else {
											echo '<a title="Нет адресов">'.Html::tag('div', Html::encode($number), ['class' => 'radio-value'.$active]).'</a>';
										}
										
									?>

                                </div>
                            </div>
						<?php endif ?>	
                        <?php endforeach; 
						*/
						?>
						
						<?php
							$list = Nalog::find()->all();
                        ?>
                        <?php foreach ($list as $obj): ?>
						<?php if ($obj):?>

                            <div class="radio-box">
                                <div class="radio">
                                    <?php
                                        $active = (is_array(@$searchModel -> nalog_id) ? (in_array($obj -> id, $searchModel -> nalog_id) ? ' active':'') : "");

										
										echo Html::a(
												Html::tag(
													'div', 
													Html::encode($obj -> number), 
													['class' => 'radio-value'.($nalog && ($obj -> number == $nalog -> number)?' active':'').$active]
												),
												['/addresses/nalog/'.$obj -> number],
												['title' => $obj -> getCaption('%number%')]
											);
										
										
									?>

                                </div>
                            </div>
						<?php endif ?>	
                        <?php endforeach; ?>
						
						
                    </div>
                </div>
            </div>
			
            <div class="clear"></div>
            <div class="">
                <div class="">
                    <div class="filter-by">                        
						по метро						
                    </div>
                </div>
            </div>
			
            <div class="clear"></div>
            <div class="">
                <div class="">
                <div class="fselect">
					<div class="form-group field-addresssearch-metro_id">

						<?php 
							// перед выводом отсортируем список метро по названию
							$list = Metro::find()-> orderBy('name')->all();
							$listS = ArrayHelper::map($list, 'id', 'name');
							$listC = ArrayHelper::map($list, 'id', 'alias');
                        ?>
					
					<select class="metro" data-url="<?=Url::to(['addresses/metro'])?>" data-def-url="<?=Url::to(['/addresses'])?>">					
						<option value=''></option>
						<?php foreach ($listS as $id => $name): ?>
							<?php if ($name) {
                                //$active = ($id == $searchModel -> metro_id) ? " active" : "";
								$options = ['value' => $listC[$id]];
								if ($metro && ($listC[$id] == $metro -> alias)) $options['selected'] = 'selected';
								if ($id == @$searchModel -> metro_id) $options['selected'] = 'selected';
								echo Html::tag('option', $name, $options);
							}	
							?>	
                        <?php endforeach; ?>
					</select>
					</div>
                </div>
                </div>
            </div>
                <div class="clear"></div>

            </div>
        </div>
    </div>
    