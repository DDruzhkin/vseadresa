<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\components\Search_promoWidget;
use frontend\models\Metro;
use frontend\models\Okrug;
use frontend\models\Nalog;
use frontend\models\User;
use frontend\models\ServicePrice;
use frontend\components\RelatedAddressWidget;


$this->title = 'Аренда юридического адреса';
?>
<?php 

	$list = [
				'label'     => 'адреса',
                'url'       =>  ['/addresses'],
                'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
			];
			
	$currentUser = User::getCurrentUser();
	if ($currentUser) {
		if ($currentUser -> checkRole(User::ROLE_OWNER)) {
			$list = [
					'label'     => 'адреса',
					'url'       =>  ['/owner/address'],
					'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
				];
			
		};
		
		if ($currentUser -> checkRole(User::ROLE_CUSTOMER)) {
			$list = [
					'label'     => 'адреса',
					'url'       =>  ['/addresses'],
					'template'  =>  '<span class="b-link">{link}</span><span class="separate-links">\</span>'.PHP_EOL,
				];
			
		};
	
	}
			
	$this->params['breadcrumbs'] = 
		[
			$list
			,
			[
				'label' => $address -> address,
			],
		
		];
        ?>

		<div class="container">
			<div class="row">
                <div class="col-md-3 col-sm-12 add-search-block hidden-xs">
                    <div class="add-searcher">
                        <span>поиск адреса:</span>

                    </div>


                    <?php
						echo $this->render('_link_filter', $vars);
						echo $this->render('_promo', []);
                    ?>

                </div>
                <?php //var_dump($address -> hasOption('new')); ?>
				<div class="col-lg-8 col-md-9 col-lg-offset-1 col-sm-offset-0 col-md-offset-0 col-sm-12 address-one">
                    <div class="col-md-12">
                    <div class="all-addresses-block">
					<?php if (!$address -> enabled):?>
					<div class="add-header row">
						<div class="alarm">
						<div class="caption">Внимание! Адрес удален.</div>
						<div>Для восстановления адреса обратитесь к администратору портала.</div>
						</div>
					</div>
					
					
					<?php endif ?>
					<div class="add-header row">
						<div class="col-md-7 col-lg-8">
							<div class="flex al">
								<?php if ($address -> hasOption('new')): ?>
								<div class="new-button al">
									new
								</div>
								<?php endif?>
								
								<h1 class="address-detail-head">
									<?=Yii::$app->controller -> h1?Yii::$app->controller -> h1:$address -> address?>
								</h1>
								
							</div>
						</div>
						<div class="col-md-3 col-md-offset-1 no-padding-d">
							<div class="icons-div flex al">
								
								<?=$address -> getOptions('<a title="%name%">%icon%</a>')?>
								
							</div>



						</div>
					</div>
					<div class="address-text row">
						<div class="col-md-8 col-lg-7">
							<div class="flex address-first-row">
								<div class="address-text-district">								
									<?=$address -> okrug_id?Html::a($address -> okrug_view, Url::to(['/addresses/okrug/'.$address -> okrug->alias]), ['class' => "card-filter"]):'-'?>
								</div>
								<div class="middot">
									&middot;
								</div>
								<div class="address-text-tax">								
									<?=$address -> nalog_id?Html::a($address -> nalog_view, Url::to(['/addresses/nalog/'.$address -> nalog -> alias]), ['class' => "card-filter"]):'-'?>
                                    

                                </div>


								<div class="middot">
									&middot;
								</div>
								<div class="address-text-area">
									<div class="flex">
									<?=$address -> square?$address -> square." м <sup>2</sup>":''?>
									</div>
								</div>
							</div>
							<div class="flex second-row-address-info">
								<div class="address-text-index">
									<span>индекс</span>
								</div>
								<div class="address-text-index-value">
                                    <?=$address -> adr_index?>
								</div>
								<div class="address-text-subway">
									<div class="flex">
										<span>метро</span>
									</div>
								</div>
								<div class="address-text-subway-value">								
									<?=$address -> metro_id?Html::a($address -> metro_view, Url::to(['/addresses/metro/'.$address -> metro->alias]), ['class' => "card-filter"]):'-'?>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-lg-offset-1 centralize-mobile">
							<div class="order-address address-one-top row">
		                        <a class="order-fancybox" href="<?=Url::to(["/address/{$address->id}/buy"])?>">
		                            <div class="order-address-button">
		                                заказать <br> адрес
		                            </div>
		                        </a>
		                    </div>
						</div>
					</div>
                    <div class="row">
						<?=\frontend\components\ImagesWidget::widget([
							'images' => $address -> photos_as_array
						]);?>
                    </div>
                    <div class="clear"></div>
					<div class="row">
						<div class="col-md-11">
							<div class="address-prelocation-info">
								<?=$address -> info?>
							</div>
                            <?php $latlng = explode(',', $address['gps']); ?>
							<div id="map" data-lat="<?=trim($latlng[0])?>" data-lng="<?=@trim($latlng[1])?>"></div>
							<div class="address-postlocation-info">

								<?=$address -> seo_text?>
								
							</div>

						</div>

					</div>
					<div class="row address-owner-info address-full-info">
						<div class="col-md-8 col-md-offset-1">
							<div class="add-owner labels">владелец адреса</div>
						</div>
					</div>
					<div class="row address-owner-info">
                        <div class="col-md-12 flex-all">
							<div class="icon">
                            	<img class="pi-icon" src="/img/user.png" alt="some">
                            </div>
                            
							<div class="owner-name">
                                <?=Html::a($address -> owner -> fullname, Url::to(['user/view', 'id' => $address -> owner_id]), ['title' => 'Перейти на карточку владельца'])?>
                            </div>
                        </div>

					</div>
					<?php
					if ($currentUser &&($currentUser -> id == $address -> owner_id) && $address -> subscribe_to) { // Информация о подписке показывается только владельцу адресу
					?>	
					
					
					<div class="row address-owner-title">
						<div class="col-md-8 col-md-offset-1">
							<div class="add-owner labels">Подписка</div>
						</div>
					</div>
					<div class="row ">
                        <div class="col-md-12 address-owner-info">
							<div class="icon">
								<!-- <img class="pi-icon" src="/img/banknote.png" alt="some"> -->
							</div>
							<div class="flexAll column address-extra-content">
								Подписка на публикацию адреса оформлена до <?=$address -> subscribe_to_view?>
							</div>
						</div>
					</div>
					<?php
					};
					
					?>
					
					<div class="row address-owner-title">
						<div class="col-md-8 col-md-offset-1">
							<div class="add-owner labels">цены</div>
						</div>
					</div>
					<div class="row ">
                        <div class="col-md-12 address-owner-info">
                        <div class="icon">
                            <img class="pi-icon" src="/img/banknote.png" alt="some">
                        </div>
                        <div class="flexAll column address-extra-content">
						<?php if ($address->price_first_6 > 0): ?>
                            <div class="flex-all">

                                <div class="service-price">
                                    <?=round($address->price_first_6)?> <i class="fa fa-rub" aria-hidden="true"></i>
                                </div>
                                <div class="service-name">
                                    Аренда адреса на 6 месяцев
                                </div>
                            </div>
						<?php endif ?>	
						<?php if ($address->price_first_11 > 0): ?>
                            <div class="flex-all">
                                <div class="service-price">
                                    <?=round($address->price_first_11)?> <i class="fa fa-rub" aria-hidden="true"></i>
                                </div>
                                <div class="service-name">
                                    Аренда адреса на 11 месяцев
                                </div>
                            </div>
						<?php endif ?>
                        <?php if ($address->price_change_6 > 0): ?>
                            <div class="flex-all">

                                <div class="service-price">
                                    <?=round($address->price_change_6)?> <i class="fa fa-rub" aria-hidden="true"></i>
                                </div>
                                <div class="service-name">
                                    Регистрация изменений на 6 месяцев
                                </div>
                            </div>
                        <?php endif ?>
                        <?php if ($address->price_change_11 > 0): ?>
                            <div class="flex-all">
                                <div class="service-price">
                                    <?=round($address->price_change_11)?> <i class="fa fa-rub" aria-hidden="true"></i>
                                </div>
                                <div class="service-name">
                                    Регистрация изменений на 11 месяцев
                                </div>
                            </div>
                        <?php endif ?>

                            <?php


$addressServiceList = $address -> serviceList -> all();
$addressServices = [];
$addressAddServices = [];

foreach($addressServiceList as $service) {
	
	//echo $service -> service_name_view.' - '.$service -> getPriceValue()."<br>";
	if (!$service -> getPriceValue()) continue; // Если цена не указана, то услугу не отображаем
	$serviceName = $service -> service_name;
	//$option = $serviceName? $serviceName -> option:null;
	//$icon = $option?$option -> getCaption('<a title="Связана с опцией %name%">%icon%</a>'):'';	
	if ($service -> stype == ServicePrice::STYPE_ONCE) {
		$priceValue = (int)$service -> price.'<i class="fa fa-rub" aria-hidden="true"></i>';	
	} else {
		$priceValue = '';
		$price6 = (int)$service -> price6;
		$price11 = (int)$service -> price11;
		if ($price6 == $price11) {
			$priceValue = $price6.' <i class="fa fa-rub" aria-hidden="true"></i> в месяц';
		} else {
			if (($price6) > 0) {
				$priceValue = $price6.' <i class="fa fa-rub" aria-hidden="true"></i> в месяц при аренде на 6 месяцев';
			};
			
			if ($price6 * $price11) $priceValue = $priceValue.'<br>';
			if ($price11 > 0) {
				$priceValue = $priceValue.$price11.' <i class="fa fa-rub" aria-hidden="true"></i> в месяц при аренде на 11 месяцев';
			};
											
		}
		
	};
	
	
	 $srv = [
										//'label' => '<span class="empty-icon">'.$icon.'</span>'.$service -> getCaption('%name%'),
										'label' => $service -> getCaption('%name%'),
										
										
										'price' => 	$priceValue,
										'stype' => $service -> stype,
									];
	if ($serviceName) $addressServices[$service -> id] = $srv; else $addressAddServices[$service -> id] = $srv;


}



?>

					
                        </div>
                        </div>

					</div>





<?php//			if (count($addressAddServices)):	?>
					<div class="row address-owner-title">
						<div class="col-md-8 col-md-offset-1">
							<div class="add-owner labels">дополнительные услуги</div>
						</div>
					</div>
                    <div class="row">
                        <div class="col-md-12 address-owner-info">

                        <div class="icon">
                            <img class="pi-icon" src="/img/pin.png" alt="some">
                        </div>
                        <div class="address-extra-content">
						
<?php 			foreach($addressServices as $id => $data): ?>
                            <div class="flex-all">
                                <div class="service-price">
                                    <?=$data['price']?>
								</div>
                                <div class="service-name">
                                    <?=$data['label']?>
                                </div>

                            </div>
<?php 			endforeach?>

					
						
						
<?php 			foreach($addressAddServices as $id => $data): ?>
                            <div class="flex-all">
                                <div class="service-price">
                                    <?=$data['price']?>
                                </div>
                                <div class="service-name">
                                    <?=$data['label']?>
                                </div>

                            </div>
<?php 			endforeach?>

                        </div>

                        </div>
                    </div>

<?php//			endif; ?>


					<div class="row">
						<div class="col-md-11">
							<div class="order-address pull-left">


                                    <a href="<?=Url::to(["/address/{$address->id}/buy"])?>" class="order-address-button">
                                            заказать <br> адрес
                                    </a>


		                    </div>
	                    </div>
					</div>

					
					<?= RelatedAddressWidget::widget(['address' => $address]);?>
					
					
				</div>
			</div>
		</div>


<?php
    $this->registerJsFile("/js/map.js", array("depends"=>[], "position"=>\yii\web\view::POS_END));
?>


