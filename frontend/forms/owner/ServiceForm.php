<?php

namespace frontend\forms\owner;


use yii\base\Model;

class ServiceForm extends Model
{

    public $name;
    public $stype;
    public $price;


    public function attributeLabels()
    {
        return [
            'name' => 'Название услуги',
            'stype' => 'Тип услуги',
            'price' => 'Цена услуги',
        ];
    }

    public function rules()
    {
        return [

        ];
    }

}