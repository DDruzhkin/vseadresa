<?php

namespace frontend\forms\owner;

use Yii;
use yii\base\Model;


class AddressSearchForm extends Model
{
    public $id;
    public $created_at_from;
    public $created_at_to;
    public $address;
    public $status;
    public $okrug;


    public function attributeLabels()
    {
        return [
            'id' => 'Идентификационный номер',
            'created_at_from' => 'Дата добавления от',
            'created_at_to' => 'Дата добавления до',
            'address' => 'Адрес',
            'status_id' => 'Статус адреса',
            'okrug_id' => 'Округ',
         ];
    }

    public function rules()
    {
        return [
            [['created_at_from', 'created_at_to'], 'date'],

        ];
    }

}