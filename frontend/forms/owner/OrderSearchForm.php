<?php

namespace frontend\forms\owner;


use yii\base\Model;


class OrderSearchForm extends Model
{
    public $created_at_from;
    public $created_at_to;
    public $number;
    public $customer_id;
    public $duration;
    public $address;
    public $status_id;
    public $delivery_type;
    public $payment_type;
    public $price_from;
    public $price_to;


    public function attributeLabels()
    {
        return [
            'created_at_from' => 'Дата создания от',
            'created_at_to' => 'Дата создания до',
            'number' => 'Номер',
            'customer_id' => 'Заказчик',
            'status_id' => 'Статус заказа',
            'duration' => 'Длительность',
            'address' => 'Адрес',
            'delivery_type' => 'Способ доставки',
            'payment_type' => 'Способ оплаты',
            'price_from' => 'Цена от',
            'price_to' => 'Цена до',
         ];
    }

    public function rules()
    {
        return [
            [['created_at_from', 'created_at_to'], 'date'],

        ];
    }

}