<?php

namespace frontend\components;

use Yii;
use yii\base\Widget;
use frontend\models\Page;


/**
 * Отображает список ссылок в футере.
 */
class NewsWidget extends Widget
{

	public function init()
    {
        parent::init();	
    }
	
    function run()
    {
        $news = Page::bySlug('news');
		if (is_null($news)) {
			$news = new Page();
			$news -> load(['slug' => 'news', 'h1' => 'Новости', 'label' => 'новости', 'type' => Page::TYPE_RUBRIC], '');
			$news -> save();
		}
		return $this->render('news',[
            'news' => $news->getPosts(),
            'slugNews' => $news->slug,
        ]);
    }
}



