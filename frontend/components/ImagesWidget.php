<?php

namespace frontend\components;

use yii\base\Widget;

/**
 *
 */
class ImagesWidget extends Widget
{
    
	public $images; // массив фотографий ['name' => имя файла, 'title' => подпись к картинке]
	
	public function init()
    {
        parent::init();	
    }

	
	
    function run()
    {

        return $this->render('images', [
            'images' => $this -> images            
        ]);
    }
}



