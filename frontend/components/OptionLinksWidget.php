<?php

namespace frontend\components;
use Yii;
use yii\base\Widget;
use frontend\models\Option;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


/**
 * Отображает список ссылок фильтров адресов по опциям.
 * В список попадают только те опции у коорых определен атрибут alias (он используется для гереации URL) и атрибут label (он используется для гереации текста ссылки)
 */
class OptionLinksWidget extends Widget
{
    	
	public $options;
	public function init()
    {
        parent::init();	
    }

	
	
    function run()
    {
		if (!$this -> options) $this -> options = [];
		$result =  Html::tag('li', Html::a('все адреса', Url::to(['/addresses/'])), ['class' => ($_SERVER["REQUEST_URI"] == "/index.php/addresses") ? "active-menu":""]);
			
		
		$list = Option::find() -> andWhere(['not', ['alias' => null]]) -> andWhere(['not', ['alias' => '']]) -> andWhere(['not', ['label' => null]]) -> andWhere(['not', ['label' => '']]) -> all();
		if ($list) {
						
			foreach($list as $item)  {
				$result.= Html::tag('li', Html::a($item -> label, Url::to(['addresses/option/'.$item -> alias.'/'])), ['class' => (stripos($_SERVER["REQUEST_URI"], 'addresses/option/'.$item -> alias) !== false) ? "active-menu":""]);
			};
			
		}
		return Html::tag('ul',$result,$this -> options);		
		
    }
}



