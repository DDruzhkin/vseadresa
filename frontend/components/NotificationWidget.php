<?php

namespace frontend\components;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Entity;
use frontend\models\Page;
use frontend\models\User;
use frontend\models\Notification;
use frontend\models\Status;
use frontend\models\Order;
use frontend\models\Address;

/**
 * Отображает форму настройки уведомлений изменения статусов
 */
class NotificationWidget extends Widget 
{
	public $notificationType; // Тип уведомлений
	public $caption; // Заголовок
	public $user;
	
	public function init()
    {
        parent::init();	
    }

	
	
    function run()
    {
		$classes = [
			Notification::NT_ORDER_STATUS => 'Order', 
			Notification::NT_ADDRESS_STATUS => 'Address', 
			Notification::NT_REVIEW_STATUS => 'Review',
			Notification::NT_USER_STATUS => 'User',
		];		
		
		// Исключения статусов, по которым не проводится уведомлений
		$excluded = [
			Notification::NT_ORDER_STATUS => ['new','draft'], 
			Notification::NT_ADDRESS_STATUS => ['new', 'partpayment', 'noactive', 'active', 'deleted', 'partpaid'],  // Должны остаться Ожидание подтверждения, Подготовка адреса, Готов к публикации, Опубликован, Заблокирован
			Notification::NT_REVIEW_STATUS => [],
			Notification::NT_USER_STATUS => [],
		];
		
		$user = User::getCurrentUser();
		if (!$user) return false;
		
		$role = false;
		if ($user -> checkRole(User::ROLE_OWNER)) $role = 'owner';
		if ($user -> checkRole(User::ROLE_CUSTOMER)) $role = 'customer';
		if ($user -> checkRole(User::ROLE_ADMIN)) $role = 'admin';		
		if (!$role) return false;
		
		if (Entity::getCurrentUser() -> checkRole(User::ROLE_OWNER)) { // Для владельца исключаем уведомление о смене статуса заказа на Доставлен
			$excluded[Notification::NT_ORDER_STATUS][] = 'delivery';
		}
		/*
		
		if (!array_key_exists($this -> notificationType, $classes)) {
			// Здест надо обработать изолированные события (несвязанные с измеением статусов)
			// Только для администратора
		}
		*/
		$list = Status::find() -> andWhere(['table' => ('frontend\models\\'.$classes[$this -> notificationType])::tableName()]) -> orderBy(['name' => 'asc']) -> all();
		$statusList = [];
		// исключаем некоторые статусы из списка (по которым уведомления не надо делать)
		foreach($list as $status) {
			
			if (!in_array($status -> alias, $excluded[$this -> notificationType]) ) {
				$statusList[$status -> id] = $status;
			}
		}
		
		
		// отрисовываем таблицу по статусам
		$notifications = [];
		$list = $this -> user -> getNotifications($this -> notificationType);
		//var_dump(ArrayHelper::map($list, 'status_id', 'email'); exit;
		foreach($list as $item) {
			$notifications[$item -> status_id] = $item;
		}
		
		echo Html::tag('h1', $this -> caption);
		?>
		
		<form class="notification-form" action="<?=Url::to(['/notification'])?>">
		<table class="table price-table counts-table"><thead><tr><th>Статус</th><th>Email</th></tr></thead>

		<?php
        ///echo "<pre>",print_r($notifications), "</pre>";
		foreach($statusList as $id => $status) {
			$statusId = $id;
			if (array_key_exists($statusId, $notifications)) {
				$enabled = $notifications[$statusId] -> enabled;
				$email = (is_null($notifications[$statusId] -> email) ? Yii::$app->user->identity->email : $notifications[$statusId] -> email);
				//var_dump($email);
			} else {
				$email = Yii::$app->user->identity->email;
				$enabled = false;
			}	
			
		echo  '
		<tr>
		<td>
		<label><input type="checkbox" name="statuses[]" value="'.$statusId.'"'.($enabled?' checked':'').'>'.$status -> name.'</label>
		</td>
		<td><input name="emails['.$statusId.']" class="form-control" type="text" value="'.$email.'"/></td>
		<td><input type="hidden" name="nnames['.$statusId.']" value="'.strtolower($classes[$this -> notificationType])."_".$status -> alias.'_'.$role.'"></td> 
		</tr>
		';	
		}
		?>
		<input type="hidden" name="type" value="<?=$this -> notificationType?>"/>
		</table>
		<input type="submit" class="reg-button change-data" value="Сохранить"/>
		</form>
		<?php

	
	
	
	}
}