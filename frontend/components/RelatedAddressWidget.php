<?php

namespace frontend\components;

use yii\base\Widget;
use common\models\Review;
use yii\data\ActiveDataProvider;

/**
 *
 */
class RelatedAddressWidget extends Widget
{

    public $address;
    public $limit = 3;

    function init()
    {
        parent::init();
    }


    public function run()
    {
        return $this->render('related-address',[
            'related' => $this->address->getLiked($this->limit)
        ]);
    }
}



