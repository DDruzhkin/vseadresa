<?php

namespace frontend\components;
use Yii;
use yii\base\Widget;
use frontend\models\Page;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


/**
 * Отображает список ссылок в меню.
 */
class MenuWidget extends Widget
{
    	
	public $options; //ul=none - не оборачивать в UL

	public $menu;
	
	public function init()
    {
        parent::init();	
    }

	
	
    function run()
    {
		if (!$this -> options) $this -> options = [];
		
		
		// Список рубрик статей
		
		$list = false;
		$articles = Page::bySlug('articles');
		
		if (is_null($articles))	{ // Если нет, то необходимо создать
			$articles = new Page();
			$articles -> slug = 'articles';
			$articles -> label = 'Статьи';
			$articles -> h1 = 'Статьи';
			$articles -> type = Page::TYPE_RUBRIC;
			$articles -> save();
		};
		$list = $articles -> getRubrics();		
		$rubrics = '';

		if ($list) {
			foreach($list as $item) {
				$rubrics =  $rubrics.Html::tag('li', Html::a($item -> h1, Url::to(['/page/'.$item -> slug.'/'])), ['class' => (stripos($_SERVER["REQUEST_URI"], 'page/'.$item -> slug) !== false) ? "active-menu":""]);
			}
		}
		
		
		$result =  '';
		
		// Если меню top или main, то добавляем пункты Цены и Информация в начало
		if (in_array($this -> menu, ['top', 'main'])) {
			// Цены 
			$result =  $result.Html::tag('li', Html::a('цены', Url::to(['/address/prices'])), ['class' => (stripos($_SERVER["REQUEST_URI"], "address/prices") !== false) ? "active-menu":""]);
			// Информация
			$result =  $result.Html::tag(
				'li', 
				Html::a(
					'информация', 
					'#',
					['class'=>"dropdown-toggle custom-dropdown" , 'data-toggle'=>"dropdown" ]
				).				
				Html::tag(
					'ul', 
					$rubrics,
					['class'=>"dropdown-menu address-li"]
				),				
				['class'=>"dropdown"]
			);
            $result =  $result.Html::tag('li', Html::a('контакты', Url::to(['/page/kontakti'])), ['class' => (stripos($_SERVER["REQUEST_URI"], "page/kontakti") !== false) ? "active-menu":""]);
		}
		
		// Список страниц, попадающие в выбранное меню
		$list = Page::find() -> andWhere('FIND_IN_SET(:menu,menu)>0',[':menu' => ($this->menu == 'top'?'main':$this->menu)]) -> andFilterWhere(['enabled' => 1]) -> all();
		
		if ($list) {
			foreach($list as $item)  {
				$result =  $result.Html::tag('li', Html::a($item -> label, Url::to(['/page/'.$item -> slug.'/'])), ['class' => (stripos($_SERVER["REQUEST_URI"], 'page/'.$item -> slug) !== false) ? "active-menu":""]);
			};
		}
		
		// Если меню main, то добавляем пункт Вход в конец

		if ($this -> menu == 'main') {			
			$result =  $result.Html::tag('li', Html::a('вход', Url::to(['/login'])));		
		}
		

		if (strtolower(ArrayHelper::getValue($this -> options, 'ul')) != 'none') {
			$result = Html::tag('ul',$result,$this -> options);
		}
		
		return $result;
		
    }
}



