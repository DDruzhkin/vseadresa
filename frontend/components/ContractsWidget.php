<?php 
namespace frontend\components;

use yii;
use yii\base\Widget;
use common\models\Entity;
use frontend\models\User;
use frontend\models\Document;
use frontend\models\DocumentType;

 /**
 * 
 */
 class ContractsWidget extends Widget
 {
 	
	public $user;
	
 	function init()
 	{
 		parent::init();
 	}
 	function run(){		
		
		$user = $this -> user ? $this -> user: User::getCurrentUser();
		$list = array_keys($user -> requisiteList(['enabled' => [0,1]]));		
		$doc_types = [DocumentType::byAlias('contract') -> id, DocumentType::byAlias('agreement') -> id];		
		$documents = Document::find() -> andWhere(['doc_type_id' => $doc_types, 'recipient_id' => $list]);		

		$documents =  $documents -> all();
		if (!User::getCurrentUser() -> checkRole(User::ROLE_ADMIN)) {
			if (count($documents) == 0){
				
				// ни одного договора - создаем
				$documents = [$user -> ownerContracting()];
			}
		};
 		return $this->render('contracts',
		[
			'documents' => $documents,
		]);
 	}
 }

 ?>
