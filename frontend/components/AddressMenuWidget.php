<?php

namespace frontend\components;

use yii\base\Widget;

/**
 *
 */
class AddressMenuWidget extends Widget
{

    public $addressId;

    function init()
    {
        parent::init();
    }

    function run()
    {
        if (empty($this->addressId)) return '';

        return $this->render('address-menu', [
            'addressId' => $this->addressId,
        ]);
    }
}



