<?php

namespace frontend\components;


use frontend\models\User;

class AccessRule extends \yii\filters\AccessRule
{

    protected function matchRole($user)
    {
        if (empty($this->roles)) {
            return true;
        }
        foreach ($this->roles as $role) {
            if ($role == '?') {
                if ($user->getIsGuest()) {
                    return true;
                }
            } elseif ($role == User::CUSTOMER_ROLE && User::isCustomer()) {
                return true;
            } elseif ($role == User::OWNER_ROLE && User::isOwner()) {
                return true;
            } elseif ($role == User::ADMIN_ROLE && User::isAdmin()) {
                return true;
            }
        }

        return false;
    }
}