<?php

namespace frontend\components;

use common\models\Entity;
use backend\models\Order;
use common\models\PageBehavior;
use frontend\models\StatusChange;
use Yii;
use yii\base\Widget;


/**
 * Отображает список ссылок в футере.
 */
class OrderWidget extends Widget
{
    public $id;
    public $limit;

    public function init()
    {
        parent::init();
    }

    function run()
    {
        $changedStatusList = StatusChange::find()
            ->where(['object_id' => $this->id])
            ->limit($this->limit)
            ->orderBy('id desc')
            ->all();

        return $this->render('order', [
            'statusList' => $changedStatusList,
        ]);
    }
}



