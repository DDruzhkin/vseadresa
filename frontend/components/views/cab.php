<?php

use yii\helpers\Html;
use frontend\models\User;

?>
<section class="mobile-cab-menu">
    <span class="open-profile-menu">меню профиля</span>
    <div class="profile-menu-closer"><img src="/img/close-menu.png" alt=""></div>
</section>
<div class="rel-menu">
    <div class="row widget-separator-with-background cab-menu">
        <div class="row">
            <div class="col-md-12 left-padding">
                <div class="row cab-widget">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="cab-extra-info">

                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="">
                            <div class="cab-menu-block">
                                <?php
                                $links = [
                                    '/site/cab' => 'профиль',
                                    '/site/notifications' => 'уведомления',
                                ];

                                if (User::isOwner()) {
                                    $links += [
                                        '/owner/address' => 'адреса',
                                        '/owner/services' => 'услуги',
                                        '/owner/orders' => 'заказы',
                                        '/owner/statistics' => 'статистика',
                                        '/owner/payment' => 'платежи',
                                    ];
                                }
                                elseif (User::isCustomer()) {
                                    $links += [
                                        '/site/orders' => 'заказы',
                                        '/site/count' => 'расчеты',
                                    ];
                                }

                                if (count($links)) {
                                    foreach ($links as $link => $label) {
                                        echo Html::a($label, '/index.php' . $link,
                                            ['class' => 'cab-menu-item']);
                                    }
                                }
                                ?>

                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>