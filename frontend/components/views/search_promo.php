<?php 
    use yii\helpers\Html; 
    use yii\helpers\ArrayHelper;
    use yii\widgets\ActiveForm;
?>

<?php 
//echo "<pre>", print_r($arr); die();

$districts = $arr['districts'];
$taxes = $arr['taxes'];
$metro = $arr['metro'];
$searchModel = $arr['searchModel'];
 ?>

<!-- <div class="add-searcher">
    <span>поиск адреса:</span>
	
</div> -->
<div class="row widget-separator-with-background">
    <?php $form = ActiveForm::begin(['action' =>['address/index'], 'method' => 'get']);
    //echo "<pre>", print_r($_GET); die();

    ?>
    <div class="row widget-filter">
        <div class="col-md-12">
            <div class="widget-form-container">
            <div class="">
                <div class="col-md-12">
                    <div class="filter-by">
                        по округу
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="">
                <div class="col-md-12">
                <div class="district-block">
                    <?php $district_array = $districts::find()->all();
                        $district_array = ArrayHelper::map($district_array,'id','abr');
                        //echo "<pre>", print_r($district_array); die();
                    ?>
                    <?php foreach ($district_array as $key=>$district): ?>

                        <div class="col-md-4 col-xs-4 radio-box">
                            <div class="radio">
                                <div class="radio-item">
                                    <?php
                                    if(isset($_GET['AddressSearch']['okrug_id']) && !is_array($_GET['AddressSearch']['okrug_id']) && (int)$_GET['AddressSearch']['okrug_id'] > 0){
                                                $searchModel->okrug_id = $_GET['AddressSearch']['okrug_id'];
                                        }
                                    echo $form->field($searchModel, 'okrug_id')->radio(['uncheck'=>null, "required"=>false, 'value' =>$key, 'class'=>'hidden-cb radio-btn district-radio', 'id'=>null], false)->label(false);
                                    ?>
                                </div>
                                <?php if($district != '' ): ?>
                                    <div class="radio-value">

                                        <?php echo $district; ?>
                                    </div>
                                <?php endif; ?>

                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="">
                <div class="col-md-12">
                    <div class="filter-by">
                        по номеру налоговой
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="">
                <div class="col-md-12">
                    <div class="tax-block">
                        <?php $taxes_array = $taxes::find()->all();
                            $taxes_array = ArrayHelper::map($taxes_array,'id','number');
                            //echo "<pre>", print_r($taxes_array); die();
                        ?>
                        <?php foreach ($taxes_array as $key=>$tax): ?>

                            <div class="radio-box">
                                <div class="radio">
                                    <div class="radio-item">

                                        <?php
                                        if(isset($_GET['AddressSearch']['nalog_id']) && !is_array($_GET['AddressSearch']['nalog_id']) && (int)$_GET['AddressSearch']['nalog_id'] > 0){
                                                $searchModel->nalog_id = $_GET['AddressSearch']['nalog_id'];
                                        }
                                        echo $form->field($searchModel, 'nalog_id')->radio(['uncheck'=>null, "required"=>false, 'value' =>$key, 'class'=>'hidden-cb radio-btn nalog-radio', 'id'=>null], false)->label(false);

                                        ?>
                                    </div>
                                    <?php if($tax != '' ): ?>
                                        <div class="radio-value">

                                            <?php echo $tax; ?>
                                        </div>
                                    <?php endif; ?>

                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="">
                <div class="col-md-12">
                    <div class="filter-by">
                        по метро
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="">
                <div class="col-md-12">
                <div class="fselect">

                    <?php $metro_array = $metro::find()->all(); ?>
                    <?php $items = ArrayHelper::map($metro_array,'id','name');
                       // echo "<pre>", print_r($items);
                    $params = [
                        'prompt' => 'станция метро',
                        'class' => 'metro',
                        'id'=> null,
                        'label' => '',
                        'options'=>[@(int)$_GET['AddressSearch']['metro_id']=>['Selected'=>true]]
                    ];
                    echo $form->field($searchModel, 'metro_id')->label(false)->dropDownList($items,$params);
                    ?>


                </div>
                </div>
            </div>

            </div>
        </div>
    </div>
    <?php ActiveForm::end();?>
	
	
	
    <div class="row hidden-xs">
        <div class="col-md-12">
        <div class="addresses-promo-block">
            <div class="col-md-12 promo-information">
                <div class="pi-container">
                    <div class="icon">
                        <?= Html::img('@web/img/diamond.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                    </div>
                    <div class="pi-txt">
                        надежность
                    </div>
                    <div class="pi-message">
                        все адреса  проверены экспертами
                    </div>
                </div>
            </div>
            <div class="col-md-12 promo-information">
                <div class="pi-container">
                    <div class="icon">
                        <?= Html::img('@web/img/banknote.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                    </div>
                    <div class="pi-txt">
                        реальная цена!
                    </div>
                    <div class="pi-message">
                        многие собственники предоставили  <br> эксклюзивные скидки для продаж
                    </div>
                </div>
            </div>
            <div class="col-md-12 promo-information">
                <div class="pi-container">
                    <div class="icon">
                        <?= Html::img('@web/img/like.png', ['alt'=>'some', 'class'=>'pi-icon']);?>
                    </div>
                    <div class="pi-txt">
                        чистота сделок
                    </div>
                    <div class="pi-message">
                        и максимальная скорость
                        платежей <br> гарантированы
                        администрацией портала
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>