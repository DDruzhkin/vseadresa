<?php
use yii\helpers\Url;
?>
<!-- ЧаВо -->	
	
    <div class="container-fluid faq-fluid">
        <div class="container narrow">
            <div class="flex visible-xs" style="align-items: center">
                <div class="block-title">

                    ответы на частые вопросы
                </div>
            </div>
            <div class="faq">
                <div class="flex hidden-xs" style="align-items: center">
                    <div class="block-title">
                        
                        ответы <br> на частые <br> вопросы
                    </div>
                </div>
                <div class="block-faq">
                    <div id="accordion">

					<?php foreach($to_customers as $item):?> <!-- $list -->
					
					
                        <div class="question-container">
                            <img class="close-question" src="/img/x.png" alt="cross" data-id="1">
                            <a data-answer-id="1" class="question"><?=$item -> question?></a>
                        </div>
                        <div class="question-answers answer-id-1">
                            <?=$item -> answer?>
                        </div>

                        
					<?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
		
	
    <div class="container-fluid">
        <div class="container">
            <div class="public-review">
                <a href="<?=Url::to(["/faq"])?>">
                    <div class="public-answer-question">
                        еще вопросы <br> и ответы на них
                    </div>
                </a>
            </div>
        </div>
    </div>
	<!-- /ЧаВо -->