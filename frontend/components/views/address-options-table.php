<?php

use yii\helpers\Html;
use yii\helpers\Url;


/*
 * @var array $options
 * @var int $addressId
 */

if (count($options)) {?>
    <table>
        <tr>
            <th>Активна</th>
            <th>Иконка</th>
            <th>Название</th>
        </tr>
        <?php foreach ($options as $option) { ?>
            <tr>
                <td>
                    <?= Html::checkbox("option[$addressId]", null)?>
                </td>
                <td>
                    <?= Html::img('/index.php/storage/icons/options/' . $option['icon'])?>
                </td>
                <td>
                    <?= $option['name']?>
                </td>

            </tr>
        <?php } ?>
    </table>
<?php }

?>


