<?php
use yii\helpers\ArrayHelper;
use Yii\Helpers\Html;
use yii\helpers\Url;

?>
<?php if ($related): ?>
    <div class="row address-promo">
        <?php foreach ($related as $address): ?>
            <div class="col-md-4">
                <div class="address-card">
                    <div class="address-img">
                        <a href="<?= Url::to(['address/' . $address->id]) ?>">
                            <?php
                            $images = $address->photos_as_array;
                            $pic = ($images && count($images) > 0) ? $images[0] : false;

                            echo $pic ?
                                Html::img($address::getImage(Url::to(ArrayHelper::getValue($pic, 'name'))), ['alt' => ArrayHelper::getValue($pic, 'title'), 'class' => 'img-responsive']) :
                                Html::img('@web/img/hm.png', ['alt' => 'Юридический адрес', 'class' => 'img-responsive']);
                            ?>
                        </a>
                    </div>
                    <div class="address-info-cnt">

                         <?php if ($address->hasOption('new')): ?>
                            <div class="new-address-label">
                               New
                            </div>
                       <?php endif; ?>
                        <div class="address-price">
                            <?= round($address->getPriceDisplayed()) ?> <span><i class="fa fa-rub"
                                                                                 aria-hidden="true"></i></span>
                        </div>
                        <div class="address">
                            <a  href="<?=Url::to(['address/'.$address->id])?>" class="card-filter"><?=$address->address?></a>
                            <?=@$address->metro?Html::a($address->metro->getCaption(), Url::to(['addresses/metro/'.$address->metro->alias]), ['class'=>["card-filter"]]):''?>
                            <?=@$address->nalog?Html::a($address->nalog->getCaption(), Url::to(['addresses/nalog/'.$address->nalog->number]), ['class'=>["card-filter"]]):''?>
                            <?=@$address->okrug?Html::a($address->okrug->getCaption(), Url::to(['addresses/okrug/'.$address->okrug->alias]), ['class'=>["card-filter"]]):''?>
                            <?= @$address->square ? Html::tag('div', $address->square . "<span>м<sup>2</sup></span>", ['class' => "address-area"]) : '' ?>
                        </div>
                        <div class="address-icons">
                            <?= $address->getOptions('<a title="%name%">%icon%</a>') ?>
                        </div>
                        <div class="address-order order-address">
                            <!--<a class="order-fancybox" href="#order-popup">заказать</a>-->
                            <?php if (Yii::$app->user->identity == null): ?>
                                <a class="order-fancybox"
                                   href="<?= Url::to(["site/login", 'back' => 'order/' . $address->id]) ?>">заказать</a>
                            <?php else: ?>
                                <a class="order-fancybox"
                                   href="<?= Url::to(["order/create", 'address_id' => $address->id]) ?>">заказать</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
