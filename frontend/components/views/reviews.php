<?php
use frontend\components\ReviewsSlider;
use frontend\components\ShowReviews;
use yii\bootstrap\Html;

?>
<?php //pred($reviews);  ?>

<div class="container-fluid review-block">
<?php if ($review_total_count):?>

    <div class="container narrow">
        <div class="row slider-block">
            <div class="reviews">
                отзывы
            </div>
            <div class="slider">
                <?php
                $count = 1;
                
                foreach ($reviews as $review):
                    ?>
                    <div class="slide-item">
                        <div class="row flex-row">
                            <div class="col-md-6 col-sm-6 slide-left-part">
                                <div class="review-author">

                                    <div class="author">
                                        <div class="author-name">
                                            <?= $review->name; ?>
                                        </div>
                                        <div class="author-role">
                                            <?= $review->author_position;  ?>
                                        </div>
                                        <div class="author-role" style="text-decoration: underline;">
                                        <?= $review->company;  ?>
                                        </div>
                                    </div>
                                    <div class="review-counter">
                                        <img class="slide-2-left" src="/img/red-triangle-left.png" alt="">
                                        <div class="slider-hr">
                                            <div class="hr-self">

                                            </div>
                                        </div>
                                        <div class="counter">
                                            <div class="counter-numbers">
                                                <span class="active-slide-page"><?= $count++; ?></span>
                                                / <?= $review_total_count ?>
                                            </div>
                                        </div>
                                        <div class="slider-hr">
                                            <div class="hr-self"></div>
                                        </div>
                                        <img class="slide-2-right" src="/img/red-triangle-right.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 slider-right-part">
                                <div class="review-content-flex">

                                    <div class="review-content">


                                        <?= $review->content; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>
    </div>
	
<?php endif?>	
</div>



<div class="container-fluid">
    <div class="container">
        <div class="public-review">
            <a href="<?= \yii\helpers\Url::to(['review/create']) ?>" class="filter-callout-reviews">
                <div class="public-review-button ">
                    добавить <br> отзыв
                </div>
            </a>
        </div>
    </div>
</div>


