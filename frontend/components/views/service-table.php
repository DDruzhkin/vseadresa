<?php

use yii\helpers\Html;

/* @var array $services
 * @var int $addressId
*/


if (count($services)) { ?>
    <table>
        <tr>
            <th>Название услуги</th>
            <th>Активность</th>
            <th>Стоимость</th>
        </tr>
    <?php foreach ($services as $service) { ?>
        <tr>
            <td>
                <?= $service['name']?>
            </td>
            <td>
                <?= Html::checkbox("service[$addressId]")?>
            </td>
            <td>
                <?= Html::textInput('price', $service['price'], ['readonly' => true])?>
            </td>
        </tr>
    <?php } ?>
    </table>
<?php }

