<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\icons\Icon;


if ($files) {
?>
<table class="table price-table counts-table"><thead><tr><th>Номер</th><th>Дата</th><th>Название</th><th></th></tr></thead>

<?php

	foreach($files as $file) {
		$number = ArrayHelper::getValue($file, 'number');
		$date = ArrayHelper::getValue($file, 'date');
		if ($date) {
			$date = Yii::$app -> formatter -> asDate(strtotime($date));
		}
		$fn = ArrayHelper::getValue($file, 'filename');
		echo  '
		<tr>
		<td>
		'.$number.'
		</td>
		<td>'.$date.'</td>
		<td>'.ArrayHelper::getValue($file, 'name').'</td>		
		<td style="text-align:center;">'.			
			Html::a(Icon::show('download'), Url::to(['/files/documents/uploaded/'.$fn]), ['title' => 'Скачать файл']).		
		'
		</td>		
		</tr>
		';		
	}
?>

</table>

<?php
} else {
	echo "<div>Нет документов</div>";
}