            <script type="text/javascript">
                // Список городов, которые являются областями, типа Москвы или Санкт-Петербурга
                var citiesAsRegion = [<?php
                    foreach (Yii::$app -> params['citiesAsRegion'] as $value) {
                        echo "'".$value."'";
                        if ($value !== end(Yii::$app -> params['citiesAsRegion']))
                            echo ', ';
                    }
                    ?>];

                // Заполнить Нас. пункт при загрузке страницы
                window.onload = function(){
                    handleDistrict($("#deliveryaddress-district"), true)
                };

                function handleDistrict(elm, onLoad) {
                    var selectedDistrict = $(elm).val();
                    var regionField = $("#deliveryaddress-region");
                    var destinationField = $("#deliveryaddress-destination");

                    if (citiesAsRegion.indexOf(selectedDistrict) > -1) {
                        if (!onLoad || (onLoad && destinationField.val() === '')){
                            destinationField.val(selectedDistrict);
                        }
                        regionField.val('');
                        regionField.prop('disabled', true);
                    } else {
                        if (!onLoad || (onLoad && destinationField.val() === '')){
                            destinationField.val('');
                        }
                        regionField.prop('disabled', false);
                    }
                }
            </script>
			<fieldset>                
				<div class="form-group field-user-fullname has-success">
					<label class="control-label" for="user-fullname">индекс
                        <?php if ($deliveryAddress->isAttributeRequired('index')) echo '<sup>*</sup>';?>
                    </label>
					<?= $personForm->field($deliveryAddress, 'index')->label(false)->textInput() ?>
					<div class="help-block"></div>
				</div>
			</fieldset>
			<fieldset>                
				<div class="form-group field-user-fullname has-success">
					<label class="control-label" for="user-fullname">область / край
                        <?php if ($deliveryAddress->isAttributeRequired('district')) echo '<sup>*</sup>';?>
                    </label>
					<?= $personForm->field($deliveryAddress, 'district')->label(false)->
                    dropDownList(Yii::$app -> params['regions'], ['onChange' => 'handleDistrict(this, false)']) ?>
					<div class="help-block"></div>
				</div>
			</fieldset>
			<fieldset>                	
				<div class="form-group field-user-fullname has-success">
					<label class="control-label" for="user-fullname">район
                        <?php if ($deliveryAddress->isAttributeRequired('region')) echo '<sup>*</sup>';?>
                    </label>
					<?= $personForm->field($deliveryAddress, 'region')->label(false)->textInput() ?>
					<div class="help-block"></div>
				</div>
			</fieldset>

			<fieldset>                	
				<div class="form-group field-user-fullname has-success">
					<label class="control-label" for="user-fullname">нас. пункт
                        <?php if ($deliveryAddress->isAttributeRequired('destination')) echo '<sup>*</sup>';?>
                    </label>
					<?= $personForm->field($deliveryAddress, 'destination')->label(false)->textInput() ?>
					<div class="help-block"></div>
				</div>
			</fieldset>
			
			<fieldset>                	
				<div class="form-group field-user-fullname has-success">
					<label class="control-label" for="user-fullname">адрес (улица/дом/квартира)
                        <?php if ($deliveryAddress->isAttributeRequired('address')) echo '<sup>*</sup>';?>
                    </label>
					<?= $personForm->field($deliveryAddress, 'address')->label(false)->textInput() ?>
					<div class="help-block"></div>
				</div>
			</fieldset>

			<fieldset>             	   	
				<div class="form-group field-user-fullname has-success">
					<label class="control-label" for="user-fullname">примечания
                        <?php if ($deliveryAddress->isAttributeRequired('info')) echo '<sup>*</sup>';?>
                    </label>
					<?=$personForm->field($deliveryAddress, 'info')->label(false)->textarea(['rows' => '10', "cols"=>'30'])?>
					<div class="help-block"></div>
				</div>
			</fieldset>			

			
			<input name="delivery_address" value="1" type="hidden"/>
			
			
				

				
				