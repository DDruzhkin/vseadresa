<?php /* @var \common\models\Service[] $services */

$i = 1;
\common\models\Service::getMeta()
?>

<?php if ($services): ?>
    <div class="row">
        <div class="row">
            <div class="col-md-12 table-cnt">
                <h3>Услуги заказа</h3>
                <div class="table-self">
                    <table class="table price-table">
                        <thead>
                        <th></th>
                        <th>Название услуги</th>
                        <th>Стоимость услуги в заказе</th>
                        <th>Длительность</th>
                        <th>Сумма</th>
                        </thead>
                        <tbody>
                        <?php if(!is_null($guarantee)):
                            ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $guarantee->service_name ?></td>
                                <td><?= number_format($guarantee->summa, 2) ?> <i class="fa fa-rub"
                                                                                aria-hidden="true"></i></td>
                                <td></td>
                                <td><?= number_format($guarantee->summa, 2) ?> <i class="fa fa-rub"
                                                                                aria-hidden="true"></i></td>
                            </tr>
                        <?php endif; ?>
                        <?php foreach ($services as $service):
                            ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $service->name ?></td>
                                <td><?= number_format($service->price, 2) ?> <i class="fa fa-rub"
                                                                                aria-hidden="true"></i></td>
                                <td><?= $service->duration ?></td>
                                <td><?= number_format($service->summa, 2) ?> <i class="fa fa-rub"
                                                                                aria-hidden="true"></i></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="hr">
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>