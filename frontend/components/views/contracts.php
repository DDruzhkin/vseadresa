<?php
use yii\bootstrap\Html;
use yii\helpers\Url;
use kartik\icons\Icon;

use frontend\components\ReviewsSlider;
use frontend\components\ShowReviews;


?>


<div class="container-fluid contract-block">

<table class="table price-table counts-table"><thead><tr><th>Документ</th><th>Номер</th><th>Дата</th><th>Статус</th><th></th></tr></thead>
<?php foreach($documents as $document):?>
<tr>
	<td><?=$document -> doc_type -> name?></td>
	<td><?=$document -> number?></td>
	<td><?=$document -> created_at_view?></td>
	<td><?=in_array($document -> doc_type -> alias, ['contract', 'agreement'])?$document -> getContractState():''?></td>
	<td><?=Html::a(Icon::show('download'), ['document/show-pdf', 'id' => $document->id], ['target' => '_blank', 'title' => 'Скачать PDF'])?></td></tr>
<?php endforeach?>
</table>
</div>