<?php
	use yii\helpers\Url;
	
	use kartik\builder\Form;
	use kartik\builder\FormGrid;
	use kartik\widgets\ActiveForm;


?>

<style>

div.filter-pp {
	display: none;
}

.form-group label {
	width: 100%;	
}

.form-group select{
	width: 80%;
	float: left;
	margin: 0;
}
</style>
<?php if (isset($options['id'])): ?>

<script>
$(document).ready(function() {
	
	$("button.reset").click(function() {		
		console.log(document.getElementById("<?=$options['id']?>"));
		document.getElementById("<?=$options['id']?>").reset();
	});
});
</script>
<?php endif ?>






<div class="filter-pp">
    <div class="filter-pp-area">

        <div class="cabinet-form">
            <div class="close-filter-button">
                <img src="/img/close-menu.png" alt="">
            </div>
            <div class="filter-head">
                <div class="filter-w">
                    Фильтр
                </div>
                <div class="filter-rulez">
                    <span class="r-ast">*</span> помеченные поля, <br> обязательны для заполнения
                </div>
            </div>

            <div class="price-filter-row">                
                <div class="col-md-12 col-xs-12 flex al between">
                    <!--<div class="price-filter-block filter-bar col-md-12 flex al">

                        <div class="price-filter-word flex al">
                            фильтр
                        </div>

                    </div>-->
					

					
					<!-- Содержимое окна -->
					<div class="filterForm">
					<?php
					$form = ActiveForm::begin($options);
					echo FormGrid::widget([
						'model' => $model,
						'form' => $form,
						'rows' => $layout
					]);


					ActiveForm::end();
					?>
					</div>
					<!-- /Содержимое окна -->
					
					

				</div>

                <div class="clear"></div>
            </div>

        </div>
    </div>
</div>

