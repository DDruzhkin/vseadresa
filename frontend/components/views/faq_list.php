<?php
use yii\helpers\Url;
?>              <?php //echo "<pre>", print_r($list), "</pre>";?>
            <div class="faq-wrapper">
                <h3>Заказчики: </h3>
                <div class="block-faq">
                    <div id="accordion">

					<?php foreach($to_customers as $item):?>
					
					
                        <div class="question-container">
                            <img class="close-question" src="/img/x.png" alt="cross" data-id="1">
                            <a data-answer-id="1" class="question"><?=$item -> question?></a>
                        </div>
                        <div class="question-answers answer-id-1">
                            <?=$item -> answer?>
                        </div>

                        
					<?php endforeach ?>
                    </div>
                </div>
            </div>
            <div class="faq-wrapper">
                <h3>Владельцы: </h3>
                <div class="block-faq">
                    <div id="accordion2">

                        <?php foreach($to_owners as $item):?>


                            <div class="question-container">
                                <img class="close-question" src="/img/x.png" alt="cross" data-id="1">
                                <a data-answer-id="1" class="question"><?=$item -> question?></a>
                            </div>
                            <div class="question-answers answer-id-1">
                                <?=$item -> answer?>
                            </div>


                        <?php endforeach ?>
                    </div>
                </div>
            </div>
