<?php 
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
<section class="mobile-cab-menu">
    <span class="open-profile-menu">меню профиля</span>
    <div class="profile-menu-closer"><img src="/img/close-menu.png" alt=""></div>
</section>
<div class="rel-menu">
<div class="row widget-separator-with-background cab-menu">
    <div class="row">
        <div class="col-md-12 left-padding">
            <div class="row cab-widget">
                <div class="row">
                    <div class="col-md-12">
                        <div class="cab-extra-info">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="">
                            <div class="cab-menu-block">
                                <a href="<?=Url::to("/index.php/site/cab_owner"); ?>" class="cab-menu-item">
                                    профиль
                                </a>
                                <a href="<?=Url::to("/index.php/site/address_card"); ?>" class="cab-menu-item">
                                    адреса
                                </a>
                                <a href="<?=Url::to("/index.php/site/services"); ?>" class="cab-menu-item">
                                    услуги
                                </a>
                                <a href="<?=Url::to("/index.php/site/orders"); ?>" class="cab-menu-item">
                                    заказы
                                </a>
                                <a href="<?=Url::to("/index.php/site/statistics"); ?>" class="cab-menu-item">
                                    статистика
                                </a>
                                <a href="<?=Url::to("/index.php/site/notifications"); ?>" class="cab-menu-item">
                                    настройка уведомлений
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>