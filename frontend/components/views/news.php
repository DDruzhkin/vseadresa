<?php
use Yii\helpers\Url;

use common\models\EntityHelper;

?>
<?php if ($news): ?>
    <div class="container-fluid">
        <div class="container narrow main-news-container">
            <div class="block-title flex visible-xs">
                новости
            </div>
            <div class="news-block flex">

            <?php for($i=0; $i<count($news); $i+=3):?>
                <div class="first-news-block flex news-mobile">
                    <div class="block-title flex hidden-xs">
                        новости
                    </div>
                    <div class="news">
                        <div class="news-date">
                            <div class="date-number">
                                <?php
                                if($news[$i]->created_at){
                                    echo date_format(date_create($news[$i]->created_at), 'd');
                                }else{
                                    echo " ";
                                }
                                ?>
                            </div>
                            <div class="date-month">
                                <?php
                                if($news[$i]->created_at) {
                                    echo date_format(date_create($news[$i]->created_at), 'F');
                                }else{
                                    echo " ";
                                }
                            ?>
                            </div>
                        </div>
                        <div class="news-title">
                            <a href="<?=Url::to(['page/'.$news[$i]->slug])?>"><?=$news[$i]->h1?></a>
                        </div>
                        <div class="news-preview">

                            <?= EntityHelper::cutText($news[$i]->content,200) ?>
                        </div>
                    </div>
                </div>

                <div class="news second-news-block news-mobile">
                    <div class="news-date">
                        <div class="date-number">
                            <?php
                            if($news[$i+1]->created_at){
                                echo date_format(date_create($news[$i+1]->created_at), 'd');
                            }else{
                                echo " ";
                            }
                            ?>
                        </div>
                        <div class="date-month">
                            <?php
                            if($news[$i+1]->created_at) {
                                echo date_format(date_create($news[$i+1]->created_at), 'F');
                            }else{
                                echo " ";
                            }
                            ?>
                        </div>
                    </div>
                    <div class="news-title">
						<a href="<?=Url::to(['page/'.$news[$i+1]->slug])?>"><?=$news[$i+1]->h1?></a>                        
                    </div>

                    <div class="news-preview">					
                        <?= EntityHelper::cutText($news[$i+1]->content,150) ?>
                    </div>
                </div>

                <div class="news third-news-block news-mobile">
                    <div class="news-date">
                        <div class="date-number">
                            <?php
                            if($news[$i+2]->created_at){
                                echo date_format(date_create($news[$i+2]->created_at), 'd');
                            }else{
                                echo " ";
                            }
                            ?>
                        </div>
                        <div class="date-month">
                            <?php
                            if($news[$i+2]->created_at) {
                                echo date_format(date_create($news[$i+2]->created_at), 'F');
                            }else{
                                echo " ";
                            }
                            ?>
                        </div>
                    </div>
                    <div class="news-title">
						<a href="<?=Url::to(['page/'.$news[$i+2]->slug])?>"><?=$news[$i+2]->h1?></a>                                                
                    </div>
                    <div class="news-preview">

                        <?= EntityHelper::cutText($news[$i+2]->content,150) ?>
                    </div>
                </div>
            <?php endfor?>

            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="public-review">
                <a href="<?= Url::to(["page/".$slugNews]) ?>">
                    <div class="public-answer-question public-news">
                        все новости
                    </div>
                </a>
            </div>
        </div>
    </div>

<?php endif; ?>