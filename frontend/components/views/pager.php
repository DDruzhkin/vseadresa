<?php

use yii\helpers\Html;

/* @var int $currentPage
 * @var int $totalPages
 * @var int $begin
 * @var int $end
 */

$left_arrow = Html::img('/img/triangle_left.png');
$right_arrow = Html::img('/img/triangle_right.png');
?>

<div class="col-md-12">

    <div class="pag">
        <hr class="pagination-line">

        <?php // переход на первую страницу ?>
        <div <?= $currentPage == 1 ? '' : 'class="change-page"' ?>
                data-page="1">
            <div class="pagination-arrow pagination-arrow-left">
                <?= $left_arrow ?>
                <?= $left_arrow ?>
            </div>
        </div>

        <?php // переход на предыдущую страницу ?>
        <div <?= $currentPage == 1 ? '' : 'class="change-page"' ?>
                data-page="<?= $currentPage - 1 ?>">
            <div class="pagination-arrow pagination-arrow-left pagination-space-left">
                <?= $left_arrow ?>
            </div>
        </div>

        <div class="pager-numbers">
            <?php foreach (range($begin, $end) as $page) {
                if ($page == $currentPage) { ?>
                    <div class="pag-page">
                        <span class="active-page"><?= $page ?></span>
                    </div>
                <?php } else { ?>
                    <div class="pag-number change-page" data-page="<?= $page ?>">
                        <span><?= $page ?></span>
                    </div>
                <?php }
            } ?>
        </div>

        <?php // переход на следующую страницу ?>
        <div <?= $currentPage == $totalPages ? '' : 'class="change-page"' ?>
                data-page="<?= $currentPage + 1 ?>">
            <div class="pagination-arrow pagination-arrow-right">
                <?= $right_arrow ?>
            </div>
        </div>

        <?php // переход на последнюю страницу ?>
        <div <?= $currentPage == $totalPages ? '' : 'class="change-page"' ?>
                data-page="<?= $totalPages ?>">
            <div class="pagination-arrow pagination-arrow-right">
                <?= $right_arrow ?>
                <?= $right_arrow ?>
            </div>
        </div>
    </div>
</div>
