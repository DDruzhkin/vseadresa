<?php
/**
 * @var int $addressId
 */

use yii\helpers\Html;
?>

<div>
    <ul>
        <li>
            <?= Html::a('Основные параметры адреса', '/index.php/owner/address/update?id=' . $addressId)?>
        </li>
        <li>
            <?= Html::a('Отображение адреса', '/index.php/owner/address/view?id=' . $addressId)?>
        </li>
        <li>
            <?= Html::a('Документы адреса', '/index.php/owner/address/docs?id=' . $addressId)?>
        </li>
        <li>
            <?= Html::a('Параметры для публикации адреса', '/index.php/owner/address/params?id=' . $addressId)?>
        </li>
    </ul>
</div>
