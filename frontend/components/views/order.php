<?php

use Yii\helpers\ArrayHelper;

?>
<?php if ($statusList): ?>
    <div class="row">
        <div class="row">
            <div class="col-md-12 table-cnt">
                <h3>Изменение статуса заказа</h3>
                <div class="table-self">
                    <table class="table price-table">
                        <thead>
                        <th>Код транзакции</th>
                        <th>Дата и время транзакции</th>
                        <th>Новый статус</th>
                        <th>Кто осуществил</th>
                        </thead>
                        <tbody>
                        <?php foreach ($statusList as $value):
                            ?>
                            <tr>
                                <td><?= $value->id ?></td>
                                <td><?= $value->date ?></td>
                                <td><?= $value->status_view ?></td>
                                <td><?= $value->actor -> fullname; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="hr">
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>