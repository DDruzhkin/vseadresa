<?php
use yii\helpers\Url;
use yii\helpers\Html;

$main = ($images && count($images)) ?$images[0]:false;
if ($main) {
?>

					<div class="col-md-12">
						<div class="sld" id="sld">
								<div class="image-to-change add-padding-right">
                                    <ul class="view-sl">
                                    <?php foreach($images as $item):?>
                                        <?=HTML::tag('li', Html::img(Url::to(['/files/images/address/'.$item['name']]), ["class"=>"img-responsive", "alt" => $item['title']]), ['class'=>"", "data-id"=>""]);?>

                                    <?php endforeach;?>
                                    </ul>
								</div>
								<div class="address-slider">
									<ul class="sl">											
									<?php $i=0; foreach($images as $item):?>
									<?=HTML::tag('li', Html::img(Url::to(['/files/images/address/'.$item['name']]), ["class"=>"gh", "alt" => $item['title']]), ['class'=>"lense-search", "data-id"=>$i])?>
									<?php $i++; endforeach;?>
									</ul>
								</div>
						</div>
					</div>
<?php
}
