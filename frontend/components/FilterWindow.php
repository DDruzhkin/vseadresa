<?php

namespace frontend\components;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\widgets\ActiveForm;

/**
 * Отображает список ссылок в меню.
 */
class FilterWindow extends Widget {
	public $options;
	public $model;
	public $layout;
	public $type = false;
	
	
	// Возвращает кнопку фильтра

	public static function getFilterButton($searchModel) {
        $cancelFilter = "";

	    if(!$searchModel -> getIsEmpty()){
	        $cancelFilter = '<div class="cancel-current-filter">
                <i class="fa fa-times cancel-filter"></i>
            </div>';
        }

		return '<div class="filter-wrp">
			<div class="flex put-to-right '.((!$searchModel -> getIsEmpty())?" active-filter":"").'">
                <i class="fa fa-filter filter-callout"></i>
            </div>'.$cancelFilter."</div>";
	}
	
	
	// Возвращает стандантрый набор кнопок
	public static function getActions() {
		return 
		['type'=>Form::INPUT_RAW, 'value'=>
            '<div class="standart-btn-container">'.
			Html::submitButton('Применить фильтр', ['class'=>'reg-button', 'style'=>'float: right; margin-right: 20px;']).
			Html::button('Отменить', ['class'=>'reg-button reset', 'style'=>'float: right; margin-right: 20px;']).
            "</div>"
		];
	}
	
	public function init()
    {
        parent::init();	
    }
	
    function run()
    {

	
	
	
		$class = trim((isset($this -> options['class'])?$this -> options['class']:'').' filterForm');		
		$this -> options['class'] = $class;
	 		

		//exit;

		return $this->render('filter-window', [
			'options' => $this -> options,
			'model' => $this -> model,
			'layout' => $this -> layout,
			'type' => $this -> type
					
			
		]) ;
		
				
    }
	
	

	
}