<?php

namespace frontend\components;
use Yii;
use yii\base\Widget;
use frontend\models\Faq;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


/**
 * Отображает список ссылок в меню.
 */
class FaqWidget extends Widget
{
	public $limit = false;
	public $template = false;
	
	public function init()
    {
        parent::init();	
    }

	
	
    function run()
    {
        //var_dump(\common\models\Faq::attrLists()['role']['Заказчик']);
		$customer_query = Faq::find()->where(['enabled' => 1, "role" => \common\models\Faq::attrLists()['role']['Заказчик']]);
		$owner_query = Faq::find()->where(['enabled' => 1, "role" => \common\models\Faq::attrLists()['role']['Владелец']]);
		//var_dump($query);
        //$query->andWhere("role", \common\models\Faq::attrLists()['role']['Заказчик']);

		if ($this -> limit) $customer_query -> limit($this -> limit);
		$to_customerts = $customer_query -> all();

        if ($this -> limit) $owner_query -> limit($this -> limit);
        $to_owners = $owner_query -> all();
				
		return $this->render('faq'.($this -> template?'_'.$this -> template:''), [
            'to_customers' => $to_customerts,
            'to_owners' => $to_owners,
        ]);
		
		

		
    }
}



