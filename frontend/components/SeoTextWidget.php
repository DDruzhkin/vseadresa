<?php

namespace frontend\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\models\EntityExt;

/**
 * Отображает список ссылок в футере.
 */
class SeoTextWidget extends Widget
{
	public $isHome = false;
	public $options = []; // свойства тега колонки с текстом

	public function init()
    {
        parent::init();	
    }
	
    function run()
    {        
	
		$text = false;
		if ($this -> isHome) {
			$text = (EntityExt::portal() -> getOptions('home.seo.text'));
		} else {
			$model = Yii::$app->controller -> model;
			if ($model && $model -> hasAttribute('seo_text')) {
				$text = $model -> seo_text;
			}
		}
		
		if ($text) {
			$textList = explode('<p>', $text);
		
			if ($textList) {
				foreach ($textList as $text) {
					if (trim($text) > '') {
						echo Html::tag('div', $text, $this -> options);
					}
					
				}
			}
	
		}
    }
}



