<?php

namespace frontend\components;

use Yii;
use yii\base\Widget;
use frontend\models\Page;
use frontend\models\DeliveryAddress;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


class DeliveryAddressWidget extends Widget
{
    	
	public $options; 	
	public $data;
	public $form;
	public $attribute;
	public $model;
	
	public function init()
    {
        parent::init();	
    }

	
	
    function run() {
		
		$deliveryAddress = new DeliveryAddress();

		$deliveryAddress -> load($this -> data, '');
		return $this->render('delivery-address', [
            'deliveryAddress' => $deliveryAddress,            
			'personForm' => $this -> form
        ]);
	}
	
}