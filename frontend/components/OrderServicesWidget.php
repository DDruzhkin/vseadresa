<?php

namespace frontend\components;
use common\models\Entity;
use backend\models\Order;
use common\models\PageBehavior;
use frontend\models\StatusChange;
use Yii;
use yii\base\Widget;


/**
 * Отображает список ссылок в футере.
 */
class OrderServicesWidget extends Widget
{
    public $services;
    public $guarantee;

    function run()
    {
        return $this->render('order-services',[
            'services' => $this->services,
            'guarantee'=>$this->guarantee
        ]);
    }
}



