<?php

namespace frontend\components;

use common\models\Service;
use frontend\models\ServicePortal;
use yii\base\Widget;

/**
 *
 */
class ServiceTableWidget extends Widget
{
    public $model = Service::class;
    public $attribute = [];
    public $options;

    function init()
    {
        parent::init();
    }

    function run()
    {
        $addressId = $this->options['addressId'];

        return $this->render('service-table', [
            'services' => ServicePortal::getActiveForAddressId($addressId),
            'addressId' => $addressId,
        ]);
    }
}



