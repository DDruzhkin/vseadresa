<?php

namespace frontend\components;

use yii\base\Widget;
use common\models\Review;
use yii\data\ActiveDataProvider;

/**
 *
 */
class ReviewsWidget extends Widget
{

	public $limit = false;
	
    public function run()
    {
        $query = Review::find()
            ->andWhere(['status_id' => Review::status('active')])
            ->orderBy(['created_at' => SORT_DESC]);
            
			
		if ($this -> limit) $query -> limit($this -> limit);

		$reviews = $query -> all();
		
        \Yii::$app->view->params['modal_view'] = '/review/create';

        return $this->render('reviews', [
            'reviews' => $reviews,
            'review_total_count' => count($reviews)
        ]);
    }
}



