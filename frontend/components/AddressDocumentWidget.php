<?php

namespace frontend\components;

use Yii;
use yii\base\Widget;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use frontend\models\Address;

/**
 * Отображает список документов адреса
 */
class AddressDocumentWidget extends Widget 
{

	public $addressId; 
	
	public function init()
    {
        parent::init();	
    }

	
	
    function run()
    {
		
		$model = Address::findOne([$this -> addressId]);	
		if (!$model)  throw new NotFoundHttpException('Некорректный id:  '.$id);
		
		// отрисовываем таблицу с документами
		
		$files = $model -> files_as_array;						
		
		return $this->render('address-documents', [
            'model' => $model,
			'files' => $files,
        ]);

	}
}