<?php
/**
 * Codeception PHP script runner
 * This script is needed to debug codeception tests under Intellij IDEA
 */

require_once dirname(__FILE__).'/../vendor/codeception/codeception/autoload.php';

use Symfony\Component\Console\Application;

$app = new Application('Codeception', Codeception\Codecept::VERSION);
$command = new Codeception\Command\Run('run');
$app->add($command);

$app->run();