<?php
namespace frontend\Helper;

use Codeception\Util\Autoload;

Autoload::addNamespace('common\tests\helpers', __DIR__ .'/../../../../common/tests/helpers');
Autoload::addNamespace('backend\Page', __DIR__ .'/../../../../backend/tests/_support/Page');

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Acceptance extends \Codeception\Module
{
    private static $context = [];

    public function findElements($selector) {
        return $this->getModule('WebDriver')->_findElements($selector);
    }

    public function clickFirstVisible($selector)
    {
        $elements = $this->getModule('WebDriver')->_findElements($selector);
        foreach ($elements as $elm) {
            if ($elm->isDisplayed()) {
                $elm->click();
                return;
            }
        }
    }

    public function grabPageSource()
    {
        return $this->getModule('WebDriver')->webDriver->getPageSource();
    }

    public function getOrderId($orderNm)
    {
        $orderRows = $this->getModule('WebDriver')->_findElements(['css' => '#w0 tr']);
        foreach ($orderRows as $row) {
            if (mb_strpos($row->getText(), $orderNm) !== false) {
                return $row->getAttribute('data-key');
            }
        }
    }

    public function assertColumnSortedAsc($rowSelector, $columnSelector)
    {
        $columnValues = $this->getModule('WebDriver')->_findElements(['css' => "$rowSelector $columnSelector"]);
        for($i = 1; $i < count($columnValues); $i++) {
            $prev = (int)preg_replace('/[^0-9.]+/', '', $columnValues[$i - 1]->getText());
            $next = (int)preg_replace('/[^0-9.]+/', '', $columnValues[$i]->getText());
            $this->getModule('Asserts')->assertGreaterOrEquals($prev, $next);
        }
    }

    public function assertColumnSortedDesc($rowSelector, $columnSelector)
    {
        $columnValues = $this->getModule('WebDriver')->_findElements(['css' => "$rowSelector $columnSelector"]);
        for($i = 1; $i < count($columnValues); $i++) {
            $prev = (int)preg_replace('/[^0-9.]+/', '', $columnValues[$i - 1]->getText());
            $next = (int)preg_replace('/[^0-9.]+/', '', $columnValues[$i]->getText());
            $this->getModule('Asserts')->assertLessOrEquals($prev, $next);
        }
    }

    public function assertDateColumnSortedDesc($rowSelector, $columnSelector)
    {
        $columnValues = $this->getModule('WebDriver')->_findElements(['css' => "$rowSelector $columnSelector"]);
        for($i = 1; $i < count($columnValues); $i++) {
            $prevDate = strtotime($columnValues[$i - 1]->getText());
            $nextDate = strtotime($columnValues[$i]->getText());
            $this->getModule('Asserts')->assertLessOrEquals($prevDate, $nextDate);
        }
    }

    public function assertDateColumnSortedAsc($rowSelector, $columnSelector)
    {
        $columnValues = $this->getModule('WebDriver')->_findElements(['css' => "$rowSelector $columnSelector"]);
        for($i = 1; $i < count($columnValues); $i++) {
            $prevString = strtotime($columnValues[$i - 1]->getText());
            $nextString = strtotime($columnValues[$i]->getText());
            $this->getModule('Asserts')->assertGreaterOrEquals($prevString, $nextString);
        }
    }

    public function assertStringColumnSortedDesc($rowSelector, $columnSelector)
    {
        $columnValues = $this->getModule('WebDriver')->_findElements(['css' => "$rowSelector $columnSelector"]);
        for($i = 1; $i < count($columnValues); $i++) {
            $prevString = $columnValues[$i - 1]->getText();
            $nextString = $columnValues[$i]->getText();
            $this->getModule('Asserts')->assertLessOrEquals(0, strcmp($prevString, $nextString));
        }
    }

    public function assertStringColumnSortedAsc($rowSelector, $columnSelector)
    {
        $columnValues = $this->getModule('WebDriver')->_findElements(['css' => "$rowSelector $columnSelector"]);
        for($i = 1; $i < count($columnValues); $i++) {
            $prevDate = $columnValues[$i - 1]->getText();
            $nextDate = $columnValues[$i]->getText();
            $this->getModule('Asserts')->assertGreaterOrEquals(0, strcmp($prevDate, $nextDate));
        }
    }

    public function assertColumnFiltered($rowSelector, $columnSelector, $expectedValue)
    {
        $columnValues = $this->getModule('WebDriver')->_findElements(['css' => "$rowSelector $columnSelector"]);
        foreach ($columnValues as $value) {
            $this->getModule('Asserts')->assertContains($expectedValue, $value->getText());
        }
    }

    public function isVisible($selector)
    {
        $elm = $this->getModule('WebDriver')->_findElements(['css' => $selector['css']]);
        return !empty($elm) && $elm[0]->isDisplayed();
    }

    public function findContainerContains($selector, $needle)
    {
        $elms = $this->getModule('WebDriver')->_findElements(['css' => $selector['css']]);
        foreach ($elms as $elm) {
            if (mb_strpos($elm->getText(), $needle) !== false) {
                return $elm;
            }
        }
        return null;
    }

    function generateRandomStr($length = 10) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

    function getMailCatcherUrl() {
        return $this->config['mailCatcherUrl'];
    }

    public function getCurrentUrl() {
        return $this->getModule('WebDriver')->_getCurrentUri();
    }

    public function switchToBackend() {
        $this->getModule('WebDriver')->_reconfigure(['url' => $this->config['backendUrl']]);
    }

    public function switchToFrontend() {
        $this->getModule('WebDriver')->_reconfigure(['url' => $this->config['frontendUrl']]);
    }

    public function setContextParam($name, $value) {
        self::$context[$name] = $value;
    }

    public function getContextParam($name) {
        return self::$context[$name];
    }
}
