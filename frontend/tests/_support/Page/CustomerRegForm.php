<?php
namespace frontend\Page;

use frontend\AcceptanceTester;

class CustomerRegForm
{
    public static $URL = '/customer/signup';

    public static $entityTypes = [
        'person' => '1',
        'individual' => '3',
        'corporate' => '2'
    ];

    protected $tester;

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    public static function open(AcceptanceTester $I)
    {
        $I->amOnPage(self::$URL);
        return new CustomerRegForm($I);
    }

    public function setName($name)
    {
        $this->tester->fillField(['css' => '#signupform-fullname'], $name);
        return $this;
    }

    public function setEmail($email)
    {
        $this->tester->fillField(['css' => '#signupform-username'], $email);
        return $this;
    }

    public function setPassword($password)
    {
        $this->tester->fillField(['css' => '#signupform-password'], $password);
        return $this;
    }

    public function setPasswordConfirmation($password)
    {
        $this->tester->fillField(['css' => '#signupform-password_repeat'], $password);
        return $this;
    }

    public function setPhone($phone)
    {
        $this->tester->fillField(['css' => '#signupform-phone'], $phone);
        return $this;
    }

    public function setEntityType($entityType)
    {
        $this->tester->selectOption(['css' => '#signupform-form'], ['value' => self::$entityTypes[$entityType]]);
        return $this;
    }

    public function checkAcceptTerms()
    {
        $this->tester->click(['css' => '.offerta-box']);
        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function save()
    {
        $I = $this->tester;
        $I->click(['css' => 'button.reg-button']);
        $I->waitForElementVisible(['css' => 'div.alert-success']);
        $I->see('Вы зарегистрировались');
        return $this;
    }
}
