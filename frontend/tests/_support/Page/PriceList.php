<?php
namespace frontend\Page;

use frontend\AcceptanceTester;

class PriceList
{
    public static $URL = '/address/prices';

    public static $SEARCH_TIMEOUT = 5;

    protected $tester;

    public static $columns = [
        'postcode' => 'col-adr_index',
        'postAddress' => 'col-address',
        'metro' => ['column' => 'col-metro_id', 'filter' => '#addresssearch-metro_id'],
        'price6' => 'col-price6',
        'price11' => 'col-price11'
    ];

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    /**
     * @param AcceptanceTester $I
     * @return PriceList
     * @throws \Exception
     */
    public static function open(AcceptanceTester $I)
    {
        $I->amOnPage(self::$URL);
        $I->waitForElement(['css' => '.price-table']);
        return new PriceList($I);
    }

    public function sort($column)
    {
        $I = $this->tester;
        $col = self::$columns[$column];
        $I->click(['css' => "th.$col a"]);
        $class = $I->grabAttributeFrom(['css' => "th.$col a"], 'class');
        $I->assertContains('active-sort', $class);
        return $this;
    }

    public function assertColumnSortedAsc($column)
    {
        $col = self::$columns[$column];
        $this->tester->assertColumnSortedAsc('.price-table tr', "td.$col");
        return $this;
    }

    public function assertColumnSortedDesc($column)
    {
        $col = self::$columns[$column];
        $this->tester->assertColumnSortedDesc('.price-table tr', "td.$col");
        return $this;
    }

    /**
     * @param $station
     * @return $this
     * @throws \Exception
     */
    public function filterByMetro($station)
    {
        $this->filter('metro', $station);
        return $this;
    }

    public function assertColumnFiltered($column, $value)
    {
        $col = self::$columns[$column]['column'];
        $this->tester->assertColumnFiltered('.price-table tr', "td.$col", $value);
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function assertResultsFound()
    {
        $I = $this->tester;
        $I->waitForElement(['css' => '.price-table tr'], self::$SEARCH_TIMEOUT);
        $I->seeElement(['css' => '.price-table tbody tr']);
        return $this;
    }

    /**
     * @param $itemNm
     * @return OrderForm
     * @throws \Exception
     */
    public function orderAddress($itemNm)
    {
        $I = $this->tester;
        $this->assertResultsFound();
        $col = self::$columns['postAddress'];
        $postAddress = $I->grabTextFrom(['css' => ".price-table tr:nth-child($itemNm) td.$col"]);
        $col = self::$columns['price6'];
        $price6 = $I->grabTextFrom(['css' => ".price-table tr:nth-child($itemNm) td.$col"]);
        $I->click(['css' => ".price-table tr:nth-child($itemNm) td a.book-it"]);
        $auth = new Authentication($I);
        if (!$auth->isLoggedIn()) {
            $auth->login('customer@mailmail.ru', '123123');
        }
        return new OrderForm($I, ['postAddress' => $postAddress, 'price6' => $price6]);
    }

    /**
     * @param $column
     * @param $value
     * @throws \Exception
     */
    private function filter($column, $value)
    {
        $I = $this->tester;
        $I->click(['css' => '.fa-filter']);
        $filterSelector = self::$columns[$column]['filter'];
        $I->waitForElementVisible(['css' => $filterSelector]);
        $I->selectOption(['css' => $filterSelector], ['text' => $value]);
        $I->click(['css' => "#AddressPriceFilterForm button[type='submit']"]);
        $I->waitForElementNotVisible(['css' => '.price-filter-block']);
    }
}
