<?php
namespace frontend\Page;

use frontend\AcceptanceTester;

class PersonForm
{
    protected $tester;

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    public function getFirstName()
    {
        return $this->tester->grabValueFrom(['css' => '#rperson-fname']);
    }

    public function setFirstName($firstName)
    {
        $this->tester->fillField(['css' => '#rperson-fname'], $firstName);
        return $this;
    }

    public function getMiddleName()
    {
        return $this->tester->grabValueFrom(['css' => '#rperson-mname']);
    }

    public function setMiddleName($lastName)
    {
        $this->tester->fillField(['css' => '#rperson-mname'], $lastName);
        return $this;
    }

    public function getLastName()
    {
        return $this->tester->grabValueFrom(['css' => '#rperson-lname']);
    }

    public function setLastName($lastName)
    {
        $this->tester->fillField(['css' => '#rperson-lname'], $lastName);
        return $this;
    }

    public function getPassportNm()
    {
        return $this->tester->grabValueFrom(['css' => '#rperson-pasport_number']);
    }

    public function setPassportNm($nm)
    {
        $this->tester->fillField(['css' => '#rperson-pasport_number'], $nm);
        return $this;
    }

    public function getPassportIssueDate()
    {
        return $this->tester->grabValueFrom(['css' => '#rperson-pasport_date']);
    }

    public function setPassportIssueDate($issueDate)
    {
        $this->tester->fillField(['css' => '#rperson-pasport_date'], $issueDate);
        return $this;
    }

    public function getPassportIssuer()
    {
        return $this->tester->grabValueFrom(['css' => '#rperson-pasport_organ']);
    }

    public function setPassportIssuer($issuer)
    {
        $this->tester->fillField(['css' => '#rperson-pasport_organ'], $issuer);
        return $this;
    }

    public function getAddress()
    {
        return $this->tester->grabValueFrom(['css' => '#rperson-address']);
    }

    public function setAddress($address)
    {
        $this->tester->fillField(['css' => '#rperson-address'], $address);
        return $this;
    }

    public function getBankIdentifier()
    {
        return $this->tester->grabValueFrom(['css' => '#rperson-bank_bik']);
    }

    public function setBankIdentifier($identifier)
    {
        $this->tester->fillField(['css' => '#rperson-bank_bik'], $identifier);
        return $this;
    }

    public function getBankName()
    {
        return $this->tester->grabValueFrom(['css' => '#rperson-bank_name']);
    }

    public function setBankName($bankName)
    {
        $this->tester->fillField(['css' => '#rperson-bank_name'], $bankName);
        return $this;
    }

    public function getCorrespondentAccount()
    {
        return $this->tester->grabValueFrom(['css' => '#rperson-bank_cor']);
    }

    public function setCorrespondentAccount($correspondentAccount)
    {
        $this->tester->fillField(['css' => '#rperson-bank_cor'], $correspondentAccount);
        return $this;
    }

    public function getBankAccountNm()
    {
        return $this->tester->grabValueFrom(['css' => '#rperson-account']);
    }

    public function setBankAccountNm($accountNm)
    {
        $this->tester->fillField(['css' => '#rperson-account'], $accountNm);
        return $this;
    }

    public function getAccountOwner()
    {
        return $this->tester->grabValueFrom(['css' => '#rperson-account_owner']);
    }

    public function setAccountOwner($accountOwner)
    {
        $this->tester->fillField(['css' => '#rperson-account_owner'], $accountOwner);
        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function save()
    {
        $I = $this->tester;
        $I->click(['css' => 'button.reg-button']);
        $I->waitForElementVisible(['css' => '.rekvizit-container .card']);
        return $this;
    }
}
