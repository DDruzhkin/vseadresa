<?php
namespace frontend\Page;

use frontend\AcceptanceTester;

class OwnerOrderList
{
    public static $URL = '/owner/order';

    public static $SEARCH_TIMEOUT = 3;

    protected $tester;

    public static $columns = [
        'price' => 'col-summa',
        'status' => 'col-status_id',
        'status_el' => 'col-status_id ',
        'date' => 'col-created_at',
        'date_el' => 'col-created_at ',
        'number' => 'col-number',
        'number_el' => 'order-rel',
    ];

    public static $filters = [
        'number' => '#ordersearch-number'
    ];

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public static function open(AcceptanceTester $I)
    {
        $I->waitForElement(['css' => ".reg-button"]);
        $I->amOnPage(self::$URL);
        return new OwnerOrderList($I);
    }

    /**
     * @param $column
     * @throws \Exception
     */
    public function sort($column)
    {
        $I = $this->tester;
        $col = self::$columns[$column];
        $this->tester->waitForElement(['css' => ".$col"]);
        $I->click(['css' => "th.$col a"]);
        $class = $I->grabAttributeFrom(['css' => "th.$col a"], 'class');
        $I->assertContains('active-sort', $class);
        return $this;
    }

    /**
     * @param $column
     * @throws \Exception
     */
    public function assertColumnSortedAsc($column)
    {
        $col = self::$columns[$column];
        $this->tester->waitForElement(['css' => ".$col"]);
        $this->tester->assertColumnSortedAsc('.price-table tr', "td.$col");
        return $this;
    }

    /**
     * @param $column
     * @throws \Exception
     */
    public function assertColumnSortedDesc($column)
    {
        $col = self::$columns[$column];
        $this->tester->waitForElement(['css' => ".$col"]);
        $this->tester->assertColumnSortedDesc('.price-table tr', "td.$col");
        return $this;
    }

    /**
     * @param $column
     * @throws \Exception
     */
    public function assertDateColumnSortedAsc($column)
    {
        $col = self::$columns[$column];
        $this->tester->waitForElement(['css' => ".$col"]);
        $this->tester->assertDateColumnSortedAsc('.price-table tr', "td.$col");
        return $this;
    }

    /**
     * @param $column
     * @throws \Exception
     */
    public function assertDateColumnSortedDesc($column)
    {
        $col = self::$columns[$column];
        $this->tester->waitForElement(['css' => ".$col"]);
        $this->tester->assertDateColumnSortedDesc('.price-table tr', "td.$col");
        return $this;
    }

    /**
     * @param $column
     * @throws \Exception
     */
    public function assertStringColumnSortedAsc($column)
    {
        $col = self::$columns[$column];
        $this->tester->waitForElement(['css' => ".$col"]);
        $this->tester->assertStringColumnSortedAsc('.price-table tr', "td.$col");
        return $this;
    }

    /**
     * @param $column
     * @throws \Exception
     */
    public function assertStringColumnSortedDesc($column)
    {
        $col = self::$columns[$column];
        $this->tester->waitForElement(['css' => ".$col"]);
        $this->tester->assertStringColumnSortedDesc('.price-table tr', "td.$col");
        return $this;
    }

    /**
     * @param $orderNm
     */
    public function assertOrderShows($orderNm)
    {
        $I = $this->tester;
        $I->amOnPage('/index.php/customer/order');
        $I->see($orderNm);
        return $this;
    }

    public function assertOrderNotShown($orderNm)
    {
        $I = $this->tester;
        $I->amOnPage('/index.php/customer/order');
        $I->dontSee($orderNm);
        return $this;
    }

    public function deleteOrder($orderNm)
    {
        $I = $this->tester;
        $orderId = $I->getOrderId($orderNm);
        $I->click(['css' => "tr[data-key='$orderId'] a.delete"]);
        $I->acceptPopup();
        return $this;
    }

    public function assertColumnFiltered($column, $value)
    {
        $col = self::$columns[$column]['column'];
        $this->tester->assertColumnFiltered('.price-table tr', "td.$col", $value);
        return $this;
    }

    public function grabFirstElementColumn($column)
    {
        $col = self::$columns[$column];
        return $this->tester->grabTextFrom(['css' => ".price-table tr:nth-child(1) td.$col"]);
    }

    public function columnContain($column, $value)
    {
        $I = $this->tester;
        $col = self::$columns[$column];
        $val = $I->grabTextFrom(['css' => ".price-table tr:nth-child(1) td.$col"]);
        $I->assertEquals($val, $value);
    }

    public function cancelCurrentFilter()
    {
        $I = $this->tester;
        $I->wait(1);
        $I->click(['css' => '.cancel-filter']);
        return $this;
    }

    /**
     * @param $value
     * @return $this
     * @throws \Exception
     */
    public function filterDate($value)
    {
        $I=$this->tester;
        $I->waitForElementVisible(['css' => '.fa-filter']);
        $I->click(['css' => '.fa-filter']);
        $I->waitForElementVisible(['css' => '#ordersearch-date_start']);
        $I->fillField(['css' => '#ordersearch-date_start'], $value);
        $startDate = strtotime($value);
        $I->fillField(['css' => '#ordersearch-date_fin'], date('d.m.Y', strtotime('+1 day', $startDate)));
        $I->click(['css' => "[type='submit']"]);
        $I->waitForElementNotVisible(['css' => '.price-filter-block']);
        $I->waitForElementVisible(['css' => '.customet-rel']);
        return $this;
    }

    /**
     * @param $filterName
     * @param $value
     * @throws \Exception
     */
    public function filter($filterName, $value)
    {
        $I = $this->tester;
        $I->waitForElementVisible(['css' => '.fa-filter']);
        $I->click(['css' => '.fa-filter']);
        $filterSelector = self::$filters[$filterName];
        $I->waitForElementVisible(['css' => $filterSelector]);
        $I->fillField(['css' => $filterSelector], $value);
        $I->click(['css' => "[type='submit']"]);
        $I->waitForElementNotVisible(['css' => '.price-filter-block']);
        return $this;
    }

}
