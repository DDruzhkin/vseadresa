<?php

namespace frontend\Page;

use frontend\AcceptanceTester;

class ServiceManagement
{
    public static $URL = '/owner/service-price';

    protected $tester;

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    /**
     * @throws \Exception
     */
    public static function open(AcceptanceTester $I)
    {
        $I->amOnPage(self::$URL);
        return new ServiceManagement($I);
    }

    public function addService() {
        $I = $this->tester;
        $I->click(['css' => "p.mobile-cabinet-button a[href='/owner/service-price/create']"]);
        return $this;
    }

    public function getServiceId($serviceName) {
        $I = $this->tester;
        $tr = $I->findContainerContains(['css' => 'table.price-table tr'], $serviceName);
        return $tr->getAttribute('data-key');
    }

    public function updateService($id) {
        $I = $this->tester;
        $I->amOnPage("/owner/service-price/update/$id");
        return $this;
    }

    public function setBuiltInService($service) {
        $I = $this->tester;
        $I->selectOption(['css' => '#serviceprice-service_name_id'], ['text' => $service]);
        return $this;
    }

    public function assertBuiltInServiceEquals($service) {
        $I = $this->tester;
        $value = $I->grabValueFrom(['css' => '#serviceprice-service_name_view']);
        $I->assertEquals($service, mb_substr($value, 5));
        return $this;
    }

    public function setServiceName($name) {
        $I = $this->tester;
        $I->fillField(['css' => '#serviceprice-name'], $name);
        return $this;
    }

    public function assertServiceNameEquals($name) {
        $I = $this->tester;
        $value = $I->grabValueFrom(['css' => '#serviceprice-name']);
        $I->assertEquals($name, $value);
        return $this;
    }

    public function setAddToAddress() {
        $I = $this->tester;
        $I->click(['css' => '#serviceprice-to_address + .form-checkbox']);
        return $this;
    }

    public function assertAddToAddressChecked() {
        $I = $this->tester;
        $isChecked = $I->grabValueFrom(['css' => '#serviceprice-to_address']) == '1';
        $I->assertTrue($isChecked);
        return $this;
    }

    public function setDescription($description) {
        $I = $this->tester;
        $I->fillField(['css' => '#serviceprice-description'], $description);
        return $this;
    }

    public function assertDescriptionEquals($description) {
        $I = $this->tester;
        $value = $I->grabValueFrom(['css' => '#serviceprice-description']);
        $I->assertEquals($description, $value);
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function saveBasicDetails() {
        $I = $this->tester;
        $I->click(['css' => 'button.next']);
        $I->waitForElement(['css' => '#serviceprice-stype']);
        return $this;
    }

    public function setServiceType($type) {
        $I = $this->tester;
        $I->selectOption(['css' => '#serviceprice-stype'], ['text' => $type]);
        return $this;
    }

    public function assertServiceTypeEquals($type) {
        $I = $this->tester;
        $value = $I->grabTextFrom(['css' => '#serviceprice-stype option:checked']);
        $I->assertEquals($type, $value);
        return $this;
    }

    public function setPrice($price) {
        $I = $this->tester;
        $I->fillField(['css' => '#serviceprice-price'], $price);
        return $this;
    }

    public function assertPriceEquals($price) {
        $I = $this->tester;
        $value = $I->grabValueFrom(['css' => '#serviceprice-price']);
        $I->assertEquals((int)$price, (int)$value);
        return $this;
    }

    public function setPriceSixMonth($price) {
        $I = $this->tester;
        $I->fillField(['css' => '#serviceprice-price6'], $price);
        return $this;
    }

    public function assertPriceSixMonthEquals($price) {
        $I = $this->tester;
        $value = $I->grabValueFrom(['css' => '#serviceprice-price6']);
        $I->assertEquals((int)$price, (int)$value);
        return $this;
    }

    public function setPriceElevenMonth($price) {
        $I = $this->tester;
        $I->fillField(['css' => '#serviceprice-price11'], $price);
        return $this;
    }

    public function assertPriceElevenMonthEquals($price) {
        $I = $this->tester;
        $value = $I->grabValueFrom(['css' => '#serviceprice-price11']);
        $I->assertEquals((int)$price, (int)$value);
        return $this;
    }

    public function setRecipient($recipient) {
        $I = $this->tester;
        $I->selectOption(['css' => '#serviceprice-recipient_id'], ['text' => $recipient]);
        return $this;
    }

    public function assertRecipientEquals($recipient) {
        $I = $this->tester;
        $value = $I->grabTextFrom(['css' => '#serviceprice-recipient_id option:checked']);
        $I->assertEquals($recipient, $value);
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function savePriceDetails() {
        $I = $this->tester;
        $I->click(['css' => 'button.next']);
        $I->waitForElement(['css' => 'table.price-table']);
        return $this;
    }
}
