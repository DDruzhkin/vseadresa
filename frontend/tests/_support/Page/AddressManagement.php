<?php

namespace frontend\Page;

use frontend\AcceptanceTester;

class AddressManagement
{
    public static $URL = '/owner/address';

    protected $tester;

    public static $fields = [
        'address' => '#addresssearch-address',
        'status' => '#addresssearch-status_id',
        'district' => '#addresssearch-okrug_id',
        'metro' => '#addresssearch-metro_id',
        'tax' => '#addresssearch-nalog_id'
    ];

    public static $values = [
        'metro' => '.address-text-subway-value',
        'tax' => '.address-text-tax',
        'address' => '.address-detail-head',
        'district' => '.address-text-district'
    ];
    public static $options = [
        'Наличие почтового обслуживания.' =>  1173,
        'Предоставление рабочего места' =>  1174,
        'Возможность оплаты по безналу' =>  1175,
        'Фактический адрес' =>  1176,
        'Бесплатный осмотр помещения перед покупкой' =>  1177,
        'Подтверждение от собственника' =>  1178,
        'Бесплатный прием банковских проверок' =>  1179,
        'Гарантия от собственника' =>  1180,
        'Юридический адрес имеет внешний вид офиса' =>  1181,
        'Адрес не является массовым' =>  1182,
        'Есть возможность секретарского обслуживания' =>  1183,
        'Договор аренды' =>  1184,
        'Уборка помещений' =>  1451,
        'Охрана' =>  1452,
        'Пролонгация  договора' =>  1453,
        'Содействие  собственника  в решении  вопрос ИФНС' =>  1454,
        'Новый' =>  2122,
        'Большая площадь' =>  2123
    ];

    public static $services = [
        'Доставка пиццы' => 3717,
        'Срочное формирование' => 3329,
        'Уборка помещений' => 3779,
        'Охрана' => 11212,
        'Почтовое обслуживание' => 3780
    ];

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    /**
     * @throws \Exception
     */
    public static function open(AcceptanceTester $I)
    {
        $I->amOnPage(self::$URL);
        return new AddressManagement($I);
    }

    public function addAddress() {
        $I = $this->tester;
        $I->click(['css' => 'a.public-address-button']);
        return $this;
    }

    public function updateBasicProperties($id) {
        return $this->clickDropDown($id, 'basic');
    }

    public function setPostCode($postcode) {
        $I = $this->tester;
        $I->fillField(['css' => '#address-adr_index'], $postcode);
        return $this;
    }

    public function assertPostCodeEquals($postcode) {
        $I = $this->tester;
        $value = $I->grabValueFrom(['css' => '#address-adr_index']);
        $I->assertEquals($postcode, $value);
        return $this;
    }

    public function setAddress($address) {
        $I = $this->tester;
        $I->fillField(['css' => '#address-address'], $address);
        return $this;
    }

    public function assertAddressEquals($address) {
        $I = $this->tester;
        $value = $I->grabValueFrom(['css' => '#address-address']);
        $I->assertEquals($address, $value);
        return $this;
    }

    public function setTaxDept($taxDepartment) {
        $I = $this->tester;
        $I->selectOption(['css' => '#address-nalog_id'], ['text' => $taxDepartment]);
        return $this;
    }

    public function assertTaxDeptEquals($taxDepartment) {
        $I = $this->tester;
        $value = $I->grabTextFrom(['css' => '#address-nalog_id option:checked']);
        $I->assertEquals($taxDepartment, $value);
        return $this;
    }

    public function setMetro($metro) {
        $I = $this->tester;
        $I->selectOption(['css' => '#address-metro_id'], ['text' => $metro]);
        return $this;
    }

    public function assertMetroEquals($metro) {
        $I = $this->tester;
        $value = $I->grabTextFrom(['css' => '#address-metro_id option:checked']);
        $I->assertEquals($metro, $value);
        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function saveBasicProperties() {
        $I = $this->tester;
        $I->click(['css' => 'button.next']);
        $I->waitForElement(['css' => '#monthSelected']);
        return $this;
    }

    public function addPublishProperties($id) {
        return $this->clickDropDown($id, 'publication');
    }

    public function updatePublishProperties($id) {
        return $this->clickDropDown($id, 'publication');
    }

    public function setSixMonthPrice($price) {
        $I = $this->tester;
        $I->fillField(['css' => '#address-price6'], $price);
        return $this;
    }

    public function assertSixMonthPriceEquals($price) {
        $I = $this->tester;
        $value = $I->grabValueFrom(['css' => '#address-price6']);
        $I->assertEquals((int)$price, (int)$value);
        return $this;
    }

    public function setElevenMonthPrice($price) {
        $I = $this->tester;
        $I->fillField(['css' => '#address-price11'], $price);
        return $this;
    }

    public function assertElevenMonthPriceEquals($price) {
        $I = $this->tester;
        $value = $I->grabValueFrom(['css' => '#address-price11']);
        $I->assertEquals((int)$price, (int)$value);
        return $this;
    }

    public function setFloorspace($floorspace) {
        $I = $this->tester;
        $I->fillField(['css' => '#address-square'], $floorspace);
        return $this;
    }

    public function assertFloorspaceEquals($floorspace) {
        $I = $this->tester;
        $value = $I->grabValueFrom(['css' => '#address-square']);
        $I->assertEquals((int)$floorspace, (int)$value);
        return $this;
    }

    public function setOption($option) {
        $I = $this->tester;
        $optionId = self::$options[$option];
        $I->click(['css' => "input[value='$optionId'] + .form-checkbox"]);
        return $this;
    }

    public function assertOptionChecked($option) {
        $I = $this->tester;
        $optionId = self::$options[$option];
        $isChecked = $I->grabAttributeFrom("input[value='$optionId']", 'checked') == 'true';
        $I->assertTrue($isChecked);
        return $this;
    }

    public function savePublishProperties() {
        $I = $this->tester;
        $I->click(['css' => 'button.next']);
        $I->wait(3);
        return $this;
    }

    public function addServices($id) {
        return $this->clickDropDown($id, 'services');
    }

    public function updateServices($id) {
        return $this->clickDropDown($id, 'services');
    }

    /**
     * @param $service - название сервиса из AddressManagement::$services
     * @param $price6 - цена услуги за полгода, если надо оставить поле нетронутым, выставить в null
     * @param $price11 - цена услуги за 11 мес, если надо оставить поле нетронутым, выставить в null
     * @return $this
     */
    public function addService($service, $price6, $price11) {
        return $this->updateService($service, $price6, $price11);
    }

    /**
     * @param $service - название сервиса из AddressManagement::$services
     * @param $price6 - цена услуги за полгода, если надо оставить поле нетронутым, выставить в null
     * @param $price11 - цена услуги за 11 мес, если надо оставить поле нетронутым, выставить в null
     * @return $this
     */
    public function updateService($service, $price6, $price11) {
        $I = $this->tester;
        $serviceId = $this->getRowId($service);
        $isChecked = $I->grabAttributeFrom("input[value='$serviceId']", 'checked') == 'true';
        if (!$isChecked) {
            $I->click(['css' => "input[value='$serviceId'] + .form-checkbox"]);
        }
        if ($price6 != null) {
            $I->fillField(['css' => "input[name='price6[$serviceId]']"], $price6);
        }
        if ($price11 != null) {
            $I->fillField(['css' => "input[name='price11[$serviceId]']"], $price11);
        }
        return $this;
    }

    public function verifyService($service, $price6, $price11) {
        $I = $this->tester;
        $serviceId = $this->getRowId($service);
        $isChecked = $I->grabAttributeFrom("input[value='$serviceId']", 'checked') == 'true';
        $I->assertTrue($isChecked);
        $I->assertEquals((int)$price6, (int)$I->grabValueFrom(['css' => "input[name='price6[$serviceId]']"]));
        $I->assertEquals((int)$price11, (int)$I->grabValueFrom(['css' => "input[name='price11[$serviceId]']"]));
        return $this;
    }

    public function verifyOneTimeService($service, $price) {
        $I = $this->tester;
        $serviceId = $this->getRowId($service);
        $isChecked = $I->grabAttributeFrom("input[value='$serviceId']", 'checked') == 'true';
        $I->assertTrue($isChecked);
        $I->assertEquals((int)$price, (int)$I->grabValueFrom(['css' => "input[name='price[$serviceId]']"]));
        return $this;
    }

    public function removeService($service) {
        $I = $this->tester;
        $serviceId = self::$services[$service];
        $isChecked = $I->grabAttributeFrom("input[value='$serviceId']", 'checked') == 'true';
        if ($isChecked) {
            $I->click(['css' => "input[value='$serviceId'] + .form-checkbox"]);
        }
        return $this;
    }

    public function saveServices() {
        $I = $this->tester;
        $I->click(['css' => "input[type='submit']"]);
        return $this;
    }

    public function addDisplay($id) {
        return $this->clickDropDown($id, 'photo');
    }

    public function updateDisplay($id) {
        return $this->clickDropDown($id, 'photo');
    }

    public function setCoordinates($coordinates) {
        $I = $this->tester;
        $I->fillField(['css' => '#address-gps'], $coordinates);
        return $this;
    }

    public function assertCoordinatesEqual($coordinates) {
        $I = $this->tester;
        $value = $I->grabTextFrom(['css' => '#address-gps']);
        $I->assertEquals($coordinates, $value);
        return $this;
    }

    public function saveDisplay() {
        $I = $this->tester;
        $I->click(['css' => 'button.next']);
        $I->wait(3);
        return $this;
    }

    public function getAddressId($address) {
        $I = $this->tester;
        $this->filter('address', $address);
        return $I->grabTextFrom(['css' => 'td.col-id']);
    }

    /**
     * @param $datetime
     * @throws \Exception
     */
    public function orderPhotographerNextWeek() {
        $I = $this->tester;
        $I->selectOption(['css' => '#monthSelected'], ['value' => date('n') + 1]);
        $I->clickFirstVisible(['css' => 'a.state0']);
        $I->click(['css' => 'button.next']);
        $I->waitForText('Адрес добавлен', 10);
        $I->click(['css' => 'a.move-to-address']);
        return $this;
    }

    public function filterByAddress($address) {
        $this->filterCleaner();
        $this->filter('address', $address);
    }

    public function filterByMetro($metro) {
        $this->filterCleaner();
        $this->filter('metro', $metro);
    }

    public function filterByTax($tax) {
        $this->filterCleaner();
        $this->filter('tax', $tax);
    }

    public function filterByDistrict($dis) {
        $this->filterCleaner();
        $this->filter('district', $dis);
    }

    public function checkFilteringByMetro(){
        $this->filterCleaner();
        $this->checkFiltering();
    }

    public function checkFilteringByAddress(){
        $this->filterCleaner();
        $this->checkFiltering();
    }

    public function checkFilteringByTax(){
        $this->filterCleaner();
        $this->checkFiltering();
    }

    private function clickDropDown($id, $item) {
        $I = $this->tester;
        $I->click(['css' => "tr[data-key='$id'] a.dropdown-toggle"]);
        $I->click(['css' => "tr[data-key='$id'] a[href='/owner/address/update/$id/$item']"]);
        return $this;
    }

    private function filter($field, $value)
    {
        $I = $this->tester;
        $I->click(['css' => '.fa-filter']);
        $filterSelector = self::$fields[$field];
        $I->waitForElementVisible(['css' => $filterSelector]);
        if ($field != 'address') {
            $I->selectOption(['css' => $filterSelector], ['text' => $value]);
        } else {
            $I->fillField(['css' => $filterSelector], $value);
        }
        $I->click(['css' => "#AddressFilterForm button[type='submit']"]);
        $I->waitForElementNotVisible(['css' => 'div.price-filter-row']);
    }

    private function checkFiltering()
    {
        $I = $this->tester;
        $I->isVisible(['css' => '.dropdown-toggle']);
    }

    public function filterCleaner()
    {
        $I = $this->tester;
        $I->click(['css' => '.fa-filter']);
        $I->waitForElementVisible(['css' => '.reg-button.reset']);
        $I->click(['css' => '.reg-button.reset']);
        $I->clearField(['css' => '#addresssearch-address']);
        $I->click(['css' => "#AddressFilterForm button[type='submit']"]);
        $I->waitForElementNotVisible(['css' => 'div.price-filter-row']);
    }

    private function getRowId($name) {
        return $this->tester->findContainerContains(['css' => 'table.price-table tr'], $name)
            ->getAttribute('data-id');
    }
}