<?php
namespace frontend\Page;

use frontend\AcceptanceTester;

class CustomerProfile
{
    public static $URL = '/customer';

    protected $tester;

    public function __construct(AcceptanceTester $I) {
        $this->tester = $I;
    }

    public function open() {
        $I = $this->tester;
        $I->click(['css' => '.person-div a']);
        return $this;
    }

    public function getName() {
        return $this->tester->grabValueFrom(['css' => '#user-fullname']);
    }

    public function setName($name) {
        $this->tester->fillField(['css' => '#user-fullname'], $name);
        return $this;
    }

    public function getEmail() {
        return $this->tester->grabValueFrom(['css' => '#user-email']);
    }

    public function setEmail($email) {
        $this->tester->fillField(['css' => '#user-email'], $email);
        return $this;
    }

    public function getPhone() {
        return $this->tester->grabValueFrom(['css' => '#user-phone']);
    }

    public function setPhone($phone) {
        $this->tester->fillField(['css' => '#user-phone'], $phone);
        return $this;
    }

    public function setOldPass($oldPass) {
        $this->tester->fillField(['css' => '#changepassword-oldpassword'], $oldPass);
        return $this;
    }

    public function setNewPass($newPass) {
        $this->tester->fillField(['css' => '#changepassword-newpassword'], $newPass);
        return $this;
    }

    public function repeatNewPass($newPass) {
        $this->tester->fillField(['css' => '#changepassword-retypepassword'], $newPass);
        return $this;
    }

    public function changePass() {
        $this->tester->click(['css' => "form[action='user/change-password'] button[type='submit'"]);
        $this->tester->wait(1);
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function save() {
        $I = $this->tester;
        $I->click(['css' => 'button.change-data']);
        $I->waitForElement(['css' => '#user-fullname']);
        $I->wait(1);
        return $this;
    }

    public function addPersonEntity() {
        $I = $this->tester;
        $I->scrollTo(['css' => '.rekvizit-container a.dropdown-toggle']);
        $I->click(['css' => '.rekvizit-container a.dropdown-toggle']);
        $I->click(['css' => '.rekvizit-container ul.dropdown-menu li:nth-child(1) a']);
        return new PersonForm($I);
    }

    public function addIndividualEntity() {
        $I = $this->tester;
        $I->scrollTo(['css' => '.rekvizit-container a.dropdown-toggle']);
        $I->click(['css' => '.rekvizit-container a.dropdown-toggle']);
        $I->click(['css' => '.rekvizit-container ul.dropdown-menu li:nth-child(2) a']);
        return new IndividualForm($I);
    }

    public function addCorporateEntity() {
        $I = $this->tester;
        $I->scrollTo(['css' => '.rekvizit-container a.dropdown-toggle']);
        $I->click(['css' => '.rekvizit-container a.dropdown-toggle']);
        $I->click(['css' => '.rekvizit-container ul.dropdown-menu li:nth-child(3) a']);
        return new CorporateForm($I);
    }
}
