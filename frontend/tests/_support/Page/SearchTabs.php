<?php
namespace frontend\Page;

use frontend\AcceptanceTester;

class SearchTabs
{
    // include url of current page
    public static $URL = '/';

    /**
     * @var array all possible search tabs
     */
    public static $tabs = [
        "district" => ["selector" => ".address-search [data-block=\"district\"]", "name" => "по округу"],
        "tax" => ["selector" => ".address-search [data-block=\"tax\"]", "name" => "по номеру налоговой"],
        "metro" => ["selector" => ".address-search [data-block=\"subway\"]", "name" => "по метро"],
        "other" => ["selector" => ".address-search [data-block=\"other\"]", "name" => "по другим параметрам"],
    ];

    public static $districts = [
        'ВАО' => 151, 'ЗАО' => 155, 'ЗелАО' => 156, 'НАО' => 158, 'САО' => 149, 'СВАО' => 150,
        'СЗАО' => 558, 'ТАО' => 157, 'ЦАО' => 162, 'ЮАО' => 153, 'ЮВАО' => 152, 'ЮЗАО' => 154
    ];

    public static $taxDepartments = [
        '1' => 2975, '2' => 2976, '3' => 2977, '17' => 2990, '22' => 2995
    ];

    public static $otherParams = [
        'Возможность оплаты по безналу' => 1175,
        'Бесплатный осмотр помещения перед покупкой' => 1177
    ];

    /**
     * @var AcceptanceTester
     */
    protected $tester;

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    public static function open(AcceptanceTester $I) {
        $I->amOnPage(self::$URL);
        return new SearchTabs($I);
    }

    /**
     * Checks if a tab is selected and active.
     * @param $tab String one of: 'district', 'tax', 'metro'
     * @return $this SearchTab page object
     */
    public function isTabActive($tab)
    {
        $I = $this->tester;
        $I->wait(1);
        $tabClasses = $I->grabAttributeFrom(self::$tabs[$tab]['selector'], 'class');
        $tabVisibleName = self::$tabs[$tab]['name'];
        $I->expect("Закладка '$tabVisibleName' активна");
        $I->assertContains('active', $tabClasses);
        return $this;
    }

    /**
     * Activates a tab.
     * @param $tab String one of: 'district', 'tax', 'metro'
     * @return $this SearchTab page object
     */
    public function clickTab($tab)
    {
        $this->tester->click(self::$tabs[$tab]['selector']);
        return $this;
    }

    /**
     * Clicks of the specified checkbox in 'district'('по округам') tab.
     * @param $district String one of keys of map {@link self::$districts}
     * @return $this SearchTab page object
     */
    public function selectDistrict($district)
    {
        $districtId = self::$districts[$district];
        $this->clickSearchTabItem($districtId);
        return $this;
    }

    /**
     * Clicks of the specified checkbox in 'tax'('по налоговым департаментам') tab.
     * @param $departmentNm String one of keys of map {@link self::$taxDepartments}
     * @return $this SearchTab page object
     */
    public function selectTaxDepartment($departmentNm)
    {
        $departmentId = self::$taxDepartments[$departmentNm];
        $this->clickSearchTabItem($departmentId);
        return $this;
    }

    public function selectMetro($stationName) {
        $this->tester->selectOption(['css' => '#addresssearch-metro_id'], ['text' => $stationName]);
        return $this;
    }

    public function selectParam($paramName) {
        $paramId = self::$otherParams[$paramName];
        $this->clickSearchTabItem($paramId);
        return $this;
    }

    /**
     * Runs search.
     * @return SearchResults
     * @throws \Exception
     */
    public function find()
    {
        $I = $this->tester;
        $I->clickFirstVisible(['css' => 'button.find']);
        $searchResults = new SearchResults($this->tester);
        return $searchResults;
    }

    private function clickSearchTabItem($id) {
        $this->tester->click(['css' => "input[value='$id'] + .form-checkbox"]);
    }
}
