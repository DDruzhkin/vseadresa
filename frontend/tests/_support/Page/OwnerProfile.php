<?php

namespace frontend\Page;

use frontend\AcceptanceTester;

class OwnerProfile
{
    public static $URL = '/index.php/owner';

    protected $tester;

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    public function open()
    {
        $I = $this->tester;
        $I->amOnPage(self::$URL);
    }

    public function downloadAgreement() {
        $I = $this->tester;
        $I->click(['css' => '.message a']);
        $I->waitForElement(['css' => '#plugin'], 3);
        $downloadLink = $I->getCurrentUrl();
        $I->assertContains('.pdf', $downloadLink);
    }

    public function getAgreementId() {
        $I = $this->tester;
        $uri = $I->grabAttributeFrom(['css' => '.message a'], 'href');
        return substr($uri, strrpos($uri, '/') + 1);
    }
}