<?php
namespace frontend\Page;

use frontend\AcceptanceTester;


class PaymentConfirmationList
{
    public static $URL = '/owner/payconfirm';

    protected $tester;

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    public static function open(AcceptanceTester $I)
    {
        $I->amOnPage(self::$URL);
        return new PaymentConfirmationList($I);
    }

    public function assertOrderShows($orderNm) {
        $I = $this->tester;
        $I->see($orderNm);
        return $this;
    }

    public function setPaymentDate($invoiceId, $date) {
        $I = $this->tester;
        $I->fillField(['css' => "input[name='po-date[$invoiceId]']"], $date);
        return $this;
    }

    public function setPaymentReceipt($invoiceId, $receiptNm) {
        $I = $this->tester;
        $I->fillField(['css' => "input[name='po-number[$invoiceId]']"], $receiptNm);
        return $this;
    }

    public function save() {
        $I = $this->tester;
        $I->click(['css' => "button[type='submit']"]);
        $I->wait(1);
        return $this;
    }

    public function assertOrderNotShown($orderNm)
    {
        $I = $this->tester;
        $I->dontSee($orderNm);
        return $this;
    }
}