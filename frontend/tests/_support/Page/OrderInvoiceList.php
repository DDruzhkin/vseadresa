<?php

namespace frontend\Page;

use frontend\AcceptanceTester;
use Facebook\WebDriver\WebDriverBy;

class OrderInvoiceList
{
    private static $URL = '/customer/order/document/';

    private $tester;
    private $orderNm;

    public function __construct(AcceptanceTester $I, $orderNm)
    {
        $this->tester = $I;
        $this->orderNm = $orderNm;
    }

    /**
     * @param AcceptanceTester $I
     * @param $orderId
     * @return OrderInvoiceList
     * @throws \Exception
     */
    public static function open(AcceptanceTester $I, $orderId) {
        $I->amOnPage(self::$URL.$orderId);
        $breadCrumbsOrderLink = $I->grabTextFrom(['css' => "a[href^='/customer/order/view/']"]);
        $matches = [];
        if (!mb_ereg('Заказ №(.+)', $breadCrumbsOrderLink, $matches)) {
            throw new \Exception('Order number was not found on page');
        }
        return new OrderInvoiceList($I, $matches[1]);
    }

    public function assertOwnerServiceInvoiceShows() {
        $I = $this->tester;
        $I->see('Оплата услуг владельца адреса по заказу №'.$this->orderNm);
        return $this;
    }

    public function getOwnerInvoiceIds() {
        return $this->getIds('владельца');
    }

    public function getPortalInvoiceIds() {
        return $this->getIds('портала');
    }

    private function getIds($type) {
        $I = $this->tester;
        $orderIds = [];
        $idx = 0;
        foreach ($I->findElements(['css' => '.price-table tbody tr']) as $row) {
            if (mb_strpos($row->getText(), $type)) {
                $docRef = $row->findElement(WebDriverBy::cssSelector("a[href^='/document/show-pdf/']"))->getAttribute('href');
                $orderIds[$idx++] = substr($docRef, strrpos($docRef, '/') + 1);
            }
        }
        return $orderIds;
    }
}