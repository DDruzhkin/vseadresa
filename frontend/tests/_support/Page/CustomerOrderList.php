<?php
namespace frontend\Page;

use frontend\AcceptanceTester;

class CustomerOrderList
{
    public static $URL = '/customer/order';

    protected $tester;

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    public static function open(AcceptanceTester $I) {
        $I->amOnPage(self::$URL);
        return new CustomerOrderList($I);
    }

    public function assertOrderShows($orderNm)
    {
        $I = $this->tester;
        $I->see($orderNm);
        return $this;
    }

    public function assertOrderNotShown($orderNm)
    {
        $I = $this->tester;
        $I->dontSee($orderNm);
        return $this;
    }

    public function deleteOrder($orderNm)
    {
        $I = $this->tester;
        $orderId = $I->getOrderId($orderNm);
        $I->click(['css' => "tr[data-key='$orderId'] a.delete"]);
        $I->acceptPopup();
        return $this;
    }

    public function grabOrderStatus($orderId) {
        $I = $this->tester;
        return $I->grabTextFrom(['css' => "td.col-status_id a[href='/customer/order/view/$orderId']"]);
    }
}
