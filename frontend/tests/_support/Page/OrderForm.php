<?php
namespace frontend\Page;

use frontend\AcceptanceTester;
use Facebook\WebDriver\WebDriverBy;

class OrderForm
{

    protected $tester;

    private $addressInfo;
    private $orderPrice;
    private $orderTotalPrice;
    private $orderId;
    private $orderNm;
    private $orderPostAddress;
    private $orderDeliveryAddress;
    private $orderDeliveryContact;
    private $orderTimeFrame;
    private $orderServicesTotal;
    private $orderGuaranty;
    private $orderDeliveryType;
    private $availableServices;
    private $selectedServices;


    public static $deliveryTypes = [
        'самовывоз' => '3815',
        'доставка до метро' => '3816',
        'доставка по адресу' => '3817',
        'доставка по адресу в точное время' => '3819'
    ];

    public function __construct(AcceptanceTester $I, $addressInfo)
    {
        $I->expect('Открыта форма заказа');
        $this->tester = $I;
        $this->addressInfo = $addressInfo;
        $this->availableServices = [];
    }

    public function getOrderPrice() {
        return $this->orderPrice;
    }

    public function getOrderId() {
        return $this->orderId;
    }

    public function getOrderNm() {
        return $this->orderNm;
    }

    public function getOrderPostAddress() {
        return $this->orderPostAddress;
    }

    public function getAvailableServices() {
        return $this->availableServices;
    }

    public function getTotalPrice() {
        return $this->orderTotalPrice;
    }

    public function getTimeFrame() {
        return $this->orderTimeFrame;
    }

    public function getDeliveryAddress() {
        return $this->orderDeliveryAddress;
    }

    public function getServiceTotal() {
        return $this->orderServicesTotal;
    }

    public function getDeliveryType() {
        return $this->orderDeliveryType;
    }

    public function getDeliveryContact() {
        return $this->orderDeliveryContact;
    }

    public function getGuaranty() {
        return $this->orderGuaranty;
    }

    public function addGuarantee() {
        $I = $this->tester;
        $I->click(['css' => '.field-order-add_guarantee .cbx-sm']);
        return $this;
    }

    public function setLegalEntity($legalEntity) {
        $I = $this->tester;
        $I->selectOption(['css' => '#order-customer_r_id'], ['text' => $legalEntity]);
        return $this;
    }

    public function setTimeFrame($term) {
        $I = $this->tester;
        $I->selectOption(['css' => '#order-duration'], ['text' => $term]);
        return $this;
    }

    public function grabAvailableServices() {
        $I = $this->tester;
        $serviceCheckboxes = $I->findElements(['css' => '#order-serviceidlist div.checkbox']);
        foreach ($serviceCheckboxes as $elm) {
            $id = $elm->findElement(WebDriverBy::cssSelector("input[type='checkbox']"))->getAttribute('value');
            $name = $elm->getText();
            $this->availableServices[$id] = $name;
        }
        return $this;
    }

    public function setRandomServices() {
        $I = $this->tester;
        foreach ($this->availableServices as $id => $name) {
            if (rand(0,1) == 1) {
                $I->click(['css' => "#order-serviceidlist input[value='$id'] + .form-checkbox"]);
                $this->selectedServices[$id] = $name;
            }
        }
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function saveBillingDetails() {
        $I = $this->tester;
        $I->click(['css' => 'button.next']);
        $I->expect('Откроется форма доставки');
        $I->waitForElement(['css' => '#order-delivery_type_id']);
        return $this;
    }

    public function setDeliveryType($deliveryType) {
        $I = $this->tester;
        $I->selectOption(['css' => '#order-delivery_type_id'], ['value' => self::$deliveryTypes[$deliveryType]]);
        return $this;
    }

    public function setDeliveryAddress($deliveryAddress) {
        $I = $this->tester;
        $I->fillField(['css' => '#order-delivery_address'], $deliveryAddress);
        return $this;
    }

    public function setDeliveryContacts($contact) {
        $I = $this->tester;
        $I->fillField(['css' => '#order-delivery_contacts'], $contact);
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function saveDeliveryDetails() {
        $I = $this->tester;
        $I->click(['css' => 'button.next']);
        $I->expect('Откроется подтверждение, что заказ оформлен');
        $I->waitForElement(['css' => '#order']);
        $I->see('оформлен');
        $this->grabOrderDetails();
        return $this;
    }

    public function assertAddressSameAsOrdered() {
        $I = $this->tester;
        $I->assertEquals($this->addressInfo['postAddress'], $this->orderPostAddress,
            'Адрес в форме заказа такой же, как в карточке, которую заказывали');
        return $this;
    }

    public function assertPriceSameAsOrdered() {
        $I = $this->tester;
        $I->assertEquals($this->addressInfo['price'], $this->orderPrice, 'Цена такая же как в карточке');
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function confirmOrder() {
        $I = $this->tester;
        $I->click(['css' => 'a.reg-button.next']);
        $I->waitForElement(['css' => 'table.price-table']);
        return $this;
    }

    private function grabOrderDetails()
    {
        $I = $this->tester;
        $orderConfirmLink = $I->grabAttributeFrom(['css' => "a.reg-button[href^='/order/confirm']"], 'href');
        $this->orderId = substr($orderConfirmLink, strrpos($orderConfirmLink, '/') + 1);
        $this->orderNm = $I->grabTextFrom(['css' => 'div.field-row:nth-child(1) div.form-input']);
        $this->orderPostAddress = $I->grabTextFrom(['css' => "h1 a[href^='/address']"]);
        $this->orderTimeFrame = $I->grabTextFrom(['css' => 'div.field-row:nth-child(2) div.form-input']);
        $this->orderPrice = self::getPrice($I->grabTextFrom(['css' => 'div.field-row:nth-child(3) div.form-input']));
        $this->orderServicesTotal = self::getPrice($I->grabTextFrom(['css' => 'div.field-row:nth-child(4) div.form-input']));
        $this->orderGuaranty = mb_strpos(
            $I->grabTextFrom(['css' => 'div.field-row:nth-child(5) div.form-input']),
            '00 руб') > -1;
        $this->orderDeliveryType = $I->grabTextFrom(['css' => 'div.field-row:nth-child(5) div.form-input']);
        if (mb_stripos($this->orderDeliveryType, 'самовывоз') > -1) {
            $this->orderTotalPrice = self::getPrice($I->grabTextFrom(['css' => 'div.field-row:nth-child(6) div.form-input']));
        } else {
            $this->orderDeliveryAddress = $I->grabTextFrom(['css' => 'div.field-row:nth-child(6) div.form-input']);
            $this->orderDeliveryContact = $I->grabTextFrom(['css' => 'div.field-row:nth-child(7) div.form-input']);
            $this->orderTotalPrice = self::getPrice($I->grabTextFrom(['css' => 'div.field-row:nth-child(8) div.form-input']));
        }


    }

    /**
     * @throws \Exception
     */
    public function isShown()
    {
        $I = $this->tester;
        $I->waitForElement(['css' => '#order-customer_r_id']);
        $I->seeElement(['css' => '#order-customer_r_id']);
    }

    private static function getPrice($str)
    {
        preg_match("/([\d\s]+),/",$str,$matches);
        if (empty($matches)) {
            return '';
        }
        return str_replace(' ', '', $matches[1]);
    }
}
