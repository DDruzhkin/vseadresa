<?php
namespace frontend\Page;

use frontend\AcceptanceTester;

class CorporateForm
{
    protected $tester;

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    public function getShortName()
    {
        return $this->tester->grabValueFrom(['css' => '#rcompany-name']);
    }

    public function setShortName($shortName)
    {
        $this->tester->fillField(['css' => '#rcompany-name'], $shortName);
        return $this;
    }

    public function getLegalName()
    {
        return $this->tester->grabValueFrom(['css' => '#rcompany-full_name']);
    }

    public function setLegalName($legalName)
    {
        $this->tester->fillField(['css' => '#rcompany-full_name'], $legalName);
        return $this;
    }

    public function getINN()
    {
        return $this->tester->grabValueFrom(['css' => '#rcompany-inn']);
    }

    public function setINN($INN)
    {
        $this->tester->fillField(['css' => '#rcompany-inn'], $INN);
        return $this;
    }

    public function getKPP()
    {
        return $this->tester->grabValueFrom(['css' => '#rcompany-kpp']);
    }

    public function setKPP($KPP)
    {
        $this->tester->fillField(['css' => '#rcompany-kpp'], $KPP);
        return $this;
    }

    public function getOGRN()
    {
        return $this->tester->grabValueFrom(['css' => '#rcompany-ogrn']);
    }

    public function setOGRN($OGRN)
    {
        $this->tester->fillField(['css' => '#rcompany-ogrn'], $OGRN);
        return $this;
    }

    public function getAddress()
    {
        return $this->tester->grabValueFrom(['css' => '#rcompany-address']);
    }

    public function setAddress($address)
    {
        $this->tester->fillField(['css' => '#rcompany-address'], $address);
        return $this;
    }

    public function getSigner()
    {
        return $this->tester->grabValueFrom(['css' => '#rcompany-dogovor_signature']);
    }

    public function setSigner($signerName)
    {
        $this->tester->fillField(['css' => '#rcompany-dogovor_signature'], $signerName);
        return $this;
    }

    public function getSignerTitle()
    {
        return $this->tester->grabValueFrom(['css' => '#rcompany-dogovor_face']);
    }

    public function setSignerTitle($title)
    {
        $this->tester->fillField(['css' => '#rcompany-dogovor_face'], $title);
        return $this;
    }

    public function getConsideration()
    {
        return $this->tester->grabValueFrom(['css' => '#rcompany-dogovor_osnovanie']);
    }

    public function setConsideration($consideration)
    {
        $this->tester->fillField(['css' => '#rcompany-dogovor_osnovanie'], $consideration);
        return $this;
    }

    public function getBankIdentifier()
    {
        return $this->tester->grabValueFrom(['css' => '#rcompany-bank_bik']);
    }

    public function setBankIdentifier($identifier)
    {
        $this->tester->fillField(['css' => '#rcompany-bank_bik'], $identifier);
        return $this;
    }

    public function getBankAccountNm()
    {
        return $this->tester->grabValueFrom(['css' => '#rcompany-account']);
    }

    public function setBankAccountNm($accountNm)
    {
        $this->tester->fillField(['css' => '#rcompany-account'], $accountNm);
        return $this;
    }

    public function getAccountOwner()
    {
        return $this->tester->grabValueFrom(['css' => '#rcompany-account_owner']);
    }

    public function setAccountOwner($accountOwner)
    {
        $this->tester->fillField(['css' => '#rcompany-account_owner'], $accountOwner);
        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function save()
    {
        $I = $this->tester;
        $I->click(['css' => 'button.reg-button']);
        $I->waitForElementVisible(['css' => '.rekvizit-container .card']);
        return $this;
    }
}
