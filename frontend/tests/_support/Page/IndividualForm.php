<?php
namespace frontend\Page;

use frontend\AcceptanceTester;

class IndividualForm
{
    protected $tester;

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    // Personal properties
    public function getFirstName()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-fname']);
    }

    public function setFirstName($firstName)
    {
        $this->tester->fillField(['css' => '#rip-fname'], $firstName);
        return $this;
    }

    public function getMiddleName()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-mname']);
    }

    public function setMiddleName($lastName)
    {
        $this->tester->fillField(['css' => '#rip-mname'], $lastName);
        return $this;
    }

    public function getLastName()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-lname']);
    }

    public function setLastName($lastName)
    {
        $this->tester->fillField(['css' => '#rip-lname'], $lastName);
        return $this;
    }

    public function getPassportNm()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-pasport_number']);
    }

    public function setPassportNm($nm)
    {
        $this->tester->fillField(['css' => '#rip-pasport_number'], $nm);
        return $this;
    }

    public function getPassportIssueDate()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-pasport_date']);
    }

    public function setPassportIssueDate($issueDate)
    {
        $this->tester->fillField(['css' => '#rip-pasport_date'], $issueDate);
        return $this;
    }

    public function getPassportIssuer()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-pasport_organ']);
    }

    public function setPassportIssuer($issuer)
    {
        $this->tester->fillField(['css' => '#rip-pasport_organ'], $issuer);
        return $this;
    }

    public function getAddress()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-address']);
    }

    public function setAddress($address)
    {
        $this->tester->fillField(['css' => '#rip-address'], $address);
        return $this;
    }

    // Individual entity properties
    public function getINN()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-inn']);
    }

    public function setINN($INN)
    {
        $this->tester->fillField(['css' => '#rip-inn'], $INN);
        return $this;
    }

    public function getOGRNIP()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-ogrnip']);
    }

    public function setOGRNIP($OGRNIP)
    {
        $this->tester->fillField(['css' => '#rip-ogrnip'], $OGRNIP);
        return $this;
    }

    public function getCertificateSeries()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-sv_seriya']);
    }

    public function setCertificateSeries($certificateSeries)
    {
        $this->tester->fillField(['css' => '#rip-sv_seriya'], $certificateSeries);
        return $this;
    }

    public function getCertificateNumber()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-sv_nomer']);
    }

    public function setCertificateNumber($certificateNumber)
    {
        $this->tester->fillField(['css' => '#rip-sv_nomer'], $certificateNumber);
        return $this;
    }

    public function getCertificateIssueDate()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-sv_date']);
    }

    public function setCertificateIssueDate($certificateIssueDate)
    {
        $this->tester->fillField(['css' => '#rip-sv_date'], $certificateIssueDate);
        return $this;
    }

    //Bank account properties
    public function getBankIdentifier()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-bank_bik']);
    }

    public function setBankIdentifier($identifier)
    {
        $this->tester->fillField(['css' => '#rip-bank_bik'], $identifier);
        return $this;
    }

    public function getBankName()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-bank_name']);
    }

    public function setBankName($bankName)
    {
        $this->tester->fillField(['css' => '#rip-bank_name'], $bankName);
        return $this;
    }

    public function getCorrespondentAccount()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-bank_cor']);
    }

    public function setCorrespondentAccount($correspondentAccount)
    {
        $this->tester->fillField(['css' => '#rip-bank_cor'], $correspondentAccount);
        return $this;
    }

    public function getBankAccountNm()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-account']);
    }

    public function setBankAccountNm($accountNm)
    {
        $this->tester->fillField(['css' => '#rip-account'], $accountNm);
        return $this;
    }

    public function getAccountOwner()
    {
        return $this->tester->grabValueFrom(['css' => '#rip-account_owner']);
    }

    public function setAccountOwner($accountOwner)
    {
        $this->tester->fillField(['css' => '#rip-account_owner'], $accountOwner);
        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function save()
    {
        $I = $this->tester;
        $I->click(['css' => 'button.reg-button']);
        $I->waitForElementVisible(['css' => '.rekvizit-container .card']);
        return $this;
    }
}
