<?php
namespace frontend\Page;

use frontend\AcceptanceTester;


class OwnerRegForm
{
    public static $URL = '/owner/signup';

    public static $entityTypes = [
        'person' => '1',
        'individual' => '3',
        'corporate' => '2'
    ];

    protected $tester;

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    public static function open(AcceptanceTester $I)
    {
        $I->amOnPage(self::$URL);
        return new OwnerRegForm($I);
    }

    public function setName($name)
    {
        $this->tester->fillField(['css' => '#ownersignup-fullname'], $name);
        return $this;
    }

    public function setEmail($email)
    {
        $this->tester->fillField(['css' => '#ownersignup-username'], $email);
        return $this;
    }

    public function setPassword($password)
    {
        $this->tester->fillField(['css' => '#ownersignup-password'], $password);
        return $this;
    }

    public function setPasswordConfirmation($password)
    {
        $this->tester->fillField(['css' => '#ownersignup-password_repeat'], $password);
        return $this;
    }

    public function setPhone($phone)
    {
        $this->tester->fillField(['css' => '#ownersignup-phone'], $phone);
        return $this;
    }

    // Step 2 of registration
    public function setEntityType($entityType)
    {
        $this->tester->selectOption(['css' => '#choose-your-status'], ['value' => self::$entityTypes[$entityType]]);
        return $this;
    }

    public function setPostcode($postcode)
    {
        $this->tester->fillField(['css' => '#deliveryaddress-index'], $postcode);
        return $this;
    }

    public function setRegion($region)
    {
        $this->tester->selectOption(['css' => '#deliveryaddress-district'], ['value' => $region]);
        return $this;
    }

    public function setDistrict($district)
    {
        $this->tester->fillField(['css' => '#region-region'], $district);
        return $this;
    }

    // set selectors
    public function setTown($town)
    {
        $this->tester->fillField(['css' => '#deliveryaddress-destination'], $town);
        return $this;
    }

    public function setAddress($address)
    {
        $this->tester->fillField(['css' => '#deliveryaddress-address'], $address);
        return $this;
    }

    public function setNotes($notes)
    {
        $this->tester->fillField(['css' => '#deliveryaddress-info'], $notes);
        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function saveStepOne()
    {
        $I = $this->tester;
        $I->click(['css' => 'button.reg-button']);
        $I->waitForElementVisible(['css' => '#choose-your-status']);
        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function saveStepTwo()
    {
        $I = $this->tester;
        $I->click(['css' => 'button.reg-button']);
        $I->waitForElementVisible(['css' => '.rekvizit-container .card']);
        return $this;
    }
}
