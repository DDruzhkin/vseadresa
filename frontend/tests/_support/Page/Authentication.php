<?php
namespace frontend\Page;

use frontend\AcceptanceTester;

class Authentication
{
    // include url of current page
    public static $URL = '/login';
    public static $LOGIN_TIMEOUT = 3;

    private static $loginField = ['css' => "#loginform-username"];
    private static $passwdField = ['css' => "#loginform-password"];
    private static $loggedInUser = ['css' => "div.username-field"];

    /**
     * @var AcceptanceTester
     */
    protected $tester;

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    public static function open(AcceptanceTester $I)
    {
        $I->amOnPage(self::$URL);
        return new Authentication($I);
    }

    /**
     * @param $user
     * @param $passwd
     * @return $this
     * @throws \Exception
     */
    public function login($user, $passwd)
    {
        $I = $this->tester;
        $I->waitForElement(self::$loginField, self::$LOGIN_TIMEOUT);
        $I->fillField(self::$loginField, $user);
        $I->fillField(self::$passwdField, $passwd);
        $I->click(['css' => "button.login-button"]);
        return $this;
    }

    public function logout() {
        $I = $this->tester;
        $I->click(['css' => '.hidden-xs div.logout-icon-div a']);
        return $this;
    }

    public function assertLoginPassed()
    {
        $I = $this->tester;
        $I->expect("Пользователь успешно зашел в систему. Имя пользователя отображается в заголовке страницы");
        $timeout = 15;
        $waitTime = 0;
        $username = "";
        while ($waitTime++ < $timeout) {
            $username = $I->grabTextFrom(self::$loggedInUser);
            if (!empty($username)) {
                break;
            }
            $I->wait(1);
        }
        $I->assertNotEmpty($username);
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function assertLoginFailed()
    {
        $I = $this->tester;
        $I->expect("Пользователь не смог зайти в систему. Отображается ошибка.");
        $I->waitForElement(['css' => '#w0-error-0'], self::$LOGIN_TIMEOUT);
        $errMsg = $I->grabTextFrom(['css' => '#w0-error-0']);
        $I->assertNotEmpty($errMsg);
        $I->assertContains('пароль', $errMsg);
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function assertUserNotLoggedIn() {
        $I = $this->tester;
        $I->waitForElement(['css' => "img[src='/img/locked.png']"], self::$LOGIN_TIMEOUT);
        $username = $I->grabTextFrom(self::$loggedInUser);
        $I->assertEmpty($username);
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function assertLoginFormShown()
    {
        $I = $this->tester;
        $I->expect('Страница недоступна неавторизованным пользователям, показывается форма для ввода логина/пароля');
        $I->waitForElement(self::$loginField, self::$LOGIN_TIMEOUT);
        return $this;
    }

    public function isLoggedIn()
    {
        $I = $this->tester;
        if ($I->isVisible(self::$loggedInUser)) {
            $username = $I->grabTextFrom(self::$loggedInUser);
            return !empty($username);
        }
        return false;
    }

    public function getAuthenticatedUser()
    {
        $I = $this->tester;
        return $I->grabTextFrom(self::$loggedInUser);
    }
}
