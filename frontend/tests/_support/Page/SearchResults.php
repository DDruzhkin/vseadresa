<?php
namespace frontend\Page;

use frontend\AcceptanceTester;

class SearchResults
{
    public static $SEARCH_TIMEOUT = 5;
    /**
     * @var AcceptanceTester
     */
    protected $tester;

    public function __construct(AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    /**
     * Checks if at least one address has been found.
     * It waits for {@link self::$SEARCH_TIMEOUT} sec until search completes.
     * @throws \Exception
     */
    public function assertResultsFound()
    {
        $I = $this->tester;
        $I->expect("Отображаются результаты поиска");
        $I->waitForElement(['css' => "div.addresses-list"], self::$SEARCH_TIMEOUT);
        $I->seeElement(['css' => "div.address-card"]);
        return $this;
    }

    public function assertNoResultsFound() {
        $I = $this->tester;
        $I->expect("Не нашлось ни одного адреса");
        $I->waitForElement(['css' => '.address-not-found'], self::$SEARCH_TIMEOUT);
        return $this;
    }

    public function orderAddress($itemNm)
    {
        $I = $this->tester;
        $this->assertResultsFound();
        $postAddress = $I->grabTextFrom(['css' => "div.addresses-list:nth-child($itemNm) div.address a"]);
        $price = $I->grabTextFrom(['css' => "div.addresses-list:nth-child($itemNm) .address-price"]);
        $I->click(['css' => "div.addresses-list:nth-child($itemNm) .order-fancybox"]);
        return new OrderForm($I, ['postAddress' => $postAddress, 'price' => $price]);
    }
}
