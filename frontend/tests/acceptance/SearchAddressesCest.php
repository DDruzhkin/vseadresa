<?php
namespace frontend\acceptance;

use frontend\Page\SearchTabs;
use frontend\AcceptanceTester;

class SearchAddressesCest
{
    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function findByTaxDepartment(AcceptanceTester $I)
    {
        SearchTabs::open($I)->clickTab('tax')->isTabActive('tax')
            ->selectTaxDepartment('17')->find()->assertResultsFound();
    }

    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function findByTaxDepartment_noResults(AcceptanceTester $I)
    {
        SearchTabs::open($I)->clickTab('tax')->isTabActive('tax')
            ->selectTaxDepartment('22')->find()->assertNoResultsFound();
    }

    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function findByMetro(AcceptanceTester $I)
    {
        SearchTabs::open($I)->clickTab('metro')->isTabActive('metro')
            ->selectMetro('м.Телецентр')->find()->assertResultsFound();
    }

    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function findByOtherParams(AcceptanceTester $I)
    {
        SearchTabs::open($I)->clickTab('other')->isTabActive('other')
            ->selectParam('Возможность оплаты по безналу')->find()->assertResultsFound();

        SearchTabs::open($I)->clickTab('other')->isTabActive('other')
            ->selectParam('Возможность оплаты по безналу')
            ->selectParam('Бесплатный осмотр помещения перед покупкой')
            ->find()->assertResultsFound();
    }
}