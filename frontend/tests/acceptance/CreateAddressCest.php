<?php

namespace frontend\acceptance;

use frontend\AcceptanceTester;
use frontend\Page\AddressManagement;
use frontend\Page\Authentication;
use frontend\Page\SearchTabs;
use backend\Page\AddressManagement as BackendAddressManagement;
use frontend\Page\ServiceManagement;

class CreateAddressCest
{
    /**
     * @param AcceptanceTester $I
     * @depends frontend\acceptance\AuthenticationCest:acceptAgreement
     * @throws \Exception
     */
    public function createServices(AcceptanceTester $I) {
        $email = $I->getContextParam('ownerEmail');
        $pass = $I->getContextParam('ownerPass');
        Authentication::open($I)->login($email, $pass)->assertLoginPassed();

        $serviceName = 'Охрана';
        ServiceManagement::open($I)->addService()->setBuiltInService($serviceName)->saveBasicDetails()
            ->setPriceSixMonth('35000')->setPriceElevenMonth('60000')
            ->savePriceDetails();
        $serviceId = ServiceManagement::open($I)->getServiceId($serviceName);
        ServiceManagement::open($I)->updateService($serviceId)
            ->assertBuiltInServiceEquals($serviceName)->assertServiceNameEquals($serviceName)->saveBasicDetails()
            ->assertPriceSixMonthEquals('35000')->assertPriceElevenMonthEquals('60000');

        $serviceName = 'Уборка помещений';
        ServiceManagement::open($I)->addService()->setBuiltInService($serviceName)
            ->setAddToAddress()->saveBasicDetails()
            ->setPriceSixMonth('15000')->setPriceElevenMonth('25000')
            ->savePriceDetails();
        $serviceId = ServiceManagement::open($I)->getServiceId($serviceName);
        ServiceManagement::open($I)->updateService($serviceId)->assertBuiltInServiceEquals($serviceName)
            ->assertAddToAddressChecked()->saveBasicDetails()
            ->assertPriceSixMonthEquals('15000')->assertPriceElevenMonthEquals('25000');

        $serviceName = 'Эксклюзивная услуга';
        ServiceManagement::open($I)->addService()->setServiceName($serviceName)->setAddToAddress()
            ->setDescription('Услуга, которой ни у кого нет')->saveBasicDetails()
            ->setServiceType('Разовая')->setPrice('5000')->savePriceDetails();
        $serviceId = ServiceManagement::open($I)->getServiceId($serviceName);
        ServiceManagement::open($I)->updateService($serviceId)->assertServiceNameEquals($serviceName)
            ->assertAddToAddressChecked()->assertDescriptionEquals('Услуга, которой ни у кого нет')->saveBasicDetails()
            ->assertServiceTypeEquals('Разовая')->assertPriceEquals('5000');

    }

    /**
     * @param AcceptanceTester $I
     * @depends createServices
     * @throws \Exception
     */
    public function createAddress(AcceptanceTester $I)
    {
        $email = $I->getContextParam('ownerEmail');
        $pass = $I->getContextParam('ownerPass');
        Authentication::open($I)->login($email, $pass)->assertLoginPassed();

        $postCode = '125983';
        $postAddress = 'ул. Академика Королева, д.16';
        $taxDepartment = 'ИФНС №17';
        $metro = 'м.Телецентр';
        $addressId = AddressManagement::open($I)->addAddress()->setPostCode($postCode)->setAddress($postAddress)
            ->setTaxDept($taxDepartment)->setMetro($metro)->saveBasicProperties()
            ->orderPhotographerNextWeek()->getAddressId($postAddress);
        AddressManagement::open($I)->updateBasicProperties($addressId)->assertPostCodeEquals($postCode)
            ->assertAddressEquals($postAddress)->assertTaxDeptEquals($taxDepartment);

        AddressManagement::open($I)->addServices($addressId)
            ->addService('Уборка помещений', '30000', '50000')
            ->addService('Охрана', '45000', '80000')
            ->saveServices();
        AddressManagement::open($I)->updateServices($addressId)
            ->verifyService('Уборка помещений', '30000', '50000')
            ->verifyService('Охрана', '45000', '80000')
            ->verifyOneTimeService('Эксклюзивная услуга', '5000'); // this service should be added by-default

        AddressManagement::open($I)->addPublishProperties($addressId)
            ->setSixMonthPrice('35000')->setElevenMonthPrice('69000')->setFloorspace('40')
            ->setOption('Фактический адрес')->setOption('Адрес не является массовым')
            ->setOption('Предоставление рабочего места')->setOption('Возможность оплаты по безналу')
            ->savePublishProperties();
        AddressManagement::open($I)->updatePublishProperties($addressId)
            ->assertSixMonthPriceEquals('35000')->assertElevenMonthPriceEquals('69000')
            ->assertFloorspaceEquals('40')
            ->assertOptionChecked('Фактический адрес')->assertOptionChecked('Адрес не является массовым')
            ->assertOptionChecked('Предоставление рабочего места')->assertOptionChecked('Возможность оплаты по безналу');

        $I->switchToBackend();
        BackendAddressManagement::open($I)->publish($addressId);
        $I->switchToFrontend();
        AddressManagement::open($I)->addDisplay($addressId)->setCoordinates('55.822818, 37.601241')->saveDisplay();
        AddressManagement::open($I)->updateDisplay($addressId)->assertCoordinatesEqual('55.822818, 37.601241');

        $I->amOnPage('/');
        SearchTabs::open($I)->clickTab('metro')
            ->isTabActive('metro')->selectMetro($metro)->find()->assertResultsFound();
        $I->see($postAddress);
        $I->setContextParam('addressPostCode', $postCode);
        $I->setContextParam('addressLocation', $postAddress);
        $I->setContextParam('addressTaxDepartment', $taxDepartment);
        $I->setContextParam('addressMetro', $metro);
    }
}