<?php
namespace frontend\acceptance;

use frontend\AcceptanceTester;
use frontend\Page\Authentication;
use frontend\Page\CustomerProfile;

class ChangeProfileCest
{
    /**
     * @depends frontend\acceptance\AuthenticationCest:registerCustomer
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function changeBasicDetails(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $auth = Authentication::open($I)
            ->login($I->getContextParam('customerEmail'), $I->getContextParam('customerPass'))
            ->assertLoginPassed();
        $profile = new CustomerProfile($I);
        $profile->open();
        $oldName = $profile->getName();
        $oldEmail = $profile->getEmail();
        $oldPhone = $profile->getPhone();
        $profile->setName('Тест')->setEmail('test_test@test.com')
            ->setPhone('7(495)111-11-11')->save();

        $I->assertEquals('Тест', $profile->getName());
        $I->assertEquals('test_test@test.com', $profile->getEmail());
        $I->assertEquals('+7(495) 111-11-11', $profile->getPhone());

        $profile->open();
        $this->assertUserInHeaderChanged($I, 'Тест', $auth);

        $profile->open()->setName($oldName)->setEmail($oldEmail)->setPhone($oldPhone)->save();
    }

    /**
     * @depends frontend\acceptance\AuthenticationCest:registerCustomer
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function addPersonEntity(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        Authentication::open($I)
            ->login($I->getContextParam('customerEmail'), $I->getContextParam('customerPass'))
            ->assertLoginPassed();
        $profile = new CustomerProfile($I);
        $profile->open();

        $profile->addPersonEntity()
            ->setFirstName("Василий")->setMiddleName("Петрович")->setLastName("Иванов")
            ->setPassportNm("45 56 630594")
            ->setPassportIssueDate("12.01.2007")
            ->setPassportIssuer("ОВД \"Восточное Дегунино\"")
            ->setAddress("Дубнинская ул. д.5 кв.10")
            ->setBankIdentifier("349572074")
            ->setBankAccountNm("39494000000000299508")
            ->setAccountOwner("Василий Иванов")
            ->save();

        $profile->open();
        $card = $I->findContainerContains(['css' => '.rekvizit-container .card'], 'Василий Петрович Иванов');
        $I->assertNotNull($card);
        $I->setContextParam('customerEntity', 'Физлицо (Иванов)');
    }

    /**
     * @depends frontend\acceptance\AuthenticationCest:registerCustomer
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function addIndividualEntity(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        Authentication::open($I)
            ->login($I->getContextParam('customerEmail'), $I->getContextParam('customerPass'))
            ->assertLoginPassed();
        $profile = new CustomerProfile($I);
        $profile->open();

        $profile->addIndividualEntity()
            ->setFirstName("Василий")->setMiddleName("Петрович")->setLastName("Иванов")
            ->setPassportNm("45 56 630594")
            ->setPassportIssueDate("12.01.2007")
            ->setPassportIssuer("ОВД \"Восточное Дегунино\"")
            ->setAddress("Дубнинская ул. д.5 кв.10")
            ->setINN("457928479558")
            ->setOGRNIP("435739452309013")
            ->setCertificateSeries("11")
            ->setCertificateNumber("123456789")
            ->setCertificateIssueDate("15.11.2012")
            ->setBankIdentifier("349572074")
            ->setBankName("ООО КБ Громозека банк")
            ->setCorrespondentAccount("30101110500000000111")
            ->setBankAccountNm("39494000000000299508")
            ->setAccountOwner("Василий Иванов")
            ->save();

        $profile->open();
        $card = $I->findContainerContains(['css' => '.rekvizit-container .card'], '4357394523090');
        $I->assertNotNull($card);
    }

    /**
     * @depends frontend\acceptance\AuthenticationCest:registerCustomer
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function addCorporateEntity(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        Authentication::open($I)
            ->login($I->getContextParam('customerEmail'), $I->getContextParam('customerPass'))
            ->assertLoginPassed();
        $profile = new CustomerProfile($I);
        $profile->open();

        $profile->addCorporateEntity()
            ->setShortName("ООО Парамбука")
            ->setLegalName("Общество с ограниченной ответственностью \"Парамбука\"")
            ->setINN("84534532749")
            ->setKPP("4593847503")
            ->setOGRN("458345937593")
            ->setAddress("113648, Москва, ул. Красного Маяка д.3/5")
            ->setSigner("Иванов В.П.")
            ->setBankIdentifier("349572074")
            ->setBankAccountNm("39494000000000299508")
            ->setAccountOwner("Василий Иванов")
            ->save();

        $profile->open();
        $card = $I->findContainerContains(['css' => '.rekvizit-container .card'], 'Парамбука');
        $I->assertNotNull($card);
    }

    /**
     * @depends frontend\acceptance\AuthenticationCest:registerCustomer
     * @throws \Exception
     */
    public function changePassword(AcceptanceTester $I) {
        $pass = $I->getContextParam('customerPass');
        $newPass = 'hgoehh12434';
        $auth = Authentication::open($I)
            ->login($I->getContextParam('customerEmail'), $pass)
            ->assertLoginPassed();
        $profile = new CustomerProfile($I);
        $profile->open()->setOldPass($pass)->setNewPass($newPass)->repeatNewPass($newPass)->changePass();

        $auth->logout();
        Authentication::open($I)->login($I->getContextParam('customerEmail'), $newPass)
            ->assertLoginPassed();
        $profile->open()->setOldPass($newPass)->setNewPass($pass)->repeatNewPass($pass)->changePass();
    }

    private function assertUserInHeaderChanged(AcceptanceTester $I, $expectedUser, Authentication $auth)
    {
        $timeout = 5;
        $waitTime = 0;
        $username = "";
        while ($waitTime++ < $timeout) {
            $username = $auth->getAuthenticatedUser();
            if (strcmp($expectedUser, $username)) {
                return;
            }
            $I->wait(1);
        }
        $I->assertEquals($expectedUser, $username);
    }
}