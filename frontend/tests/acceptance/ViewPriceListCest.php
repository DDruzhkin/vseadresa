<?php
namespace frontend\acceptance;

use frontend\AcceptanceTester;
use frontend\Page\PriceList;

class ViewPriceListCest
{
    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function checkSorting(AcceptanceTester $I)
    {
        $I->expect('Открылся прайс-лист всех адресов');
        $priceList = PriceList::open($I);
        $I->expect('Прайс-лист отсортирован по цене за 6 мес по возрастанию');
        $priceList->sort('price6')->assertColumnSortedAsc('price6');
        $I->expect('Прайс-лист отсортирован по цене за 6 мес по убыванию');
        $priceList->sort('price6')->assertColumnSortedDesc('price6');

        $I->expect('Прайс-лист отсортирован по цене за 11 мес по возрастанию');
        $priceList->sort('price11')->assertColumnSortedAsc('price11');
        $I->expect('Прайс-лист отсортирован по цене за 11 мес по убыванию');
        $priceList->sort('price11')->assertColumnSortedDesc('price11');
    }

    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function checkFilteringAndOrdering(AcceptanceTester $I)
    {
        $priceList = PriceList::open($I);
        $I->expect('Открылся диалог фильтрации');
        $metro = 'м.Телецентр';
        $priceList->filterByMetro($metro);
        $I->expect('Прайс-лист отфильтрован по метро '.$metro);
        $I->wait(3);
        $priceList->assertColumnFiltered('metro', $metro);
        $orderForm = $priceList->orderAddress(1);
        $orderForm->isShown();
    }
}