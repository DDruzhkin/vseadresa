<?php
namespace frontend\acceptance;

use frontend\AcceptanceTester;
use frontend\Page\Authentication;
use frontend\Page\AddressManagement;


class FilterAddressCest
{
    /**
     * @param AcceptanceTester $I
     * @depends frontend\acceptance\CreateAddressCest:createAddress
     * @throws \Exception
     */
    public function checkOwnerFilteringAndOrdering(AcceptanceTester $I)
    {
        $I->expect('Открылась страница адресов');
        Authentication::open($I)->
        login($I->getContextParam('ownerEmail'), $I->getContextParam('ownerPass'))->assertLoginPassed();
        AddressManagement::open($I);

        $postAddress = $I->getContextParam('addressLocation');
        $taxDepartment = $I->getContextParam('addressTaxDepartment');
        $metro = $I->getContextParam('addressMetro');
        $orderList = new AddressManagement($I);

        $I->expect('Открылся диалог фильтрации');
        $I->expect('В адресах присутствует адресс '.$postAddress);
        $orderList->filterByAddress($postAddress);
        $orderList->checkFilteringByAddress();
        $orderList->filterByTax($taxDepartment);
        $I->expect('В адресах присутствует налоговая '.$taxDepartment);
        $orderList->checkFilteringByTax();
        $orderList->filterByMetro($metro);
        $I->expect("В адресах присутствует метро $metro");
        $orderList->checkFilteringByMetro();
    }
}