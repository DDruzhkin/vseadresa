<?php
namespace frontend\acceptance;

use frontend\AcceptanceTester;
use common\tests\helpers\DFS;
use frontend\Page\Authentication;


class SiteTraversalCest
{

    public function checkOpenPages(AcceptanceTester $I)
    {
        $site = DFS::start('http://portal.steigenhaus.com/', $I);
        $I->assertFalse($site->errorsFound(), 'Найдены битые ссылки или ошибки на странице см. отчет выше');
    }

    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function checkCustomerPages(AcceptanceTester $I)
    {
        $auth = Authentication::open($I);
        if ($auth->isLoggedIn())
        {
            $auth->logout()->assertUserNotLoggedIn();
        }
        $auth->login('customer@mailmail.ru', '123123')->assertLoginPassed();
        $site = DFS::start('http://portal.steigenhaus.com/customer', $I);
        $I->assertFalse($site->errorsFound(), 'Найдены битые ссылки или ошибки на странице см. отчет выше');
    }

    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function checkOwnerPages(AcceptanceTester $I)
    {
        $auth = Authentication::open($I);
        if ($auth->isLoggedIn())
        {
            $auth->logout()->assertUserNotLoggedIn();
        }
        $auth->login('owner@mailmail.ru', '123123')->assertLoginPassed();
        $site = DFS::start('http://portal.steigenhaus.com/owner', $I);
        $I->assertFalse($site->errorsFound(), 'Найдены битые ссылки или ошибки на странице см. отчет выше');
    }

    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function checkAdminPages(AcceptanceTester $I)
    {
        $auth = Authentication::open($I);
        if ($auth->isLoggedIn())
        {
            $auth->logout()->assertUserNotLoggedIn();
        }
        $I->amOnPage('/admin');
        $auth->login('admin@mailmail.ru', '123123')->assertLoginPassed();
        $site = DFS::start('http://portal.steigenhaus.com/admin', $I);
        $I->assertFalse($site->errorsFound(), 'Найдены битые ссылки или ошибки на странице см. отчет выше');
    }
}