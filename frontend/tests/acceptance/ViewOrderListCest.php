<?php
namespace frontend\acceptance;

use frontend\AcceptanceTester;
use frontend\Page\Authentication;
use frontend\Page\OwnerOrderList;

class ViewOrderListCest
{
    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function checkOwnerSorting(AcceptanceTester $I)
    {
        $I->expect('Открылась таблица всех заказов');
        Authentication::open($I)->login('owner@mailmail.ru', '123123');
        $orderList = OwnerOrderList::open($I);
        $I->expect('Заказы отсортированы по цене по возрастанию');
        $orderList->sort('price');
        $orderList->assertColumnSortedAsc('price');
        $I->expect('Заказы отсортированы по цене по убыванию');
        $orderList->sort('price');
        $orderList->assertColumnSortedDesc('price');
        $I->expect('Заказы отсортированы по номеру по возрастанию');
        $orderList->sort('number');
        $orderList->assertColumnSortedAsc('number_el');
        $I->expect('Заказы отсортированы по номеру по убыванию');
        $orderList->sort('number');
        $orderList->assertColumnSortedDesc('number_el');
        $I->expect('Заказы отсортированы по дате по возрастанию');
        $orderList->sort('date');
        $orderList->assertDateColumnSortedAsc('date');
        $I->expect('Заказы отсортированы по дате по убыванию');
        $orderList->sort('date');
        $orderList->assertDateColumnSortedDesc('date');
    }

    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function checkOwnerFilteringAndOrdering(AcceptanceTester $I)
    {
        Authentication::open($I)->login('owner@mailmail.ru', '123123');
        $orderList = OwnerOrderList::open($I);

        $I->expect('Открылся диалог фильтрации');
        $numberValue = $orderList->grabFirstElementColumn('number');
        $orderList->filter('number', $numberValue);
        $I->expect('В списке заказов присутствует номер '.$numberValue);
        $orderList->columnContain('number', $numberValue);
        $orderList->cancelCurrentFilter();

        $dateValue = $orderList->grabFirstElementColumn('date_el');
        $I->expect('В списке заказов присутствует заказ с датой '.$dateValue);
        $orderList->filterDate(substr($dateValue, 0, 10));
        $orderList->columnContain('date', $dateValue);
        $I->wait(1);
    }
}