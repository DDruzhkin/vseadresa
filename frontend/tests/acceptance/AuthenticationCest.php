<?php
namespace frontend\acceptance;

use frontend\AcceptanceTester;
use frontend\Page\Authentication;
use frontend\Page\CustomerRegForm;
use frontend\Page\OwnerProfile;
use frontend\Page\OwnerRegForm;
use frontend\Page\CustomerOrderList;
use frontend\Page\PersonForm;
use frontend\Page\CustomerProfile;
use common\tests\helpers\MailHandler;
use backend\Page\AgreementManagement;
use backend\Page\Authentication as BackendAuthentication;

class AuthenticationCest
{
    private static $PASS = '123123';

    private $mailHandler;
    private $ownerEmail;
    private $agreementId;

    /**
     * @param AcceptanceTester $I
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \yii\base\ErrorException
     */
    function _before(AcceptanceTester $I)
    {
        $this->mailHandler = new MailHandler($I->getMailCatcherUrl());
        $this->mailHandler->deleteAllMails();
    }

    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function loginSuccess(AcceptanceTester $I)
    {
        Authentication::open($I)
            ->login('customer@mailmail.ru', '123123')
            ->assertLoginPassed();
    }

    /**
     * @param AcceptanceTester $I
     * @depends loginSuccess
     * @throws \Exception
     */
    public function logoutSuccess(AcceptanceTester $I) {
        $auth = new Authentication($I);
        $auth->logout()->assertUserNotLoggedIn();
    }

    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function loginFailed(AcceptanceTester $I)
    {
        Authentication::open($I)
            ->login('customer@mailmail.ru', '!!!')
            ->assertLoginFailed();
    }

    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function unauthorizedAccessToPages(AcceptanceTester $I)
    {
        $I->amOnPage(CustomerProfile::$URL);
        $auth = new Authentication($I);
        $auth->assertLoginFormShown();

        $I->amOnPage(CustomerOrderList::$URL);
        $auth->assertLoginFormShown();
    }

    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function registerCustomer(AcceptanceTester $I)
    {
        $suffix = $I->generateRandomStr();
        $email = "addressov.pokupatel.$suffix@gmail.com";
        CustomerRegForm::open($I)
            ->setName('Адресов Покупатель')
            ->setEmail($email)
            ->setPassword(self::$PASS)
            ->setPasswordConfirmation(self::$PASS)
            ->setPhone('4951112211')
            ->setEntityType('person')
            ->checkAcceptTerms()
            ->save();

        $I->expect("Приглашение пришло на email заказчика");
        $message = $this->mailHandler->getMailTo($email);
        $I->assertContains('Благодарим за успешную регистрацию', $message);
        $I->setContextParam('customerEmail', $email);
        $I->setContextParam('customerPass', self::$PASS);
    }

    /**
     * @param AcceptanceTester $I
     * @throws \Exception
     */
    public function registerOwner(AcceptanceTester $I)
    {
        $suffix = $I->generateRandomStr();
        $this->ownerEmail = "addressov.prodavets.$suffix@gmail.com";
        $ownerForm = OwnerRegForm::open($I)
            ->setName('Адресов Продавец')
            ->setEmail($this->ownerEmail)
            ->setPassword(self::$PASS)
            ->setPasswordConfirmation(self::$PASS)
            ->setPhone('4951112211')
            ->saveStepOne()
            ->setEntityType('person')
            ->setPostcode('122612')
            ->setRegion('Москва')
            ->setTown('Звенигород')
            ->setAddress('ул. Усманова, д.3 кв.17');
        $personRegForm = new PersonForm($I);
        $personRegForm
            ->setLastName('Колыванов')->setFirstName('Юрий')->setMiddleName('Дмитриевич')
            ->setPassportNm('45 56 630594')->setPassportIssueDate('12.01.2007')
            ->setPassportIssuer('ОВД \'Восточное Дегунино\'')
            ->setAddress('Дубнинская ул. д.5 кв.10')
            ->setBankIdentifier('349572074')
            ->setBankName("ООО КБ Громозека банк")
            ->setCorrespondentAccount("30101110500000000111")
            ->setBankAccountNm('39494000000000299508')
            ->setAccountOwner('Колыванов Ю.Д.');
        $ownerForm->saveStepTwo();

        $I->expect('Приглашение пришло на email владельца');
        $message = $this->mailHandler->getMailTo($this->ownerEmail);
        $I->assertContains('Благодарим за успешную регистрацию', $message);

        $I->expect('Открылась страница профиля с предложением подписать договор');
        $I->see('Для работы необходимо заключить договор');
        $ownerProfile = new OwnerProfile($I);
        $this->agreementId = $ownerProfile->getAgreementId();
    }

    /**
     * Comment this test because headless chrome does not support pdf-viewer plugin
     * https://github.com/GoogleChrome/puppeteer/issues/1872
     * @depends registerOwner
     */
    /*public function downloadAgreement(AcceptanceTester $I) {
        $auth = new Authentication($I);
        $auth->logout();
        $auth->open();
        $auth->login($this->email, self::$PASS);
        $auth->assertLoginPassed();
        $ownerProfile = new OwnerProfile($I);
        $ownerProfile->downloadAgreement();
    }*/

    /**
     * @depends registerOwner
     */
    public function acceptAgreement(AcceptanceTester $I) {
        $I->switchToBackend();
        BackendAuthentication::open($I)->login('admin@mailmail.ru', '123123');

        $agreementManagement = new AgreementManagement($I);
        $agreementManagement->open();
        $state = $agreementManagement->getState($this->agreementId);
        $I->assertEquals('Не действует', $state);

        $agreementManagement->sign($this->agreementId);
        $state = $agreementManagement->getState($this->agreementId);
        $I->assertEquals('На подписании', $state);

        $agreementManagement->activate($this->agreementId);
        $state = $agreementManagement->getState($this->agreementId);
        $I->assertEquals('Действует', $state);

        $I->switchToFrontend();
        $ownerProfile = new OwnerProfile($I);
        $ownerProfile->open();
        $I->dontSee('Для работы необходимо заключить договор');

        $I->setContextParam('ownerEmail', $this->ownerEmail);
        $I->setContextParam('ownerPass', self::$PASS);
    }
}