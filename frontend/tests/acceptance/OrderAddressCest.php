<?php
namespace frontend\acceptance;

use frontend\Page\OrderForm;
use frontend\Page\OrderInvoiceList;
use frontend\Page\CustomerOrderList;
use frontend\Page\PaymentConfirmationList;
use frontend\Page\SearchTabs;
use frontend\Page\Authentication;
use frontend\AcceptanceTester;
use common\tests\helpers\MailHandler;

class OrderAddressCest
{
    private $mailHandler;

    /**
     * @param AcceptanceTester $I
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \yii\base\ErrorException
     */
    function _before(AcceptanceTester $I)
    {
        $this->mailHandler = new MailHandler($I->getMailCatcherUrl());
        $this->mailHandler->deleteAllMails();
    }

    /**
     * @param AcceptanceTester $I
     * @depends frontend\acceptance\CreateAddressCest:createAddress
     * @depends frontend\acceptance\ChangeProfileCest:addPersonEntity
     * @throws \Exception
     */
    public function orderAddressFromSearchResults(AcceptanceTester $I)
    {
        $customerEmail = $I->getContextParam('customerEmail');
        $customerPass = $I->getContextParam('customerPass');
        $addressMetro = $I->getContextParam('addressMetro');
        $customerEntity = $I->getContextParam('customerEntity');
        $I->expect('Открыт поиск адресов');
        $searchResults = SearchTabs::open($I)->clickTab('metro')->isTabActive('metro')
            ->selectMetro($addressMetro)->find()->assertResultsFound();

        $orderForm = $searchResults->orderAddress(1);
        $auth = new Authentication($I);
        $auth->login($customerEmail, $customerPass);
        $auth->assertLoginPassed();
        $I->expect('После авторизации пользователю показали форму заказа');
        $timeFrame = '6 месяцев';
        $deliveryAddress = 'Лесная, 5';
        $deliveryContact = '555-55-55';
        $orderForm->setLegalEntity($customerEntity)
            ->setTimeFrame($timeFrame)
            ->grabAvailableServices()
            ->setRandomServices()
            ->saveBillingDetails();
        $orderForm->setDeliveryType('доставка до метро')
            ->setDeliveryAddress($deliveryAddress)
            ->setDeliveryContacts($deliveryContact)
            ->saveDeliveryDetails()
            ->assertAddressSameAsOrdered()
            ->assertPriceSameAsOrdered()
            ->confirmOrder();
        $I->assertEquals($timeFrame, $orderForm->getTimeFrame());
        $I->assertTrue($orderForm->getGuaranty());
        $I->assertEquals($deliveryAddress, $orderForm->getDeliveryAddress());
        $I->assertContains('метро', $orderForm->getDeliveryType());
        $I->assertEquals($deliveryContact, $orderForm->getDeliveryContact());

        $I->expect('Заказчик получил подтверждение на email');
        $message = $this->mailHandler->getMailTo($customerEmail);
        $I->assertContains('выставлен счет', $message);
        $I->assertContains($orderForm->getOrderNm(), $message);

        $I->expect('В списке счетов за заказ есть счет за услуги владельца');
        $orderInvoices = new OrderInvoiceList($I, $orderForm->getOrderNm());
        $orderInvoices->assertOwnerServiceInvoiceShows();
        $ownerInvoiceIds = $orderInvoices->getOwnerInvoiceIds();
        $portalInvoiceIds = $orderInvoices->getPortalInvoiceIds();

        $I->expect('Новый заказ появился в Списке заказов');
        CustomerOrderList::open($I)->assertOrderShows($orderForm->getOrderNm());

        $I->expect('После того, как продавец подтвердил оплату, статус заказа стал Оплачен');
        $auth->logout();
        $ownerEmail = $I->getContextParam('ownerEmail');
        $ownerPass = $I->getContextParam('ownerPass');
        $auth = Authentication::open($I)->login($ownerEmail, $ownerPass)->assertLoginPassed();
        $paymentConfirmations = PaymentConfirmationList::open($I);
        $paymentDate = date('d.m.Y');
        foreach ($ownerInvoiceIds as $invoiceId) {
            $paymentConfirmations->setPaymentDate($invoiceId, $paymentDate);
            $paymentConfirmations->setPaymentReceipt($invoiceId, rand(1, 100));
        }
        $paymentConfirmations->save()->assertOrderNotShown($orderForm->getOrderNm());
        $auth->logout();
        Authentication::open($I)->login($customerEmail, $customerPass)->assertLoginPassed();
        $orderStatus = CustomerOrderList::open($I)->grabOrderStatus($orderForm->getOrderId());
        $I->assertEquals('Частично оплачен', $orderStatus);
    }
}
