<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "adr_metro".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Address[] $addresses
 */
class Metro extends \common\models\Metro
{

    /**
     * @param string $value
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function searchByPhrase(string $value)
    {
        return self::find()
            ->select(['id', 'name'])
            ->where("name LIKE :v", ['v' => $value . '%'])
            ->limit(10)
            ->all();
    }

    /**
     * @param string $name
     * @return int
     */
    public static function getIdByName(string $name)
    {
        $item = self::findOne(['name' => $name]);
        return $item->id;
    }

}