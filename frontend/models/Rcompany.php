<?php
namespace frontend\models;

use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;


class Rcompany extends \common\models\Rcompany
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['tax_system_vat'], 'string', 'max' => 1],
            [['account_owner', 'bank_cor', 'bank_name', 'account', 'bank_bik', 'dogovor_osnovanie', 'dogovor_face',
             'dogovor_signature', 'address', 'ogrn', 'kpp', 'inn'], 'required'],
        ]);
    }
}
