<?php

namespace frontend\models;


use common\models\Order;
use common\models\User;
use common\models\EntityHelper;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;



class OrderSearch extends Model
{
			
	public $number;	
	public $status_id;	
	public $address_id;		
	public $date_start;	
	public $date_fin;	
	public $customer_id;	
	
	
	
	public function getIsEmpty() {
		return !((boolean)$this -> address_id || (boolean)$this -> status_id  || (boolean)$this -> number  || (boolean)$this -> date_start  || (boolean)$this -> date_fin);
	}

    public function rules()
    {
        return [
            [['date_start', 'date_fin'], 'safe'],
			[['status_id'], 'integer'],				
            ['address_id', 'string', 'length' => [3]],
            ['number', 'string', 'length' => [3]],            
        ];
    }



	
    public function search($params)
    {

		
	
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => 9,
			],
            'sort'=>[
                'defaultOrder'=>[
                    'id'=>SORT_DESC
                ]
            ]
        ]);
		
		
        $this->load($params);
		
		
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		
		
        /* фильтры по значению, по списку для ссылки*/
        if ((boolean)$this->number) $query->andWhere(['like', 'number', $this->number]);
		
		
		/* Фильтрв по диапазону дат */
		
		
				
		if ($this -> date_start && $this -> date_fin) {
			$date_start = EntityHelper::datetToSql($this -> date_start);
			$date_fin = EntityHelper::datetToSql($this -> date_fin, 1);
//			var_dump($date_start); echo "<br>";
//			var_dump($date_fin); echo "<br>";
			$query->andFilterWhere(['between', 'created_at', $date_start, $date_fin]);
		} else {
		}
		
		
		

        /* фильтры по like для ссылки*/
        if ($this -> customer_id) {
            $concat = 'CONCAT('.User::getCaptionTemplateAsStringList().')';

            $list = User::find() -> andWhere(['like', $concat , $this->customer_id])->asArray()->all();
			
            $list = ArrayHelper::getColumn($list, 'id');
            if (count ($list) > 0) {
                $list = implode(',',$list);
                $query->andWhere("customer_id in ($list)");
            } else { // Условие не выполнено
                $query->andWhere('false');
                return $dataProvider;
            }
        }

					
				
		/* фильтры по like для ссылки*/
		if ($this -> address_id) {			
			$concat = 'CONCAT('.Address::getCaptionTemplateAsStringList().')';

			$list = Address::find() -> andWhere(['like', $concat , $this->address_id])->asArray()->all();
			$list = ArrayHelper::getColumn($list, 'id');			
			if (count ($list) > 0) {
				$list = implode(',',$list);			
				$query->andWhere("address_id in ($list)");			
			} else { // Условие не выполнено
				$query->andWhere('false');
				return $dataProvider;
			}
		}
				

        /* фильтры по значению, по списку для ссылки*/
        $query->andFilterWhere(['status_id' => (boolean)$this -> status_id?(int)$this -> status_id:""]);
		
		$query->andFilterWhere(['customer_id' => (boolean)$this -> customer_id?(int)$this -> customer_id:""]);

		
//		$query->where(['not', ['paid_at' => null]]);
		

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;  echo '<br>';
        return $dataProvider;
    }

	

}
