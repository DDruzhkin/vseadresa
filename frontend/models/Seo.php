<?php
namespace frontend\models;


use yii;



/**
 * This is the model class for table "adr_seo".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $keywords
 *
 * @property Address[] $addresses
 * @property Metro[] $metros
 * @property Nalog[] $nalogs
 * @property Okrug[] $okrugs
 * @property Option[] $options
 * @property Page[] $pages
 * @property Topic[] $topics
 */
class Seo extends \common\models\Seo
{
	
}