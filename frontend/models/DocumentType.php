<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "adr_document_types".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Document[] $documents
 */
class DocumentType extends \common\models\DocumentType
{

}
