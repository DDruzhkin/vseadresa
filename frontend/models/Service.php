<?php

namespace frontend\models;


class Service extends \common\models\Service
{

    const STYPE_SINGLE = 'Разовая';
    const STYPE_MONTHLY = 'Ежемесячная';

    public static $stypes = [
        self::STYPE_SINGLE,
        self::STYPE_MONTHLY,
    ];


    /**
     * @param int $serviceId
     * @param int $userId
     * @throws \Exception
     */
    public static function deleteService(int $serviceId, int $userId = 0)
    {
        if (empty($userId)) $userId = User::getUserId();

        $service = self::findOne($serviceId);
        if ($service->owner_id != $userId) {
            throw new \Exception('Вы не имеете право удалить эту услугу');
        }

        $service->delete();
    }

    /**
     * @param int $serviceId
     * @param array $params
     */
    public static function updateService(int $serviceId, array $params)
    {
        $service = self::findOne($serviceId);
        if (empty($service)) return;

        $params = $params['Service'];

        // TODO добавить название услуги
        $service->stype = $params['stype'] == self::STYPE_SINGLE
            ? self::STYPE_SINGLE : self::STYPE_MONTHLY;
        $service->price = $params['price'];
        $service->save();
    }

    public static function createService($params)
    {

    }

}