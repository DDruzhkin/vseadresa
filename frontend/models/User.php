<?php
namespace frontend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends \common\models\User
{
    const ADMIN_ROLE = 'Администратор';
    const OWNER_ROLE = 'Владелец';
    const CUSTOMER_ROLE = 'Заказчик';
    const GUEST_ROLE = 'Гость';

/**
	 * @var array EAuth attributes
	 */
	public $profile;

	public function checkRole($role) {
		return in_array($role, explode(',', $this -> role));		
	}
	
	
	public static function findIdentity($id) {
		if (Yii::$app->getSession()->has('user-'.$id)) {
			return new self(Yii::$app->getSession()->get('user-'.$id));
		}
		else {
			return isset(self::$users[$id]) ? new self(self::$users[$id]) : null;
		}
	}

	
	
	/**
	 * @param \nodge\eauth\ServiceBase $service
	 * @return User
	 * @throws ErrorException
	 */
	public static function findByEAuth($service) {
		if (!$service->getIsAuthenticated()) {
			throw new ErrorException('EAuth user should be authenticated before creating identity.');
		}

		$id = $service->getServiceName().'-'.$service->getId();
		$attributes = [
			'id' => $id,
			'username' => $service->getAttribute('name'),
			'authKey' => md5($id),
			'profile' => $service->getAttributes(),
		];
		$attributes['profile']['service'] = $service->getServiceName();
		Yii::$app->getSession()->set('user-'.$id, $attributes);
		return new self($attributes);
	}

    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }


        $user = new User();
        $user->username = $this->username;

        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->role_customer = 1;
        $user->save();

        return $user->save() ? $user : null;
    }


    /**
     * @param int $userId
     * @return null|static
     */
    public static function getById(int $userId = 0)
    {
        if (empty($userId)) $userId = self::getUserId();

        return self::findOne($userId);
    }

    /**
     * @param int $userId
     * @return string
     */
    public static function getRole(int $userId = 0)
    {
        $user = self::getById($userId);
        if (empty($user)) return self::GUEST_ROLE;

        $role = $user['role'];

        if (strpos($role, self::ADMIN_ROLE) !== false)
            return self::ADMIN_ROLE;
        elseif (strpos($role, self::OWNER_ROLE) !== false)
            return self::OWNER_ROLE;
        elseif (strpos($role, self::CUSTOMER_ROLE) !== false)
            return self::CUSTOMER_ROLE;

        return self::GUEST_ROLE;
    }

    /**
     * @param int $userId
     * @return bool
     */
    public static function isAdmin(int $userId = 0)
    {
        return self::getRole($userId) == self::ADMIN_ROLE;
    }

    /**
     * @param int $userId
     * @return bool
     */
    public static function isOwner(int $userId = 0)
    {
        return self::getRole($userId) == self::OWNER_ROLE;
    }

    /**
     * @param int $userId
     * @return bool
     */
    public static function isCustomer(int $userId = 0)
    {
        return self::getRole($userId) == self::CUSTOMER_ROLE;
    }


    public static function getAllCustomers()
    {
        $ownerId = self::getUserId();
        return self::find()
            ->where("role LIKE '%" . self::CUSTOMER_ROLE . "%'")
            ->where("id IN (SELECT customer_id FROM {{%orders}}
                WHERE address_id IN (SELECT id FROM {{%addresses}} 
                    WHERE owner_id = $ownerId))")
            ->all();
    }

    /**
     * @return int
     */
    public static function getUserId()
    {
        $user = Yii::$app->user;
        return $user->isGuest ? 0 : $user->id;
    }

}
