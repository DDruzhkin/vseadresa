<?php
/*
*	Модель рассчета статистических показателей портала
*/

namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\User;
use frontend\models\Address;
use frontend\models\AddressSearch;
use frontend\models\DocumentType;
use frontend\models\Document;

class Stat extends Model{

	public $by_month = true;
	public $by_days;
	public $monthFrom;
	public $monthTo;
	public $dayTo;
	public $dayFrom;
	public $entity;

	public static function getValue($name, $filterParam = null) {
		$result = false;
		$cache = Yii::$app->cache;			
		$key = 'stat.'.$name;
		// if ($cache) $result = $cache->get($key);  //временно
		if($result === false) {

			$result = self::calculate($name, $filterParam);
			if ($cache) $cache->set($key, $result, $cache -> statDuration);	
		}
		
		return $result;
	}
	
	
	public static function calculate($name, $filterParam = null) {
		$result = 0;
		
		switch ($name) {
			case 'address.count': {
				$statusId = Address::status('published') -> id;				
				$result = Address::find(['status_id' => $statusId]) -> count(); // Число адресов в статусе "Опубликован"
				break;
			}
			
			case 'owner.count': {
				$statusId = User::status('blocked') -> id;				
				$result = User::find()
				-> where(['not', ['status_id' => $statusId]])
				->andWhere('FIND_IN_SET(:role,role)>0',[':role' => User::ROLE_OWNER])
				-> count(); // Число неблокированных  Владельцев
				break;
			}
			
			case 'contract.count': {
				$docTypeId = DocumentType::byAlias('contract') -> id;				
				$result = Address::find(['doc_type_id' => $docTypeId]) -> count(); // Число документов с топом contract
				break;
			}
            case 'address.stat': {
                //pre($filterParam);
                if($filterParam['by_month']==1){
                    //die("ss");
                    $result = (new \yii\db\Query())->select(['adr_addresses.id', 'address', 'count(*) as orders_number'])->from('adr_orders')->leftJoin('adr_addresses', 'adr_orders.address_id=adr_addresses.id')->where(['adr_addresses.owner_id'=>Yii::$app->user->identity->id])->groupBy('adr_orders.address_id');
                    //pred($result->all());

                    //$searchModel -> load(Yii::$app->request->post());
                    //$dataProvider = $searchModel->search();

                }

            }
						
		}
		
		
		return $result;
	}

    public function getIsEmpty() {
        //return !((boolean)$this -> address_id || (boolean)$this -> status_id  || (boolean)$this -> number  || (boolean)$this -> date_start  || (boolean)$this -> date_fin);
        return true;
    }
	
	
}