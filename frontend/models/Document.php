<?php

namespace frontend\models;

use Yii;
use yii\web\View;


class Document extends \common\models\Document
{

    /**
     * @param int $documentId
     * @return mixed
     */
    public static function getPdfDocument(int $documentId)
    {
        $document = self::findOne($documentId);
        $view = new View();

        $mpdf = Yii::$app->pdf->api;
        $mpdf->WriteHtml($view->render('/documents/sb-bank.php'));
        return $mpdf->Output();
    }
}