<?php

namespace frontend\models;

use Yii;
use yii\validators\EmailValidator;

/**
 * Class NotificationsVsStatusesSettings
 * @package backend\models
 *
 * This is the model class for table "adr_notifications_vs_statuses_settings".
 *
 */
class NotificationsVsStatusesSettings extends \common\models\NotificationsVsStatusesSettings
{
    public static $exceptionStatusNames = [
        'Новый',
        'Черновик',
    ];

    /** Получаем все настройки уведомлений при изменении статуса.
     *  Если какой-то настройки нет - добавляем
     *
     * @param int $userId
     * @return array
     * @throws \yii\db\Exception
     */
    public static function getSettingsByUserId(int $userId = 0)
    {
        if (empty($userId)) $userId = Yii::$app->user->id;

        $settings = self::findAll(['user_id' => $userId]);
        $status_ids = array_column($settings, 'status_id');
        $statuses = Status::getOrderStatuses();
        foreach ($statuses as $status) {
            if (
                in_array($status['name'], self::$exceptionStatusNames) ||
                in_array($status['id'], $status_ids)
            )
                continue;

            $self = new self();
            $self->user_id = $userId;
            $self->status_id = $status['id'];
            $self->save();
        }

        $table = self::tableName();
        return Yii::$app->db->createCommand("SELECT *,
            (SELECT name FROM adr_status WHERE id = $table.status_id) name
            FROM $table
            WHERE user_id = :user_id", ['user_id' => $userId])->queryAll();
    }

    /** Сохранение настроек уведомлений пользователя
     *
     * @param array $params
     * @param int $userId
     * @throws \Exception
     */
    public static function saveSettings(array $params, int $userId = 0)
    {
        $emailValidator = new EmailValidator();
        $defaultEmail = (string)$params['defaultEmail'];
        if (!empty($defaultEmail) && !$emailValidator->validate($defaultEmail)) {
            throw new \Exception('Неверный адрес электронной почты');
        }

        if (empty($userId)) $userId = Yii::$app->user->id;

        $emptyDefaultEnabled = empty($params['defaultEnabled']);

        $user = User::findOne($userId);
        $user->notification_email = $defaultEmail;
        $user->notification_enabled = $emptyDefaultEnabled ? 'no' : 'yes';
        $user->save();

        $settings = self::findAll(['user_id' => $userId]);
        foreach ($settings as $item) {
            $statusId = $item->status_id;
            $email = $params['email'][$statusId];
            if (!empty($email) && !$emailValidator->validate($email)) {
                throw new \Exception('Неверный адрес электронной почты');
            }
            $item->email = $email;
            $item->enabled = empty($params['enabled'][$statusId]) ||
                $emptyDefaultEnabled? 'no' : 'yes';
            $item->save();
        }
    }


}