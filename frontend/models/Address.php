<?php

namespace frontend\models;

use common\models\Entity;
use common\models\EntityHelper;
use common\models\PhotoCalendar;
use frontend\models\User;
use Yii;
use DateInterval;
use DatePeriod;
use DateTime;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "adr_addresses".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $address
 * @property string $adr_index
 * @property integer $demo
 * @property string $status
 * @property string $parameters
 * @property integer $okrug
 * @property integer $nalogovaya
 * @property integer $metro
 * @property integer $region
 * @property integer $square
 * @property double $rating
 * @property double $gps_long
 * @property double $gps_lat
 * @property integer $price_type
 * @property integer $order_count
 * @property string $info
 *
 * @property Metro $metro0
 * @property Nalogs $nalogovaya0
 * @property User $owner
 * @property Okrug $okrug0
 * @property Region $region0
 * @property Orders[] $orders
 * @property PhotoVisits[] $photoVisits
 * @property ServicePrices[] $servicePrices
 * @property Services[] $services
 * @property SimilarAddresses[] $similarAddresses
 * @property SimilarAddresses[] $similarAddresses0
 */
class Address extends \common\models\Address
{
    public $dateInterval;

    public function rules()
    {
        return array_merge(parent::rules(), [
			[['photographerInterval'], 'integer'],
            [['serviceIdList', 'dateInterval', 'photographerDate'], 'safe']
        ]);
    }

	
/**
 *	Перемешивает приоритеты у адресов
 *	Запускается из консольного приложения
 */
	public static function mixPriorities() {
		
		echo __method__."()\n\r";		
		Yii::$app->db->createCommand('update '.self::tableName().' set priority = RAND()*9999')->execute();
		self::recalcPriorities();
	}


/**
 *	Пересчитывает общий приоритет адреса с учетом приоритета владельца
 */
	public static function recalcPriorities() {
//		echo __method__."()\n\r";		
		$addressTable = self::tableName();
		$userTable = User::tableName();
		$sql = "
		update $addressTable
		set cpriority = 
		(
			select $userTable.priority 
			from $userTable
			where owner_id = $userTable.id
		) + priority";
		
		//echo $sql; exit;
		Yii::$app->db->createCommand($sql)->execute();

	}
	
	public static function subscribing() {	
	
		$list = Address::find() -> andFilterWhere(['<', 'subscribe_to', EntityHelper::datetToSql(Entity::dbNow())]) -> all();
		foreach($list as $address) {
			echo "[".$address -> address."] ".$address -> address."\n";
			$address -> publicate(true);
		}

	}
	
	
	
}
