<?php
namespace frontend\models;

use yii;
use yii\base\Model;
use frontend\models\User;
use common\models\Rcompany;
use common\models\Rperson;
use common\models\Rip;
use app\components\StatusValidator;

/**
 * Signup form
 */
class SignupFormAdmin extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $phone;
    public $form;
    public $role;
    public $fullname;
    public $full_name;
    public $offerta_confirm;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'email'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 5, 'max' => 45],

            ['email', 'trim'],
           // ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 45],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],            

            ['fullname', 'trim'],
            ['fullname', 'required'],
            
            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['password_repeat', 'required'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        	
        $user = new User();
        $user->username = $this -> username;       
		$user->email = $this -> email;
		$user->fullname = $this -> fullname;
		$user->role = User::ROLE_ADMIN;

        $user->setPassword($this->password);
        $user->generateAuthKey();
	    
	    $user->save();	 
		return	$user;	
    }


}
