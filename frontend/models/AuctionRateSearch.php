<?php
/* генерируемый файл, редактировать его не надо */


namespace frontend\models;


use common\models\EntityHelper;

use frontend\models\Address;
use frontend\models\AuctionRate;
use frontend\models\User;

use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * AuctionRateSearch represents the model behind the search form about `\backend\models\AuctionRate`.
 */
class AuctionRateSearch extends Model {
	
	
	public $created_at;	
	public $rate_date_start;	
	public $rate_date_fin;	
	public $auction_date;	
	public $auction_date_start;	
	public $auction_date_fin;		
	public $address_id;	
	public $owner_id;	
	public $amount;	
	
	
	
	    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'auction_date_start', 'auction_date_fin', 'rate_date_start', 'rate_date_fin'], 'safe'],
            [['amount'], 'number'],
            ['address_id', 'string', 'length' => [3]],
            ['owner_id', 'string', 'length' => [3]],
        ];
    }
	
	
	public function getIsEmpty() {
		return !(
			(boolean)$this -> created_at || 
			(boolean)$this -> rate_date_start ||
			(boolean)$this -> rate_date_fin ||
			(boolean)$this -> auction_date || 
			(boolean)$this -> auction_date_start ||
			(boolean)$this -> auction_date_fin ||
			(boolean)$this -> address_id  || 
			(boolean)$this -> owner_id  || 
			(boolean)$this -> amount
		);
	}
		
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AuctionRate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
				'defaultOrder'=>[
					'created_at'=>SORT_DESC				
				]
			]
        ]);

		
		
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
		$query		
            ->leftJoin(Auction::tableName(), AuctionRate::tableName().'.auction_id = '.Auction::tableName().'.id');
		
						
		
		if ($this -> rate_date_start && $this -> rate_date_fin) {				
		
			$query->andWhere(['between', 'created_at', EntityHelper::datetToSql($this -> rate_date_start), EntityHelper::datetToSql($this -> rate_date_fin)]);
		}
		
		if ($this -> auction_date_start && $this -> auction_date_fin) {						
			$query->andWhere(['between', Auction::tableName().'.date', EntityHelper::datetToSql($this -> auction_date_start), EntityHelper::datetToSql($this -> auction_date_fin)]);
		}		
		
		if ($this -> amount) {	
			$query->andWhere([AuctionRate::tableName().'.amount' => $this -> amount]);
		}
						
			
		/* фильтры по like для ссылки*/
		if ($this -> address_id) {			
			$concat = 'CONCAT('.Address::getCaptionTemplateAsStringList().')';

			$list = Address::find() -> andWhere(['like', $concat , $this->address_id])->asArray()->all();
			$list = ArrayHelper::getColumn($list, 'id');			
			if (count ($list) > 0) {
				$list = implode(',',$list);			
				$query->andWhere("address_id in ($list)");			
			} else { // Условие не выполнено
				$query->andWhere('false');
				return $dataProvider;
			}
		}
				
				
					
				
				
						
			
		/* фильтры по like для ссылки*/
		if ($this -> owner_id) {			
			$concat = 'CONCAT('.User::getCaptionTemplateAsStringList().')';

			$list = User::find() -> andWhere(['like', $concat , $this->owner_id])->asArray()->all();
			$list = ArrayHelper::getColumn($list, 'id');			
			if (count ($list) > 0) {
				$list = implode(',',$list);			
				$query->andWhere("owner_id in ($list)");			
			} else { // Условие не выполнено
				$query->andWhere('false');
				return $dataProvider;
			}
		}
				

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
