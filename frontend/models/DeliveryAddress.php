<?php
namespace frontend\models;

use yii\db\ActiveRecord;
use yii\base\Model;

class DeliveryAddress extends Model
{
    public $index;
    public $district;
	public $region;
    public $destination;
    public $address;
    public $info;
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['index'], 'required'],
            [['index'], 'string', 'length' => 6],
			
            [['district'], 'required'],
            [['district'], 'string'],

            [['region'], 'string'],
						
            [['destination'], 'required'],
            [['destination'], 'string'],


            [['address'], 'required'],
            [['address'], 'string'],
            [['info'], 'string'],

        ]);
    }

	public function attributeLabels() {
		return [
			'index' => 'индекс',
			'district' => 'область / край',
			'destination' => 'нас. пункт',
			'region' => 'район',
			'address' => 'адрес',
			'info' => 'примечания',
		];
	}
	
	
}
?>