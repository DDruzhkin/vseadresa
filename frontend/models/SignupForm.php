<?php
namespace frontend\models;

use yii;
use yii\base\Model;
use frontend\models\User;
use common\models\Rcompany;
use common\models\Rperson;
use common\models\Rip;
use app\components\StatusValidator;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $phone;
    public $form;
    public $role;
    public $fullname;
    public $full_name;
    public $offerta_confirm;

    public static function attrLists() {
        return [
            'role' => ['Администратор' => 'Администратор', 'Заказчик' => 'Заказчик', 'Владелец' => 'Владелец', ],
            'form_mode' => [1 => 'Автоматический', 2 => 'Ручной', ],
            'form' => [1 => 'Физлицо', 2 => 'Юрлицо', 3 => 'ИП'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            //['username', 'email'],
            ['username', 'match', 'pattern' => '/^[a-zA-Zа-яА-Я_\d][-a-zA-Zа-яА-Я0-9_\.\d]*\@[a-zA-Zа-яА-Я\d][-a-zA-Zа-яА-Я\.\d]*\.[a-zA-Zа-яА-Я]{2,4}$/'],
            ['username', 'required', 'message'=>'Необходимо заполнить «Email».'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот E-mail уже занят.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
           // ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот E-mail уже занят.'],

            ['phone', 'trim'],
            ['phone', 'match', 'pattern' => '/\+7\([0-9]{3}\) [0-9]{3}-[0-9]{2}-[0-9]{2}/', 'message' => 'Телефон должен быть в формате +7(999) 999-99-99'],
            ['phone', 'required', 'message'=>'Необходимо заполнить «Телефон».'],

			
			
			
            ['fullname', 'trim'],
            ['fullname', 'required', 'message'=>'Необходимо заполнить «Фамилия, имя».'],
			

            ['full_name', 'required', 'when' => function ($model) {
                return $model->form == '2';
                }, 'whenClient' => "function (attribute, value) {
                return $('#signupform-form').val() == '2';
            }"],

			
			
			
			
            ['form', 'trim'],
            ['form', 'required'],
            //['offerta_confirm', 'in'],
            ['offerta_confirm', 'required', 'message'=>'Необходимо заполнить «Подтверждение договора оферты».'],

			
			/*['offerta_confirm', function ($attribute, $params)
			{				
				//if (!$this -> offerta_confirm) 
					$this->addError('offerta_confirm', $attribute. ' Необходимо подтвердить договор оферты');
			
			}, 'skipOnEmpty' => false],*/
			

            /*['offerta_confirm', 'boolean'],*/
            			

            ['password', 'required', 'message'=>'Необходимо заполнить «Пароль».'],
            ['password', 'string', 'min' => 6],

            ['password_repeat', 'required', 'message'=>'Необходимо заполнить «Подтверждение пароля».'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Пароли не совпадают" ],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
	 


	 
    public function signup()
    {
				
        if (!$this->validate()) {
            return null;
        }
        
	
        $user = new User();
        $user->username = $this->username;
        $user->role = $this->role;
        $user->fullname = $this->fullname;
        $user->offerta_confirm = $this->offerta_confirm;
        $user->phone = $this->phone;
        $user->form = $this->form;

        $user->email = $this->username;
        $user->setPassword($this->password);
        $user->generateAuthKey();
	    $user->role_customer = 1;

	    $user->save();
	    $new_id = $user->id;

	    //var_dump($user); die();
        if($this->form == '1'){ // подключаем модель для физ. лица
            $person = new Rperson();
            $person->user_id = $new_id;
            $person->form = SignupForm::attrLists()['form'][1];
            $person->save();
            $user->default_profile_id = $person->id;

        }
        if($this->form == '2'){ // подключаем модель для юр. лица
            $company = new Rcompany();
            $company->full_name = $this->full_name;
            $company->user_id = $new_id;
            $company->form = SignupForm::attrLists()['form'][2];
            $company->save();
            $user->default_profile_id = $company->id;
        }

        if($this->form == '3'){ // подключаем модель для юр. лица
            $rip = new Rip();

            $rip->user_id = $new_id;
            $rip->form = SignupForm::attrLists()['form'][3];
            $rip->save();
            $user->default_profile_id = $rip->id;
        }
        $user->save();

		
        //echo "\nerrors: ";print_r($user->getErrors()); exit;

        return $user->save() ? $user : null;
    }


}
