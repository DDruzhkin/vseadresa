<?php

namespace frontend\models;


class Payment extends \common\models\Payment
{
    const CASH_TYPE = 'Наличные';
    const CARD_TYPE = 'По карте';
    const ACCOUNT_TYPE = 'По счету';

    public static $types = [
        self::CASH_TYPE,
        self::CARD_TYPE,
        self::ACCOUNT_TYPE,
    ];


}