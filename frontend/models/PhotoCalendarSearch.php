<?php
/* генерируемый файл, редактировать его не надо */


namespace frontend\models;

use frontend\models\Address;
use frontend\models\PhotoCalendar;
use frontend\models\User;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PhotoCalendarSearch represents the model behind the search form about `\backend\models\PhotoCalendar`.
 */
class PhotoCalendarSearch extends Model {
	
	
	
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PhotoCalendar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'id'=>SORT_DESC				
			]
     ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		// Ограничение на показ - адрес должен находиться в статусах: {Ожидание оплаты, Частично оплачен, Ожидание подтверждения???} Ожидание подтверждения, Подготовка адреса
		$statusAliases = ['paymentwaiting', 'partpaid', 'confirmwating'];
		$aStatuses = [];
		foreach($statusAliases as $alias) {
			$id = Address::statusByAlias($alias) -> id;
			$aStatuses[] = $id;
		}
		
		// Ограничение на показ - пользователь должен находиться в статусе: Активный
		$statusAliases = ['active'];
		$uStatuses = [];
		foreach($statusAliases as $alias) {
			$id = User::statusByAlias($alias) -> id;
			$uStatuses[] = $id;
		}
	
		
		$pTable = PhotoCalendar::tableName();
		$aTable = Address::tableName();
		$uTable = User::tableName();
		
		$query -> innerJoin ($aTable, $aTable.'.id = '.$pTable.'.address_id'); //INNER JOIN adr_addresses adr ON adr.id = ph.address_id 
		$query -> innerJoin ($uTable, $uTable.'.id = '.$pTable.'.user_id'); //INNER JOIN adr_users us ON us.id = ph.user_id 
		$query -> andWhere([$aTable.'.enabled' => 1, $aTable.'.status_id' => $aStatuses]);
		$query -> andWhere([$uTable.'.status_id' => $uStatuses]); // AND us.status_id = 928
			$query -> andWhere([$pTable.'.enabled' => 1]);  
		
		
		
		
			

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql; exit;
	
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
