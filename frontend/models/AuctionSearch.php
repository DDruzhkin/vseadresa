<?php
/* генерируемый файл, редактировать его не надо */


namespace frontend\models;

use common\models\EntityHelper;
use backend\models\Auction;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AuctionSearch represents the model behind the search form about `\backend\models\Auction`.
 */
class AuctionSearch extends Model {
	
	
	public $address_id;	
	public $owner_id;	
	public $status_id;	
	public $auction_date_start;
	public $auction_date_fin;
	
	
	    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['auction_date_start', 'auction_date_fin'], 'safe'],
            [['address_id', 'owner_id', 'status_id'], 'integer'],
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

	public function getIsEmpty() {
		return !(
			(boolean)$this -> auction_date_start ||
			(boolean)$this -> auction_date_fin
		);
	}
	
	
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Auction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'id'=>SORT_DESC				
			]
     ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
		
		if ($this -> auction_date_start && $this -> auction_date_fin) {						
			$query->andWhere(['between', 'date', EntityHelper::datetToSql($this -> auction_date_start), EntityHelper::datetToSql($this -> auction_date_fin)]);
		}
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['address_id' => $this -> address_id > ""?(int)$this -> address_id:""]);
						
					
				
				
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['owner_id' => $this -> owner_id > ""?(int)$this -> owner_id:""]);
						
					
				
				
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['status_id' => $this -> status_id > ""?(int)$this -> status_id:""]);
						
					
				
				
			

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
