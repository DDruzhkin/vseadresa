<?php
namespace frontend\models;


/**
 * Class Status
 * @package frontend\models
 *
 */
class Status extends \common\models\Status
{

    /**
     * @return static[]
     */
    public static function getOrderStatuses()
    {
        return self::findAll(['table' => Order::tableName()]);
    }

    /**
     * @return static[]
     */
    public static function getAddressStatuses()
    {
        return self::findAll(['table' => Address::tableName()]);
    }

    /**
     * @return null|static
     */
    public static function getAddressNewStatus()
    {
        return self::findOne(['table' => Address::tableName(), 'alias' => 'new']);
    }

}
