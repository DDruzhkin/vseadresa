<?php
namespace frontend\models;





/**
 * This is the model class for table "adr_reviews".
 *
 * @property integer $id
 * @property string $date
 * @property string $fullname
 * @property string $email
 * @property string $company
 * @property integer $status_id
 * @property string $content
 * @property integer $user_id
 *
 * @property Status $status
 * @property User $user
 */

class Review extends \common\models\Review {

	   public function sendEmail($subjects, $template, $params, $from ,$to){ 
       return Yii::$app->mailer 
           ->compose([ 
               'text' => $template . '-text.php', 
               'html' => $template . '-html.php', 
           ], ['model' => $params]) 
           ->setFrom($from) 
           ->setTo($to) 
           ->setSubject($subjects) 
           ->send(); 
   } 

}