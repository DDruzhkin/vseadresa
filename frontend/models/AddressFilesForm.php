<?php
namespace frontend\models;
 
use yii\base\Model;
use yii\web\UploadedFile;
use Yii;
 
class AddressFilesForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $files;
 
    public function rules()
    {
        return [
            [['files'], 'file', 'skipOnEmpty' => false, 'maxFiles' => 10,'checkExtensionByMimeType'=>false],
        ];
    }
     
    public function upload()
    {
        if ($this->validate()) { 
            foreach ($this->imageFiles as $file) {
                $filename=Yii::$app->getSecurity()->generateRandomString(15);
                // echo $filename;
                $file->saveAs('uploads/' . $filename . '.' . $file->extension);
            }
            return true;
        } else {
            return false;
        }
    }
}