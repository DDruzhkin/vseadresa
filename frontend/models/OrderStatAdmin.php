<?php

namespace frontend\models;

use yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\data\ActiveDataProvider;

use common\models\Entity;
use common\models\EntityHelper;
use frontend\models\Order;


/**
 * Статистика по заказам для админки
 */
class OrderStatAdmin extends Model {
	
	const PERIOD_YEAR = 'year';
	const PERIOD_MONTH = 'month';
	const PERIOD_DAY = 'day';


	public $status_id;
//	public $owner_id;
//	public $customer_id;
	public $user_id;
	public $address_id;
	public $period = self::PERIOD_MONTH; 

	
	public function rules()
    {
        return [
            //[['status_id', 'owner_id', 'customer_id', 'address_id'], 'integer'],
			[['status_id', 'user_id', 'address_id'], 'integer'],
            [['period'], 'safe']            
        ];
    }
	
	public function search($params = false)
    {
		
		if ($params) $this->load($params);
		
		if (!$this -> status_id) $this -> status_id = Order::statusByAlias('paid') -> id;
		
        $query = (new \yii\db\Query());
		
		
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [ 
				'pageSize' => 12, 
			], 
			
        ]);
		
		
		$finish = Entity::dbNow();
		$start = date('d.m.Y', strtotime($finish.' -1'.$this -> period));			
		$start = EntityHelper::dateToSql($start);			
		$finish = EntityHelper::dateToSql($finish);
		$user = false;
		if ($this -> user_id) {
			$user = User::findOne($this -> user_id);
		}
		
		switch($this -> period) {
			case  self::PERIOD_YEAR: {
				$query
										
					-> select(['DATE_FORMAT(created_at, "%m/%Y") as "Месяц"', '(select name from adr_status where adr_status.id = status_id) as "Статус"', 'count(*) as "Количество"', ' replace(format(sum(summa), 0), ","," ") as "Сумма, руб."'])					
					-> groupBy(['MONTH(created_at)', 'status_id'])
					
					
			//		-> group by status_id, MONTH(created_at) 
					
				;
				break;
			};
			case  self::PERIOD_MONTH: {
				$query
					-> select(['DATE_FORMAT(created_at, "%d.%m.%Y") as "День"', '(select name from adr_status where adr_status.id = status_id) as "Статус"', 'count(*) as "Количество"', 'replace(format(sum(summa), 0), ","," ") as "Сумма, руб."'])
					-> groupBy(['TO_DAYS(created_at)', 'status_id'])
				;
				break;
			};
			case  self::PERIOD_DAY: {
				$query					
					-> select(['DATE_FORMAT(created_at, "%d.%m.%Y %H:00") as "Час"', '(select name from adr_status where adr_status.id = status_id) as "Статус"', 'count(*) as "Количество"', 'replace(format(sum(summa), 0), ","," ") as "Сумма, руб."'])
					-> groupBy(['HOUR(created_at)', 'status_id'])
				;
				break;
				
			};
			
		}

		
		$query-> from(Order::tableName());
		if (!is_null($query)) {
			$query -> where(['between', 'created_at', $start, $finish]);
			
			if ($this -> address_id) $query -> andWhere(['address_id' => $this -> address_id]);
			if ($user) {
				if ($user -> checkRole(User::ROLE_OWNER))$query -> andWhere(['owner_id' => $this -> user_id]);
				if ($user -> checkRole(User::ROLE_CUSTOMER))$query -> andWhere(['customer_id' => $this -> user_id]);
			}

		}

	echo "<br>";
		//echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql; exit;
		return $dataProvider;
	
	//	exit;
	}
	

	
}