<?php
namespace frontend\models;


use yii;
use yii\base\Model;
use frontend\models\User;
use app\components\StatusValidator;

/**
 * Signup form
 */
class OwnerSignup extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $phone;
    public $form;
    public $role;
    public $fullname;
    public $offerta_confirm;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'email'],
            ['username', 'required', 'message'=>'Необходимо заполнить «Email».'],
            ['username', 'match', 'pattern' => '/^[a-zA-Zа-яА-Я_\d][-a-zA-Zа-яА-Я0-9_\.\d]*\@[a-zA-Zа-яА-Я\d][-a-zA-Zа-яА-Я\.\d]*\.[a-zA-Zа-яА-Я]{2,4}$/'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот E-mail уже занят.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            // ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот E-mail уже занят.'],

            ['phone', 'trim'],
            ['phone', 'match', 'pattern' => '/\+7\([0-9]{3}\) [0-9]{3}-[0-9]{2}-[0-9]{2}/'],
            ['phone', 'required', 'message'=>'Необходимо заполнить «Телефон».'],

            ['fullname', 'trim'],
            ['fullname', 'required', 'message'=>'Необходимо заполнить «Фамилия, имя».'],

            /*['form', 'trim'],
            ['form', 'required'],*/


            ['password', 'required', "message"=>"Необходимо заполнить «Пароль»."],
            ['password', 'string', 'min' => 6],

            ['password_repeat', 'required', 'message'=>'Необходимо заполнить «Подтверждение пароля».'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Пароли не совпадают" ],
        ];
    }
    public static function attrLists() {
        return [
            'role' => ['Администратор' => 'Администратор', 'Заказчик' => 'Заказчик', 'Владелец' => 'Владелец', ],
            'form_mode' => [1 => 'Автоматический', 2 => 'Ручной', ],
            'form' => [1 => 'Физ. лицо', 2 => 'Юр. лицо', 3 => 'ИП'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }


        $user = new User();
        $user->username = $this->username;
        $user->role = $this->role;
        $user->fullname = $this->fullname;
        $user->phone = $this->phone;
        $user->form = $this->form;

        $user->email = $this->username;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->role_customer = 1;


        //Yii::info($user, 'entity');

        //var_dump($user -> parent() -> tablename()); exit;
        $user->save();

        //echo "\nerrors: ";print_r($user->getErrors()); exit;

        return $user->save() ? $user : null;
    }


}
