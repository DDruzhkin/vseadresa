<?php
namespace frontend\models;
use Yii;




/**
 * This is the model class for table "adr_service_prices".
 *
 * @property integer $id
 * @property integer $service_name_id
 * @property string $date
 * @property integer $status_id
 * @property integer $owner_id
 * @property integer $address_id
 * @property integer $type
 * @property string $price
 * @property string $price6
 * @property string $price11
 *
 * @property Document[] $documents
 * @property Address $address
 * @property User $owner
 * @property ServiceName $serviceName
 * @property Status $status
 */
class ServicePrice extends \common\models\ServicePrice
{



}