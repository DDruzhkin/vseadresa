<?php

namespace frontend\models;


use yii\data\ActiveDataProvider;

class ServiceSearch extends \common\models\ServiceSearch
{
    /**
     * @param array $params
     * @return \yii\data\ActiveDataProvider
     */
    public function search($params)
    {
        $dataProvider = parent::search($params);
        if (!empty($params['owner_id'])) {
            $dataProvider->query->where(['owner_id' => $params['owner_id']]);
        }

        return $dataProvider;
    }

    public static function searchByOrderId(int $orderId)
    {
        $query = Service::find()
            ->where(['order_id' => $orderId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        return $dataProvider;
    }

}