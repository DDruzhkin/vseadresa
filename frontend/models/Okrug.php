<?php

namespace frontend\models;



class Okrug extends \common\models\Okrug
{

    public static function getAllForSelect()
    {
        return array_column(self::find()->all(), 'name', 'id');
    }

}