<?php
namespace frontend\models;

use common\models\EntityHelper;
use common\models\NotificationManagerExt;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist', 'targetClass' => '\common\models\User', 'filter' => [],
                'message' => Yii::t('app','notUserWithEmail')
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app','Email'),
        ];
    }


    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::find()
            ->where(['email' => $this->email])
//            ->andWhere(['!=','status_id',User::status('blocked')->id])
            ->one();


        if (!$user) {
			throw new Exception('Данный пользователь заблокирован');
            //return false;
        }

        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {				
                return false;
            }
        }

        $subject = 'Восстановление пароля для ' . Yii::$app->name;

        //return  EntityHelper::sendEmail($subject, $user, $user->email, 'password_recovery');
		
		Yii::$app -> notifier -> notifyPasswordRecovery($user);
		return true;

    }
}
