<?php

namespace frontend\models;


use stdClass;
use Yii;
use yii\db\ActiveRecord;

use common\models\Entity;
use common\models\EntityHelper;
use common\models\NotificationManagerExt;
/**
 * This is the model class for table "adr_orders".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $number
 * @property integer $customer_id
 * @property integer $status_id
 * @property string $duration
 * @property integer $address_id
 * @property integer $delivery_type
 * @property string $delivery_address
 * @property string $delivery_contacts
 * @property integer $payment_type
 * @property double $price
 *
 * @property Document[] $documents
 * @property Address $address
 * @property Status $status
 * @property User $customer
 * @property Payment[] $payments
 */
class Order extends \common\models\Order
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['serviceIdList'], 'each', 'rule' => ['integer']],
        ]);
    }


/**
 *	Удаляет заказы в статусах new и draft которые были созданы более $days дней назад
 *	Запускается из консольного приложения
 */
	public static function clearDrafts($days = 3) {
		
		echo __method__."(".$days.")\n\r";
		
		$new_id = Order::statusByAlias('new') -> id;
		$draft_id = Order::statusByAlias('draft') -> id;
		$c = Order::deleteAll('TO_DAYS(NOW()) - TO_DAYS(created_at) > '.$days.' AND status_id in ('.$new_id.', '.$draft_id.')');
		
		
		return $c;
	}




/**
 *		Процедура отправляет уведомления по срочным заказам, которые находятся в стадии формирования более 3 часов.
 */

	

	public static function notifyFormation3Hour() {
		$orderTable = self::tableName();		
		$statusId = Order::statusByAlias('formation') -> id;
		$query = Order::find()
					-> where('status_changed_at between DATE_SUB(NOW(), INTERVAL 4 HOUR) AND DATE_SUB(NOW(), INTERVAL 3 HOUR)')
					-> andWhere(['status_id' => $statusId])
					-> andWhere(['is_quick' => 1])
					;
					
//echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
		$list = $query -> all();
		
		$params = [];
		foreach($list as $order) {
			$delta = (strtotime(Entity::dbNow()) - strtotime($order -> status_changed_at));			
			$m = (int)($delta/60);
			$h = (int)($m/60);
			$m = $m % 60;
			
			$params['hour'] = $h;
			$params['minute'] = $m;
			Yii::$app -> notifier -> notifyOrder(
				NotificationManagerExt::ORDER_FORMATION3, 
				$order, 
				NotificationManagerExt::ORDER_FORMATION3_SUBJECT, 
				$params
			);
			
			echo "№".$order -> number."\r\n";
		};

		return count($list);
	}
	

/**
 *				Процедура отправляет уведомления по заказам, которые находятся в стадии формирования более 24 часа.
 */
	
	
	public static function notifyFormation24Hour() {
		$orderTable = self::tableName();		
		$statusId = Order::statusByAlias('formation') -> id;
		$query = Order::find()
					-> where('status_changed_at between DATE_SUB(NOW(), INTERVAL 25 HOUR) AND DATE_SUB(NOW(), INTERVAL 24 HOUR)')
					-> andWhere(['status_id' => $statusId])
					;
					

		$list = $query -> all();

		$params = [];
		foreach($list as $order) {
			$delta = (strtotime(Entity::dbNow()) - strtotime($order -> status_changed_at));
			$m = (int)($delta/60);
			$h = (int)($m/60);
			$m = $m % 60;
			
			$params['hour'] = $h;
			$params['minute'] = $m;
			
			Yii::$app -> notifier -> notifyOrder(
				NotificationManagerExt::ORDER_FORMATION24, 
				$order, 
				NotificationManagerExt::ORDER_FORMATION24_SUBJECT, 
				$params
			);
			echo "№".$order -> number." => $h:$m\r\n";
		};
		
		return count($list);
		
	}
}