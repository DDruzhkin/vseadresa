<?php

namespace frontend\models;

use Yii;
use yii\data\ActiveDataProvider;

class StatusChange extends \common\models\StatusChange
{

    // получаем текущий статус адреса
    /**
     * @param int $addressId
     * @return bool|null|array
     */
    public static function getCurrentAddressStatus(int $addressId)
    {
        $statusTable = Status::tableName();
        $addressTable = Address::tableName();
        $change = self::find()
            ->where("object_id = $addressId")
            ->andWhere("status IN (SELECT id FROM $statusTable WHERE `table` = '$addressTable')")
            ->orderBy('date DESC')
            ->limit(1)
            ->one();
        if (empty($change)) return false;

        return Status::findOne($change->status)->toArray();
    }

    /** получаем все изменения статуса адреса
     *
     * @param int $addressId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllChangesByAddressId(int $addressId)
    {
        $statusTable = Status::tableName();
        $addressTable = Address::tableName();
        return self::find()
            ->where("object_id = $addressId")
            ->andWhere("status IN (SELECT id FROM $statusTable WHERE `table` = '$addressTable')")
            ->orderBy('date DESC')
            ->all();
    }

    /** Проверяем был ли адрес в статусе 'new'
     *
     * @param int $addressId
     * @return bool
     */
    public static function isWasAddressNewStatus(int $addressId)
    {
        if (Address::isNewAddress($addressId)) return false;

        $allChanges = self::getAllChangesByAddressId($addressId);
        $statusIds = array_column($allChanges, 'status');
        $newStatus = Status::getAddressNewStatus();
        if (empty($newStatus)) return false;

        return in_array($newStatus->id, $statusIds);
    }

    /** Меняем статус адреса
     *
     * @param int $addressId
     * @param int $statusId
     * @param int $userId
     * @throws \Exception
     */
    public static function changeAddressStatus(int $addressId, int $statusId, int $userId = 0)
    {
        $address = Address::findOne($addressId);
        if ($statusId == $address->id) {
            throw new \Exception('Новый статус совпадает с существующим');
        }
        if (empty($userId)) $userId = Yii::$app->user->id;

        $self = new self();
        $self->object_id = $addressId;
        $self->status = $statusId;
        $self->status_old = $address->status_id;
        $self->actor_id = $userId;
        $self->save();

        $address->status_id = $statusId;
        $address->save();
    }


    public static function searchByOrderId(int $orderId)
    {
        $query = self::find()
            ->where(['object_id' => $orderId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        return $dataProvider;
    }

}