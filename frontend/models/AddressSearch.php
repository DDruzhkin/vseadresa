<?php

namespace frontend\models;


use frontend\models\Address;
use frontend\models\Nalog;
use frontend\models\User;
use common\models\AddressOption;
use common\models\Entity;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * AddressSearch represents the model behind the search form about `\backend\models\Address`.
 */
class AddressSearch extends Model{
	
	
	
	public $id;	
	
	public $nalog_id;	
	public $nalog;
	public $metro_id;
	public $okrug_id;	
	public $status_id;
	public $options_id;	
	public $address;
	public $demo = true; // показать только те адреса, которые могут быть в каталоге
	
	public function getIsEmpty() {
		return !((boolean)$this -> nalog_id || (boolean)$this -> metro_id  || (boolean)$this -> okrug_id  || (boolean)$this -> options_id );
	}
	
	/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_id'], 'integer'],            
			[['options_id'],  'each', 'rule' => ['integer']],			
			[['metro_id', 'nalog_id', 'okrug_id'], 		 'safe'],			
			['address', 'string', 'length' => [3]],                  
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params = false, $demo = null)
    {
		
		
		$auctionAction = ((int)(date('h', strtotime(Entity::dbNow()))) < 14); // включется ранжирование по аукциону (до 14 часов)
		
		
		if (!is_null($demo)) $this -> demo = $demo;
		$query = Address::find();
//		var_dump(Address::getToCatalogCondition()); exit;
		
		$activeAuctionStatus = Auction::statusByAlias('active');
		
		$query -> select(['`adr_addresses`.*', 	
					(($auctionAction && $activeAuctionStatus)?'(select 
						sum(adr_auction_rates.amount)
					from 
						adr_auction_rates, adr_auctions
					where 
						adr_auction_rates.address_id = adr_addresses.id 
						AND adr_auction_rates.auction_id=adr_auctions.id 
						AND adr_auctions.status_id='.$activeAuctionStatus -> id.'
						AND TO_DAYS(adr_auctions.date) = TO_DAYS(NOW())) as auction_amount':'(0) as auction_amount')
				]);
		
		if ($this -> demo) {			
		
			$query -> where(Address::getToCatalogCondition());
		
		}
		
		
		$query -> andWhere(['enabled' => 1]);
				
        $query		
            ->leftJoin('adr_metro', Address::tableName().'.metro_id = '.Metro::tableName().'.id')
            ->leftJoin('adr_nalogs', Address::tableName().'.nalog_id = '.Nalog::tableName().'.id')
            ->leftJoin('adr_okrugs', Address::tableName().'.okrug_id = '.Okrug::tableName().'.id')
		;


		if ($this -> demo) {
			//$query -> orderby(['auction_amount'=>SORT_DESC, 'cpriority'=>SORT_DESC]); //TODO: решить проблему с сортировкой
		};
		
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
/*			
			'pagination' => [
				'pageSize' => 3,
			],
*/
			'sort'=>[
                'defaultOrder' => ['price11' => SORT_DESC],
			    'attributes' => [
                    'id',
			        'metro_view' => [
                        'asc'=>['adr_metro.name'=>SORT_ASC],
                        'desc'=>['adr_metro.name'=>SORT_DESC],
                    ],
                    'nalog_view' => [
                        'asc'=>['adr_nalogs.number'=>SORT_ASC],
                        'desc'=>['adr_nalogs.number'=>SORT_DESC],
                    ],
                    'okrug_view' => [
                        'asc'=>['adr_okrugs.name'=>SORT_ASC],
                        'desc'=>['adr_okrugs.name'=>SORT_DESC],
                    ],
                    'price_first_6',
                    'price_change_6',
                    'price_first_11',
                    'price_change_11',
                    'price6',
                    'price11'
				],
			 ]
			 
        ]);
		


        if ($params) $this->load($params);

		//	var_dump($this -> attributes); exit;

		if (is_string($this -> nalog_id))  $this -> nalog_id = (int)$this -> nalog_id;
		if (is_string($this -> okrug_id))  $this -> okrug_id = (int)$this -> okrug_id;
		
		
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
			echo "Ошибка валидации<br/>";				

			var_dump($this -> getErrors()); exit;
            return $dataProvider;
        }
		
		
		
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['id' => $this -> id > ""?(int)$this -> id:""]);
		
		
		if ((boolean)$this->address) $query->andWhere(['like', 'address', $this->address]);
		//1175, 2122, 2123
		$conditions = [];		
		if($this['options_id'] && count($this['options_id'])) {
			
			$condition = $this -> getMultiCondition('options_id', AddressOption::tableName().'.option_id');
			
			$query->innerJoin(
				"(select distinct address_id from `".AddressOption::tableName()."` where (`option_id` IN (".implode(',',$this['options_id'])."))) as opts", 
				'opts.address_id = '.Address::tableName().'.id');			
			
			//$query->rightJoin(AddressOption::tableName(), AddressOption::tableName().'.address_id = '.Address::tableName().'.id');			
			//$condition = $this -> getMultiCondition('options_id', AddressOption::tableName().'.option_id');
			//if ($condition) $conditions[] = $condition;		
		}
		
		

		$condition = $this -> getMultiCondition('okrug_id', Address::tableName().'.okrug_id');
		if ($condition) $conditions[] = $condition;		
		$condition = $this -> getMultiCondition('nalog_id', Address::tableName().'.nalog_id');		

		if ($condition) $conditions[] = $condition;		
		$condition = $this -> getMultiCondition('metro_id', Address::tableName().'.metro_id');		
		if ($condition) $conditions[] = $condition;		
		$condition = $this -> getMultiCondition('status_id', Address::tableName().'.status_id');		
		if ($condition) $conditions[] = $condition;		
				
		if (count($conditions)) $query->andWhere(array_merge(['and'], $conditions));		
			
//			echo '<br><br>sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;		exit;
        return $dataProvider;
    }
	
	protected function getMultiCondition($name, $fieldname = false) {
		
		$result = false;
		if (!$fieldname) $fieldname = $name;
			$param = $this[$name];
			if ($param) {
				if(is_array($param)) {
					$list = [];
					foreach($param as $val) {
						$list[] = (int)$val;
					}
					$condition = [$fieldname => $list];

				} else {
					$condition = $fieldname.'='.$param;
				}
				
				$result = $condition;
			}		
		return $result;
	}
		
}
