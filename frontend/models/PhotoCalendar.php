<?php

namespace frontend\models;

use yii;
use common\models\Entity;
use common\models\EntityHelper;

/**
 * This is the model class for table "adr_photo_calendar".
 *
 * @property integer $id
 * @property integer $address_id
 * @property integer $user_id
 * @property string $date
 * @property integer $interval
 * @property integer $enabled
 *
 * @property Address $address
 * @property User $user
 */
class PhotoCalendar extends \common\models\PhotoCalendar
{
	// Состояние слота - вычисляется
	const SLOT_IS_FREE = 0; // Слот сводобен и доступен для заказа
	const SLOT_IS_CLOSED = 1; // Нерабочий слот - заказ не доступен
	const SLOT_IS_BUSY = 2; // Слот занят - заказ не доступен
	const SLOT_IS_MY = 3; // Мой слот (для Владельца)
	
	// значения свойства слота enabled - в базе
	const ENABLED_FREE = 0;	//слот свободен.То есть подчиняется правилам выходных по умолчанию. Возможен заказ, если не выходной.
	const ENABLED_BUSY = 1;	//слот занят, заказ невозможен
	const ENABLED_OFF = 2;	//слот закрыт админом, то есть заказ невозможен, даже, если не выходной
	const ENABLED_ON = 3;	//слот открыт админом, то есть заказ возможен, даже, если выходной
	
	const SLOT_STATE_TITLES = [
		self::SLOT_IS_FREE => 'Время свободно. Доступно для заказа',
		self::SLOT_IS_CLOSED => 'Нерабочее время - заказ не доступен',
		self::SLOT_IS_BUSY => 'Время уже занято - заказ не доступен',		
		self::SLOT_IS_MY => 'Фотограф зарезервирован на мой адрес',		
		
	];
	
	const ADMIN_SLOT_STATE_TITLES = [
		self::SLOT_IS_FREE => 'Слот свободен, доступен для заказа - Нажмите, чтобы заблокировать.',
		self::SLOT_IS_CLOSED => 'Слот заблокирован, не доступен для заказа - Нажмите, чтобы разблокировать',
		self::SLOT_IS_BUSY => 'Слот занят - Изменить состояние невозможно',
	];
	
	
	
	public static function getIntervals() {
		return  [
				1 => '10:00-13:00',
				2 => '13:00-16:00',
				3 => '16:00-19:00',
			];
	}
	
	
	// Удалить резерв фотографа для адреса
	public static function deletByAddress($address_id) {
		self::deleteAll(['address_id' => $address_id]);
	}
	

    public function getInterval()
    {
		
		$intervals = self::getIntervals();
		
        return $intervals[(int)$this -> interval];
    }


	// Возвращает элемент по указаннй дате и номеру интервала, если он существует
	// ОБъект создается даже, если не найден элемент в базе, но не сохраняется;
	public static function byDate($date, $interval){
		$result = NULL;
		//return self::find(['date' => EntityHelper::dateToSql($date)]);
		
		$result = self::find() -> where(['DATE(date)' => EntityHelper::dateToSql($date)]);
		if ($interval !== false) {
			$result -> andWhere(['interval' => $interval]);
		}
		
		
		$result = $result -> one();	
		if (is_null($result)) {
			$result =  new PhotoCalendar();
			$result -> enabled = self::ENABLED_FREE;
			$result -> date = EntityHelper::dateToSql($date);
			$result -> interval = $interval;
			$result -> user_id = self::currentUser();
		}
		
		return $result;
	}
	
	public static function checkHoliday($date) {
		$d = date('d.m', strtotime($date));
		$holidais = ['01.01', '02.01', '03.01', '04.01', '05.01', '06.01', '07.01', '08.01', '12.02', '08.03', '01.05', '09.05', '12.06', '04.11'];
		return in_array($d, $holidais);
	}
	
	// Состояние слота - 
	public function getState() {
		$enabled = $this -> enabled; // 0 - слот свободен.То есть подчиняется правилам выходных по умолчанию. Возможен заказ, если не выходной.
									 // 1 - слот занят, заказ не возможен
									 // 2 - слот закрыт админом, то есть заказ невозможен, даже, если не выходной
									 // 3 - слот открыт админом, то есть заказ возможен, даже, если выходной
			
		$wd = date('N', strtotime($this -> date));
		
		
		$result = (($wd <= 5) && (!self::checkHoliday($this -> date)))? self::SLOT_IS_FREE:self::SLOT_IS_CLOSED; // Значение по умолчанию - суббота и воскресенье
		
		switch ($enabled) {
			case self::ENABLED_BUSY: {
				$result = self::SLOT_IS_BUSY;
				break;
			}
			case self::ENABLED_OFF: {
				$result = self::SLOT_IS_CLOSED;
				break;
			}
			case self::ENABLED_ON: {
				$result = self::SLOT_IS_FREE;
				break;
			}
		}
		
		return $result;
									 
	}
	
    public static function getPhotoCalendarDataSinceToday()
    {
        /* @var PhotoCalendar[] $calendars */
        $calendars = PhotoCalendar::find()->where(['>=', 'date', date('Y-m-d')])->all();
        $bookedDates = [];

        foreach ($calendars as $calendar) {
            $bookedDates[date('Y-m-d', strtotime($calendar->date)) . '_' . $calendar->interval] = intval($calendar->enabled);
        }

        return $bookedDates;
    }
	
	
	//Удаляет неоплаченные визиты, после выставления счета для которых прошло больше 7 дней
	// Возвращает число удаленных визитов
	public static function removeOldUnpaiedVisits() {
		$searchModel = new PhotoCalendarSearch();
		$dataProvider = $searchModel -> search([]);
		
		$list = $dataProvider -> query -> all();
		$count = 0;
		foreach($list as $item) {
			// Получим послений счет за фотографа
			if ($item -> address -> photo_order) {
				$invoice = $item -> address -> photo_order -> getDocuments() -> orderBy(['created_at' => 'desc']) -> one();
				//if ($invoice && ((strtotime(Entity::dbNow()) - strtotime($invoice -> created_at))/86400 > 7)) {
					if ($invoice && ((strtotime(Entity::dbNow()) - strtotime($invoice -> created_at))/86400 > 7)) {
					$item -> delete();
					$count++;
				}
			}
		};
		
		return $count;
		
	}	
	
	

}