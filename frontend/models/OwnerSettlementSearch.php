<?php
/* Ручной файл */
/*
Расчеты Владельца по адресам

*/

namespace frontend\models;

use yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

use common\models\Entity;
use common\models\EntityHelper;
use frontend\models\Payment;
use frontend\models\DocumentType;


class OwnerSettlementSearch extends Model {
		
	public $dateFrom;
	public $dateTo;
	public $orderNumber;
	
	public function getIsEmpty() {	
		return !(boolean)count(Yii::$app->request->get());
	}	
	
	public function rules()
    {
		
		return [		
			[['dateFrom', 'dateTo'], 'safe'],			
			[['orderNumber'],  'string'],			
		];
		
	}	
	
	
    public function search($params = false)
    {		
		
		if ($params) {
			$this -> load($params);

			if (!$this->validate()) {
				return $dataProvider;
			}
		}
		
	
		$userId = Entity::currentUser();		
		

		$statusIdList = array_keys(ArrayHelper::map(Status::find() -> where(['alias' => ['paid', 'partpaid'], 'table' => Order::tableName()]) -> all(), 'id', 'alias'));

		$query = (new \yii\db\Query()) 
			-> select ([
				Order::tableName().'.created_at  created_at' , 
				Order::tableName().'.number as number', 
				Order::tableName().'.id as order_id', 
				Payment::tableName().'.date as pay_date',
				Payment::tableName().'.value as pay_value',
				'payment_order_date',				
				'payment_order_number',				
			])
			-> from (Order::tableName())
			-> innerJoin(Document::tableName(), Document::tableName().'.order_id = '.Order::tableName().'.id')
			-> innerJoin(Payment::tableName(), Payment::tableName().'.invoice_id = '.Document::tableName().'.id')
			-> andWhere([Order::tableName().'.owner_id' => $userId])
			-> andWhere([Order::tableName().'.status_id' => $statusIdList])
			-> andWhere([Payment::tableName().'.confirmed' => 1])// только подтвержденные оплаты
			;
						
			if ($this -> orderNumber) $query -> andWhere(['like', Order::tableName().'.number', $this -> orderNumber]);
			if ($this -> dateFrom || $this -> dateTo) $query -> andWhere(['between', Order::tableName().'.created_at', EntityHelper::datetToSql($this -> dateFrom), EntityHelper::datetToSql($this -> dateTo, 1)]);
			
		$query->orderBy([
			'pay_date' => SORT_DESC,
		]);
			
		
/*		
		$rlist = array_keys($user -> requisiteList());
			
		$query = Payment::find() 
			-> andWhere(['recipient_id' => $rlist])
			-> andWhere(['not', ['invoice_id' => NULL]]);
			

*/



			
		//echo $query -> prepare(Yii::$app->db->queryBuilder)->createCommand()->sql; 
	

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
//				'date'=>SORT_DESC				
			
			],
			],
			'pagination' => [
				'pageSize' => 9,
			],
        ]);

		return $dataProvider;
	
	}
	
	

	
}