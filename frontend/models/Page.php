<?php

namespace frontend\models;


use yii;



/**
 * This is the model class for table "adr_pages".
 *
 * @property integer $id
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $h1
 * @property string $content
 * @property string $menu
 * @property string $seo_text
 * @property string $slug
 * @property string $template
 * @property integer $type
 * @property string $tag_str
 * @property integer $tag_int
 * @property integer $seo_id
 *
 * @property Seo $seo
 */
class Page extends \common\models\Page
{
}