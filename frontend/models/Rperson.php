<?php

namespace frontend\models;
use yii\helpers\ArrayHelper;
use Yii;


class Rperson extends \common\models\Rperson
{

    /* Метаданные атрибутов*/
	public static function attrMeta() {
        $parentAttrMeta = [];
        if (self::parentClassName()) {
            $parentAttrMeta = self::parentClassName()::attrMeta();
        };

        return ArrayHelper::merge(		
            $parentAttrMeta,
            [
                            'id' => ['show' => '1', 'filter' => 'value', 'label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'visible' => '1', ], 
        'created_at' => ['label' => 'Дата добавления', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'show' => '1', 'visible' => '1', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'readonly' => '1', 'required' => '0', ], 
        'form' => ['visible' => '1', 'label' => 'Правовой статус', 'filter' => 'list', 'readonly' => '1', 'default' => 'Физлицо', 'type' => 'enum', 'required' => '1', 'show' => '1', ], 
        'user_id' => ['label' => 'Пользователь', 'comment' => 'Пользователь, с которым связаны реквизиты этого физического лица', 'default' => 'currentUser()', 'filter' => 'like', 'link' => '1', 'model' => 'User', 'type' => 'ref', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
        'inn' => ['label' => 'ИНН', 'visible' => '1', 'filter' => 'like', 'mask' => '999999999999', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
        'fname' => ['show' => '1', 'visible' => '1', 'label' => 'Имя', 'filter' => 'like', 'type' => 'string', 'required' => '1', ], 
        'mname' => ['show' => '1', 'visible' => '1', 'label' => 'Отчество', 'filter' => 'like', 'type' => 'string', 'required' => '0', ], 
        'lname' => ['show' => '1', 'visible' => '1', 'label' => 'Фамилия', 'filter' => 'like', 'type' => 'string', 'required' => '1', ], 
        'bdate' => ['label' => 'Дата рождения', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'show' => '1', 'visible' => '0', 'required' => '0', ], 
        'pasport_number' => ['show' => '1', 'visible' => '1', 'label' => 'Номер/серия паспорта', 'filter' => 'like', 'mask' => '@pasport', 'type' => 'string', 'required' => '0', ], 
        'pasport_date' => ['show' => '1', 'visible' => '0', 'label' => 'Дата выдачи паспорта', 'type' => 'date', 'format' => 'd.m.Y', 'required' => '0', ], 
        'pasport_organ' => ['show' => '1', 'visible' => '1', 'label' => 'Орган, выдавший паспорт', 'type' => 'string', 'required' => '0', ], 
        'address' => ['show' => '1', 'visible' => '1', 'label' => 'Адрес регистрации', 'filter' => 'like', 'type' => 'string', 'required' => '0', ], 
        'phone' => ['show' => '1', 'visible' => '0', 'label' => 'Телефон', 'filter' => 'like', 'type' => 'string', 'required' => '0', ], 
        'tax_system_vat' => ['show' => '0', 'label' => 'Используетcя НДС', 'comment' => 'Используется система налогообложения, в которой используется НДС', 'type' => 'boolean', 'default' => 0, 'required' => '0', 'visible' => '0', ], 
        'eq_login' => ['show' => '1', 'visible' => '0', 'label' => 'Эквайринг логин', 'comment' => 'Логин для подключения к эквайрингу', 'type' => 'string', 'required' => '0', ], 
        'eq_password' => ['show' => '1', 'visible' => '0', 'label' => 'Пароль логин', 'comment' => 'Пароль для подключения к эквайрингу', 'type' => 'string', 'required' => '0', ], 
        'bank_bik' => ['show' => '1', 'visible' => '0', 'label' => 'БИК банка', 'filter' => 'like', 'mask' => '999999999', 'type' => 'string', 'required' => '0', ], 
        'bank_name' => ['show' => '1', 'visible' => '0', 'label' => 'Название банка', 'filter' => 'like', 'type' => 'string', 'required' => '1', ], 
        'bank_cor' => ['show' => '1', 'visible' => '0', 'label' => 'Кор.счет банка', 'filter' => 'like', 'mask' => '99999999999999999999', 'type' => 'string', 'required' => '1', ], 
        'account' => ['show' => '1', 'visible' => '0', 'label' => 'Номер счета', 'filter' => 'like', 'mask' => '@account', 'type' => 'string', 'required' => '0', ], 
        'account_owner' => ['show' => '1', 'visible' => '0', 'label' => 'Владелец счета', 'type' => 'string', 'required' => '0', ], 

                    
            ]
        );

        
        
    }
    public function rules()
        {
            return array_merge(parent::rules(), [
               
                [['bank_cor', 'bank_bik', 'bank_name', 'account_owner', 'account', 'address', 'pasport_organ', 'pasport_date', 'pasport_number', 'mname'], 'required'],
            ]);
        }
}
