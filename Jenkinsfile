String branch = '*/master'

node('ci-node') {
    stage('Build') {
        dir('portal') {
            checkout([$class: 'GitSCM', branches: [[name: branch]],
                      userRemoteConfigs: [[credentialsId: 'sh-bitbucket', url: 'https://bitbucket.org/steigenhaus1/portal.git']]])
        }
        sh '''
            cd $WORKSPACE/portal
            php init --env=Development --overwrite=All
            cp $WORKSPACE/portal/environments/ci/common/config/main-local.php $WORKSPACE/portal/common/config/
            export COMPOSER_HOME=$HOME/.config/composer
            /usr/local/bin/composer global require "fxp/composer-asset-plugin:~1.4.2"
            /usr/local/bin/composer install
        '''
    }
    stage('Init DB') {
        withCredentials([usernamePassword(credentialsId: 'reference-mysql', usernameVariable: 'MYSQL_USER', passwordVariable: 'MYSQL_PASSWORD')]) {
            sh '''
                mysqldump -h 138.201.155.142 -u $MYSQL_USER -p$MYSQL_PASSWORD --no-data portal > $WORKSPACE/schema.sql
                mysqldump -h 138.201.155.142 -u $MYSQL_USER -p$MYSQL_PASSWORD --no-create-info portal > $WORKSPACE/data_init.sql
            '''
        }
        withCredentials([usernamePassword(credentialsId: 'mysql-root', usernameVariable: 'MYSQL_USER', passwordVariable: 'MYSQL_PASSWORD')]) {
            sh '''
                mysql -h 127.0.0.1 -u $MYSQL_USER -p$MYSQL_PASSWORD < $WORKSPACE/portal/environments/ci/db/create_db_and_user.sql
                mysql -h 127.0.0.1 -u ci_portal -p@Mysql2go portal_ci < $WORKSPACE/schema.sql
                mysql -h 127.0.0.1 -u ci_portal -p@Mysql2go portal_ci < $WORKSPACE/data_init.sql
            '''
        }
    }
    stage('Deploy') {
        sh '''
            sudo rm -rf /var/www/portal3/*
            sudo cp -r $WORKSPACE/portal/* /var/www/portal3/
            sudo chown -R apache:apache /var/www/portal3/
        '''
    }
}

node('ui-test-node') {
    stage('UI test') {
        checkout([$class           : 'GitSCM', branches: [[name: branch]],
                  userRemoteConfigs: [[credentialsId: 'sh-bitbucket', url: 'https://bitbucket.org/steigenhaus1/portal.git']]])
        try {
            wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm']) {
                sh '''
                    rm -rf "$WORKSPACE/frontend/tests/_output/"
                    composer install
                    $WORKSPACE/vendor/bin/codecept run --html --env jenkins -o "modules: config: WebDriver: url: http://portal.steigenhaus.com/" -c frontend
                '''
            }
        } finally {
            def exitCode = sh script: 'find frontend/tests/_output/ -name *.png | egrep .', returnStatus: true
            if (exitCode == 0) {
                archiveArtifacts artifacts: 'frontend/tests/_output/*.png'
            }
            archiveArtifacts artifacts: 'frontend/tests/_output/report.html'
        }
    }
}
