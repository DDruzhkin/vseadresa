USE portal_ci;
SET FOREIGN_KEY_CHECKS=0;
SET NAMES utf8;
UNLOCK TABLES;

--
-- Table structure for table `_adr_address_parameters`
--

DROP TABLE IF EXISTS `_adr_address_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_adr_address_parameters` (
  `id` int(11) NOT NULL COMMENT 'show:false',
  `code` varchar(10) DEFAULT NULL COMMENT 'label:Код;filter:value;readonly:1;',
  `name` varchar(45) DEFAULT NULL COMMENT 'label:Название параметра;filter:like;readonly:1;',
  `partype` varchar(45) DEFAULT NULL COMMENT 'label:Тип параметра;filter:like;readonly:1;',
  `icon` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Title: Список параметров адреса;\r\nimp: Параметр адреса;\r\nrodp:Параметра адреса;\r\nvinp: Параметр адреса;\r\nModel:Parameter;\r\nparent:objects;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_address_option`
--

DROP TABLE IF EXISTS `adr_address_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_address_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) NOT NULL DEFAULT '0',
  `option_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_id` (`id`),
  KEY `FK_address` (`address_id`),
  KEY `FK_option` (`option_id`),
  CONSTRAINT `FK_address` FOREIGN KEY (`address_id`) REFERENCES `adr_addresses` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_option` FOREIGN KEY (`option_id`) REFERENCES `adr_options` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=856 DEFAULT CHARSET=utf8 COMMENT='Model: AddressOption;\r\ncomment: Связующая таблица адреса и опций;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_addresses`
--

DROP TABLE IF EXISTS `adr_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_addresses` (
  `id` int(11) NOT NULL COMMENT 'show:true;filter:value;',
  `gps` text COMMENT 'label:Координаты;visible:0;show:1;format:gps;default:55.751429,37.618879',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Дата добавления;filter:range;type:date;format:d.m.Y;show:1;visible:1;',
  `status_changed_at` timestamp NULL DEFAULT NULL COMMENT 'label:Дата изменения статуса;show:0;comment:Дата последнего изменения статуса. Выставляется автоматически;',
  `adr_index` varchar(6) DEFAULT NULL COMMENT 'label:Индекс;show:1;visible:0;mask:999999;comment:Почтовый индекс адреса - 6 цифр.',
  `address` text NOT NULL COMMENT 'label:Адрес;filter:like;',
  `status_id` int(11) DEFAULT NULL COMMENT 'label:Статус адреса;default:defaultStatus;comment:Текущий статус адреса;filter:list;',
  `owner_id` int(11) NOT NULL COMMENT 'label:Владелец; comment:Владелец адреса;default:currentUser();filter:like;link:1;',
  `nalog_id` int(11) DEFAULT NULL COMMENT 'label:№ налоговой;visible:false;comment: Номер налоговой инспекции;filter:list;link:1;comment: Налоговая, к которой относится адрес; visible:1;',
  `metro_id` int(11) DEFAULT NULL COMMENT 'label:Метро; format:autocomplete;visible:0;comment:станция метро;filter:list;link:1;',
  `region_id` int(11) DEFAULT NULL COMMENT 'label:Район;comment:Район Москвы;show:1;visible:0;filter:list;link:1;',
  `okrug_id` int(11) DEFAULT NULL COMMENT 'label:Округ;comment:Административный округ Москвы;filter:list;link:1;comment: Административный округ адреса;readonly:1;',
  `demo` tinyint(4) DEFAULT NULL COMMENT 'label: В каталоге;comment:Демонстрировать адрес в каталоге;visible:0;type:boolean;',
  `parameters` text COMMENT 'label:Параметры;show:false;comment:Дополнительные параметры адреса',
  `square` decimal(6,1) DEFAULT NULL COMMENT 'label:Площадь;visible:false;comment:Площадь помещения, включенного в аренду в кв.м;mask:@decimal;',
  `rating` double DEFAULT '0' COMMENT 'label:Рейтинг;show:0;',
  `price_type` tinyint(4) DEFAULT NULL COMMENT 'label:Тип цены;comment:Текущий (отображаемый) тип цены;show:0;visible:false;',
  `options` tinyint(1) DEFAULT NULL COMMENT 'label:Опции;show:1;visible:0;format:options;itemtemplate:%icon% - %name%;listitemtemplate:%icon%;type:manytomany;viatable:adr_address_option;format:checkboxlist;',
  `photo_date` timestamp NULL DEFAULT NULL COMMENT 'label:Дата визита фотографа;show:1;visible:0;type:datetime;',
  `photo_order_id` int(11) DEFAULT NULL COMMENT 'label:Заказ на визит фотографа;show:0;visible:0;',
  `quickable` tinyint(1) DEFAULT NULL COMMENT 'label:Возможно срочное оформление;show:1;visible:0;type:boolean;comment:Может ли быть обеспечено срочное формирвоание заказа для этого адреса;',
  `order_count` int(10) unsigned DEFAULT NULL COMMENT 'label:Число заказов;comment:Число заказов по адресу за последний месяц;readonly:1; show:0;',
  `info` text COMMENT 'label:Примечания;visible:false;',
  `price6` decimal(11,4) DEFAULT '0.0000' COMMENT 'label: Цена за 6 месяцев;comment:Цена адреса, 6 месяцев;visible:0;;format:[''decimal'', 2] ;mask:@money;',
  `price11` decimal(11,4) DEFAULT '0.0000' COMMENT 'label: Цена за 11 месяцев;comment:Цена адреса, 11 месяцев;visible:0;;format:[''decimal'', 2] ;mask:@money;',
  `price6min` decimal(11,4) NOT NULL DEFAULT '0.0000' COMMENT 'label: Мин.цена за 6 месяцев;comment:Минимальная цена адреса за 6 месяцев. Устанавливается администратором;visible:0;;format:[''decimal'', 2] ;mask:@money;',
  `price11min` decimal(11,4) NOT NULL DEFAULT '0.0000' COMMENT 'label: Мин.цена за 11 месяцев;comment:Минимальная цена адреса за 11 месяцев. Устанавливается администратором;visible:0;;format:[''decimal'', 2] ;mask:@money;',
  `price6max` decimal(11,4) NOT NULL DEFAULT '100000.0000' COMMENT 'label: Макс.цена за 6 месяцев;comment:Максимальная цена адреса за 6 месяцев. Устанавливается администратором;visible:0;;format:[''decimal'', 2] ;mask:@money;',
  `price11max` decimal(11,4) NOT NULL DEFAULT '100000.0000' COMMENT 'label: Макс.цена за 11 месяцев;comment:Максимальная цена адреса за 11 месяцев. Устанавливается администратором;visible:0;;format:[''decimal'', 2] ;mask:@money;',
  `photos` text COMMENT 'label:Фотографии;visible:0;show:1;format:images;type:json;',
  `files` text COMMENT 'label:Документы;visible:0;type:json;format:images;comments:Документы, прикрепленные к адресу в виде файлов;',
  `range` int(10) DEFAULT '0' COMMENT 'label:Ранг адреса; visible:0;comment:Используется для ранжирования адресов всписке;',
  `seo_id` int(11) DEFAULT NULL COMMENT 'label:Seo-данные;link:1;show:0;',
  `h1` varchar(255) DEFAULT NULL COMMENT 'show:1;visible:0;',
  `title` varchar(255) DEFAULT NULL COMMENT 'show:1;visible:0;',
  `meta_description` varchar(255) DEFAULT NULL COMMENT 'label:Поле Meta Description; show:1;visible:0;',
  `seo_text` text COMMENT 'label:SEO Текст; visible:0;show:1;',
  `enabled` tinyint(1) DEFAULT NULL COMMENT 'label:Активен;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `address_of_user_idx` (`owner_id`),
  KEY `rating_idx` (`rating`),
  KEY `address_nalog_idx` (`nalog_id`),
  KEY `address_okrug_idx` (`okrug_id`),
  KEY `address_region_idx` (`region_id`),
  KEY `address_metro_idx` (`metro_id`),
  KEY `address_status_idx` (`status_id`),
  KEY `range_KEY` (`range`),
  KEY `FK_address_seo` (`seo_id`),
  KEY `FK_photo_order` (`photo_order_id`),
  CONSTRAINT `FK_address_metro` FOREIGN KEY (`metro_id`) REFERENCES `adr_metro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_address_nalog` FOREIGN KEY (`nalog_id`) REFERENCES `adr_nalogs` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_address_of_user` FOREIGN KEY (`owner_id`) REFERENCES `adr_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_address_okrug` FOREIGN KEY (`okrug_id`) REFERENCES `adr_okrugs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_address_region` FOREIGN KEY (`region_id`) REFERENCES `adr_regions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_address_seo` FOREIGN KEY (`seo_id`) REFERENCES `adr_seo` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_address_status` FOREIGN KEY (`status_id`) REFERENCES `adr_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_photo_order` FOREIGN KEY (`photo_order_id`) REFERENCES `adr_porders` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Title: Адреса;\r\nModel: Address;\r\nimp: Адрес;\r\nrodp: Адреса;\r\nvinp: Адрес;\r\nparent:objects;\r\ncaption:%address%';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_app_options`
--

DROP TABLE IF EXISTS `adr_app_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_app_options` (
  `id` int(11) NOT NULL COMMENT 'show=false',
  `name` varchar(45) DEFAULT NULL COMMENT 'title:Имя;visible:true;show:true',
  `alias` varchar(45) DEFAULT NULL COMMENT 'label:Псевдоним;comment:Внутренне имя опции для привязки ее к участию в бизнес-логике;show:0;readonly:1;',
  `val_datetime` datetime DEFAULT NULL COMMENT 'title:Значение;visible:false;;show:true',
  `val_int` int(11) DEFAULT NULL COMMENT 'title:Значение;visible:false;show:true',
  `val_float` double DEFAULT NULL COMMENT 'title:Значение;visible:false;show:true',
  `val_str` varchar(255) DEFAULT NULL COMMENT 'title:Значение;visible:false;show:true',
  `val_text` text COMMENT 'title:Значение;visible:false;show:true',
  `vtype` enum('int','float','str','text','datetime') DEFAULT 'str' COMMENT 'title:Тип;visible:true;show:true',
  `user_id` int(11) DEFAULT '0' COMMENT 'show:false',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `alias_UNIQUE` (`alias`),
  KEY `option_of_user_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='title: Настройки;\r\nmodel:AppOption;\r\nimp: Системная опция;\r\nrodp: Системной опции;\r\nvinp: Системную опцию;\r\nparent:objects;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_auction_rates`
--

DROP TABLE IF EXISTS `adr_auction_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_auction_rates` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Дата ставки;type:datetime;filter:range;',
  `auction_id` int(11) DEFAULT NULL COMMENT 'label:Дата акуциона;filter:list;',
  `address_id` int(11) DEFAULT NULL COMMENT 'label:Адрес;filter:list;condition:[owner_id=>owner_id];',
  `owner_id` int(11) DEFAULT NULL COMMENT 'label:Владелец адреса;filter:list;default:currentUser();',
  `amount` float(10,4) DEFAULT '0.0000' COMMENT 'label:Размер ставки; format:decimal;title:Размер ставки;;format:[''decimal'', 2] ;mask:@decimal;',
  PRIMARY KEY (`id`),
  KEY `FK_rate_auction` (`auction_id`),
  KEY `FK_rate_address` (`address_id`),
  KEY `FK_rate_owner` (`owner_id`),
  CONSTRAINT `FK_rate_address` FOREIGN KEY (`address_id`) REFERENCES `adr_addresses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_rate_auction` FOREIGN KEY (`auction_id`) REFERENCES `adr_auctions` (`id`) ON DELETE NO ACTION,
  CONSTRAINT `FK_rate_owner` FOREIGN KEY (`owner_id`) REFERENCES `adr_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Title: Список ставок аукциона;\r\nimp: Ставка аукциона;\r\nrodp:Ставки аукциона;\r\nvinp: Ставку аукциона;\r\nModel:AuctionRate;\r\nparent:objects;\r\ncaption:Ставка аукциона %amount%;\r\nconstants:STEP=100;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_auctions`
--

DROP TABLE IF EXISTS `adr_auctions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_auctions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NULL DEFAULT NULL COMMENT 'label:Дата аукцина;type:date;',
  `price` float(10,4) NOT NULL DEFAULT '0.0000' COMMENT 'label:Максимальная ставка;',
  `address_id` int(11) DEFAULT NULL COMMENT 'label:Адрес победитель;filter:list;',
  `owner_id` int(11) DEFAULT NULL COMMENT 'label:Влаелец адреса победителя;filter:list;default:currentUser();',
  `status_id` int(11) DEFAULT NULL COMMENT 'label:Статус аукциона;filter:list;',
  `rate_id` int(11) DEFAULT NULL COMMENT 'label:Ставка победитель;visible:0;',
  `rate_count` int(11) NOT NULL DEFAULT '0' COMMENT 'label:Число сделанных ставок;',
  `amount` float(10,4) DEFAULT NULL COMMENT 'label: Объем всех ставок; format:decimal;title:Размер ставки;;format:[''decimal'', 2] ;mask:@decimal;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_id` (`id`),
  KEY `FK_auction_address` (`address_id`),
  KEY `FK_auction_owner` (`owner_id`),
  KEY `FK_auction_status` (`status_id`),
  KEY `FK_action_rate` (`rate_id`),
  CONSTRAINT `FK_action_rate` FOREIGN KEY (`rate_id`) REFERENCES `adr_auction_rates` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_auction_address` FOREIGN KEY (`address_id`) REFERENCES `adr_addresses` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_auction_owner` FOREIGN KEY (`owner_id`) REFERENCES `adr_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_auction_status` FOREIGN KEY (`status_id`) REFERENCES `adr_status` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=9275 DEFAULT CHARSET=utf8 COMMENT='Title: Список аукционов;\r\nimp: Аукцион;\r\nrodp:Аукциона;\r\nvinp: Аукцион;\r\nModel:Auction;\r\nparent:objects;\r\ncaption:Аукцион %date_view%;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_auth`
--

DROP TABLE IF EXISTS `adr_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_auth` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `source` varchar(50) NOT NULL,
  `source_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-auth-user_id-user-id` (`user_id`),
  CONSTRAINT `fk-auth-user_id-user-id` FOREIGN KEY (`user_id`) REFERENCES `adr_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_counters`
--

DROP TABLE IF EXISTS `adr_counters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_counters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(50) DEFAULT NULL COMMENT 'label: Псевдоним номера',
  `value` int(11) NOT NULL DEFAULT '0' COMMENT 'label: Последний номер',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'label: Дата получения последнего номера',
  `template` varchar(50) DEFAULT NULL COMMENT 'label:шаблон генерации номера',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='title: Счетчики;\r\nmodel:BaseCounter;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_deliveries`
--

DROP TABLE IF EXISTS `adr_deliveries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_deliveries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(50) DEFAULT NULL COMMENT 'label:Псевдоним;',
  `name` varchar(50) DEFAULT NULL COMMENT 'label:Название;',
  `description` text COMMENT 'label:Описание;visible:0;',
  `price` decimal(11,4) DEFAULT '0.0000' COMMENT 'label: Стоимость доставки;visible:1;format:currency ;mask:@money;',
  `delivery_requirement` tinyint(1) DEFAULT '1' COMMENT 'label:Требуется доставка;comment:Для этого способа доставки требуется физическая доставка;type:boolean;visible:0;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `aliasUNIQUE` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=3820 DEFAULT CHARSET=utf8 COMMENT='Title: Способы доставки;\r\nrodp:способа доставки;\r\nvinp: способ доставки;\r\nimp: способ доставки;\r\nModel:Delivery;\r\nparent:objects;\r\ncaption:%name% - %price_view% ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_document_types`
--

DROP TABLE IF EXISTS `adr_document_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_document_types` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT 'label:Название;comment: Человеко понятное название типа документа;',
  `alias` varchar(45) NOT NULL COMMENT 'label:Псевдоним;comment:Внутренне имя типа документа для привязки ее к участию в бизнес-логике;show:0;readonly:1;',
  `template` text COMMENT 'label:Шаблон;comment:Шаблон для генерации документа в pdf;show:0;readonly:1;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `alias_UNIQUE` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Title: Типы документов;\r\nimp:Тип документа;\r\nrodp:Типа документа;\r\nvinp:Тип документа;\r\nModel:DocumentType;\r\nparent:objects;\r\nsortAttr:name;\r\nsortDir: asc;\r\ncaption:%name%;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_documents`
--

DROP TABLE IF EXISTS `adr_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_documents` (
  `id` int(11) NOT NULL COMMENT 'show:1;visible:0;filter:value;',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Создан;comment:Дата создания документа;visible:1;filter:range;type:date;format:d.m.Y;',
  `doc_type_id` int(11) NOT NULL COMMENT 'label:Тип документа;visible:1;filter:list;',
  `number` varchar(45) DEFAULT NULL COMMENT 'label:Номер;comment:Номер документа;filter:value;',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'label:Изменен;content: Дата последнего изменения документа;show:0;type:date;format:d.m.Y;',
  `order_id` int(11) DEFAULT NULL COMMENT 'label:Заказ;show:false;comment:Заказ, к которому относится документ',
  `porder_id` int(11) DEFAULT NULL COMMENT 'label:Портальный заказ;show:false;comment:Портальный заказ, если счет выставлен на оплату портальной услуги;',
  `address_id` int(11) DEFAULT NULL COMMENT 'label:Адрес;show:false;comment:Адрес, связанный с документов, напрмиер, для счета это адрес, аренда которого входит в оплату счета;',
  `summa` float(10,4) DEFAULT NULL COMMENT 'label: Сумма;vidible:1;;format:[''decimal'', 2] ;mask:@decimal;',
  `filename` tinytext COMMENT 'label:Файл;comment: Имя файла, в котором хранится документ;show:0;',
  `owner_id` int(11) DEFAULT NULL COMMENT 'label:Владелец(создатель) документа; comment:Реквизиты стороны владельца(создателя) документа. Указывается, если реквизиты владельца зарегистрированы на портале;link:1;readonly:1;model:CObject;',
  `recipient_id` int(11) DEFAULT NULL COMMENT 'label:Получатель документа; comment:Реквизиты второй стороны - получателя документа. Указывается, если реквизиты получателя зарегистрированы на портале;link:1;readonly:1;model:CObject;',
  `info` text COMMENT 'label:Комментарии;comment:Дополнительная информация по документу;show:1;',
  `state` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'label:Состояние;',
  `pay_id` varchar(50) NOT NULL DEFAULT '0' COMMENT 'show:0;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `document_of_order_idx` (`order_id`),
  KEY `document_date` (`created_at`),
  KEY `number_idx` (`number`),
  KEY `FK_document_type` (`doc_type_id`),
  KEY `FK_documents_recipient` (`recipient_id`),
  KEY `FK_document_owner` (`owner_id`),
  KEY `FK_document_of_porder` (`porder_id`),
  KEY `FK_document_address` (`address_id`),
  CONSTRAINT `FK_document_address` FOREIGN KEY (`address_id`) REFERENCES `adr_addresses` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_documents_recipient` FOREIGN KEY (`recipient_id`) REFERENCES `adr_objects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_document_of_order` FOREIGN KEY (`order_id`) REFERENCES `adr_orders` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_document_of_porder` FOREIGN KEY (`porder_id`) REFERENCES `adr_porders` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_document_owner` FOREIGN KEY (`owner_id`) REFERENCES `adr_objects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_document_type` FOREIGN KEY (`doc_type_id`) REFERENCES `adr_document_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='title: Список документов;\r\nimp:Документ;\r\nrodp:Документа;\r\nvinp:Документ;\r\nmodel:Document;\r\ncaption:%doc_type_id_view% №%number% от \\"%created_at_view%\\";\r\nparent:objects;\r\nsortAttr:created_at;\r\nsortDir:desc;\r\nconstants:CONTRACT_STATE_DEACTIVE=0,CONTRACT_STATE_ACTIVE=1,CONTRACT_STATE_SIGNING=2;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_faq`
--

DROP TABLE IF EXISTS `adr_faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Дата добавления;filter:range;type:date;format:d.m.Y;show:1;visible:1;',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'label:Дата изменения;show:0;',
  `question` text NOT NULL COMMENT 'label:Вопрос;',
  `answer` text COMMENT 'label:Ответ;visible:0;',
  `role` set('Заказчик','Владелец') DEFAULT NULL COMMENT 'label:Для кого вопрос;visible:1;filter:list;format:checkboxlist;',
  `enabled` tinyint(1) DEFAULT '0' COMMENT 'label:Активен;type:boolean;visible:0;',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2405 DEFAULT CHARSET=utf8 COMMENT='model: Faq;\r\ncomment: Часто задаваемые вопросы;\r\nparent: objects;\r\nconstants: ROLE_CUSTOMER=''Заказчик'',ROLE_OWNER=''Владелец'';';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_files`
--

DROP TABLE IF EXISTS `adr_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_files` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT 'Имя файла',
  `type` tinyint(4) DEFAULT NULL COMMENT 'прикладной тип файла, чтобы указать, как он должен обрабатываться в системе',
  `date` datetime DEFAULT NULL COMMENT 'время создания',
  `date_acc` datetime DEFAULT NULL COMMENT 'Время последнего доступа',
  `owner_id` int(11) DEFAULT NULL COMMENT 'id объекта, к которому привязан файл',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_invoices`
--

DROP TABLE IF EXISTS `adr_invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9317 DEFAULT CHARSET=utf8 COMMENT='title: Список счетов;\r\nimp:Счет;\r\nrodp:Счета;\r\nvinp:Счет;\r\nmodel:Invoice;\r\ncaption: счет №%number% от \\"%created_at_view%\\";\r\nparent:documents;\r\nsortAttr:created_at;\r\nsortDir:desc;\r\nconstants: STATE_NOPAID = 0,STATE_PAID = 1;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_metro`
--

DROP TABLE IF EXISTS `adr_metro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_metro` (
  `id` int(11) NOT NULL COMMENT 'show:false',
  `alias` varchar(45) NOT NULL COMMENT 'label:Код;filter:value;readonly:1;',
  `name` varchar(45) NOT NULL COMMENT 'label:Название станции;filter:like;readonly:1;',
  `title` varchar(255) DEFAULT NULL COMMENT 'label:Поле Title; show:1;visible:0;',
  `meta_descrition` varchar(255) DEFAULT NULL COMMENT 'label:Поле Meta Description; show:1;visible:0;',
  `seo_text` text COMMENT 'label:SEO Текст;show:1;visible:0;',
  `h1` varchar(255) DEFAULT NULL COMMENT 'label:h1; visible:0;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `alias_UNIQUE` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Title: Список станций метро;\r\nimp: Станция метро;\r\nrodp:Станции метро;\r\nvinp: Станцию метро;\r\nModel:Metro;\r\nparent:objects;\r\ncaption:м.%name%;\r\ncomment: Справочник станций метро Москвы, может быть изменен с помощью администратора сайта.;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_migration`
--

DROP TABLE IF EXISTS `adr_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_nalogs`
--

DROP TABLE IF EXISTS `adr_nalogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_nalogs` (
  `id` int(11) NOT NULL,
  `number` smallint(6) DEFAULT NULL COMMENT 'label:Номер;comment:Номер налоговой инспеции;filter:value;readonly:1;',
  `okrug_id` int(11) DEFAULT NULL COMMENT 'label:Округ;comment:Округ, к которому относится налоговая инспекция;filter:list;readonly:1;',
  `title` varchar(255) DEFAULT NULL COMMENT 'label:Поле Title; show:1;visible:0;',
  `meta_description` varchar(255) DEFAULT NULL COMMENT 'label:Поле Meta Description; show:1;visible:0;',
  `seo_text` text COMMENT 'label:SEO Текст;show:1;visible:0;comment: Для разбивки на колонки использовать <p>;',
  `h1` varchar(50) DEFAULT NULL COMMENT 'label:h1; visible:0;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `number_UNUQUE` (`number`),
  KEY `nalog_okrug_idx` (`okrug_id`),
  CONSTRAINT `nalog_okrug` FOREIGN KEY (`okrug_id`) REFERENCES `adr_okrugs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Title: Список налоговых инспекций;\r\nimp: Налоговая инспекция;\r\nrodp: Налоговой инспекции;\r\nvinp: Налоговую инспекцию;\r\nModel:Nalog;\r\ncaption:ИФНС №%number%;\r\nparent:objects;\r\ncomment: Справочник налоговых инспекций Москвы, может быть изменен с помощью администратора сайта.;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_notifications`
--

DROP TABLE IF EXISTS `adr_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('Изменение статуса заказа','Изменение статуса адреса','Изменение статуса отзыв','Успешная регистрация','Срочное оформление','Подтверждение платежа') NOT NULL COMMENT 'label:Тип оповещения;',
  `email` varchar(45) DEFAULT NULL COMMENT 'label: Адрес, на который будут уходить оповещения;',
  `user_id` int(11) DEFAULT NULL COMMENT 'label: Пользователь, к которму относятся оповещения;',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` tinyint(1) DEFAULT NULL COMMENT 'label:Активно;type:boolean;',
  `status_id` int(11) DEFAULT NULL COMMENT 'label:Статус;comment:Статус, при переходе на который возникает уведомление;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `notification_of_user_idx` (`user_id`),
  KEY `FK_notification_status` (`status_id`),
  CONSTRAINT `FK_notification_status` FOREIGN KEY (`status_id`) REFERENCES `adr_status` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_notification_user` FOREIGN KEY (`user_id`) REFERENCES `adr_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6911 DEFAULT CHARSET=utf8 COMMENT='Title: Уведомления;\r\nModel:Notification;\r\nparent:objects;\r\nconstants:NT_ORDER_STATUS=''Изменение статуса заказа'',NT_ADDRESS_STATUS=''Изменение статуса адреса'',NT_REVIEW_STATUS=''Изменение статуса отзыв'',NT_REGISTRATION=''Успешная регистрация'',NT_QUICK_ORDER=''Срочное оформление'',NT_PAYMENT_CONFIRMATION=''Подтверждение платежа'';';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_objects`
--

DROP TABLE IF EXISTS `adr_objects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tablename` varchar(50) DEFAULT NULL COMMENT 'show:false;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9317 DEFAULT CHARSET=utf8 COMMENT='Model:CObject';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_okrugs`
--

DROP TABLE IF EXISTS `adr_okrugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_okrugs` (
  `id` int(11) NOT NULL COMMENT 'show: false',
  `alias` varchar(45) NOT NULL COMMENT 'label: Код округа; comment:Название административного округа;filter:like;readonly:1;',
  `name` varchar(45) DEFAULT NULL COMMENT 'label: Название округа; comment:Название административного округа;filter:like;readonly:1;',
  `abr` varchar(5) NOT NULL COMMENT 'label:Аббревиатура;comment: Сокращенное название административного округа;readonly:1;',
  `title` varchar(255) DEFAULT NULL COMMENT 'label:Поле Title; show:1;visible:0;',
  `meta_description` varchar(255) DEFAULT NULL COMMENT 'label:Поле Meta Description; show:1;visible:0;',
  `h1` varchar(255) DEFAULT NULL COMMENT 'label:h1; visible:0;',
  `seo_text` text COMMENT 'label:SEO Текст;show:1;visible:0;comment: Для разбивки на колонки использовать <p>;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `alias_UNIQUE` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Title: Административные округа Москвы;\r\nrodp: округа;\r\nvinp: округ;\r\nimp: округ;\r\nModel:Okrug;\r\ncaption:%abr%;\r\nparent:objects;\r\ncomment: Справочник административных округов Москвы, может быть изменен с помощью администратора сайта.;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_options`
--

DROP TABLE IF EXISTS `adr_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icon` varchar(50) DEFAULT NULL COMMENT 'label:Иконка;directory:options;format:icon;',
  `alias` varchar(45) COMMENT 'label:Псевдоним;comment:Внутренне имя опции для привязки ее к участию в бизнес-логике;readonly:0;',
  `stype` enum('Опция адреса') DEFAULT 'Опция адреса' COMMENT 'label:Тип параметра;show:0;filter:list;',
  `name` varchar(100) NOT NULL COMMENT 'label:Название параметра;filter:like;',
  `label` varchar(100) DEFAULT NULL COMMENT 'label: Подпись для меню;',
  `title` varchar(255) DEFAULT NULL COMMENT 'label:Поле Title; show:1;visible:0;',
  `meta_description` varchar(255) DEFAULT NULL COMMENT 'label:Поле Meta Description; show:1;visible:0;',
  `h1` varchar(255) DEFAULT NULL COMMENT 'label:h1; visible:0;',
  `seo_text` text COMMENT 'label:SEO Текст;show:1;visible:0;comment: Для разбивки на колонки использовать <p>;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `alias_UNIQUE` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=2124 DEFAULT CHARSET=utf8 COMMENT='title: Список опций адресов;\r\nimp:Опция;\r\nrodp:Опции;\r\nvinp:Опцию;\r\nModel:Option;\r\nparent:objects;\r\ncomment: Список опций адресов. Дополнительные иконки могу быть добавлены системным администратором.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_orders`
--

DROP TABLE IF EXISTS `adr_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_orders` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Дата создания;filter:range;',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'label:Дата изменения;show:0;',
  `status_changed_at` timestamp NULL DEFAULT NULL COMMENT 'label:Дата изменения статуса;show:0;comment:Дата последнего изменения статуса. Выставляется автоматически;',
  `number` varchar(45) DEFAULT NULL COMMENT 'label:Номер;filter:value;',
  `status_id` int(11) DEFAULT NULL COMMENT 'label:Статус;filter:list; vidible:1;',
  `duration` enum('6 месяцев','11 месяцев') NOT NULL DEFAULT '11 месяцев' COMMENT 'label:Срок оказания услуг;comment:Срок приобретения услуг заказа в месяцах.;visible:1;filter:list;',
  `customer_id` int(11) DEFAULT NULL COMMENT 'label:Заказчик;filter:like;link:1;comment:Пользователь, совершивший заказ;',
  `customer_r_id` int(11) DEFAULT NULL COMMENT 'label:Реквизиты заказчика;filter:like;link:1;comment:Реквизиты заказчика, на которые будет оформлен заказ и необходимые документы;readonly:0;visible:0;condition:[user_id=>customer_id];',
  `address_id` int(11) NOT NULL COMMENT 'label:Адрес;comment:Адрес, к которому относится закза. Заказать можно одну или несколько услуг, относящихся только к одному адресу;filter:like; visible:1;link:1;condition:[owner_id=>owner_id];',
  `owner_id` int(11) DEFAULT NULL COMMENT 'label:Владелец;filter:like;link:1;comment:Владелец заказанного адреса;readonly:1;',
  `date_start` timestamp NULL DEFAULT NULL COMMENT 'label:Старт;comment:Дата начала аренды адреса;filter:range;type:date;format:d.m.Y;show:1;visible:0;',
  `date_fin` timestamp NULL DEFAULT NULL COMMENT 'label:Окончание;comment:Дата окончания аренды адреса. Определяется датой старта и длительностью аренды.;filter:range;type:date;format:d.m.Y;show:1;visible:0;readonly:1;',
  `address_price` double(10,4) unsigned zerofill DEFAULT NULL COMMENT 'label:Стоимость адреса;comment: Стоимость адреса за выбранный период; visible:1;readonly:1;format:[''decimal'', 2] ;',
  `is_quick` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'label:Срочный;comment:Заказ является срочным. Устанавливается автоматически при наличии услуги срочного оформления в заказе;visible:0;type:boolean;',
  `paid_at` timestamp NULL DEFAULT NULL COMMENT 'label: Заказ оплачен; show:0;comment:Дата оплаты заказа;',
  `delivery_id` int(11) DEFAULT NULL COMMENT 'label:Заказ на доставку;comment:Заказ порталу на доставку документов; visible:0;readonly:1;link:1;',
  `delivery_type_id` int(11) COMMENT 'label:Способ доставки;visible:0;',
  `delivery_contacts` varchar(150) DEFAULT NULL COMMENT 'label: Контакты по доставке;comment:контактная информация для связи по вопросу доставки;visible:0;',
  `delivery_address` varchar(150) DEFAULT NULL COMMENT 'label:Адрес доставки;comment:Адрес доставки документов; visible:0;',
  `guarantee_id` int(11) DEFAULT NULL COMMENT 'label:Заказ на гарантию;comment:Заказ на пр; visible:0;readonly:1;link:1;',
  `payment_type` enum('не задан','по карте','по счету') DEFAULT 'не задан' COMMENT 'label:Способ оплаты;filter:list;visible:1;',
  `summa` double(10,4) DEFAULT '0.0000' COMMENT 'label:Сумма;comment: Общая стоимость заказа; visible:1;readonly:1;format:[''decimal'', 2] ;mask:@decimal;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `date_UNIQUE` (`created_at`),
  KEY `customer_order_user_idx` (`customer_id`),
  KEY `order_of_address_idx` (`address_id`),
  KEY `date_idx` (`created_at`),
  KEY `order_status_idx` (`status_id`),
  KEY `order_for_owner` (`owner_id`),
  KEY `order_customer_r` (`customer_r_id`),
  KEY `order_delivery` (`delivery_id`),
  KEY `order_delivery_type` (`delivery_type_id`),
  KEY `order_guarantee` (`guarantee_id`),
  CONSTRAINT `order_guarantee` FOREIGN KEY (`guarantee_id`) REFERENCES `adr_porders` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `order_customer_r` FOREIGN KEY (`customer_r_id`) REFERENCES `adr_objects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `order_delivery` FOREIGN KEY (`delivery_id`) REFERENCES `adr_porders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `order_delivery_type` FOREIGN KEY (`delivery_type_id`) REFERENCES `adr_deliveries` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `order_for_owner` FOREIGN KEY (`owner_id`) REFERENCES `adr_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `order_of_address` FOREIGN KEY (`address_id`) REFERENCES `adr_addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `order_status` FOREIGN KEY (`status_id`) REFERENCES `adr_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `order_to_user` FOREIGN KEY (`customer_id`) REFERENCES `adr_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Title: Заказы;\r\nimp:Заказ;\r\nrodp:Заказа;\r\nvinp:Заказ;\r\nModel:Order;\r\nparent:objects;\r\ncaption:№%number% на сумму %summa_view%;\r\ncomment: Список всех заказов, сделанных на портале.;\r\nconstants:PTYPE_NONE=''не задан'',PTYPE_CACH=''Наличные'',PTYPE_CARD=''По карте'',PTYPE_BANK=''По счету'',DURATION_ONCE=''разово'',DURATION_6=''6 месяцев'',DURATION_11=''11 месяцев'',DTYPE_PICKUP=''самовывоз'',DTYPE_DELIVERY=''доставка'';';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_pages`
--

DROP TABLE IF EXISTS `adr_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_pages` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL COMMENT 'label:Раздел;filter:list;show:1;visible:1;link:1;;condition:[type=>TYPE_RUBRIC]',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Дата добавления;filter:range;type:date;format:d.m.Y;show:1;visible:1;readonly:0;',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'label:Дата изменения;show:0;',
  `h1` varchar(255) NOT NULL COMMENT 'label:Заголовок H1;filter:like;',
  `label` varchar(50) DEFAULT NULL COMMENT 'label:Метка;comment:Текст, который будет отображаться в виде ссылки на эту страницу; visible:0;content:Текст ссылки на эту страницу, который используется для построения меню',
  `content` longtext COMMENT 'label:Текст страницы;comment: В тексте страницы могут быть использованы HTML теги;visible:0;',
  `menu` set('main','conditions') DEFAULT '' COMMENT 'label:Меню; comment: Меню, в котором отображать страницу;format:checkboxlist;filter:list;',
  `title` varchar(255) DEFAULT '' COMMENT 'label:Поле Title; show:1;visible:0;',
  `meta_description` varchar(255) DEFAULT '' COMMENT 'label:Поле Meta Description; show:1;visible:0;',
  `seo_text` text COMMENT 'label:SEO-текст;visible:0;',
  `slug` varchar(100) DEFAULT NULL COMMENT 'label:Псевдоним;comment:латинский псевдоним страницы, который будет использован для генерации URL страницы;filter:like;requeried:0;comment:Используется для генерации URL. Если оставить пустым, то будет автоматическисгенерировано.',
  `template` varchar(100) DEFAULT NULL COMMENT 'label:Шаблон;comment:Шаблон для отображения страницы - имя view;visible:0;',
  `type` enum('static','post','rubric') DEFAULT 'post' COMMENT 'label:Тип страницы;show:1;filter:list;',
  `tag_str` varchar(45) DEFAULT NULL COMMENT 'label:дополнительное строковое поле, которое может быть использовано в произволных целях;show:0;',
  `tag_int` int(11) DEFAULT NULL COMMENT 'label:дополнительное целочисленное поле, которое может быть использовано в произволных целях;show:0;',
  `seo_id` int(11) DEFAULT NULL COMMENT 'label:Seo-данные;visible:0;show:0;',
  `visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'label:Показывать;type:boolean;visible:0;',
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'label:Активна;type:boolean;visible:0;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`),
  KEY `FK_pages_seo` (`seo_id`),
  KEY `FK_parent_page` (`parent_id`),
  CONSTRAINT `FK_pages_seo` FOREIGN KEY (`seo_id`) REFERENCES `adr_seo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_parent_page` FOREIGN KEY (`parent_id`) REFERENCES `adr_pages` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Title: Страницы;\r\nModel:Page;\r\ncomment: Страницы сайта;\r\nparent:objects;\r\nconstants:MENU_MAIN=''main'',MENU_CONDITIONS=''conditions'', TYPE_STATIC=''static'',TYPE_POST=''post'',TYPE_RUBRIC=''rubric'';\r\ncaption:%h1%;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_payments`
--

DROP TABLE IF EXISTS `adr_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_payments` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Дата;comment:Дата проведения оплаты;type:date;format:d.m.Y;',
  `ptype` enum('Наличными','По карте','По счету') DEFAULT NULL COMMENT 'label:Тип платежа;comment:Тип платежа, по счету, по карте, наличными...',
  `value` decimal(10,4) DEFAULT '0.0000' COMMENT 'format:decimal;title:Сумма;comment:Сумма платежа;format:[''decimal'', 2] ;mask:@decimal;',
  `payment_order_id` int(11) DEFAULT NULL COMMENT 'label:Платежка;comment:Платежное поручение, по которому произведена оплата;link:1;',
  `invoice_id` int(11) DEFAULT NULL COMMENT 'label:Счет;comment:Счет, по которому была произведена оплата;link:1;',
  `info` text COMMENT 'show:1;label:Информация;comment:Дополнительная информация о платеже',
  `data` text COMMENT 'show:0;type:json;',
  `loaded_at` timestamp NULL DEFAULT NULL,
  `payment_order_date` timestamp NULL DEFAULT NULL,
  `payment_order_number` varchar(50) DEFAULT NULL,
  `payment_order_summa` float(10,4) DEFAULT NULL COMMENT 'label:Сумма по платежке;',
  `payer_id` int(11) DEFAULT NULL COMMENT 'label:Плательщик;',
  `payer_name` varchar(255) DEFAULT NULL,
  `invoice_date` timestamp NULL DEFAULT NULL,
  `invoice_number` varchar(50) DEFAULT NULL,
  `invoice_summa` float(10,4) DEFAULT NULL COMMENT 'label: Сумма по счету;',
  `recipient_id` int(11) DEFAULT NULL COMMENT 'label: Получатель;',
  `recipient_name` varchar(255) DEFAULT NULL,
  `pay_id` varchar(50) DEFAULT NULL COMMENT 'show:0;comment:используется для эквайринга',
  `confirmed` int(1) DEFAULT '0' COMMENT 'show:0; comment: платеж проведен',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `payment_invoice` (`invoice_id`),
  KEY `payment_payment_order` (`payment_order_id`),
  CONSTRAINT `payment_invoice` FOREIGN KEY (`invoice_id`) REFERENCES `adr_documents` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `payment_payment_order` FOREIGN KEY (`payment_order_id`) REFERENCES `adr_documents` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='title: Платежи;\r\nModel:Payment;\r\nvinp:Платеж;\r\nimp:Платеж;\r\nrodp:Платежа;\r\nparent:objects;\r\ncaption: на сумму %value% руб.;\r\nconstants:PTYPE_NONE=''не задан'',PTYPE_CACH=''Наличными'',PTYPE_CARD=''По карте'',PTYPE_BANK=''По счету'';';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_photo_calendar`
--

DROP TABLE IF EXISTS `adr_photo_calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_photo_calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL COMMENT 'label: Адреса;condition:[owner_id=>user_id];',
  `user_id` int(11) NOT NULL COMMENT 'label:Пользователь;readonly:0;',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'label:Дата;type:date;',
  `interval` tinyint(1) NOT NULL COMMENT 'label:Интервал;visible:0;',
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'label:Активен;type:boolean;visible:0;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `FK_photo_calendar_user` (`user_id`),
  KEY `FK_photo_calendar_address` (`address_id`),
  CONSTRAINT `FK_photo_calendar_address` FOREIGN KEY (`address_id`) REFERENCES `adr_addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_photo_calendar_users` FOREIGN KEY (`user_id`) REFERENCES `adr_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9301 DEFAULT CHARSET=utf8 COMMENT='title: Календарь фотографа;\r\nModel:PhotoCalendar;\r\nparent:objects;\r\ncaption: %date% - %interval%;\r\nconstants:INTERVAL_MORNING=1,INTERVAL_AFTERNON=2,INTERVAL_EVENING=3;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_photo_visits`
--

DROP TABLE IF EXISTS `adr_photo_visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_photo_visits` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL COMMENT 'title:Дата визита;comment:Дата визита фотографа',
  `address_id` int(11) DEFAULT NULL COMMENT 'title:Адрес;comment: Адрес, на который выезжает фотограф',
  `info` text COMMENT 'title:Комментарии;comment: Дополнительная информация по визиту фотографа',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `photo_visit_address_idx` (`address_id`),
  CONSTRAINT `photo_visit_address` FOREIGN KEY (`address_id`) REFERENCES `adr_addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='title: Визиты фотографа\r\nparent:objects;\r\nmodel:PhotoVisit;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_porders`
--

DROP TABLE IF EXISTS `adr_porders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_porders` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'show:true;filter:value;',
  `number` varchar(45) DEFAULT NULL COMMENT 'label:Номер;filter:value;',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Дата создания;filter:range;',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'label:Дата изменения;show:0;',
  `service_name` varchar(50) DEFAULT NULL COMMENT 'label:Название;Comment: Название заказанной услуги;visible:1;filter:like;',
  `info` text COMMENT 'label:Дополнительная информация;visible:0;',
  `service_id` int(11) DEFAULT NULL COMMENT 'label:Услуга;comment:Заказанная услуга;visible:1;filter:1;link:1;',
  `customer_id` int(11) DEFAULT NULL COMMENT 'label:Заказчик;filter:like;link:1;comment:Пользователь, совершивший заказ;filter:like;',
  `customer_r_id` int(11) DEFAULT NULL COMMENT 'label:Реквизиты заказчика;filter:like;link:1;comment:Реквизиты заказчика, на которые будет оформлен заказ и необходимые документы;readonly:0;visible:0;condition:[user_id=>customer_id];',
  `recipient_id` int(11) DEFAULT NULL COMMENT 'label:Реквизиты портала;visible:0;comment:Реквизиты получателя оплаты за усугу;readonly:0;link:0;condition:[];',
  `summa` double(10,4) DEFAULT NULL COMMENT 'label:Сумма;comment: Общая стоимость заказа; visible:1;readonly:1;format:[''decimal'', 2] ;mask:@decimal;',
  `invoice_id` int(11) DEFAULT NULL COMMENT 'label:Счет;visible:0;comment:Счет, выставленный для оплаты услуги;readonly:0;link:0;readonly:1;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `FK_porder_recipient` (`recipient_id`),
  KEY `FK_porder_requisites` (`customer_r_id`),
  KEY `FK_porder_service` (`service_id`),
  KEY `FK_porder_user` (`customer_id`),
  KEY `FR_porder_invoice` (`invoice_id`),
  CONSTRAINT `FK_porder_recipient` FOREIGN KEY (`recipient_id`) REFERENCES `adr_objects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_porder_requisites` FOREIGN KEY (`customer_r_id`) REFERENCES `adr_objects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_porder_service` FOREIGN KEY (`service_id`) REFERENCES `adr_service_portal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_porder_user` FOREIGN KEY (`customer_id`) REFERENCES `adr_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FR_porder_invoice` FOREIGN KEY (`invoice_id`) REFERENCES `adr_invoices` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9316 DEFAULT CHARSET=utf8 COMMENT='Title: Заказы портала;\r\nimp:Заказ;\r\nrodp:Заказа;\r\nvinp:Заказ;\r\nModel:Porder;\r\nparent:objects;\r\ncaption: %service_name% (№%number%);\r\ncomment: Список заказов порталу.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_rcompany`
--

DROP TABLE IF EXISTS `adr_rcompany`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_rcompany` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'show:true;filter:value;',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Дата добавления;filter:range;type:date;format:d.m.Y;show:1;visible:1;',
  `form` enum('Физлицо','Юрлицо','ИП') NOT NULL DEFAULT 'Юрлицо' COMMENT 'visible:0;label:Правовой статус;filter:list;readonly:0;',
  `user_id` int(11) DEFAULT NULL COMMENT 'label:Пользователь; comment:Пользователь, с которым связаны реквизиты этого юридического лица;default:currentUser();filter:like;link:1;',
  `inn` varchar(12) DEFAULT NULL COMMENT 'label:ИНН;visible:1;filter:like;mask:9999999999;',
  `kpp` varchar(12) DEFAULT NULL COMMENT 'label:КПП;visible:0;filter:like;mask:999999999;',
  `ogrn` varchar(15) DEFAULT NULL COMMENT 'label:ОГРН;visible:0;filter:like;mask:9999999999999;',
  `name` varchar(50) NOT NULL COMMENT 'show:true;visible:true;label:Название;show:1;filter:like',
  `full_name` varchar(255) DEFAULT NULL COMMENT 'show:true;visible:true;label:Полное название;show:1;visible:0;filter:like',
  `address` varchar(100) DEFAULT NULL COMMENT 'show:true;visible:true;label:Адрес местонахождения;show:1;filter:like;',
  `phone` varchar(15) DEFAULT NULL COMMENT 'show:true;visible:0;label:Телефон;filter:like;',
  `bank_bik` varchar(9) DEFAULT NULL COMMENT 'show:true;visible:0;label:БИК банка;filter:like;mask:999999999;',
  `account` varchar(26) DEFAULT NULL COMMENT 'show:true;visible:0;label:Номер счета;filter:like;mask:@account;',
  `account_owner` varchar(50) DEFAULT NULL COMMENT 'show:true;visible:0;label:Владелец счета;',
  `dogovor_signature` varchar(50) DEFAULT NULL COMMENT 'show:true;visible:0;label:ФИО подписанта;comment:Фамилия Имя Отчество лица подписывающего договор;',
  `dogovor_face` varchar(50) DEFAULT NULL COMMENT 'show:true;visible:0;label:Договор подписывается в лице;default:Генерального директора;',
  `dogovor_osnovanie` varchar(50) DEFAULT NULL COMMENT 'show:true;visible:0;label:Договор на основании;default:Устава;comment:Подписант договора действует на основании;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_id` (`id`),
  KEY `FK_user` (`user_id`),
  KEY `KEY_inn` (`inn`),
  CONSTRAINT `adr_rcompany_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `adr_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9262 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Title: Реквизиты юрлиц;\r\nModel: Rcompany;\r\nimp: Реквизиты юрлица;\r\nrodp: Реквизиты юрлица;\r\nvinp: Реквизиты юрлица;\r\nparent:objects;\r\ncaption: %form% (%name%);\r\nconstants:FORM_PERSON=''Физлицо'',FORM_COMPANY=''Юрлицо'',FORM_IP=''ИП'';';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_regions`
--

DROP TABLE IF EXISTS `adr_regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_regions` (
  `id` int(11) NOT NULL COMMENT 'show:false',
  `name` varchar(45) NOT NULL COMMENT 'label:Название района;filter:like;',
  `okrug_id` int(11) DEFAULT NULL COMMENT 'label:Округ;filter:list;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `region_okrug` (`okrug_id`),
  CONSTRAINT `region_okrug` FOREIGN KEY (`okrug_id`) REFERENCES `adr_okrugs` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Title: Районы Москвы;\r\nrodp:района;\r\nvinp: район;\r\nimp: район;\r\nModel:Region;\r\nparent:objects;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_reviews`
--

DROP TABLE IF EXISTS `adr_reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_reviews` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Дата добавления;filter:range;type:date;format:d.m.Y;',
  `status_changed_at` timestamp NULL DEFAULT NULL COMMENT 'label:Дата изменения статуса;show:0;comment:Дата последнего изменения статуса. Выставляется автоматически;',
  `name` varchar(50) NOT NULL COMMENT 'label:Автор;filter:like;visible:true;',
  `email` varchar(50) DEFAULT NULL COMMENT 'label:e-mail;filter:like;visible:true;',
  `company` varchar(50) DEFAULT NULL COMMENT 'label:Название организации;filter:like;visible:true;',
  `author_position` varchar(50) DEFAULT NULL COMMENT 'label:Должность автора отзыва;show:1;visible:0;',
  `status_id` int(11) DEFAULT NULL COMMENT 'label:Статус;visible:true;filter:list;',
  `content` text NOT NULL COMMENT 'label:Текст отзыва;show:1;visible:0;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `date_idx` (`created_at`),
  KEY `review_status` (`status_id`),
  CONSTRAINT `review_status` FOREIGN KEY (`status_id`) REFERENCES `adr_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Title: Отзывы;\r\nrodp:отзыва;\r\nvinp: отзыв;\r\nimp: отзыв;\r\nModel:Review;\r\nparent:objects;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_rip`
--

DROP TABLE IF EXISTS `adr_rip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_rip` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'show:true;filter:value;',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Дата добавления;filter:range;type:date;format:d.m.Y;show:1;visible:1;',
  `form` enum('Физлицо','Юрлицо','ИП') NOT NULL DEFAULT 'ИП' COMMENT 'visible:true;label:Правовой статус;filter:list;',
  `user_id` int(11) NOT NULL COMMENT 'label:Пользователь; comment:Пользователь, с которым связаны реквизиты этого индивидуального предпринимателя;default:currentUser();filter:like;link:1;',
  `inn` varchar(12) NOT NULL COMMENT 'label:ИНН;visible:1;filter:like;mask:999999999999;',
  `kpp` varchar(12) DEFAULT NULL COMMENT 'label:КПП;visible:0;filter:like;mask:999999999;',
  `ogrnip` varchar(15) DEFAULT NULL COMMENT 'label:ОГРНИП;visible:0;filter:like;mask:999999999999999;',
  `fname` varchar(50) NOT NULL COMMENT 'show:true;visible:true;label:Имя;show:1;filter:like',
  `mname` varchar(50) DEFAULT NULL COMMENT 'show:true;visible:true;label:Отчество;show:1;filter:like',
  `lname` varchar(50) NOT NULL COMMENT 'show:true;visible:true;label:Фамилия;show:1;filter:like',
  `pasport_number` varchar(50) DEFAULT NULL COMMENT 'show:true;visible:0;label:Номер/серия паспорта;filter:like;mask:@pasport;',
  `pasport_date` timestamp NULL DEFAULT NULL COMMENT 'show:true;visible:0;label:Дата выдачи паспорта;type:date;format:d.m.Ytype:date;format:d.m.Y;',
  `pasport_organ` varchar(100) DEFAULT NULL COMMENT 'show:true;visible:0;label:Орган, выдавший паспорт;',
  `address` varchar(100) DEFAULT NULL COMMENT 'show:true;visible:0;label:Адрес регистрации;filter:like;',
  `phone` varchar(15) DEFAULT NULL COMMENT 'show:true;visible:0;label:Телефон;filter:like;',
  `bank_bik` varchar(9) DEFAULT NULL COMMENT 'show:true;visible:0;label:БИК банка;filter:like;mask:999999999;',
  `account` varchar(26) DEFAULT NULL COMMENT 'show:true;visible:0;label:Номер счета;filter:like;mask:@account;',
  `account_owner` varchar(50) DEFAULT NULL COMMENT 'show:true;visible:0;label:Владелец счета;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_id` (`id`),
  KEY `FK_user` (`user_id`),
  KEY `KEY_inn` (`inn`),
  CONSTRAINT `FK_user` FOREIGN KEY (`user_id`) REFERENCES `adr_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9064 DEFAULT CHARSET=utf8 COMMENT='Title: Реквизиты ИП;\r\nModel: Rip;\r\nimp: Реквизиты;\r\nrodp: Реквизитов;\r\nvinp: Реквизиты;\r\nparent:objects;\r\ncaption: %form% (%lname%);\r\nconstants:FORM_PERSON=''Физлицо'',FORM_COMPANY=''Юрлицо'',FORM_IP=''ИП'';';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_rperson`
--

DROP TABLE IF EXISTS `adr_rperson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_rperson` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'show:true;filter:value;',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Дата добавления;filter:range;type:date;format:d.m.Y;show:1;visible:1;',
  `form` enum('Физлицо','Юрлицо','ИП') NOT NULL DEFAULT 'Физлицо' COMMENT 'visible:true;label:Правовой статус;filter:list;readonly:1;',
  `user_id` int(11) NOT NULL COMMENT 'label:Пользователь; comment:Пользователь, с которым связаны реквизиты этого физического лица;default:currentUser();filter:like;link:1;',
  `inn` varchar(12) DEFAULT NULL COMMENT 'label:ИНН;visible:1;filter:like;mask:999999999999;',
  `fname` varchar(50) NOT NULL COMMENT 'show:true;visible:true;label:Имя;show:1;filter:like',
  `mname` varchar(50) DEFAULT NULL COMMENT 'show:true;visible:true;label:Отчество;show:1;filter:like',
  `lname` varchar(50) NOT NULL COMMENT 'show:true;visible:true;label:Фамилия;show:1;filter:like',
  `bdate` varchar(50) DEFAULT NULL COMMENT 'label:Дата рождения;filter:range;type:date;format:d.m.Y;show:1;visible:0;',
  `pasport_number` varchar(50) DEFAULT NULL COMMENT 'show:true;visible:true;label:Номер/серия паспорта;show:1;filter:like;mask:@pasport;',
  `pasport_date` timestamp NULL DEFAULT NULL COMMENT 'show:true;visible:0;label:Дата выдачи паспорта;type:date;format:d.m.Ytype:date;format:d.m.Y;',
  `pasport_organ` varchar(100) DEFAULT NULL COMMENT 'show:true;visible:true;label:Орган, выдавший паспорт;show:1;',
  `address` varchar(100) DEFAULT NULL COMMENT 'show:true;visible:true;label:Адрес регистрации;show:1;filter:like;',
  `phone` varchar(15) DEFAULT NULL COMMENT 'show:true;visible:0;label:Телефон;filter:like;',
  `bank_bik` varchar(9) DEFAULT NULL COMMENT 'show:true;visible:0;label:БИК банка;filter:like;mask:999999999;',
  `account` varchar(26) DEFAULT NULL COMMENT 'show:true;visible:0;label:Номер счета;filter:like;mask:@account;',
  `account_owner` varchar(50) DEFAULT NULL COMMENT 'show:true;visible:0;label:Владелец счета;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_id` (`id`),
  KEY `FK_user` (`user_id`),
  KEY `KEY_inn` (`inn`),
  CONSTRAINT `adr_rperson_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `adr_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9271 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Title: Реквизиты физлица;\r\nModel: Rperson;\r\nimp: Реквизиты физлица;\r\nrodp: Реквизиты физлица;\r\nvinp: Реквизиты физлица;\r\nparent:objects;\r\ncaption: %form% (%lname%);\r\nconstants:FORM_PERSON=''Физлицо'',FORM_COMPANY=''Юрлицо'',FORM_IP=''ИП'';';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_seo`
--

DROP TABLE IF EXISTS `adr_seo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_seo` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT 'filter:like;',
  `description` varchar(255) DEFAULT NULL COMMENT 'visible:0;',
  `keywords` varchar(255) DEFAULT NULL COMMENT 'visible:0;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Title: SEO-данные;\r\nModel:Seo;\r\nparent:objects;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_service_names`
--

DROP TABLE IF EXISTS `adr_service_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_service_names` (
  `id` int(11) NOT NULL COMMENT 'title:Код услуги;show:true',
  `code` varchar(9) DEFAULT '00' COMMENT 'label:Код;visible:1;filter:value;mask:99;',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT 'label:Название услуги;Comment: Название услуги,Список некоторых услуг владельцев, Предоставление юридического адреса, Почтовое обслуживание, Предоставление рабочего места;visible:1;filter:like;',
  `stype` enum('Разовая','Ежемесячная') NOT NULL DEFAULT 'Ежемесячная' COMMENT 'label:Тип услуги ;content:Услуга является разовой или ежемесячной;visible:1;filter:list;comment:Является ли услуга разовой или оказывается ежемесячно;',
  `to_address` tinyint(1) DEFAULT NULL COMMENT 'type:boolean;label:Добавлять в адрес;visible:false;;content:Добавлять ли услугу в адрес по умолчанию;readonly:0;comment: Добавлять ли эту услугу автоматически при добавлении нового адреса;type:boolean;',
  `description` text COMMENT 'label:Описание;visible:false;readonly:0;comment: Подробное описание услуги, которое будет показано заказчику;',
  `option_id` int(11) DEFAULT NULL COMMENT 'label:Связанная опция;visible:1;readonly:0;comment: Опция, соответствующая услуге. Будет автоматически выставляться при добавлении услуги. А услуга автоматчиески добавляться при добавлении опции;',
  `undeleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'label:Неудаляемая;comment:Услуга является неудаляемой, ее невозможно удалить из справочника;visible:0;readonly:1;type:boolean;',
  `alias` varchar(45) DEFAULT NULL COMMENT 'label:Псевдоним;comment:Внутренне имя услуги для привязки ее к участию в бизнес-логике, устанавливается разработчиком;show:1;visible:0;readonly:1;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `alias_UNIQUE` (`alias`),
  KEY `FK_service_option` (`option_id`),
  CONSTRAINT `FK_service_option` FOREIGN KEY (`option_id`) REFERENCES `adr_options` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Title: Справочник услуг;\r\nrodp: услуги;\r\nvinp: услугу;\r\nimp: услуга;\r\nmodel: ServiceName;\r\nparent:objects;\r\ncaption:[%code%] %name%;\r\ncomment: Справочник услуг, оказываемых владельцами адресов. Этот справочник меняется только администратором портала. Владельцы адресов могут свофримровать свой справочник услуг в рамках этого справочника.;\r\nconstants:STYPE_ONCE=''Разовая'', STYPE_MONTHLY=''Ежемесячная'';';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_service_portal`
--

DROP TABLE IF EXISTS `adr_service_portal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_service_portal` (
  `id` int(11) NOT NULL COMMENT 'title:Код услуги;show:true',
  `name` varchar(45) NOT NULL COMMENT 'label:Название услуги;Comment: Название услуги, предоставляемой порталом;visible:1;filter:like;',
  `alias` varchar(45) DEFAULT NULL COMMENT 'label:Псевдоним;Comment: Псевдоним услуги латиницей для поиска ее из програмного кода;visible:0;filter:like;',
  `for` enum('владельца','заказчика') NOT NULL DEFAULT 'владельца' COMMENT 'label:Услуга для;visible:1;filter:list;comment:Кому может быть оказана услуга;',
  `price` double DEFAULT NULL COMMENT 'label:Стоимость услуги;visible:1;format:[''decimal'', 2] ;mask:@money;',
  `price_rules` text COMMENT 'label:Правила цены;comment:Правила формирования цены в зависимости от дополнительных парамтеров;visible:0;',
  `undeleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'label:Неудаляемая;comment:Услуга является неудаляемой, этот статус возможно изменить только через разработчика;visible:0;readonly:1;type:boolean;',
  `recipient_id` int(11) DEFAULT NULL COMMENT 'label:Реквизиты;visible:0;comment:Реквизиты получателя оплаты за усугу;readonly:0;link:0;condition:[user_id=>owner_id];',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `alias_UNIQUE` (`alias`),
  KEY `FK_service_portal_recipient` (`recipient_id`),
  CONSTRAINT `FK_service_portal_recipient` FOREIGN KEY (`recipient_id`) REFERENCES `adr_objects` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Title: Справочник услуг портала;\r\nrodp: услуги;\r\nvinp: услугу;\r\nimp: услуга;\r\nmodel: ServicePortal;\r\nparent:objects;\r\ncomment: Справочник услуг, оказываемых порталом как владельцам, так и заказчикам.;\r\nconstants:FOR_OWNER=''владельца'',FOR_CUSTOMER=''заказчика'';';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_service_prices`
--

DROP TABLE IF EXISTS `adr_service_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_service_prices` (
  `id` int(11) NOT NULL,
  `code` varchar(9) DEFAULT NULL COMMENT 'label:Код;visible:1;filter:value;readonly:1;comment:Если услуга системная, то код совпадает с кодом услуги из справочника;',
  `stype` enum('Разовая','Ежемесячная') NOT NULL DEFAULT 'Ежемесячная' COMMENT 'label:Тип услуги ;content:Услуга является разовой или ежемесячной;visible:1;filter:list;comment:Является ли услуга разовой или оказывается ежемесячно. Если услуга системная, то тип совпадает с типом услуги из справочника;',
  `service_name_id` int(11) DEFAULT NULL COMMENT 'label:Системная услуга;visible:0;readonly:0;comment:Если услуга является системной, то выбрать услугу из справочника услуг;link:1;visible:1;filter:list;',
  `name` varchar(50) NOT NULL COMMENT 'label:Название услуги;Comment: Если услуга системная, то название совпадает с названием услуги из общего справочника услуг.;visible:1;filter:like;',
  `alias` varchar(45) DEFAULT NULL COMMENT 'label:Псевдоним;comment:Внутренне имя услуги для привязки ее к участию в бизнес-логике;show:0;readonly:1;',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Дата добавления;filter:range;type:date;format:d.m.Y;visible:0;',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'label:Дата изменения;filter:range;type:date;format:d.m.Y;show:0;',
  `to_address` tinyint(1) unsigned DEFAULT NULL COMMENT 'type:boolean;label:Добавлять в адрес;visible:false;;content:Добавлять ли услугу в адрес по умолчанию;readonly:0;comment: Добавлять ли эту услугу автоматически при добавлении нового адреса;',
  `owner_id` int(11) NOT NULL COMMENT 'label:Владелец; comment:Владелец адреса, определяется адресом;default:currentUser();filter:like;link:1;readonly:1;',
  `recipient_id` int(11) DEFAULT NULL COMMENT 'label:Реквизиты;visible:0;comment:Реквизиты получателя оплаты за усугу;readonly:0;link:0;condition:[user_id=>owner_id];',
  `address_id` int(11) DEFAULT NULL COMMENT 'label:Адрес;comment:Адрес, к которому относится цена услуги;visible:1;filter:like;link:1;condition:[owner_id=>owner_id];',
  `description` text COMMENT 'label:Описание;visible:false;readonly:0;comment: Подробное описание услуги, которое будет показано заказчику;',
  `type` enum('Обычная','Акционная') DEFAULT 'Обычная' COMMENT 'label:Тип цены;comment:Тип цены. Могут быть обычная цена и под различные акции;visible:0;',
  `price` decimal(10,4) DEFAULT NULL COMMENT 'label:Цена, руб;comment:Цена для разовой услуги;visible:1;format:[''decimal'', 2] ;mask:@money;',
  `price6` decimal(10,4) DEFAULT NULL COMMENT 'label: Цена 6, руб;comment:Цена услуги за 6 месяцев;visible:1;format:[''decimal'', 2] ;mask:@money;',
  `price11` decimal(10,4) DEFAULT NULL COMMENT 'label:Цена 11, руб;comment:Цена услуги за 11 месяцев;visible:1;format:[''decimal'', 2] ;mask:@money;',
  `enabled` tinyint(1) DEFAULT '1' COMMENT 'label: Активна;comment:Услуга активна и может быть выбрана для заказа;visible:0;format:boolean;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `price_of_service_name_idx` (`service_name_id`),
  KEY `price_of_owner_idx` (`owner_id`),
  KEY `price_of_address_idx` (`address_id`),
  KEY `alias_idx` (`alias`),
  KEY `FK_price_of_recepient` (`recipient_id`),
  CONSTRAINT `FK_price_of_address` FOREIGN KEY (`address_id`) REFERENCES `adr_addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_price_of_owner` FOREIGN KEY (`owner_id`) REFERENCES `adr_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_price_of_recepient` FOREIGN KEY (`recipient_id`) REFERENCES `adr_objects` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_service_recipient` FOREIGN KEY (`service_name_id`) REFERENCES `adr_service_names` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='title:Прайс-лист на услуги;\r\nrodp: услуги ;\r\nvinp: услугу;\r\nimp: услуга;\r\nModel:ServicePrice;\r\nparent:objects;\r\ncaption:[%code%] %name%;\r\ncomment: Список всех цен на все услуги на портале. Список можно фильтровать по адресам и владельцам.;\r\nconstants:STYPE_ONCE=''Разовая'', STYPE_MONTHLY=''Ежемесячная'',PTYPE_NORMAL=''Обычная'',PTYPE_ACTION=''Акционная'';\r\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_services`
--

DROP TABLE IF EXISTS `adr_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_services` (
  `id` int(11) NOT NULL,
  `code` varchar(9) DEFAULT NULL COMMENT 'label:Код;visible:1;filter:value;mask:99999;readonly:1;',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Дата;filter:range;type:date;format:d.m.Y;show:1;visible:1;comment: Дата добавления услуги в заказ;',
  `service_price_id` int(11) NOT NULL COMMENT 'label:Услуга;visible:1;link:1;comment:Заказанная услуга;readonly:1;',
  `order_id` int(11) DEFAULT NULL COMMENT 'label:Заказ;filter:like;visible:1;comment:Заказ, в рамках которого была заказана услуга;link:1;readonly:1;',
  `date_start` timestamp NULL DEFAULT NULL COMMENT 'label:Старт;comment:Дата начала оказания услуги;filter:range;type:date;format:d.m.Y;show:1;visible:1;',
  `date_fin` timestamp NULL DEFAULT NULL COMMENT 'label:Завершение;comment:Дата завершения услуги. Определяется датой старта и длительностью услуги.;filter:range;type:date;format:d.m.Y;show:1;visible:1;readonly:1;',
  `address_id` int(11) DEFAULT NULL COMMENT 'label:Адрес;visible:1;filter:like;link:1;comment: Адрес, к которому относится услуга, определяется заказом.;readonly:1;',
  `owner_id` int(11) DEFAULT NULL COMMENT 'label:Владалец;visible:1;filter:like;show:1;comment:Владелец адреса, для которого заказана услуга. Определяется адресом.;link:1;readonly:1;',
  `name` varchar(50) NOT NULL COMMENT 'label:Название услуги;Comment: Название услуги,Список некоторых услуг владельцев, Предоставление юридического адреса, Почтовое обслуживание, Предоставление рабочего места;visible:1;filter:like;readonly:1;show:0;',
  `stype` enum('Разовая','Ежемесячная') DEFAULT NULL COMMENT 'label:Тип услуги ;content:Услуга является разовой или ежемесячной;visible:1;filter:list;comment:Является ли услуга разовой или оказывается ежемесячно, определяется справочником услуг;readonly:1;',
  `duration` enum('разово','6 месяцев','11 месяцев') DEFAULT NULL COMMENT 'label:Длительность;comment:Длительность услуг заказа в месяцах, определяется заказом;visible:1;filter:list;readonly:1;',
  `status_id` int(11) DEFAULT NULL COMMENT 'label:Статус;show:0;',
  `price` decimal(10,4) DEFAULT NULL COMMENT 'label:Стоимость, руб;comment:Полная стоимость оказания услуги;visible:1;format:[''decimal'', 2] ;readonly:1;mask:@,money;',
  `summa` decimal(10,4) DEFAULT NULL COMMENT 'label:Стоимость, руб;comment:Полная стоимость оказания услуги;visible:1;format:[''decimal'', 2] ;readonly:1;mask:@,money;',
  `invoice_id` int(11) DEFAULT NULL COMMENT 'label:Счет;visible:0;comment:Счет, выставленный для оплаты заказанной услуги;link:0;readonly:1;',
  `nds` int(3) NOT NULL DEFAULT '18' COMMENT 'label:НДС;visible:0;comment:Ставка НДС для данной услуги в %;readonly:0;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `service_order_idx` (`order_id`),
  KEY `service_status_idx` (`status_id`),
  KEY `service_price_idx` (`service_price_id`),
  KEY `services_address_idx` (`address_id`),
  KEY `FK_services_owner` (`owner_id`),
  KEY `FK_adr_services_adr_documents` (`invoice_id`),
  CONSTRAINT `FK_adr_services_adr_documents` FOREIGN KEY (`invoice_id`) REFERENCES `adr_documents` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_services_address` FOREIGN KEY (`address_id`) REFERENCES `adr_addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_services_owner` FOREIGN KEY (`owner_id`) REFERENCES `adr_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_service_order` FOREIGN KEY (`order_id`) REFERENCES `adr_orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_service_price` FOREIGN KEY (`service_price_id`) REFERENCES `adr_service_prices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_service_status` FOREIGN KEY (`status_id`) REFERENCES `adr_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='title: Заказанные услуги;\r\nparent:objects;\r\nModel:Service;\r\ncomment: Услуги, находящиеся в заказах\r\ncapation: %name%;\r\nconstants:STYPE_ONCE=''Разовая'', STYPE_MONTHLY=''Ежемесячная'',DURATION_ONCE=''разово'',DURATION_6=''6 месяцев'',DURATION_11=''11 месяцев'';';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_similar_addresses`
--

DROP TABLE IF EXISTS `adr_similar_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_similar_addresses` (
  `id` int(11) NOT NULL,
  `address_from` int(11) DEFAULT NULL,
  `address_to` int(11) DEFAULT NULL,
  `weight` double DEFAULT NULL COMMENT 'вес связи',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `address_from_idx` (`address_from`),
  KEY `address_to_idx` (`address_to`),
  CONSTRAINT `address_from_adress` FOREIGN KEY (`address_from`) REFERENCES `adr_addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `address_to_address` FOREIGN KEY (`address_to`) REFERENCES `adr_addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_social_account`
--

DROP TABLE IF EXISTS `adr_social_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL DEFAULT '0',
  `provider` varchar(50) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `data` text,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fx_account_of_user` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_status`
--

DROP TABLE IF EXISTS `adr_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_status` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `alias` varchar(45) DEFAULT NULL COMMENT 'label:Псевдоним;comment:Внутренне имя услуги для привязки ее к участию в бизнес-логике;show:0;readonly:1;',
  `table` varchar(50) DEFAULT NULL,
  `isdefault` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `names_idx` (`name`,`table`),
  KEY `alias_idx` (`isdefault`),
  KEY `status_idx` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='title: Статусы;\r\nModel:Status;\r\nparent:objects;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_status_changes`
--

DROP TABLE IF EXISTS `adr_status_changes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_status_changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'comment: Дата изменения',
  `object_id` int(11) DEFAULT NULL COMMENT 'comment: объект, у которого был изменен статус',
  `status_old` int(11) DEFAULT NULL COMMENT 'comment: Старый статус',
  `status` int(11) DEFAULT NULL COMMENT 'comment: Новый статус',
  `actor_id` int(11) DEFAULT NULL COMMENT 'comment: Кто изменил статус',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `status_order_idx` (`object_id`),
  KEY `status_actor_idx` (`actor_id`),
  KEY `change_from` (`status_old`),
  KEY `change_to` (`status`),
  KEY `date_idx` (`date`),
  CONSTRAINT `change_from` FOREIGN KEY (`status_old`) REFERENCES `adr_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `change_to` FOREIGN KEY (`status`) REFERENCES `adr_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `status_actor` FOREIGN KEY (`actor_id`) REFERENCES `adr_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2006 DEFAULT CHARSET=utf8 COMMENT='title:Изменения статусов;\r\nmodel:StatusChange;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_status_links`
--

DROP TABLE IF EXISTS `adr_status_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_status_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) NOT NULL DEFAULT '0',
  `to_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `status_from` (`from_id`),
  KEY `status_to` (`to_id`),
  CONSTRAINT `status_from` FOREIGN KEY (`from_id`) REFERENCES `adr_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `status_to` FOREIGN KEY (`to_id`) REFERENCES `adr_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1466 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_topics`
--

DROP TABLE IF EXISTS `adr_topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_topics` (
  `id` int(11) NOT NULL,
  `h1` varchar(255) NOT NULL,
  `text` text,
  `slug1` varchar(50) DEFAULT NULL,
  `slug2` varchar(50) DEFAULT NULL,
  `seo_id` int(11) DEFAULT NULL COMMENT 'label:Seo-данные;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `FK_topics_seo` (`seo_id`),
  CONSTRAINT `FK_topics_seo` FOREIGN KEY (`seo_id`) REFERENCES `adr_seo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Title: Разделы для статей;\r\nModel:Topic;\r\nparent:objects;';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_users`
--

DROP TABLE IF EXISTS `adr_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_users` (
  `id` int(11) NOT NULL COMMENT 'sbow:true;visible:true;filter:value;',
  `fullname` varchar(255) DEFAULT NULL COMMENT 'show:true;visible:true;label:ФИО;show:1;filter:like',
  `username` varchar(45) NOT NULL COMMENT 'label: Логин;show:1;filter:like;',
  `role` set('Администратор','Заказчик','Владелец') DEFAULT NULL COMMENT 'show:true;visible:true;label:Тип;filter:list;format:checkboxlist;',
  `status_id` int(11) DEFAULT '0' COMMENT 'label:Состояние;format:status;filter:list;',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'label:Дата добавления;filter:range;type:date;format:d.m.Y;show:1;visible:1;',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'label:Дата изменения;show:false;format:date;',
  `status_changed_at` timestamp NULL DEFAULT NULL COMMENT 'label:Дата изменения статуса;show:0;comment:Дата последнего изменения статуса. Выставляется автоматически;',
  `registration_ip` varchar(45) DEFAULT NULL COMMENT 'show:false;',
  `password` varchar(45) DEFAULT NULL COMMENT 'visible:0;label: Пароль;',
  `password_hash` varchar(100) DEFAULT NULL COMMENT 'show:false',
  `password_reset_token` varchar(45) DEFAULT NULL COMMENT 'show:false',
  `auth_key` varchar(45) NOT NULL COMMENT 'show:false',
  `psw_recovery_code` varchar(32) DEFAULT NULL COMMENT 'show:false;label:код восстановления пароля',
  `email` varchar(45) DEFAULT NULL COMMENT 'show:1;label:Email;format:email;filter:like;mask:@email;',
  `phone` varchar(45) DEFAULT NULL COMMENT 'label:Телефон;show:1;filter:like;mask:@phone;',
  `role_admin` tinyint(4) DEFAULT NULL COMMENT 'show:false;comment:является админитратором портала',
  `notifications` varchar(45) DEFAULT NULL COMMENT 'label:Уведомления;show:false;comment:Информация о включенных/выключенных уведомлениях',
  `form_mode` enum('Автоматический','Ручной') DEFAULT 'Автоматический' COMMENT 'label:Перевод в Формирование;show:false;comment:автоматический/ручной перевод заказов в статус Формирование',
  `date_visit` datetime DEFAULT NULL COMMENT 'show:false;label:Последний визит;visible:false;comment:Время последнего визита пользователя;type:date;format:d.m.Y;',
  `used` tinyint(4) DEFAULT NULL COMMENT 'show:false;comment:Флаг того, что пользователь задействован в финансовых операциях, то  есть если флаг установлен, то изменение его платежных реквизитов оказывается невозможным. Для их изменения требуется создание нового пользователя',
  `new_id` int(11) DEFAULT NULL COMMENT 'show:false;comment:Ссылка на новго пользователя, если были изменены реквизиты',
  `auth` varchar(45) DEFAULT NULL COMMENT 'show:false',
  `psw_recovery_date` datetime DEFAULT NULL COMMENT 'show:false;Дата начала восстановления пароля',
  `_status_` tinyint(4) DEFAULT NULL COMMENT 'show:false;',
  `notification_email` varchar(100) DEFAULT NULL COMMENT 'show:0;',
  `notification_enabled` enum('yes','no') NOT NULL DEFAULT 'yes' COMMENT 'show:0;',
  `default_profile_id` int(11) DEFAULT NULL COMMENT 'label:Основной профиль;show:0;',
  `options` text COMMENT 'label:Опции;type:json;show:0;',
  `offerta_confirm` tinyint(1) DEFAULT NULL COMMENT 'label:Подтверждение офферты; visible:0; type:boolean;',
  `delivery_address` text COMMENT 'label:Адрес доставки; comments: Адрес доставки документов на адреса;type:json;show:0;',
  `info` text COMMENT 'visible:0;label:Информация;visible:false;comment:Дополнительная информация о пользователе',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`username`),
  KEY `user_to_new_idx` (`new_id`),
  KEY `psw_recovery_code_idx` (`psw_recovery_code`),
  KEY `user_status` (`status_id`),
  CONSTRAINT `user_status` FOREIGN KEY (`status_id`) REFERENCES `adr_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_to_new` FOREIGN KEY (`new_id`) REFERENCES `adr_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Title: Пользователи;\r\nModel:User;\r\nimp:Пользователь;\r\nrodp:Пользователя;\r\nvinp:Пользователя;\r\ncaption:[%id%] %fullname% (%username%);\r\nparent:objects;\r\nconstants:ROLE_ADMIN=''Администратор'', ROLE_CUSTOMER=''Заказчик'',ROLE_OWNER=''Владелец'', FM_AUTO=''Автоматический'',FM_MANUL=''Ручной'';';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adr_widget`
--

DROP TABLE IF EXISTS `adr_widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adr_widget` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL COMMENT 'Заголовок виджета',
  `panel` int(11) DEFAULT NULL COMMENT 'панель, на которой размещен виджет. 0 - нигде',
  `position` int(11) DEFAULT NULL COMMENT 'положение внутри панели',
  `template` varchar(150) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `art_users1`
--

DROP TABLE IF EXISTS `art_users1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `art_users1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `_status_` tinyint(4) NOT NULL DEFAULT '0',
  `password_hash` varchar(100) NOT NULL,
  `auth_key` varchar(45) NOT NULL,
  `psw_recovery_code` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `confirmed_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unconfirmed_email` tinyint(4) NOT NULL DEFAULT '0',
  `blocked_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(100) NOT NULL,
  `stage` tinyint(4) DEFAULT '0',
  `point_id` int(11) DEFAULT NULL,
  `about` mediumtext,
  `registration_ip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='class: User\r\nparent:objects';
/*!40101 SET character_set_client = @saved_cs_client */;

-- Dump completed on 2018-05-13 10:25:45
