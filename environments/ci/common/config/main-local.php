<?php
error_reporting(E_ALL ^ E_DEPRECATED);
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=portal_ci',
            'username' => 'ci_portal',
            'password' => '@Mysql2go',
            'charset' => 'utf8',
            'tablePrefix' => 'adr_'
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => '127.0.0.1',
                'username' => 'dev@test.org',
                'password' => 'password',
                'port' => '1025',
                'encryption' => '',
            ],
        ],
        'cache'         => [
            'servers' => [
                [
                    'host' => '127.0.0.1',
                    'port' => 11211,
                    'weight' => 60,
                ],
            ],
            'class'        => 'common\models\MemCache',
            'listDuration' => 600,
            'id' => 'ci_test',						// уникальный для каждой копии системы на одном сервере MemCache
        ],
    ],
];
