<?php
/*
Контроллер StorageController задуман для единого доступа к изображениям. 
В дальнейшем возможно экстраполировать его и на все файлы.
Простое наследование в frontend или backend этого контроллера дает доступ к единому хранилищу изображений
Изображения становятся доступны по URL типа /storage/icons/options/key.png , где icons - имя хранилища, options - имя подборки, key.png - имя файла
Поддерживаются изображения с типов: jpg (jpeg), png, gif
Если вместо имени файла поставить  list, то вернется список всех изображения подборки соответствюущего хранилища в формати JSON, например, /storage/icons/options/list

Место хранения изображений устанавливается в конфигурации params.php - Yii::$app -> params['storage']['path'])
Для картинок отдельная директория и action - actionImages, в который надо передать имя набора изображений и имя файла.
Для иконок отдельная директория и action - actionIcons, в который надо передать имя набора иконок и имя файла.
на текущий момент набор совпадает с именем класса, в котором используются картинки или иконки, например, в наборе addresse хранятся картинки адресов, а в Option - иконки для опций.
*/
namespace common\controllers;

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\FileHelper;



/**
 * AddressController implements the CRUD actions for Address model.
 */
class FilesController extends Controller {

	public function actionIndex() {

	}

// Возвращает изображение из указанного файла $fileName
	protected  function getImage($fileName) {
		$contentType = false;
		$ext = strtolower(strrchr($fileName, '.'));
		switch ($ext){
			case ".jpg":
			case ".jpeg":
				$contentType = 'image/jpeg';
				break;

			case ".png":
				$contentType = 'image/png';
				break;

			case ".gif":
				$contentType = 'image/gif';
				break;
		};
		if ($contentType) {
			Yii::$app->response->format = yii\web\Response::FORMAT_RAW;
			Yii::$app->response->headers->add('content-type', $contentType);
			Yii::$app->response->data = file_get_contents($fileName);
			return \Yii::$app->response;
		} else NotFoundHttpException('Формат изображения не поддерживается');

	}



	protected  function getDocument($fileName) {
		$contentType = false;
		
		$ext = strtolower(strrchr($fileName, '.'));
		switch ($ext){
			case ".pdf":
				$contentType = 'application/pdf';
				break;
				
			default: {
				$contentType = FileHelper::getMimeType($fileName);
				break;
			}
			
		};
		if ($contentType) {
			Yii::$app->response->format = yii\web\Response::FORMAT_RAW;
			Yii::$app->response->headers->add('content-type', $contentType);
			Yii::$app->response->data = file_get_contents($fileName);
			return \Yii::$app->response;
		} else NotFoundHttpException('Формат документа не поддерживается');

	}


// Хранилище изображений. $name - имя хранилища, $file = имя файла, вернет изображение. Если $file = 'list', то вернет список всех изображеий в хранилище
	public function actionImages($name, $file) {
		//$path = Yii::getAlias(Yii::$app -> params['storage']['path']).'images/'.($name?$name.'/':'');
		$path = self::getPath('images', $name);
		if ($file == 'list') {
			return json_encode($this -> getFileList($path), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);;
		} else {

			$fileName = $path.$file;
			return $this -> getImage($fileName);
		}
	}


// Хранилище изображений. $name - имя хранилища, $file = имя файла . Если $file = 'list', то вернет список всех иконок в хранилище
	public function actionIcons($name, $file) {
		//$path = Yii::getAlias(Yii::$app -> params['storage']['path']).'icons/'.($name?$name.'/':'');
		$path = self::getPath('icons', $name);
		if ($file == 'list') {
			return json_encode($this -> getFileList($path), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);;
		} else {

			$fileName = $path.$file;
			return $this -> getImage($fileName);
		}
	}

// Хранилище документов. $name - имя хранилища, $file = имя файла . Если $file = 'list', то вернет список всех документов в хранилище
	public function actionDocuments($name, $file) {

		$path = self::getPath('documents', $name);
		if ($file == 'list') {
			return json_encode($this -> getFileList($path), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);;
		} else {
			$fileName = $path.$file;
			return $this -> getDocument($fileName);
		}
	}


// возвращает путь к хранилищу
	public static function getPath($type, $name) {
		return Yii::getAlias(Yii::$app -> params['storage']['path']).$type.'/'.($name?$name.'/':'');
	}



// Список всех картинок в указанном наборе $name
	public static function getImageList($name) {
		//$path = Yii::getAlias(Yii::$app -> params['storage']['path']).'image/'.($name?$name.'/':'');
		$path = self::getPath('image', $name);
		return self::getFileList($path);
	}

// Список всех иконок в указанном наборе $name
	public static function getIconList($name) {
		//$path = Yii::getAlias(Yii::$app -> params['storage']['path']).'icons/'.($name?$name.'/':'');
		$path = self::getPath('icons', $name);
		return self::getFileList($path);
	}

	public static function getDocumentList($name) {
		//$path = Yii::getAlias(Yii::$app -> params['storage']['path']).'documents/'.($name?$name.'/':'');
		$path = self::getPath('documents', $name);
		return self::getFileList($path);
	}


	public static function getFileList($path) {
		$list = scandir($path);
		$result = [];
		foreach($list as $fn){
			if (($fn == '.')||($fn == '..')) continue;
			$result[] = $fn;
		}
		return $result;
	}

	// удалить файл из хранилища
	public static function removeFile($type, $name, $file){
		$fileName = self::getPath($type, $name).$file;
		
		if (file_exists($fileName)) {
			unlink($fileName);
		}
	}
	
	public static function uploadFiles($fieldName) {
		$path = self::getPath('documents', 'uploaded');
		if (!file_exists($path)) {
			mkdir ($path, 0775, true);
		}
		$result = [];
		$uploaded = UploadedFile::getInstancesByName($fieldName);
		foreach($uploaded as $file) {
			$ext = $file->extension;
			$filename = md5(uniqid(rand(), true)).'.'.$ext;
			$file -> saveAs($path.$filename);			
			$result[$file -> name] = ['filename' => $filename, 'error' => $file -> error];			
		}
		return $result;
	}

}
