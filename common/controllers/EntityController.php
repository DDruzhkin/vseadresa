<?php

namespace common\controllers;

use Yii;

use common\models\Entity;
use yii\web\Controller;
use yii\helpers\Url;

class EntityController extends Controller {


    /**
     * Finds the  model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Application Model the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Entity::byId($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая модель не найдена.');
        }
    }

    /**
     * Проверить существование представления
     **/

    protected function checkView($view) {
        $file = Yii::$app->getViewPath() . DIRECTORY_SEPARATOR .$this -> id .DIRECTORY_SEPARATOR . ltrim($view, '/').'.php';
        return file_exists($file);
    }

	/**
     * Вернуться на предыдущую страницу
	 * Если referrer не определен, то перенаправляется на $default
	 * Если и $default не определен, то на homeUrl
     **/
	protected function toBack($default = false) {
		return $this->redirect(Yii::$app->request->referrer ?: ($default?$default:Yii::$app->homeUrl));
	}
	
}
