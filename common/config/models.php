<?php

return [

	'Address' => [
		'blinks' => [
			['label' => 'Документы Адреса', 'url' => ['/address/files/%id%']],			
			['label' => 'Заказы', 'url' => ['/order?OrderSearch[address_id]=%id%']],
			['label' => 'Услуги', 'url' => ['/service-price?ServicePriceSearch[address_id]=%address%']],			
			
		],
		'listViewMenu' => [
		
		//	'<li class="divider"></li>',
			['label' => 'Изменить', 'url' => ['address/update/%id%']],									
		],
	],
	
	'User' => [
		'blinks' => [
			['label' => 'Реквизиты', 'url' => ['/requisites/%id%']],
			'<li class="divider"></li>',
			['label' => 'Заказы', 'url' => ['/order?OrderSearch[customer_id]=[%id%]']],
			['label' => 'Адреса', 'url' => ['/address?AddressSearch[owner_id]=[%id%]']],
			['label' => 'Услуги', 'url' => ['/service-price?ServicePriceSearch[owner_id]=[%id%]']],			
			
		],
		'listViewMenu' => [			
			['label' => 'Изменить', 'url' => ['/user/update/%id%']],									
		],
	],

	'Okrug' => [
		'blinks' => [
			['label' => 'Адреса', 'url' => ['/address?AddressSearch[okrug_id]=%id%']],
			['label' => 'Налоговые инспекции', 'url' => ['/nalog?NalogSearch[okrug_id]=%id%']],
			['label' => 'Районы', 'url' => ['/region?RegionSearch[okrug_id]=%id%']],
		],
		'listViewMenu' => [					
			['label' => 'Изменить', 'url' => ['/okrug/update/%id%']],									
		],
	],
	
	'Region' => [
		'blinks' => [
			['label' => 'Адреса', 'url' => ['/address?AddressSearch[region_id]=%id%']],			
		],
		'listViewMenu' => [
			['label' => 'Изменить', 'url' => ['/region/update/%id%']],									
		],
	],
	
	'Metro' => [
		'blinks' => [
			['label' => 'Адреса', 'url' => ['/address?AddressSearch[metro_id]=%id%']],			
		],
		'listViewMenu' => [
					
			['label' => 'Изменить', 'url' => ['/metro/update/%id%']],									
		],
	],
	
	
	'Order' => [
		'blinks' => [
			['label' => 'Адреса', 'url' => ['/address?AddressSearch[metro_id]=%id%']],			
		],
		'listViewMenu' => [				
			['label' => 'Изменить', 'url' => ['/order/update/%id%']],									
		],
	],
	
	
	'Nalog' => [
		'blinks' => [
			['label' => 'Адреса', 'url' => ['/address?AddressSearch[nalog_id]=%id%']],			
		],
		'listViewMenu' => [				
			['label' => 'Изменить', 'url' => ['/nalog/update/%id%']],									
		],
	],
	
	'Rip' => [
		'listViewMenu' => [				
			['label' => 'Изменить', 'url' => ['/rip/update/%id%']],									
		],
	],
	
	'Rperson' => [
		'listViewMenu' => [				
			['label' => 'Изменить', 'url' => ['/rperson/update/%id%']],									
		],
	],
	'Rcompany' => [
		'listViewMenu' => [				
			['label' => 'Изменить', 'url' => ['/rcompany/update/%id%']],									
		],
	],
	
	'ServiceName' => [
		'blinks' => [
			['label' => 'Цены', 'url' => ['/service-price?ServicePriceSearch[service_name_id]=%id%']],			
		],
		'listViewMenu' => [				
			['label' => 'Изменить', 'url' => ['/service-name/update/%id%']],									
		],
	],
	'ServicePrice' => [
		'listViewMenu' => [				
			['label' => 'Изменить', 'url' => ['/service-price/update/%id%']],									
		],
	],
	
	'Service' => [
		'listViewMenu' => [				
			['label' => 'Изменить', 'url' => ['/service/update/%id%']],									
		],
	],
	
	'Document' => [
		'blinks' => [
		
		],
		'listViewMenu' => [					
			['label' => 'Изменить', 'url' => ['/document/update/%id%']],									
		],
	],
	
	
	
	
]?>