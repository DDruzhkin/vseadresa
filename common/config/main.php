<?php
use kartik\mpdf\Pdf;

return [
    'name'=>'VseAdresa.pro',

    'aliases' => [
        '@storage' => '@common/storage',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => ['log','gii'],
    'language' => 'ru',
    'components' => [
		'eventManager' => [
			'class' => 'common\models\EventManagerExt',
		],
	
        'notifier' =>  [
            'class' => 'common\models\NotificationManagerExt',
        ],

        'pdf' => [
            'class' => Pdf::classname(),
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            // refer settings section for all configuration options
        ],

        'mailer' => [

            'class' => 'yii\swiftmailer\Mailer',
            'htmlLayout' => 'layout-html.php',
            'textLayout' => 'layout-text.php',
            'useFileTransport' => false,
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'en',
                    'fileMap' => [
                        'app' => 'app.php',
                    ]
                ],
            ],
        ],


        'assetManager' => [
            'forceCopy' => true,
        ],

        'cache'         => [
            'class'        => 'common\models\MemCache',
            'useMemcached' => true,
            'servers' => [
                [
                    'host' => 'localhost',
                    'port' => 11211,
                ],
            ],
        ],

        'image' => [
            'class' => 'yii\image\ImageDriver',
            'driver' => 'GD',  //GD or Imagick
        ],

        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:d.m.Y',
            'datetimeFormat' => 'php:d.m.Y H:i',
            'timeFormat' => 'php:H:i:s',
            'locale' => 'ru-RU',

        ],
        'baseUrl' => '',

        'log' => [
            'flushInterval' => 1,
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['entity'],
                    'exportInterval' => 1,
                    'levels' => ['info'],
                    'logFile' => '/var/www/portal/mgribov/log.txt',
                    'logVars' => []
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
			'showScriptName' => false,
            'rules' => [

                //	'GET <controller:[\w_\-]>' => '<controller>/index',



                'help' => 'site/help',
                'help/<modelname:[\w+_\-]>' => 'site/help',

                'options' => 'user-portal-options/',



                'GET requisites/<id:\w*>' => 'requisites',
                'GET user/default-profile/<id:\d+>/<rid:\d+>' => 'user/default-profile',
                'GET <controller:[\w_\-]+>/view/<id:\d+>' => '<controller>/view',
                'GET <controller:[\w_\-]+>/update/<id:\d+>' => '<controller>/update',
                'POST <controller:[\w_\-]+>/update/<id:\d+>' => '<controller>/update',
        




                'GET files/<action:[\w_\-]+>/<name:[\w_\-]+>/<file:[._\sA-Za-z-0-9-]+>' => 'files/<action>',

//			'GET <controller:\w+>/update/<id:\d+>/<tab:\w+>' => '<controller>/update',
//			'POST <controller:\w+>/update/<id:\d+>/<tab:\w+>' => '<controller>/update',



#			'/' => 'site/index',
#	                'about' => 'site/about',
                'contact' => 'site/contact',
                #              	'login' => 'site/login',
            ],
        ],
    ],
    'modules' => [

        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],

        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['127.0.0.1', '87.240.15.21', '87.240.15.125', '79.165.233.242','213.87.149.24', '213.87.134.138','213.87.158.253', '213.87.156.70', '79.165.147.42', '79.165.149.65', '79.165.233.81'],
            'generators' => [ //here
                'crud' => [ // generator name
                    'class' => 'common\gii\addresses\crud\Generator', // generator class
                    'templates' => [ //setting for out templates
                        'defRus' => '@common/gii/addresses/crud/default', //
                        'Options' => '@common/gii/addresses/crud/options', // for options
                    ]
                ],
                'model' => [
                    'class' => 'common\gii\addresses\model\Generator',
                    'templates' => [
                        'defRus' => '@common/gii/addresses/model/default',
                    ]
                ]
            ],

        ],
    ],





];
