<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\gii\addresses;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Schema;
use yii\db\TableSchema;
use yii\gii\CodeFile;
use yii\helpers\Inflector;
use yii\helpers\BaseJson;
use yii\base\NotSupportedException;

/**
 * This generator will generate one or multiple ActiveRecord classes for the specified database table.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Generator extends \yii\gii\Generator
{

public $linkedAttrs = false; // список полей, у которых надо сделать ссылку
public $manyToManyAttrs = false; // список полей типа manyToMany

public $db = 'db';
public $tableCustomMetaData;
public $tableCaptionInfo;
public $tableLinks;
public $columnMetaData;
protected $uses; // Список в use;
protected $defaultUses = [];

protected $classNames;


// Возвращает возможные варианты фильтров по типу поля
public function enabledFilterByType($type = '') {
	$data  = [
		'integer' => ['value'],
		'smallint' => ['value'],				
		'date' => ['range'],		
		'string' => ['value', 'list', 'like'],
		'text' => ['like'],
		'set' => ['list'],
		'enum' => ['list'],
		'ref' => ['list'],
		'' => []
	];
	
	return array_key_exists($type, $data)?$data[$type]:$data[''];		
		
}


// допустимые типы
public function enabledType() {
	$data  = ['integer', 'smallint', 'double', 'date', 'datetime', 'time', 'string', 'text', 'set', 'enum', 'ref', 'boolean'];	
	return $data;		
}


/* Получить список возможных значений поля для типов Enum и Set */
/*
public  function getEnumValues($tableName, $fieldName) {
	$val = Yii::$app->db->createCommand(
			"SHOW COLUMNS FROM ".$tableName." WHERE Field = '".$fieldName."'"
		) -> queryOne()['Type'];

    preg_match("/^(enum|set)\(\'(.*)\'\)$/", $val, $matches);
    $list = explode("','", $matches[2]);
	
	return $list;
}
	
	*/
	

public function clearUse($className) {
	$this -> uses[$className] = $this -> defaultUses;
}

public function addUse($className, $value)
{ // Добавить путь в use
	$value = ltrim(trim($value),'\\');

	if ($value > '') 
	{
		if (!$this -> uses ) $this -> uses = [];		
		if (array_key_exists($className, $this -> uses)) $arr = $this -> uses[$className]; else $arr = $this -> defaultUses;	
		if (!in_array($value, $arr)) 
		{
			$arr[] = $value;
			$this -> uses[$className] = $arr;
		}
	}
	return $arr;
}


public function generateUses($className){
	$result = null;
	if (isset($this -> uses[$className])) 
	{
		sort($this -> uses[$className]);
		foreach($this -> uses[$className] as $use) 
		{
			$result = $result.'use '.$use.";\n";		
		}
	}

	return $result;
}


public function getName()
{
	return 'Undefined Gernerator';
}	

public function generate()
{
}


    protected function getDbConnection()
    {
        return Yii::$app->get($this->db, false);
    }



    /**
     * @return string[] all db schema names or an array with a single empty string
     * @throws NotSupportedException
     * @since 2.0.5
     */
    protected function getSchemaNames()
    {
        $db = $this->getDbConnection();
        $schema = $db->getSchema();
        if ($schema->hasMethod('getSchemaNames')) { // keep BC to Yii versions < 2.0.4
            try {
                $schemaNames = $schema->getSchemaNames();
            } catch (NotSupportedException $e) {
                // schema names are not supported by schema
            }
        }
        if (!isset($schemaNames)) {
            if (($pos = strpos($this->tableName, '.')) !== false) {
                $schemaNames = [substr($this->tableName, 0, $pos)];
            } else {
                $schemaNames = [''];
            }
        }
        return $schemaNames;
    }



    /**
     * Generates a class name from the specified table name.
     * @param string $tableName the table name (which may contain schema prefix)
     * @param boolean $useSchemaName should schema name be included in the class name, if present
     * @return string the generated class name
     */
    public function generateClassName($tableName, $useSchemaName = null)
    {
        if (isset($this->classNames[$tableName]) && ($this->classNames[$tableName] > '')) {
            return $this->classNames[$tableName];
        }

        $schemaName = '';
        $fullTableName = $tableName;
        if (($pos = strrpos($tableName, '.')) !== false) {
            if (($useSchemaName === null && $this->useSchemaName) || $useSchemaName) {
                $schemaName = substr($tableName, 0, $pos) . '_';
            }
            $tableName = substr($tableName, $pos + 1);
        }

	$modelName = $this -> getTableCustomMetaData($tableName, 'model');
	if (!$modelName) {
	        $db = $this->getDbConnection();
        	$patterns = [];
	        $patterns[] = "/^{$db->tablePrefix}(.*?)$/";
        	$patterns[] = "/^(.*?){$db->tablePrefix}$/";
	        if (strpos($this->tableName, '*') !== false) {
        	    $pattern = $this->tableName;
	            if (($pos = strrpos($pattern, '.')) !== false) {
                	$pattern = substr($pattern, $pos + 1);
        	    }
	            $patterns[] = '/^' . str_replace('*', '(\w+)', $pattern) . '$/';
        	}
	        $className = $tableName;
        	foreach ($patterns as $pattern) {
	            if (preg_match($pattern, $tableName, $matches)) {
                	$className = $matches[1];
        	        break;
	            }
        	}
		$modelName= Inflector::id2camel($schemaName.$className, '_');
	}
        return $this->classNames[$fullTableName] = $modelName;
    }


/* Парсинг данных из комментария в массив */
public static function commentToMetaData($text) {
	$data = array();
		$lines = explode (";",$text);
		foreach($lines as $line) {
			$arr = explode (":",$line);
			if (count($arr) > 1) {
			        $name = $arr[0];
				unset($arr[0]); 
				$value = implode(':', $arr);
				$data[strtolower(trim(strtolower($name)))] = trim($value);
			}
		}
	return $data;

}




/* Возвращает метаданные колонок для таблицы укащанного класса */
public function getColumnMetaDataByClass($className) {
	$tableName = $className::tableName();
	return $generator->getColumnMetaData($tableName);
}

/* Возвращает метаданные колонок для указанной таблицы  */
public function getColumnMetaData($table) {

	$this -> manyToManyAttrs = false;
	$this -> linkedAttrs = false;

	if (false && $this -> columnMetaData) {
		$result = $this -> columnMetaData;
	} else { 
	
		// Вытащим ссылки
		$refs = [];
		$fkeys = $table -> foreignKeys;
		foreach($fkeys as $name => $value) {
			$tableName = $value['0'];
			foreach($value as $name1 => $value1) {					
					if (($name1 != '0')){
						$refs[$name1] = $tableName;
					}
			}			
		}
	
	
	foreach ($table->columns as $column) {
		
		$data = false;
		$label = false;
		$colName = $column->name;
		if (!empty($column->comment)) {
			$data = $this->commentToMetaData($column->comment);		
			if (array_key_exists('label', $data)) $label = $data['label'];

		};
		// Если label столбца не указан...
		if (!$label) {
			if (!strcasecmp($colName, 'id')) {
				$label = 'ID';
			} else {
                $label = Inflector::camel2words($colName);
	            if (!empty($label) && substr_compare($label, ' id', -3, 3, true) === 0) {
					$label = substr($label, 0, -3) . ' ID';
				}
			}
		}
		
		$data['label'] = $label;
		
		
		$def = $column -> defaultValue;
		if ($def !== NULL ) $def = BaseJson::encode($def);
		if ($def !== NULL)  $data['default'] = $def;
		
		// Если readonly не указан, то по умолчанию для ID, created_at, updated_at - 1, иначе - 0
		if (!array_key_exists('readonly', $data)) {			
			if ((strtolower($colName) == 'id' ) 
				||(strtolower($colName) == 'created_at' ) 
				||(strtolower($colName) == 'updated_at' ) 
				)
				$data['readonly'] = '1';
			
		}

		
		// Нормализуем поле type - он может быть перекрыт мета-данными, если это не ссылка
		if (array_key_exists($colName, $refs)) {
			$type = 'ref';
			
			$tableName = $refs[$colName];
			
			$data['model'] = $this -> getTableCustomMetaData($tableName, 'model'); // Модель, на которую ссылается атрибут
			
		} else {
			if (array_key_exists('type', $data)) $type = $data['type']; else $type = false;
			$type = isset($data['type'])?$data['type']:false;
			
			if (!$type) // || !in_array($type,  self::enabledType())
			{ // Берем тип из описания БД
				$type = $column -> dbType;

				// Определяем типы set, enum			
				if ((strripos($type, 'set') !== false)) $type = 'set';
				elseif ((strripos($type, 'enum') !== false)) $type = 'enum';
				else $type = $column -> type;
			}
		}
		$data['type'] = $type;
		$data['required'] = (integer)(!$column -> allowNull);
			
		// Нормализуем поле filter
		// Если фильтр указан и <> false, то если фильтр не распознан, то присваиваем значение по умолчанию
		if (array_key_exists('filter', $data) && ($data['filter'] != 'false')) $filter = $data['filter']; $filter = false;
		$list = self::enabledFilterByType($colName);
		if (!in_array($filter, $list)) {
			if (count($list) > 0) $filter = $list[0];
		}
	//	echo $colName.' => '.(array_key_exists('filter', $data)?$data['filter']:"-").'<br/>';
			
		// Если show не указан, то значение по умолчанию для поля id show = false, иначе show = true
		$data['show'] = (array_key_exists('show', $data)? $data['show']:(strtolower($colName) != 'id'));
		$data['show'] = (($data['show'] == 'true') || ($data['show'] == '1'))?'1':'0';
		// Если visible не указан, то приравнивается значению show
		$data['visible'] = (array_key_exists('visible', $data)? $data['visible']:$data['show']);
		$data['visible'] = (($data['visible'] == 'true') || ($data['visible'] == '1'))?'1':'0';
		
		if ($type == 'ref') // Пока гиперссылки определяем только на ссылочных полях
		{
			$data['link'] = isset($data['link'])?$data['link']:'0';
			if ((boolean)$data['link']) {
				$this -> linkedAttrs[$colName] = $data;
			}
		}

		//echo $colName.' => '.$data['show'].','.$data['visible'].'<br/>';
		
		//  Падежи
		/*
		$keys = array('imp','rodp','vinp');
		foreach($keys as $key) {
			$data[$key] = (array_key_exists($key, $data)?$data[$key]:'');
		}
		*/
		if (strtolower($type) == 'manytomany')
		{
			$data = $this -> getManyToManyMeta($data);
			$this -> manyToManyAttrs[$colName] = $data;
		}

		$result[$colName] = $data;
	}
	$this -> columnMetaData = $result;
	}
	
	//$this -> linkedAttrs = $this -> generateLinks();

    return $result;
}

	// проверяет корректность отношения manyToMany и дополняет метаданные
	private function getManyToManyMeta($data) {
		if (!isset($data['viatable'])) return false; // не указана таблица-связка
		$viaTale = $data['viatable'];
		
		
		$refs = $this -> getTableLinks($viaTale);
		$index = count($refs);
		if ($index < 2) return false; //должно быть более одной связи
		foreach($refs as $name => $item) 
		{
			$index--;
			if ($item['tableName'] == $this -> tableName) {
				break;
			}				
		};
		if ($index < 0) return false; // не найдена связь с основной таблицей
		
		foreach($refs as $fname => $item) 
		{			
			if ($name != $fname) {
				break;
			}				
		};
		$ref = $refs[$fname];
		$data['mtm_bname'] = $name; // поле с обратной ссылкой
		$data['mtm_fname'] = $fname;// поле с прямой ссылкой
		$data['mtm_model'] = $ref['className'];
		return $data;
	}

	// формирует ссылки для полей, которых должны быть сфорированы ссылки
	// пока вместо шаблона просто ставим 1;
	/*
	protected  function generateLinks() 
	{
		$result = [];
		if ($this -> linkedAttrs) 
		{
			foreach ($this -> linkedAttrs as $attrName => $data) {
				//$model = $data['model'];			
				//$result[$attrName] = Html::a('%label%',[Inflector::camel2id($model).'/update/%id%']); ссылка на объект, вместо %id% подставляется ID объекта, на который идет ссылка
				$result[$attrName] = 1;
			}
		}
		return $this -> linkedAttrs;
	}
*/

    /**
     * Возвращает мета-данные таблицы из комментариев
     * @param string $tableName the table name 
     * @return array the generated metadata
     */
public  function getTableCustomMetaData($tableName, $name = '') {
	$result = false;
	$name = strtolower($name);

	// Если данные для указанной таблицы еще не получены
	if (!$this -> tableCustomMetaData || !array_key_exists($tableName, $this -> tableCustomMetaData)) {
		// Получаем комментарий к таблице
		$comment = Yii::$app->db->createCommand(
			'SELECT table_comment
			FROM information_schema.tables
			WHERE table_name = :tname'
		) -> bindValue(':tname', $tableName)
		->queryScalar();
		
		// Преобразовываем в массив
		$data = $this -> commentToMetaData($comment);
		// Сохраняем, чтобы большене пересчитывать
		$this -> tableCustomMetaData[$tableName] = $data;
	} else $data = $this -> tableCustomMetaData[$tableName];

	
	// Если в аргументе указано имя конкретного поля
	if ($name > '') { // Тогда возвращаем значение этого поля
		if (array_key_exists($name, $data)) $result = $data[$name];
	} else $result = $data; // иначе возвращаем весь массив

	return $result;

}


/**
/* Вычисление информации о заголовке объект
*/

public function getTableCaptionInfo($tableName) {
	$fields = [];	
	// Если данные для указанной таблицы еще не получены	
	if (!$this -> tableCaptionInfo || !array_key_exists($tableName, $this -> tableCaptionInfo)) {
		$caption = $this -> getTableCustomMetaData($tableName,'caption'); // считываем из мета-данных таблицы
		
		$db = $this->getDbConnection();
		$tableSchema = $db->getTableSchema($tableName);
		$columnName = [];
		foreach ($tableSchema->columns as $column) {
			$columnNames[] = strtolower($column->name);
		}
		
		if ($caption > '') {
			// определим список задействованных в Caption полей
			
			foreach ($columnNames as $name) {				
				if (strpos($caption, $name) !== false) $fields[] = $name;
			}
		} else { // ЕСЛИ caption не задан, то генерим его по-умолчанию
			if (in_array('title', $columnNames)) {
				$caption = '%title%';
				$fields[] = 'title';
			} elseif (in_array('name', $columnNames)) {
				$caption = '%name%';
				$fields[] = 'name';
			} elseif (in_array('id', $columnNames)) {
				$caption = '[%id%]';
				$fields[] = 'id';
			} else {
				$cname = $columnNames[0];
				$caption = '[%'.$cname.'%]';
				$fields[] = $caption;
			}
		}
		$result = ['template' => $caption, 'fields' => $fields];
		$this -> tableCaptionInfo[$tableName] = $result;
		
	} else $result = $this -> tableCaptionInfo[$tableName];

	return $result;
}

public function getTableLinks($tableName){
        $db = $this->getDbConnection();

	if (false&& $this -> tableLinks){
		$links = $this -> tableLinks[$tableName];
	} else{

        foreach ($this->getSchemaNames() as $schemaName) {
            foreach ($db->getSchema()->getTableSchemas($schemaName) as $table) {
                $className = $this->generateClassName($table->fullName);

		$links = ($this -> tableLinks && array_key_exists($table->fullName, $this -> tableLinks))?$this -> tableLinks[$table->fullName]:array();

                foreach ($table->foreignKeys as $refs) {
                    $refTable = $refs[0];
                    $refTableSchema = $db->getTableSchema($refTable);
                    if ($refTableSchema === null) {
                        // Foreign key could point to non-existing table
                        continue;
                    }
                    unset($refs[0]);
                    $field = array_keys($refs)[0];

                    $refClassName = $this->generateClassName($refTable);
			$links[$field] = array('tableName' => $refTable,'className' => $refClassName);

                }
		$this ->tableLinks[$table->fullName] = $links;
            }


        }
        $links = $this ->tableLinks[$tableName];
    }


	return  $links;
}

public function getTableByName($tableName) {
	return $this->getDbConnection()->getTableSchema($tableName);
}


}