<?php
/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this yii\web\View */
/* @var $generator common\gii\addresses\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $queryClassName string query class name */
/* @var $tableSchema yii\db\TableSchema */
/* // @var $labels string[] list of attribute labels (name => label) */
/* @var $columns array[] list of column attributes  (name => data) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */




$tableName = $generator->generateTableName($tableName);
//$modelClass = $generator -> ns.'\\'.$generator -> generateClassName($tableName);
$modelClass = $generator -> getTableCustomMetaData($tableName, 'model');
$table = $generator -> getTableByName($generator -> tableName);
$rules = $generator->generateRules($table);

$generator -> addUse($modelClass, 'yii');
if($generator->interface == 'IdentityInterface') $generator -> addUse($modelClass, 'yii\web\IdentityInterface');
if($tableName == 'adr_status') $generator -> addUse($modelClass, 'yii');


$manyToManyAttrs = $generator -> manyToManyAttrs?$generator -> manyToManyAttrs:[];



echo "<?php\n";
?>
/* генерируемый файл, редактировать его не надо */


namespace <?= $generator->ns ?>;


<?=$generator->generateUses($modelClass) ?>



/**
 * This is the model class for table "<?=$tableName  ?>".
 *
<?php foreach ($tableSchema->columns as $column): ?>
 * @property <?= "{$column->phpType} \${$column->name}\n" ?>
<?php endforeach; ?>
<?php if (!empty($relations)): ?>
 *
<?php foreach ($relations as $name => $relation): ?>
 * @property <?= $relation[1] . ($relation[2] ? '[]' : '') . ' $' . lcfirst($name) . "\n" ?>
<?php endforeach; ?>
<?php endif; ?>
 */
class <?= $className ?> extends <?= '\\' . ltrim($generator->baseClass, '\\')  ?> <?=($generator->interface > ''?" implements ".$generator->interface:'')."\n"?>
{
	
<?php $generator->generateConstants($tableName) ?>
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '<?= $generator->generateTableName($tableName) ?>'; 
    }


<?php

$parentTable = $generator -> getTablePrefix().$generator -> getTableCustomMetaData($tableName, 'parent');

$parentClass = $generator -> getTableCustomMetaData($parentTable, 'model');


if ($parentClass) {
	$parentClass = '\\'.$generator->ns.'\\'.$parentClass;
?>

    public static function parentClassName(){
		return '<?=$parentClass ?>';
    }

<?php

}


?>


	/* Списки для перечисляемых типов атрибутов */
<?=$generator->generateAttrLists($tableName) ?>
	/* Метаданные класса */
<?=$generator->generateTableMetaData($tableName) ?>
	/* Метаданные атрибутов*/
<?=$generator->generateAttrMetaData($tableName) ?>
	/* Поведения*/
<?=$generator->generateBehaviors($tableName) ?>

	
<?php
	/* Зависимые от атрибутов методы*/
	$generator->generateAttrRelatedMethods($tableName) 
?>

<?php
	/* Генерация методов для атрибутов типа JSON*/
	$generator->generateJSONMethods($tableName) 
?>

<?php if ($generator->generateTableName($tableName) == 'adr_status'): ?>

	// Значение статуса по умолчанию для указанного прикладного класса
	public static function getDefault($className) {
		return self::findByCondition(['table' => $className::tableName(), 'isdefault' => 1]) -> one();
	}

	// Список значений статусов, из которых может статус измениться в текуший
	public function getFrom() {
		return self::findBySql("select s.*  from {{%status_links}} sl, {{%status}} s where s.id = from_id AND to_id = :to", ["to" => $this -> id]);
	}
	
	// Список значений статусов, в которые может статус измениться из текуший
	public function getTo() {
		return self::findBySql("select s.*  from {{%status_links}} sl, {{%status}} s where s.id = to_id AND from_id = :from", ["from" => $this -> id]);
	}

	// Сохраняет изменения статуса в истории
	public static function saveToHistory($oldValue, $value, $object) {
		if (gettype($oldValue) == 'object') $oldValue = $oldValue -> id;
		if (gettype($value) == 'object') $value = $value -> id;
		if (gettype($object) == 'object') $object = $object -> id;


		if (!$oldValue && !$value || ($oldValue == $value) ) return false;

		$list = ['object_id' => $object];
		if ($oldValue) $list['status_old'] = $oldValue;
		if ($value) $list['status'] = $value;

		$command = Yii::$app->db->createCommand();

		$result = $command->insert('adr_status_changes', $list) -> execute();
		return $result;
   }


<?php endif; ?>


<?php if ($generator->db !== 'db'): ?>


    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('<?= $generator->db ?>');
    }
<?php endif; ?>



<?php if ($rules): ?>
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [<?= "\n            " . implode(",\n            ", $rules) . ",\n        " ?>]);
    }
<?php endif; ?>	

	
	
<?php foreach ($manyToManyAttrs as $name => $data): ?>    
	public function get<?= ucfirst($name)?>($template = false)
    {
        $list =  $this->hasMany(<?=$data['mtm_model']?>::className(), ['id' => '<?=$data['mtm_fname']?>'])
             ->viaTable('<?=$data['viatable']?>', ['<?=$data['mtm_bname']?>' => 'id']);
		$result = $this -> getListValue($list, $template, 'options');
		return $result;
    }

<?php endforeach; ?>
	
	
<?php foreach ($relations as $name => $relation): ?>    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get<?= $name ?>() {
        <?= $relation[0] . "\n" ?>
    }
<?php endforeach; ?>
<?php if ($queryClassName): ?>
<?php
    $queryClassFullName = ($generator->ns === $generator->queryNs) ? $queryClassName : '\\' . $generator->queryNs . '\\' . $queryClassName;
    echo "\n";
?>
    /**
     * @inheritdoc
     * @return <?= $queryClassFullName ?> the active query used by this AR class.
     */
    public static function find()
    {
        return new <?= $queryClassFullName ?>(get_called_class());
    }


<?php endif; ?>
<?php

	$template = trim($generator -> getTableCaptionInfo($tableName)['template']);

	//$textTmplate = ((ltrim($template, '[') == $template) && (ltrim($template, ']') == $template)); //Text template

?>


<?php if ($template): ?>

    public static function captionTemplate(){

        return "<?=$template?>";
    }

<?php endif; ?>


<?php 

if ($generator -> hasLinksFunctionText) {
	echo "/* Возвращает число ссылок на модель */";
	echo $generator -> hasLinksFunctionText;
} 
?>




<?php if($generator->interface == 'IdentityInterface'):?>
    public static function findIdentity($id)
    {
//        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]); 
	return static::findOne($id); 
    }


    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        //return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
	return static::findOne(['username' => $username]);
    }



    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }



    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }



<?php endif; ?>


}