<?php
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();

$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

$table = $generator -> getTableByName($generator -> tableName);
$modelClass = StringHelper::basename($generator->modelClass);
$ns = StringHelper::dirname($generator->modelClass);

$attrList = $generator -> getColumnMetaData($table);

$generator -> clearUse($generator->modelClass);

$generator -> addUse($generator->modelClass, $generator->modelClass);
$generator -> addUse($generator->modelClass, 'yii\helpers\Html');
$generator -> addUse($generator->modelClass, 'kartik\builder\Form');
$generator -> addUse($generator->modelClass, 'kartik\widgets\ActiveForm');
$generator -> addUse($generator->modelClass, 'kartik\builder\FormGrid');
$generator -> addUse($generator->modelClass, 'kartik\nav\NavX');
$generator -> addUse($generator->modelClass, 'kartik\icons\Icon');
$generator -> addUse($generator->modelClass, 'yii\helpers\Url');



$hasLinkedAttrs = $generator -> linkedAttrs && (count($generator -> linkedAttrs) > 0); // Присутствуют поля с гиперссылками
$hasManyToManyAttrs = $generator -> manyToManyAttrs && (count($generator -> manyToManyAttrs) > 0); // Присутствуют поля со связью ManyToMany

if ($hasManyToManyAttrs)
{
	foreach($generator -> manyToManyAttrs as $attrName => $data) {		
		$generator -> addUse($generator->modelClass, $ns.'\\'.$data['mtm_model']);		
	}
	
}


echo "<?php\n";
?>
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
<?=$generator -> generateUses($generator->modelClass)?>

$data = [

<?php 
$colCount = 2;
$col = 0;
//var_dump($generator->modelClass); exit;

$totalCount = count($attrList);

$gpsAttr = false;
$uploading = false; //через форму будут закачки файлов
//$photoAttr = false;

foreach ($attrList as $attrName => $data){
	$totalCount--;
	if ($attrName == 'id') continue;	
	if (!(int)$data['show']) continue;
	
	$readonly = isset($data['readonly']) && $data['readonly'];
	$type=$data['type'];
	//$comment=isset($data['comment'])?$data['comment']:'';
	$format=isset($data['format']) ? $data['format']:'';
	if ($format=='gps') {
		$gpsAttr = $attrName;
		//continue;
	}
	
	if ($format=='images') {
		$uploading = true;
		//continue;
	}	
	
	

	$url = $model::getFLinkTemplate($attrName);
	$comment = '';
	if ($url){		// Дополняем комментарий ссылкой на объект
		$comment = '<a title="Перейти" href="\'.Url::to([$model -> getCaption(\''.$url.'\')]).\'">\'.Icon::show(\'link\').\'</a>';
	}
	
	if ($readonly) $attrName = $attrName.'_view';	
	
	//$hintText = $comment>''?'\'hint\'=>\''.$comment.'\'.'.$modelClass.'::getMeta(\''.$attrName.'\', \'comment\'), ':$modelClass.'::getMeta(\''.$attrName.'\', \'comment\'), ';
	$hintText = '\'hint\'=>\''.$comment.'\'.'.$modelClass.'::getMeta(\''.$attrName.'\', \'comment\'), ';
	
//	if ($attrName == 'dogovor_osnovanie'){
//		echo '!!!!'.$hintText; exit;
	//}
	$readOnlyText = $readonly?'\'readonly\'=>\'true\', ':'';	
	$controlText = 'INPUT_TEXT';
	$itemsText = '';
	$widgetText = '';
	$options = '';
	$valueText = '';
	$attrPostFix = ''; // Постфикс имени атрибута, например, "_as_array"
	// TODO: Исправить БАГ! Если последнне поле оказывается не отображаемым в форме, то кнопка "Сохранить" не добавляется.
	if ($col == 0) $columns = $colCount;
	
	
	//'options' => ['type'=>Form::INPUT_CHECKBOX_LIST, 'items' => Option::listAll("%icon% - %name%"), 'label'=>Address::getMeta('options', 'label'), 'options'=>['multiple' => true]],				
	
//	if ($attrName == 'options'){var_dump($data); exit;};
	$break = false;
	if (!$readonly){
		if ($type == 'ref') {// выпадающий список
			$controlText = 'INPUT_DROPDOWN_LIST';
			if (isset($data['condition'])) {
				$itemsText = '$model -> getRelatedListValues(\''.$attrName.'\')';
			} else {
				$itemsText = 'getListValues(\''.$attrName.'\')';
			}
		};
		
		if (($type == 'date') || ($type == 'datetime')) { // датапикер
			$controlText = 'INPUT_WIDGET';
			$widgetText = '\'widgetClass\'=>\'\kartik\date\DatePicker\', ';
			$options = '\'type\' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, \'pluginOptions\' => [\'autoclose\'=>true, \'format\' => \'dd.mm.yyyy\'], ';
		};
		
		if (($format == 'images') || ($type == 'json')) { // Редактор набора изображений
			$controlText = 'INPUT_WIDGET';
			$widgetText = '\'widgetClass\'=>\'\common\widgets\photos\PhotoList\', ';			
		};
		
		if ($format == 'icon') { // выбор иконки
			$controlText = 'INPUT_RADIO_LIST';
			$itemsText = 'getIcon(\''.$attrName.'\')';
		};
		
		if ($format == 'checkboxlist') 
		{
			$controlText = 'INPUT_CHECKBOX_LIST';
			$options = '\'multiple\' => true';
			$itemsText = 'getList(\''.$attrName.'\')';
			$attrPostFix = '_as_array';
		}
		if (isset($data['mtm_model'])) 
		{
			$itemsText = $data['mtm_model'].'::listAll('.$modelClass.'::getMeta(\''.$attrName.'\', \'itemtemplate\'))';
		}


			
		if ($type == 'enum') {// выпадающий список
			$controlText = 'INPUT_DROPDOWN_LIST';
			$itemsText = 'getList(\''.$attrName.'\')';
		};
	
		
		
		/// Маски
		if (($controlText == 'INPUT_TEXT') && (isset($data['mask']))) { // Маска
		
			$mask = $data['mask'];
			$alias = false;
			$options = '';			
			if ($mask[0] == '@') {
				$alias = substr($mask, 1);
				$m = $generator->modelClass::getMaskByAlias($alias);			

				if ($m) {
					$mask = $modelClass.'::getMaskByAlias("'.$alias.'")';					
					$alias = false;
				} else {
					if (in_array($alias, ['email', 'decimal', 'date','ip'])) {					
						$mask = false;
					} else $mask = '\''.$mask.'\'';
				}			
			
			}  else $mask = '\''.$mask.'\'';

			
			if ($mask) $options = '\'mask\' => '.$mask;
			if ($alias) {
				$options = '\'alias\' =>  \''.$alias.'\'';
				
				if ($alias == 'decimal')  $options = $options.', \'groupSeparator\' => \' \',\'autoGroup\' => true';			
				$options = '\'clientOptions\' => ['.$options.']';
			}
			if ($options > '') $options = $options.',';
						
						
						
			$controlText = 'INPUT_WIDGET';
			$widgetText = '\'widgetClass\'=>\'\yii\widgets\MaskedInput\', ';
		};
		


		
		
		
	} else { // отдельная обработка элементов для только чтения
		if ($format == 'icon') { // изображение иконки
			$controlText = 'INPUT_RAW';
			$valueText = '\'<label class="control-label">\'.Option::getMeta(\'icon\', \'label\').\': </label>\'.Option::getIcon(\'icon\', $model -> __get(\'icon\')),';
		};		
	}
	
	
	

	
	if ($type == 'boolean') { // чекбокс	
		$controlText = 'INPUT_WIDGET';
		$widgetText = '\'widgetClass\'=>\'\kartik\checkbox\CheckboxX\', ';
		$options = '\'pluginOptions\' => [\'threeState\' => false],';
		 
	};
	
	if ($type == 'text') { // текстовое поле
		$controlText = 'INPUT_TEXTAREA';	
		$columns = 1;
		$break = true;

	};
	





	$options = $options.$readOnlyText;
	
	
	
if (($col +1 >= $colCount) && $break)	 {
	
?>		
				],
			],
				
		],		
	],
		
<?php 	
	
	if ($break) $col = 0;
}

	
	if ($itemsText > '') 
	{
		$itemsText = (strpos($itemsText, '::') === false && strpos($itemsText, '->') === false)?$modelClass.'::'.$itemsText:$itemsText; // Если класс не задан, то подставляем текущий
		
		$itemsText = '\'items\' => '.$itemsText.', ';
	}
	
	if ($col == 0) {
		
?>	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=><?=$columns?>,								
					'attributes' => [			
		
<?php 		
		$first = false;
	}
	
	if (!$readonly&&(($attrName == 'status_id') || ($attrName == 'status_id_view'))) { // Статус отрабатывается отдельно
?>						'status_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=><?=$modelClass?>::getMeta('<?=$attrName?>', 'label'), <?=$hintText?>'items' => $model -> getStatusList()],	
<?php
	}else {
		
?>						'<?=$attrName.$attrPostFix?>' => ['type'=>Form::<?=$controlText?>, <?=$widgetText?>'label'=><?=$modelClass?>::getMeta('<?=$attrName?>', 'label'), <?=$itemsText?><?=$valueText?><?=$hintText?>'options'=>[<?=$options?>]],
<?php  					

	}
	if (($col + 1 >= $columns) || ($totalCount == 0)) {
		$first = false;
?>		
				],
			],				
		],				
	],


<?php if ($totalCount == 0):?>
		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
<?php endif ?>
	
<?php 	
		
	}	
	
$col++; if ($col >= $columns) $col = 0;	
	
	
}		
?>

]




/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">


<?php
echo "<?php\n";
?>

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => <?=$modelClass?>::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



<?php if($gpsAttr):?>

$gps = $model -> <?=$gpsAttr?>;
$gps = explode(',', $gps);
if (count($gps) < 2) {
	$gps = ['55.751429','37.618879'];
}
echo sergmoro1\yamap\widgets\Yamap::widget([
	'points' => [
		[
			'lat' => $gps[0],
			'lng' => $gps[1],
			'icon' => '',
			'header' => '',
			'body'=> $model -> getCaption(),
			'footer' => '',
			'inputId' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass))?>-<?=$gpsAttr?>'
		],
	],
	'params' => ['visible' => true, 'zoom' => 15]
	
]); 
<?php endif?>
<?php
$options = '';
if ($uploading) {
	$options = '\'enctype\' => \'multipart/form-data\'';
}
if ($options > '') $options = '\'options\' => ['.$options.']';
?>
$form = ActiveForm::begin([<?=$options?>]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
