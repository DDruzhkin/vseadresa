<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

$table = $generator -> getTableByName($generator -> tableName);
$modelClass = StringHelper::basename($generator->modelClass);
$ns = StringHelper::dirname($generator->modelClass);
$attrList = $generator -> getColumnMetaData($table);
$hasLinkedAttrs = $generator -> linkedAttrs && (count($generator -> linkedAttrs) > 0); // Присутствуют поля с гиперссылками
$hasManyToManyAttrs = $generator -> manyToManyAttrs && (count($generator -> manyToManyAttrs) > 0); // Присутствуют поля со связью ManyToMany
//$table = $generator -> getTableByName($generator -> tableName);
//$attrList = $generator -> getColumnMetaData($table);


$generator -> clearUse($generator->modelClass);
$generator -> addUse($generator->modelClass,'yii\helpers\Html');
$generator -> addUse($generator->modelClass,'yii\grid\GridView');
$generator -> addUse($generator->modelClass,'kartik\daterange\DateRangePicker');
$generator -> addUse($generator->modelClass,'yii\helpers\Url');
$generator -> addUse($generator->modelClass,'kartik\nav\NavX');	
$generator -> addUse($generator->modelClass,'kartik\icons\Icon');	
$generator -> addUse($generator->modelClass,ltrim($generator->modelClass, '\\'));

if ($hasLinkedAttrs)
{
	$generator -> addUse($generator->modelClass, 'common\models\Entity');
	$generator -> addUse($generator->modelClass, 'yii\helpers\Inflector');	
}

if ($hasManyToManyAttrs)
{
	foreach($generator -> manyToManyAttrs as $attrName => $data) {		
		$generator -> addUse($generator->modelClass, $ns.'\\'.$data['mtm_model']);		
	}
	
}


if (!empty($generator->searchModelClass)) $generator -> addUse($generator->modelClass,ltrim($generator->searchModelClass, '\\'));
if ($generator->enablePjax) $generator -> addUse($generator->modelClass,'yii\widgets\Pjax');

echo "<?php\n";
?>
/* генерируемый файл, редактировать его не надо */



<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

<?=$generator -> generateUses($generator->modelClass)?>

$this->title = <?= $generator->generateString($generator->getMetaData('title')) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">

    <h1><?= "<?= " ?>Html::encode($this->title) ?></h1>
	<div class="content">
	<?= "<?= " ?><?= $generator->modelClass ?>::getMeta('','comment')	?>
	</div>
<?php if(!empty($generator->searchModelClass)): ?>
<?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
<?php endif; ?>

    <p>
        <?= "<?= " ?>Html::a(<?= $generator->generateString('Добавить '.$generator->getMetaData('vinp')) ?>, ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?= $generator->enablePjax ? '<?php Pjax::begin(); ?>' : '' ?>
<?php if ($generator->indexWidgetType === 'grid'): ?>
    <?= "<?= " ?>GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\<?=$modelClass?>::getListViewMenu($data),
							],
						],
					]);
				}					
			],


<?php
$count = 0;                      

//echo $generator -> ns; exit;
	
foreach ($attrList as $colName => $data) {
	
	if (!(int)$data['show']) continue;	
	
	$classes='';              
	$format = '';
	$filter = '';
	$type = '';
	$format_type = '';
	$value = '';
	$link = isset($data['link'])?$data['link']:false;// нужна ли ссылка в поле
	$cformat = ''; // тот который указывается у столбца свойством format
	$linked = isset($data['link'])?(boolean)$data['link']:false;
	
	if (!(int)$data['visible']) $classes=' hidden';	
	
	$type = $data['type'];
	
	if (isset($data['format'])) {
		$frmt = $data['format'];
		switch($frmt) {
			case 'icon': {
				$cformat = '\'image\'';
				$value = 'function($data){ return $data -> getIcon(\''.$colName.'\', $data -> '.$colName.', \'path\');}';
				break;

			};		
			case 'checkboxlist':
			case 'boolean': 
			case 'images': 
			case 'currency': 
			case 'files':
			case 'gps': {
				$cformat = '';
				break;
			}

			default: {
				$cformat = $frmt;
			}
		}

		switch($type) {
			
			case 'date':
			case 'datetime':
			case 'time':{
				//$cformat = '\''.trim($frmt, '\'"').'\'';
				$cformat = '';
				break;
			}

		}


	}
	
	
	if ($type == 'date') {
		$format_type = 'date';
		if (array_key_exists('format', $data)) $format = $data['format'];
		if ($format == '') $format = 'd.m.Y';
		//$cformat = "['date', 'php:$format']";
	}
	
	
	if ($type == 'ref') {
		if ($link) {
			$model = $data['model'];
			$value = '
			function($data)
			{ 
				$value = Html::a($data[\''.$colName.'_view\'],[Inflector::camel2id(\''.$model.'\').\'/update/\'.$data[\''.$colName.'\']]);
				return $value;
			}';			
		} else {
			$value = 'function($data){ return $data[\''.$colName.'_view\'];}';
		}

	} else $link = false; // пока гиерссылки только на ссылочных полях
	
	if (in_array($type, ['manytomany']) )
	{
		$cformat = '\'raw\'';
		$value = '
				function($data){ 
					$model = Address::findone($data[\'id\']);
					if ($model) {
						return $model -> getOptions(\'@listitemtemplate\');
					}
				}';
	}
	if ($link) $cformat = '\'raw\'';
	//if ($value > '') {echo $value; exit;};
	
	if (array_key_exists('filter', $data) && $data['filter'] != false) {
		$filterType = $data['filter'];
		if ($filterType == 'list') {
			if (in_array($type, ['ref', 'integer', 'float', 'string'])) $filter = $modelClass.'::getExistsValues(\''.$colName.'\'),	';
			else $filter = $modelClass.'::getList(\''.$colName.'\'),	';
		}elseif ((in_array($type, ['date', 'datetime'])) && ($filterType == 'range')) {
			
			$filter = 'DateRangePicker::widget([
						\'model\' => $searchModel,
						\'attribute\' => \''.$colName.'\',
						
						\'presetDropdown\'=>true,
						\'convertFormat\' => true,
						\'hideInput\'=>true,
						\'pluginOptions\'=>[							
							\'locale\'=>[\'format\'=>\''.$format.'\']
						]
						
					]),';
		}
	}
	
	//if ($cformat != '\'raw\'') $cformat = '';/// TODO разобраться с форматами - вылазят неформатные форматы, типа, gps, images...
	
	
	/*
	$format = $generator->generateColumnFormat($column);

	if ($format > '') {
			$format_type = " form-".$format;
			$format = "'format' => '".$format."',";

	}
	*/
	
	
	if ($value > '') $value = '\'value\' => '.$value.',';
	
	$col = "['attribute' => '".$colName."',
				'label' => ".$modelClass."::getMeta('".$colName."', 'label'),
				'contentOptions' => ['class' => 'col-".$colName.$classes.' '.$format_type."'],
				'filterOptions' => ['class' => 'col-".$colName.$classes."'],
				'headerOptions' => ['class' => 'col-".$colName.$classes.' '.$format_type."'],				
				'footerOptions' => ['class' => 'col-".$colName.$classes.' '.$format_type."'],
				".($filter > ''?"'filter' => ".$filter:"")."
				".$value."
				".($cformat > ''?"'format' => ".$cformat.",":'')."
			]";
				
				
	echo "            ".$col. ",\n";
			
};			
	


?>

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
        },
    ]) ?>
<?php endif; ?>
<?= $generator->enablePjax ? '<?php Pjax::end(); ?>' : '' ?>
</div>
