<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$links = $generator -> getTableLinks($generator -> tableName);
$table = $generator -> getTableByName($generator -> tableName);
$attrList = $generator -> getColumnMetaData($table);

echo "<?php\n";
?>
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = "<?=$generator->getMetaData('imp')?> ".$model->getCaption();
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$attrList = $model -> getAttributes(); 
$attributes = [];
	foreach ($attrList as $colName => $value) {		
		$type = $model -> getMeta($colName, 'type');
		$show = (int)$model -> getMeta($colName, 'show');
		if (!$show) continue;
		$attributes[] = $colName."_view" . ($type == 'text'?':ntext':'');
	}


?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-view">

    <h1><?= "<?= " ?>Html::encode($this->title) ?></h1>

    <p>
        <?= "<?= " ?>Html::a(<?= $generator->generateString('Изменить') ?>, ['update', <?= $urlParams ?>], ['class' => 'btn btn-primary']) ?>
        <?= "<?= " ?>Html::a(<?= $generator->generateString('Удалить') ?>, ['delete', <?= $urlParams ?>], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => <?= $generator->generateString('Вы уверены, что хотите удалить элемент?') ?>,
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= "<?= " ?>DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

</div>
