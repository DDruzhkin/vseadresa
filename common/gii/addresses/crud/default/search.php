<?php
/**
 * This is the template for generating CRUD search class of the specified model.
 */

use yii\helpers\StringHelper;


$generator -> clearUse($generator->modelClass);
/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
$ns = StringHelper::dirname($generator->modelClass);

if ($modelClass === $searchModelClass) {
    $modelAlias = $modelClass . 'Model';
}
$attrList = $generator -> getFilteredColumns(); // Только фильтруемые атрибуты
$refs = $generator -> getTableLinks($generator -> tableName); /// поля-ссылки модели

$rules = [];
$attrListLRef = [];
foreach ($attrList as $attrName => $data) { 

	

	if (($data['filter'] == 'like') && ($data['type'] == 'ref')) {
		$rules[] = '[\''.$attrName.'\', \'string\', \'length\' => [3]]';
		$attrListLRef[] = $attrName; // собираются атрибуты, по которым должна идти фильтрация по like для ref
		
		$model = $ns.'\\'.$refs[$attrName]['className'];
		$generator -> addUse($generator->modelClass, $model);
		$generator -> addUse($generator->modelClass, 'yii\helpers\ArrayHelper');
	}
	
	
}

$sortAttr = $generator -> getMetaData('sortAttr');
$sortDir = $generator -> getMetaData('sortDir');
if (!$sortDir) {
	if (!$sortAttr)$sortDir = 'desc'; else $sortDir = 'asc';
};
if (!$sortAttr) $sortAttr = 'id';
$sortDir = strtoupper($sortDir);


$rAttr = array_diff(array_keys($attrList), $attrListLRef);
if (count ($rAttr) > 0) $rules = array_merge($generator->generateSearchRules($rAttr), $rules); else $rules = false;



$labels = $generator->generateSearchLabels();

$searchAttributes = $generator->getSearchAttributes();
$searchConditions = $generator->generateSearchConditions();

$generator -> addUse($generator->modelClass,'yii');
$generator -> addUse($generator->modelClass,'yii\base\Model');
$generator -> addUse($generator->modelClass,'yii\data\ActiveDataProvider');
$generator -> addUse($generator->modelClass,ltrim($generator->modelClass, '\\') . (isset($modelAlias) ? " as $modelAlias" : ""));



echo "<?php\n";
?>
/* генерируемый файл, редактировать его не надо */


namespace <?= StringHelper::dirname(ltrim($generator->searchModelClass, '\\')) ?>;

<?=$generator -> generateUses($generator->modelClass)?>

/**
 * <?= $searchModelClass ?> represents the model behind the search form about `<?= $generator->modelClass ?>`.
 */
class <?= $searchModelClass ?> extends Model {
	
	
<?php foreach ($attrList as $attrName => $data): 
	

?>	public $<?=$attrName?>;	
<?php endforeach; ?>
	
	
	<?php if ($rules): ?>
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            <?= implode(",\n            ", $rules) ?>,
        ];
    }
	
	<?php endif ?>

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = <?= isset($modelAlias) ? $modelAlias : $modelClass ?>::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'<?=$sortAttr?>'=>SORT_<?=$sortDir?>
				
			]
     ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
		
		
	<?php foreach ($attrList as $colName => $data): 
			$filter = $data['filter'];
			$type = $data['type'];
			
	?>					
		<?php if(($filter == 'like') &&($type == 'ref')):
		
			//$model = $ns.'\\'.$refs[$colName]['className'];
			$model = $refs[$colName]['className'];
			
			
		?>	
		/* фильтры по like для ссылки*/
		if ($this -> <?=$colName?>) {			
			$concat = 'CONCAT('.<?=$model?>::getCaptionTemplateAsStringList().')';

			$list = <?=$model?>::find() -> andWhere(['like', $concat , $this-><?=$colName?>])->asArray()->all();
			$list = ArrayHelper::getColumn($list, 'id');			
			if (count ($list) > 0) {
				$list = implode(',',$list);			
				$query->andWhere("<?=$colName?> in ($list)");			
			} else { // Условие не выполнено
				$query->andWhere('false');
				return $dataProvider;
			}
		}
		<?php endif; ?>		
		<?php if(($filter == 'value') || ($filter == 'list' && ($type == 'ref'))):?>	
		/* фильтры по значению, по списку для ссылки*/
		<?php if(($type == 'integer') || ($type == 'smallint') || ($type == 'ref')):?>$query->andFilterWhere(['<?=$colName?>' => $this -> <?=$colName?> > ""?(int)$this -> <?=$colName?>:""]);
		<?php else: ?>$query->andFilterWhere(['<?=$colName?>' => $this-><?=$colName?>]);<?php endif; ?>
		<?php endif; ?>		
		<?php if(($filter == 'list') && ($type == 'set')):?>	
		/* фильтры по set */
		if ($this-><?=$colName?>) {
			if($this-><?=$colName?> > '') {
				$this-><?=$colName?> = <?=$modelClass?>::getList('<?=$colName?>')[$this-><?=$colName?>];
				$query->andWhere('FIND_IN_SET(:<?=$colName?>,<?=$colName?>)>0',[':<?=$colName?>' => $this-><?=$colName?>]);
			}			
		}
		<?php endif; ?>			
		<?php if(($filter == 'range') && ($type == 'date')):?>		
		/* фильтры по daterangepicker */
		if ($this -> <?=$colName?>) {
			list($dateFrom, $dateTo) = explode('-', $this -> <?=$colName?>);
			$dateFrom = self::datetToSql($dateFrom);
			$dateTo = self::datetToSql($dateTo, 1);
			
			$query->andFilterWhere(['between', '<?=$colName?>', $dateFrom, $dateTo]);
		};
		<?php endif; ?>		
		<?php if(($filter == 'like') && ($type != 'ref')):?>		
		/* фильтры по like */
		$query->andFilterWhere(['like', '<?=$colName?>', $this-><?=$colName?>]);
		<?php endif; ?>		
	<?php endforeach; ?>
		

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function dateToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
