<?php

return [
	'category' => 'Заказ',
	'title' => 'Частичная оплата',
	'role' => 'Администратор',
    'view' => 'representation',
    'content' => '
        <p>На сайте Vseadresa.pro по заказу № {this.number} на адрес {address.address} подтверждена частичная оплата</p>
     '
];


