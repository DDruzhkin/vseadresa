<?php

return [
	'category' => 'Заказ',
	'title' => 'Доставка',
	'role' => 'Заказчик',
    'view' => 'representation',
    'content' => '
        <p>Необходимые документы по заказу № {this.number} на Ваш адрес {address.address} готовы. Заказ переведен в статус Доставка</p>

     '
];


