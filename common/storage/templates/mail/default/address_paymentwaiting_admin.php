<?php
            return[
					'role' => 'Администратор',
					'title' => 'Ожидание оплаты по Адресу',
					'category' => 'Адрес',
                   'view' => 'default-view',
                   'content' => '<p>На сайте Vseadresa.pro адрес {this.address} перешел в состояние ожидания оплаты.&nbsp;Владелец адреса&nbsp;{owner.fullname}.</p>
'
           ];
        