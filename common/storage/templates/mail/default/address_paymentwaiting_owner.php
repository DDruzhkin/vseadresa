<?php
            return[
					'role' => 'Администратор',
					'title' => 'Ожидание оплаты по Адресу',
					'category' => 'Адрес',
                   'view' => 'default-view',
                   'content' => '<p>На сайте Vseadresa.pro за услуги по&nbsp;Вашему&nbsp;адресу {this.address} ожидается оплата.&nbsp;</p>

<p>Вам необходимо обеспечить своевременную оплату счетов!</p>
'
           ];
        