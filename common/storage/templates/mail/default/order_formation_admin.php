<?php

return [
	'category' => 'Заказ',
	'title' => 'Формирование заказа',
	'role' => 'Администратор',
    'view' => 'representation',
    'content' => '
       <p>На сайте Vseadresa.pro заказ № {this.number} на адрес {address.address} переведен в статус Формирование</p>       
     '
];


