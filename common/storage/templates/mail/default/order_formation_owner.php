<?php

return [
	'category' => 'Заказ',
	'title' => 'Формирование заказа',
	'role' => 'Владелец',
    'view' => 'representation',
    'content' => '
       <p>На сайте Vseadresa.pro заказ № {this.number} на Ваш адрес {address.address} переведен в статус Формирование</p>       
     '
];


