<?php

return [
	'category' => 'Заказ',
	'title' => 'Частичная оплата',
	'role' => 'Владелец',
    'view' => 'representation',
    'content' => '
        <p>На сайте Vseadresa.pro по заказу № {this.number} на Ваш адрес {address.address} подтверждена частичная оплата</p>
     '
];


