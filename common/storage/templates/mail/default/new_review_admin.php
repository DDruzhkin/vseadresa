<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
	'category' => 'Отзывы',
	'title' => 'Новый отзыв',
	'role' => 'Администратор',
    'view' => 'representation',
    'content' => '
			Получен новый отзыв:
            <p>{this.content}</p>
			Автор: {this.author_position} {this.name} ({this.company})
            
     '
];


