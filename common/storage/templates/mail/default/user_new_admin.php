<?php

return [
	'category' => 'Пользователь',
	'title' => 'Регистрация Пользователя',
	'role' => 'Администратор',
    'view' => 'representation',
    'content' => '
        На сайте VseAdresa.pro зарегистрирован новый Пользователь: ({fullname}).
     '
];


