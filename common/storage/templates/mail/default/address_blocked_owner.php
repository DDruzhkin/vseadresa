<?php
            return[
					'role' => 'Владелец',
					'title' => 'Блокировка адреса',
					'category' => 'Адрес',
                   'view' => 'default-view',
                   'content' => '<p>Ваш адрес {this.address} заблокирован, не отображается на сайте VseAdresa.pro и не может быть заказан.</p>
'
           ];
        