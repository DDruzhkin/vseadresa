<?php

return [
	'category' => 'Заказ',
	'title' => 'Доставка',
	'role' => 'Заказчик',
    'view' => 'representation',
    'content' => '
        <p>Необходимые документы по Вашему заказу № {this.number} на адрес {address.address}  готовы и будут отправлены на доставку.</p>

     '
];


