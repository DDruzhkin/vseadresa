<?php

return [
	'category' => 'Заказ',
	'title' => 'Выставлен счет',
	'role' => 'Администратор',
    'view' => 'representation',
    'content' => '
          <p>На сайте Vseadresa.pro по заказу №{this.number} на адрес {address.address} выставлен счет.</p>
		  <p>Общая сумма заказа: {this.summa}</p>
     '
];


