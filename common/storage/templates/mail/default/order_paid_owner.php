<?php
            return[
					'role' => 'Владелец',
					'title' => 'Заказ Оплачен',
					'category' => 'Заказ',
                   'view' => 'default-view',
                   'content' => '<p>На сайте Vseadresa.pro для адреса {address.address} подтверждена полная оплата заказа № {this.number}.</p>

<p>Теперь заказ № {this.number} виден в Вашем личном кабинете</p>

<p>Обеспечьте своевременное выполнение заказа.</p>
'
           ];
        