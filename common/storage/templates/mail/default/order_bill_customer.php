<?php
            return[
					'role' => 'Заказчик',
					'title' => 'Выставлен счет',
					'category' => 'Заказ',
                   'view' => 'default-view',
                   'content' => '<p>На сайте Vseadresa.pro за Ваш заказ № {this.number}&nbsp;на адрес {address.address}&nbsp;выставлен счет.</p>

<p>Общая сумма заказа: {this.summa}р.</p>

<p>Для завершения процедуры Заказ Вам необходимо обеспечить своевременную оплату счета.</p>
'
           ];
        