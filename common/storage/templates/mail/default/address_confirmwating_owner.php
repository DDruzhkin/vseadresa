<?php
            return[
					'role' => 'Владелец',
					'title' => 'Ожидание подтверждения',
					'category' => 'Адрес',
                   'view' => 'default-view',
                   'content' => '<p>На сайте Vseadresa.pro по Вашему&nbsp;адресу {this.address} подтверждена оплата услуг фотографа.</p>

<p>Ожидайте визита фотографа.</p>
'
           ];
        