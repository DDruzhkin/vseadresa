<?php
            return[
					'role' => 'Администратор',
					'title' => 'Пользователь активирован',
					'category' => 'Пользователь',
                   'view' => 'default-view',
                   'content' => '<p>На сайте VseAdresa.pro Владелец ({this.fullname}) заключил Договор (Доп.соглашение) и переведен в статус &laquo;Активный&raquo;.</p>
'
           ];
        