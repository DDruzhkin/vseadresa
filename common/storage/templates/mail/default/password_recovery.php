<?php
            return[
					'role' => 'Пользователь',
					'title' => 'Восстановление пароля',
					'category' => 'Пользователь',
                   'view' => 'default-view',
                   'content' => '<p>Здравствуйте, {user.fullname}.</p>

<p>Перейдите по ссылке, чтобы восстановить пароль:</p>

<p><a href="{common.host}/reset-password?token={user.password_reset_token}">{common.host}/reset-password?token={user.password_reset_token}</a></p>
'
           ];
        