<?php

return [
	'category' => 'Адрес',
	'title' => 'Удаление адреса',
	'role' => 'Владелец',
    'view' => 'representation',
    'content' => '
        <p> Ваш адрес ({address}) удален администратором. </p>
     '
];


