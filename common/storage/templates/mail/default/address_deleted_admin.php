<?php

return [	
	'category' => 'Адрес',
	'title' => 'Удаление адреса',
	'role' => 'Администратор',
    'view' => 'representation',
    'content' => '
        <p>Адрес ({address}, {company}) удален.</p>
    
        <p>Вы можете восстановить адрес.</p>

     '
];


