<?php

return [
	'category' => 'Заказ',
	'title' => 'Документы в пункте самовывоза',
	'role' => 'Владелец',
    'view' => 'representation',
    'content' => '
        <p>Необходимые документы по заказу № {this.number} на Ваш адрес {address.address} готовы и находятся в пункте самовывоза.</p>

     '
];


