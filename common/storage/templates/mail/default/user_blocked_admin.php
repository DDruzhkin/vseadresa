<?php
            return[
					'role' => 'Администратор',
					'title' => 'Пользователь заблокирован',
					'category' => 'Пользователь',
                   'view' => 'default-view',
                   'content' => '<p>На сайте VseAdresa.pro Пользователь ({this.fullname}) заблокирован.</p>
'
           ];
        