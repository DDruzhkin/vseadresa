<?php
            return[
					'role' => 'Заказчик',
					'title' => 'Формирование заказа',
					'category' => 'Заказ',
                   'view' => 'default-view',
                   'content' => '<p>На сайте Vseadresa.pro Ваш заказ № {this.number} на адрес&nbsp;{address.address}&nbsp;переведен в статус Формирование.</p>

<p>Подготовка документов для заключения договора началась.</p>
'
           ];
        