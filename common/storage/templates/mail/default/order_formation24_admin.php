<?php

return [
	'category' => 'Заказ',
	'title' => 'Заказ в статусе Формирование более 24 часов',
	'role' => 'Администратор',
    'view' => 'representation',
    'content' => '
        <p>Заказ № {this.number} на адрес {address.address} находится в статусе “Формирование” более 24 часов: {hour}ч.{minute}мин.</p>
     '
];



