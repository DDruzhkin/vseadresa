<?php

return [
	'category' => 'Заказ',
	'title' => 'Доставка',
	'role' => 'Администратор',
    'view' => 'representation',
    'content' => '
        <p>Необходимые документы по заказу № {this.number} на адрес {address.address} готовы. Заказ переведен в статус Доставка</p>

     '
];


