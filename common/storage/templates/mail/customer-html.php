<?php
use yii\helpers\Url;

?>
<table align="center"  style="    background: #17bfff30;" border="0" cellpadding="0" cellspacing="0" width="680">
    <tbody>
    <tr>
        <td style="width:28px;" width="28">
        </td>
        <td style="vertical-align:top;width:620px" align="center" valign="top" width="620">
            <table style="border-collapse:collapse;width:620px;" align="center" border="0" cellpadding="0"
                   cellspacing="0" width="620">
                <tbody>
                <tr>
                    <td>
                        <div style="line-height:24px;height:24px;">&nbsp;</div>
                        <div><font face="Arial" color="#944e0a" size="3">
                                <p style="font-size:20px;font-family:Arial,Tahoma,Verdana,sans-serif;color:#944e0a !important;line-height:25px;text-align:left;margin:0;">
                                    Здравствуйте <br> <span><?= $model->fullname ?></span></p>
                            </font></div>
                    </td>
                    <td style="text-align:right;width:133px;" align="right" width="133">
                        <div style="line-height:12px;height:12px;">&nbsp;</div>
                        <div style="" class="mmr2-mso_lh0_mailru_css_attribute_postfix">
                            <div class="header-padding-left">
                                <a href="<?= Url::base(true); ?>">
                                    <div class="logo"
                                         style="    font-size: 1.8em; font-family: solomon-bold;letter-spacing: 1px;">
                                        <span class="main-page-link" style="    color: #33424D;">vseadresa</span><span
                                            class="pro" style="    color: #F93237;">.pro</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <div style="line-height:12px;height:24px;">&nbsp;<br>
                &nbsp;</div>
            <div><font face="Arial" color="#522700" size="6">
                    <p style="font-size:50px;font-family:Arial,Tahoma,Verdana,sans-serif;color:#522700 !important;line-height:55px;text-align:center;margin:0;"
                       align="center">Спасибо,
                        что <br>
                        зарегистрировались <br>
                        в <a href="<?= Url::base(true); ?>" style="font-size:50px;font-family:Arial,Tahoma,Verdana,sans-serif;color:#522700 !important;line-height:55px;text-align:center;margin:0;    text-decoration: none;">vseadresa.<span style="">pro</span></a>
                    </p>
                </font></div>
            <div style="line-height:15px;height:30px;">&nbsp;<br>
                &nbsp;</div>
            <div></div>

            <div>
                <div style="margin-bottom: 11px;font-size: 20px;">Ваши данные</div>
                <div style="padding-bottom: 5px;font-size: 17px;"><span>ФИО</span>-<?= $model->fullname ?></div>
                <div style="padding-bottom: 5px;font-size: 17px;"><span>Эл.адрес</span>-<?= $model->email ?></div>
            </div>

            <div style="line-height:24px;height:96px;">&nbsp;<br>
                &nbsp;<br>
                &nbsp;<br>
                &nbsp;</div>
        </td>
    </tr>
    </tbody>
</table>