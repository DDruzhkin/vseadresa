<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $model->password_reset_token]);
?>
<?= Yii::t('app','hello')?> <?= $model->username ?>,

<?= Yii::t('app','followLinkResetPassword')?>

<?= $resetLink ?>
