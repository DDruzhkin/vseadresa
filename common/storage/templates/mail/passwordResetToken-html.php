<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $model->password_reset_token]);
?>
<div class="password-reset">
    <p><?= Yii::t('app','hello')?> <?= Html::encode($model->username) ?>,</p>

    <p><?= Yii::t('app','followLinkResetPassword')?></p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
