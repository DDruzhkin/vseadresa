<?php

use common\models\Entity;
use common\models\EntityExt;
use common\models\EntityHelper;
use common\models\Rip;

$owner = $model->owner;
$recipient = $model->recipient;


/*define('FORM_PERSON', ['name' => 'person', 'type' => 'Физлицо']);

define('FORM_COMPANY', ['name' => 'company', 'type' => 'Юрлицо']);

define('FORM_IP', ['name' => 'ip', 'type' => 'ИП']);*/


if ($owner && $recipient):

    $template = null;

    $createdAt = strtotime($model->created_at);
	$d=date('j',$createdAt);
    $m=date('n',$createdAt);
    $y=date('Y',$createdAt) + 3; 
	
	$finishAt = mktime(0,0,0,$m,$d,$y); // дата через три года после начала договора
	
	$recipientData = $recipient -> attributes;
	$recipientData['headerText'] = $recipient -> getHeaderText("Принципал");
	$recipientData['footerText'] = $recipient -> getFooterText();
	
	$ownerData = $owner -> attributes;
	$ownerData['headerText'] = $owner -> getHeaderText("Агент");
	$ownerData['footerText'] = $owner -> getFooterText();
	
	$params['contract'] = [
		'start' => Yii::$app -> formatter -> asDate($createdAt, 'd MMMM Y'),
		'finish' => Yii::$app -> formatter -> asDate($finishAt, 'd MMMM Y'),
		'number' => $model->number
	];
	
	$params['recipient'] = $recipientData;
	$params['owner'] = $ownerData;
	
	$params['options'] = EntityExt::portal() -> options;
	
    $template = file_get_contents(Yii::getAlias('@storage/templates/documents/contract.html'));
	
	$text = EntityHelper::replaceContent($template, $params);
	echo  $text;

endif;
?>
