<?php

use common\models\EntityHelper;
use frontend\models\Rperson;
use frontend\models\Service;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;


$owner = $model->owner;
//var_dump((array)$owner);
$recipient = $model->recipient;


$porder = $model -> porder;

$customerTypeData = EntityHelper::getUserTypeData($recipient);
$ownerTypeData = EntityHelper::getUserTypeData($owner);

$hasVAT = $owner -> tax_system_vat;

$params['invoice'] = $model -> attributes;
$params['owner'] = $owner -> attributes;
$params['owner']['title'] = $owner -> getTitle();
$params['recipient'] = $recipient -> attributes;
$params['recipient']['title'] = $recipient -> getTitle();
$params['summa'] = ((int)$model -> summa).' руб. 00 коп.';
$params['date'] = $model -> created_at_view;
$params['order']=$model->order;

//var_dump($params); exit;
?>
<?php if ($owner && $recipient): ?>
<?php
	if ($recipient -> form == Rperson::FORM_PERSON) {
		$params['subject'] = 'Оплата услуг по счету №'.$model->number;
		$template = file_get_contents(Yii::getAlias('@storage/templates/documents/pd4.html'));		
		$text = EntityHelper::replaceContent($template, $params);
		echo  $text;
	} else {
?>
    <table class="owner-recipient-date" style=" width: 100%;border: 1px solid #020202;margin-bottom: 20px;">
        <thead>
        <tr>
            <th style="padding:5px;width:50%">Получатель</th>
            <th style="padding:5px;width:50%">Плательщик</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th style="padding:5px;width:50%"><?= ArrayHelper::getValue($ownerTypeData, 'fullname') ?></th>
            <th style="padding:5px;width:50%"><?= $model->order->requisite_company_name ?></th>
        </tr>
        <tr>
            <td style="padding:5px;width:50%"><?= $owner->address ?></td>
            <td style="padding:5px;width:50%"><?= $recipient->address ?></td>
        </tr>
        <tr>
            <td style="padding:5px;width:50%">
                <?php if ($owner->inn || $owner->kpp): ?>
                    ИНН/КПП <?= $owner->inn; ?>
                    <?php if ($owner->kpp): ?> /
                        <?= $owner->kpp ?>
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td style="padding:5px;width:50%">
                <?php if ($model->order->requisite_inn || $model->order->requisite_kpp): ?>
                    ИНН/КПП <?= $model->order->requisite_inn; ?>
                    <?php if ($model->order->requisite_kpp): ?> /
                        <?= $model->order->requisite_kpp ?>
                    <?php endif; ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td style="padding:5px;width:50%">ОГРН <?= $owner->ogrnip?:$owner->ogrn ?></td>
            <td style="padding:5px;width:50%">ОГРН <?= $model->order->requisite_ogrn ?></td>
        </tr>
        <tr>
            <td style="padding:5px;width:50%">БИК <?= $owner->bank_bik ?></td>
            <td style="padding:5px;width:50%"><?php if($model->order->requisite_bank_id): ?>БИК <?= $model->order->requisite_bank_id ?><?php endif?></td>
        </tr>
        <?php if($owner->bank_name):?>
            <tr>
                <td style="padding:5px;width:50%"><?= $owner->bank_name ?></td>
                <td style="padding:5px;width:50%"><?php if($model->order->requisite_bank_name): ?><?= $model->order->requisite_bank_name ?><?php endif?></td>
            </tr>
        <?php endif;?>

        <tr>
            <td style="padding:5px;width:50%">р/с <?= $owner->account ?></td>
            <td style="padding:5px;width:50%"><?php if($model->order->requisite_bank_account): ?>р/с <?= str_replace(' ','',$model->order->requisite_bank_account) ?><?php endif;?></td>
        </tr>
        <?php if($owner->bank_cor):?>
            <tr>
                <td style="padding:5px;width:50%">к/с <?= $owner->bank_cor ?></td>
                <td style="padding:5px;width:50%"><?php if($model->order->requisite_bank_correspondent_account): ?>к/с <?= str_replace(' ','',$model->order->requisite_bank_correspondent_account)?><?php endif;?></td>
            </tr>
        <?php endif;?>
        <tr>
            <td style="padding:5px;width:50%"><?= $owner->account_owner ?></td>
            <td style="padding:5px;width:50%"></td>
        </tr>
<!--
        <tr>
            <td style="padding:5px;width:50%">Телефон <?= $owner->phone ?></td>
            <td style="padding:5px;width:50%">Телефон <?= $recipient->phone ?></td>
        </tr>
-->		
        </tbody>
    </table>


    <div><h4 class="text-center">
            <strong>
                СЧЕТ № <?= $model->number ?> от <?= $model->created_at_view ?>
            </strong>
        </h4></div>
    <div class="text-right"><?= $model->info ?></div>
    <table class="table table-striped document-service-table" style="    border: 1px solid #dedede;">
        <thead>
        <tr>
            <th>№</th>
            <th>Товар / Услуги</th>
            <th>Цена,руб.</th>
            <th>Кол-во</th>
            <th>Ед.изм</th>
			<?php if ($hasVAT): ?>
            <th>НДС</th>
			<?php endif?>
            <th>Сумма,руб.</th>
        </tr>
        </thead>
        <tbody>

        <?php
		
		
		
		if (is_null($porder)) { // Обычный заказ
		
			$services = $model -> getServices() -> all();
			
			$list = [];
			foreach($services as $service) {
				$serv = new stdClass;
				$serv->name = $service -> name;
				$serv->price = $service -> price;
				$serv->summa = $service -> summa;			
				$serv->count = $service -> getDurationValue();
				$serv->unit = $serv->count == 1?'шт.':'мес.';
				$serv->nds = Service::getMeta('nds', 'default');
				$list[] = $serv;
			}		
			if ($model->address_id) {// Заказана аренда адреса
				$rentAddress = new stdClass;
				$rentAddress->name = 'Аренда адреса ('.$model->order -> duration.')';
				$rentAddress->price = (int)$model->order->addressPrice();
				$rentAddress->summa = (int)$model->order->addressPrice();
				$rentAddress->count = 1;
				$rentAddress->unit = 'шт.';
				$rentAddress->nds = ($hasVAT ? Service::getMeta('nds', 'default') : 0);
				$list[] = $rentAddress;
			}
		} else { // портальный заказ
			$pService = $porder -> service;	
			$portalService = new stdClass;
			$portalService->name = $pService -> name;			
			$portalService->price = (int)$porder->summa;
			$portalService->summa = (int)$porder->summa;
			$portalService->count = 1;
			$portalService->unit = 'шт.';
			$portalService->nds = ($hasVAT ? Service::getMeta('nds', 'default') : 0);
			$list = [$portalService];	 // в поратальных заказах бывает только одна услуга		
		}
        
        $sumPrice = $sumSumma = 0;
		$nds = 0; // общий НДС должен считаться отдельно по каждой ставке. На текуший момент этого не сделано из предположения, что ставка одна (т.к. управление ставками не реализовано)
        foreach ($list as $key => $service):
			if ($owner -> tax_system_vat){
				$price = round($service->price / ($service->nds / 100 + 1) * $service->nds / 100, 2);
				$nds = $service->nds;
			} else {
				$price = 0;
				$nds = 0;
			}
			
            $sumPrice += $price;
            $sumSumma += $service->summa;
            ?>
            <tr>
                <td style="border: 1px solid #e0dede;"><?= $key + 1 ?></td>
                <td style="border: 1px solid #e0dede;"><?= $service->name ?></td>
                <td style="border: 1px solid #e0dede;"><?= number_format($service->price, 2, ".", ""); ?></td>
                <td style="border: 1px solid #e0dede;"><?=$service->count?></td>
                <td style="border: 1px solid #e0dede;"><?=$service->unit?></td>
				<?php if ($hasVAT): ?>
                <td style="border: 1px solid #e0dede;"><?= $service->nds?>%</td>
				<?php endif ?>
                <td style="border: 1px solid #e0dede;"><?= number_format($service->summa, 2, ".", ""); ?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
			
            <td style="border: 1px solid #e0dede;" colspan="<?=($hasVAT?6:5)?>" class="text-right">Итого в т.ч. НДС:
				<?php if ($hasVAT): ?>
					(<?= $nds ?>%)
					= <?= number_format($sumPrice, 2, ".", ""); ?> руб
				<?php else: ?>
					без НДС
				<?php endif?>
            </td>
			
			
			
            <td style="border: 1px solid #e0dede;" colspan="1"><?= number_format($sumSumma, 2, ".", ""); ?> </td>
        </tr>
        </tbody>
    </table>
	

<?php	
	}
?>
<?php endif; ?>
