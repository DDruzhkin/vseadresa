<?php

use common\models\EntityHelper;
use frontend\models\Rperson;
use frontend\models\Service;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;


$owner = $model->owner;
//var_dump((array)$owner);
$recipient = $model->recipient;


$porder = $model->porder;

$customerTypeData = EntityHelper::getUserTypeData($recipient);
$ownerTypeData = EntityHelper::getUserTypeData($owner);

$hasVAT = $owner->tax_system_vat;

$params['invoice'] = $model->attributes;
$params['owner'] = $owner->attributes;
$params['owner']['title'] = $owner->getTitle();
$params['recipient'] = $recipient->attributes;
$params['recipient']['title'] = $recipient->getTitle();
$params['summa'] = ((int)$model->summa) . ' руб. 00 коп.';
$params['date'] = $model->created_at_view;
$params['order'] = $model->order;

if (!function_exists('num_str')) {
    function num_str($num)
    {
        $nul = 'ноль';
        $ten = array(
            array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
            array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        );
        $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
        $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
        $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
        $unit = array( // Units
            array('копейка', 'копейки', 'копеек', 1),
            array('рубль', 'рубля', 'рублей', 0),
            array('тысяча', 'тысячи', 'тысяч', 1),
            array('миллион', 'миллиона', 'миллионов', 0),
            array('миллиард', 'милиарда', 'миллиардов', 0),
        );
        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) {
                if (!intval($v)) continue;
                $uk = sizeof($unit) - $uk - 1;
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                $out[] = $hundred[$i1];
                if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3];
                else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3];
                if ($uk > 1) $out[] = morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
            }
        } else $out[] = $nul;
        $out[] = morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]);
        $out[] = $kop . ' ' . morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]);
        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

    function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) return $f5;
        $n = $n % 10;
        if ($n > 1 && $n < 5) return $f2;
        if ($n == 1) return $f1;
        return $f5;
    }

    ;
}
//var_dump($ownerTypeData);
?>
<?php if ($owner && $recipient): ?>
    <div>
        <table style="width: 100%; border-collapse: collapse;font-size:12px;">
            <tr>
                <td style="width:55%;border: 0.5px solid #3a3a3a; border-bottom: none" colspan="2">
                    Банк получателя
                </td>
                <td style="width:11%;border: 0.5px solid #3a3a3a;">БИК</td>
                <td style="width:45%;border: 0.5px solid #3a3a3a;  border-bottom: none"><?= $owner->bank_bik ?></td>
            </tr>
            <tr>
                <td style="width:55%;border: 0.5px solid #3a3a3a; border-top: none; vertical-align: bottom" colspan="2"
                    rowspan="2">
                    <?= $owner->bank_name ?>
                </td>
                <td style="width:11%;border: 0.5px solid #3a3a3a; border-bottom: none">К/С №</td>
                <td style="width:45%;border: 0.5px solid #3a3a3a; border-top: none; vertical-align: top"
                    rowspan="2"> <?= $owner->bank_cor ?></td>
            </tr>
            <tr>
                <td style="width:11%;border: 0.5px solid #3a3a3a; border-top: none; color: #fff">_</td>
            </tr>
            <tr>
                <td style="width:27.5%;border: 0.5px solid #3a3a3a;">ИНН <?= $owner->inn; ?></td>
                <td style="width:27.5%;border: 0.5px solid #3a3a3a;">КПП <?= $owner->kpp ?></td>
                <td style="width:11%;border: 0.5px solid #3a3a3a; border-bottom: none">Р/С №</td>
                <td style="width:45%;border: 0.5px solid #3a3a3a; border-bottom: none"> <?= $owner->account ?></td>
            </tr>
            <tr>
                <td style="width:55%;border: 0.5px solid #3a3a3a; border-bottom: none" colspan="2">Получатель</td>
                <td style="width:11%;border: 0.5px solid #3a3a3a; border-bottom: none; border-top: none"></td>
                <td style="width:45%;border: 0.5px solid #3a3a3a; border-bottom: none; border-top: none"></td>
            </tr>
            <tr>
                <td style="width:55%;border: 0.5px solid #3a3a3a; border-top: none; vertical-align: bottom" colspan="2"
                    rowspan="2"><?= ArrayHelper::getValue($ownerTypeData, 'fullname') ?></td>
                <td style="width:11%;border: 0.5px solid #3a3a3a; border-top: none; border-bottom: none; color: #fff">_</td>
                <td style="width:45%;border: 0.5px solid #3a3a3a; border-top: none; color: #fff" rowspan="2">_</td>
            </tr>
            <tr>
                <td style="width:11%;border: 0.5px solid #3a3a3a; border-top: none; color: #fff ">_</td>
            </tr>
        </table>
        <div style="border-bottom: 0.5px solid #3a3a3a; width: 100%">
            <h4 style="padding-top: 15px; padding-bottom: 15px ">
                <strong>
                    СЧЕТ на оплату №<?= $model->number ?> от <?= $model->created_at_view ?>г.
                </strong>
            </h4>
        </div>
        <table style="width: 100%; border-collapse: collapse;font-size:11px;">
            <tr>
                <td style="width: 20%; padding:7px 0">Поставщик<br>(Исполнитель):<br></td>
                <td style="width: 80%"><?= ArrayHelper::getValue($ownerTypeData, 'fullname') ?></td>
            </tr>
            <tr>
                <td style="width: 20%; padding:7px 0">Покупатель<br>(Заказчик):<br></td>
                <td style="width: 80%"><?=/* $model->*/$order->requisite_company_name ?></td>
            </tr>
            <tr>
                <td style="width: 20%; padding:7px 0 20px 0">Основание:</td>
                <td style="width: 80%; padding:7px 0 20px 0">Оплата услуг по заказу №<?= $order->number ?></td>
            </tr>
        </table>
        <table style="border-collapse: collapse; width: 100%; text-align: center;font-size:12px;">
            <tr>
                <th style="border: 0.5px solid #3a3a3a;text-align: center; width: 5%;">№</th>
                <th style="border: 0.5px solid #3a3a3a;text-align: center; width: 50%;">Товары(работы, услуги)</th>
                <th style="border: 0.5px solid #3a3a3a;text-align: center; width: 7.5%;">Кол-во</th>
                <th style="border: 0.5px solid #3a3a3a;text-align: center; width: 7.5%;">Ед</th>
                <th style="border: 0.5px solid #3a3a3a;text-align: center; width: 15%;">Цена</th>
                <?php if (0 && $hasVAT): ?>
                    <th>НДС</th>
                <?php endif ?>
                <th style="border: 0.5px solid #3a3a3a;text-align: center; width: 15%;">Сумма</th>
            </tr>
            <?php


            if (is_null($porder)) { // Обычный заказ

                $services = $model->getServices()->all();

                $list = [];
                foreach ($services as $service) {
                    $serv = new stdClass;
                    $serv->name = $service->name;
                    $serv->price = $service->price;
                    $serv->summa = $service->summa;
                    $serv->count = $service->getDurationValue();
                    $serv->unit = $serv->count == 1 ? 'шт.' : 'мес.';
                    $serv->nds = Service::getMeta('nds', 'default');
                    $list[] = $serv;
                }
                if ($model->address_id) {// Заказана аренда адреса
                    $rentAddress = new stdClass;
                    $rentAddress->name = 'Аренда адреса (' . $model->order->duration . ')';
                    $rentAddress->price = (int)$model->order->addressPrice();
                    $rentAddress->summa = (int)$model->order->addressPrice();
                    $rentAddress->count = 1;
                    $rentAddress->unit = 'шт.';
                    $rentAddress->nds = ($hasVAT ? Service::getMeta('nds', 'default') : 0);
                    $list[] = $rentAddress;
                }
            } else { // портальный заказ
                $pService = $porder->service;
                $portalService = new stdClass;
                $portalService->name = $pService->name;
                $portalService->price = (int)$porder->summa;
                $portalService->summa = (int)$porder->summa;
                $portalService->count = 1;
                $portalService->unit = 'шт.';
                $portalService->nds = ($hasVAT ? Service::getMeta('nds', 'default') : 0);
                $list = [$portalService];     // в поратальных заказах бывает только одна услуга
            }

            $sumPrice = $sumSumma = 0;
            $nds = 0; // общий НДС должен считаться отдельно по каждой ставке. На текуший момент этого не сделано из предположения, что ставка одна (т.к. управление ставками не реализовано)
            foreach ($list as $key => $service):
                if ($owner->tax_system_vat) {
                    $price = round($service->price / ($service->nds / 100 + 1) * $service->nds / 100, 2);
                    $nds = $service->nds;
                } else {
                    $price = 0;
                    $nds = 0;
                }

                $sumPrice += $price;
                $sumSumma += $service->summa;
                ?>
                <tr>
                    <td style="border: 0.5px solid #3a3a3a; padding: 15px 0;text-align: center"><?= $key + 1 ?></td>
                    <td style="border: 0.5px solid #3a3a3a;text-align: center"><?= $service->name ?></td>
                    <td style="border: 0.5px solid #3a3a3a;text-align: center"><?= $service->count ?></td>
                    <td style="border: 0.5px solid #3a3a3a;text-align: center"><?= $service->unit ?></td>
                    <td style="border: 0.5px solid #3a3a3a;text-align: center"><?= number_format($service->price, 2, ".", ""); ?></td>

                    <?php if (0 && $hasVAT): ?>
                        <td style="border: 0.5px solid #e0dede;"><?= $service->nds ?>%</td>
                    <?php endif ?>
                    <td style="border: 0.5px solid #3a3a3a;text-align: center"><?= number_format($service->summa, 2, ".", ""); ?></td>
                </tr>
            <?php endforeach; ?>
            <?php if (0): ?>
                <tr>

                    <td style="border: 0.5px solid #e0dede;" colspan="<?= ($hasVAT ? 6 : 5) ?>" class="text-right">Итого в
                        т.ч.
                        НДС:
                        <?php if ($hasVAT): ?>
                            (<?= $nds ?>%)
                            = <?= number_format($sumPrice, 2, ".", ""); ?> руб
                        <?php else: ?>
                            без НДС
                        <?php endif ?>
                    </td>


                    <td style="border: 0.5px solid #e0dede;"
                        colspan="1"><?= number_format($sumSumma, 2, ".", ""); ?> </td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
        <table style="width: 100%; border-collapse: collapse;font-size:10px; margin-top: 7px">
            <tr>
                <td style="width: 85%; text-align: right"><b>Итого:</b></td>
                <td style="width: 15%; text-align: left"><?= number_format($sumSumma, 2, ".", ""); ?></td>
            </tr>
            <tr>
                <td style="width: 85%; text-align: right"><b>В том числе НДС:</b></td>
                <td style="width: 15%; text-align: left"><?= $nds ?></td>
            </tr>
            <tr>
                <td style="width: 85%; text-align: right"><b>Всего к оплате:</b></td>
                <td style="width: 15%; text-align: left"><?= number_format($sumSumma + $nds, 2, ".", ""); ?></td>
            </tr>
        </table>
        <table style="width: 100%; border-collapse: collapse;font-size:10px;">
            <tr>
                <td style="padding-top: 7px">
                    Всего наименований, <?= count($list) ?>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Сумма прописью</b>
                </td>
            </tr>

            <tr>
                <td style="padding-top: 20px; border-bottom: 0.5px solid #3a3a3a">
                    <?= num_str(number_format($sumSumma + $nds, 2, ".", "")) ?>
                </td>
            </tr>
        </table>
        <table style="width: 100%; border-collapse: collapse;font-size:12px; margin-top: -15px">
            <tr>
                <td style="width: 15%; padding-top: 25px; vertical-align: bottom"><b>Руководитель</b></td>
                <td style="width: 40%; padding-top: 25px; vertical-align: bottom; border-bottom: 0.5px solid #3a3a3a; text-align: center">
                    <?php if($owner->sign):?>
                    <img style="margin-top: -10px" height="45px" src="/files/images/requisites/><?= $owner->sign; ?>">
                    <?php endif;?>
                </td>
                <td style="width: 45%; padding-top: 25px; vertical-align: bottom;">
                    ( <?= ArrayHelper::getValue($ownerTypeData, 'short_name') ?> )
                </td>
            </tr>
            <tr>
                <td style="width: 15%;"></td>
                <td style="width: 40%; text-align: left">
                    <?php if($owner->stamp):?>
                    <img height="150px" style="margin-top: -50px; padding-left: 20px"
                                                                src="/files/images/requisites/><?= $owner->stamp; ?>">
                    <?php endif;?>
                </td>
                <td style="width: 45%; padding: 10px 0 0 4px; vertical-align: top">Расшифровка</td>
            </tr>
        </table>
    </div>

<?php endif; ?>
