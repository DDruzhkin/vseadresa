<?php

use common\models\Entity;
use common\models\EntityExt;
use common\models\EntityHelper;
use common\models\Rip;

$owner = $model->owner;
$recipient = $model->recipient;


/*define('FORM_PERSON', ['name' => 'person', 'type' => 'Физлицо']);

define('FORM_COMPANY', ['name' => 'company', 'type' => 'Юрлицо']);

define('FORM_IP', ['name' => 'ip', 'type' => 'ИП']);*/


if ($owner && $recipient):

    $template = null;

	$originalContract = $recipient -> user ->  getContract();
	
    $createdAt = strtotime($model->created_at);
	$d=date('j',$createdAt);
    $m=date('n',$createdAt);
    $y=date('Y',$createdAt) + 3; 
	
	$finishAt = mktime(0,0,0,$m,$d,$y); // дата через три года после начала договора
	
	$recipientData = $recipient -> attributes;
	$recipientData['headerText'] = $recipient -> getHeaderText("Принципал");
	$recipientData['footerText'] = $recipient -> getFooterText();
	
	
	$recipientOld = Entity::byId($recipient -> prior_id);
	$recipientOldData = $recipientOld -> attributes;
	$recipientOldData['headerText'] = $recipientOld -> getHeaderText("Принципал");
	$recipientOldData['footerText'] = $recipientOld -> getFooterText();
	
	
	$ownerData = $owner -> attributes;
	$ownerData['headerText'] = $owner -> getHeaderText("Агент");
	$ownerData['footerText'] = $owner -> getFooterText();
	
	$params['contract'] = [
		'start' => Yii::$app -> formatter -> asDate($createdAt, 'd MMMM Y'),
		'finish' => Yii::$app -> formatter -> asDate($finishAt, 'd MMMM Y'),
		'number' => $model->number
	];
	
	

	$params['originalContract'] = [	
		'start' => Yii::$app -> formatter -> asDate($originalContract->created_at, 'd MMMM Y'),
		'number' => $originalContract -> number,
	];
	
	$params['recipient'] = $recipientData;
	$params['recipient']['old'] = $recipientOldData;
	$params['owner'] = $ownerData;
	
	$params['options'] = EntityExt::portal() -> options;
	
    $template = file_get_contents(Yii::getAlias('@storage/templates/documents/agreement.html'));
	
	$text = EntityHelper::replaceContent($template, $params);
	
	echo  $text;

endif;
?>
