<?php

namespace common\widgets;

use yii\web\AssetBundle;

class YamapAsset extends AssetBundle
{
	public $sourcePath = '@common/widgets/views';
	public $js = [
		'yamap.js',
	];
	public $depends = [
		'yii\web\YiiAsset',
	];
}
