
ymaps.ready(init);
var yaMapAreas={};
var map;
var collection; 

var placemarkColor = [
	"twirl#lightblueStretchyIcon", "twirl#violetStretchyIcon", "twirl#greenStretchyIcon",
	"twirl#redStretchyIcon", "twirl#yellowStretchyIcon",
	"twirl#darkblueStretchyIcon", "twirl#nightStretchyIcon",
	"twirl#greyStretchyIcon", "twirl#blueStretchyIcon",
	"twirl#orangeStretchyIcon", "twirl#darkorangeStretchyIcon",
	"twirl#pinkStretchyIcon", "twirl#whiteStretchyIcon"
];

function init() {
for(var map_canvas in yaMapAreas) 
{
	var yaMapPoints = yaMapAreas[map_canvas]["points"];
	var yaMapParams = yaMapAreas[map_canvas]["params"];

	if(yaMapPoints.length != 0) {
		var point=yaMapPoints[0];
		
		// get map canvas
		var mc=document.getElementById(map_canvas);
		// save visability
		var display=mc.style.display;
		// show map canvas
		mc.style.display='block';
		// set width and height
		mc.style.width=yaMapParams['width'];
		mc.style.height=yaMapParams['height'];
		
		// make Yandex map and set needed controls
		map = new ymaps.Map (map_canvas, { behaviors: ['scrollZoom', 'drag'], center: [point['lat'], point['lng']], zoom: yaMapParams['zoom']}); 
		map.controls.add('smallZoomControl');
		map.controls.add('mapTools');

		// make points collection
		collection = new ymaps.GeoObjectCollection();
		
		// place all defined points on a map
		for(var i=0;i<yaMapPoints.length;i++) {
			point=yaMapPoints[i];
			collection.add(makePlacemark(point));
			}
		map.geoObjects.add(collection);

		// all points should be visible on the map 
		if(yaMapPoints.length>1)
			map.setBounds(collection.getBounds());

		// the map may be hidden and shown by request, 
		// for example by clicking on a ref
		if(yaMapParams['visible'])
			mc.style.display='block';
		else
			if(display=='none') mc.style.display='none';
		}
	}
}

function makePlacemark(point) {
newPlacemark = new ymaps.Placemark([point['lat'], point['lng']], 
	{
		
		
		iconContent: point['icon'],
		//balloonContentHeader: point['header'],
		//balloonContentBody: '<em>' + point['body'] + '</em>',
		//balloonContentFooter: point['footer'] 
		
	},	
	
	{
		draggable: true,
		//preset: placemarkColor[point['color'] !== undefined ? point['color'] : 0 ] //'twirl#blueStretchyIcon' 
	} 
	);
	
	
	newPlacemark.events.add("dragend", function (e) {
            coords = this.geometry.getCoordinates();			
            savecoordinats(newPlacemark, point['inputId']);
    }, newPlacemark);
	
	/*
	newPlacemark.events.add('drag', function (e) {

		console.log(placemark.geometry.getCoordinates())
	});*/
	
	
	function savecoordinats(placemark, inputId) {
        var new_coords = [coords[0].toFixed(6), coords[1].toFixed(6)];
        placemark.getOverlay().getData().geometry.setCoordinates(new_coords);
        var c = document.getElementById(inputId);				
		if (c) c.value = new_coords;
		
    }
	
return newPlacemark;
}
