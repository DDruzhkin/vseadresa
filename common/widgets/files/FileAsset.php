<?php
namespace commoon\widgets\photos;
use yii\web\AssetBundle;
class PhotosAsset extends AssetBundle
{
	public $sourcePath = '@common/widgets/files/assets';
	public $css = [
		'css/files.css'
	];
	
	public $js = [
    	'js/images.js'
	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}

?>