<?php
namespace common\widgets\photos;

use Yii;
use yii\base\Widget;
use yii\web\View;
use yii\helpers\Url;

class FileList extends Widget {
	public $model;
	public $attribute;
	public $label;
//	public $inputId;
	
	public function init() {
		parent::init();
		$view = Yii::$app->getView();
		$this->registerAssets();
		$view->registerJs($this->getJs());
		$view->registerCssFile('/css/widget-files.css');		
	}
	public function run() {		
		return $this->render('photos', ['model' => $this -> model, 'attribute' => $this -> attribute, 'label' => $this -> label]);
		
	}
	
	public function registerAssets()
	{
		$view = $this->getView();		
	}
	
	private function getJs() {
	$cellHtml = "html = '<li class=\"selected-img\"><table border=\"0\"><tr><td width=\"150px\" style=\"\">' + html + '<td><tr></table></li>';";	
	//$srcPath = Url::to(['storage/images/'.$this -> model -> modelId()]);
	$srcPath = $this -> model -> getImage();	
    $script = "
	
	
	
	
	function renderImages_".$this -> attribute."() {
		
		function calcImageListValue() { // Рассчитать значение для сохранения списка изображений
			attr = '".$this -> attribute."';			
			var list = new Array();
			$('#' + attr + '-image-list ul.old li').each(function(){
				name = $(this).find('img').attr('data-name');
				title = $(this).find('input').val();
				status = 'ok';
				if ($(this).hasClass('deleted')) status = 'deleted';
				
				list.push({
					name: name,
					title:title,
					status: status
				})
				
			})
			
			$('#' + attr + '-image-list ul.new li').each(function(){
				name = $(this).find('img').attr('data-name');
				title = $(this).find('input').val();
				list.push({
					name: name,
					title:title,
					status: 'new'
				})
				
			})
			
			$('#".$this -> model -> modelId().'-'.$this -> attribute."').val(JSON.stringify(list));	
			//console.log(JSON.stringify(list));
			
		}
	
		
		var elList = $('#".$this -> attribute."-image-list ul.old');			
		elList.on('change', '.".$this -> model -> modelId()."_".$this -> attribute."', function(){
			calcImageListValue();
			return false;
		});
		elList.empty();
		value = $('#".$this -> model -> modelId().'-'.$this -> attribute."').val().trim();
		console.log(value);
		var list = false;
		if (value) list = JSON.parse(value);
		for (var i=0, len=list.length; i<len; i++) {
			var item = list[i];
			var name = item.name.trim();
			var title = item.title.trim();				
			if (name > '') {				
					html = ['<img data-name=\"' + name + '\" class=\"thumb\" src=\"".$srcPath."', name, '\" />'].join('');
					html = html + '<a class=\'delete\' href=\'#\' onClick=\'return false;\'>x</a>';
					html = html + '<p/><input class=\'".$this -> model -> modelId()."_".$this -> attribute."\' value=\"' + title + '\"></input>';
					".$cellHtml."
					el = elList.append(html);
					
				//el = elList.append('<li><img src=\"' + value + '\"/><a data-img=\"' + value + '\"   class=\'delete\' href=\'#\' onClick=\'return false;\'>x</a></li>');										
			}
		}		
		$(document).on('click', '#".$this -> attribute."-image-list ul.old li a.delete', function () {
			li = $(this).parent().parent().parent().parent().parent();
			if(li.hasClass('deleted')) {
				li.removeClass('deleted');
				$(this).html('X');
				li.find('input').attr('readonly', false);
			} else {
				li.addClass('deleted');
				$(this).html('V');
				li.find('input').attr('readonly', true);
								
			}
			calcImageListValue();
			
		});
		calcImageListValue();
	}
	
	function startLoading(elementId) {
		loading = $('#loading-screen');
		
		if (elementId) {			
			element = $(elementId);
			if (element) {			
				loading.height(element.height());
				loading.width(element.width());
				loading.offset(element.offset());
				///console.log(element.position());
			}
		}
		loading.show();
		
		
	}
	function breakLoading() {
		$('#loading-screen').hide();		
		
	}
	
	$.ajaxSetup({
		beforeSend: function(){
			startLoading();
		},
		complete: function(){
			breakLoading();
		}   
	});
	
	function removeImage(image) {		
		 image = image.trim();
		 console.log(image);

	}
	
	//$('#form-".$this -> attribute." input.send').(function(e){
		
	
	$('#".$this -> attribute."-file').change(function(e){
		list = $('#".$this -> attribute."-image-list ul.new');
		list.empty(); // Удаляем предыдущий выбор		
		var files = e.target.files; 
		for (var i = 0, f; f = files[i]; i++) {
			if (!f.type.match('image.*')) {
				alert(\"Image only please....\");
			}
			//console.log(f);
			var reader = new FileReader();
			
			reader.onload = (function(theFile) {
				
				var data = { file: this.result };      
				
				return function (e) {
					
					html = ['<img data-name=\"' + theFile.name + '\" class=\"thumb\" title=\"', escape(theFile.name), '\" src=\"', e.target.result, '\" />'].join('');
					html = html + '<br/><input onChange=\"calcImageListValue_".$this -> attribute."(); return false;\" value=\"' + theFile.name.replace(/\.[^.]+$/, \"\") + '\"></input>';
//					html = '<li class=\"selected-img\">' + html + '</li>';
					".$cellHtml."
					var div = list.append(html);					
					calcImageListValue_".$this -> attribute."();
					//var div = renderNewImage(e.target.result, theFile.name.replace(/\.[^.]+$/, \"\"));
				};			

			})(f);
			
			reader.onerror = function(event) {
				console.error(\"Файл не может быть прочитан! код \" + event.target.error.code);
			};
		
			reader.readAsDataURL(f);			
			
		}
		   		
        e.preventDefault();
  
        return false;
    });     

	
	
	function calcImageListValue_".$this -> attribute."() { // Рассчитать значение для сохранения списка изображений
		attr = '".$this -> attribute."';
			var list = new Array();
			$('#' + attr + '-image-list ul.old li').each(function(){
				name = $(this).find('img').attr('data-name');
				title = $(this).find('input').val();
				status = 'ok';
				if ($(this).hasClass('deleted')) status = 'deleted';
				
				list.push({
					name: name,
					title:title,
					status: status
				})
				
			})
			
			$('#' + attr + '-image-list ul.new li').each(function(){
				name = $(this).find('img').attr('data-name');
				title = $(this).find('input').val();
				list.push({
					name: name,
					title:title,
					status: 'new'
				})
				
			})
			
			$('#".$this -> model -> modelId()."-' + attr).val(JSON.stringify(list));				
			
}
	
	
	
	renderImages_".$this -> attribute."();
	
	
	";
	//$this->view->registerJs($script, View::POS_READY, 'uploadimage');
	$this->view->registerJs($script, yii\web\View::POS_READY);
	
	}
	
	
	
	
	
	
	
}
?>