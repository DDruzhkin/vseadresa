<?php
namespace commoon\widgets\photos;
use yii\web\AssetBundle;
class PhotosAsset extends AssetBundle
{
	public $sourcePath = '@common/widgets/photos/assets';
	public $css = [
		'css/images.css'
	];
	
	public $js = [
    	'js/images.js'
	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}

?>