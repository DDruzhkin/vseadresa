<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>


<div id="loading-screen">
	<img src="/images/loading-circle-blue.gif"/>
</div>


<div id="<?=$attribute?>-image-list"  style="width:800px">

<label class="control-label"><?=$label?></label>
<div class="image-list">
<ul class="old">	
</ul>
<div class="clear"></div>
<hr/>
<h3>Добавить изображения:</h3>
<div class="image-new-list">
<ul class="new">	
</ul>
<div class="clear"></div>

<input id='<?=$attribute?>-file' name="<?=$model -> baseClassName()?>[<?=$attribute?>_files][]" type='file' multiple = 'multiple'/>
<textarea id='<?=$model -> modelId()?>-<?=$attribute?>' style="display:none;" name="<?=$model -> baseClassName()?>[<?=$attribute?>]">
<?=$model -> __get($attribute)?>
</textarea>

</div>
