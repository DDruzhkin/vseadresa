<?php
namespace common\widgets\ymap;

use Yii;
use yii\base\Widget;
use yii\web\View;
use yii\helpers\Url;

class Ymap extends Widget {
	public $model;
	public $attribute;
	public $label;	
	public $params = ['visible' => 0,'zoom' => 13];
	public $options =['style' => 'width: 100%; height: 200px; margin-right:10px;'];
	
//	public $inputId;
	
	public function init() {
		parent::init();
		$view = Yii::$app->getView();
		$this->registerAssets();
				
		$script = $view->registerJs($this->getJs());		
		$this->view->registerJsFile('https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU');

		YmapAsset::register($this->view);
	}
	public function run() {		
		return $this->render('ymap', ['model' => $this -> model, 'attribute' => $this -> attribute, 'label' => $this -> label, 'options' => $this -> options]);
		
	}
	
	public function registerAssets()
	{
		$view = $this->getView();		
	}
	
	private function getJs() {
		
		$gps = explode(',', $this -> model -> gps);
		
		if (count($gps) < 2) {
			$gps = 0;
		};
		
		$params = $this->params;
		
		$params['behaviors'] =['scrollZoom', 'drag'];
		//$params['center'] = $gps;
		
		$gps = json_encode($gps);
		$params = json_encode($params);
		$address = $this -> model -> address;
		$attribute = $this -> attribute;
		$script = <<<JS

		ymaps.ready(init);

        function init () {
			

			gps = $gps;
			if (gps === 0) {			
				address = "Москва, $address";

				var myGeocoder = ymaps.geocode(address);
				myGeocoder.then(
					function (res) {
						 gps = res.geoObjects.get(0).geometry.getCoordinates();;
						 console.log(gps, "GPS");
						 showMap(gps);

					}
				);
			} else {
				showMap(gps);
			}
			//

        };
		
		function showMap(gps) {
			params = $params;
			params['center'] = gps;
            var map = new ymaps.Map("ymap", 			 
				params
            );
			
			map.controls.add('smallZoomControl');
			map.controls.add('mapTools');
			
			var searchControl = new ymaps.control.SearchControl({noPlacemark: true});
                 map.controls.add(searchControl); 
				
				
			map.geoObjects.add(makePlacemark(gps));
			
			map.events.add("click", function(e){
                newPlacemark.geometry.setCoordinates(e.get("coordPosition"));

            });
			
		}
		
		function makePlacemark(gps) {
			

			newPlacemark = new ymaps.Placemark(
                gps, 
                 
                {
                    'hintContent': 'Местоположение адреса $address' ,
					
                }, 
				{
					'draggable'	: true			
				}
			);
		
			if ($gps == 0) {				
				document.getElementById("$attribute-map-input").value = gps;
			}
		
		
			newPlacemark.events.add("dragend", function (e) {
				coords = this.geometry.getCoordinates();
				savecoordinats(newPlacemark, "$attribute-map-input");
			}, newPlacemark);
			
			return newPlacemark;
		}
		
		
		function savecoordinats(placemark, inputId) {
			
				var new_coords = [coords[0].toFixed(6), coords[1].toFixed(6)];
				placemark.getOverlay().getData().geometry.setCoordinates(new_coords);
				document.getElementById(inputId).value = new_coords;
				//console.log(new_coords);
			
		};
		
		function crds(crd) {
			var myGeocoder = ymaps.geocode(crd);
			myGeocoder.then(
				function (res) {
					return res.geoObjects.get(0).geometry.getCoordinates();;
				}
			);
		}
		
		
JS;
		
		
		$this->view->registerJs($script, yii\web\View::POS_READY);
		
		
		
	
	}

}
?>