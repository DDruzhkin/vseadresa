<?php
namespace common\widgets\ymap;
use yii\web\AssetBundle;


class YmapAsset extends AssetBundle
{
	public $sourcePath = '@common/widgets/ymap/assets';
/*	
	public $css = [
		'css/ymap.css'
	];
	
	public $js = [
    	'js/ymap.js'
	];
*/	
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}

?>