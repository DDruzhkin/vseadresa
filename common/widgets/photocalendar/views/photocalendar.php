<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Entity;
use common\models\EntityHelper;
use frontend\models\PhotoCalendar;


function getHints($adminMode) {
	if ($adminMode) {
		$hints = PhotoCalendar::ADMIN_SLOT_STATE_TITLES;				
	} else {
		$hints = PhotoCalendar::SLOT_STATE_TITLES;		
	}
	
	return $hints;
}

function getLockedHints($adminMode) {
	
	$hints = getHints($adminMode);
	if ($adminMode) {
		$lockedHints = [
			PhotoCalendar::SLOT_IS_FREE => 'Слот свободен. Изменить состояние прошлых дат невозможно',
			PhotoCalendar::SLOT_IS_CLOSED => 'Слот заблокирован. Изменить состояние прошлых дат невозможно',
			PhotoCalendar::SLOT_IS_BUSY => 'Слот занят. Изменить состояние прошлых дат невозможно',
		];
	} else {
		$lockedHints = [
			PhotoCalendar::SLOT_IS_FREE => 'Зарезервировать фотографа возможно только на ближайшие три месяца от ближайшего рабочего дня',
			PhotoCalendar::SLOT_IS_CLOSED => 'Зарезервировать фотографа возможно только на ближайшие три месяца от ближайшего рабочего дня',
			PhotoCalendar::SLOT_IS_BUSY => 'Зарезервировать фотографа возможно только на ближайшие три месяца от ближайшего рабочего дня',
			PhotoCalendar::SLOT_IS_MY => 'Зарезервировать фотографа возможно только на ближайшие три месяца от ближайшего рабочего дня',
		];
	}
	
	return $lockedHints;
}



$hints = getHints($adminMode);

?>

<style>
#photo-calendar span.day {
	font-size: 24pt;
	color: red;
}

td.interval {
	font-size: 9pt;
	text-align: center;
}

td a.state<?=PhotoCalendar::SLOT_IS_FREE?> {
	color: green;
}



td a.state<?=PhotoCalendar::SLOT_IS_CLOSED?> {
	color: red;
}

td a.state<?=PhotoCalendar::SLOT_IS_BUSY?>,
td.past,
td.future,
td a.past,
td a.future {
	opacity: 0.4;
	cursor: default;
}

td a.state<?=PhotoCalendar::SLOT_IS_MY?>, 
td a.my {
	color: red;
	font-weight: bold;
	border: 1px solid red;
	opacity: 1;
}

</style>

<script>

$(document).ready(function() {
	
<?php if ($adminMode):?>
function changeIntervalState(el) {

	var titles = <?=json_encode(\frontend\models\PhotoCalendar::ADMIN_SLOT_STATE_TITLES, JSON_UNESCAPED_UNICODE)?>;
	$.ajax({
            url: '<?php echo Url::to(['admin/photo-calendar/change-interval']) ?>',
			type: 'get',
			data: {'date' : $(el).attr('data-date'), 'id' : $(el).attr('data-id')},
			success: function (data) {
			    $(el).removeClass('state1');
				$(el).removeClass('state0');
				$(el).addClass('state' + data);
				$(el).attr('title', titles[data]);				
			}

      });
	  return false;
}

$("td.interval a.state1").click(
    function ()
    {
		return changeIntervalState(this);
    }
);

$("td.interval a.state0").click(
    function ()
    {	
		return changeIntervalState(this);
    }
);


$("#photo-calendar select#monthSelected").change(function() {	
	document.location.href = "<?=Url::to(['/admin/photo-calendar/manage'])?>?month=" + $("#photo-calendar select#monthSelected").val();
});
<?php else:?>

function setMy(el) {
	my = $('td a.my');
	my.attr('title', '<?=$hints[PhotoCalendar::SLOT_IS_FREE]?>');
	my.removeClass('my');	
	my.removeClass('state2');	
	
	el.addClass('my');	
	el.addClass('state0');	
	el.attr('title', '<?=$hints[PhotoCalendar::SLOT_IS_MY]?>');
	id = el.attr('data-id');
	console.log(id);
	date = el.attr('data-date');
	$("#<?=$inputInterval?>").val(id);
	$("#<?=$inputDate?>").val(date);
	
}

setMy($("a[data-id='<?=$model -> photographerInterval?>'][data-date='<?=$model -> photographerDate?>']"));


$("table.month").hide();
$("table.month-" + $("#photo-calendar select#monthSelected").val()).show();

$("#photo-calendar select#monthSelected").change(function() {
	$("table.month").hide();
	$("table.month-" + $(this).val()).show();
});


// выбор интервала
$("td.interval a").click(
function (){
	return false;
});

$("td.interval a.state0").click(
    function ()
    {	
		
		setMy($(this));
		return false;
    }
);


<?php endif?>

});

</script>


<?php	


// Возвращает HTML-текст интервала
function drawInterval($date, $id, $title, $adminMode = true) {
	
	$hints = getHints($adminMode);
	$lockedHints = getLockedHints($adminMode);
	
	if ($date) {
		
		if ($adminMode) {	// для админка	
		
			$pc = PhotoCalendar::byDate(date('d.m.Y', $date), $id);
			$state = $pc -> getState();
			$hint = isPast($date)?$lockedHints[$state]:$hints[$state];
			$title = Html::a($title, '#', ['title' => $hint, 'data-date' => date('d.m.Y', $date), 'data-id' => $id, 'class' => 'state'.$state.(isPast($date)?' past':'')]);
		} else {		// для владельца
			
			$isPresent = !isPast($date) && !isFuture($date);			
			if ($isPresent) {
				$pc = PhotoCalendar::byDate(date('d.m.Y', $date), $id);
				$state = $pc -> getState();
			} else { // слот из будущего или прошлого или мой
				$pc = null;
				$state = PhotoCalendar::SLOT_IS_BUSY;
				/*
				if (isMy($date, $id)) {
					$state = PhotoCalendar::SLOT_IS_MY;
				}
				*/
			}
			$hint = (!$isPresent)?$lockedHints[$state]:$hints[$state];//.(isPresent?'isPresent':'noPresent');
			$title = Html::a($title, '#', [
					'title' => $hint, 
					'data-date' => date('d.m.Y', $date), 
					'data-id' => $id, 
					'class' => 'state'.(!is_null($pc) ? $state:'').(						
							isPast($date)?' past':(isFuture($date)?' future':'')						
					)
			]);			
		}
	} else $title = '';
	
	
	
	return $title;
	
}

// Дата передается в числовом формате
// Проверяет, является ли слот моим (сравнение со значениями в полях)
/*
function isMy($date, $interval) {
		
	echo $date .' -> '.strtotime($model -> photographerDate).'<br>';
	return isset($model) && !is_null($model) && ((strtotime($model -> photographerDate) == $date) && ($model -> photographerInterval == $interval));
}
*/

// Дата передается в числовом формате
function isPast($date) {
	return $date < strtotime(Entity::dbNow());
}

// Дата в будущем - больше трех месяцев
function isFuture($date) {
	if ($date > strtotime(Entity::dbNow())) {
		$date1 = new DateTime(date('y-m-d', $date));
		$date2 = new DateTime(Entity::dbNow());
		$interval = date_diff($date1, $date2);
		$month = $interval -> m + ($interval -> y * 12);
		$result = $month > 2;
	} else $result = false;
	
	return $result;


	
	return $date > strtotime(Entity::dbNow());
}

//Возвращает HTML-текст ячейки для отображения указанной даты в календаре
function drawDay($date, $adminMode = true) {
	$result = '';
	$day = $date?(int)date('d', $date):"";
	$my = $date?Yii::$app -> formatter -> asDate($date, 'MMM Y'):"";
	$result = $result."<table class='stop-order' border='0'><tr><td class='".(isPast($date)?' past':'')."'><span class='day'>$day</span> $my</td></tr>";
	
		$intervals = PhotoCalendar::getIntervals();
		
		
		foreach($intervals as $id => $title) {
			
			$title = drawInterval($date, $id, $title, $adminMode);
			$result = $result."<tr><td class='interval id".$id."'>$title</tr></td>";
		}
	
	
	
	$result = $result."</table>";
	
	return $result;
};


// $visible - скрыть или не надо таблицу с месяцем
function drawMonth($month, $year, $adminMode = true, $visible = true) {
	
	$dayCount = cal_days_in_month(CAL_GREGORIAN, $month, $year);

	$matrix = [1 => [], 2 => [], 3 => [], 4 => [], 5 => [], 6 => [], 7 => []];
	
	for($day = 1; $day <= $dayCount; $day++) {		
		$date  = mktime(0,0,0, $month, $day, $year);
		$wd = date('N', $date);
				
		if ($day == 1) {
			for($i = 1; $i < $wd; $i++) { // заполняем незаполненные дни с прошлого месяца.
				$matrix[$i] = [''];
			};			
		}
		$list = $matrix[$wd];
		$list[] = $day;		
		$matrix[$wd] = $list;
			
	};
	
	$weekDays = [1 => 'Пн', 2 => 'Вт', 3 => 'Ср', 4 => 'Чт', 5 => 'Пт', 6 => 'Сб', 7 => 'Вс'];
	echo "<table width='100%' class='month month-".$month."'".($visible?"":" style='display:none'").">";
	for ($wd = 1; $wd <= 7; $wd++) {
		$list = $matrix[$wd];
		echo "<tr>" ;
		
		echo "<td class='head'>".$weekDays[$wd]."</td>" ;
		foreach($list as $day) {		
			$day = drawDay($day == ""?false:mktime(0,0,0, $month, $day, $year), $adminMode);
			echo "<td class='day$wd'>$day</td>" ;		
		}
		
		echo "</tr>" ;
	};
	echo "</table>";

	
}





?>


<div id="photo-calendar">
<select id="monthSelected">
<?php
	$year = (int)date('Y', strtotime(Entity::dbNow()));	
	$curYear = $year;
	$cMonth = (int)date('m', strtotime(Entity::dbNow()));
	
	$monthList = EntityHelper::monthList();
	if ($adminMode) {
		foreach($monthList as $id => $title)  {
			echo "<option value='".($id + 1)."'".($curMonth == $id + 1?" selected":"").">$title $year</option>";
		}
	} else {
		for($id = 0; $id < 3; $id++) {
			$month = $cMonth + $id;
			if ($month > 12) {				
				$month = $month - 12;				
				$year++;
			}
			$title = $monthList[$month - 1];
			echo "<option value='".$month."'".($curMonth == $month?" selected":"").">$title $year</option>";
		}		
	}

?>
</select>

<?php
	$year = $curYear;
	if ($adminMode) {
		drawMonth($curMonth, $year, $adminMode);
	} else {
		
		
		for($i = 0; $i <= 3; $i++) {
			//echo $month.'<br/>';
			$month = $cMonth + $i;			
			if ($month > 12) {
				$month = $month - 12;
				$year++;
			}
			drawMonth($month, $year, $adminMode, $curMonth == $month);
		
		}
	};


	
?>
<!--
<?php if (!$adminMode): ?>
<form method="GET" id="photo-form">
<input id="interval-input" value="<?=$model -> photographerInterval?>"></input>
<input id="date-input" value="<?=$model -> photographerDate?>"></input>


<button type="submit" class="reg-button next">Сохранить</button>

</form>
<?php endif ?>

-->
</div>
