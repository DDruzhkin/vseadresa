<?php
namespace common\widgets\photocalendar;

use Yii;
use yii\base\Widget;
use yii\web\View;
use yii\helpers\Url;

use common\models\Entity;
?>



<?php
class PhotoCalendar extends Widget {
//	
//	public $attribute;
//	public $label;

	public $model;	// модель адреса
	public $curMonth = false; // текущий отображаемый месяц
	public $date;
	public $mode = 'admin'; // Режим отображения - для даминистратора или для владелца адреса
	public $inputs; // id инпутов для обмена данными

	public function init() {
		parent::init();
		$view = Yii::$app->getView();
		$this->registerAssets();
		
		
		if (!$this -> curMonth) {// Если не указан текуший месяц, то попытаемся его вычислить
			if ($this -> model && $this -> inputs  && isset($this -> inputs['date'])) { // Если указан инпут date, то берем месяц из модели (значения должны совпадать в инпуте и в моделе)
				$this -> curMonth = (int)date('m', strtotime($this -> model -> photographerDate)); 
			} else { // иначе из текущей даты
				$this -> curMonth = $this -> getCurMonth(); 
			}
			
			
		}			
		
		//$view->registerJs($this->getJs());
//		$view->registerJs('/css/photocalendar.js');
//		$view->registerCssFile('/css/photocalendar.css');		
	}
	public function run() {		
	
		//return $this->render('photocalendar', ['model' => $this -> model, 'attribute' => $this -> attribute, 'label' => $this -> label]);
		return $this->render('photocalendar', [
			'model' => $this -> model, 
			'curMonth' => $this -> curMonth, 
			'adminMode' => $this -> mode == 'admin',
			'inputInterval' => $this -> inputs['interval'],
			'inputDate' => $this -> inputs['date'],
		]);
		
	}
	
	public function registerAssets()
	{
		$view = $this->getView();		
	}
	
	private function getJs() {
	
//		$this->view->registerJs($script, yii\web\View::POS_READY);
	
	}
	
	
	// Возвращает список месяцев за текущий период (год)
	public static function getMothList() {
		
		
	}
	
	
	public function getCurMonth() {
		return (int)date('m', strtotime(Entity::dbNow()));
	}
	
	
	
}
?>