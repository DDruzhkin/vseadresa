<?php
namespace commoon\widgets\photocalendar;
use yii\web\AssetBundle;
class PhotoCalendarAsset extends AssetBundle
{
	public $sourcePath = '@common/widgets/photocalendar/assets';
	public $css = [
		'css/photocalendar.css'
	];
	
	public $js = [
    	'js/photocalendar.js'
	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}

?>