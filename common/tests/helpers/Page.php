<?php

namespace common\tests\helpers;


use \Codeception\Actor;

class Page
{
    private $serverName;
    private $uri;
    private $tester;

    /**
     * Page constructor.
     * @param $url
     * @param $I
     */
    public function __construct($url, Actor $I)
    {
        $part = strpos($url, '/', 7);
        $this->serverName = substr($url, 0, $part);
        $this->uri = substr($url, $part);
        $this->tester = $I;
        $I->amOnPage($this->uri);
    }

    /**
     * @param $url
     * @return array
     */
    public function getLinks()
    {
        $I = $this->tester;
        $pageData = $I->grabPageSource();
        preg_match_all("/<a\s+(?:[^>]*?\s+)?href=[\"'](.*?)[\"']/", $pageData, $out, PREG_SET_ORDER);
        $URL = [];
        foreach ($out as $links)
            if (strpos($links[1], "/") === 0)
                array_push($URL, $this->serverName.$links[1]);
        return $URL;
    }

    public function errorCheck()
    {
        $I = $this->tester;
        try {
            $I->dontSee("Ошибка");
            $I->dontSee("Error");
        } catch (\Exception $e) {
            return $this->errorCheckExclusions();
        }
        return true;
    }

    private function errorCheckExclusions()
    {
        $I = $this->tester;

        // exclude insufficient rights errors
        // return false to stop processing, but do not add error
        $elms = $I->findElements(['css' => 'div.error-block .message']);
        if (!empty($elms))
        {
            $msg = $I->grabTextFrom(['css' => 'div.error-block .message']);
            if (mb_strpos($msg, 'Для совершения действия не достаточно прав.') !== false) {
                return true;
            }
        }

        // exclude 404 error statistics page
        $elms = $I->findElements(['css' => 'div.jumbotron h1']);
        if (!empty($elms))
        {
            $msg = $I->grabTextFrom(['css' => 'div.jumbotron h1']);
            if (mb_strpos($msg, 'Статистика по ошибкам 404') !== false) {
                return true;
            }
        }

        return false;
    }
}