<?php

namespace common\tests\helpers;

use GuzzleHttp\Client;
use yii\base\ErrorException;

class MailHandler
{
    private $client;

    function __construct($baseUri) {
        $this->client = new Client(['base_uri' => $baseUri]);
    }

    /**
     * Ищет в mocked mailbox-е письмо для указанного email адреса
     * @param $email - полностью email или имя без домена (всё, что до @)
     * @param $timeout - (optional, default = 60 sec) таймаут в секундах, через сколько прекратить ждать email
     * @return null|string
     * @throws \GuzzleHttp\Exception\GuzzleException|\yii\base\ErrorException
     */
    public function getMailTo($email, $timeout = 60) {
        $recipient = explode('@', $email)[0];

        $startTime = time();
        $mailBodyEncoded = null;
        while ($mailBodyEncoded == null && time() - $startTime < $timeout) {
            $response = $this->client->request('GET', 'api/v2/search?kind=to&query='.$recipient);
            if ($response->getStatusCode() >= 400) {
                throw new ErrorException("Error requesting mail from server");
            }
            if ($response == null || $response->getBody() == null) {
                sleep(5);
                continue;
            }
            $responseJson = json_decode((string)$response->getBody());
            if ($responseJson == null || $responseJson == '' || $responseJson->items == null || count($responseJson->items) < 1) {
                sleep(5);
                continue;
            }
            $mailBodyEncoded = $responseJson->items[0]->MIME->Parts;
        }
        if ($mailBodyEncoded == null) {
            return null;
        }

        return $this->decodeMessage($mailBodyEncoded);
    }

    /**
     * Удаляет все email-ы, для всех адресатов.
     * @throws ErrorException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deleteAllMails() {
        $response = $this->client->request('DELETE', 'api/v1/messages');
        if ($response->getStatusCode() >= 400) {
            throw new ErrorException("Error delete mails from server");
        }
    }

    private function decodeMessage($mimeParts) {
        $encoding = $mimeParts[1]->Headers->{'Content-Transfer-Encoding'}[0];
        $message = $mimeParts[1]->Body;

        if(strcasecmp($encoding,'BASE64') == 0) {
            $message = imap_base64($message);
        } else if(strcasecmp($encoding,'8BIT') == 0) {
            $message = imap_8bit($message);
        } else {
            $message = imap_qprint($message);
        }
        return $message;
    }
}