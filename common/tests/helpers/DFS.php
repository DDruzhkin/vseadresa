<?php

namespace common\tests\helpers;

use common\tests\helpers\Page;
use \Codeception\Actor;

class DFS
{
    private $visited;
    private $errorsFound = false;
    private static $REPETITIVE_URLS = [
        "/id=(\d+)/i",
        "/\S+_id=(\d+)/i",
        "/name=([\S^&]+)/i",
        "/page=(\d+)/i",
        "/address\/(\d+)/i",
        "/address\/nalog\/(\d+)/i",
        "/address\/update\/(\d+)/i",
        "/auction.+\/(\d+)/i",
        "/document\/show-pdf\/(\d+)/i",
        "/index\?page=(\d+)/i",
        "/user\/view\/(\d+)/i",
        "/order\/.+\/(\d+)/i",
        "/nalog\/(\d+)/i",
        "/update\/(\d+)/i"
    ];
    private static $FORBIDDEN_URLS = [
        "/@/",
        "/\/logout/i",
        "/sort=/i",
        "/bamp/i",
        "/delete\?id=\d+/i",
        "/\/show-pdf\//i"
    ];
    private $tester;

    private function __construct($URL, Actor $I)
    {
        $this->tester = $I;
        $this->visited = [];
    }

    public static function start($URL, Actor $I)
    {
        $instance = new DFS($URL, $I);
        $instance->dfs($URL);
        return $instance;
    }

    public function continueFrom($URL)
    {
        $this->dfs($URL);
    }

    public function errorsFound()
    {
        return $this->errorsFound;
    }

    private function dfs($URL, Array $path = [])
    {
        $I = $this->tester;
        if (array_search($URL, $this->visited) === false && !self::isForbiddenUrl($URL))
        {
            array_push($this->visited, $URL);
        }
        else
        {
            return;
        }
        $URL = self::normalizeUrl($URL);
        array_push($path, $URL);
        $page = new Page($URL, $I);
        if (!$page->errorCheck())
        {
            // report error and return in case error was found
            $this->errorsFound = true;
            $I->comment(implode(' -> ', $path));
            array_pop($path);
            return;
        }
        $notVisitedUrls = self::notVisitedNodes($page->getLinks());
        if (empty($notVisitedUrls))
        {
            // return if page doesn't have not visited URLs
            array_pop($path);
            return;
        }

        foreach ($notVisitedUrls as $url)
        {
            self::dfs($url, $path);
        }
    }


    private function notVisitedNodes($links)
    {
        $notVisitedNodes = array();
        foreach ($links as $link)
        {
            $match = [];
            if (DFS::isRepetitiveUrl($link, $match))
            {
                $linkTemplate = str_replace($match[1], "00", $link);
                if (array_search($linkTemplate, $this->visited) === false) {
                    array_push($notVisitedNodes, $link);
                    array_push($this->visited, $linkTemplate);
                }
            }
            else if (array_search($link, $notVisitedNodes) === false and array_search($link, $this->visited) === false)
            {
                array_push($notVisitedNodes, $link);
            }
        }
        return $notVisitedNodes;
    }

    private static function isRepetitiveUrl($url, &$match)
    {
        foreach (DFS::$REPETITIVE_URLS as $pattern)
        {
            if (preg_match($pattern, $url, $match))
                return true;
        }
        return false;
    }

    private static function isForbiddenUrl($url)
    {
        foreach (DFS::$FORBIDDEN_URLS as $pattern)
        {
            if (preg_match($pattern, $url))
                return true;
        }
        return false;
    }

    private static function normalizeUrl($url) {
        return str_replace('&amp;', '&', $url);
    }
}