<?php

namespace common\tests\unit\helpers;

use GuzzleHttp\Psr7\Response;

class UserTest extends \Codeception\Test\Unit
{
    /**
     * @throws \Exception
     */
    public function testReadMail_successPath()
    {
        $guzzleMock = $this->make(
            'GuzzleHttp\Client',['request' =>
                function() {
                    $response = new Response(200, [],
                        file_get_contents(codecept_data_dir() . 'mailhog_response.json'), '1.1', null);
                    return $response;
            }]
        );

        $mailMock = $this->make(
            'common\tests\helpers\MailHandler',['client' => $guzzleMock]
        );

        $mailBody = $mailMock->getMailTo("test@test.ru");
        $this->assertContains('Благодарим за успешную регистрацию', $mailBody);
    }

    /**
     * @throws \Exception
     */
    public function testReadMail_mailWithoutDomain()
    {
        $guzzleMock = $this->make(
            'GuzzleHttp\Client',['request' =>
                function() {
                    $response = new Response(200, [],
                        file_get_contents(codecept_data_dir() . 'mailhog_response.json'), '1.1', null);
                    return $response;
                }]
        );

        $mailMock = $this->make(
            'common\tests\helpers\MailHandler',['client' => $guzzleMock]
        );

        $mailBody = $mailMock->getMailTo("test");
        $this->assertContains('Благодарим за успешную регистрацию', $mailBody);
    }

    /**
     * @dataProvider providerErrorResponses
     * @throws \Exception
     */
    public function testReadMail_mailNotFound($responseStr, $result)
    {
        $guzzleMock = $this->make(
            'GuzzleHttp\Client',['request' =>
                function($responseStr) {
                    $response = new Response(200, [], $responseStr,
                        '1.1', null);
                    return $response;
                }]
        );

        $mailMock = $this->make(
            'common\tests\helpers\MailHandler',['client' => $guzzleMock]
        );

        try {
            $mailBody = $mailMock->getMailTo("test");
        } catch (\Exception $e) {
            $this->fail('Not expected exception occurs');
        }
        $this->assertNull($mailBody);
    }

    /**
     * @return array
     */
    public function providerErrorResponses()
    {
        return array(
            array('{"total": 0,"count": 0,"start": 0,"items": []}\'', null),
            array('', null),
            array('{}', null)
        );
    }

    /**
     *
     * @expectedException \Exception
     */
    public function testReadMail_unexpectedException()
    {
        $guzzleMock = $this->make(
            'GuzzleHttp\Client',['request' =>
                function() {
                    $response = new Response(500, [],
                        '', '1.1', null);
                    return $response;
                }]
        );

        $mailMock = $this->make(
            'common\tests\helpers\MailHandler',['client' => $guzzleMock]
        );

        $mailBody = $mailMock->getMailTo("test");
    }
}