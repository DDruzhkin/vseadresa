<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_services".
 *
 * @property integer $id
 * @property string $code
 * @property string $created_at
 * @property integer $service_price_id
 * @property integer $order_id
 * @property string $date_start
 * @property string $date_fin
 * @property integer $address_id
 * @property integer $owner_id
 * @property string $name
 * @property string $stype
 * @property string $duration
 * @property integer $status_id
 * @property string $price
 * @property string $summa
 * @property integer $invoice_id
 * @property integer $nds
 *
 * @property Document $invoice
 * @property Address $address
 * @property User $owner
 * @property Order $order
 * @property ServicePrice $servicePrice
 * @property Status $status
 */
class Service extends \common\models\EntityExt 
{
	
	/* Константы модели */
	const STYPE_ONCE = 'Разовая';
	const STYPE_MONTHLY = 'Ежемесячная';
	const DURATION_ONCE = 'разово';
	const DURATION_6 = '6 месяцев';
	const DURATION_11 = '11 месяцев';
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_services'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [
			'stype' => ['Разовая' => 'Разовая', 'Ежемесячная' => 'Ежемесячная', ],  
			'duration' => ['разово' => 'разово', '6 месяцев' => '6 месяцев', '11 месяцев' => '11 месяцев', ],  

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Заказанные услуги', 
			'parent' => 'objects', 
			'model' => 'Service', 
			'comment' => 'Услуги, находящиеся в заказах
capation: %name%', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'code' => ['label' => 'Код', 'visible' => '1', 'filter' => 'value', 'mask' => '99999', 'readonly' => '1', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
			'created_at' => ['label' => 'Дата', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'show' => '1', 'visible' => '1', 'comment' => 'Дата добавления услуги в заказ', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'readonly' => '1', 'required' => '1', ], 
			'service_price_id' => ['label' => 'Услуга', 'visible' => '1', 'link' => '1', 'comment' => 'Заказанная услуга', 'readonly' => '1', 'model' => 'ServicePrice', 'type' => 'ref', 'required' => '1', 'show' => '1', ], 
			'order_id' => ['label' => 'Заказ', 'filter' => 'like', 'visible' => '1', 'comment' => 'Заказ, в рамках которого была заказана услуга', 'link' => '1', 'readonly' => '1', 'model' => 'Order', 'type' => 'ref', 'required' => '0', 'show' => '1', ], 
			'date_start' => ['label' => 'Старт', 'comment' => 'Дата начала оказания услуги', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'show' => '1', 'visible' => '1', 'required' => '0', ], 
			'date_fin' => ['label' => 'Завершение', 'comment' => 'Дата завершения услуги. Определяется датой старта и длительностью услуги.', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'show' => '1', 'visible' => '1', 'readonly' => '1', 'required' => '0', ], 
			'address_id' => ['label' => 'Адрес', 'visible' => '1', 'filter' => 'like', 'link' => '1', 'comment' => 'Адрес, к которому относится услуга, определяется заказом.', 'readonly' => '1', 'model' => 'Address', 'type' => 'ref', 'required' => '0', 'show' => '1', ], 
			'owner_id' => ['label' => 'Владалец', 'visible' => '1', 'filter' => 'like', 'show' => '1', 'comment' => 'Владелец адреса, для которого заказана услуга. Определяется адресом.', 'link' => '1', 'readonly' => '1', 'model' => 'User', 'type' => 'ref', 'required' => '0', ], 
			'name' => ['label' => 'Название услуги', 'comment' => 'Название услуги,Список некоторых услуг владельцев, Предоставление юридического адреса, Почтовое обслуживание, Предоставление рабочего места', 'visible' => '1', 'filter' => 'like', 'readonly' => '1', 'show' => '0', 'type' => 'string', 'required' => '1', ], 
			'stype' => ['label' => 'Тип услуги', 'content' => 'Услуга является разовой или ежемесячной', 'visible' => '1', 'filter' => 'list', 'comment' => 'Является ли услуга разовой или оказывается ежемесячно, определяется справочником услуг', 'readonly' => '1', 'type' => 'enum', 'required' => '0', 'show' => '1', ], 
			'duration' => ['label' => 'Длительность', 'comment' => 'Длительность услуг заказа в месяцах, определяется заказом', 'visible' => '1', 'filter' => 'list', 'readonly' => '1', 'type' => 'enum', 'required' => '0', 'show' => '1', ], 
			'status_id' => ['label' => 'Статус', 'show' => '0', 'model' => 'Status', 'type' => 'ref', 'required' => '0', 'visible' => '0', 'link' => '0', ], 
			'price' => ['label' => 'Стоимость, руб', 'comment' => 'Полная стоимость оказания услуги', 'visible' => '1', 'format' => '[\'decimal\', 2]', 'readonly' => '1', 'mask' => '@,money', 'type' => 'decimal', 'required' => '0', 'show' => '1', ], 
			'summa' => ['label' => 'Стоимость, руб', 'comment' => 'Полная стоимость оказания услуги', 'visible' => '1', 'format' => '[\'decimal\', 2]', 'readonly' => '1', 'mask' => '@,money', 'type' => 'decimal', 'required' => '0', 'show' => '1', ], 
			'invoice_id' => ['label' => 'Счет', 'visible' => '0', 'comment' => 'Счет, выставленный для оплаты заказанной услуги', 'link' => '0', 'readonly' => '1', 'model' => 'Document', 'type' => 'ref', 'required' => '0', 'show' => '1', ], 
			'nds' => ['label' => 'НДС', 'visible' => '0', 'comment' => 'Ставка НДС для данной услуги в %', 'readonly' => '0', 'default' => 18, 'type' => 'integer', 'required' => '1', 'show' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
		
	return array_merge($result,
	[
		
		[
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'create_time',
			
			'value' => new Expression('NOW()'),			
			
		],
	]);
	
		
}
	







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'service_price_id', 'name'], 'required'],
            [['id', 'service_price_id', 'order_id', 'address_id', 'owner_id', 'status_id', 'invoice_id', 'nds'], 'integer'],
            [['created_at', 'date_start', 'date_fin'], 'safe'],
            [['stype', 'duration'], 'string'],
            [['price', 'summa'], 'number'],
            [['code'], 'string', 'max' => 9],
            [['name'], 'string', 'max' => 50],
            [['id'], 'unique'],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::className(), 'targetAttribute' => ['invoice_id' => 'id']],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['service_price_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServicePrice::className(), 'targetAttribute' => ['service_price_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice() {
        return $this->hasOne(Document::className(), ['id' => 'invoice_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress() {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner() {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicePrice() {
        return $this->hasOne(ServicePrice::className(), ['id' => 'service_price_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus() {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }



    public static function captionTemplate(){

        return "%name%";
    }





}