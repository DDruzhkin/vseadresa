<?php
namespace common\models;

use yii\base\Behavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use common\models\User;
use common\models\Status;
use common\models\Notification;
use frontend\models\Document;
use frontend\models\DocumentType;



class UserBehavior extends Behavior {
	
	
	
	public static function getServicePriceListDescription() {
		return '
		Геттер для свойства getServicePriceList - справочник услуг владельца
		';
	}
	
	public function getServicePriceList() {
		$user = $this -> owner;		
		return $user -> getServicePrices() -> andWhere(['address_id' => NULL, 'enabled' => 1]) -> orderBy('service_name_id'); // сортировка нужна, чтобы системные группировались от пользовательских
	}
	
	
	public function statusByAlias($alias) {
		$model = $this -> owner;
		return Status::findOne(['alias' => $alias, 'table' => $model -> tableName()]);
	}
 	
	
	public static function getPortalRoleDescription() {
		return 
		'
		Возвращает роль пользователя в строковом виде для отображения в интерфейсе - Владелец, Заказчик, Админ, Портал
		';
	}
	
	public function getPortalRole() {
		$user = $this -> owner;				
		if ($user -> id == EntityExt::portal() -> id) return 'Портал';
		if ($user -> checkRole(User::ROLE_OWNER)) return 'Владелец';
		if ($user -> checkRole(User::ROLE_CUSTOMER)) return 'Заказчик';
		if ($user -> checkRole(User::ROLE_ADMIN)) return 'Администратор';
		return "-";
	}
	
	
	public static function hasPubleshedAddressDescription() {
		return 
		'
		Есть ли у пользователя опубликованные адреса
		';
	}
	
	public function hasPubleshedAddress() {
		$user = $this -> owner;		 
		if (!$user -> checkRole(User::ROLE_OWNER)) throw new ForbiddenHttpException('Не доступная для не Владельцев адресов функция');		
		return (boolean)(Address::find() -> andWhere(['owner_id' => $user -> id, 'status_id' => Address::statusByAlias('published') -> id]) -> count());
		
	}	
	
	public function checkRoleDescription() 
	{
		return '
			<code>public function checkRole($role) </code><br>			
			Проверка присутствия у пользователя роли<br>			
		';
	}
	
	public function checkRole($role) 
	{
		return in_array($role, $this -> owner -> role_as_array);
	}
	
	
	
	public static function setDefaultProfileDescription() 
	{
		return '
			<code>public function setDefaultProfile($id, $save = true)  </code><br>			
			Устанавливает реквизиты поумолчанию для пользователя<br>
			Если профиль не принадлежит текущему пользователю, то возникает исключение.<br>
			Данные пользователя будут сохранены, если $save = true<br>
		';
	}	
	
	public function setDefaultProfile($id, $save = true)  
	{
		$user = $this -> owner;	
		$r = Entity::byId($id);
		if (!$r) throw new Exception('Неверно указан id реквизитов: '.$id);
		if ($r -> user_id != $user -> id) throw new Exception('Попытка указать пользовтелю в качестве реквизитов по умолчанию чужие реквизиты');
		
		$user -> default_profile_id = $id;
		if ($save) $user -> save();
		
	}
	
	
	public static function addServicePriceDescription() 
	{
		return '
			<code>public function addServicePrice($serviceNameId = NULL, $save = false) </code><br>			
			Добавляет услугу в справочник владельца<br>
			Если ownerId не указан, то используется текущий пользователь<br>			
			Возвращает новую созданную услугу<br>			
		';
	}
	
	
	public function addServicePrice($serviceNameId = NULL, $save = false) 
	{		
		$user = $this -> owner;		
		$result = $user -> addSerivcePrice(
			[
				'service_name_id' => $serviceNameId, 
				'owner_id' => $user -> id
			]
		); // передаем только ключевые атрибуты с учетом, что все остальное присвоится в integrate		
		
		if ($save) $result -> save();
		return $result;
	}
	
	
	public static function requisiteListDescription() {
		return 
		'
		<code>public function requisiteList($template = false, $condition = false)</code><br/>
		Возвращает все имеющиеся варианты реквизитов у пользователя.<br/>		
		$condition - необязательный параметр, дополнительные условия на реквизиты, например, на форму собственности	<br/>	
		$template - необязательный параметр, позволяет задать шаблон вывода объектов в списке, например, caption: %form% %name% %inn%;<br/>
		';
	}
	

	public function requisiteList($condition = false, $template = false) {
		$result = [];
		if (!$condition || !array_key_exists('enabled', $condition)) {		
			$condition['enabled'] = 1;
		};
		
		
		if (!$condition || !array_key_exists('user_id', $condition)) {		
			$condition['user_id'] = $this -> owner -> id;
		};
		
		$result = ArrayHelper::merge(
			Rcompany::listAll($template, $condition),
			Rip::listAll($template, $condition),
			Rperson::listAll($template, $condition)
		);
		
		return $result;
	}
	
	public static function getContractDescription() {
		return '
		<code>getContractDescription()</code></br>		
		Возвращает контракт пользователя, если он сушествует</br>
		Если контрактов несколько, то возвращается последний<br/>
		Поиск контракт производится по всем реквизитам, не зависимо от их статуса (удален или нет)<br/>
		';
	}

	public function getContract() {
		$user = $this -> owner;	
		$list = array_keys($user -> requisiteList(['enabled' => [0,1]]));		
		$doc_types = [DocumentType::byAlias('contract') -> id];
		$documents = Document::find() -> andWhere(['doc_type_id' => $doc_types, 'recipient_id' => $list]);		
		$documents -> orderBy(['created_at' => SORT_DESC]);
		$documents -> limit(1);
		return  $documents -> one();		
	}	
	
	public static function getServiceIdListDescription() {
		return '
		<code>getServiceIdList()</code></br>		
		Список id услуг из справочника услуг</br>
		В список включены только активные услуги<br/>
		';
	}
	public function getServiceIdList() {
		$user = $this -> owner;	
		$list = $user  -> serviceList -> andFilterWhere(['enabled' => 1]) -> all();					
		$result = [];
		
		foreach($list as $item) {								 
				$result[] = $item -> id;
		}
		
		return $result;
	}

	
	public function addOwnerSerivcePrice($params) {
		$user = $this -> owner;
		$params['owner_id'] = $user -> id;
		$params['address_id'] = NULL;
		return $user -> addSerivcePrice($params);
	}
	
	
	public function getServiceOptions() 
	{				
		$result = getServiceOptionsByList($this -> serviceIdList);
	}

	
	public static function getNotificationsDescription() {
		return '
			Возврашает уведомления указанного типа для пользователя
		';
			
	}
	
	public function getNotifications($type) {
		$user = $this -> owner;
		return Notification::find() -> andWhere(['type' => $type, 'user_id' => $user -> id]) -> all();
	}
	
	public static function setNotificationsDescription() {
		return '
			Устанавливает уведомления указанного типа для пользователя.<br>
			Для этого передается тип устанавливаемых уведомлений ($type),<br>
			Массив статусов с активными уведомлениями ($statuses),<br>
			Массив email, соответствующих статусам ($emails).<br>
		';
			
	}
	
	public function setNotifications($type, $statuses, $emails, $nnames) {


		$other = ($type == 'other');
		if (is_null($statuses)) $statuses = [];
		if (is_null($nnames)) $nnames = [];

		if (is_null($type) || is_null($statuses) || is_null($emails)) return false;
		$user = $this -> owner;
		$list = $user -> getNotifications($type);

		if (!$statuses) $statuses = [];
		if (!$emails) $emails = [];
		$enables = [];
		foreach($emails as $statusId => $email) { // в $emails должны быть все варианты статусов
			$enables[$statusId] = in_array($statusId, $statuses);
		}

		$exists = []; // список статусов, для которых уже существуют уведомления
		// обновим те, которые уже лежат в базе
		

		
		foreach($list as $item) {	

			$statusId = $other?$item -> nname:$item -> status_id;
			$exists[] = $statusId;
			if (array_key_exists($statusId, $emails)) { // Иначе не должно быть, но проверку сделаем
				$item -> enabled = $enables[$statusId];
				$item -> email = $emails[$statusId];
				$item -> nname = $nnames[$statusId];				
				//$item -> nname = '';
				$item -> save();
			}		
			
		};
	

	
		// добавим те, которые не созданы
		
		$list = array_diff(array_keys($emails), $exists);		

		
		foreach($list as $statusId) {
			$item = new Notification();
			
			if (!$other) $item -> status_id = $statusId;
			//$item -> type = $other?$statusId:$type;			
			$item -> type = $type;			
			$item -> user_id = $user -> id;
			$item -> enabled = $enables[$statusId];			
			$item -> nname = $nnames[$statusId];			
			$item -> email = trim($emails[$statusId]);	

			$item -> save();
		}
		
		
		return true;
	}
	
	
	public static function ownerAgreementDescription() {
		
		
	}
	
	
	public function ownerAgreement($contract_id = false) {
		$user = $this -> owner;
		if (is_null($user) || !$user -> checkRole(User::ROLE_OWNER)) 
			throw new ForbiddenHttpException ('Нет прав на создания дополнительного соглашения - разрешено только Владельцам адреса');
				
		
		$owner_r = $user -> default_profile_id;
		$portal_r = User::portal() -> default_profile_id;
		
		$r = Entity::byId($owner_r);	
		
		if (!$contract_id) {		
			$contract_id = $r -> contract_id;			
		};
		
		if (!$contract_id) { // Договор не привязан - найдем и привяжем
			$cType = DocumentType::byAlias('contract');
			$contract = Document::findOne(['doc_type_id' => $cType, 'recipient_id' => $owner_r]);
			if ($contract) {
				$contract_id = $contract -> id;
				$r -> contract_id = $contract_id;
				$r -> save();
			}
		}
		
		
		
		if (!$contract_id) throw new NotFoundHttpException('Невозможно создать дополнительное соглашение без договора');
		$contract = Document::findOne([$contract_id]);
		if (is_null($contract)) throw new NotFoundHttpException('Невозможно создать дополнительное соглашение без договора');
		
		$portal_r = $contract-> owner_id;
		$owner_r = $contract-> recipient_id;
		
		// попытаемся найти неподписанное соглашение, его и будем менять
		$documentType = DocumentType::byAlias('agreement');
		
		$agreement = Document::findOne(['doc_type_id' => $documentType, 'recipient_id' => $owner_r, 'state' => 0]);
		
		if (!$agreement) $agreement = $documentType -> createDocument(); // Если нет не подписанных, то создаем новое
		
		$agreement -> owner_id = $portal_r;
		$agreement -> recipient_id = $owner_r;
		$agreement -> state = Document::CONTRACT_STATE_DEACTIVE;
		$agreement -> save();		
		
		$r -> contract_id = $agreement -> id;
		$r -> save();
		
		return $agreement;
		
	}
	
	public static function ownerContractingDescription() {
		return '
			<code>public function ownerContracting($template = false, $owner_r = false, $portal_r = false)</code><br>
			Создание договора между владельцем и порталом<br>
			$template - шаблон, на основе которого сформировать договор. Если не указано, то используется по умолчанию, указанный для договоров<br>
			$owner_r - реквизиты  владелца, на которые сформировать договор. Если не указано, то используются реквизиты по умолчанию для текущего владельца<br>
			$portal_r - реквизиты портала, на которые сформировать договор. Если не указано, то используются реквизиты портала по умолчанию<br>
			Формирование договора между владельцем и порталом<br>
			Проверка на сущуствование договора не производится. То есть при вызове этого метода договор всегда создается новый.<br>
			
			Метод доступен только для Владельцов.
		';
			
	}
	
	
	public function ownerContracting($template = false, $owner_r = false, $portal_r = false) {
		
		//$user = User::getCurrentUser();
		$user = $this -> owner;
		if (is_null($user) || !$user -> checkRole(User::ROLE_OWNER)) 
			throw new ForbiddenHttpException ('Нет прав на создания договора - разрешено только Владельцам адреса');
				
		
		
		if (!$owner_r)  $owner_r = $user -> default_profile_id;		
		
		$r = Entity::byId($owner_r);
		
		if ($r ) {
			$contract = $r -> contract;	
			
			if (!$contract || ($contract -> recipient_id != $owner_r)) {
		
				$documentType = DocumentType::byAlias($r -> prior_id?'agreement':'contract'); //Если реквизиты не менялись, то договор, иначе допник.
				$contract = $documentType -> createDocument();
	
		
				if (!$portal_r)  $portal_r = User::portal() -> default_profile_id;
				if (!$portal_r) {		
					throw new NotFoundHttpException('Не заданы реквизиты портала');
				}
				
				$contract -> owner_id = $portal_r;
				$contract -> recipient_id = $owner_r;
				$contract -> state = Document::CONTRACT_STATE_DEACTIVE;
				$contract -> removeFile();
				$contract -> save();		
				
				$r -> contract_id = $contract -> id;						
				$r -> save();
				
			}
		} else {
					throw new NotFoundHttpException('Не заданы реквизиты пользователя');
		}
		
		return $contract;
	
	}
	
	public static function getContractsDescription() {
		return '
		Список всех договоров пользователя.
		Если у пользователя ни одного договора, то создается один.
		';
	}
	
	
	public function getContracts() {
		$user = $this -> owner;
		$list = array_keys($user -> requisiteList());	

		$contract_type_id = DocumentType::byAlias('contract') -> id;
		$documents = Document::find() -> andWhere(['doc_type_id' => $contract_type_id, 'recipient_id' => $list]) -> all();		
		
		if (count($documents) == 0){
			// ни одного договора - создаем
			$documents = [$user -> ownerContracting()];
		}
		
		return $documents;
	}
	
	public static function getActiveDescription() {
		return 
		'
		геттер для свойства active - пользователь имеет статус active
		';
	}
	
	// TODO: где-то еще описано это свойство для User - найти и удалить.
	public function getActive() {
		$user = $this -> owner;
		$result = $user -> status -> alias == 'active';
		
		/* Смену статуса перенесем в смену статуса договора
		if (!$result) {
			
			if (($user -> status -> alias == 'new')) {
				$contracts = $this -> getActualePortalContracts();
				var_dump($contracts); exit;
				if (is_array($contracts) && count($contracts)) {
					
					$status = Status::findOne(['alias' => 'active', 'table' => User::tableName()]);
					
					$user -> status_id = $status -> id; // активируем, если не был активным и есть подпсанный контракт					
					$user -> save();
					$result = true;
				}	
			}
		}
		*/
		
		return $result;
	//	return $user ->status == Status::byAlias
	}
	
	
	public static function getActualePortalContractsDescription() {
		return 
		'
		Получить список актуальных договоров с порталом
		';
	}	
	
	
	public function getActualePortalContracts() {
		$user = $this -> owner;
		
		$user_r = array_keys($user -> requisiteList());
		$portal_r = array_keys(User::portal()-> requisiteList());
		$documentType = DocumentType::byAlias('contract');		
		$result = Document::find() -> andWhere(['doc_type_id' => $documentType -> id, 'owner_id' => $portal_r, 'recipient_id' => $user_r]) -> all();
		return $result;
	}
	
	public static function getDefaultProfileDescription() {
		return 
		'
		Возвращает реквизиты по умолчанию для пользователя
		';
	}	
	
	public function getDefaultProfile() {
		$user = $this -> owner;
		
		$id = $user -> default_profile_id;
		if ($id) {
			$result = Entity::byId($id);
		} else $result = null;
		
		return $result;
	}
	
	public static function getAdminMenuItemsDescription() {
		return
		'
		Возвращает пункты меню для пользователя в ЛКА, в зависимости от текущего состояния пользователя
		';
	}
	
	public function getAdminMenuItems() {
		$user = $this -> owner;		
		$menuItems = [];
		if ($user -> checkRole(User::ROLE_OWNER)) {
			$statuses = $user -> status -> getTo() -> all();
			$statuses = array_flip(Entity::objectsByTemplate($statuses, '%alias%'));
			$labels = ['active' => 'Активировать', 'blocked' => 'Заблокировать'];
			
		
			foreach($labels as $alias => $label) {
				if (!array_key_exists($alias, $statuses)) continue;
				$menuItems[] = [
					'label' => $label,				
					'url' => Url::to(['/admin/user/to-'.$alias, 'id' => $user -> id]),
				];
			};
		}
		$menuItems[] = [
			'label' => 'Приоритет',
			'items' => [
				[
					'label' => 'Максимальный',
					'url' => Url::to(['/admin/user/priority', 'id' => $user -> id, 'priority' => 9999]),
				],
				
				[
					'label' => 'Минимальный',
					'url' => Url::to(['/admin/user/priority', 'id' => $user -> id, 'priority' => 0]),
				],
			]
		];
		

		if (count($menuItems)) $menuItems[] = '<li class="divider"></li>';
		
		$menuItems[] = [
			'label' => 'Заказы',				
			'url' => Url::to(['/admin/order/', 'user_id' => $user -> id]),
		];

		if ($user -> checkRole(User::ROLE_OWNER)) {		
			$menuItems[] = [
				'label' => 'Адреса',				
				'url' => Url::to(['/admin/address/', 'owner_id' => $user -> id]),
			];
		}
		
		$menuItems[] = [
			'label' => 'Расчеты по адресам',				
			'url' => Url::to(['/admin/payment/', 'user_id' => $user -> id]),
		];
		
		$menuItems[] = [
			'label' => 'Оплаты',				
			'url' => Url::to(['/admin/payment/', 'payer_id' => $user -> id]),
		];
		$menuItems[] = [
			'label' => 'Услуги портала',				
			'url' => Url::to(['/admin/porder/', 'user_id' => $user -> id]),
		];
		
		
		if ($user -> checkRole(User::ROLE_CUSTOMER)) {		
		
			$menuItems[] = [
				'label' => 'Статистика по заказам',
				 
					'items' => [					
							['label' => 'Владельцам', 'url' => Url::to(['/admin/stat/orders', 'user_id' => $user -> id])],
							['label' => 'Порталу', 'url' => Url::to(['/admin/stat/portal-orders', 'user_id' => $user -> id])],					
					]
				
				
			];
		}
		
		if ($user -> checkRole(User::ROLE_OWNER)) {		
		
			$menuItems[] = [
				'label' => 'Статистика по заказам',
				 
					'items' => [					
							['label' => 'От Заказчиков', 'url' => Url::to(['/admin/stat/orders', 'user_id' => $user -> id])],
							['label' => 'Порталу', 'url' => Url::to(['/admin/stat/portal-orders', 'user_id' => $user -> id])],					
					]
				
				
			];
		}
		
		$menuItems[] = '<li class="divider"></li>';
		
		$menuItems[] = [
			'label' => 'Изменить',				
			'url' => Url::to(['/admin/user/update/', 'id' => $user -> id]),
		];
		
		$menuItems[] = [
			'label' => 'Карточка',				
			'url' => Url::to(['/admin/user/view/', 'id' => $user -> id]),
		];
		
		
		
		return $menuItems;
		
		
		
	}
	
	
}


