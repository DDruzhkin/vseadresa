<?php
namespace common\models;

use yii\base\Behavior;
use common\models\Service;
use common\models\Order;
use common\models\EntityExt;

class ServicePriceBehavior extends Behavior
{
	
	
	
	public function createServiceDescription() {
		return 
		'<code>public function createService($params, $model = NULL, $save = false);</code><br/>
		Создать услугу в заказ на основе текущей позиции прайс-листа. Можно создать на базе существующего объекта $model, либо функция создаст новый.<br/>
		В качестве заказчика строго устанавливаетя текущий пользователь в целях безопасности - подмены заказчика.<br/>
		Пример использования: <br/>
		<code>ServicePrice::findone(953) -> createService(["order_id" => 1303]);</code>
		';
	}
	
	// Обязательные параметры: order_id
	public function createService($params, $model = NULL, $save = false) {
		if (!$model) $model = new Service();
		$params['service_price_id'] = $this -> owner -> id;
		$params['customer_id'] = Entity::currentUser();
		$model -> load($params, '');				
		$model -> integrate();		
		return $model;
	}
	
	public function getOwnerServicePriceDescription() {
		return '
			Возвращает соответствующую услугу в справочнике владельца<br>
			Если текущая оказывается сама исправочника, то возвращает сама себя<br>
		';
	}
	
	public function getOwnerServicePrice() {
		$servicePrice = $this -> owner;
		
		
		if ($servicePrice -> address_id) {
			$result = ServicePrice::find() -> andFilterWhere(['owner_id'=>$servicePrice -> owner_id]) -> andWhere(['address_id' => NULL]);
			if ($servicePrice -> service_name_id == NULL) {
				$result -> andWhere(['service_name_id' => NULL, 'name' => $servicePrice -> name]);
			} else {
				$result -> andFilterWhere(['service_name_id'=>$servicePrice -> service_name_id]);
			}
			///if ($servicePrice -> id == 2058) {var_dump($result -> createCommand()->getRawSql()); exit;}
			if ($result) $result = $result -> one();
			
		} else $result = $servicePrice;
		
		return $result;
	}
	
	public function getAddressServicePriceDescription() 
	{
		return '
			Возвращает соответствующую услугу в справочнике адреса<br>
			Если текущая оказывается сама из справочника адреса, то возвращает сама себя<br>
		';
	}
	
	public function getAddressServicePrice($addressId) {
		$servicePrice = $this -> owner;		

		if (!$servicePrice -> address_id) {
			$result = ServicePrice::find() -> andFilterWhere(['owner_id'=>$servicePrice -> owner_id, 'address_id' => $addressId]);
			
			if ($servicePrice -> service_name_id == NULL) {
				$result -> andWhere(['service_name_id' => NULL, 'name' => $servicePrice -> name]);
			} else {
				$result -> andFilterWhere(['service_name_id'=>$servicePrice -> service_name_id]);
			}			
			if ($result) $result = $result -> one();
			
		} else $result = $servicePrice;
		
		return $result;
	}
	
	public function addAddressServicePriceDescription() 
	{
		return '
			<code>public function addAddressServicePrice($addressId, $save = false) </code><br>			
			Добавляет услугу в справочник адреса на основе услуги из справочника Владельца<br>
			$address_id - ИД адреса, в справочник которого добавить услугу<br>
			Проверка на то, что текущая услуга относится к справочнику владельца не произв<br>
			Возвращает новую созданную услугу<br>			
		';
	}
	
	
	public function addAddressServicePrice($addressId, $save = false) 
	{		
		
		$servicePrice = $this -> owner;		
		$result = $servicePrice -> addSerivcePrice(
		
			[
				'service_name_id' => $servicePrice -> service_name_id, 
				'address_id' => $addressId,
				'name' => $servicePrice -> name,
			]
		); // передаем только ключевые атрибуты с учетом, что все остальное присвоится в integrate		
		
		if ($save) $result -> save();
		return $result;
	}
	
	
	
	// числовое значение цены. Если услуга длительная, то возвращатся максимальное значение
	public function getPriceValue() {	
		$servicePrice = $this -> owner;			
		return $servicePrice -> stype == ServicePrice::STYPE_ONCE?$servicePrice -> price:MAX($servicePrice -> price6, $servicePrice -> price11);
	}
	
	// Отображение цены услуги
	public function getPriceView() {	
		$servicePrice = $this -> owner;			
		if ($servicePrice -> stype == ServicePrice::STYPE_ONCE) {
			$p = (int)$servicePrice -> price;
			if ($p > 0) {
				$result = '<a title="Разовая стоимость услуги">'.$p.'</a>';
			} else {
				$result = '';
			}
		} else {
			$result = '<a title="Цена за месяц при заказе на 6 месяцев">'.((int)$servicePrice -> price6).'</a>/<a title="Цена за месяц при заказе на 11 месяцев">'.((int)$servicePrice -> price11).'</a>';
		}
		return $result;
	}
	
}
?>