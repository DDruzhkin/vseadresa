<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;

use backend\models\Address;
use backend\models\Auction;
use backend\models\User;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * AuctionSearch represents the model behind the search form about `\backend\models\Auction`.
 */
class AuctionSearch extends Model {
	
	
	public $date;	
	public $address_id;	
	public $owner_id;	
	public $status_id;	
	public $rate_id;	
	public $rate_count;	
	public $amount;	
	
	
	    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['status_id', 'rate_id', 'rate_count'], 'integer'],
            [['amount'], 'number'],
            ['address_id', 'string', 'length' => [3]],
            ['owner_id', 'string', 'length' => [3]],
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Auction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'id'=>SORT_DESC				
			]
     ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
		
		
						
				
				
					
				
		/* фильтры по daterangepicker */
		if ($this -> date) {
			list($dateFrom, $dateTo) = explode('-', $this -> date);
			$dateFrom = self::datetToSql($dateFrom);
			$dateTo = self::datetToSql($dateTo, 1);
			
			$query->andFilterWhere(['between', 'date', $dateFrom, $dateTo]);
		};
				
				
						
			
		/* фильтры по like для ссылки*/
		if ($this -> address_id) {			
			$concat = 'CONCAT('.Address::getCaptionTemplateAsStringList().')';

			$list = Address::find() -> andWhere(['like', $concat , $this->address_id])->asArray()->all();
			$list = ArrayHelper::getColumn($list, 'id');			
			if (count ($list) > 0) {
				$list = implode(',',$list);			
				$query->andWhere("address_id in ($list)");			
			} else { // Условие не выполнено
				$query->andWhere('false');
				return $dataProvider;
			}
		}
				
				
					
				
				
						
			
		/* фильтры по like для ссылки*/
		if ($this -> owner_id) {			
			$concat = 'CONCAT('.User::getCaptionTemplateAsStringList().')';

			$list = User::find() -> andWhere(['like', $concat , $this->owner_id])->asArray()->all();
			$list = ArrayHelper::getColumn($list, 'id');			
			if (count ($list) > 0) {
				$list = implode(',',$list);			
				$query->andWhere("owner_id in ($list)");			
			} else { // Условие не выполнено
				$query->andWhere('false');
				return $dataProvider;
			}
		}
				
				
					
				
				
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['status_id' => $this -> status_id > ""?(int)$this -> status_id:""]);
						
					
				
				
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['rate_id' => $this -> rate_id > ""?(int)$this -> rate_id:""]);
						
					
				
				
						
				
				
					
				
				
						
				
				
					
				
				
			

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
