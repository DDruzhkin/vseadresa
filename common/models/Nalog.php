<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_nalogs".
 *
 * @property integer $id
 * @property integer $number
 * @property integer $okrug_id
 * @property string $title
 * @property string $meta_description
 * @property string $seo_text
 * @property string $h1
 *
 * @property Address[] $addresses
 * @property Okrug $okrug
 */
class Nalog extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_nalogs'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Список налоговых инспекций', 
			'imp' => 'Налоговая инспекция', 
			'rodp' => 'Налоговой инспекции', 
			'vinp' => 'Налоговую инспекцию', 
			'model' => 'Nalog', 
			'caption' => 'ИФНС №%number%', 
			'parent' => 'objects', 
			'comment' => 'Справочник налоговых инспекций Москвы, может быть изменен с помощью администратора сайта.', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
					'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
					'number' => ['label' => 'Номер', 'comment' => 'Номер налоговой инспеции', 'filter' => 'value', 'readonly' => '0', 'type' => 'smallint', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
					'okrug_id' => ['label' => 'Округ', 'comment' => 'Округ, к которому относится налоговая инспекция', 'filter' => 'list', 'readonly' => '0', 'model' => 'Okrug', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 
					'title' => ['label' => 'Поле Title', 'show' => '1', 'visible' => '0', 'type' => 'string', 'required' => '0', ], 
					'meta_description' => ['label' => 'Поле Meta Description', 'show' => '1', 'visible' => '0', 'type' => 'string', 'required' => '0', ], 
					'seo_text' => ['label' => 'SEO Текст', 'show' => '1', 'visible' => '0', 'comment' => 'Для разбивки на колонки использовать &ltp&gt', 'type' => 'text', 'required' => '0', ], 
					'h1' => ['label' => 'h1', 'visible' => '0', 'type' => 'string', 'required' => '0', 'show' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	

	// Создать объект по указанному псевдониму, если такой псевдоним существует.
	public static function byNumber($value) 
	{
		return self::findOne(['number' => $value]);
	}







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
		
            [['id', 'number'], 'required'],
            [['id', 'number', 'okrug_id'], 'integer'],
            [['seo_text'], 'string'],
            [['title', 'meta_description'], 'string', 'max' => 255],
            [['h1'], 'string', 'max' => 50],
            
            [['okrug_id'], 'exist', 'skipOnError' => true, 'targetClass' => Okrug::className(), 'targetAttribute' => ['okrug_id' => 'id']],
			
			[['id'], 'unique'],
            [['number'], 'unique', 'message' => 'Такой номер налоговой уже существует в справочнике'],
			
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses() {
        return $this->hasMany(Address::className(), ['nalog_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOkrug() {
        return $this->hasOne(Okrug::className(), ['id' => 'okrug_id']);
    }



    public static function captionTemplate(){

        return "ИФНС №%number%";
    }





}