<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;



/**
 * This is the model class for table "adr_address_option".
 *
 * @property integer $id
 * @property integer $address_id
 * @property integer $option_id
 *
 * @property Address $address
 * @property Option $option
 */
class AddressOption extends \common\models\EntityExt
{




	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'adr_address_option';
	}




	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'model' => 'AddressOption',
			'comment' => 'Связующая таблица адреса и опций',

		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
		return [
			'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ],
			'address_id' => ['label' => 'Address ID', 'default' => '0', 'model' => 'Address', 'type' => 'ref', 'required' => '1', 'show' => '1', 'visible' => '1', 'link' => '0', ],
			'option_id' => ['label' => 'Option ID', 'default' => '0', 'model' => 'Option', 'type' => 'ref', 'required' => '1', 'show' => '1', 'visible' => '1', 'link' => '0', ],

		];
	}
	/* Поведения*/
	public function behaviors(){

		$result = [];
		$ns = "common\models\\";
		$class = $ns.$this -> meta()['model'].'Behavior';

		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};

		return $result;


	}








	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return array_merge(parent::rules(), [
			[['address_id', 'option_id'], 'integer'],
			[['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
			[['option_id'], 'exist', 'skipOnError' => true, 'targetClass' => Option::className(), 'targetAttribute' => ['option_id' => 'id']],
		]);
	}








	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAddress() {
		return $this->hasOne(Address::className(), ['id' => 'address_id']);
	}


	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOption() {
		return $this->hasOne(Option::className(), ['id' => 'option_id']);
	}



	public static function captionTemplate(){

		return "[%id%]";
	}





}