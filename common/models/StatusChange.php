<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;



/**
 * This is the model class for table "adr_status_changes".
 *
 * @property integer $id
 * @property string $date
 * @property integer $object_id
 * @property integer $status_old
 * @property integer $status
 * @property integer $actor_id
 *
 * @property Status $statusOld
 * @property Status $status0
 * @property User $actor
 */
class StatusChange extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_status_changes'; 
    }




	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Изменения статусов', 
			'model' => 'StatusChange', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
		return [
			'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'date' => ['comment' => 'Дата изменения', 'label' => 'Date', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'type' => 'timestamp', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'object_id' => ['comment' => 'объект, у которого был изменен статус', 'label' => 'Object ID', 'type' => 'integer', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'status_old' => ['comment' => 'Старый статус', 'label' => 'Status Old', 'model' => 'Status', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 
			'status' => ['comment' => 'Новый статус', 'label' => 'Status', 'model' => 'Status', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 
			'actor_id' => ['comment' => 'Кто изменил статус', 'label' => 'Actor ID', 'model' => 'User', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 
				
		];
	}
	/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['date'], 'safe'],
            [['object_id', 'status_old', 'status', 'actor_id'], 'integer'],
            [['status_old'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_old' => 'id']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status' => 'id']],
            [['actor_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['actor_id' => 'id']],
        ]);
    }
	

    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOld() {
        return $this->hasOne(Status::className(), ['id' => 'status_old']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0() {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActor() {
        return $this->hasOne(User::className(), ['id' => 'actor_id']);
    }



    public static function captionTemplate(){

        return "[%id%]";
    }





}