<?php

namespace common\models;

use frontend\models\Document;
use InvalidArgumentException;
use yii\web\NotFoundHttpException;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\Behavior;
use yii\helpers\Url;

class DocumentBehavior extends Behavior
{





	public static function createPaymentDescription() {
		return
		'
		<code>payment($params = false)</code>
		Создает подтвержденную оплату на основе платежного поручения (текущий документ)<br>		
		Если соответствующая оплата уже существует (идентификация по ссылке на платежку), то просто обновляются данные
		Могут быть переданы параметры для платежного поручения в $params<br>
		';
	}	
	
	// Оплата счета - создание платежа, изменение статуса счета
	public function createPayment($params = false) {
		
//		echo 'payment<br>';
//		var_dump($payment_order);
		$payment_order = $this -> owner;
		if ($payment_order -> doc_type -> alias != 'paymentorder') return; // Это не платежка - уходим
				
		// попробуем платеж найти в базе по платежке
		$payment = Payment::find() -> andWhere(['payment_order_id' => $payment_order -> id]) -> one(); 

		if (!$payment) {
			$payment = new Payment(); // Если не найден, то создаем новый
		};						
				
		
		// Инициализируем платеж
	
		$payment -> date = $payment_order -> created_at;
		if ($payment_order -> doc_type -> alias == 'paymentorder') {
			$payment -> ptype = Payment::PTYPE_BANK;
		}
		
		$payment -> value = $payment_order -> summa;
		$payment -> payment_order_id = $payment_order -> id;
		/*
		$payment -> invoice_id = $invoice -> id;
		$payment -> payer_id = $invoice -> recipient_id;
		$payment -> recipient_id = $invoice -> owner_id;		
		*/
		
		$payment -> payer_id = $payment_order -> owner_id;	// владелец платежки является плательщиком
		$payment -> recipient_id = $payment_order -> recipient_id;// получатель платежки является получателем платежа
		$payment -> confirmed = 1;
					
		if ($params) $payment -> load($params, '');		
		
		$payment -> save();		
		
				
		return $payment;
	}


	public static function getAdminMenuItemsDescription() {
		return
		'
		Возвращает пункты меню для документа, в Админке, в зависимости от текущего состояния заказа.
		';
	}

	public function getAdminMenuItems() {
		$model = $this -> owner;		
		$contract = $model;
		$sections = [];
		$conrollerId = strtolower($model -> baseClassName());
	
		$menuItems = false;
		if (in_array($model -> doc_type -> alias, ['contract', 'agreement'])) {
					$stateTo = [
						Document::CONTRACT_STATE_DEACTIVE => [Document::CONTRACT_STATE_SIGNING], 
						Document::CONTRACT_STATE_SIGNING => [Document::CONTRACT_STATE_ACTIVE, Document::CONTRACT_STATE_DEACTIVE],
						Document::CONTRACT_STATE_ACTIVE => [Document::CONTRACT_STATE_DEACTIVE]
					];
		
					$id = $contract -> id;
					
					$items = [];
					if (!is_null($contract)) {
						$state = $contract -> state;
						
						$avStates = $stateTo[is_null($state)?0:$state];
						$item = false;
						foreach($avStates as $state) {
							switch($state) {
								case Document::CONTRACT_STATE_DEACTIVE: {
									$item = [
										'label' => 'Деактивировать',
										'url' => Url::toRoute(['deactivate', 'id' => $id])
									]; break;
								}
								case Document::CONTRACT_STATE_SIGNING: {
									$item = [
										'label' => 'На подписание',
										'url' => Url::toRoute(['tosigning', 'id' => $id])
									]; break;
								}
								case Document::CONTRACT_STATE_ACTIVE: {
									$item = [
										'label' => 'Активировать',
										'url' => Url::toRoute(['activate', 'id' => $id])
									]; break;
								}
							};
							$menuItems[] = $item;
						}
					}
			
			
			
		}
		
		
		return $menuItems;
		
	}
		

	public static function removeFileDescription() {
		return '
		Удаляет сгенерированный файл документа.<br/>
		Необходим, например, при изменении реквизитов, которым соответствуте неподписанный документ.
		';
	}
		
	public function removeFile($save = true) {
		$model = $this->owner;
		if ($model -> filename) {			
			$fn = $model -> filename;
			$model -> filename = null;
			if ($save) $model -> save();
			$template = $this->owner->doc_type->template;			
			$fn = Yii::getAlias('@storage/documents/') . $template . '/'.$fn;
			if (file_exists($fn)) unlink($fn);
		}
	}
		
    public function generatePdf()
    {

        $template = $this->owner->doc_type->template;
		
		if (!$template)
			throw new NotFoundHttpException('Шаблон для типа документа "'.$this->owner->doc_type->getCaption().'" не задан. ');

        if (!is_dir(Yii::getAlias('@storage/documents/' . $template . '/'))) {
            mkdir(Yii::getAlias('@storage/documents/' . $template . '/'));
        }

        $services = null;

        if($order = $this->owner->order){
            
            $services = $order->getServices()->all();
        }

        $pathTemplate = Yii::getAlias('@storage/templates/documents/' . $template . '.php');

        if (!file_exists($pathTemplate) || !$template) {
            throw new InvalidArgumentException("Файл шаблона ". $template ." не найден.");
        }

        $content = Yii::$app->controller->renderPartial('//documents/pdf', [
            'view' => $pathTemplate,
            'model' => $this->owner,
            'order' => $order,
            'services' => $services,
        ]);
        

        if (!$this->owner->filename) {
            $this->owner->filename = uniqid(time()) . '.pdf';
            $updateDb = true;

        }
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
            'destination' => Pdf::DEST_FILE,
            'filename' => Yii::getAlias('@storage/documents/' . $template . '/') . $this->owner->filename,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px} .invoice-body{font-size: 10px !important',
            'options' => [
                'title' => 'Generating PDF',
                //'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
                //'SetHeader' => [date("r")],
                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);


        $pdf->render();
        if (isset($updateDb)) {
            Yii::$app->db->createCommand()
                ->update(Document::tableName(), ['filename' => $this->owner->filename], ['id' => $this->owner->id])
                ->execute();
        }


        return $this->owner->filename;

    }



    public function getPdf()
    {
        $fileName = $this->owner->filename;
        $template = $this->owner->doc_type->template;

        if (!$fileName || !file_exists($filePath = Yii::getAlias('@storage/documents/' . $template . '/' . $fileName)) ||
            filemtime($filePath) < strtotime($this->owner->updated_at) || true) {
            $fileName = $this->generatePdf();
        }
$fileName = $this->generatePdf();
		
        return Url::base(true) . Url::to(['/files/documents/' . $template . '/' . $fileName]);
    }



	public function getContractState() {
		$document = $this -> owner;
		$state = (is_null($document -> state)?0:$document -> state);		
		if (in_array($state, [Document::CONTRACT_STATE_DEACTIVE, Document::CONTRACT_STATE_ACTIVE, Document::CONTRACT_STATE_SIGNING])) {
			$list = ['Не действует','Действует','На подписании'];
			return $list[$state];
		} else throw new NotFoundHttpException('Неизвестное значение состояния для Договора: '.$document -> state);


	}

		
	public static function balanceDescription() {
		return
		'
		<code>balance($checkStatus = true; $save = false)</code>
		Вычисляет сумму оплаты по счету.<br>
		Если $checkStatus, то выставляет вычисленный статус оплачен для счета.<br>
		Если ни одной оплаты не было, то статус счета не затрагивается.<br>
		Возвращает оплаченную сумму по счету.<br>
		Для изменения статуса этот метод должен вызываться в местах оплаты
		';
	}
	
	public function balance($checkStatus = true, $save = true) {
		$invoice = $this -> owner;
		$query = (new \yii\db\Query()) 
			-> select('SUM(p.value) as value')
			-> from(Payment::tableName().' p')			
			-> andWhere(['invoice_id' => $invoice -> id, 'confirmed' => 1]);
		
		$oldState = $invoice -> state;
		$paied = ($query -> one())['value'];
		if ($checkStatus) {
			if ($invoice -> summa > $paied) { // не оплачен
				$invoice -> state = Invoice::STATE_NOPAID;				
			} else { // полная оплата
				$invoice -> state = Invoice::STATE_PAID;
			}			
		};
		$invoice -> changeState($oldState, $invoice -> state);
		if ($save) $invoice ->  save();
		
		return $paied;
		
	}

	
	public static function changeStateDescription() {
		return
		'
		<code>public function changeState($old, $new) {</code>
		Метод, который вызывается автоматически из balance, если состояние документа изменилось<br/>
		Вызо производится до записи документа
		';
	}
	
	public function changeState($old, $new) {
		$invoice = $this -> owner;
		/*
		$typeAlias = $invoice -> doc_type -> alias;
		if (($typeAlias == 'invoice') && ($old != $new)) {
			if ($new == Invoice::STATE_PAID) { // Счет был только что оплачен
				if ($invoice -> porder -> service -> alias == 'subscribe') { 
				
					$invoice -> address -> setSubscribe($invoice); // устанавливаем подписку
				}
			}
		}
		
		*/
		
	}
	
	
}