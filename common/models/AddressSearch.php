<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;

use frontend\models\Address;
use frontend\models\User;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * AddressSearch represents the model behind the search form about `\backend\models\Address`.
 */
class AddressSearch extends Model
{


    public $id;
    public $created_at;
    public $address;
    public $status_id;
    public $owner_id;
    public $nalog_id;
    public $metro_id;
    public $region_id;
    public $okrug_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_id', 'nalog_id', 'metro_id', 'region_id', 'okrug_id'], 'integer'],
            [['created_at', 'address'], 'safe'],
            ['owner_id', 'string', 'length' => [3]],
        ];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Address::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        /* фильтры по значению, по списку для ссылки*/
        $query->andFilterWhere(['id' => $this->id > "" ? (int)$this->id : ""]);


        /* фильтры по daterangepicker */
        if ($this->created_at) {
            list($dateFrom, $dateTo) = explode('-', $this->created_at);
            $dateFrom = self::datetToSql($dateFrom);
            $dateTo = self::datetToSql($dateTo, 1);

            $query->andFilterWhere(['between', 'created_at', $dateFrom, $dateTo]);
        };


        /* фильтры по like */
        $query->andFilterWhere(['like', 'address', $this->address]);


        /* фильтры по значению, по списку для ссылки*/
        $query->andFilterWhere(['status_id' => $this->status_id > "" ? (int)$this->status_id : ""]);


        /* фильтры по like для ссылки*/
        if ($this->owner_id) {
            $concat = 'CONCAT(' . User::getCaptionTemplateAsStringList() . ')';

            $list = User::find()->andWhere(['like', $concat, $this->owner_id])->asArray()->all();
            $list = ArrayHelper::getColumn($list, 'id');
            if (count($list) > 0) {
                $list = implode(',', $list);
                $query->andWhere("owner_id in ($list)");
            } else { // Условие не выполнено
                $query->andWhere('false');
                return $dataProvider;
            }
        }


        /* фильтры по значению, по списку для ссылки*/
        $query->andFilterWhere(['nalog_id' => $this->nalog_id > "" ? (int)$this->nalog_id : ""]);


        /* фильтры по значению, по списку для ссылки*/
        $query->andFilterWhere(['metro_id' => $this->metro_id > "" ? (int)$this->metro_id : ""]);


        /* фильтры по значению, по списку для ссылки*/
        $query->andFilterWhere(['region_id' => $this->region_id > "" ? (int)$this->region_id : ""]);


        /* фильтры по значению, по списку для ссылки*/
        $query->andFilterWhere(['okrug_id' => $this->okrug_id > "" ? (int)$this->okrug_id : ""]);


        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }

    public static function datetToSql($date, $addDays = 0)
    {
        $date = strtotime(trim($date)) + $addDays * 86400;
        $date = date("Y-m-d", $date);
        return $date;
    }

}
