<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;



/**
 * This is the model class for table "adr_seo".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $keywords
 *
 * @property Address[] $addresses
 * @property Metro[] $metros
 * @property Nalog[] $nalogs
 * @property Okrug[] $okrugs
 * @property Option[] $options
 * @property Page[] $pages
 * @property Topic[] $topics
 */
class Seo extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_seo'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'SEO-данные', 
			'model' => 'Seo', 
			'parent' => 'objects', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
		return [
			'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'title' => ['filter' => 'like', 'label' => 'Title', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'description' => ['visible' => '0', 'label' => 'Description', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
			'keywords' => ['visible' => '0', 'label' => 'Keywords', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
				
		];
	}
	/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['title', 'description', 'keywords'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses() {
        return $this->hasMany(Address::className(), ['seo_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetros() {
        return $this->hasMany(Metro::className(), ['seo_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNalogs() {
        return $this->hasMany(Nalog::className(), ['seo_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOkrugs() {
        return $this->hasMany(Okrug::className(), ['seo_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions() {
        return $this->hasMany(Option::className(), ['seo_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages() {
        return $this->hasMany(Page::className(), ['seo_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopics() {
        return $this->hasMany(Topic::className(), ['seo_id' => 'id']);
    }



    public static function captionTemplate(){

        return "%title%";
    }





}