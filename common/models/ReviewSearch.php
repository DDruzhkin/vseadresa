<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;

use backend\models\Review;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ReviewSearch represents the model behind the search form about `\backend\models\Review`.
 */
class ReviewSearch extends Model {
	
	
	public $created_at;	
	public $name;	
	public $email;	
	public $company;	
	public $status_id;	
	
	
	    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'name', 'email', 'company'], 'safe'],
            [['status_id'], 'integer'],
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Review::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'id'=>SORT_DESC				
			]
     ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
		
		
						
				
				
					
				
		/* фильтры по daterangepicker */
		if ($this -> created_at) {
			list($dateFrom, $dateTo) = explode('-', $this -> created_at);
			$dateFrom = self::datetToSql($dateFrom);
			$dateTo = self::datetToSql($dateTo, 1);
			
			$query->andFilterWhere(['between', 'created_at', $dateFrom, $dateTo]);
		};
				
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'name', $this->name]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'email', $this->email]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'company', $this->company]);
				
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['status_id' => $this -> status_id > ""?(int)$this -> status_id:""]);
						
					
				
				
			

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
