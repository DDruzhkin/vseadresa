<?php

namespace common\models;

/**
 * Class NotificationsVsStatusesSettings
 * @package common\models
 *
 * This is the model class for table "adr_notifications_vs_statuses_settings".
 *
 * @property integer $id
 * @property int $user_id
 * @property int $status_id
 * @property string $enabled
 * @property string $email
 *
 */
class NotificationsVsStatusesSettings extends CObject
{

    public static function tableName()
    {
        return 'adr_notifications_vs_statuses_settings';
    }

}