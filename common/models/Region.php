<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;



/**
 * This is the model class for table "adr_regions".
 *
 * @property integer $id
 * @property string $name
 * @property integer $okrug_id
 *
 * @property Address[] $addresses
 * @property Okrug $okrug
 */
class Region extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_regions'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Районы Москвы', 
			'rodp' => 'района', 
			'vinp' => 'район', 
			'imp' => 'район', 
			'model' => 'Region', 
			'parent' => 'objects', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
		return [
			'id' => ['show' => '0', 'label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'visible' => '0', ], 
			'name' => ['label' => 'Название района', 'filter' => 'like', 'type' => 'string', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'okrug_id' => ['label' => 'Округ', 'filter' => 'list', 'model' => 'Okrug', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 
				
		];
	}
	/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'name'], 'required'],
            [['id', 'okrug_id'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['okrug_id'], 'exist', 'skipOnError' => true, 'targetClass' => Okrug::className(), 'targetAttribute' => ['okrug_id' => 'id']],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses() {
        return $this->hasMany(Address::className(), ['region_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOkrug() {
        return $this->hasOne(Okrug::className(), ['id' => 'okrug_id']);
    }



    public static function captionTemplate(){

        return "%name%";
    }





}