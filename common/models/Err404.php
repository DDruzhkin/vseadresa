<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_err404".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $ip
 * @property string $method
 * @property string $url
 */
class Err404 extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_err404'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Статистика по ошибкам 404', 
			'model' => 'Err404', 
			'parent' => 'objects', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'created_at' => ['label' => 'Дата добавления', 'filter' => 'range', 'type' => 'datetime', 'show' => '1', 'visible' => '1', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'readonly' => '1', 'required' => '1', ], 
			'ip' => ['label' => 'IP', 'visible' => '1', 'filter' => 'like', 'type' => 'string', 'required' => '1', 'show' => '1', ], 
			'method' => ['label' => 'HTTP-метод', 'visible' => '1', 'filter' => 'list', 'type' => 'string', 'required' => '1', 'show' => '1', ], 
			'url' => ['label' => 'Url', 'visible' => '0', 'type' => 'text', 'required' => '1', 'show' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
		
	return array_merge($result,
	[
		
		[
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'create_time',
			
			'value' => new Expression('NOW()'),			
			
		],
	]);
	
		
}
	







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['created_at'], 'safe'],
            [['ip', 'method', 'url'], 'required'],
            [['url'], 'string'],
            [['ip'], 'string', 'max' => 32],
            [['method'], 'string', 'max' => 10],
        ]);
    }
	

	
	
	
	



    public static function captionTemplate(){

        return "[%id%]";
    }





}