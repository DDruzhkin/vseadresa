<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_porders".
 *
 * @property integer $id
 * @property string $number
 * @property string $created_at
 * @property string $updated_at
 * @property string $service_name
 * @property string $info
 * @property integer $service_id
 * @property integer $customer_id
 * @property integer $customer_r_id
 * @property integer $recipient_id
 * @property double $summa
 * @property integer $invoice_id
 *
 * @property Address[] $addresses
 * @property Address[] $addresses0
 * @property Document[] $documents
 * @property Order[] $orders
 * @property Order[] $orders0
 * @property CObject $recipient
 * @property CObject $customerR
 * @property ServicePortal $service
 * @property User $customer
 */
class Porder extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_porders'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Заказы портала', 
			'imp' => 'Заказ', 
			'rodp' => 'Заказа', 
			'vinp' => 'Заказ', 
			'model' => 'Porder', 
			'parent' => 'objects', 
			'caption' => '%service_name% (№%number%)', 
			'comment' => 'Список заказов порталу.', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['show' => '1', 'filter' => 'value', 'label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'visible' => '1', ], 
			'number' => ['label' => 'Номер', 'filter' => 'value', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'created_at' => ['label' => 'Дата создания', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'show' => '1', 'visible' => '1', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'readonly' => '1', 'required' => '0', ], 
			'updated_at' => ['label' => 'Дата изменения', 'show' => '0', 'readonly' => '1', 'type' => 'timestamp', 'required' => '0', 'visible' => '0', ], 
			'service_name' => ['label' => 'Название', 'comment' => 'Название заказанной услуги', 'visible' => '1', 'filter' => 'like', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
			'info' => ['label' => 'Дополнительная информация', 'visible' => '0', 'type' => 'text', 'required' => '0', 'show' => '1', ], 
			'service_id' => ['label' => 'Услуга', 'comment' => 'Заказанная услуга', 'visible' => '1', 'filter' => '1', 'link' => '1', 'model' => 'ServicePortal', 'type' => 'ref', 'required' => '0', 'show' => '1', ], 
			'customer_id' => ['label' => 'Заказчик', 'filter' => 'like', 'link' => '1', 'comment' => 'Пользователь, совершивший заказ', 'model' => 'User', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'customer_r_id' => ['label' => 'Реквизиты заказчика', 'filter' => 'like', 'link' => '1', 'comment' => 'Реквизиты заказчика, на которые будет оформлен заказ и необходимые документы', 'readonly' => '0', 'visible' => '0', 'condition' => '[user_id=>customer_id]', 'model' => 'CObject', 'type' => 'ref', 'required' => '0', 'show' => '1', ], 
			'recipient_id' => ['label' => 'Реквизиты портала', 'visible' => '0', 'comment' => 'Реквизиты получателя оплаты за усугу', 'readonly' => '0', 'link' => '0', 'condition' => '[]', 'model' => 'CObject', 'type' => 'ref', 'required' => '0', 'show' => '1', ], 
			'summa' => ['label' => 'Сумма', 'comment' => 'Общая стоимость заказа', 'visible' => '1', 'readonly' => '1', 'format' => 'currency', 'mask' => '@decimal', 'type' => 'double', 'required' => '0', 'show' => '1', ], 
			'invoice_id' => ['label' => 'Счет', 'visible' => '0', 'comment' => 'Счет, выставленный для оплаты услуги', 'readonly' => '1', 'link' => '0', 'type' => 'integer', 'required' => '0', 'show' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
		
	return array_merge($result,
	[
		
		[
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'create_time',
			'updatedAtAttribute' => 'update_time',
			'value' => new Expression('NOW()'),			
			
		],
	]);
	
		
}
	







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['created_at', 'updated_at'], 'safe'],
            [['info'], 'string'],
            [['service_id', 'customer_id', 'customer_r_id', 'recipient_id', 'invoice_id'], 'integer'],
            [['summa'], 'number'],
            [['number'], 'string', 'max' => 45],
            [['service_name'], 'string', 'max' => 50],
            [['recipient_id'], 'exist', 'skipOnError' => true, 'targetClass' => CObject::className(), 'targetAttribute' => ['recipient_id' => 'id']],
            [['customer_r_id'], 'exist', 'skipOnError' => true, 'targetClass' => CObject::className(), 'targetAttribute' => ['customer_r_id' => 'id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServicePortal::className(), 'targetAttribute' => ['service_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses() {
        return $this->hasMany(Address::className(), ['photo_order_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses0() {
        return $this->hasMany(Address::className(), ['subscribe_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments() {
        return $this->hasMany(Document::className(), ['porder_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Order::className(), ['delivery_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders0() {
        return $this->hasMany(Order::className(), ['guarantee_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient() {
        return $this->hasOne(CObject::className(), ['id' => 'recipient_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerR() {
        return $this->hasOne(CObject::className(), ['id' => 'customer_r_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService() {
        return $this->hasOne(ServicePortal::className(), ['id' => 'service_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer() {
        return $this->hasOne(User::className(), ['id' => 'customer_id']);
    }



    public static function captionTemplate(){

        return "%service_name% (№%number%)";
    }





}