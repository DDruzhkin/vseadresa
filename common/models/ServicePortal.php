<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;



/**
 * This is the model class for table "adr_service_portal".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $for
 * @property double $price
 * @property string $price_rules
 * @property integer $undeleted
 * @property integer $recipient_id
 *
 * @property Porder[] $porders
 * @property CObject $recipient
 */
class ServicePortal extends \common\models\EntityExt 
{
	
	/* Константы модели */
	const FOR_OWNER = 'владельца';
	const FOR_CUSTOMER = 'заказчика';
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_service_portal'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [
			'for' => ['владельца' => 'владельца', 'заказчика' => 'заказчика', ],  

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Справочник услуг портала', 
			'rodp' => 'услуги', 
			'vinp' => 'услугу', 
			'imp' => 'услуга', 
			'model' => 'ServicePortal', 
			'parent' => 'objects', 
			'comment' => 'Справочник услуг, оказываемых порталом как владельцам, так и заказчикам.', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
		return [
			'id' => ['title' => 'Код услуги', 'show' => '1', 'label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'visible' => '1', ], 
			'name' => ['label' => 'Название услуги', 'comment' => 'Название услуги, предоставляемой порталом', 'visible' => '1', 'filter' => 'like', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
			'alias' => ['label' => 'Псевдоним', 'comment' => 'Псевдоним услуги латиницей для поиска ее из програмного кода', 'visible' => '0', 'filter' => 'like', 'type' => 'string', 'required' => '0', 'show' => '1', ],
			'for' => ['label' => 'Услуга для', 'visible' => '1', 'filter' => 'list', 'comment' => 'Кому может быть оказана услуга', 'default' => 'владельца', 'type' => 'enum', 'required' => '1', 'show' => '1', ], 
			'price' => ['label' => 'Стоимость услуги', 'visible' => '1', 'format' => '[\'decimal\', 2]', 'mask' => '@money', 'type' => 'double', 'required' => '0', 'show' => '1', ], 
			'price_rules' => ['label' => 'Правила цены', 'comment' => 'Правила формирования цены в зависимости от дополнительных парамтеров', 'visible' => '0', 'type' => 'text', 'required' => '0', 'show' => '1', ], 
			'undeleted' => ['label' => 'Неудаляемая', 'comment' => 'Услуга является неудаляемой, этот статус возможно изменить только через разработчика', 'visible' => '0', 'readonly' => '1', 'type' => 'boolean', 'default' => '0', 'required' => '1', 'show' => '1', ], 
			'recipient_id' => ['label' => 'Реквизиты', 'visible' => '0', 'comment' => 'Реквизиты получателя оплаты за усугу', 'readonly' => '0', 'link' => '0', 'condition' => '[user_id=>owner_id]', 'model' => 'CObject', 'type' => 'ref', 'required' => '0', 'show' => '1', ], 

				
		];
	}
	/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	

	// Создать объект по указанному псевдониму, если такой псевдоним существует.
	public static function byAlias($value) 
	{
		return self::findOne(['alias' => $value]);
	}








    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id'], 'required'],
            [['id', 'recipient_id'], 'integer'],
            [['for', 'price_rules'], 'string'],
            [['price'], 'number'],
            [['name', 'alias'], 'string', 'max' => 45],
            [['undeleted'], 'string', 'max' => 1],
            [['id'], 'unique'],
            [['alias'], 'unique'],
            [['recipient_id'], 'exist', 'skipOnError' => true, 'targetClass' => CObject::className(), 'targetAttribute' => ['recipient_id' => 'id']],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPorders() {
        return $this->hasMany(Porder::className(), ['service_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient() {
        return $this->hasOne(CObject::className(), ['id' => 'recipient_id']);
    }




    public static function captionTemplate(){

        return "%name%";
    }





}