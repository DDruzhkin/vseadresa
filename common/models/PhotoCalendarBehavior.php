<?php
namespace common\models;

use yii\base\Behavior;
use common\models\Entity;
use common\models\User;
use common\models\Porder;
use common\models\ServicePortal;
use yii\base\Exception;
use yii\helpers\Inflector;
use yii\helpers\Url;

class PhotoCalendarBehavior extends Behavior {
	
	
	public static function getAdminMenuItemsDescription() {
		return
		'
		Возвращает пункты меню для пользователя в ЛКА, в зависимости от текущего состояния визита
		';
	}
	
	
	public function getAdminMenuItems() {
		$model = $this -> owner;	
		$menuItems	= [];
		if ($model -> address -> photo_order && !$model -> address -> photo_order -> isPaied) {			
			$menuItems[] = [
				'label' => 'Отменить визит',
				'url' => Url::to(['/admin/photo-calendar/cancel-visit', 'id' => $model -> id])
			];
		}
		
		if (count($menuItems)) $menuItems[] = '<li class="divider"></li>';
		
		$menuItems[] = [
			'label' => 'Карточка адреса',
			'url' => Url::to(['/admin/address/view', 'id' => $model -> address_id])
		];
		
		$menuItems[] = [
			'label' => 'Карточка Владельца',
			'url' => Url::to(['/admin/user/view', 'id' => $model -> user_id])
		];
		return $menuItems;

	
	}

}
	