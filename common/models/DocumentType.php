<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_document_types".
 *
 * @property integer $id
 * @property string $name
 * @property string $template
 * @property string $alias
 *
 * @property Document[] $documents
 */
class DocumentType extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_document_types'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Типы документов', 
			'imp' => 'Тип документа', 
			'rodp' => 'Типа документа', 
			'vinp' => 'Тип документа', 
			'model' => 'DocumentType', 
			'parent' => 'objects', 
			'sortattr' => 'name', 
			'sortdir' => 'asc', 
			'caption' => '%name%', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'name' => ['label' => 'Название', 'comment' => 'Человеко понятное название типа документа', 'type' => 'string', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'template' => ['label' => 'Шаблон', 'comment' => 'Шаблон для генерации документа в pdf', 'show' => '0', 'readonly' => '1', 'type' => 'text', 'required' => '0', 'visible' => '0', ], 
			'alias' => ['label' => 'Псевдоним', 'comment' => 'Внутренне имя типа документа для привязки ее к участию в бизнес-логике', 'visible' => '1', 'readonly' => '0', 'type' => 'string', 'required' => '1', 'show' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	

	// Создать объект по указанному псевдониму, если такой псевдоним существует.
	public static function byAlias($value) 
	{
		return self::findOne(['alias' => $value]);
	}







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'name', 'alias'], 'required'],
            [['id'], 'integer'],
            [['template'], 'string'],
            [['name'], 'string', 'max' => 50],
            [['alias'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['alias'], 'unique'],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments() {
        return $this->hasMany(Document::className(), ['doc_type_id' => 'id']);
    }



    public static function captionTemplate(){

        return "%name%";
    }





}