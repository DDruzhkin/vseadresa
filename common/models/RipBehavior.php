<?php
namespace common\models;

use yii;
use yii\base\Behavior;
use yii\helpers\Url;
use common\models\EntityExt;

class RipBehavior extends RequisiteBehavior
{

	public function getTitle() {
		$r = $this -> owner;
		$res = "ИП ".$r->lname.' ';
		if ($r->fname) $res = $res.mb_substr($r->fname, 0, 1).'.';
		if ($r->mname) $res = $res.mb_substr($r->mname, 0, 1).'.';
		return trim($res);
	}



	
	public function findEqual($attrs = false) {
		$attrs = [
			'inn', 
			'kpp',
			'ogrnip',
			'fname',
			'mname',
			'lname',
			'pasport_number',
			'pasport_date',
			'pasport_organ',
			'sv_seriya',
			'sv_nomer',
			'sv_date',
			'address',
			'phone',
			'bank_bik',
			'bank_name',
			'bank_cor',
			'account',
			'account_owner',
			'tax_system_vat',			
		];
				
		return parent::findEqual($attrs);
		
	}	
	

		
	public function getHeaderText($sideName) {
		$r = $this -> owner;
		$params = $r -> attributes;
		$params['sv_date'] = Yii::$app -> formatter -> asDate(strtotime($params['sv_date']), 'd MMMM Y');
		$params['sideName'] = $sideName;
		$template = '
			<strong>ИП {lname} {fname} {mname}</strong>  действующ(ая/ий) на основании свидетельства о регистрации серия {sv_seriya} № {sv_nomer} от {sv_date} года, именуем(ая/ый) в дальнейшем «{sideName}»
		';
		
		return EntityHelper::replaceContent($template, $params);
	}
	

		
	public function getFooterText() {
		$r = $this -> owner;
		$params = $r -> attributes;
		$template = '			
<strong>ИП {lname} {fname} {mname}</strong><br/>
{address}<br/>
ИНН {inn}, <br/>
ОГРНИП № {ogrnip}<br/>
р/с {account}<br/>
в {bank_name}<br/>
к/с {bank_cor}<br/>
БИК {bank_bik}<br/>
		';
		return EntityHelper::replaceContent($template, $params);
	}	

	

}

?>