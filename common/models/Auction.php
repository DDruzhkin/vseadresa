<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_auctions".
 *
 * @property integer $id
 * @property string $date
 * @property double $price
 * @property integer $address_id
 * @property integer $owner_id
 * @property integer $status_id
 * @property integer $rate_id
 * @property integer $rate_count
 * @property double $amount
 *
 * @property AuctionRate[] $auctionRates
 * @property AuctionRate $rate
 * @property Address $address
 * @property User $owner
 * @property Status $status
 */
class Auction extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_auctions'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Список аукционов', 
			'imp' => 'Аукцион', 
			'rodp' => 'Аукциона', 
			'vinp' => 'Аукцион', 
			'model' => 'Auction', 
			'parent' => 'objects', 
			'caption' => 'Аукцион %date_view%', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'date' => ['label' => 'Дата аукциона', 'type' => 'date', 'filter' => 'range', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'price' => ['label' => 'Максимальная ставка', 'show' => '0', 'default' => '0', 'type' => 'float', 'required' => '1', 'visible' => '0', ], 
			'address_id' => ['label' => 'Адрес победитель', 'filter' => 'like', 'model' => 'Address', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 
			'owner_id' => ['label' => 'Владелец адреса победителя', 'filter' => 'like', 'model' => 'User', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 
			'status_id' => ['label' => 'Статус аукциона', 'filter' => 'list', 'model' => 'Status', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 
			'rate_id' => ['label' => 'Ставка победитель', 'visible' => '1', 'filter' => 'list', 'model' => 'AuctionRate', 'type' => 'ref', 'required' => '0', 'show' => '1', 'link' => '0', ], 
			'rate_count' => ['label' => 'Число ставок', 'comment' => 'Число сделанных ставок', 'visible' => '1', 'filter' => 'list', 'default' => 0, 'type' => 'integer', 'required' => '1', 'show' => '1', ], 
			'amount' => ['label' => 'Объем ставок', 'comment' => 'Объем сделанных ставок', 'format' => '[\'decimal\', 2]', 'title' => 'Размер ставки', 'mask' => '@decimal', 'filter' => 'list', 'type' => 'float', 'required' => '0', 'show' => '1', 'visible' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	

	public static function statusByAlias($alias) {
			return Status::findOne(["alias" => $alias, "table" => self::tableName()]);
	}
			
			






    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['date'], 'safe'],
            [['price', 'amount'], 'number'],
            [['address_id', 'owner_id', 'status_id', 'rate_id', 'rate_count'], 'integer'],
            [['rate_id'], 'exist', 'skipOnError' => true, 'targetClass' => AuctionRate::className(), 'targetAttribute' => ['rate_id' => 'id']],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuctionRates() {
        return $this->hasMany(AuctionRate::className(), ['auction_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRate() {
        return $this->hasOne(AuctionRate::className(), ['id' => 'rate_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress() {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner() {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus() {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }



    public static function captionTemplate(){

        return "Аукцион %date_view%";
    }





}