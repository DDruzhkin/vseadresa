<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_invoices".
 *
 * @property integer $id
 *
 * @property Porder[] $porders
 */
class Invoice extends \common\models\EntityExt 
{
	
	/* Константы модели */
	const STATE_NOPAID = 0;
	const STATE_PAID = 1;
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_invoices'; 
    }



    public static function parentClassName(){
		return '\common\models\Document';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Список счетов', 
			'imp' => 'Счет', 
			'rodp' => 'Счета', 
			'vinp' => 'Счет', 
			'model' => 'Invoice', 
			'caption' => 'счет №%number% от \"%created_at_view%\"', 
			'parent' => 'documents', 
			'sortattr' => 'created_at', 
			'sortdir' => 'desc', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	







	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPorders() {
        return $this->hasMany(Porder::className(), ['invoice_id' => 'id']);
    }



    public static function captionTemplate(){

        return "счет №%number% от \"%created_at_view%\"";
    }





}