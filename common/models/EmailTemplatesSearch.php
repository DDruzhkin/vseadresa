<?php

namespace common\models;

use common\models\EmailTemplates;
use yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * AddressSearch represents the model behind the search form about `\backend\models\Address`.
 */
class EmailTemplatesSearch extends Model {
	
	protected $model;
	public $category;
	    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [            
            [['category'], 'string'],            
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

	public function getValueList($fieldName) {
		return $this -> model -> getValueList($fieldName);
	}
	
    public function search($params)
    {

        $this->load($params);

        $allModels = [];

		$this -> model = new EmailTemplate();		
		
        foreach($this -> model->getFileList('user') as $value){
			if ($this -> category && ($this -> category > '') && ($value['category'] != $this -> category)) continue;
			$name = $value['name'];		
			
			$value['type'] = Yii::t('app','Custom');
            $allModels[$name] = $value;
        }

        foreach($this -> model->getFileList('default') as $value){
			if ($this -> category && ($this -> category > '') && ($value['category'] != $this -> category)) continue;
			$name = $value['name'];			
			
            if(!isset($allModels[$name])){
				$value['type'] = Yii::t('app','Default');
				$allModels[$name] = $value;
				
            }
        };

		ArrayHelper::multisort($allModels, 'name');
		
		
        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);		
		
		
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
	
        return $dataProvider;
    }
	
	
}
