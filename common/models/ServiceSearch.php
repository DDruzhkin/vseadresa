<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;

use backend\models\Address;
use backend\models\Order;
use backend\models\Service;
use backend\models\User;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ServiceSearch represents the model behind the search form about `\backend\models\Service`.
 */
class ServiceSearch extends Model {
	
	
	public $code;	
	public $created_at;	
	public $order_id;	
	public $date_start;	
	public $date_fin;	
	public $address_id;	
	public $owner_id;	
	public $name;	
	public $stype;	
	public $duration;	
	
	
	    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'created_at', 'date_start', 'date_fin', 'name', 'stype', 'duration'], 'safe'],
            ['order_id', 'string', 'length' => [3]],
            ['address_id', 'string', 'length' => [3]],
            ['owner_id', 'string', 'length' => [3]],
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Service::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'id'=>SORT_DESC				
			]
     ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
		
		
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['code' => $this->code]);				
					
				
				
						
				
				
					
				
		/* фильтры по daterangepicker */
		if ($this -> created_at) {
			list($dateFrom, $dateTo) = explode('-', $this -> created_at);
			$dateFrom = self::datetToSql($dateFrom);
			$dateTo = self::datetToSql($dateTo, 1);
			
			$query->andFilterWhere(['between', 'created_at', $dateFrom, $dateTo]);
		};
				
				
						
			
		/* фильтры по like для ссылки*/
		if ($this -> order_id) {			
			$concat = 'CONCAT('.Order::getCaptionTemplateAsStringList().')';

			$list = Order::find() -> andWhere(['like', $concat , $this->order_id])->asArray()->all();
			$list = ArrayHelper::getColumn($list, 'id');			
			if (count ($list) > 0) {
				$list = implode(',',$list);			
				$query->andWhere("order_id in ($list)");			
			} else { // Условие не выполнено
				$query->andWhere('false');
				return $dataProvider;
			}
		}
				
				
					
				
				
						
				
				
					
				
		/* фильтры по daterangepicker */
		if ($this -> date_start) {
			list($dateFrom, $dateTo) = explode('-', $this -> date_start);
			$dateFrom = self::datetToSql($dateFrom);
			$dateTo = self::datetToSql($dateTo, 1);
			
			$query->andFilterWhere(['between', 'date_start', $dateFrom, $dateTo]);
		};
				
				
						
				
				
					
				
		/* фильтры по daterangepicker */
		if ($this -> date_fin) {
			list($dateFrom, $dateTo) = explode('-', $this -> date_fin);
			$dateFrom = self::datetToSql($dateFrom);
			$dateTo = self::datetToSql($dateTo, 1);
			
			$query->andFilterWhere(['between', 'date_fin', $dateFrom, $dateTo]);
		};
				
				
						
			
		/* фильтры по like для ссылки*/
		if ($this -> address_id) {			
			$concat = 'CONCAT('.Address::getCaptionTemplateAsStringList().')';

			$list = Address::find() -> andWhere(['like', $concat , $this->address_id])->asArray()->all();
			$list = ArrayHelper::getColumn($list, 'id');			
			if (count ($list) > 0) {
				$list = implode(',',$list);			
				$query->andWhere("address_id in ($list)");			
			} else { // Условие не выполнено
				$query->andWhere('false');
				return $dataProvider;
			}
		}
				
				
					
				
				
						
			
		/* фильтры по like для ссылки*/
		if ($this -> owner_id) {			
			$concat = 'CONCAT('.User::getCaptionTemplateAsStringList().')';

			$list = User::find() -> andWhere(['like', $concat , $this->owner_id])->asArray()->all();
			$list = ArrayHelper::getColumn($list, 'id');			
			if (count ($list) > 0) {
				$list = implode(',',$list);			
				$query->andWhere("owner_id in ($list)");			
			} else { // Условие не выполнено
				$query->andWhere('false');
				return $dataProvider;
			}
		}
				
				
					
				
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'name', $this->name]);
				
						
				
				
					
				
				
						
				
				
					
				
				
			

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
