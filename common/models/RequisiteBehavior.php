<?php
namespace common\models;

use yii;
use yii\base\Behavior;
use yii\helpers\Url;
use common\models\EntityExt;

class RequisiteBehavior extends Behavior
{
	public function getTitle() {
	
	}
	
	public static function findEqualDescription() {
		return '
		<code>public function findEqual($attrs = false) </code><br/>
		Находит реквизиты по полному совпдаению указанных в аргументе имент атрибутов.<br/>
		Если список полей не указан, то по умолчанию используется весь имеющийся список атрибутов.<br/>
		Возвращает массив id совпадающих реквизитов<br/>
		Если не найдено, то возвращает null<br/>
		';
	}
	
	
	public function findEqual($attrs = false) {
		$model = $this -> owner;
		$result = null;
		
		if (!$attrs) $attrs = $model -> attributes();				
		$conditions = ['and'];		
		
		foreach($attrs as $name) {
			
			$value = $model[$name];
			if( in_array($model -> getMeta($name, 'type'), ['date', 'datetime', 'time'])) {
				$value = date('Y.m.d H:i:s', strtotime($value));
			}
				
			if (!$model[$name]) {
				$conditions[] = ['or', ['not', $name], [$name => null]];
			} else {
				$conditions[] = [$name => $value];
			}
		};
		
//		var_dump($conditions); echo "<br>";
		$query = $model::find() -> where($conditions);
		
		$list =$query -> all();
		if ($list) {
			foreach($list as $item) {
				$result[] = $item -> id;
			}
		}
		
		return $result;
		
	}	
	
	
	public static function getHeaderTextDescription() {
		return '
		Возвращает текст реквизитов для шапки документа
		';
	}
		
	public function getHeaderText($sideName) {
	}
	
	
	public static function getFooterTextDescription() {
		return '
		Возвращает текст реквизитов для размещения в конце документа
		';
	}
		
	public function getFooterText() {
	}
	
	public static function payRegOrderDescription() {
		return '
		
		передать:<br/>
		сумму заказа в рублях $params[\'amount\'] <br/>
	 	номер заказа $params[\'number\']<br/>
	 	url для возврата $params[\'returnUrl\']<br/>
		';
	}


    public function payRegOrder($params) {
        $r = $this -> owner;
//		if (!$r -> eq_login || !$r -> eq_password) 
//			throw new NotFoundHttpException('Эквайринг не подключен для '.$r -> getCaption());
        $params['userName'] = $r -> eq_login;
        $params['password'] = $r -> eq_password;
        return PayManager::payRegOrder($params);
    }
    public function payCheck($params) {
        $r = $this -> owner;
//		if (!$r -> eq_login || !$r -> eq_password)
//			throw new NotFoundHttpException('Эквайринг не подключен для '.$r -> getCaption());
        $params['userName'] = $r -> eq_login;
        $params['password'] = $r -> eq_password;
        return PayManager::payCheck($params);
    }
	
	
	public static function getEditableDescription() {
		return '
		Можно ли изменять реквизиты</br>
		Запрещается изменение, если есть ссылки на эти реквизиты, кроме неподписанного договора (соглашения)</br>
		';
	}
	
	public function getEditable() {
		$model = $this -> owner;
		$linkCount = $model -> hasLinks();
				
		if ($linkCount == 1) { // вероятнее всего это контракт, связанный с реквизитами, проверим.
			$c = $model -> parent() -> getDocuments() -> one();
			if (!is_null($c) && $c -> id == $model -> contract_id) { // да, связка только на контракт, она считается только если контракт подписан
				if ($model -> contract -> state == Document::CONTRACT_STATE_DEACTIVE) $linkCount = 0; 
			}
		}
		return $linkCount == 0;
	}
	
	
	/*
	public static function hasLinksDescription() {
		return '	
		<code>public function hasLinks()</code><br/>
		Проверяет, есть ли ссылки на реквизиты<br/>
	
		'
		
	}
	
	public function hasLinks() {
		
	}
	*/
	
};