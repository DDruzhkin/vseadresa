<?php

namespace common\models;

use Yii;

class EventManager
{
		public $events = null;
		
		
		/* 
		*	Связывает событие и фукнцию 
		* 	связываемая фунцкция должна иметь один аргумент - массив с параметрами
		*/
		public function linkEvent($eventName, $func) {
			if ($this -> events) {
				if (array_key_exists($eventName, $this -> events)) {
					$functList = $this -> events[$eventName];
					$functList[] = $func; 
					$this -> events[$eventName] = $functList;
				}
			} else  $this -> events[$eventName] = [$func];
		}
		
		/* 
		*	Вызов события
		*/
		public function callEvent($eventName, $params = []) {
			if (!$this -> events) return;
			if (array_key_exists($eventName, $this -> events)) {
				$functList = $this -> events[$eventName];
				foreach($functList as $func) {
					$func($params);
				}
				
			}
			
			
		}
	
}