<?php
/*
*	Менеджер работы с реквизитами для инкапсуляции процессов сохранения истории изменения реквизитов
* 	Все методы статические, то есть нет необходиости создавать экземпляр этой модели.
*	Модель прикладная, то есть завязана на конкретное приложение и структуру данных
*/
namespace common\models;

use yii;
use yii\base\Model;
use yii\helpers\Url;
use common\models\EntityExt;

class RequisiteManager extends Model {
	
	
	
	// Сохранение реквизитов
	// Проверяет, есть ли связки с другими объектами
	// Если связок нет, то реквизиты сохраняются как обычный у обычной модели
	// Если есть связка, то создается новый экземпляр реквизитов со ссылкой на старые реквизиты (атрибут prior_id)
	public static function save($model) {
		
		$list = $model -> findEqual();			
		$save = false;
		if ($list && in_array($model -> id, $list)) {//точно такие же реквизиты хранятся в этой же модели, то есть модель не изменилась			
			// Сохраняемые реквизиты ничем не отличаются от прежних, то есть сохараняем без лишних движений
			$result = $model;
			$save = true;					
							
		}
		
		if (!$save) { // проверяем наличие связок на реквизиты
			$save = true;
			$result = null;
			if ($model -> id) {
				$linkCount = $model -> hasLinks();

				if ($linkCount == 1) { // вероятнее всего это контракт, связанный с реквизитами, проверим.
					$c = $model -> parent() -> getDocuments() -> one();
					
					if ($c -> id == $model -> contract_id) { // да, связка только на контракт, она считается только если контакт подписан					
						if ($model -> contract -> state == Document::CONTRACT_STATE_DEACTIVE) $linkCount = 0; 
					}
				}


				if ($linkCount) { // связки есть - создаем клона
					$result = $model -> createClone();
					$result -> prior_id = $model -> id;
				}
			};

			
			if (!$result){ // связок нет - сохраняем в текущем объекте
				$result = $model;							
			};


			// блокируем пользователя. Блокировка возможно даже, если не был создан клон
			$user = $model -> user;
			$user -> status_id = $user -> statusByAlias("blocked") ->id;			
			$user -> save();
			
		}

		$result -> save();
		
		if ($model -> id && ($result -> id != $model -> id)) {
			/// создался клон реквизитов, необходимо произвести нужные переносы и действия
			// Если у кого-то старые реквизиты используются как по умолчанию - подменить их
			$user = User::find() -> where(['default_profile_id' => $model -> id]) -> one();
			if ($user) {
				$user -> default_profile_id = $result -> id;
				$user -> save();
			};
			// старые реквизиты помечаем как удаленные
			if ($model -> enabled) {
				$model -> enabled = 0;
				$model -> save();
			};
			
			// законтрактовать по новым реквизитам
			if (Entity::getCurrentUser() ->checkRole(User::ROLE_OWNER)) {
				$result -> user -> ownerContracting(); // сформируем договор или допник (внутри автоматическая проверка на существование)			
			}
		}
		
		
		
		return $result;

	}
}