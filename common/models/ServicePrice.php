<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_service_prices".
 *
 * @property integer $id
 * @property string $code
 * @property string $stype
 * @property integer $service_name_id
 * @property string $name
 * @property string $alias
 * @property string $created_at
 * @property string $updated_at
 * @property integer $to_address
 * @property integer $owner_id
 * @property integer $recipient_id
 * @property integer $address_id
 * @property string $description
 * @property string $type
 * @property string $price
 * @property string $price6
 * @property string $price11
 * @property integer $enabled
 *
 * @property Address $address
 * @property User $owner
 * @property CObject $recipient
 * @property ServiceName $serviceName
 * @property Service[] $services
 */
class ServicePrice extends \common\models\EntityExt 
{
	
	/* Константы модели */
	const STYPE_ONCE = 'Разовая';
	const STYPE_MONTHLY = 'Ежемесячная';
	const PTYPE_NORMAL = 'Обычная';
	const PTYPE_ACTION = 'Акционная';
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_service_prices'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [
			'stype' => ['Разовая' => 'Разовая', 'Ежемесячная' => 'Ежемесячная', ],  
			'type' => ['Обычная' => 'Обычная', 'Акционная' => 'Акционная', ],  

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Прайс-лист на услуги', 
			'rodp' => 'услуги', 
			'vinp' => 'услугу', 
			'imp' => 'услуга', 
			'model' => 'ServicePrice', 
			'parent' => 'objects', 
			'caption' => '[%code%] %name%', 
			'comment' => 'Список всех цен на все услуги на портале. Список можно фильтровать по адресам и владельцам.', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'code' => ['label' => 'Код', 'visible' => '1', 'filter' => 'value', 'readonly' => '1', 'comment' => 'Если услуга системная, то код совпадает с кодом услуги из справочника', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
			'stype' => ['label' => 'Тип услуги', 'content' => 'Услуга является разовой или ежемесячной', 'visible' => '1', 'filter' => 'list', 'comment' => 'Является ли услуга разовой или оказывается ежемесячно. Если услуга системная, то тип совпадает с типом услуги из справочника', 'default' => 'Ежемесячная', 'type' => 'enum', 'required' => '1', 'show' => '1', ], 
			'service_name_id' => ['label' => 'Системная услуга', 'visible' => '1', 'readonly' => '0', 'comment' => 'Если услуга является системной, то выбрать услугу из справочника услуг', 'link' => '1', 'filter' => 'list', 'model' => 'ServiceName', 'type' => 'ref', 'required' => '0', 'show' => '1', ], 
			'name' => ['label' => 'Название услуги', 'comment' => 'Если услуга системная, то название совпадает с названием услуги из общего справочника услуг.', 'visible' => '1', 'filter' => 'like', 'type' => 'string', 'required' => '1', 'show' => '1', ], 
			'alias' => ['label' => 'Псевдоним', 'comment' => 'Внутренне имя услуги для привязки ее к участию в бизнес-логике', 'show' => '0', 'readonly' => '1', 'type' => 'string', 'required' => '0', 'visible' => '0', ], 
			'created_at' => ['label' => 'Дата добавления', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'visible' => '0', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'readonly' => '1', 'required' => '1', 'show' => '1', ], 
			'updated_at' => ['label' => 'Дата изменения', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'show' => '0', 'readonly' => '1', 'required' => '0', 'visible' => '0', ], 
			'to_address' => ['type' => 'boolean', 'label' => 'Добавлять в адрес', 'visible' => '0', 'content' => 'Добавлять ли услугу в адрес по умолчанию', 'readonly' => '0', 'comment' => 'Добавлять ли эту услугу автоматически при добавлении нового адреса', 'required' => '0', 'show' => '1', ], 
			'owner_id' => ['label' => 'Владелец', 'comment' => 'Владелец адреса, определяется адресом', 'default' => 'currentUser()', 'filter' => 'like', 'link' => '1', 'readonly' => '1', 'model' => 'User', 'type' => 'ref', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'recipient_id' => ['label' => 'Реквизиты', 'visible' => '0', 'comment' => 'Реквизиты получателя оплаты за усугу', 'readonly' => '0', 'link' => '0', 'condition' => '[user_id=>owner_id]', 'model' => 'CObject', 'type' => 'ref', 'required' => '0', 'show' => '1', ], 
			'address_id' => ['label' => 'Адрес', 'comment' => 'Адрес, к которому относится цена услуги', 'visible' => '1', 'filter' => 'like', 'link' => '1', 'condition' => '[owner_id=>owner_id]', 'model' => 'Address', 'type' => 'ref', 'required' => '0', 'show' => '1', ], 
			'description' => ['label' => 'Описание', 'visible' => '0', 'readonly' => '0', 'comment' => 'Подробное описание услуги, которое будет показано заказчику', 'type' => 'text', 'required' => '0', 'show' => '1', ], 
			'type' => ['label' => 'Тип цены', 'comment' => 'Тип цены. Могут быть обычная цена и под различные акции', 'visible' => '0', 'default' => 'Обычная', 'type' => 'enum', 'required' => '0', 'show' => '1', ], 
			'price' => ['label' => 'Цена, руб', 'comment' => 'Цена для разовой услуги', 'visible' => '1', 'format' => '[\'decimal\', 2]', 'mask' => '@money', 'type' => 'decimal', 'required' => '0', 'show' => '1', ],
			'price6' => ['label' => 'Стоимость месяца при анедне адреса на 6 месяцев, руб/мес.', 'comment' => 'Цена услуги при заказе на 6 месяцев', 'visible' => '1', 'format' => '[\'decimal\', 2]', 'mask' => '@money', 'type' => 'decimal', 'required' => '0', 'show' => '1', ], 
			'price11' => ['label' => 'Стоимость месяца при анедне адреса на 11 месяцев, руб/мес.', 'comment' => 'Цена услуги при заказе на 11 месяцев', 'visible' => '1', 'format' => '[\'decimal\', 2]', 'mask' => '@money', 'type' => 'decimal', 'required' => '0', 'show' => '1', ], 
			'enabled' => ['label' => 'Активна', 'comment' => 'Услуга активна и может быть выбрана для заказа', 'visible' => '0', 'format' => 'boolean', 'default' => '1', 'type' => 'tinyint', 'required' => '0', 'show' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
		
	return array_merge($result,
	[
		
		[
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'create_time',
			'updatedAtAttribute' => 'update_time',
			'value' => new Expression('NOW()'),			
			
		],
	]);
	
		
}
	







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'name', 'owner_id'], 'required'],
            [['id', 'service_name_id', 'owner_id', 'recipient_id', 'address_id'], 'integer'],
            [['stype', 'description', 'type'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['price', 'price6', 'price11'], 'number'],
            [['code'], 'string', 'max' => 9],
            [['name'], 'string', 'max' => 50],
            [['alias'], 'string', 'max' => 45],
            [['to_address', 'enabled'], 'string', 'max' => 1],
            [['id'], 'unique'],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['recipient_id'], 'exist', 'skipOnError' => true, 'targetClass' => CObject::className(), 'targetAttribute' => ['recipient_id' => 'id']],
            [['service_name_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServiceName::className(), 'targetAttribute' => ['service_name_id' => 'id']],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress() {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner() {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient() {
        return $this->hasOne(CObject::className(), ['id' => 'recipient_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceName() {
        return $this->hasOne(ServiceName::className(), ['id' => 'service_name_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices() {
        return $this->hasMany(Service::className(), ['service_price_id' => 'id']);
    }



    public static function captionTemplate(){

        return "[%code%] %name%";
    }





}