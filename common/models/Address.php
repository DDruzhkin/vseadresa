<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "adr_addresses".
 *
 * @property integer $id
 * @property string $gps
 * @property string $created_at
 * @property string $status_changed_at
 * @property string $adr_index
 * @property string $address
 * @property integer $status_id
 * @property integer $owner_id
 * @property integer $nalog_id
 * @property integer $metro_id
 * @property integer $region_id
 * @property integer $okrug_id
 * @property integer $demo
 * @property string $is_new
 * @property string $parameters
 * @property string $square
 * @property double $rating
 * @property integer $price_type
 * @property integer $options
 * @property string $photo_date
 * @property string $subscribe_to
 * @property integer $subscribe_id
 * @property integer $photo_order_id
 * @property integer $quickable
 * @property string $info
 * @property string $price6
 * @property string $price11
 * @property string $price_first_6
 * @property string $price_first_11
 * @property string $price_change_6
 * @property string $price_change_11
 * @property string $price_prolongation_6
 * @property string $price_prolongation_11
 * @property string $price6min
 * @property string $price11min
 * @property string $price6max
 * @property string $price11max
 * @property string $photos
 * @property string $files
 * @property integer $range
 * @property integer $seo_id
 * @property string $h1
 * @property string $title
 * @property string $meta_description
 * @property string $seo_text
 * @property integer $enabled
 * @property integer $cpriority
 * @property integer $priority
 *
 * @property AddressOption[] $addressOptions
 * @property Metro $metro
 * @property Nalog $nalog
 * @property User $owner
 * @property Okrug $okrug
 * @property Region $region
 * @property Seo $seo
 * @property Status $status
 * @property Porder $photoOrder
 * @property Porder $subscribe
 * @property AuctionRate[] $auctionRates
 * @property Auction[] $auctions
 * @property Document[] $documents
 * @property Order[] $orders
 * @property PhotoCalendar[] $photoCalendars
 * @property PhotoVisit[] $photoVisits
 * @property ServicePrice[] $servicePrices
 * @property Service[] $services
 * @property SimilarAddresses[] $similarAddresses
 * @property SimilarAddresses[] $similarAddresses0
 */
class Address extends \common\models\EntityExt
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adr_addresses';
    }


    public static function parentClassName()
    {
        return '\common\models\CObject';
    }


    /* Списки для перечисляемых типов атрибутов */
    public static function attrLists()
    {
        return [

        ];
    }

    /* Метаданные класса */
    public static function meta()
    {
        return [
            'title' => 'Адреса',
            'model' => 'Address',
            'imp' => 'Адрес',
            'rodp' => 'Адреса',
            'vinp' => 'Адрес',
            'parent' => 'objects',
            'caption' => '%address%',

        ];
    }

    /* Метаданные атрибутов*/
    public static function attrMeta()
    {
        $parentAttrMeta = [];
        if (self::parentClassName()) {
            $parentAttrMeta = self::parentClassName()::attrMeta();
        };

        return ArrayHelper::merge(
            $parentAttrMeta,
            [
                'id' => ['show' => '1', 'filter' => 'value', 'label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'visible' => '1',],
                'gps' => ['label' => 'Координаты', 'visible' => '0', 'show' => '1', 'format' => 'gps', 'default' => '55.751429,37.618879', 'type' => 'text', 'required' => '0',],
                'created_at' => ['label' => 'Дата добавления', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'show' => '1', 'visible' => '1', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'readonly' => '1', 'required' => '0',],
                'status_changed_at' => ['label' => 'Дата изменения статуса', 'show' => '0', 'comment' => 'Дата последнего изменения статуса. Выставляется автоматически', 'type' => 'timestamp', 'required' => '0', 'visible' => '0',],
                'adr_index' => ['label' => 'Индекс', 'show' => '1', 'visible' => '0', 'mask' => '999999', 'comment' => 'Почтовый индекс адреса - 6 цифр.', 'type' => 'string', 'required' => '0',],
                'address' => ['label' => 'Адрес', 'filter' => 'like', 'type' => 'text', 'required' => '1', 'show' => '1', 'visible' => '1',],
                'status_id' => ['label' => 'Статус адреса', 'default' => 'defaultStatus', 'comment' => 'Текущий статус адреса', 'filter' => 'list', 'model' => 'Status', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0',],
                'owner_id' => ['label' => 'Владелец', 'comment' => 'Владелец адреса', 'default' => 'currentUser()', 'filter' => 'like', 'link' => '1', 'model' => 'User', 'type' => 'ref', 'required' => '1', 'show' => '1', 'visible' => '1',],
                'nalog_id' => ['label' => '№ налоговой', 'visible' => '1', 'comment' => 'Налоговая, к которой относится адрес', 'filter' => 'list', 'link' => '1', 'model' => 'Nalog', 'type' => 'ref', 'required' => '0', 'show' => '1',],
                'metro_id' => ['label' => 'Метро', 'format' => 'autocomplete', 'visible' => '0', 'comment' => 'станция метро', 'filter' => 'list', 'link' => '1', 'model' => 'Metro', 'type' => 'ref', 'required' => '0', 'show' => '1',],
                'region_id' => ['label' => 'Район', 'comment' => 'Район Москвы', 'show' => '1', 'visible' => '0', 'filter' => 'list', 'link' => '1', 'model' => 'Region', 'type' => 'ref', 'required' => '0',],
                'okrug_id' => ['label' => 'Округ', 'comment' => 'Административный округ адреса', 'filter' => 'list', 'link' => '1', 'readonly' => '1', 'model' => 'Okrug', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1',],
                'demo' => ['label' => 'Показывать в каталоге', 'comment' => 'Демонстрировать адрес в каталоге', 'visible' => '0', 'type' => 'boolean', 'default' => 1, 'required' => '0', 'show' => '1',],
                'is_new' => ['label' => 'Новый', 'comment' => 'Адрес отмечен новым', 'type' => 'timestamp', 'required' => '0', 'show' => '1', 'visible' => '1',],
                'parameters' => ['label' => 'Параметры', 'show' => '0', 'comment' => 'Дополнительные параметры адреса', 'type' => 'text', 'required' => '0', 'visible' => '0',],
                'square' => ['label' => 'Площадь', 'visible' => '0', 'comment' => 'Площадь помещения, включенного в аренду в кв.м', 'mask' => '@decimal', 'type' => 'decimal', 'required' => '0', 'show' => '1',],
                'rating' => ['label' => 'Рейтинг', 'show' => '0', 'default' => 0, 'type' => 'double', 'required' => '0', 'visible' => '0',],
                'price_type' => ['label' => 'Тип цены', 'comment' => 'Текущий (отображаемый) тип цены', 'show' => '0', 'visible' => '0', 'type' => 'tinyint', 'required' => '0',],
                'options' => ['label' => 'Опции', 'show' => '1', 'visible' => '0', 'format' => 'checkboxlist', 'itemtemplate' => '%icon% - %name%', 'listitemtemplate' => '%icon%', 'type' => 'manytomany', 'viatable' => 'adr_address_option', 'required' => '0', 'mtm_bname' => 'address_id', 'mtm_fname' => 'option_id', 'mtm_model' => 'Option',],
                'photo_date' => ['label' => 'Дата визита фотографа', 'show' => '1', 'visible' => '0', 'type' => 'datetime', 'required' => '0',],
                'subscribe_to' => ['label' => 'Окончание подписки', 'show' => '1', 'visible' => '0', 'type' => 'date', 'comment' => 'Дата окончания подписки адреса, после которой адрес блокируется', 'required' => '0',],
                'subscribe_id' => ['label' => 'Ссылка на услугу подписки', 'visible' => '0', 'model' => 'Porder', 'type' => 'ref', 'required' => '0', 'show' => '1', 'link' => '0',],
                'photo_order_id' => ['label' => 'Заказ на визит фотографа', 'show' => '0', 'visible' => '0', 'model' => 'Porder', 'type' => 'ref', 'required' => '0', 'link' => '0',],
                'quickable' => ['label' => 'Возможно срочное оформление', 'show' => '1', 'visible' => '0', 'type' => 'boolean', 'comment' => 'Может ли быть обеспечено срочное формирвоание заказа для этого адреса', 'required' => '0',],
                'reg_msk' => ['label' => 'Москва', 'show' => '1', 'visible' => '0', 'type' => 'boolean', 'comment' => 'Ограничение по региону регистрации', 'required' => '0',],
                'reg_mo' => ['label' => 'Московская область', 'show' => '1', 'visible' => '0', 'type' => 'boolean', 'comment' => 'Ограничение по региону регистрации', 'required' => '0',],
                'reg_other' => ['label' => 'Другие регионы РФ', 'show' => '1', 'visible' => '0', 'type' => 'boolean', 'comment' => 'Ограничение по региону регистрации', 'required' => '0',],
                'info' => ['label' => 'Примечания', 'visible' => '0', 'type' => 'text', 'required' => '0', 'show' => '1',],
                'price_first_6' => [
                    'label' => 'Цена за 6 месяцев',
                    'comment' => 'Цена первичной регистрации адреса на 6 месяцев',
                    'description' => 'Первичная регистрация адреса на 6 месяцев',
                    'visible' => '0',
                    'format' => '[\'decimal\', 2]',
                    'mask' => '@money',
                    'default' => "0.0000",
                    'type' => 'decimal',
                    'required' => '0',
                    'show' => '1',
                ],
                'price_first_11' => [
                    'label' => 'Цена за 11 месяцев',
                    'comment' => 'Цена первичной регистрации адреса на 11 месяцев',
                    'description' => 'Первичная регистрация адреса на 11 месяцев',
                    'visible' => '0',
                    'format' => '[\'decimal\', 2]',
                    'mask' => '@money',
                    'default' => "0.0000",
                    'type' => 'decimal',
                    'required' => '0',
                    'show' => '1',
                ],
                'price_change_6' => [
                    'label' => 'Цена за 6 месяцев',
                    'comment' => 'Цена регистрации изменений адреса на 6 месяцев',
                    'description' => 'Регистрация изменений адреса на 6 месяцев',
                    'visible' => '0',
                    'format' => '[\'decimal\', 2]',
                    'mask' => '@money',
                    'default' => "0.0000",
                    'type' => 'decimal',
                    'required' => '0',
                    'show' => '1',
                ],
                'price_change_11' => [
                    'label' => 'Цена за 11 месяцев',
                    'comment' => 'Цена регистрации изменений адреса на 11 месяцев',
                    'description' => 'Регистрация изменений адреса на 11 месяцев',
                    'visible' => '0',
                    'format' => '[\'decimal\', 2]',
                    'mask' => '@money',
                    'default' => "0.0000",
                    'type' => 'decimal',
                    'required' => '0',
                    'show' => '1',
                ],
                'price_prolongation_6' => [
                    'label' => 'Цена за 6 месяцев',
                    'comment' => 'Цена прологации адреса на 6 месяцев',
                    'visible' => '0',
                    'format' => '[\'decimal\', 2]',
                    'mask' => '@money',
                    'default' => "0.0000",
                    'type' => 'decimal',
                    'required' => '0',
                    'show' => '1',
                ],
                'price_prolongation_11' => [
                    'label' => 'Цена за 11 месяцев',
                    'comment' => 'Цена прологации адреса на 11 месяцев',
                    'visible' => '0',
                    'format' => '[\'decimal\', 2]',
                    'mask' => '@money',
                    'default' => "0.0000",
                    'type' => 'decimal',
                    'required' => '0',
                    'show' => '1',
                ],
                'price6min' => ['label' => 'Мин.цена за 6 месяцев', 'comment' => 'Минимальная цена адреса за 6 месяцев. Устанавливается администратором', 'visible' => '0', 'format' => '[\'decimal\', 2]', 'mask' => '@money', 'default' => "0.0000", 'type' => 'decimal', 'required' => '1', 'show' => '1',],
                'price11min' => ['label' => 'Мин.цена за 11 месяцев', 'comment' => 'Минимальная цена адреса за 11 месяцев. Устанавливается администратором', 'visible' => '0', 'format' => '[\'decimal\', 2]', 'mask' => '@money', 'default' => "0.0000", 'type' => 'decimal', 'required' => '1', 'show' => '1',],
                'price6max' => ['label' => 'Макс.цена за 6 месяцев', 'comment' => 'Максимальная цена адреса за 6 месяцев. Устанавливается администратором', 'visible' => '0', 'format' => '[\'decimal\', 2]', 'mask' => '@money', 'default' => "100000.0000", 'type' => 'decimal', 'required' => '1', 'show' => '1',],
                'price11max' => ['label' => 'Макс.цена за 11 месяцев', 'comment' => 'Максимальная цена адреса за 11 месяцев. Устанавливается администратором', 'visible' => '0', 'format' => '[\'decimal\', 2]', 'mask' => '@money', 'default' => "100000.0000", 'type' => 'decimal', 'required' => '1', 'show' => '1',],
                'photos' => ['label' => 'Фотографии', 'visible' => '0', 'show' => '1', 'format' => 'images', 'type' => 'json', 'required' => '0',],
                'files' => ['label' => 'Документы', 'visible' => '0', 'type' => 'json', 'format' => 'files', 'comments' => 'Документы, прикрепленные к адресу в виде файлов', 'required' => '0', 'show' => '1',],
                'range' => ['label' => 'Ранг адреса', 'visible' => '0', 'comment' => 'Используется для ранжирования адресов всписке', 'default' => 0, 'type' => 'integer', 'required' => '0', 'show' => '1',],
                'seo_id' => ['label' => 'Seo-данные', 'link' => '1', 'show' => '0', 'model' => 'Seo', 'type' => 'ref', 'required' => '0', 'visible' => '0',],
                'h1' => ['show' => '1', 'visible' => '0', 'label' => 'H1', 'type' => 'string', 'required' => '0',],
                'title' => ['show' => '1', 'visible' => '0', 'label' => 'Title', 'type' => 'string', 'required' => '0',],
                'meta_description' => ['label' => 'Поле Meta Description', 'show' => '1', 'visible' => '0', 'type' => 'string', 'required' => '0',],
                'seo_text' => ['label' => 'SEO Текст', 'visible' => '0', 'show' => '1', 'type' => 'text', 'required' => '0',],
                'enabled' => ['label' => 'Активен', 'default' => '1', 'type' => 'tinyint', 'required' => '0', 'show' => '1', 'visible' => '1',],
                'cpriority' => ['label' => 'Общий приоритет', 'show' => '0', 'comment' => 'Общий пересчитываемый приоритет с учетом приоритета владельца', 'default' => 0, 'type' => 'integer', 'required' => '0', 'visible' => '0',],
                'priority' => ['label' => 'Приоритет', 'visible' => '0', 'default' => 0, 'type' => 'integer', 'required' => '0', 'show' => '1',],


            ]
        );
    }

    /* Поведения*/
    public function behaviors()
    {

        $result = [];
        $ns = "common\models\\";
        $class = $ns . $this->meta()['model'] . 'Behavior';

        if (class_exists($class)) {
            $result['ext'] = [
                'class' => $class,
            ];
        };

        return array_merge($result,
            [

                [
                    'class' => TimestampBehavior::className(),
                    'createdAtAttribute' => 'create_time',

                    'value' => new Expression('NOW()'),

                ],
                'ManyToMany' => [
                    'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                    'relations' => [
                        'options' => 'options'
                    ],
                ],
            ]);


    }


    public static function statusByAlias($alias)
    {
        return Status::findOne(["alias" => $alias, "table" => self::tableName()]);
    }


    public function getPhotos($name)
    {
        return $this->getArrayValue('photos', $name);
    }

    public function setPhotos($name, $value, $save = false)
    {
        $this->setArrayValue('photos', $name, $value, $save);
    }

    public function getFiles($name)
    {
        return $this->getArrayValue('files', $name);
    }

    public function setFiles($name, $value, $save = false)
    {
        $this->setArrayValue('files', $name, $value, $save);
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'address', 'owner_id'], 'required'],
            [['id', 'status_id', 'owner_id', 'nalog_id', 'metro_id', 'region_id', 'okrug_id', 'subscribe_id', 'photo_order_id', 'range', 'seo_id', 'cpriority', 'priority'], 'integer'],
            [['gps', 'address', 'parameters', 'info', 'photos', 'files', 'seo_text'], 'string'],
            [['created_at', 'status_changed_at', 'is_new', 'photo_date', 'subscribe_to'], 'safe'],
            [[
                'square',
                'rating',
                'price_first_6', 'price_first_11',
                'price_change_6', 'price_change_11',
                'price_prolongation_6', 'price_prolongation_11',
                'price6min', 'price11min',
                'price6max', 'price11max'
            ], 'number'],
            [['adr_index'], 'string', 'max' => 6],
            [['demo', 'price_type'], 'string', 'max' => 4],
            [['quickable', 'enabled','reg_msk','reg_mo','reg_other'], 'string', 'max' => 1],
            [['h1', 'title', 'meta_description'], 'string', 'max' => 255],
            [['options'], 'each', 'rule' => ['integer']],
            [['tax_agencies'], 'each', 'rule' => ['integer']],
            [['id'], 'unique'],
            [['metro_id'], 'exist', 'skipOnError' => true, 'targetClass' => Metro::className(), 'targetAttribute' => ['metro_id' => 'id']],
            [['nalog_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nalog::className(), 'targetAttribute' => ['nalog_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['okrug_id'], 'exist', 'skipOnError' => true, 'targetClass' => Okrug::className(), 'targetAttribute' => ['okrug_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
            [['seo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Seo::className(), 'targetAttribute' => ['seo_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['photo_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Porder::className(), 'targetAttribute' => ['photo_order_id' => 'id']],
            [['subscribe_id'], 'exist', 'skipOnError' => true, 'targetClass' => Porder::className(), 'targetAttribute' => ['subscribe_id' => 'id']],
        ]);
    }


    public function getOptions($template = false)
    {
        $list = $this->hasMany(Option::className(), ['id' => 'option_id'])
            ->viaTable('adr_address_option', ['address_id' => 'id']);
        $result = $this->getListValue($list, $template, 'options');
        return $result;
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getAddressTaxAgencies()
    {
        return $this->hasMany(AddressTaxAgency::class, ['address_id' => 'id']);
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getTaxAgencies()
    {
        return $this->hasMany(Nalog::className(), ['id' => 'tax_agency_id'])
            ->viaTable('adr_address_tax_agency', ['address_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressOptions()
    {
        return $this->hasMany(AddressOption::className(), ['address_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetro()
    {
        return $this->hasOne(Metro::className(), ['id' => 'metro_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNalog()
    {
        return $this->hasOne(Nalog::className(), ['id' => 'nalog_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOkrug()
    {
        return $this->hasOne(Okrug::className(), ['id' => 'okrug_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeo()
    {
        return $this->hasOne(Seo::className(), ['id' => 'seo_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotoOrder()
    {
        return $this->hasOne(Porder::className(), ['id' => 'photo_order_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribe()
    {
        return $this->hasOne(Porder::className(), ['id' => 'subscribe_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuctionRates()
    {
        return $this->hasMany(AuctionRate::className(), ['address_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuctions()
    {
        return $this->hasMany(Auction::className(), ['address_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Document::className(), ['address_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['address_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotoCalendars()
    {
        return $this->hasMany(PhotoCalendar::className(), ['address_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotoVisits()
    {
        return $this->hasMany(PhotoVisit::className(), ['address_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicePrices()
    {
        return $this->hasMany(ServicePrice::className(), ['address_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['address_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSimilarAddresses()
    {
        return $this->hasMany(SimilarAddresses::className(), ['address_from' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSimilarAddresses0()
    {
        return $this->hasMany(SimilarAddresses::className(), ['address_to' => 'id']);
    }


    public static function captionTemplate()
    {

        return "%address%";
    }


    /* Возвращает число ссылок на модель */
    function hasLinks()
    {

        return
            ($this->parent() ? $this->parent()->hasLinks() : 0) +
            $this->getAddressOptions()->count() +
            $this->getAuctionRates()->count() +
            $this->getAuctions()->count() +
            $this->getDocuments()->count() +
            $this->getOrders()->count() +
            $this->getPhotoCalendars()->count() +
            $this->getPhotoVisits()->count() +
            $this->getServicePrices()->count() +
            $this->getServices()->count() +
            $this->getSimilarAddresses()->count() +
            $this->getSimilarAddresses0()->count();
    }


}