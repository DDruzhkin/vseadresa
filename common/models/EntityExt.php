<?php
/*
*	ЭТО ПРИКЛАДНОЙ КЛАСС. ЕГО МОЖНО ДОПОЛНЯТЬ ДЛЯ РЕАЛИЗАЦИИ ПРИКЛАДНЫХ (ПОД КОНКРЕТНЫЙ ПРОЕКТ) ЗАДАЧ.
* 	ДЛЯ КАСТОМИЗАЦИИ КОНКРЕТНЫХ КЛАССОВ (ДОБАВЛЕНИЯ МЕТОДОВ И СВОЙСТВ НЕОБХОДИМО ИСПОЛЬЗОВАТЬ КЛАССЫ ПОВЕДЕНИЙ СООТВЕТСТВУЮЩИХ МОДЕЛЕЙ
* 	[ИМЯ МОДЕЛИ]Behavior, НАПРИМЕР, AddressBehavior
*/

namespace common\models;

use Yii;
use common\models\Entity;
use common\models\Order;
use common\models\DocumentType;
use common\models\ServiceName;
use common\models\ServicePrice;
use common\models\Service;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\base\Exception;

/*
* Класс для расширения общего прикладного функционала. 
* В частности, здесь описываются методы согласования моделей в формате имен integrate[Имя модели] и integrate[Имя модели]Ext, например integrateOrder и integrateOrderExt
*/

class EntityExt extends Entity
{


    // Здесь можно изменить или добавить новые алиасы для масок ввода

    static function getMaskAliasList()
    {
        return parent::getMaskAliasList();

    }

    // Возвращает H1. Если есть атрибут h1, то его значение, если нет или не заполнен, то getCaption();
    // При необходимости могут быть добавлены любые правила
    public function getH1()
    {
        $result = $this->h1;
        if ($result) $result = $this->getCaption();
        return $result;
    }

    // Пользователь портала - хранит реквизиты портала и опции
    static function portal()
    {
        $userAttrs = Yii::$app->params['portalUser'];

        $userName = $userAttrs['username'];

        $user = User::findOne(['username' => $userName]);
        if (!$user) {
            $user = new User();
            $user->load($userAttrs, '');
            $user->save();
        }

        return $user;
    }

    // Генерация номера документа(заказа) - надо просто вызвать перед записью объекта
    // $name - идентификатор счетчика, если не указан, то свпадает с id модели
    // $field - имя поле модели для хранения номера
    public function generateNumber($name = false, $field = 'number')
    {
        // инициализируем номера (счетчики)
        if (!$name) $name = $this->modelId();         // Если $name не указано, то подставляем id модели
        if (!$this[$field]) {    // Номер устанавливается только, если поле пустое
            $this[$field] = \common\models\Counter::byAlias($name)->getValue();
        }
    }



    // Добавить в прайс-лист услугу
    // обязательные параметры: 'address_id', 'service_name_id'
    // привязка к текущему пользователю
    public static function addSerivcePrice($params)
    {
        $serviceNameId = @$params['service_name_id'];
        $userId = @$params['owner_id'] ? $params['owner_id'] : User::currentUser();
        $addressId = @$params['address_id'];
        $name = @$params['name'];
        $result = null;

        $result = ServicePrice::find()->andFilterWhere(['owner_id' => $userId]);

        if ($addressId == NULL) {
            $result->andWhere(['address_id' => NULL]);
        } else {
            if ($addressId === null) throw new Exception('не определен атрибут address_id для добавления пользовательской услуги');
            $result->andFilterWhere(['address_id' => $addressId]);
        };

        if ($serviceNameId == NULL) {
            if (!$name) throw new Exception('не определен атрибут name или service_name_id для добавления пользовательской услуги');
            $result->andWhere(['service_name_id' => NULL, 'name' => $name]);
        } else {
            $result->andFilterWhere(['service_name_id' => $serviceNameId]);
        }
//		var_dump($result -> createCommand()->getRawSql()); exit;

        $result = $result ? $result->one() : false;
        //	var_dump($result); exit;
        if (!$result) $result = new ServicePrice(); // не найдено - создаем
        $params['owner_id'] = $userId;
        $result->load($params, '');    // загружаем данные
        if (!$result->service_name_id) $result->service_name_id = null;

        $result->restore();            // восстанавливаем
        return $result;

    }

    // Удалить из прайс-листа услугу
    // обязательные параметры: 'address_id', 'service_name_id'
    // привязка к текущему пользователю
    public static function deleteSerivcePrice($params, $force = false)
    {
        $serviceNameId = $params['service_name_id'];
        $userId = User::currentUser();
        $addressId = $params['address_id'];
        $result = ServicePrice::findOne(['address_id' => $addressId, 'owner_id' => $userId, 'service_name_id' => $serviceNameId]); // Поищем, существует ли эта услуга в прайсе
    }


    // возвращает список связанных опций с услгами из списка в аргументе
    public static function getServiceOptionsByList($serviceList)
    {
        $result = [];
        foreach ($serviceList as $id) {
            $servicePrice = ServicePrice::findOne($id);
            //echo $servicePrice->name.'<br>';
            if ($servicePrice && $servicePrice->servie_name_id) $result[] = $servicePrice->servie_name->option;
        }

        return $result;
    }

    // Условие (массив) который ограничивает показ адресов в каталоге.<br>
    // В виде массива для генерации запроса.<br>
    public static function getToCatalogCondition()
    {

        $statuses = [Address::statusByAlias('published')->id]; // статусы, в которых отображается адрес
        $userBlockerStatusId = User::statusByAlias('blocked')->id;
        return ['and',
            ['enabled' => 1],
            ['demo' => 1],
            ['status_id' => $statuses],
            ['or', ['subscribe_to' => NULL], 'subscribe_to >= NOW()'],
            ['not', ['(select status_id from adr_users where adr_users.id = owner_id)' => $userBlockerStatusId]]
        ];

    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////// Процедуры согласования 	//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Заказ
    protected function integrateOrder()
    {

        $autoIntegrate = $this->autoIntegrate;

        $this->autoIntegrate = false;
        // Владелец должен быть строго тот же, что и у адреса
        $address = $this->address;
        if ($address) $this->owner_id = $address->owner_id;

        $dirty = $this->getDirtyAttributes();

        if (($this->getOldAttribute('status_id') != $this->status_id)) { // статус изменился

            /* счета выставляются принудительно
            if ($this -> status -> alias == 'new') {  // Если статус заказа сменился на new, то выставляем счета
                $this -> bill();
            }
            */

        }


        //Цены меняем в случае, если цена адреса не указана или произошло изменение значения duration у заказа

        $input = Yii::$app->request->post(Order::baseClassName());

        if ($address) {
//			if (!$this -> address_price || isset($dirty['duration'])) { //
//				$address = $this -> address;
//				if($address) {
//					switch ($this -> duration) {
//						case Order::DURATION_6: {
//							$price = $address -> price6;
//							break;
//						}
//						case Order::DURATION_11: {
//							$price = $address -> price11;
//							break;
//						}
//						default: throw new Exception('Не определена длительность услуги');
//					}
//				};
//				$this -> address_price = $price;
//			}
        } else {
            $this->address_price = 0; // Если адреса нет, то цена адреса нулевая
        };


        if (isset($dirty['duration'])) { // меняем продолжительность оказания услуг
            switch ($dirty['duration']) {
                case 'price_first_11':
                case 'price_change_11':
                case Address::getMeta('price_first_11', 'description'):
                case Address::getMeta('price_change_11', 'description'):
                    $this->duration = Order::DURATION_11;
                    break;
                case 'price_first_6':
                case 'price_change_6':
                case Address::getMeta('price_first_6', 'description'):
                case Address::getMeta('price_change_6', 'description'):
                    $this->duration = Order::DURATION_6;
                    break;
            }
            //$this->duration = $dirty['duration'];
            if (Yii::$app->cache) Yii::$app->cache->setObject($this);
            //if (Yii::$app->cache) Yii::$app->cache -> clearObject($this -> id);

            foreach ($this->getServices()->all() as $service) {

                $service->autointegrate = false;
                $service->save();

            }
            //$this -> calculate();	считается ниже
        }


        // согласовываем пользователя и реквизиты

        if ($this->customer_r_id) {  // Если реквизиты установлены, то устанавливаем заказчика, чьи реквизиты
            $r = self::byId($this->customer_r_id);
            $this->customer_id = $r->user_id;
        } else {
            $customer = $this->customer; // Если не установлены
            if (!$customer) {
                $this->customer_id = self::currentUser();
                $customer = $this->customer;
            }

            if ($customer) {
                $this->customer_r_id = $customer->default_profile_id;
            }

        }

        //die(var_dump($input));
        // Дата завершения оказания услуг
        if ($this->date_start && isset($input['duration'])) {
//			$d = 0;
//			if ($this -> duration == Order::DURATION_6) $d = 6;
//			if ($this -> duration == Order::DURATION_11) $d = 11;
            list (, $type, $month) = explode('_', $input['duration']);

            if ($month > 0) {
                $this->date_fin = date('Y-m-d', strtotime($this->date_start . ' +' . $month . ' month'));
                $this->date_fin = date('Y-m-d', strtotime($this->date_fin . ' -1 days'));
            }
        }

        // Срочность

        $s = ServicePrice::findOne(['alias' => 'quick_order', 'address_id' => $this->address_id]); // Срочность есть в прайсе адреса

        $this->is_quick = ($s !== NULL) && (boolean)Service::findOne(['order_id' => $this->id, 'service_price_id' => $s->id]); // Срчность находится в составе заказа


        // Доставка по умолчанию - самовывоз
        if (is_null($this->delivery_type_id)) {
            $this->delivery_type_id = Delivery::byAlias('pickup')->id;
        }


        // Гарантия
        if ($this->add_guarantee) {
            if (is_null($this->guarantee_id)) {
                $this->createGuarantee(false);
            }
        } else {
            if (!is_null($this->guarantee_id)) {
                $this->guarantee->delete();
                $this->guarantee_id = null;
            }
        }

        // сумма заказа
        /*
        $price = $this -> address_price;
        foreach($this -> getServices() -> all() as $item) {
            $price = $price + $item -> price * $item -> durationValue;
        }

        */
        $summa = $this->calculate();    // пересчитаем сумму

        // Фиксация даты оплаты
        if ($this->status->alias == 'paid') {
            $paid_at = $this->status_changed_at;
            if (!$paid_at) $paid_at = self::dbNow();
            $this->paid_at = $paid_at;
        }


        $this->autoIntegrate = $autoIntegrate;
    }

    protected function integrateOrderDeliveryStored()
    {
        return $this->integrateOrderDelivery();
    }


    // Создание заказа на доставку, если в заказе требуется доставка
    protected function integrateOrderDelivery()
    {

        $old_delivery_id = $this->delivery_id;
        $order = $this;
        // Доставка
        if (is_null($old_delivery_id)) {
            if ($order->delivery_type && $order->delivery_type->delivery_requirement) { // тип доставку требует, чтобы был заказ на доставку
                // Если заказа на доставку нет,то добавляем заказ услуги портала на доставку
                if (!$order->delivery_id) { // нет заказа на доставку - создаем
                    $porder = \common\models\ServicePortal::byAlias('delivary')->order([], NULL, true);
                    $order->delivery_id = $porder->id;
                };
                $porder->customer_r_id = $order->customer_r_id;
                $porder->summa = $order->delivery_type->price;
                $porder->info = $order->delivery_type->name;
                $porder->save();
///				$porder -> bill(['order_id' => $order -> id]);

            } else {
                /*
                    if ($order -> delivery_id) { // удалить заказ
                        $s = Porder::findOne([$order -> delivery_id]);
                        if($s) $s -> delete(); // Удаляем заказ на доставку
                    }
                    $order -> delivery_id = NULL;
                */
            }

        }

        return $old_delivery_id != $this->delivery_id;
    }

    protected function integrateOrderExt()
    {

        // Фиксируем время изменения статуса на Оплачен
        if (!$this->paid_at && ($this->status->alias == 'paid')) {
            $this->paid_at = self::dbNow();
        }

        if ($this->delivery_id) {
            if ($this->delivery->customer_r_id != $this->customer_r_id) {
                $this->delivery->customer_r_id = $this->customer_r_id;
                $this->delivery->customer_id = $this->customer_id;
                $this->delivery->save();
            }
        }


        if ($this->guarantee_id) {
            if ($this->guarantee->customer_r_id != $this->customer_r_id) {
                $this->guarantee->customer_r_id = $this->customer_r_id;
                $this->guarantee->customer_id = $this->customer_id;
                $this->guarantee->save();
            }
        }


        $save = is_null($this->number);

        $this->generateNumber();
        $attrs = $this->lastDirtyAttributes;

        if (array_key_exists('duration', $attrs)) { // Если изменилось продолжительность услуги, то ее надо изменить во всех позициях
            $list = $this->getServices()->all();
            foreach ($list as $item) {
                $item->save(); // делаем внутреннее согласование позиции
            }
        }


        $save = $save || $this->integrateOrderDelivery(); //// (Заказ на доставку создаем, когда производится вызов confirm - подвтерждение заказа) - лучше создавать до подтверждения, чтобы посчитать общую сумму заказа с доставкой

        $oldSumma = $this->summa;
        $summa = $this->calculate();    // Сумма может измениться после внешних согласований, поэтому пересчитываем
        $save = $save || ($oldSumma != $this->summa);

        if ($save) $this->save();


        /*
        // Проверяем число заказов у адреса, при необходимости будет выставлен счет на продление подписки адреса, а адрес снят с публикации
        /// Алгоритм изменился - закомментировано
        $this -> address -> checkOrders();
        */


    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

    static function createAuction($date, $statusId)
    {
//		echo 'createAuction: '.Yii::$app -> formatter -> asDate($date).'<br>';
        $auction = new Auction();
        $auction->date = $date;
        $auction->status_id = $statusId; // Аукцион в будущем должен быть создан активным

        $auction->save();
        return $auction;
    }

    // согласование списка аукционов - добавляются на будущие даты, удаляются пустые в прошлом
    static function integrateAuctionList()
    {

        $statusActiveId = Status::findOne(['alias' => 'active', 'table' => Auction::tableName()])->id;
        $statusNoActiveId = Status::findOne(['alias' => 'noactive', 'table' => Auction::tableName()])->id;

        // закрываем открытые аукционы после сегодня 17:00, если был пустым, то удаляем
        $nDate = ((int)(strtotime(self::dbNow()) / 86400) * 86400 + 17 * 3600);
        // выбираем все пустые аукционы до сегодня 17:00 - они должны быть удалены
        $query = Auction::find()->where(['rate_count' => 0])->andWhere(['<=', 'date', date('Y-m-d', $nDate)])->orderBy(['date' => SORT_DESC]);
        $list = $query->all();
        foreach ($list as $auction) {
            $auction->delete(); // удаляем так, чтобы проходили все проверки.
        };

        // выбираем активные аукционы до сегодня 17:00 (среди них пустых уже не должно быть, т.к. удалили на предыдущим шаге) - меняем статус на закрыт
        $query = Auction::find()->where(['status_id' => $statusActiveId])->andWhere(['<=', 'date', date('Y-m-d', $nDate)])->orderBy(['date' => SORT_DESC]);
        $list = $query->all();
        foreach ($list as $auction) {
            $auction->status_id = $statusNoActiveId; // удаляем так,а не запросом. чтобы проходили все проверки.
            $auction->save();
        };

        // выбираем аукционы после сегодня 17:00 (все должны быть активныим) - достраиваем ряд, чтобы были аукционы на каждый день на 30 дней вперед
        $query = Auction::find()->andWhere(['>', 'date', date('Y-m-d', $nDate)])->orderBy(['date' => SORT_DESC]);
        $list = $query->all();
        $date = null;
        $chDate = ((int)(strtotime(self::dbNow()) / 86400 + 30)) * 86400;
        // Создаем аукционы до +30 дней от сегодня. (Сделано так, что дырки не затыкаются, то есть, если не существует аукциона медлу двумя датами в будушем, то он не будет создан. Но такой ситуации штатно не должно возникать)
        while ($chDate > $nDate) {
            $a = Auction::findOne(['date' => date('Y-m-d', $chDate)]);
            if (!$a) {
                self::createAuction($chDate, $statusActiveId);
            } else break;
            $chDate = $chDate - 86400;
        };


        // * Проверяем и Добавляем аукционы в будущем

        /*
                $query = Auction::find() -> orderBy(['date' =>  SORT_DESC]);

                $list = $query -> all();

                //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql.'<br>';
                var_dump($list); exit;




                //echo Yii::$app -> formatter -> asDate($startDate);
                $date = null;

                foreach($list as $auction) {
                    $date = strToTime($auction -> date);
                    while ($chDate > $date) {
                        self::createAuction($chDate, $statusActiveId);
                        $chDate = $chDate - 86400;
                    };
                    $chDate = $date - 86400;

                };

                $statusId = Status::findOne(['alias' => 'active', 'table' => Auction::tableName()]) -> id;

                // хвост аукиционов из будущего до сегодня
                while ($chDate > self::dbNow()) {
                    self::createAuction($chDate, $statusId);
                    //echo Yii::$app -> formatter -> asDate($auction -> date).'<br>';
                    $chDate = $chDate - 86400;
                }

                // * Удаляем пустые аукционы в прошлом
                $query = Auction::find() -> andWhere(['rate_count' => 0, 'status_id' => $statusNoActiveId]);
                //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
                $list = $query -> all();
                foreach($list as $auction) {
                    $auction -> delete();
                }
            */
        return $list;
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		


    protected function integrateAuctionRateExt()
    {
        // при создании новой ставки, надо проверить, стал ли объект победителем и изменить данные у аукциона

        $query = (new \yii\db\Query())
            ->select(['count(*) as c', 'sum(ar.amount) as s', 'max(ar.created_at) as ar_date'])
            ->from('adr_auction_rates ar')
            ->where(['address_id' => $this->address_id, 'auction_id' => $this->auction_id]);

        $data = $query->one();


        $auction = $this->auction;
        $newAmount = $data['s'];// + $this -> amount;

        /*
        echo $this -> address_view;
        echo $newAmount.'<br>';
        exit;
        */


        if (!$auction->amount || ($auction->amount < $newAmount)) {

            //$auction -> price = $auction -> price $this -> amount;
            $auction->amount = $newAmount;
            $auction->address_id = $this->address_id;
            $auction->owner_id = $this->owner_id;
            $auction->rate_id = $this->id;
            $auction->rate_count = $data['c'];
            $auction->save();

        }
    }



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

    // Платеж
    protected function integratePayment()
    {
        $invoice = $this->invoice;

        if (is_null($invoice)) {
            $this->invoice_date = null;
            $this->invoice_number = null;
            $this->invoice_summa = null;
        } else {

            if (!$this->info) $this->info = $invoice->info;

            $this->invoice_date = date('Y-m-d', strtotime($invoice->created_at));
            $this->invoice_number = $invoice->number;
            $this->invoice_summa = $invoice->summa;
            $this->payer_id = $invoice->recipient_id;

            $this->recipient_id = $invoice->owner_id;

            $invoice->balance();
            $invoice->save();
        }


        $payment_order = $this->payment_order;

        if (is_null($payment_order)) {
            $this->payment_order_date = null;
            $this->payment_order_number = null;
            $this->payment_order_summa = null;
        } else {
            $this->payment_order_date = date('Y-m-d', strtotime($payment_order->created_at));
            $this->payment_order_number = $payment_order->number;
            $this->payment_order_summa = $payment_order->summa;

            $this->payer_id = $payment_order->owner_id;
            $this->recipient_id = $payment_order->recipient_id;
        }


        $this->payer_name = $this->payer ? $this->payer->getTitle() : '';
        $this->recipient_name = $this->recipient ? $this->recipient->getTitle() : '';

    }

    protected function integratePaymentExt()
    {
        //Вызываем пересчет баланса заказа, с которым связан оплачиваемый счет
        $invoice = $this->invoice;
        //if (!is_null($invoice) && is_null($invoice -> porder) && !is_null($invoice -> order)) {

        if (!is_null($invoice) && !is_null($invoice->order)) {
            $invoice->order->balance();
        }
        if (!is_null($invoice) && !is_null($invoice->address)) {
            $invoice->address->balance();
        }

    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

    // Заказ порталу
    protected function integratePorder()
    {
        $this->generateNumber();

        // Забираем необходимые значения у услуги
        if (!$this->service_id) throw new Exception('Не определен атрибут service_id');
        $service = $this->service;
        if ($this->summa == 0) $this->summa = $service->price; // меняем, если не указана
        if (!$this->service_name) $this->service_name = $service->name; // меняем, если не указано
        if (!$this->info) $this->info = $service->name; // меняем, если не указано

        // Реквизиты со стороны портала
        if (!$this->recipient) { // Если не указаны, то
            $this->recipient_id = $service->recipient_id; // Берем связанные с услугой - меняем жестко

            if (!$this->recipient_id) $this->recipient_id = self::portal()->default_profile_id; // берем реквизиты портала по умолчанию
        }

        // TODO: Возможно, надо добавить проверку согласованности роли заказчика и предназначения услуги (для кого)

        // согласовываем пользователя и реквизиты
        if ($this->customer_r_id && !$this->customer_id) {  // Если реквизиты установлены, то устанавливаем заказчика, чьи реквизиты
            $r = self::byId($this->customer_r_id);
            $this->customer_id = $r->user_id;
        } else {
            $customer = $this->customer;
            if (!$customer) {
                $this->customer_id = self::currentUser();
                $customer = $this->customer;
            }

        }

    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

    // Адрес
    protected function integrateAddress()
    {
        // Округ инициализируется либо по атрибуту nalog_id либо region_id
        $model = $this->nalog;
        if (!$model) $model = $this->region;
        if ($model) $this->okrug_id = $model->okrug_id;

        /*
        //Продление подписки при переходе статуса в Опубликован
        if ($this -> status -> alias == 'published') {
            $this -> subscribe_to = date('Y-m-d H:i:s', strtotime($this -> status_changed_at.' +1month'));
        }
        */

        // применяем вилку
        //$this -> price6 = $this -> price6?$this -> price6():0;
        //$this -> price11 = $this -> price11?$this -> price11():0;


        // Фиксируем время установки флажка Новый
        if (!$this->is_new && $this->hasOption('new')) {
            $this->is_new = Entity::dbNow();
        }

        if (!$this->canNew()) {
            $options = $this->options;

            if ($options) {
                $new_id = Option::byAlias('new')->id;
                $i = array_search($new_id, $options);
                if ($i !== false) unset($options[$i]); // удаляем флажок новый, если ставить его нельзя
                $this->options = $options;
            }
        }


    }

    protected function integrateAddressExt()
    {

        $photoCalendar = $this->getPhotoCalendars()->one();

        // Должен существовать заказ услуг фотографа
        if (is_null($this->photo_order) && $photoCalendar) { // Если его нет, и есть информация о заказе фотографа, то создаем портальный заказ на фотографа, а адрес переводим в статус "Ожидание оплаты"
            $autointegrate = $this->autointegrate;
            $this->autointegrate = false;
            $servicePortal = ServicePortal::byAlias('photography');
            $order = $servicePortal->order(false, false, true); // Создаем заказ
            $order->customer_r_id = $this->owner->default_profile_id;
            $order->save();
            $this->photo_order_id = $order->id;
            $order->bill(['address_id' => $this->id]);    // Выставляем счет по заказу фотографа
            $this->status_id = $this->statusByAlias('paymentwaiting')->id; // меняем статус адреса на ожидание оплаты
            $this->save();
            $this->autointegrate = $autointegrate;
        };

        if ((Yii:: $app->id != 'app-console') && !count($this->serviceIdList)) { // штатно адрес не может быть без справочника услуг, поэтому, если справочника услуг нет, то считаем, что адрес новый и тогда заполняем его услугами, которые должны быть автоматически добавлены
            $list = $this->serviceIdList;  // текущий список услуг в адресе
//			var_dump($list);
            $ownerList = $this->getCurrentUser()->ServicePriceList->andFilterWhere(['to_address' => 1])->all(); // список услуг из справочнике владельца, которые должны быть добавлены
            $ownerList = $ownerList ? array_keys($this->objectsByTemplate($ownerList, '')) : [];

            $adds = array_diff($ownerList, $list);// Список тех, которые должны быть добавлены, но их в справочнике адреса нет
            // добавляем
            foreach ($adds as $id) {
                $ownerServicePrice = ServicePrice::findOne($id);
                $ownerServicePrice->addAddressServicePrice($this->id, true);

                //$this -> addSerivcePrice(['service_name_id' => $ownerServicePrice -> service_name_id, 'address_id' => $this->id, 'name']) -> save();
            }
        }


        /*
        TODO: сделать, если попросят
        // синхронизуем услуги и опции
        $addressOptions = $this -> serviceOptions(); // список опционных услуг в справочнике адреса
        $ownerOptions = $this -> owner -> serviceOptions(); // список опционных услуг в справочнике владельца
        $options = $this -> options; // опции, установленные у адреса
        // Должны быть исключены опции
        var_dump($addressOptions); exit;
        */

    }

    protected function integrateAddressDel($real)
    {
        // Надо удалить заказ на фотографа и убрать резервирование времени
        if ($this->deletable) {
            if ($this->photo_order) {
                $this->photo_order->delete();
            };
            \frontend\models\PhotoCalendar::deletByAddress($this->id);
        };
        return true;
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

    // Услуга в прайс-листе
    protected function integrateServicePrice()
    {

        $owner_id = User::currentUser();

        if ($this->service_name_id === 0) $this->service_name_id = null;

        //*** согласование с PriceName, если системная, или со справочником владельца, если услуга из справочника адреса
        $serviceName = $this->service_name;
        if ($serviceName === false) throw new Exception('Установлен несуществующий id модели ServiceName');
        $values = [];
        $sServicePrice = false;

        if ($serviceName) { // Если это услуга системная
            $values = Entity::extractParams($serviceName->attributes, ['code', 'stype', 'name', 'alias', 'to_address', 'description']);// копируемые из справочной услуги атрибуты
        }

        if ($this->address) { //если услуга в справочнике адреса (адрес указан)
            $values['owner_id'] = $this->address->owner_id; // владелец должен совпадать с владальцем адреса
            // найдем эту услугу в справочнике владельца
            $ownerService = $this->getOwnerServicePrice();
            if ($ownerService && ($ownerService->id != $this->id)) {
                $values = Entity::extractParams($ownerService->attributes, array_keys($ownerService->attributes));// копируемые из справочной услуг владельца все атрибуты
                unset($values['id']);
                unset($values['enabled']);
            }
        }

        foreach ($values as $name => $value) {
            $v = $this->__get($name);

            // копируем атрибуты:
            $copy = false;
            if (in_array($name, ['code', 'stype', 'owner_id'])) { // в любом случае
                $copy = true;
            } else if (in_array($name, ['to_address'])) { // Если новая запись
                $copy = $this->getIsNewRecord();
            } else {
                $copy = (($v === null) || (is_string($v) && trim($v) == ''));  // Если значение пустое
            };
            if ($copy) $this->__set($name, $value);
        }

        // реквизиты
        if (!$this->recipient && $this->owner && $this->owner->default_profile_id) {
            $this->recipient_id = $this->owner->default_profile_id;
        }

    }

    protected function integrateServicePriceExt()
    {

        if ($this->address) { //если услуга в справочнике адреса
            $owner_id = User::currentUser();
            //$name = $this -> service_name;
            // проверим, есть ли эта услуга в справочнике владельца
            $ownerService = $this->getOwnerServicePrice();
            if (!$ownerService) { // Если в справочнике владельца нет такой услуги, то добавим
                //$ownerService = new ServicePrice();
                $values = Entity::extractParams($this->attributes, array_keys($this->attributes));// копируемые из справочной услуг владельца все атрибуты
                unset($values['id']);
                unset($values['address_id']);
                $ownerService = $this->addSerivcePrice($values);
                $ownerService->autoIntegrate = false;
                $ownerService->save();
            };
        }
    }

    protected function integrateServicePriceDel($real)
    {
        // не админам не позволяем удалять неудаляемые услуги
        if ($this->getCurrentUser()->checkRole(User::ROLE_ADMIN)) {
            $serviceName = null;
        } else {
            $serviceName = $this->service_name;
        }
        return !($serviceName ? $serviceName->undeleted : 0);
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

    // Позиция (услуга) в заказе
    protected function integrateService()
    {

        $order = $this->order; // Заказ, в который добавляется услуга (аргумент)


        if (!$order) throw new Exception('Не определен атрибут order_id');
        $servicePrice = $this->service_price;// Услуга с ценой, которая добавляется в заказ
        if (!$servicePrice) throw new Exception('Не определен атрибут service_price_id');
        $values = Entity::extractParams($servicePrice->attributes, ['code', 'stype', 'name']);// копируемые из услуги атрибуты
        ///$values = array_merge($values, Entity::extractParams($order -> attributes,['address_id', 'owner_id', 'duration']));// копируемые из заказа атрибуты
        $values = array_merge($values, Entity::extractParams($order->attributes, ['address_id', 'owner_id']));// копируемые из заказа атрибуты
        $values['service_price_id'] = $servicePrice->id;
        $values['order_id'] = $order->id;


        // Дата завершения оказания услуги
        if ($this->date_start) {
            $this->date_fin = date('Y-m-d', strtotime($this->date_start . '+' . $this->duration . ' month'));
            $this->date_fin = date('Y-m-d', strtotime($this->date_fin . ' -1 days'));
        }
        /*$debug=debug_backtrace();
        foreach($debug as $call){
            $bt[]=$call['function'];
        }
        echo '<pre>';
        var_dump($bt);
        echo '</pre>';*/
        // определяем цену строго по прайсу в зависимости от длительности
        //var_dump($order -> duration);
        switch ($order->duration) {
            case 'price_first_11':
            case 'price_change_11':
            case Address::getMeta('price_first_11', 'description'):
            case Address::getMeta('price_change_11', 'description'):
                $order->duration = Order::DURATION_11;
                break;
            case 'price_first_6':
            case 'price_change_6':
            case Address::getMeta('price_first_6', 'description'):
            case Address::getMeta('price_change_6', 'description'):
                $order->duration = Order::DURATION_6;
                break;
        }
        switch ($order->duration) {
            default:
                {
                    //die();
                    if ($servicePrice->stype != ServicePrice::STYPE_ONCE) throw new Exception('Недопустимое значение Длительности  для длительной услуги');// Длительная услуга не может существовать в разовом заказе
                    $price = $servicePrice->price;
                }
            case Order::DURATION_6:
                { // разовая услуга может существовать в длительном заказе, но при этом она остается разовой и цена используется разовая
                    if ($servicePrice->stype == ServicePrice::STYPE_ONCE) {
                        $price = $servicePrice->price;
                        $this->duration = Service::DURATION_ONCE;
                    } else {
                        $price = $servicePrice->price6;
                        $this->duration = Service::DURATION_6;

                    }
                    break;
                }
            case Order::DURATION_11:
                {
                    if ($servicePrice->stype == ServicePrice::STYPE_ONCE) {
                        $price = $servicePrice->price;
                        $this->duration = Service::DURATION_ONCE;
                    } else {
                        $price = $servicePrice->price11;
                        $this->duration = Service::DURATION_11;
                    }
                    break;
                }
        }


        $values['price'] = $price;

        foreach ($values as $name => $value) {
            $this->__set($name, $value);
        }

        $this->summa = $price * $this->durationValue; // в случае изменения правил расчета стоимости услуги, следует изменить

    }

    protected function integrateServiceExt()
    {

//		$this -> order -> integrate(false); // вызываем внутреннее согласования заказа
//		$this -> order -> save();

    }

    protected function integrateServiceDel()
    {
        // Изменение стоимости заказа
        $this->order->integrate(false); // вызываем внутреннее согласования заказа
        $this->order->save();
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

    // физическое лицо
    protected function integrateRperson()
    {
        // Владелец счета

        if (trim($this->account_owner) == '') $this->account_owner = trim($this->fname . ' ' . $this->mname) . ' ' . $this->lname;
    }

    protected function integrateRpersonExt()
    {
        // Реквизит по умолчанию для пользователя
        $user = $this->user;
        if (!$user->default_profile_id) {
            $user->default_profile_id = $this->id;
            $user->save();
        }
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // ИП
    protected function integrateRip()
    {
        // Владелец счета
        if (trim($this->account_owner) == '') $this->account_owner = trim($this->fname . ' ' . $this->mname) . ' ' . $this->lname;
    }

    protected function integrateRipExt()
    {
        // Реквизит по умолчанию для пользователя
        $user = $this->user;
        if (!$user->default_profile_id) {
            $user->default_profile_id = $this->id;
            $user->save();
        }
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

    // юридическое лицо
    protected function integrateRcompany()
    {
        if (trim($this->account_owner) == '') $this->account_owner = $this->name;
    }

    protected function integrateRcompanyExt()
    {
        // Реквизит по умолчанию для пользователя
        $user = $this->user;
        if (!$user->default_profile_id) {
            $user->default_profile_id = $this->id;
            $user->save();
        }
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

// Счета
    protected function integrateInvoice()
    {
        $this->generateNumber('pinvoice');
        $this->doc_type_id = DocumentType::byAlias('invoice')->id;
        return $this->parent()->integrateDocument(); // пока вызываем вручную

    }



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

    // Документы
    protected function integrateDocument()
    {

        if ($this->doc_type && ($this->doc_type->alias == 'contract')) { // Для Договора
            $this->generateNumber('ownerContract');
        }

        if ($this->doc_type && ($this->doc_type->alias == 'agreement')) { // Для Допника
            $this->generateNumber('ownerAgreement');
        }

        if ($this->doc_type && ($this->doc_type->alias == 'invoice')) { // Для счета
            /// Посчитаем сумму счета
            // получаем список услуг  и складываем сумму, попутно проверяем согласование реквизитов и заказа (счет может быть выставлен только на один заказ)
            $owner_id = $this->owner_id; // тот, кому платят - кто выставил счет
            $recipient_id = $this->recipient_id; // тот, кто платит - кому выставлен счет (заказчик)

            $order_id = false;
            $summa = 0;

            $porder = $this->porder;
            $order = $this->order;
            $address = $this->address;
            // Проверим согласование реквизитов счета и заказа, к которой относится услуга
            if (!is_null($porder)) { // Портальный заказ


                if ($recipient_id) {

                    // Перепрошьем
                    $this->owner_id = $porder->recipient_id;
                    $this->recipient_id = $porder->customer_r_id;

                    /*
                    if ($recipient_id != $order -> customer_r_id) {

                        echo "Проврка не прошла";
                        var_dump($this -> attributes);  echo "<br><br>";
                        var_dump($recipient_id); echo "<br><br>";
                        var_dump($order -> customer_r_id);
                        exit;
                    }
                    */
                    if ($recipient_id != $porder->customer_r_id) throw new Exception('Заказчик заказа не соответствует плательщику по счету ');
                } else {
                    // плательщик не указан, просто выставляем его
                    $this->recipient_id = $porder->customer_r_id;
                };


                $summa = $summa + $porder->summa;

            }


            if (is_null($porder) && !is_null($order)) { // обычный заказ

                if (!is_null($address)) { // Если к заказу привязан адрес, то добавим сумму адреса
                    $summa = $order->address_price;
                }
                // проходим по списку услуг, включенных в документ
                $list = $this->getServices()->all();

                foreach ($list as $item) {
                    if ($order_id) {
                        // прверка заказов
                        if ($item->order_id != $order_id) throw new Exception('В счете присутствуют услуги из разных заказов');
                    } else {
                        $order_id = $item->order_id;
                        $order = $item->order;
                    };


                    if (!$owner_id) {
                        // получатель денег не указан, просто выставляем его
                        $this->owner_id != $item->owner_id;
                        $owner_id = $this->owner_id;
                    };


                    $summa = $summa + $item->summa;
                }
            };

            $this->summa = $summa;
        }


    }

    protected function integrateDocumentExt()
    {

        if ($this->doc_type && (in_array($this->doc_type->alias, ['contract', 'agreement'])) && !is_null($this->recipient)) { // Для Договора

            $user = $this->recipient->user;

            if (!is_null($user) && $user->checkRole(User::ROLE_OWNER)/* && ($user -> status -> alias == 'new')*/) {
                if ($this->state == Document::CONTRACT_STATE_ACTIVE) { // Если для владельца в статусе Новый подписывается договор, то переводим владельца в статус Актиыный
                    $activeStatusId = Status::findOne(['alias' => 'active', 'table' => User::tableName()])->id;
                    $user->status_id = $activeStatusId;
                    $user->save();
                }
            }

        }


    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

    // Опции
    protected function integrateOption()
    {
        //var_dump($this -> alias); exit;
        if ($this->alias == '') {
            $this->alias = NULL;
        }

    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

    // Округ
    protected function integrateOkrug()
    {
        // генерация кода
        if (!$this->alias || (trim($this->alias) == '')) {
            $this->alias = strtolower(EntityHelper::ru2Lat(trim($this->abr)));
        };

    }

    protected function integrateOkrugExt()
    {

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

    // Метро
    protected function integrateMetro()
    {
        // генерация кода
        if (!$this->alias || (trim($this->alias) == '')) {
            $this->alias = strtolower(EntityHelper::ru2Lat(trim($this->name)));
        };

    }



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
// Страницы

    protected function integratePage()
    {
        // генерация кода
        if (!$this->slug || (trim($this->slug) == '')) {
            $i = 0;

            do {
                $slug = trim(strtolower(EntityHelper::ru2Lat(trim($this->h1))), '_');
                if ($i > 0) $slug = $slug . $i;
                $i++;
            } while (Page::find()->where(['and', ['slug' => $slug], ['not', ['id' => $this->id]]])->count());

            $this->slug = $slug;
        };

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	


    /*************************************************************************************************************************/
    /******************** ПЕРЕКРЫТИЯ МЕТОДОВ *********************************************************************************/
    /*************************************************************************************************************************/

// Возвращает список всех возможных значений атрибута $name в виде массива id => caption
    public function getRelatedListValues($name, $template = false, $condition = false, $required = null)
    {
        // подмена для поля recipient модели ServicePrice

        if (
            ($name == 'recipient_id') && ((self::baseClassName() == 'ServicePrice') || (self::baseClassName() == 'ServicePortal') || (self::baseClassName() == 'Porder'))

            ||
            ($name == 'customer_r_id') && ((self::baseClassName() == 'Porder') || (self::baseClassName() == 'Order'))
        ) {

            $condition = $this->getConditionFor($name);
            if (self::baseClassName() == 'ServicePortal') {

                $list = self::portal()->requisiteList($condition, $template);


            } else {

                switch ($name) {
                    case 'customer_r_id':
                        {
                            $user = $this->customer;
                            break;
                        };
                    case 'recipient_id':
                        {
                            if (self::baseClassName() == 'Porder') {
                                $user = $this->portal();
                                break;
                            }
                        };
                    default:
                        {
                            $user = $this->owner;
                        }
                }
                if ($user) {
                    $list = $user->requisiteList($condition, $template);
                } else {
                    $list = [];
                }
            }


            if ((is_null($required) && (self::getMeta($name, 'required') == 0)) || ($required === false))
                $list = ArrayHelper::merge(['' => ''], $list);  // Если поле необязательное, то добавляем сверху пустой вариант
            return $list;
        }
        return parent::getRelatedListValues($name, $template, $condition);


    }


// вспомогательный метод, который сортирует списки выбора
    protected static function sortList($list, $template, $noEmpty = false)
    {
        if (in_array(self::baseClassName(), ['Okrug', 'Metro'])) {

            asort($list);
            if ($noEmpty) {
                //пустое значение удалим
                if (count($list)) {
                    foreach ($list as $name => $value) {
                        break;
                    };
                    if (!$value) unset($list[$name]);
                }

            }
            $result = $list;

        } else if (in_array(self::baseClassName(), ['Nalog'])) {
            // сортировка списка налоговых по number // TODO: чтобы увеличить производительность, надо сортировку делать прямо в запросе

            //var_dump($list);

            //exit;

            $numbers = [];
            foreach ($list as $id => $name) {
                $numbers[$id] = Nalog::findOne($id)->number;

                //$result[$name] = (int)$value;
            }
            asort($numbers);
            $result = [];
            foreach ($numbers as $id => $number) {
                $result[$id] = $list[$id];
            }

        } else $result = $list;


        return $result;
    }

    public static function getListValues($name, $template = false, $required = null)
    {
        $list = self::sortList(parent::getListValues($name, $template, $required), $template);
        return $list;
    }

    public static function getList($attrName)
    {
        $result = parent::getList($attrName);
        return $result;
    }

    public static function listAll($template = false, $condition = false, $noEmpty = false)
    {

        $list = self::sortList(parent::listAll($template, $condition), $template, $noEmpty);
        return $list;

    }


}

?>