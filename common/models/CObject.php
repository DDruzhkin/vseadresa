<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;



/**
 * This is the model class for table "adr_objects".
 *
 * @property integer $id
 * @property string $tablename
 *
 * @property Document[] $documents
 * @property Document[] $documents0
 * @property Order[] $orders
 * @property Payment[] $payments
 * @property Payment[] $payments0
 * @property Porder[] $porders
 * @property Porder[] $porders0
 * @property Rcompany[] $rcompanies
 * @property Rip[] $rips
 * @property Rperson[] $rpeople
 * @property ServicePortal[] $servicePortals
 * @property ServicePrice[] $servicePrices
 */
class CObject extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_objects'; 
    }




	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'model' => 'CObject', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			
			return 
				[
									'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'tablename' => ['show' => '0', 'label' => 'Tablename', 'type' => 'string', 'required' => '0', 'visible' => '0', ], 

						
				];
	}		
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['tablename'], 'string', 'max' => 50],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments() {
        return $this->hasMany(Document::className(), ['recipient_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments0() {
        return $this->hasMany(Document::className(), ['owner_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Order::className(), ['customer_r_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments() {
        return $this->hasMany(Payment::className(), ['payer_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments0() {
        return $this->hasMany(Payment::className(), ['recipient_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPorders() {
        return $this->hasMany(Porder::className(), ['recipient_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPorders0() {
        return $this->hasMany(Porder::className(), ['customer_r_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRcompanies() {
        return $this->hasMany(Rcompany::className(), ['prior_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRips() {
        return $this->hasMany(Rip::className(), ['prior_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRpeople() {
        return $this->hasMany(Rperson::className(), ['prior_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicePortals() {
        return $this->hasMany(ServicePortal::className(), ['recipient_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicePrices() {
        return $this->hasMany(ServicePrice::className(), ['recipient_id' => 'id']);
    }



    public static function captionTemplate(){

        return "[%id%]";
    }



/* Возвращает число ссылок на модель */
			function hasLinks() {
				
				return 
					($this -> parent()?$this -> parent() -> hasLinks():0) + 
					$this -> getDocuments() -> count() + 
					$this -> getDocuments0() -> count() + 
					$this -> getOrders() -> count() + 
					$this -> getPayments() -> count() + 
					$this -> getPayments0() -> count() + 
					$this -> getPorders() -> count() + 
					$this -> getPorders0() -> count() + 
					$this -> getRcompanies() -> count() + 
					$this -> getRips() -> count() + 
					$this -> getRpeople() -> count() + 
					$this -> getServicePortals() -> count() + 
					$this -> getServicePrices() -> count();
			}
			





}