<?php

namespace common\models;

use Yii;
use yii\base\Behavior;
use yii\helpers\Url;

use InvalidArgumentException;

class OptionBehavior extends AddressListBehavior {
	public function getDeletable() {
		$model = $this -> owner;
		return ($model -> getAddressOptions() -> count() == 0) && ($model -> getServiceNames() -> count() == 0) && !$model -> undeleted;
		
	}
}