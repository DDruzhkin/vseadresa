<?php
namespace common\models;

use yii\base\Behavior;
use common\models\Entity;
use common\models\User;
use common\models\Invoice;
use common\models\Porder;
use common\models\ServicePortal;
use yii\base\Exception;
use yii\helpers\Inflector;
use yii\helpers\Url;

class PorderBehavior extends Behavior {
	
	
	public function getAdminMenuItems() {
		$model = $this -> owner;		
		$sections = [];
		

		$menuItems[] = [
			'label' => 'Платежы',
			'url' => Url::to(['/admin/'.Inflector::camel2id(Payment::baseClassName()), 'porder_id' => $model -> id])
		];
		
		$menuItems[] = [
			'label' => 'Услуга',
			'url' => Url::to(['/admin/'.Inflector::camel2id(ServicePortal::baseClassName()).'/update/', 'id' => $model -> service_id])
		];
		
		return $menuItems;

	
	}
	
	public static function getInvoiceDescription() {
		return 
		'
		<code>public function getInvoice()</code><br/>
		Возвращает выставленный счет.
		';
	}
		
	public function getInvoice() {
		$porder = $this -> owner;		
		return $porder -> getDocuments() -> one(); // 
	}
	
	
	
	public static function getIsPaiedDescription() {
		return 
		'
		<code>public function getIsPaied()</code><br/>
		Оплачен ли заказ.<br/>		
		Проходит по всем оплаченным счетам, привязанным к заказу и считает сумму. Если сумма таких счетов больше или равна сумме заказа, то возвращает true
		';
	}
	
	public function getIsPaied() {
		$model = $this -> owner;		
		$list = $model -> getDocuments() -> andWhere(['state' => 1]) -> all();
		$summa = 0;
		foreach($list as $item) {
			$summa = $summa + $item -> summa;
		}
		return  $summa >= $model -> summa; // 
		//return  $summa; // 
	}
	
	public static function billDescription() {
		return 
		'
		<code>public function bill($params = [])</code><br/>
		Выставить счет по заказу.<br/>
		В $params[\'info\'] можно передать комментарий к счету<br/>
		Если счет выставляется на портальную услугу в рамках обычного счета, то в параметрах надо передать order_id, чтобы привязать новый счет к обычному заказу<br/>
		Если счет уже выставлен, то новый не формируется, а возвращается старый. Если необходимо выставить новый счет, то его надо развязать с заказом<br/>		
		Созданный документ сразу сохраняется.  Заказ также будет сохранен для сохранения целостности<br/>
		Метод возвращает объект документа - выставленного счета.<br/>
		';
	}
	
	
	
	
	public function bill($params = []) {
		$porder = $this -> owner;		
		$invoice = $porder -> invoice; // проверим, есть ли привязанный счет
		
		if (!$invoice) {
			//echo "Создание нового счета<br>";
			$invoice = new Invoice();								
			//$docTypeId = (\common\models\DocumentType::byAlias('invoice')) ->id; 			
			//$invoice -> doc_type_id = $docTypeId;						
			
			if (isset($params['info'])) {
				$info = $params['info'];
			} else {
				$info = 'Оплата услуги портала по заказу №'.$porder -> number.' - '.$porder -> service -> name;
			}
			if (isset($params['order_id'])) $invoice -> order_id = $params['order_id'];
			if (isset($params['address_id'])) $invoice -> address_id = $params['address_id'];
			
			
			$invoice -> info = $info;
			$invoice -> porder_id = $porder -> id;
			//$porder -> invoice_id = $invoice -> id;
			//$porder -> save();			
			//$invoice -> recipient_id = $porder -> customer_r_id;
			//$invoice -> owner_id = $porder -> recipient_id;
		} else {
			// Здесь проверим согласованность заказа с имеющимся счетом			
			if ($porder -> customer_r_id && ($porder -> customer_r_id != $invoice -> recipient_id)) throw new Exception('Не согласован портальный заказ и счет:  заказчик заказа и плательщик по счету');
			if ($porder -> recipient_id && ($porder -> recipient_id != $invoice -> owner_id)) throw new Exception('Не согласован портальный заказ и счет:  реквизиты портала у заказа и получетль по счету');			
		}
		
		$invoice -> save(); //сохраняем для формирования целостности
		
		//echo "<br>"; var_dump($invoice -> attributes);
		return $invoice;
	}
		
}