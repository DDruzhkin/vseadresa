<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_payments".
 *
 * @property integer $id
 * @property string $date
 * @property string $ptype
 * @property string $value
 * @property integer $payment_order_id
 * @property integer $invoice_id
 * @property string $info
 * @property string $data
 * @property string $loaded_at
 * @property string $payment_order_date
 * @property string $payment_order_number
 * @property double $payment_order_summa
 * @property integer $payer_id
 * @property string $payer_name
 * @property string $invoice_date
 * @property string $invoice_number
 * @property double $invoice_summa
 * @property integer $recipient_id
 * @property string $recipient_name
 * @property string $pay_id
 * @property integer $confirmed
 *
 * @property CObject $payer
 * @property CObject $recipient
 * @property Document $invoice
 * @property Document $paymentOrder
 */
class Payment extends \common\models\EntityExt 
{
	
	/* Константы модели */
	const PTYPE_NONE = 'не задан';
	const PTYPE_CACH = 'Наличными';
	const PTYPE_CARD = 'По карте';
	const PTYPE_BANK = 'По счету';
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_payments'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [
			'ptype' => ['Наличными' => 'Наличными', 'По карте' => 'По карте', 'По счету' => 'По счету', ],  

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Платежи', 
			'model' => 'Payment', 
			'vinp' => 'Платеж', 
			'imp' => 'Платеж', 
			'rodp' => 'Платежа', 
			'parent' => 'objects', 
			'caption' => 'на сумму %value% руб.', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'date' => ['label' => 'Дата', 'comment' => 'Дата поступления платежа', 'type' => 'date', 'format' => 'd.m.Y', 'filter' => 'range', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'ptype' => ['label' => 'Вид платежа', 'comment' => '', 'type' => 'enum', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'value' => ['format' => '[\'decimal\', 2]', 'label' => 'Сумма', 'comment' => 'Сумма платежа', 'mask' => '@decimal', 'default' => "0.0000", 'type' => 'decimal', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'payment_order_id' => ['label' => 'Платежка', 'comment' => 'Платежное поручение, по которому произведена оплата', 'link' => '1', 'show' => '0', 'model' => 'Document', 'type' => 'ref', 'required' => '0', 'visible' => '0', ], 
			'invoice_id' => ['label' => 'Счет', 'comment' => 'Счет, по которому была произведена оплата', 'link' => '1', 'visible' => '0', 'model' => 'Document', 'type' => 'ref', 'required' => '0', 'show' => '1', ], 
			'info' => ['show' => '1', 'label' => 'Информация', 'comment' => 'Дополнительная информация о платеже', 'visible' => '0', 'type' => 'text', 'required' => '0', ], 
			'data' => ['show' => '0', 'type' => 'json', 'label' => 'Data', 'required' => '0', 'visible' => '0', ], 
			'loaded_at' => ['readonly' => '0', 'label' => 'Дата загрузки', 'comment' => 'Дату загрузки платежа в систему, если платеж был импортирован', 'visible' => '0', 'type' => 'timestamp', 'required' => '0', 'show' => '1', ], 
			'payment_order_date' => ['show' => '0', 'label' => 'Payment Order Date', 'type' => 'timestamp', 'required' => '0', 'visible' => '0', ], 
			'payment_order_number' => ['show' => '0', 'label' => 'Payment Order Number', 'type' => 'string', 'required' => '0', 'visible' => '0', ], 
			'payment_order_summa' => ['label' => 'Сумма по платежке', 'show' => '0', 'type' => 'float', 'required' => '0', 'visible' => '0', ], 
			'payer_id' => ['label' => 'Реквизиты плательщика', 'show' => '0', 'model' => 'CObject', 'type' => 'ref', 'required' => '0', 'visible' => '0', 'link' => '0', ], 
			'payer_name' => ['label' => 'Плательщик', 'filter' => 'like', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'invoice_date' => ['label' => 'Дата счета', 'visible' => '1', 'filter' => 'range', 'type' => 'date', 'required' => '0', 'show' => '1', ], 
			'invoice_number' => ['label' => 'Номер счета', 'visible' => '1', 'filter' => 'like', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
			'invoice_summa' => ['label' => 'Сумма по счету', 'show' => '0', 'type' => 'float', 'required' => '0', 'visible' => '0', ], 
			'recipient_id' => ['label' => 'Реквизиты получателя', 'show' => '0', 'model' => 'CObject', 'type' => 'ref', 'required' => '0', 'visible' => '0', 'link' => '0', ], 
			'recipient_name' => ['label' => 'Получатель', 'filter' => 'like', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'pay_id' => ['show' => '0', 'comment' => 'используется для эквайринга', 'label' => 'Pay ID', 'type' => 'string', 'required' => '0', 'visible' => '0', ], 
			'confirmed' => ['show' => '0', 'comment' => 'платеж проведен', 'label' => 'Confirmed', 'default' => 0, 'type' => 'integer', 'required' => '0', 'visible' => '0', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	


	public function getData($name) {
		return $this -> getArrayValue('data', $name);
	}
			
	public function setData($name, $value, $save = false) {
		$this -> setArrayValue('data', $name, $value, $save);
	}
			





    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id'], 'required'],
            [['id', 'payment_order_id', 'invoice_id', 'payer_id', 'recipient_id', 'confirmed'], 'integer'],
            [['date', 'loaded_at', 'payment_order_date', 'invoice_date'], 'safe'],
            [['ptype', 'info', 'data'], 'string'],
            [['value', 'payment_order_summa', 'invoice_summa'], 'number'],
            [['payment_order_number', 'invoice_number', 'pay_id'], 'string', 'max' => 50],
            [['payer_name', 'recipient_name'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['payer_id'], 'exist', 'skipOnError' => true, 'targetClass' => CObject::className(), 'targetAttribute' => ['payer_id' => 'id']],
            [['recipient_id'], 'exist', 'skipOnError' => true, 'targetClass' => CObject::className(), 'targetAttribute' => ['recipient_id' => 'id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::className(), 'targetAttribute' => ['invoice_id' => 'id']],
            [['payment_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::className(), 'targetAttribute' => ['payment_order_id' => 'id']],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayer() {
        return $this->hasOne(CObject::className(), ['id' => 'payer_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient() {
        return $this->hasOne(CObject::className(), ['id' => 'recipient_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice() {
        return $this->hasOne(Document::className(), ['id' => 'invoice_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrder() {
        return $this->hasOne(Document::className(), ['id' => 'payment_order_id']);
    }



    public static function captionTemplate(){

        return "на сумму %value% руб.";
    }





}