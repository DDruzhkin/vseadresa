<?php
namespace common\models;

use YII;
use yii\base\Behavior;
use yii\helpers\Url;

use common\models\EntityExt;
use frontend\models\Order;
use frontend\models\ServicePrice;

use kartik\icons\Icon;

class OrderBehavior extends Behavior
{



	public static function addServiceDescription() {
		return
		'
		<code>public function addService($params, $model = NULL, $save = false)</code><br/>
		Добавить в заказ услугу. Необходимо передать id услуги из прайса (класс ServicePrice) в параметре service_price_id.<br/>
		В параметрах $params можно передать другие параметры для инициализации созданной услуги, например,  date_start.<br/>
		$save - сохранить созданную модель услуги<br/>
		Параметр функции $model не используется.		
		';
	}


	public function addService($params, $model = NULL, $save = false) {

		$order = $this -> owner;

		if (!isset($params['service_price_id'])) die ('Не указан обязательный параметр service_price_id для добавления услуги в заказ');
		$servicePriceId = (int)$params['service_price_id'];
		$servicePrice = ServicePrice::findone($servicePriceId);
		if (!$servicePrice) die ('Указан некорректный обязательный параметр service_price_id для добавления услуги в заказ');

/*
		if	(($servicePrice -> stype == 'Ежемесячная') && ($order -> duration == 'разово')) die ('Добавляемая в заказ услуга не согласована с заказом - длительность');  // "разовый" заказ не согласуется с длительной услугой
		if	(((int)$servicePrice -> address_id !== (int)$order -> address_id)) {
				die ('Добавляемая в заказ услуга не согласована с заказом - адреса: '.$servicePrice -> address_id.' !== '.$order -> address_id); 	// несогласованы адреса
		};
		if	(((int)$servicePrice -> owner_id !== (int)$order -> address -> owner_id))die ('Добавляемая в заказ услуга не согласована с заказом - владельцы');	// несогласованы владельцы

*/

		$params['order_id'] = $order -> id;

		// проверим, существует ли уже такая услуга в заказе
		$model = Service::findone(Service::extractParams($params, ['order_id', 'service_price_id']));

		if(!$model)	{
			$model = new Service();//создаем услугу, если в заказе такой еще не существует
		}

		// инициализируем услугу
		$model -> load($params, '');


		if ($save) {
			$model -> save();
		} else {
			$model -> integrate();
		}

		return $model;
	}

	
			

	public static function deleteServiceDescription() {
		return
		'
		<code>public function deleteService($params)</code><br/>
		Удалить услугу из заказа. Необходимо передать id услуги из прайса (класс ServicePrice) в параметре service_price_id.<br/>	
		Удаляется услуга только если она действительно относится к этому заказу.<br/>	
		';
	}
	public function deleteService($params) {

		$order = $this -> owner;
		if (!isset($params['service_price_id'])) die ('Не указан обязательный параметр service_price_id для удаления услуги из заказа');
		$servicePriceId = $params['service_price_id'];
		Service::deleteAll(['order_id' => $order -> id, 'service_price_id' => $servicePriceId]);
		$order -> integrate();
	}

	public static function clearDescription() {
		return
		'
		<code>public function clear() </code><br/>
		Очистить заказ от всех входящих в него услгуг - удаляются все заказанные услуги из заказа<br/>			
		';
	}
	public function clear() {
		$order = $this -> owner;
		Service::deleteAll(['order_id' => $order -> id]);
		$order -> integrate();
	}


	public static function getServiceIdListDescriiption() {
		return '
			Список id услуг в заказе (ИД из справочника адресов)
		';
	}

	public function getServiceIdList() {
		$order = $this -> owner;
		$list = $order -> getServices() -> all();

		$result = [];
		foreach($list as $item) {
			$result[] = $item -> service_price_id;
		}
		return $result;
	}

	public static function setServiceIdListDescriiption() {
		return '
			Инициализация списка id услуг в заказе (ИД из справочника адресов)<br/>
			Удаляются услуги, которых нет в новом списке и добавляются из списка, но нет в заказе<br/>
		';
	}

	public function setServiceIdList($value) {
		$order = $this -> owner;
		$newList = $value?$value:[];
		// старый список
		$list = $order -> serviceIdList;
		if ($list) {
			$oldList = $list;
		} else $oldList = [];

		// разница

		$dels = array_diff($oldList, $newList); // те, которые надо удалить
		$adds = array_diff($newList, $oldList); // те, которые надо добавить

		//Удаляем
		foreach($dels as $id) {
			//echo  'deleting: '.$id.' -> '.ServicePrice::findOne($id) -> getAddressServicePrice($address -> id) -> id.'<br>';
			$order -> deleteService(['service_price_id' => $id], null, true);

		}

		// добавляем
		foreach($adds as $id) {
			$order -> addService(['service_price_id' => $id], null, true);
//			ServicePrice::findOne($id) -> addAddressServicePrice($address -> id, true);						
		}
	}


	public static function assignDescription() {
		return
		'
		<code>public function assign($params, $model = NULL, $save = false){</code><br/>
		Заказ заполняется переданными данными - массив значений атрибутоы</br>
		Можно передать массив ID услуг из прайс-листа (класс ServicePrice) в параметре services. Если этот параметр передан, то состав заказа будет соответствовать этому массиву. </br>
		Если не передал, то изменений в составе не произойдет.</br>
		Метод возвращает объект заказа.</br>
		';

	}

	public function assign($params, $model = NULL, $save = false) {
		$order = $this -> owner;
		$order -> load($params, '');

		if (isset($params['services'])) { // Синхронизируем переданный список услуг с имеющимся в заказе
			if (!$order -> id) die ('К новому заказу без его сохранения невозможно прикрепить объекты');
			$serviceList = $params['services']; // Список ID услуг из прайса
			for($i = 0; $i < count($serviceList); $i++) {
				$serviceList[$i] = (int)$serviceList[$i];
			}
			$list = $order -> getServices() -> all();
			$orderServiceList = [];
			foreach($list as $item) {
				$id = $item -> service_price_id;
				if (!in_array($id, $orderServiceList)) {
					$orderServiceList[] = (int)$id;
				}
			}
			foreach($orderServiceList as $servicePriceId) {
				if (!in_array($servicePriceId, $serviceList)) { // удаляем лишние
					$order -> deleteService(['service_price_id' => $servicePriceId]);
				}
			}


			foreach($serviceList as $servicePriceId) {
				if (!in_array($servicePriceId, $orderServiceList)) { // // добавляем недостающие
					$order -> addService(['service_price_id' =>  $servicePriceId], null, true);
				}
			}
		};


		$order -> integrate();
		return $order;
	}



	public static function canCloneDescription() {
		return
		'
		<code>public function canClone() </code><br/>
		Может ли заказчик создать новый заказ на основе текущего. Определяетя текущим статусом заказа.<br/>			
		';
	}
	public function canClone() {
		$status = $this -> owner -> status -> alias;
		return in_array($status, 'extension', 'completed', 'cancel');
	}

	public static function changeStatusDescription() {
		return
		'
		<code>public function changeStatus($alias, $save = false)</code><br/>
		Изменяет статус заказа по алиасу статуса <br/>			
		Проверяет возможность такой смены<br/>			
		Если статус изменен успешно, то возвращает true<br/>			
		';
	}
	public function changeStatus($alias, $save = false) {
		$result = false;
		$order = $this -> owner; // текуший заказ
		$list = $order -> getStatusList("%alias%"); // список возможных вариантов статусов для изменения
		
		$status_id = array_search ($alias, $list); // получим id статуса по алиасу среди списка возможных значений
		
		if ($status_id) {
			$order -> status_id = $status_id;
			if ($save) $order -> save();
		}
		return $result;
	}

	
	public static function balanceDescription() {
		return
		'
		<code>balance($checkStatus = true; $save = false)</code>
		Вычисляет сумму оплаты по всем счетам по этому заказу.<br>
		Если $checkStatus, то выставляет статус в оплачен или частично оплачен.<br>
		Если ни одной оплаты не было, то статус не затрагивается.<br>
		Возвращает оплаченную сумму по заказу.<br>
		Для изменения статуса этот метод должен вызываться в местах оплаты
		';
	}
	
	public function balance($checkStatus = true, $save = true) {
		$order = $this -> owner;
		$query = (new \yii\db\Query()) 
			-> select('SUM(p.value) as value')
			-> from(Payment::tableName().' p, '.Document::tableName().' d')
			-> where('p.invoice_id = d.id')
			-> andWhere(['d.order_id' => $order -> id, 'p.confirmed' => 1]);
		
		$paied = ($query -> one())['value'];

		//var_dump($paied); echo "<br>";
		if ($checkStatus && !is_null($paied)) { 			
		//echo $order -> summa.' > '.$paied.'<br>';
			if ($order -> summa > $paied) { // частичная оплата
				$order -> changeStatus('partpaid', false);
			} else { // полная оплата
				$order -> changeStatus('paid', false);
				// Устанавливаем сроки выполненяи заказа (аренды)
				$order -> date_start = Entity::dbNow(); // Дата окончания автоматически вычислится при сохранении заказа	
			}
		};
		
		if ($save) $order -> save();
		
		return $paied;
		
	}
	
	
	public static function calculateDescription() {
		return
		'
		<code>public function calculate($save = false) </code><br/>
		Производит рассчет сумм по заказу. <br/>
		Возвращает массив со структурой суммы заказа<br/>
		Также функция обовляет значение атрибута Summa<br/>
		В случае $save = true - заказ сохраняется.
		';
	}
	public function calculate($save = false) {
		$order = $this -> owner;		
		$result["address"] = ['title' => 'Стоимость аренды адреса', 'value' => $order -> address_price];

		$result["services"] = ['title' => 'Стоимость дополнительных услуг ', 'value' => (float)\common\models\Service::find()
								->where(['order_id' => $order -> id])
								->sum('summa')];
		
								
		// при привязке к заказку других портальных услуг необходимо дополнить
		$portalSumm = 0;
		$portalSumm = $portalSumm + ($order -> delivery_id ? (float)($order -> delivery -> summa) : 0);		
		$portalSumm = $portalSumm + ($order -> guarantee_id ? (float)($order -> guarantee -> summa) : 0);
		
		$result["portal"] = ['title' => 'Стоимость услуг портала', 'value' => $portalSumm];

		$total = 0;
		foreach($result as $name => $value) {
			$total = $total + (float)$value['value'];
		}
		$result["total"] = ['title' => 'Общая сумма заказа', 'value' => $total];
		$order -> summa = $total;
		if ($save) $order -> save();

		return $result;

	}



	public function getInvoices() {
		$result = [];
		$order = $this -> owner;
		$services = $order -> getServices() -> all(); // список услуг в заказе
		foreach($services as $serivce) { // формируем список различных реквизитов и соответствюущих им счетов, если они существуют
			$owner_id = $serivce -> service_price -> recipient_id; // реквизиты владельца документа (счета) - реквизты получателя оплаты
			$invoice = $serivce -> invoice;
			if ($invoice) {
					$result[$owner_id] =  $invoice; // Если счет уже есть, то сохраним его
			} else if (!array_key_exists($owner_id, $result)) $result[$owner_id] = null;
		}

		return $result;
	}


	public static function confirmDescription() {
		return
		'
		<code>public function confirm($params = [], , $save = true)</code><br/>
		Подтвердждение заказа. Выставляются счета (вызов метода bill)
		';
	}


	public function confirm($params = [], $save = true) {

		// выставляем счета		
		$order = $this -> owner;
		$order -> bill($params, $save);

	}


	public static function billDescription() {
		return
		'
		<code>public function bill($params = [], , $save = false)</code><br/>
		Выставить счет по заказу.<br/>
		В $params[\'info\'] можно передать комментарий к выставляемым счетам<br/>
		Если счет уже выставлен, то он не заменяется. Если необходимо выставить новый счет, то его надо развязать с услугами<br/>		
		У одного заказа может быть неколько счетов<br/>
		Созданные счета сразу сохраняются.  Заказ также будет сохранен для сохранения целостности<br/>
		Метод возвращает массив объектов документов - все выставленные счета по заказу.<br/>
		';
	}


	public function bill($params = [], $save = false) {
		// TODO: Упаковать весь рассчет в транзакцию
	
		$order = $this -> owner;
		
		if (!in_array($order -> status -> alias, ['new'])) return false;// Операция производится только в статусах заказа 'new'
		
		$services = $order -> getServices() -> all(); // список услуг в заказе
		
		$info = (isset($params['info']))?$params['info']:'Оплата услуг владельца адреса по заказу №'.$order -> number;

		$model = false; // 
		$result = []; //здесь будет храниться список счетов по заказу
		
		
		
		// снача сформируем отдельный счет на аренду адреса (услуги на те же реквизиты с этим счетом сгруппируются)
		$address_r = $order -> owner -> default_profile_id; // Реквизиты, на которые будут оплачиваться аренда адреса;
		$invoice = new Invoice();				
		$invoice -> info = $info;
		$invoice -> address_id = $order -> address_id;
		$invoice -> owner_id = $address_r;
		if (!$order -> id) $order -> save(); // чтобы получить id
		$invoice -> order_id = $order -> id;		
		$result[$address_r] = $invoice;				
				
		
		// группируем по реквизитам получателя оплаты (владельца), попутно считаем суммы по каждой группе
		foreach($services as $serivce) { // формируем список различных реквизитов и соответствюущих им счетов (если они существуют) на основе списка услуг в заказе
			$owner_id = $serivce -> service_price -> recipient_id; // реквизиты владельца документа (счета) - реквизты получателя оплаты - тот, кто оказывает услугу
			$invoice = $serivce -> invoice;			
			if ($invoice) {
					$result[$owner_id] =  $invoice; // Если счет уже есть, то сохраним его в массиве
			} else if (!array_key_exists($owner_id, $result)) $result[$owner_id] = null;
			

		}		

		// проходим по списку сгруппированных реквизитов		
		foreach($result as $owner_id => $invoice) {
			if (!$invoice) { // счета нет, поэтому создаем новый счет
				$invoice = new Invoice();
				//$invoice -> doc_type_id = (\common\models\DocumentType::byAlias('invoice')) ->id;
				$invoice -> info = $info;
				$invoice -> owner_id = $owner_id;				
				$result[$owner_id] = $invoice;				
			};
			
			if (!$order -> id) $order -> save(); // чтобы получить id	
			$invoice -> recipient_id = $order -> customer_r_id;
			$invoice -> order_id = $order -> id;			
			//$invoice -> save();				

		}

		
		
		
		// снова пройдем по услугам и привяжим к каждой соответствующий счет
		foreach($services as $serivce) {
			$owner_id = $serivce -> service_price -> recipient_id;
			if (is_array($result[$owner_id])) continue;
			$result[$owner_id] -> save();
			$serivce -> invoice_id = $result[$owner_id] -> id;			
			$serivce -> save();
		}		
		
		// Еще раз пройдем по счетам, чтобы они пересчитались с учетом привязок услуг
		foreach($result as $owner_id => $invoice) {
			$invoice -> save();			
		}
		
		// * Добавим в список счетов счета по портальным услугам		

		// Доставка
		if ($invoice = $order -> delivery_id) {
			$invoice = $order -> delivery -> bill(['order_id' => $order -> id]); // Выставляем счета на заказ доставки (портальная услуга)
			//$result[$order -> delivery -> service -> recipient_id] = $invoice;
			$result[$invoice -> owner_id] = $invoice;			
		}
		// Гарантия
		if ($invoice = $order -> guarantee_id) {
			$invoice = $order -> guarantee -> bill(['order_id' => $order -> id]); // Выставляем счета на заказ гарантии		
			$result[$invoice -> owner_id] = $invoice;
		}		

		$order -> changeStatus('bill', $save);

		return $result;

	}

	public static function addressPriceDescription() {
		return
		'
		Возвращает стоимость заказанного адреса с учетом выбранного периода
		';
	}
	public function addressPrice() {
		$order = $this -> owner;

		return $order->address_price;
	}


	public static function getEditableDescription() {
		return
		'
		Может ли быть заказ изменен. Зависит от его текущего статуса.
		';
	}

	public function getEditable() {
		return in_array($this -> owner -> status -> alias , ['new','draft']);
	}

	
	
	public static function getAdminMenuItemsDescription() {
		return
		'
		Возвращает пункты меню для адреса, в Админке, в зависимости от текущего состояния заказа.
		';
	}

	public function getAdminMenuItems() {
		$model = $this -> owner;		
		$sections = [];
		$conrollerId = strtolower($model -> baseClassName());
	
		$menuItems = false;
	
		if (($model -> status -> alias == 'delivery') && ($model -> delivery_type -> alias == 'pickup')) { // Если в статус Доставка и способ доставки Самовывоз
			$menuItems[] = [
				'label' => 'В пункте самовывоза',
				'url' => Url::to(['/admin/'.$conrollerId.'/pickup/', 'id' => $model -> id])
			];				
		};
		
		if (in_array($model -> status -> alias, ['delivery', 'pickup'])) {
			$menuItems[] = [
				'label' => 'Выполнен',
				'url' => Url::to(['/admin/'.$conrollerId.'/done/', 'id' => $model -> id])
			];	
		}
	
	
		if ($menuItems && count($menuItems) > 0) {
			$menuItems[] = '<li class="divider"></li>';
		}
		
	
		$menuItems[] = [
			'label' => 'Карточка заказа',
			'url' => Url::to(['/admin/'.$conrollerId.'/view/', 'id' => $model -> id])
		];
		
		$menuItems[] = [
			'label' => 'Заказчик',
			'url' => Url::to(['/admin/user/view/', 'id' => $model -> customer_id])
		];
		
		$menuItems[] = [
			'label' => 'Владелец',
			'url' => Url::to(['/admin/user/view/', 'id' => $model -> owner_id])
		];
		
		$menuItems[] = '<li class="divider"></li>';
		$menuItems[] = [
			'label' => 'Расчеты',
			'url' => Url::to(['/admin/payment/', 'order_id' => $model -> id])
		];

		return $menuItems;

	
	}	
	

	public static function getOwnerMenuItemsDescription() {
		return
		'
		Возвращает пункты меню списка заказов в ЛКВ, в зависимости от текущего состояния заказа
		';
	}

	public function getOwnerMenuItems() {
		$order = $this -> owner;
		$menuItems =[];
		
		if (in_array($order -> status -> alias, ['paid'])) {
			$menuItems[] =
			[
				'label' => Icon::show('newspaper-o', ['style'=>'margin-left:-16px; font-size: 8pt;']).' На формирование',
				'encodeLabels' => true, 
				'url' => Url::toRoute(['formation', 'id' => $order -> id])
			];
		};
		
		if (in_array($order -> status -> alias, ['formation'])) {
			$menuItems[] =
			[
				'label' => Icon::show('truck', ['style'=>'margin-left:-16px; font-size: 8pt;']).' На доставку',
				'encodeLabels' => true, 				
				'url' => Url::toRoute(['delivery', 'id' => $order -> id])
			];
		};



		if (in_array($order -> status -> alias, ['bill','partpaid', 'paid', 'formation', 'delivery', 'pickup', 'done', 'extension', 'completed'])) {
			$menuItems[] =
			[
				'label' => 'Счета',
				'url' => Url::toRoute(['document', 'id' => $order -> id])
			];
		};


		$menuItems[] =
			[
				'label' => 'Карточка заказа',				
				'url' => Url::toRoute(['view', 'id' => $order -> id]),
			];




		return $menuItems;


	}


	public static function getCustomerMenuItemsDescription() {
		return
		'
		Возвращает пункты меню списка заказов в ЛКВ, в зависимости от текущего состояния заказа
		';
	}

	public function getCustomerMenuItems() {
		$order = $this -> owner;

		if (in_array($order -> status -> alias, ['bill','partpaid', 'paid', 'formation', 'delivery', 'pickup', 'done', 'extension', 'completed'])) {
			$menuItems[] =
			[
				'label' => 'Счета',
				'url' => Url::toRoute(['document', 'id' => $order -> id])
			];
		};

		if (in_array($order -> status -> alias, ['new'])) {
			$menuItems[] =
			[
				'label' => 'Подтвердить',
				'url' => Url::toRoute(['confirm', 'id' => $order -> id])
			];
		};
/*
		if (in_array($order -> status -> alias, ['bill','partpaid'])) {
			$menuItems[] =
			[
				'label' => 'Оплатить',
				'url' => Url::toRoute(['pay', 'id' => $order -> id])
			];
		};

*/

//		$menuItems[] = 	'<li class="divider"></li>';

		if ($this -> editable) {
			$menuItems[] =
			[
				'label' => $order -> status -> alias == 'draft'?'Закончить оформление':'Изменить',
				'url' => Url::toRoute(['update', 'id' => $order -> id]),
			];
		};

		if (in_array($order -> status -> alias, ['extension','completed', 'canceled'])) {
			$menuItems[] =
			[
				'label' => 'Новый',
				'url' => Url::toRoute(['clone', 'id' => $order -> id]),
			];
		}

		$menuItems[] =
			[
				'label' => 'Карточка заказа',
				'url' => Url::toRoute(['view', 'id' => $order -> id]),
			];

		if ($order -> canGuarantee()) {
			$menuItems[] =
			[
				'label' => 'Заказать гарантию',
				'url' => Url::toRoute(['guarantee', 'id' => $order -> id]),
			];
		}


			
			
		return $menuItems;

	}

	
	public static function canGuaranteeDescription() {
		return '
		Возможно ли заказать услугу гарантии. <br>
		Условия:<br>		
		- у заказа нет заказа гарантии<br>
		- У адреса есть опция Гарантия<br>		
		- если заказ в статусе Выполнен, то в течение 3-х суток после того, как заказ в этот статус был переведен
		- не в статусе Отменен
		';
	}
	
	public function canGuarantee() {
		return false;
		$order = $this -> owner;
		$address = $order -> address;		
		return  
			 (!$order -> guarantee_id) 
			&& $address -> hasOption('guarantee')
			&& (!($order -> status -> alias == 'done') || ($order -> status -> alias == 'done') && ((strtotime(Entity::dbNow()) - strtotime($order -> status_changed_at))/86400 < 3))
			&& !($order -> status -> alias == 'canceled');
	}
	
	
	public static function createGuaranteeDescription() {
		return '
		Создает услуги гарантии для текущего заказа<br/>
		Если гарантия уже существует, то новая не создается, возвращается существующая.<br/>
		Если гарантия создается, то отправляется уведомление Заказчику<br/>
		Если $bill = true, то сразу формируются счета<br/>
		';
	}
	
	public function createGuarantee($bill = true) {
		$order = $this -> owner;		
		if ($order -> guarantee_id)  return $order -> guarantee;
		$result = \common\models\ServicePortal::byAlias('guarantee') -> order([], NULL, true);		
		$order -> guarantee_id = $result -> id;	
		$result -> customer_r_id = $order -> customer_r_id;
		$result -> save();		
		$order -> save();		
		if ($bill) $result -> bill(['order_id' => $order -> id]); // Выставляем счета на заказ гарантии (портальная услуга)				
		return $result;
	}
	
	
}
?>