<?php

namespace common\models;

class AddressTaxAgency extends EntityExt
{
    /**
     * Declares the name of the database table associated with this AR class.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'adr_address_tax_agency';
    }

    /**
     * Returns the validation rules for attributes.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['address_id', 'tax_agency_id'], 'integer'],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::class, 'targetAttribute' => ['address_id' => 'id']],
            [['tax_agency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nalog::class, 'targetAttribute' => ['tax_agency_id' => 'id']],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::class, ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxAgency()
    {
        return $this->hasOne(Nalog::class, ['id' => 'tax_agency_id']);
    }

    public static function getAddressAgencyId($addr_id)
    {
        return array_column(self::find()->select('tax_agency_id')->where(['address_id' => $addr_id])->asArray()->all(), 'tax_agency_id');
    }

    public static function getAddressAgencies($addr_id)
    {
        return array_map(function ($item){
            return $item['taxAgency']['number'];
        },
            self::find()->where(['address_id' => $addr_id])->with('taxAgency')->asArray()->all()
        );
    }
}