<?php
namespace common\models;

use yii;
use yii\base\Behavior;
use yii\helpers\Url;
use common\models\EntityExt;

class ReviewBehavior extends AddressListBehavior
{

public static function getAdminMenuItemsDescription() {
		return
		'
		Возвращает пункты меню для отзыва в ЛКА, в зависимости от текущего состояния отзыва
		';
	}
	
	public function getDeletable() {
		return true;
	}
	
	public function getAdminMenuItems() {
		$review = $this -> owner;		
		$menuItems = [];
		$statuses = $review -> status -> getTo() -> all();
		$statuses = array_flip(Entity::objectsByTemplate($statuses, '%alias%'));
		$labels = ['active' => 'Показать', 'noactive' => 'Убрать'];
		foreach($labels as $alias => $label) {
			if (!array_key_exists($alias, $statuses)) continue;
			$menuItems[] = [
				'label' => $label,				
				'url' => Url::to(['/admin/review/to-'.$alias, 'id' => $review -> id]),
			];
		};
		
		if (count($menuItems)) $menuItems[] = '<li class="divider"></li>';
		$menuItems[] = [
			'label' => 'Изменить',				
			'url' => Url::to(['/admin/review/update', 'id' => $review -> id]),
		];
		
		/*
		$menuItems[] = [
			'label' => 'Просмотреть',				
			'url' => Url::to(['/admin/review/view', 'id' => $review -> id]),
		];
		*/
		return $menuItems;
		
		
		
	}	
}

?>