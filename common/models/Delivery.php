<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_deliveries".
 *
 * @property integer $id
 * @property string $alias
 * @property string $name
 * @property string $description
 * @property string $price
 * @property integer $delivery_requirement
 *
 * @property Order[] $orders
 */
class Delivery extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_deliveries'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Способы доставки', 
			'rodp' => 'способа доставки', 
			'vinp' => 'способ доставки', 
			'imp' => 'способ доставки', 
			'model' => 'Delivery', 
			'parent' => 'objects', 
			'caption' => '%name% - %price_view%', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'alias' => ['label' => 'Псевдоним', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'name' => ['label' => 'Название', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'description' => ['label' => 'Описание', 'visible' => '0', 'type' => 'text', 'required' => '0', 'show' => '1', ], 
			'price' => ['label' => 'Стоимость доставки', 'visible' => '1', 'format' => 'currency', 'mask' => '@money', 'default' => "0.0000", 'type' => 'decimal', 'required' => '0', 'show' => '1', ], 
			'delivery_requirement' => ['label' => 'Требуется доставка', 'comment' => 'Для этого способа доставки требуется физическая доставка', 'type' => 'boolean', 'visible' => '0', 'default' => 1, 'required' => '0', 'show' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	

	// Создать объект по указанному псевдониму, если такой псевдоним существует.
	public static function byAlias($value) 
	{
		return self::findOne(['alias' => $value]);
	}







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['description'], 'string'],
            [['price'], 'number'],
            [['alias', 'name'], 'string', 'max' => 50],
            [['delivery_requirement'], 'string', 'max' => 1],
            [['alias'], 'unique'],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Order::className(), ['delivery_type_id' => 'id']);
    }



    public static function captionTemplate(){

        return "%name% - %price_view%";
    }





}