<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_okrugs".
 *
 * @property integer $id
 * @property string $alias
 * @property string $name
 * @property string $abr
 * @property string $title
 * @property string $meta_description
 * @property string $h1
 * @property string $seo_text
 *
 * @property Address[] $addresses
 * @property Nalog[] $nalogs
 * @property Region[] $regions
 */
class Okrug extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_okrugs'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Административные округа Москвы', 
			'rodp' => 'округа', 
			'vinp' => 'округ', 
			'imp' => 'округ', 
			'model' => 'Okrug', 
			'caption' => '%abr%', 
			'parent' => 'objects', 
			'comment' => 'Справочник административных округов Москвы, может быть изменен с помощью администратора сайта.', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['show' => '0', 'label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'visible' => '0', ], 
			'alias' => ['label' => 'Код округа', 'comment' => 'Название административного округа', 'filter' => 'like', 'readonly' => '1', 'type' => 'string', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'name' => ['label' => 'Название округа', 'comment' => 'Название административного округа', 'filter' => 'like', 'readonly' => '1', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'abr' => ['label' => 'Аббревиатура', 'comment' => 'Сокращенное название административного округа', 'readonly' => '1', 'type' => 'string', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'title' => ['label' => 'Поле Title', 'show' => '1', 'visible' => '0', 'type' => 'string', 'required' => '0', ], 
			'meta_description' => ['label' => 'Поле Meta Description', 'show' => '1', 'visible' => '0', 'type' => 'string', 'required' => '0', ], 
			'h1' => ['label' => 'h1', 'visible' => '0', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
			'seo_text' => ['label' => 'SEO Текст', 'show' => '1', 'visible' => '0', 'comment' => 'Для разбивки на колонки использовать &ltp&gt', 'type' => 'text', 'required' => '0', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	

	// Создать объект по указанному псевдониму, если такой псевдоним существует.
	public static function byAlias($value) 
	{
		return self::findOne(['alias' => $value]);
	}







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'alias', 'abr'], 'required'],
            [['id'], 'integer'],
            [['seo_text'], 'string'],
            [['alias', 'name'], 'string', 'max' => 45],
            [['abr'], 'string', 'max' => 5],
            [['title', 'meta_description', 'h1'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['alias'], 'unique'],
			[['abr'], 'unique'],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses() {
        return $this->hasMany(Address::className(), ['okrug_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNalogs() {
        return $this->hasMany(Nalog::className(), ['okrug_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegions() {
        return $this->hasMany(Region::className(), ['okrug_id' => 'id']);
    }



    public static function captionTemplate(){

        return "%abr%";
    }





}