<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;

use backend\models\Rip;
use backend\models\User;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * RipSearch represents the model behind the search form about `\backend\models\Rip`.
 */
class RipSearch extends Model {
	
	
	public $id;	
	public $created_at;	
	public $form;	
	public $user_id;	
	public $inn;	
	public $kpp;	
	public $ogrnip;	
	public $fname;	
	public $mname;	
	public $lname;	
	public $pasport_number;	
	public $address;	
	public $phone;	
	public $bank_bik;	
	public $bank_name;	
	public $bank_cor;	
	public $account;	
	
	
	    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['created_at', 'form', 'inn', 'kpp', 'ogrnip', 'fname', 'mname', 'lname', 'pasport_number', 'address', 'phone', 'bank_bik', 'bank_name', 'bank_cor', 'account'], 'safe'],
            ['user_id', 'string', 'length' => [3]],
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rip::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'id'=>SORT_DESC				
			]
     ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
		
		
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['id' => $this -> id > ""?(int)$this -> id:""]);
						
					
				
				
						
				
				
					
				
		/* фильтры по daterangepicker */
		if ($this -> created_at) {
			list($dateFrom, $dateTo) = explode('-', $this -> created_at);
			$dateFrom = self::datetToSql($dateFrom);
			$dateTo = self::datetToSql($dateTo, 1);
			
			$query->andFilterWhere(['between', 'created_at', $dateFrom, $dateTo]);
		};
				
				
						
				
				
					
				
				
						
			
		/* фильтры по like для ссылки*/
		if ($this -> user_id) {			
			$concat = 'CONCAT('.User::getCaptionTemplateAsStringList().')';

			$list = User::find() -> andWhere(['like', $concat , $this->user_id])->asArray()->all();
			$list = ArrayHelper::getColumn($list, 'id');			
			if (count ($list) > 0) {
				$list = implode(',',$list);			
				$query->andWhere("user_id in ($list)");			
			} else { // Условие не выполнено
				$query->andWhere('false');
				return $dataProvider;
			}
		}
				
				
					
				
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'inn', $this->inn]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'kpp', $this->kpp]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'ogrnip', $this->ogrnip]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'fname', $this->fname]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'mname', $this->mname]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'lname', $this->lname]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'pasport_number', $this->pasport_number]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'address', $this->address]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'phone', $this->phone]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'bank_bik', $this->bank_bik]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'bank_name', $this->bank_name]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'bank_cor', $this->bank_cor]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'account', $this->account]);
				
			

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
