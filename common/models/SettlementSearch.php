<?php
/* Ручной файл */


namespace common\models;

use backend\models\Payment;
use backend\models\DocumentType;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PaymentSearch represents the model behind the search form about `\backend\models\Payment`.
 */
class SettlementSearch extends Model {
	
	const SHOW_INVOICES = 1;	
	const SHOW_PAYMENTS = 2;
	
	const COND_EQUAL = 0;	
	const COND_LESS = 1;
	const COND_MORE = 2;
	
	const COND_SYMBOLS = [self::COND_EQUAL => '=', self::COND_LESS => '<', self::COND_MORE => '>'];
	
	// атрибуты фильтрации
	public $payerId = null;	 // список ИД реквизитов плательщика
	public $recipientId = null;	  // список ИД реквизитов получателя денег
	public $dateFrom;	
	public $dateTo;	
	public $orderNumber;
	public $summa = 0;
	public $summaCondition = self::COND_MORE;
	public $documentTypes = [self::SHOW_INVOICES, self::SHOW_PAYMENTS];
	
	public $hiddenFilter; // Фильтр, который будет применяться не зависимо от установленного фильтра
	
	public function getIsEmpty() {	
		return !(boolean)count(Yii::$app->request->get());
	}	
	
	
	public function rules()
    {
		
		return [
			[['documentTypes'],  'each', 'rule' => ['integer']],
			[['summaCondition', 'summa'], 'number'],
			[['orderNumber', 'dateFrom', 'dateTo'], 'safe'],			
			[['payerId', 'recipientId'],  'each', 'rule' => ['integer']],
			
		];
		
	}
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params = false)
    {				

	
		if ($params) $this -> load($params);
		if ($this -> hiddenFilter) {
			foreach($this -> hiddenFilter as $name => $value) {
				$this[$name] = $value;
			}
		}
	
	
		$doc = Document::tableName(); // счета
		$pay = Payment::tableName(); // платежи
		
		if (count($this -> documentTypes) == 0) $this -> documentTypes = [self::SHOW_INVOICES, self::SHOW_PAYMENTS];		
		
		
		$queryDoc = false; $queryPay = false;
		if (in_array(self::SHOW_INVOICES, $this -> documentTypes)) { // показываем  счета
		// ЗАПРОС НА СЧЕТА
			$queryDoc = (new \yii\db\Query()) 
			-> select([
				'id', 
				'number', 
				'created_at as date', 
				'("счет") as operation', 
				'order_id',  
				'ROUND(summa) as summa', 
				'state', 
				'(null) as invoice_id', 
				'owner_id as recipient_id', 
				'recipient_id as payer_id',
				'(1) as is_invoice' 
			])
						
			-> andWhere (['doc_type_id' => DocumentType::byAlias('invoice') -> id])	
			;
			
			
//			if ($orderNumber) $queryDoc -> andWhere(['like', 
			if($this -> payerId && count($this -> payerId)) {
				$queryDoc -> andWhere(['recipient_id' => $this -> payerId]);
			} else {
				$queryDoc -> andWhere (['not', ['recipient_id' => null]]);
			};
			
			if($this -> recipientId && count($this -> recipientId)) {
				$queryDoc -> andWhere(['owner_id' => $this -> recipientId]);
			} else {
				$queryDoc -> andWhere (['not', ['owner_id' => null]]);
			}
			
			if($this -> dateFrom) {
				$queryDoc -> andFilterWhere (['>', $doc.'.created_at', EntityHelper::dateToSql($this -> dateFrom)]);
			};
			
			if($this -> dateTo) {
				$queryDoc -> andFilterWhere (['<', $doc.'.created_at', EntityHelper::dateToSql($this -> dateTo, 1)]);
			}
			
			if ($this -> summa > 0) {
				$queryDoc -> andFilterWhere ([self::COND_SYMBOLS[$this -> summaCondition], $doc.'.summa', $this -> summa]);
			}
						
			
			$queryDoc ->from($doc)
			;
			
			
//			echo $queryDoc -> prepare(Yii::$app->db->queryBuilder)->createCommand()->sql.'<br>'; 
			
			

			
//			var_dump($this -> payerId); echo "<br>";
			
		};
		if (in_array(self::SHOW_PAYMENTS, $this -> documentTypes)) { // показываем платежи
		// ЗАПРОС НА ПЛАТЕЖИ
			$queryPay = (new \yii\db\Query()) 
			-> select([
				'id', 
				'(null) as number', 
				'date', 
				'concat(\'Оплата \', lower(`ptype`)) AS `operation`', 
				'(select order_id from '.$doc.' d where d.id=invoice_id)  as order_id',  
				'value as summa', 
				'(NULL) as state', 
				'invoice_id', 
				'recipient_id', 
				'payer_id',
				'(0) as is_invoice' 
			])
			-> from($pay)	
			-> andWhere(['confirmed' => 1])
			;
			
						
			
			if($this -> payerId && count($this -> payerId)) {
				$queryPay -> andWhere(['payer_id' => $this -> payerId]);
			} else {
				$queryPay -> andWhere (['not', ['payer_id' => null]]);
			};
			
			if($this -> recipientId && count($this -> recipientId)) {
				$queryPay -> andWhere(['recipient_id' => $this -> recipientId]);
			} else {
				$queryPay -> andWhere (['not', ['recipient_id' => null]]);	
			}
			
			if($this -> dateFrom) {
				$queryPay -> andFilterWhere (['>', $pay.'.date', EntityHelper::dateToSql($this -> dateFrom)]);
			};
			
			if($this -> dateTo) {
				$queryPay -> andFilterWhere (['<', $pay.'.date', EntityHelper::dateToSql($this -> dateTo)]);
			}
			
			if ($this -> summa > 0) {
				$queryDoc -> andFilterWhere ([self::COND_SYMBOLS[$this -> summaCondition], $doc.'.summa', $this -> summa]);
			}
		};
		
		
		if($queryDoc && $queryPay) {
			
			$queryDoc->union($queryPay, true);						
			$query = (new \yii\db\Query()) -> from(['u' => $queryDoc]);
			
		} else {
			$query = $queryDoc ? $queryDoc : $queryPay;		
		}
		
		$query -> orderBy(['date' => SORT_DESC]);
//		if ($this -> payerId) $query -> where(['payer_id' => $this -> payerId]);
		
		
		
/*
		else {
		
			//$query = Document::find()
			$query = (new \yii\db\Query())		
			-> select([$doc.'.id as invoice_id', $doc.'.created_at as invoice_date', $doc.'.number as invoice_number', $doc.'.summa as invoice_summa', $pay.'.date as payment_date', $pay.'.value as payment_summa'])
			->from($doc)
			-> join('LEFT OUTER JOIN', Payment::tableName(),
					Document::tableName().'.id = '.Payment::tableName().'.invoice_id')

			-> andFilterWhere(['doc_type_id' => DocumentType::byAlias('invoice') -> id])
			;
	//		echo $query -> prepare(Yii::$app->db->queryBuilder)->createCommand()->sql; 

			//exit;
		
		*/
	/*
		select d.id, number, created_at, summa, p.value
		from adr_documents d
		LEFT OUTER JOIN adr_payments p ON p.invoice_id = d.id
		where doc_type_id = 1167
	*/

//			$documentTypes = [self::SHOW_INVOICES, self::SHOW_PAYMENTS];
//		};
		
        // add conditions that should always apply here

		
//		echo $queryPay -> prepare(Yii::$app->db->queryBuilder)->createCommand()->sql; 
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
//				'date'=>SORT_DESC				
			
			],
			],
			'pagination' => [
				'pageSize' => 9,
			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
	
		
			

//        echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	

}
