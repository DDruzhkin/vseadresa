<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "adr_rip".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $form
 * @property integer $user_id
 * @property string $inn
 * @property string $kpp
 * @property string $ogrnip
 * @property string $fname
 * @property string $mname
 * @property string $lname
 * @property string $pasport_number
 * @property string $pasport_date
 * @property string $pasport_organ
 * @property string $sv_seriya
 * @property string $sv_nomer
 * @property string $sv_date
 * @property string $address
 * @property string $phone
 * @property string $bank_bik
 * @property string $bank_name
 * @property string $bank_cor
 * @property string $account
 * @property string $account_owner
 * @property integer $contract_id
 * @property string $eq_login
 * @property string $eq_password
 * @property integer $prior_id
 * @property integer $enabled
 * @property integer $tax_system_vat
 *
 * @property CObject $prior
 * @property Document $contract
 * @property User $user
 */
class Rip extends \common\models\EntityExt
{

    /* Константы модели */
    const FORM_PERSON = 'Физлицо';
    const FORM_COMPANY = 'Юрлицо';
    const FORM_IP = 'ИП';


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adr_rip';
    }


    public static function parentClassName()
    {
        return '\common\models\CObject';
    }


    /* Списки для перечисляемых типов атрибутов */
    public static function attrLists()
    {
        return [
            'form' => ['Физлицо' => 'Физлицо', 'Юрлицо' => 'Юрлицо', 'ИП' => 'ИП',],

        ];
    }

    /* Метаданные класса */
    public static function meta()
    {
        return [
            'title' => 'Реквизиты ИП',
            'model' => 'Rip',
            'imp' => 'Реквизиты',
            'rodp' => 'Реквизитов',
            'vinp' => 'Реквизиты',
            'parent' => 'objects',
            'caption' => '%form% %lname%',

        ];
    }

    /* Метаданные атрибутов*/
    public static function attrMeta()
    {
        $parentAttrMeta = [];
        if (self::parentClassName()) {
            $parentAttrMeta = self::parentClassName()::attrMeta();
        };

        return ArrayHelper::merge(
            $parentAttrMeta,
            [
                'id' => ['show' => '1', 'filter' => 'value', 'label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'visible' => '1',],
                'created_at' => ['label' => 'Дата добавления', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'show' => '1', 'visible' => '1', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'readonly' => '1', 'required' => '0',],
                'form' => ['visible' => '1', 'label' => 'Правовой статус', 'filter' => 'list', 'default' => 'ИП', 'type' => 'enum', 'required' => '1', 'show' => '1',],
                'user_id' => ['label' => 'Пользователь', 'comment' => 'Пользователь, с которым связаны реквизиты этого индивидуального предпринимателя', 'default' => 'currentUser()', 'filter' => 'like', 'link' => '1', 'model' => 'User', 'type' => 'ref', 'required' => '1', 'show' => '1', 'visible' => '1',],
                'inn' => ['label' => 'ИНН', 'visible' => '1', 'filter' => 'like', 'mask' => '999999999999', 'type' => 'string', 'required' => '1', 'show' => '1',],
                'kpp' => ['label' => 'КПП', 'visible' => '0', 'filter' => 'like', 'mask' => '999999999', 'show' => '0', 'type' => 'string', 'required' => '0',],
                'ogrnip' => ['label' => 'ОГРНИП', 'visible' => '0', 'filter' => 'like', 'mask' => '999999999999999', 'type' => 'string', 'required' => '1', 'show' => '1',],
                'fname' => ['show' => '1', 'visible' => '1', 'label' => 'Имя', 'filter' => 'like', 'type' => 'string', 'required' => '1',],
                'mname' => ['show' => '1', 'visible' => '1', 'label' => 'Отчество', 'filter' => 'like', 'type' => 'string', 'required' => '0',],
                'lname' => ['show' => '1', 'visible' => '1', 'label' => 'Фамилия', 'filter' => 'like', 'type' => 'string', 'required' => '1',],
                'pasport_number' => ['show' => '1', 'visible' => '0', 'label' => 'Номер/серия паспорта', 'filter' => 'like', 'mask' => '@pasport', 'type' => 'string', 'required' => '0',],
                'pasport_date' => ['show' => '1', 'visible' => '0', 'label' => 'Дата выдачи паспорта', 'type' => 'date', 'format' => 'd.m.Y', 'required' => '0',],
                'pasport_organ' => ['show' => '1', 'visible' => '0', 'label' => 'Орган, выдавший паспорт', 'type' => 'string', 'required' => '0',],
                'sv_seriya' => ['visible' => '0', 'label' => 'Серия свидетельства', 'type' => 'string', 'required' => '0', 'show' => '1',],
                'sv_nomer' => ['visible' => '0', 'label' => 'Номер свидетельства', 'type' => 'string', 'required' => '0', 'show' => '1',],
                'sv_date' => ['visible' => '0', 'label' => 'Дата свидетельства', 'type' => 'date', 'required' => '0', 'default' => "0000-00-00 00:00:00", 'show' => '1',],
                'address' => ['show' => '1', 'visible' => '0', 'label' => 'Адрес регистрации', 'filter' => 'like', 'type' => 'string', 'required' => '0',],
                'phone' => ['show' => '1', 'visible' => '0', 'label' => 'Телефон', 'filter' => 'like', 'type' => 'string', 'required' => '0',],
                'bank_bik' => ['show' => '1', 'visible' => '0', 'label' => 'БИК банка', 'filter' => 'like', 'mask' => '999999999', 'type' => 'string', 'required' => '0',],
                'bank_name' => ['show' => '1', 'visible' => '0', 'label' => 'Название банка', 'filter' => 'like', 'type' => 'string', 'required' => '0',],
                'bank_cor' => ['show' => '1', 'visible' => '0', 'label' => 'Кор.счет банка', 'filter' => 'like', 'mask' => '99999999999999999999', 'type' => 'string', 'required' => '0',],
                'account' => ['show' => '1', 'visible' => '0', 'label' => 'Номер счета', 'filter' => 'like', 'mask' => '@account', 'type' => 'string', 'required' => '0',],
                'account_owner' => ['show' => '1', 'visible' => '0', 'label' => 'Владелец счета', 'type' => 'string', 'required' => '0',],
                'contract_id' => ['label' => 'Договор', 'comment' => 'Используемый договор', 'visible' => '0', 'model' => 'Document', 'type' => 'ref', 'required' => '0', 'show' => '1', 'link' => '0',],
                'eq_login' => ['show' => '1', 'visible' => '0', 'label' => 'Эквайринг логин', 'comment' => 'Логин для подключения к эквайрингу', 'type' => 'string', 'required' => '0',],
                'eq_password' => ['show' => '1', 'visible' => '0', 'label' => 'Пароль', 'comment' => 'Эквайринг пароль для подключения к эквайрингу', 'type' => 'string', 'required' => '0',],
                'prior_id' => ['show' => '0', 'label' => 'Предыдущая редакция реквизитов', 'model' => 'CObject', 'type' => 'ref', 'required' => '0', 'visible' => '0', 'link' => '0',],
                'enabled' => ['show' => '0', 'label' => 'Enabled', 'default' => '1', 'type' => 'tinyint', 'required' => '0', 'visible' => '0',],
                'tax_system_vat' => ['visible' => '1', 'label' => 'Используетcя НДС', 'comment' => 'Используется система налогообложения, в которой используется НДС', 'type' => 'boolean', 'default' => 0, 'required' => '0', 'show' => '1',],
                'sign' => ['visible' => '1', 'label' => 'Подпись', 'comment' => 'Файл подписи в формате png с прозрачным фоном', 'type' => 'string', 'default' => "", 'required' => '0', 'show' => '1',],
                'stamp' => ['visible' => '1', 'label' => 'Печать', 'comment' => 'Файл печати в формате png с прозрачным фоном', 'type' => 'string', 'default' => "", 'required' => '0', 'show' => '1',],


            ]
        );
    }

    /* Поведения*/
    public function behaviors()
    {

        $result = [];
        $ns = "common\models\\";
        $class = $ns . $this->meta()['model'] . 'Behavior';

        if (class_exists($class)) {
            $result['ext'] = [
                'class' => $class,
            ];
        };

        return array_merge($result,
            [

                [
                    'class' => TimestampBehavior::className(),
                    'createdAtAttribute' => 'create_time',

                    'value' => new Expression('NOW()'),

                ],
            ]);


    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['created_at', 'pasport_date', 'sv_date'], 'safe'],
            [['form', 'sign', 'stamp'], 'string'],
            [['sign', 'stamp'],'string', 'skipOnEmpty' => true],
            [['user_id', 'inn', 'ogrnip', 'fname', 'lname'], 'required'],
            [['user_id', 'contract_id', 'prior_id'], 'integer'],
            [['inn', 'kpp'], 'string', 'max' => 12],
            [['ogrnip', 'phone'], 'string', 'max' => 15],
            [['fname', 'mname', 'lname', 'pasport_number', 'sv_seriya', 'sv_nomer', 'account_owner', 'eq_login', 'eq_password'], 'string', 'max' => 50],
            [['pasport_organ', 'address', 'bank_name'], 'string', 'max' => 100],
            [['bank_bik'], 'string', 'max' => 9],
            [['bank_cor'], 'string', 'max' => 20],
            [['account'], 'string', 'max' => 26],
            [['enabled', 'tax_system_vat'], 'string', 'max' => 1],
            [['prior_id'], 'exist', 'skipOnError' => true, 'targetClass' => CObject::className(), 'targetAttribute' => ['prior_id' => 'id']],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::className(), 'targetAttribute' => ['contract_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrior()
    {
        return $this->hasOne(CObject::className(), ['id' => 'prior_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Document::className(), ['id' => 'contract_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public static function captionTemplate()
    {

        return "%form% %lname%";
    }


    /* Возвращает число ссылок на модель */
    function hasLinks()
    {

        return
            ($this->parent() ? $this->parent()->hasLinks() : 0);
    }


}