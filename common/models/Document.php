<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_documents".
 *
 * @property integer $id
 * @property string $created_at
 * @property integer $doc_type_id
 * @property string $number
 * @property string $updated_at
 * @property integer $order_id
 * @property integer $porder_id
 * @property integer $address_id
 * @property double $summa
 * @property string $filename
 * @property integer $owner_id
 * @property integer $recipient_id
 * @property string $info
 * @property integer $state
 * @property string $pay_id
 *
 * @property CObject $recipient
 * @property Address $address
 * @property Order $order
 * @property Porder $porder
 * @property CObject $owner
 * @property DocumentType $docType
 * @property Payment[] $payments
 * @property Payment[] $payments0
 * @property Rcompany[] $rcompanies
 * @property Rip[] $rips
 * @property Rperson[] $rpeople
 * @property Service[] $services
 */
class Document extends \common\models\EntityExt 
{
	
	/* Константы модели */
	const CONTRACT_STATE_DEACTIVE = 0;
	const CONTRACT_STATE_ACTIVE = 1;
	const CONTRACT_STATE_SIGNING = 2;
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_documents'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Список документов', 
			'imp' => 'Документ', 
			'rodp' => 'Документа', 
			'vinp' => 'Документ', 
			'model' => 'Document', 
			'caption' => '%doc_type_id_view% №%number% от \"%created_at_view%\"', 
			'parent' => 'objects', 
			'sortattr' => 'created_at', 
			'sortdir' => 'desc', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['show' => '1', 'visible' => '0', 'filter' => 'value', 'label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', ], 
			'created_at' => ['label' => 'Создан', 'comment' => 'Дата создания документа', 'visible' => '1', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'readonly' => '1', 'required' => '0', 'show' => '1', ], 
			'doc_type_id' => ['label' => 'Тип документа', 'visible' => '1', 'filter' => 'list', 'model' => 'DocumentType', 'type' => 'ref', 'required' => '1', 'show' => '1', 'link' => '0', ], 
			'number' => ['label' => 'Номер', 'comment' => 'Номер документа', 'filter' => 'value', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'updated_at' => ['label' => 'Изменен', 'content' => 'Дата последнего изменения документа', 'show' => '0', 'type' => 'date', 'format' => 'd.m.Y', 'readonly' => '1', 'required' => '0', 'visible' => '0', ], 
			'order_id' => ['label' => 'Заказ', 'show' => '0', 'comment' => 'Заказ, к которому относится документ', 'model' => 'Order', 'type' => 'ref', 'required' => '0', 'visible' => '0', 'link' => '0', ], 
			'porder_id' => ['label' => 'Портальный заказ', 'show' => '0', 'comment' => 'Портальный заказ, если счет выставлен на оплату портальной услуги', 'model' => 'Porder', 'type' => 'ref', 'required' => '0', 'visible' => '0', 'link' => '0', ], 
			'address_id' => ['label' => 'Адрес', 'show' => '0', 'comment' => 'Адрес, связанный с документов, напрмиер, для счета это адрес, аренда которого входит в оплату счета', 'model' => 'Address', 'type' => 'ref', 'required' => '0', 'visible' => '0', 'link' => '0', ], 
			'summa' => ['label' => 'Сумма', 'vidible' => '1', 'format' => '[\'decimal\', 2]', 'mask' => '@decimal', 'type' => 'float', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'filename' => ['label' => 'Файл', 'comment' => 'Имя файла, в котором хранится документ', 'show' => '0', 'type' => 'text', 'required' => '0', 'visible' => '0', ], 
			'owner_id' => ['label' => 'Владелец(создатель) документа', 'comment' => 'Реквизиты стороны владельца(создателя) документа. Указывается, если реквизиты владельца зарегистрированы на портале', 'link' => '1', 'readonly' => '1', 'model' => 'CObject', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'recipient_id' => ['label' => 'Получатель документа', 'comment' => 'Реквизиты второй стороны - получателя документа. Указывается, если реквизиты получателя зарегистрированы на портале', 'link' => '1', 'readonly' => '1', 'model' => 'CObject', 'filter' => 'like', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'info' => ['label' => 'Комментарии', 'comment' => 'Дополнительная информация по документу', 'show' => '1', 'type' => 'text', 'required' => '0', 'visible' => '1', ], 
			'state' => ['label' => 'Состояние', 'default' => '0', 'type' => 'tinyint', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'pay_id' => ['show' => '0', 'label' => 'Pay ID', 'default' => "0", 'type' => 'string', 'required' => '1', 'visible' => '0', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
		
	return array_merge($result,
	[
		
		[
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'create_time',
			'updatedAtAttribute' => 'update_time',
			'value' => new Expression('NOW()'),			
			
		],
	]);
	
		
}
	







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'doc_type_id'], 'required'],
            [['id', 'doc_type_id', 'order_id', 'porder_id', 'address_id', 'owner_id', 'recipient_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['summa'], 'number'],
            [['filename', 'info'], 'string'],
            [['number'], 'string', 'max' => 45],
            [['state'], 'string', 'max' => 4],
            [['pay_id'], 'string', 'max' => 50],
            [['id'], 'unique'],
            [['recipient_id'], 'exist', 'skipOnError' => true, 'targetClass' => CObject::className(), 'targetAttribute' => ['recipient_id' => 'id']],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['porder_id'], 'exist', 'skipOnError' => true, 'targetClass' => Porder::className(), 'targetAttribute' => ['porder_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => CObject::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['doc_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentType::className(), 'targetAttribute' => ['doc_type_id' => 'id']],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient() {
        return $this->hasOne(CObject::className(), ['id' => 'recipient_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress() {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPorder() {
        return $this->hasOne(Porder::className(), ['id' => 'porder_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner() {
        return $this->hasOne(CObject::className(), ['id' => 'owner_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocType() {
        return $this->hasOne(DocumentType::className(), ['id' => 'doc_type_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments() {
        return $this->hasMany(Payment::className(), ['invoice_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments0() {
        return $this->hasMany(Payment::className(), ['payment_order_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRcompanies() {
        return $this->hasMany(Rcompany::className(), ['contract_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRips() {
        return $this->hasMany(Rip::className(), ['contract_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRpeople() {
        return $this->hasMany(Rperson::className(), ['contract_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices() {
        return $this->hasMany(Service::className(), ['invoice_id' => 'id']);
    }



    public static function captionTemplate(){

        return "%doc_type_id_view% №%number% от \"%created_at_view%\"";
    }



/* Возвращает число ссылок на модель */
			function hasLinks() {
				
				return 
					($this -> parent()?$this -> parent() -> hasLinks():0)  + 
					$this -> getPayments() -> count() + 
					$this -> getPayments0() -> count() + 
					$this -> getRcompanies() -> count() + 
					$this -> getRips() -> count() + 
					$this -> getRpeople() -> count() + 
					$this -> getServices() -> count();
			}
			





}