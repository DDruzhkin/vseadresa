<?php
namespace common\models;

use yii;
use yii\base\Behavior;
use yii\helpers\Url;
use common\models\Page;
use common\models\EntityExt;

class PageBehavior extends AddressListBehavior
{

	public function getDeletable() {
		$model = $this -> owner;
		return ($model -> getPages() -> count() == 0);
		
	}

	
	
	public static function getRubricsDescription() 
	{
		return 
		'
		<code>public function getRubrics() </code><br/>
		Возвращает список объектов класса Page, видимых, активных рубрик, являющихся дочернеми текущему объекту (странице).		
		';
	}
	
	public function getRubrics() 
	{
		return $this -> getChildren($type = Page::TYPE_RUBRIC);
	}
	
	
	public static function getPostDescription() 
	{
		return 
		'
		<code>public function getRubrics() </code><br/>
		Возвращает список объектов класса Page, видимых, активных постов (статей), являющихся дочернеми текущему объекту (странице).		
		';
	}
	
	public function getPosts() 
	{
		return $this -> getChildren($type = Page::TYPE_POST);
	}
	
	
	public static function getChildrenDescription() 
	{
		return 
		'
		<code>public function getChildren($type) </code><br/>
		Возвращает список объектов класса Page, видимых, активных, с указанным типом, являющихся дочернеми текущему объекту (странице).		
		';
	}
	
	public function getChildren($type) 
	{
		$page = $this -> owner;
		return Page::find() -> where(['parent_id' => $page -> id, 'enabled' => 1, 'visible' => 1, 'type' => $type]) -> all();			
	}
	
	
	public static function getParentsDescription() 
	{
		return 
		'
		<code>public function getParents() </code><br/>
		Вернуть список родителей.		
		';
	}
	
	public function getParents() 
	{
		$page = $this -> owner;

		$result = [];
		$parent = $page;		
		do{
			$parent = $parent -> parent;
			
			if ($parent) {
				if ($parent -> id == $page -> id) break; //защита от зациклинности
				$result[] = $parent;			
			}
		} while ($parent);		
		return $result;		
	}
	
	
	public function getUrl() {
		$page = $this -> owner;		
		return Url::to(['/'.Yii::$app->controller->id.'/'.$page -> slug]);
	}
	


}
?>