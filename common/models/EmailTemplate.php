<?php

namespace common\models;

use backend\controllers\FilesController;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;


class EmailTemplate extends Model
{
    public $name;
	public $category;
    public $content;
	public $title;
	public $view;
	public $role;	
    public $template;
    public $type;
    public $oldAttributes;
	protected $values; // Сюда собираются все значения полей из $filteredAttributes
	protected $filteredAttributes = ['category']; //

    const FILE_EXTENSION = '.php';

    public function rules()
    {
        return [
            [['name', 'content', 'template', 'view'], 'required'],
			[['content', 'template'], 'string'],
            [['title', 'name', 'category', 'role'], 'string', 'max' => 255],
            ['name', 'validateName'],
        ];
    }

    public function validateName($attribute, $params, $validator)
    {
        if ($this->oldAttributes) {
            if ($this->name == $this->oldAttributes['name']) {
                return true;
            }
        }

        if (file_exists($this->getFullPath())) {
            $this->addError('name', Yii::t('app', 'nameMustBeUnique'));
        }
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
			'title' => Yii::t('app', 'Title'),
			'category' => Yii::t('app', 'Category'),
            'content' => Yii::t('app', 'Content'),
            'template' => Yii::t('app', 'Template'),
			'role' => Yii::t('app', 'Role'),
			
        ];
    }

	// По имени поля возвращает список имеющихся значений в списке всех шаблонов (значения собираются при вызове getFileList)
	public function getValueList($fieldName) {
		$result = ArrayHelper::getValue($this -> values, $fieldName);		
		asort($result);		
		return $result;
	}
	

    public function getFileList($dir = 'default',  $collect = true) // или user
    {
        $result = [];
        $pathMailUser = Yii::getAlias('@storage/templates/mail/'.$dir);
        if (is_dir($pathMailUser)) {
            $files = FilesController::getFileList($pathMailUser);
            foreach ($files as $key => $file) {
				
                if (strrpos($file, self::FILE_EXTENSION)) {
					$name = substr($file, 0, strrpos($file, self::FILE_EXTENSION));
					$model = self::findOne($name);
					$attrs = $model -> attributes;
					if ($collect) {
					// попутно собираем все значения по атрибутам
						foreach($attrs as $name => $value) {	
						
							if (!in_array($name, $this -> filteredAttributes)) continue;
							$list = ArrayHelper::getValue($this -> values, $name);
							if ($list) {
								if (!in_array($value, $list)) $list[$value] = $value;
							} else $list = [$value => $value];
							$this -> values[$name] = $list;
						}
					}
//					var_dump($f -> attributes); exit;
                    $result[] = $model -> attributes;
                }
            }
        }

        return $result;
    }

	/*
    public static function getDefaultFileList()
    {
        $result = [];
        $pathMailUser = Yii::getAlias('@storage/templates/mail/default');
        if (is_dir($pathMailUser)) {
            $files = FilesController::getFileList($pathMailUser);
            foreach ($files as $key => $file) {
                if (strrpos($file, self::FILE_EXTENSION)) {
                    $result[] = substr($file, 0, strrpos($file, self::FILE_EXTENSION));
                }
            }
        }

        return $result;
    }
	*/

    public function getViewFileList()
    {
        $result = [];
        $viewPathMail = Yii::getAlias('@storage/templates/mail/view');
        if (is_dir($viewPathMail)) {
            $files = FilesController::getFileList($viewPathMail);
            foreach ($files as $key => $file) {
                $result[]['name'] = $file;
            }
        }

        return $result;
    }

    public function save()
    {
        $pathMailUser = Yii::getAlias('@storage/templates/mail/user');

        if (!is_dir($pathMailUser)) {
            mkdir($pathMailUser);
        }

        $path = $this->getFullPath();

        if ($this->oldAttributes) {
            if ($this->name !== $this->oldAttributes['name']) {
                $oldPath = trim($pathMailUser . '/' . $this->oldAttributes['name'] . self::FILE_EXTENSION);
                if (file_exists($oldPath)) {
                    rename($oldPath, $path);
                }
                unset($this->oldAttributes);
            }
        }

        $content = "<?php
            return[
					'role' => '" . $this->role . "',
					'title' => '" . $this->title . "',
					'category' => '" . $this->category . "',
                   'view' => '" . str_replace(self::FILE_EXTENSION, '', $this->template) . "',
                   'content' => '" . $this->content . "'
           ];
        ";


        $html = fopen($path, 'w');
        fwrite($html, $content);
        fclose($html);

        return true;
    }

    public static function findOne($name)
    {
        $pathTemplate = Yii::getAlias('@storage/templates/mail/user/' . $name . self::FILE_EXTENSION);

        if (!file_exists($pathTemplate)) {
            $pathTemplate = Yii::getAlias('@storage/templates/mail/default/' . $name . self::FILE_EXTENSION);
            if (!file_exists($pathTemplate)) {
                throw new NotFoundHttpException("Файл не существует");
            }
        }

        $model = new self();
		$model->name = $name;
		
        $fileData = self::getFileData($pathTemplate);

		$model->load($fileData, '');		
        return $model;
    }

    public static function getFileData($path)
    {
        $file = require($path);
		$category = ArrayHelper::getValue($file, 'category'); if (!$category) $category = '-';
        $content = ArrayHelper::getValue($file, 'content');
        $view = ArrayHelper::getValue($file, 'view') . self::FILE_EXTENSION;
        $title = ArrayHelper::getValue($file, 'title');				
		$role = ArrayHelper::getValue($file, 'role');				

        return [			
			'category' => $category	,
            'content' => $content,
            'view' => $view,
			'title' => $title,
			'role' => $role,
        ];
    }

    public function getFullPath()
    {
        return Yii::getAlias('@storage/templates/mail/user/' . $this->name . self::FILE_EXTENSION);
    }


    public function delete()
    {
        if (file_exists($this->getFullPath())) {
            unlink($this->getFullPath());
        }
    }

}
