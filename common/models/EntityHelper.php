<?php

namespace common\models;


use DateInterval;
use DatePeriod;
use DateTime;
use Exception;
use stdClass;
use yii;
use backend\models\EmailTemplate;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\web\NotFoundHttpException;
use yii\web\Request;

//require_once Yii::getAlias('@vendor/phpexcel/PHPExcel/IOFactory.php');


class EntityHelper
{


//Латинизация кириллицы
    public static function ru2Lat($value)
    {
        $rus = array_flip(['ё', 'ж', 'ц', 'ч', 'ш', 'щ', 'ю', 'я', 'Ё', 'Ж', 'Ц', 'Ч', 'Ш', 'Щ', 'Ю', 'Я', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ъ', 'Ы', 'Ь', 'Э', 'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ъ', 'ы', 'ь', 'э']);
        $lat = ['yo', 'zh', 'tc', 'ch', 'sh', 'sh', 'yu', 'ya', 'YO', 'ZH', 'TC', 'CH', 'SH', 'SH', 'YU', 'YA', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', '_', 'I', '_', 'E', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', '_', 'i', '_', 'e'];

        $result = '';
        $oldCh = '';
        for ($i = 0; $i < strlen($value); $i++) {

            $ch = mb_substr($value, $i, 1);
            if (array_key_exists($ch, $rus)) {
                $ch = $lat[$rus[$ch]];
            } else $ch = '_';
            if (!($oldCh == $ch && $ch = '_')) $result = $result . $ch;
            $oldCh = $ch;
        }

        //$string = str_replace($rus,$lat,$string);
        //$string = Inflector::transliterate($string); // Требует PHP расширение intl
        return trim($result, '_');
    }


    //Вернуть начало текста длиной $lenght (обрезка по словам)
    public static function cutText($value, $lenght = 100)
    {
        $value = strip_tags($value);
        if (mb_strlen($value) > $lenght) {
            $words = explode(' ', $value);
            $result = '';
            foreach ($words as $word) {
                if (trim($word) == '') continue;
                if (mb_strlen($result) + mb_strlen($word) + 4 > $lenght) break;
                $result = $result . ' ' . $word;
            }
            $result = $result . '...';
        } else {
            $result = $value;
        }
        return $result;

    }


    // Сокращает текст до длины $maxLen, вставляя в середину многоточие (если текст был сокращен)
    public static function mCutText($value, $maxLen = 100)
    {
        $result = $value;
        $length = strlen($value);
        if ($length < 9) $length = 9;
        if ($length > $maxLen) {
            $l1 = (int)(($maxLen - 5) / 2);
            $l2 = $maxLen - $l1 - 5;
            $result = substr($value, 0, $l1) . ' ... ' . substr($value, -$l2);
        }
        return $result;

    }


    // Возвращает список переменных, используемых в шаблоне c %
    public static function getTemplateVars($template)
    {
        $result = false;
        $list = [];
        if (preg_match_all('(%(\w+)%)', $template, $list)) {

            if (isset($list[0])) {
                $list = $list[0];
                foreach ($list as $item) {
                    $result[] = trim(trim($item, '%'));
                }
            }
        };
        return $result;
    }

    /**
     * Возвращает набор общих параметров для использования, в шаблонах
     */
    public static function getCommonOptions()
    {
        return [

            'host' => (Yii:: $app->id != 'app-console') ? Yii::$app->request->getHostInfo() : '',
        ];
    }

    /**
     * Function for sending emails.
     * @param string $subject - subject of email
     * @param array|object|string $params - if $params is string then will be sent email with $params content
     * if $params is object or array, then data will be sent to template which was given by argument $template
     *
     * @param string $to - receiver email address
     * @param string $from - sender email address
     * @param string $template - template file name
     * if argument $template is not empty then file will be searched in /common/storage/mail/user folder, if file doesn't exists then will be searched in /common/storage/mail/default,
     * if file not exists then will be thrown Exception
     * @return bool
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public static function sendEmail($subject, $params, $to, $template = null)
    {
        $mailer = Yii::$app->mailer;

        if (method_exists($mailer->getTransport(), 'getExtensionHandlers')) {
            $from = $mailer->getTransport()->getUsername();
        } else {
            throw  new Exception('произошла ошибка отправки письма');
        }

        $composeView = null;
        $layoutContent = [];


        if ($params && (is_object($params) || is_array($params))) {

            $mailer->viewPath = Yii::getAlias('@storage/templates/mail');
            $fileData = EmailTemplate::findOne($template);


            $content = self::replaceContent($fileData['content'], $params);

            $composeView = 'view/' . $fileData->view;

            $layoutContent = [
                'content' => $content
            ];
        }


        $subject = self::replaceContent($subject, $params);
        $sendMail = $mailer
            ->compose($composeView, $layoutContent)
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject);

        if (is_string($params)) {
            $sendMail->setHtmlBody($params);
        }
        return $sendMail->send();
    }


    public static function replaceContent($content, $params)
    {
        if ($content) {
            if (preg_match_all("/{(.*?)}/", $content, $m)) {
                foreach ($m[1] as $i => $varname) {
                    $replaceName = ($objectViewName = ArrayHelper::getValue($params, $varname . '_view'))
                        ? $objectViewName
                        : ArrayHelper::getValue($params, $varname);
                    if (!is_array($replaceName)) {
                        $content = str_replace($m[0][$i], sprintf('%s', $replaceName), $content);
                    }
                }
            }
        }

        return $content;
    }


    public static function monthList($lang = 'ru')
    {
        $result = null;
        switch ($lang) {
            case 'en':
                $result = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            case 'ru':
                $result = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
        }

        return $result;
    }


    //change English month name to Russia
    public function changeLangMonth($month)
    {
        $en_months = self::monthList('en');
        $ru_months = self::monthList('ru');

        return ArrayHelper::getValue($ru_months, array_search($month, $en_months));
    }

    //change English day name to Russia
    public function changeLangDay($month)
    {
        $ru_weekdays = array('Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье');
        $en_weekdays = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');

        return ArrayHelper::getValue($ru_weekdays, array_search($month, $en_weekdays));
    }


    // Возвращает таблицу дат для следующих $month месяцев (для выбора интервала фотографа)
    public static function getNextMonthsTable($month = 3)
    {
        $array = [];

        // доступная неделя текущая до пятницы и следующая, с пятницы до воскресенья включительно
        $today = strtotime(EntityExt::dbNow());// + 86400 * (+3); //Дата начала недли для выбора


        $wd = date('N', $today);
        $wd = 1 - $wd;
        if ($wd <= -4) $wd = $wd + 7;
        // $firstDate - понедельник текущей недели, либо следующей с пятницы до воскресенья
        $firstDate = strtotime($wd . ' day', $today);
        $lastDate = strtotime('+' . $month . ' month', $firstDate);


        if (date('D', $lastDate) !== 'Fri') {
            $lastDate = strtotime('next friday', $lastDate);
        }


        $endDate = new DateTime(date('Y-m-d', strtotime('+1 day', $lastDate)));

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod(new DateTime(date('Y-m-d', $firstDate)), $interval, new DateTime($endDate->format('Y-m-d')));

        foreach ($period as $value) {

            if ($value->format('D') == 'Sat' || $value->format('D') == 'Sun') {
                continue;
            }

            $array[] = $value->format('Y-m-d');
        }

        return $array;
    }

    public static function getDropdownDate()
    {
        $data = [];

        foreach (array_chunk(self::getNextMonthsTable(), 5) as $key => $value) {
            $firstDate = strtotime(ArrayHelper::getValue($value, '0'));
            $secondDate = strtotime(ArrayHelper::getValue($value, '4'));
            $dropKey = $firstDate . '-' . $secondDate;

            $dropValue = date('d', $firstDate) . ' ' . EntityHelper::changeLangMonth(date('F', $firstDate)) . ' - ' .
                date('d', $secondDate) . ' ' . EntityHelper::changeLangMonth(date('F', $secondDate));

            $data[$key]['key'] = $dropValue;
            $data[$key]['startEndDate'] = date('Y-m-d', $firstDate) . '_' . date('Y-m-d', $secondDate);
        }

        return $data;
    }



    // создает платежи по массивам данных, пришедшим со страницы подтверждения б/н платежей
    // dates - даты платежек
    // $numbers - номера платежек
    // $summs - смуммы платежек
    public static function initPayments($dates, $numbers, $summs)
    {
        $paymentOrders = [];

        foreach ($dates as $id => $date) {
            if (!$date) continue;
            $date = strtotime($date);
            if (!array_key_exists($id, $paymentOrders)) {
                $item = ['date' => $date];
            } else {
                $item = $paymentOrders[$id];
                $item['date'] = $date;
            };
            $paymentOrders[$id] = $item;
        };


        foreach ($numbers as $id => $number) {
            if (!$number) continue;
            if (!array_key_exists($id, $paymentOrders)) continue;
            $item = $paymentOrders[$id];
            $item['number'] = $number;
            $paymentOrders[$id] = $item;
        };

        foreach ($summs as $id => $summa) {
            if (!$summa) continue;
            if (!array_key_exists($id, $paymentOrders)) continue;
            $item = $paymentOrders[$id];
            $item['summa'] = $summa;
            $paymentOrders[$id] = $item;
        };

        //var_dump($paymentOrders);
        $documentType = DocumentType::byAlias('paymentorder'); // Платежное поручение
        foreach ($paymentOrders as $invoice_id => $data) {
            $invoice = Invoice::findOne([$invoice_id]);
            $amount = $data['summa'];
            // Создаем платежку
            $document = new Document();
            $document->doc_type_id = $documentType->id;
            $document->created_at = $data['date'];
            $document->number = $data['number'];
            $document->summa = $amount;
            $document->owner_id = $invoice->recipient_id;
            $document->recipient_id = $invoice->owner_id;
            $document->order_id = $invoice->order_id;

            $document->save();

            // Создаем оплату
            $payment = new Payment();
            $payment->ptype = Payment::PTYPE_BANK;
            $payment->value = $amount;
            $payment->invoice_id = $invoice->id;
            $payment->payment_order_id = $document->id;
            $payment->confirmed = true;
            $payment->save();        // здесь изменяется статуса заказа (реализовано в integrate)

            // Изменяем статус счета  // ??? надо ли это здесь (или в integrate)
            if ($payment->value == $invoice->summa) {
                $invoice->state = Invoice::STATE_PAID;
                $invoice->save();
            };


        }

    }


    public static function getUserTypeData($model)
    {
        $result = new stdClass;
        if ($model) {
            switch ($model::tableName()) {
                case Rperson::tableName():
                    $result->form = $model->form;
                    $result->fullname = $model->fname . ' ' . $model->mname . ' ' . $model->lname;

                    $fname = $model->fname ? mb_substr(trim($model->fname), 0, 1) . '.' : '';
                    $mname = $model->mname ? mb_substr(trim($model->mname), 0, 1) . '.' : '';
                    $result->short_name = $model->lname . ' ' . $fname . $mname;
                    break;
                case Rcompany::tableName():
                    $result->dogovor_face = $model->dogovor_face;
                    $result->form = $model->form;
                    $result->fullname = $model->full_name;
                    $name_parts = explode(' ', $model->dogovor_face);
                    if (count($name_parts) > 0) {
                        $name_parts[0] = $name_parts[0] . ' ';
                        for ($i = 1; $i < count($name_parts); $i++) {
                            $name_parts[$i] = mb_substr($name_parts[$i], 0, 1) . '.';
                        }
                        $shortname = implode('', $name_parts);
                    }
                    $result->short_name = $shortname ?: '';
                    break;
                case Rip::tableName():
                    $result->form = $model->form;
                    $fname = '';
                    $mname = '';
                    if (trim($model->fname)) {
                        $fname = mb_substr(trim($model->fname), 0, 1) . '.';
                    };

                    if (trim($model->mname)) {
                        $mname = mb_substr(trim($model->mname), 0, 1) . '.';
                    };
                    $result->short_name = $model->lname . ' ' . $fname . $mname;
                    $result->fullname = 'ИП ' . $model->lname . ' ' . $fname . $mname;
                    break;
            }
        }

        return $result;
    }

    public static function createPivot($by = 'day', $group = 'address_id', $userId = false)
    {

        //$by = 'day';
        //$by = 'month';
        //$group = 'customer_r_id';
        //$group = 'address_id';

        $sql = "			
		SELECT " .
            (($by == 'day') ? "DATE_FORMAT(created_at, '%d.%m.%Y') as date, " : "DATE_FORMAT(created_at, '%m	.%Y') as date, ") .

            $group . ", 		
		COUNT(*) as order_count,
		(SUM(paid_at>0)) as paid_order_count,
		(SUM((paid_at>0) * summa)) as paid_order_summ
		FROM adr_orders a
		"
            . ($userId ? "where owner_id=" . $userId . " " : "") .
            "
		GROUP BY " . (($by == 'day') ? "DATE_FORMAT(created_at, '%d.%m.%Y'), " : "DATE_FORMAT(created_at, '%m.%Y'), ") .
            "customer_id 
		ORDER BY created_at DESC
	";

        $connection = Yii::$app->db;
        $data = $connection->createCommand($sql)->queryAll();
        $hor = []; // горизонтальное измерение - date
        $ver = []; // вертикальное измерение - par

        $res = [];
        $valNames = ['order_count' => 'Заказов всего', 'paid_order_count' => 'Оплаченных заказов', 'paid_order_summ' => 'Сумма оплаченных заказов'];


        foreach ($data as $row) {
            $par = '';
            if ($row[$group]) {
                $obj = Entity::byId([$row[$group]]);
                if ($obj) {

                    if ($group == 'customer_r_id') {
                        if (!in_array($obj->baseClassName(), ['Rcompany', 'Rperson', 'Rip'])) continue;
                        $par = $obj->getTitle();

                    } else if ($group == 'address_id') {
                        $par = $obj->address;
                    } else    $par = $obj->getCaption();
                }
            }


            $row['par'] = $par;

            $date = $row['date'];
            $par = $row['par'];
            if (!in_array($date, $hor)) $hor[] = $date;
            if (!in_array($par, $ver)) $ver[] = $par;
            foreach ($valNames as $valName => $title) {
                $res[$valName][$date][$par] = $row[$valName]; // повтором не должно быть, поэтому не суммируем
            }
        }


        //отсортируем дату
        $arr = [];
        foreach ($hor as $date) {
            if ($by == 'month') $date1 = '01.' . $date; else $date1 = $date;
            $arr[$date] = strtotime($date1);
        }

        arsort($arr);

        $hor = array_keys($arr);
        asort($ver);


        $titleTail = 'по ' . ($group == 'address' ? 'адресам' : 'заказчикам') . ', сгруппированые по ' . ($by == 'day' ? 'дням' : 'месяцам');
        self::pivotToExcel($res, $hor, $ver, $valNames, $titleTail);
    }


    public static function testExcel()
    {


        $inputFileName = '/var/www/portal/mgribov/common/storage/templates/documents/stat.xlsx';
        $objPHPexcel = \PHPExcel_IOFactory::load($inputFileName);

        $objWorksheet = $objPHPexcel->getActiveSheet();
        $objWorksheet->getCell('A1')->setValue('John');
        $objWorksheet->getCell('A2')->setValue('Smith');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel5');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="address-stat-' . date('d.m.Y') . '.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        $objWriter->save('php://output');


    }


    //преобразование стиля в массив
    public static function getStyleArray($style)
    {

        return [

            'font' => [
                'name' => $style->getFont()->getName(),
                'bold' => $style->getFont()->getBold(),
                'italic' => $style->getFont()->getItalic(),
                'underline' => $style->getFont()->getUnderline(),
                'strikethrough' => $style->getFont()->getStrikethrough(),
                'size' => $style->getFont()->getSize(),
                'color' => [
                    'argb' => $style->getFont()->getColor()->getArgb(),
                ]
            ],

            'alignment' => [
                'horizontal' => $style->getAlignment()->getHorizontal(),
                'vertical' => $style->getAlignment()->getVertical(),
                'textrotation' => $style->getAlignment()->getTextRotation(),
                'wraptext' => $style->getAlignment()->getWrapText(),
                'shrinktofit' => $style->getAlignment()->getShrinkToFit(),
                'indent' => $style->getAlignment()->getIndent(),

            ],

            'borders' => [

                'top' => [
                    'style' => $style->getBorders()->getTop()->getBorderStyle(),
                    'color' => [
                        'argb' => $style->getBorders()->getTop()->getColor()->getArgb(),
                    ]
                ],

                'left' => [
                    'style' => $style->getBorders()->getLeft()->getBorderStyle(),
                    'color' => [
                        'argb' => $style->getBorders()->getLeft()->getColor()->getArgb(),
                    ]
                ],
                'right' => [
                    'style' => $style->getBorders()->getRight()->getBorderStyle(),
                    'color' => [
                        'argb' => $style->getBorders()->getRight()->getColor()->getArgb(),
                    ]
                ],

                'bottom' => [
                    'style' => $style->getBorders()->getBottom()->getBorderStyle(),
                    'color' => [
                        'argb' => $style->getBorders()->getBottom()->getColor()->getArgb(),
                    ]
                ],

            ],

            'fill' => [

                'type' => $style->getFill()->getFillType(),

                'rotation' => $style->getFill()->getRotation(),


                'startcolor' => [
                    'argb' => $style->getFill()->getStartColor()->getArgb(),

                ],

                'endcolor' => [
                    'argb' => $style->getFill()->getEndColor()->getArgb(),
                ],

            ],

        ];

    }

    // Передается трехмерный массив с данными, два измерения и список листов
    public static function pivotToExcel($data, $hors, $vers, $sheets, $titleTail = '')
    {


        $inputFileName = Yii::getAlias('@storage/templates/documents/stat.xlsx');
        $objPHPExcel = \PHPExcel_IOFactory::load($inputFileName);


        $sheetIndex = 0;


        foreach ($sheets as $sheetName => $title) {
            $objPHPExcel->setActiveSheetIndex($sheetIndex);
            $sheetIndex++;
            $sheet = $objPHPExcel->getActiveSheet();
            $horStyle = self::getStyleArray($sheet->getCellByColumnAndRow(2, 2)->getStyle());
            $verStyle = self::getStyleArray($sheet->getCellByColumnAndRow(1, 3)->getStyle());
            $valStyle = self::getStyleArray($sheet->getCellByColumnAndRow(2, 3)->getStyle());
            $valStyle2 = self::getStyleArray($sheet->getCellByColumnAndRow(2, 4)->getStyle());


            //var_dump($styleArr -> getFont() -> getSize());
            //var_dump(self::getStyleArray($horStyle));
            //exit;
            $sheet->setTitle($title);
            // Общий заголовок
            $sheet->setCellValueByColumnAndRow(2, 1, $title . ' ' . $titleTail);
            $sheet->getStyleByColumnAndRow(2, 1)->applyFromArray(['size' => '32pt']);
            // Заголовки для измерений
            $index = 2;
            foreach ($hors as $hor) {
                $sheet->setCellValueByColumnAndRow($index, 2, $hor);
                $sheet->getCellByColumnAndRow($index, 2)->getStyle()->applyFromArray($horStyle);
                $index++;
            }
            $index = 3;
            foreach ($vers as $ver) {
                $sheet->setCellValueByColumnAndRow(1, $index, $ver);
                $sheet->getCellByColumnAndRow(1, $index)->getStyle()->applyFromArray($verStyle);
                $index++;
            }


            // Данные
            $horIndex = 1;
            $odd = false;
            foreach ($hors as $hor) {
                $horIndex++;
                $verIndex = 2;
                foreach ($vers as $ver) {
                    $verIndex++;
                    $odd = ($verIndex % 2 == 0);
                    $val = 0;

                    if (isset($data[$sheetName][$hor][$ver])) {
                        $val = $data[$sheetName][$hor][$ver];

                    }
                    $sheet->setCellValueByColumnAndRow($horIndex, $verIndex, $val);

                    $sheet->getCellByColumnAndRow($horIndex, $verIndex)->getStyle()->applyFromArray($odd ? $valStyle2 : $valStyle);

                }
            }

        };

        $objPHPExcel->setActiveSheetIndex(0);


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="address-stat-' . date('d.m.Y') . '.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter->save('php://output');

        exit;


    }


    public static function dateToSql($date, $addDays = 0)
    {
        $date = strtotime(trim($date)) + $addDays * 86400;
        $date = date("Y-m-d", $date);
        return $date;
    }

    // дату в формате SQL, можно добавить сдвиг по дням
    public static function datetToSql($date, $addDays = 0)
    {
        $date = strtotime(trim($date)) + $addDays * 86400;
        $date = date("Y-m-d", $date);
        return $date;
    }

    // Ассоцивтивный массив пердставляет строкой в формате [ключ]="[значение]" через пробелы, как свойства HTML-тегов
    public static function propertiesToString($data)
    {
        $result = '';
        if (is_array($data)) {
            foreach ($data as $name => $value) {
                $result = $result . ' ' . $name . '="' . $value . '"';
            }
        }
        return trim($result);
    }


}