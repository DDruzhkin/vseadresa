<?php

namespace common\models;

use backend\models\Region;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RegionSearch represents the model behind the search form about `\backend\models\Region`.
 */
class RegionSearch extends Model {
	
	
	public $name;	
	public $okrug_id;	
	
	
	    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'safe'],
            [['okrug_id'], 'integer'],
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Region::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'id'=>SORT_DESC				
			]
     ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
		
		
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'name', $this->name]);
				
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['okrug_id' => $this -> okrug_id > ""?(int)$this -> okrug_id:""]);
						
					
				
				
			

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
