<?php

namespace common\models;

use backend\models\Document;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * DocumentSearch represents the model behind the search form about `\backend\models\Document`.
 */
class ContractSearch extends Model {
	
	
	public $id;	
//	public $created_at;	
	public $doc_type_id;	
	public $number;	
	public $state;
	public $recipient_id;
	
	
	    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'doc_type_id', 'state'], 'integer'],
//            [['created_at', 'number'], 'safe'],
			['recipient_id', 'string', 'length' => [3]],
            [['number'], 'safe'],
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		$contract_type_id = DocumentType::byAlias('contract') -> id;
		$agreement_type_id = DocumentType::byAlias('agreement') -> id;
        $query = Document::find() -> andWhere(['doc_type_id' => [$contract_type_id, $agreement_type_id]]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'created_at'=>SORT_DESC				
			]
     ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
		
		
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['id' => $this -> id > ""?(int)$this -> id:""]);
						
					
				
				
						
		if ($this -> state) {
			
			$query->andFilterWhere(['state' => $this -> state - 1]);
		};
					
				
					
				
		/* фильтры по daterangepicker */
/*		
		if ($this -> created_at) {
			list($dateFrom, $dateTo) = explode('-', $this -> created_at);
			$dateFrom = self::datetToSql($dateFrom);
			$dateTo = self::datetToSql($dateTo, 1);
			
			$query->andFilterWhere(['between', 'created_at', $dateFrom, $dateTo]);
		};
				
				*/
						
			/* фильтры по like для ссылки*/
			
		if ($this -> recipient_id) {			
		
			$concat = 'CONCAT('.Rcompany::getCaptionTemplateAsStringList().')';
			$_list = Rcompany::find() -> andWhere(['like', $concat , $this->recipient_id])->asArray()->all();
			$list = ArrayHelper::getColumn($_list, 'id');
		
		
			$concat = 'CONCAT('.Rip::getCaptionTemplateAsStringList().')';
			$_list = Rip::find() -> andWhere(['like', $concat , $this->recipient_id])->asArray()->all();
			$list = array_merge($list, ArrayHelper::getColumn($_list, 'id'));
			
		
			$concat = 'CONCAT('.Rperson::getCaptionTemplateAsStringList().')';
			$_list = Rperson::find() -> andWhere(['like', $concat , $this->recipient_id])->asArray()->all();			
			$list = array_merge($list, ArrayHelper::getColumn($_list, 'id'));
			
			if (count ($list) > 0) {
				$list = implode(',',$list);			
				$query->andWhere("recipient_id in ($list)");			
			} else { // Условие не выполнено
				$query->andWhere('false');
				return $dataProvider;
			}
		}			
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['doc_type_id' => $this -> doc_type_id > ""?(int)$this -> doc_type_id:""]);
						
					
				
				
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['number' => $this->number]);				
					
				
				
			

//        echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
