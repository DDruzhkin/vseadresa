<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;



/**
 * This is the model class for table "adr_app_options".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $val_datetime
 * @property integer $val_int
 * @property double $val_float
 * @property string $val_str
 * @property string $val_text
 * @property string $vtype
 * @property integer $user_id
 *
 * @property User $user
 */
class AppOption extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_app_options'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [
			'vtype' => ['int' => 'int', 'float' => 'float', 'str' => 'str', 'text' => 'text', 'datetime' => 'datetime', ],  

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Настройки', 
			'model' => 'AppOption', 
			'imp' => 'Системная опция', 
			'rodp' => 'Системной опции', 
			'vinp' => 'Системную опцию', 
			'parent' => 'objects', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
		return [
			'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'name' => ['title' => 'Имя', 'visible' => '1', 'show' => '1', 'label' => 'Name', 'type' => 'string', 'required' => '0', ], 
			'alias' => ['label' => 'Псевдоним', 'comment' => 'Внутренне имя опции для привязки ее к участию в бизнес-логике', 'show' => '0', 'readonly' => '1', 'type' => 'string', 'required' => '0', 'visible' => '0', ], 
			'val_datetime' => ['title' => 'Значение', 'visible' => '0', 'show' => '1', 'label' => 'Val Datetime', 'type' => 'datetime', 'required' => '0', ], 
			'val_int' => ['title' => 'Значение', 'visible' => '0', 'show' => '1', 'label' => 'Val Int', 'type' => 'integer', 'required' => '0', ], 
			'val_float' => ['title' => 'Значение', 'visible' => '0', 'show' => '1', 'label' => 'Val Float', 'type' => 'double', 'required' => '0', ], 
			'val_str' => ['title' => 'Значение', 'visible' => '0', 'show' => '1', 'label' => 'Val Str', 'type' => 'string', 'required' => '0', ], 
			'val_text' => ['title' => 'Значение', 'visible' => '0', 'show' => '1', 'label' => 'Val Text', 'type' => 'text', 'required' => '0', ], 
			'vtype' => ['title' => 'Тип', 'visible' => '1', 'show' => '1', 'label' => 'Vtype', 'default' => 'str', 'type' => 'enum', 'required' => '0', ], 
			'user_id' => ['show' => '0', 'label' => 'User ID', 'default' => '0', 'model' => 'User', 'type' => 'ref', 'required' => '0', 'visible' => '0', 'link' => '0', ], 
				
		];
	}
	/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	

	// Создать объект по указанному псевдониму, если такой псевдоним существует.
	public static function byAlias($value) 
	{
		return self::findOne(['alias' => $value]);
	}







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id'], 'required'],
            [['id', 'val_int', 'user_id'], 'integer'],
            [['val_datetime'], 'safe'],
            [['val_float'], 'number'],
            [['val_text', 'vtype'], 'string'],
            [['name', 'alias'], 'string', 'max' => 45],
            [['val_str'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['alias'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }



    public static function captionTemplate(){

        return "%name%";
    }





}