<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_photo_calendar".
 *
 * @property integer $id
 * @property integer $address_id
 * @property integer $user_id
 * @property string $date
 * @property integer $interval
 * @property integer $enabled
 *
 * @property Address $address
 * @property User $user
 */
class PhotoCalendar extends \common\models\EntityExt
{

	/* Константы модели */
	const INTERVAL_MORNING = 1;
	const INTERVAL_AFTERNON = 2;
	const INTERVAL_EVENING = 3;



    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_photo_calendar';
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Календарь фотографа',
			'model' => 'PhotoCalendar',
			'parent' => 'objects',
			'caption' => '%date% - %interval%',

		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ],
			'address_id' => ['label' => 'Адреса', 'condition' => '[owner_id=>user_id]', 'model' => 'Address', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ],
			'user_id' => ['label' => 'Пользователь', 'readonly' => '0', 'model' => 'User', 'type' => 'ref', 'required' => '1', 'show' => '1', 'visible' => '1', 'link' => '0', ],
			'date' => ['label' => 'Дата', 'type' => 'date', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'required' => '1', 'show' => '1', 'visible' => '1', ],
			'interval' => ['label' => 'Интервал', 'type' => 'tinyint', 'required' => '1', 'show' => '1', 'visible' => '1', ],
			'enabled' => ['label' => 'Активен', 'type' => 'boolean', 'default' => 1, 'required' => '1', 'show' => '1', 'visible' => '1', ],


				]
			);
		}
		/* Поведения*/
	public function behaviors(){

	$result = [];
		$ns = "common\models\\";
		$class = $ns.$this -> meta()['model'].'Behavior';

		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};

		return $result;


}








    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['address_id', 'user_id'], 'integer'],
            [['user_id', 'interval'], 'required'],
            [['date'], 'safe'],
            [['interval', 'enabled'], 'string', 'max' => 1],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ]);
    }








    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress() {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }



    public static function captionTemplate(){

        return "%date% - %interval%";
    }





}