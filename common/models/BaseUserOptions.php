<?php

namespace common\models;


use yii;
use yii\base\Model;

class BaseUserOptions extends Model
{


	public $user; // Model User для связки

	public static function attrMeta() {
		return [
			
		];
	}
	

	
	public function attributeLabels() {	
		$list = $this -> attrMeta();
		$result = [];
		foreach ($list as $attrName => $data) {
			$value = $data['label'];
			$result[$attrName] = '['.str_replace('_', '.', $attrName).'] '.$value;
		}
		return $result;	
	}
	
	
   /**
     * @inheritdoc
     */
	 
	 
    public function rules()
    {
		$list = [];
		foreach ($this -> attrMeta() as $name => $mdata) {
				$list[] = $name;
		}
		
        return [
            [$list, 'safe']
        ];
	
    }
	
	
	public static function getMeta($attrName = '', $name = ''){		
		$result = NULL;		
		$attrName = strtolower($attrName);
		
		$AttrMeta = get_called_class()::attrMeta();
		
		if (($attrName > '') && (array_key_exists($attrName, $AttrMeta))) {
			$result = $AttrMeta[$attrName];
		}

		if ($result && $name > '') {
			if (array_key_exists($name, $result)) {
				$result = $result[$name];
			} else $result = false;
		}
		return $result;
    }
		

	public function __get($name) {	
		if (!$this -> user) return NULL;
		$name = str_replace('_', '.', $name);
		return $this -> user -> getOptions($name);
		
	}
		
		/*
		
	public function load ( $data, $formName = null )	{
		parent::load ( $data, $formName);
		foreach ($this -> attrMeta() as $name => $mdata) {
			$this -> __set($name, $this -> __get($name));
		}
		
	}
	
	*/
		
	public function __set($name, $value){		
		if (!$this -> user) return;
		$name = str_replace('_', '.', $name);		
		$this -> user -> setOptions($name, $value);
	}
		
	public function save() {		
		if (!$this -> user) return;		
		return $this -> user -> save();
	}
		
		
		
}	

?>