<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\web\NotFoundHttpException;

class PayManager
{
	
	public static function payPost($cmd, $params) {
		$url = trim(Yii::$app -> params['acquiring']['url'],'/').'/payment/'.$cmd;				
		$client = new Client();
		$response = $client->createRequest()
			->setMethod('POST')
			->setUrl($url)
			->setData($params)
			->send();
		if ($response->isOk) {
			return $response -> data;			
		} else {			
			throw new NotFoundHttpException('Произошла ошибка с кодом: '.$response -> statusCode);
		}
	}

	
    public static function payCheck($params)
    {
        return self::payPost('rest/getOrderStatusExtended.do',
            [
                'language' => 'ru',
                'orderId' => $params['id'],
                'orderNumber' => $params['number'],
                'userName' => 'best-ur-adres-api',
                'password' => 'q#H}G7PKtzbw',
            ]);
    }
	// передать:
	//	сумму заказа в рублях $params['amount'] 
	// 	номер заказа $params['number']
	// 	url для возврата $params['returnUrl']
	public static function payRegOrder($params) {
		
		$userName = ArrayHelper::getValue($params, 'userName');
		$password = ArrayHelper::getValue($params, 'password');
	
		if (!$userName || !$password) { // нет доступа(настроек) к эквайрингу 
			//тогда вызов производится через реквизиты портала по умолчанию 
			return EntityExt::portal() -> defaultProfile -> payRegOrder($params);			
			
		} else {
		
			return self::payPost('rest/register.do',
			[
				'amount' => isset($params['amount'])? (int)(100 * $params['amount']):1,
				'currency' => 643,
				'language' => 'ru',
				'orderNumber'=> $params['number'],
				'userName' => $params['userName'],
				'password' => $params['password'],
				/*
				'userName' => 'best-ur-adres-api',
				'password' => 'best-ur-adres',
				*/
				'returnUrl' => $params['returnUrl']
			]);
		
		}
	}
	
}