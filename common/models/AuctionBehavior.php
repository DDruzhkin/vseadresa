<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\AutionRate;
use common\models\User;
use kartik\icons\Icon;


class AuctionBehavior extends Behavior
{

	public static function getAddressRatesDescription() {
		return '
		Возвращает все ставки для указанного адреса по текущему аукциону. Первой идет последняя сделанная ставка<br/>		
		';
	}
	
	public function getAddressRates($addressId, $limit = 0) {
		$auction = $this -> owner;
		$query = AuctionRate::find() 
			-> andWhere(['auction_id' => $auction -> id, 'address_id' => $addressId])
			-> orderBy(['created_at' => SORT_DESC]);
		if ($limit) $query -> limit($limit);
		return $query -> all();
	}
	



	public static function getAuctionAddressesDescription() {
		return '
		Возвращает список адресов, участвующих в аукционе, а также число и сумму ставок, дату последней ставки<br/>		
		';
	}
	
	public function getAuctionAddresses() {
		$auction = $this -> owner;
		
		$activeStatusId = Status::find() -> andWhere(['alias' => 'active', 'table' => Auction::tableName()]) -> one() -> id;
		$query = $rows = (new \yii\db\Query())
		-> select(['ar.owner_id', 'adr.id', 'count(*) as c', 'sum(ar.amount) as s', 'max(ar.created_at) as ar_date'])
		-> from('adr_addresses adr, adr_auction_rates ar')
		-> where('adr.id = ar.address_id') 
		-> andWhere(['auction_id' => $auction -> id])
		-> groupBy(['adr.id']) 
		-> orderBy(['s' => SORT_DESC])
		;
		
		//echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
		
		$list = $query -> all();
		$result = [];
		$position = 1;
		foreach($list as $data) {
			$data['position'] = $position;
			$result[$data['id']] = $data;
			$position++;
		}
		
		return $result;	
	}

	public function getRateCount() {
		$auction = $this -> owner;		
		$result = $auction -> rate_count;
		if (!$result) $result == 'Ставок нет';
		return $result;
	}
		

	// Создание ставки
	public function addRate() {
		$auction = $this -> owner;
		$result = new AuctionRate();
		$result -> auction_id = $auction -> id;
		$result -> owner_id = User::currentUser();
		$result -> amount = AuctionRate::STEP;
		$result -> integrate();
		return $result;
	}
	
	public function getMenu() {
		$auction = $this -> owner;		
		$result = [];
		if ($auction -> status -> alias == 'active') {
			$result[] =
				[
					'label' => Icon::show('thumbs-o-up', ['style'=>'margin-left:-16px; font-size: 8pt;']).' Сделать ставки',
					'encodeLabels' => true,                                 
					'url' => Url::toRoute(['rate', 'id' => $auction->id]),
				];
		};
		
		$result[] =
				[
					'label' => 'Карточка аукциона',
					                           
					'url' => Url::toRoute(['view', 'id' => $auction->id]),
				];
		
		return $result;
	}
	
		


}