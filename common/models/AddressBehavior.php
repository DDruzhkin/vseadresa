<?php
namespace common\models;

use yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\base\Behavior;

use common\models\Order;
use common\models\EntityExt;
use common\models\Auction;
use frontend\models\PhotoCalendar;

use kartik\icons\Icon;

class AddressBehavior extends Behavior
{

	
	public function getDeletable() {
		return true;
	}
	

	public static function newPeriodDescription() {
		return '
		<code>public function newPeriod()</code><br/>
		Число дней, в течени которых адрес может оставаться с опцией Новый<br/>		
		';
	}
	
	public  function newPeriod() {
		return 14;
	}
	
	
	public static function canNewDescription() {
		return '
		<code>public function canNew()</code><br/>
		Может ли адрес быть с опицей новый<br/>		
		Не может, если опция Новый была установлена более, чем  newPeriod дней назад.<br/>		
		';
	}
	
	
	public function canNew() {
		$address = $this -> owner;
		return !$address -> is_new || ((strtotime(Entity::dbNow()) - strtotime($address -> is_new)) / 86400 <= $address -> newPeriod());
	}
	
	
	public static function unDeleteDescription() {
		return '
		<code>public function unDelete()</code><br/>
		Восстановить удаленный адрес<br/>		
		';
	}
		
	public function unDelete() {		
		$model = $this -> owner;		
		$model -> enabled = 1;
		$model -> save();
		
		return (boolean)$model -> enabled;
			
	}
	
	public function getPhotographerInterval() {
		$address = $this -> owner;
		$userId = $address -> owner_id;
		$ph = PhotoCalendar::findOne(['address_id' => $address -> id, 'user_id' => $userId]);
		
		return $ph?$ph -> interval:null;
		
	}
	
	public function setPhotographerInterval($value) {
		if ($value) { // Если не задан интервал, то не сохраняется
			$address = $this -> owner;
			$userId = $address -> owner_id;
			$ph = PhotoCalendar::findOne(['address_id' => $address -> id, 'user_id' => $userId]);
			if (!$ph) {
				$ph = new PhotoCalendar();
				$ph -> address_id = $address -> id;
				$ph -> user_id = $userId;
				$ph -> enabled = PhotoCalendar::ENABLED_BUSY;
			}
			$ph -> interval = $value;
			$ph -> save();
		};
	}
	
	
	public function getPhotographerDate() {
		$address = $this -> owner;
		$userId = $address -> owner_id;
		$ph = PhotoCalendar::findOne(['address_id' => $address -> id, 'user_id' => $userId]);
		
		return $ph?$ph -> date:null;
		
	}
	
	public function setPhotographerDate($value) {
		
		if ($value) { // Если не задан интервал, то не сохраняется
			$address = $this -> owner;
			$userId = $address -> owner_id;
			$ph = PhotoCalendar::findOne(['address_id' => $address -> id, 'user_id' => $userId]);
			if (!$ph) {
				$ph = new PhotoCalendar();
				$ph -> address_id = $address -> id;
				$ph -> user_id = $userId;
			}
			$ph -> date = $value;
			$ph -> save();
		}
	}
	
	public static function unPublicateDescription() {
		return '
		<code>public function unPublicate()</code><br/>
		Снять адрес с публикации<br/>
		Адрес переводится в статус Готов к публикации только из статуса Опубликован. Иначе никаких действий не производится.
		';
	}
	
	
	public function unPublicate() {
		$address = $this -> owner;
		if ($address -> status -> alias == 'published'){
			$statusId = Address::statusByAlias('ready') -> id; // Готов к публикации
			$address -> status_id = $statusId;
			$address -> save();
		}
	}
	

	public static function publicateDescription() {
		return '
		<code>public function publicate($rebulic = false)</code><br/>
		Опубликовать адрес<br/>
		Адрес переводится в статус Опубликован, только из статуса Готов к публикации. Иначе никаких действий не производится и возвращается false.<br/>
		Если публикация прошла удачно и не требуется оплаты, то возвращается true.<br/>
		Если адрес ранее публиковался и при этом конец подписки устанавливался на дату большую текущей, то дата не меняется, иначе устанавливается дата  конца подписки +1month к текущей
		аргумент $rebulic позволяет провести эту процедуру из статуса published, то есть не зависимо от статуса.
		';
	}
	
	
	public function publicate($rebulic = false) {
	
		$address = $this -> owner;
		
		$result = false;
		if ($address -> status -> alias == 'ready' || ($rebulic && $address -> status -> alias == 'published')){
			$statusId = Address::statusByAlias('published') -> id; //  Статус "опубликован"
			$subscribe_to = strtotime($address -> subscribe_to);
			$now = strtotime(Entity::dbNow());
			if (!$subscribe_to || ($subscribe_to < $now)) { 
				// Проводим проверку заказов для выставления счета
				$address -> $subscribe_to = $now;
				//$result = ($address -> getOrderCount() < 2);
				$result= true;
				if ($result) {
					$this -> createSubscribe();
				};
			};
			//var_dump($address -> subscribe_to);	exit;			
			$address -> status_id = $statusId;						
			$address -> save();			
		}
		
		return $result;
	}
	
	
	
	protected function getInvoiceQuery($all = false) {
		$address = $this -> owner;
		$invoiceTypeId = DocumentType::byAlias('invoice');        
        $documents = $address -> getDocuments()-> where(['doc_type_id' => $invoiceTypeId]);
		
		if (!$all) {
			$documents -> andWhere(['state' => Invoice::STATE_NOPAID]);
		}
		return $documents;
	}
	
	public static function getInvoicesDescription() {
		return
		'
		<code>getInvoices($all = true)</code><br>
		Возвращает список счетов у Адреса.<br>
		Если $all = true, то все счета, иначе только не оплаченные.<br>
		';
		
		
	}
		
	public function getInvoices($all = false) {
		
		return $this -> getInvoiceQuery($all) -> all();
		
	}
	
	public static function getInvoiceCountDescription() {
		return
		'
		<code>getInvoiceCount($all = true)</code><br>
		Возвращает число счетов у Адреса.<br>
		Если $all = true, то все счета, иначе только не оплаченные.<br>
		';
		
		
	}
	
	public function getInvoiceCount($all = false) {
		
		return $this -> getInvoiceQuery($all) -> count();
		
	}
	
	
	public static function balanceDescription() {
		return
		'
		<code>balance($checkStatus = true; $save = false)</code>
		Вычисляет сумму оплат по счетам для этого адреса.<br>
		Вычисляет сумму портальных заказов по этому адресу (пока это только услуги фотографа).<br>
		Вычисляется статус по балансу.<br>
		Если $checkStatus, то выставляет статус в оплачен или частично оплачен.<br>
		Если ни одной оплаты не было, то статус не затрагивается.<br>
		Возвращает сумму, которую немобходимо оплатить по адресу.<br>
		Для изменения статуса этот метод должен вызываться в местах проведения оплаты
		';
	}
	
	public function balance($checkStatus = true, $save = true) {
		$address = $this -> owner;
		// сумма оплаченных счетов
		$query = (new \yii\db\Query()) 
			-> select('SUM(p.value) as value')
			-> from(Payment::tableName().' p, '.Document::tableName().' d')
			-> where('p.invoice_id = d.id')
			-> andWhere(['d.address_id' => $address -> id, 'p.confirmed' => 1]);
					
		$paied = ($query -> one())['value'];
		// сумма заказов
		$query = (new \yii\db\Query()) 
			-> select('SUM(o.summa) as value')
			-> from([Porder::tableName().' o', Address::tableName().' a'])			
			//-> andWhere(['or', 'o.id = a.photo_order_id', 'o.id = a.subscribe_order_id'])
			-> where('o.id = a.photo_order_id')
			-> andWhere(['a.id' => $address -> id]);
					
					
		
		$summa = ($query -> one())['value'];

		//echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
		
				
		if ($checkStatus && !is_null($paied)) { 					
			if ($summa > $paied) { // частичная оплата
				$address -> changeStatus('partpayment');
			} else { // полная оплата
				$address -> changeStatus('confirmwating');				
			}
		};
		
		if ($save) $address -> save();
		
		return $summa - $paied;
		
	}
	
	public static function changeStatusDescription() {
		return
		'
		<code>public function changeStatus($alias, $save = false)</code><br/>
		Изменяет статус адреса по алиасу статуса <br/>			
		Проверяет возможность такой смены<br/>			
		Если статус изменен успешно, то возвращает true<br/>			
		';
	}
	
	public function changeStatus($alias, $save = false) {
		$result = false;
		$address = $this -> owner; // текуший адрес
		$list = $address -> getStatusList("%alias%"); // список возможных вариантов статусов для изменения
		
		$status_id = array_search ($alias, $list); // получим id статуса по алиасу среди списка возможных значений
		
		if ($status_id) {
			$address -> status_id = $status_id;
			if ($save) $address -> save();
		}
		return $result;
	}

/*	
	
	public static function checkOrdersDescription() {
		return '
		<code>public function checkOrders() </code><br>
		Проверяет число заказов, если их меньше 2-х , то возвращает true, иначе конец подписки устанавливается на сегодня, адрес сохраняется и через вызов checkSubscibe выставляются счет - возвращает счет<br/>
		Проверка осушествляется только, если подписка не оформлена. Если подписка оформлена, то возвращается false<br/>
		';
	}
	
	public function checkOrders($save = true) {
		$address = $this -> owner;		
		
		if (is_null($address -> subscribe_to)) {
			$result = ($address -> getOrderCount() < 2);
			
			if (!$result) {
				$address -> subscribe_to = Entity::dbNow();				
				if ($save) $address -> save();
				$result = $address -> checkSubscribe($save);
			}
		} else $result = false;
		
		
		return $result;
	}

*/

/*
	public static function checkSubscribeDescription() {
		return '
		<code>public function checkSubscribe($publicate = false, $save = true) </code><br>
		Проверяет подписку<br/>
		Если подписки пока нет (льготные период), то проверяет заказы, и если требуется оплата, то возвращает выставленныц счет, если оплаты не требуется, то возвращает true<br/>
		Если до конца подписки осталось 3 дня или меньше или она просрочена, то выставляет счета и возвращает выставленный счет, <br/>		
		Если подписка оформлена и оплачена, то возврашает true<br/>
		
		';
	}	
	
	
	public function checkSubscribe($save = true) {
		
		$address = $this -> owner;		
		$subscibed = !is_null($address -> subscribe_to); // Есть ли подписка
		
		$result = true;
		if (!$subscibed)  // Подписки нет, проверим, нужна ли она
		{
			$noSubscribing = ($address -> checkOrders() === false);
		} else $noSubscribing = false;
		
		if (!$noSubscribing) { // подписка нужна
			/// Здесь  $subscibed всегда = true
			
			if ($address -> checkSubscribeInvoice()) { // Счет выставлен
				$result = $address -> subscibe;
			} else { // Нет выставленного счета		
				if (strtotime($address -> subscribe_to) - strtotime(Entity::dbNow()) < 86400 * 3)  // пора выставлять счет - до даты подписки меньше трех дней	
				{
					$result = $address -> createSubscribe(); // Создаем подписку и выставляем счет					
					
				}
			} 
		};// else $result = false;
		
		if (!is_null($address -> subscribe_to) && (strtotime($address -> subscribe_to) <= strtotime(Entity::dbNow()))) // подписка закончилась и период не оплачен, поэтому статус адреса переводим в Готов к публикации, если он Опубликован
					{
						
						if ($address -> status -> alias == 'published'){
							$statusId = Address::statusByAlias('ready') -> id; // Готов к публикации
							$address -> status_id = $statusId;			
							if ($save) $address -> save();
						}
						
					};
		
		
		return $result;
	}
	
*/	


	public static function getSubscribeInvoiceDescription() {	
		return '
		Возвращает счет на текущую услугу подписки
		';
	}

	public function getSubscribeInvoice() {
		$address = $this -> owner;		
		$invoice = null;
		if ($address -> subscibe_id) {
			$porder = $address -> subscibe;
			if ($porder -> invoice_id) {
				$invoice = $porder -> invoice;
			}
		}
		return $invoice;
	}


	public static function checkSubscribeInvoiceDescription() {	
	return '
	<code>public function checkSubscribeInvoice() </code><br>
	Проверяет, есть ли неоплаченный счет на продление подписки, то есть выставлен ли счет на подписку
	';
	}
	
	public function checkSubscribeInvoice() {
		
		$invoice = $this -> getSubscribeInvoice();
		return (!is_null($invoice) && (is_null($invoice) || ($invoice -> state != 0)));
	}
	
	
/*	
	public static function setSubscribeDescription() {
		return '
		<code>public function setSubscribe($invoice) </code><br/>
		Если это оплата подписки, то продлеваем ее на один месяц от текущей даты, если подписка просрочена и от даты конца подписки, если не просрочена(!! не зависимо от суммы счета) <br/>
		Метод вызывается при оплате счета за подписку
		';
	}
	
	public function setSubscribe($invoice) {
		
		$address = $this -> owner;											
		
		$now = strtotime(Entity::dbNow());
					
		if (strtotime($address -> subscribe_to) < $now) {
			$start = $now;// стартуем с сейчас
		} else {
			$start = strtotime($address -> subscribe_to);// стартуем с конца подписки
		};		
		$address -> subscribe_to = strtotime(Entity::dbNow().' +1 month');		
		$address -> status_id = Address::statusByAlias('published') -> id;
		$address -> save();
		
	}
*/	
	
	public static function createSubscribeDescription() {
		return '
			<code>public function createSubscribe() </code><br/>
			Продлевает подписку от текушей даты на месяц.
			Если нет счета на подпику, то создает портальный заказ subscibe и выставляет счета по нему.<br/>
			Заказ создается, если либо нет привязанного заказа, либо привязанный заказ содержит оплаченный счет<br/>
			Никаких прроверок условий на состояние адреса не проводится - операция проводится в любом случае в любом статусе<br/>
			Метод возвращает портальный заказ на подписку
		';
		
		
	}
	
	public function createSubscribe() {
		
		$address = $this -> owner;				
		$address -> subscribe_to = date('Y-m-d H:i:s', strtotime(date('Y-m-d').' +1month'));
		
		if ($address -> checkSubscribeInvoice()) { // уже есть не выставленный счет
			$result = $address -> subscribe;
		} else {
			$result = \common\models\ServicePortal::byAlias('subscribe') -> order([], NULL, true);
			$address -> subscribe_id = $result -> id;	
			$result -> customer_r_id = $address -> owner -> default_profile_id; // Подставляются реквизиты по умолчанию
			$result -> save();		
			
			$address -> save();		
			
			if ($address -> getOrderCount() >= 2) {
				$result -> bill(['address_id' => $address -> id]); // Выставляем счета на адрес
			}
		};
		
		return $result;
		
	}
			
	
	public static function getPriceDisplayedDescription()
    {
		return 'отображаемая цена для адреса - цена за минимальный возможный срок';
	}
	// 
	public function getPriceDisplayed()
    {
		$address = $this->owner;

		return $address->price_first_6 > 0
            ? $address->price_first_6
            : $address->price_first_11;
	}

	public static function getOrderCountDescription() {
		return '
		<code>public function getOrderCount() </code><br>
		Число заказов на этот адрес за последний месяц
		';
	}
	// 
	public function getOrderCount() {
		$address = $this -> owner;
		$now = date('Y-m-d', strtotime(Entity::dbNow()));
		$start = date('Y-m-d', strtotime(Entity::dbNow().' -1month'));
		
		$statuses = [Order::statusByAlias('done') -> id]; // Статус, в которых считаются заказы
		$result = Order::find() -> where(['address_id' => $address -> id, 'status_id' => $statuses, ]) -> andWhere(['between', 'status_changed_at', $start, $now]);		
		return $result-> count();
	}	

	
	
	
	public static function getServiceListDescription() {
		return '
		Геттер для свойства serviceList - справочник всех услуг адреса, включая деактивные
		';
	}

	public function getServiceList() {
		$address = $this -> owner;
		$user = $address -> owner;
		$list = $address -> getServicePrices() -> andFilterWhere(['owner_id'=>$user -> id]);
		return $list;
	}


	public static function getServiceIdListDescription() {
		return '
		Геттер для свойства serviceIdList - справочник услуг адреса</br>
		Возвращаются id соответствующих услуг справочника владельца<br/>

		';
	}

	public function getServiceIdList() {
		$address = $this -> owner;
		// Список услуг адреса
		$list = $address  -> serviceList -> andFilterWhere(['enabled' => 1]) -> all();
		
		// Получаем спискок ИД услуг в справочнике владельца
		$result = [];
		foreach($list as $item) {

			$result[] = $item -> ownerServicePrice -> id;// соответствующая услуга из справочника владельца
		}
		//exit;
		return $result;
	}

	
	
	public static function setServiceListDescription() {
		return '
			Инициализация справочника услуг адреса. 
			Устанавливается набор услуг и цены на них.
			
		';
	}
	
	public function setServiceList($serviceIdList, $prices, $prices6, $prices11) {
		
		$address = $this -> owner;
		$address->serviceIdList = $serviceIdList;	// Формируем список услуг у адреса

		foreach($serviceIdList as $id) {
			$ownerService = ServicePrice::findOne($id);
			$service = $ownerService->getAddressServicePrice($address->id);

			if ($service) {
				if (isset($prices[$id])) $service -> price = $prices[$id];
				if (isset($prices6[$id])) $service -> price6 = $prices6[$id];
				if (isset($prices11[$id])) $service -> price11 = $prices11[$id];
				$service->save();
			}
		}
		
	}
	
	

	public static function setServiceIdListDescription() {
		return '
		Сеттер для свойства serviceIdList - справочник услуг адреса</br>
		передается массив id соответствующих услуг справочника владельца<br/>
		Возвращает список новых созданных услуг<br/>
		';
	}

	

	// $value - новый список услуг адреса как список id соответствующихй услуг в справочнике владельца
	public function setServiceIdList($value) {
		
		$address = $this -> owner;
		$newList = $value?$value:[];
		// старый список
		$list = $address -> serviceIdList;		
		

		$oldList = $list?$list:[];
		// разница

		$dels = array_diff($oldList, $newList); // те, которые надо удалить
		$adds = array_diff($newList, $oldList); // те, которые надо добавить

		// мягко удаляем
		foreach($dels as $id) {
//			echo  'deleting: '.$id.' -> '.ServicePrice::findOne($id) -> getAddressServicePrice($address -> id) -> id.'<br>';
			ServicePrice::findOne($id) -> getAddressServicePrice($address -> id) -> delete(false);

		}

		// добавляем
		foreach($adds as $id) {			
			ServicePrice::findOne($id) -> addAddressServicePrice($address -> id, true);
		}		
	}




	public static function addQuickServicePriceDescription() {
		return
			'
		<code>public function addQuickServicePrice($params) {</code></br>
		Добавляет в справочник владельца (текуший пользователь) услугу быстрого оформления заказа - алиас quick_order</br>
		Если услуга была деактивирована, то активирует ее. </br>
		Метод возвращает объект ServicePrice.</br>
		';
	}

	public function addQuickServicePrice($params) {
		$address = $this -> owner;
		$sn = ServiceName::byAlias('quick_order');
		$params['service_name_id'] = $sn -> id;
		if (!$address -> id) $address -> save();
		$params['address_id'] = $address -> id;
		return $address -> addSerivcePrice($params);
	}


	public static function deleteQuickServicePriceDescription() {
		return
			'
		<code>public function deleteQuickServicePrice() {</code></br>
		Удаляет  из справочника владельца (текуший пользователь) услугу быстрого оформления заказа - алиас quick_order</br>
		Если услуга имеет связи, то она только деактивируется.</br>
		';
	}

	public function deleteQuickServicePrice($force = false) {
		$address = $this -> owner;
		$sn = ServiceName::byAlias('quick_order');
		$params['service_name_id'] = $sn -> id;
		$params['address_id'] = $address -> id;

		return $address -> deleteSerivcePrice($params, $force);
	}

	public static function quickServicePriceDescription() {
		return
			'
		<code>public function quickServicePrice() {</code></br>

		';
	}

	public function quickServicePrice() {
		$address = $this -> owner;
		$sn = ServiceName::byAlias('quick_order');
		$params['service_name_id'] = $sn -> id;
		$params['address_id'] = $address -> id;
		$params['owner_id'] = $address -> currentUser();
		return ServicePrice::findOne($params);
	}


	public static function orderDescription() {
		return
			'
		<code>public function order($params, $model = NULL, $save = false)</code></br>
		Редактирует или создает (если не существует) заказ на основе адреса. </br>
		Заказ заполняется переданными данными.</br>
		Можно передать массив ID услуг из прайс-листа (класс ServicePrice) в параметре services. Если этот параметр передан, то состав заказа будет соответствовать этому массиву. </br>
		Если не передал, то изменений в составе не произойдет.</br>
		Метод возвращает объект заказа.</br>
		';
	}



	public function order($params, $model = NULL, $save = false) {
		$address_id = $this -> owner -> id;
		$customer_id = Order::currentUser();
		$condition = ["address_id" => $address_id, "customer_id" => $customer_id];
		if (isset($params["customer_r_id"])) {
			$condition["customer_r_id"] = $params["customer_r_id"];
		}

		if (!$model) { // не дана модель заказа - найдем или создадим
			/*
			$model = Order::findOne(["address_id" => $address_id, "customer_id" => $customer_id]);// должен быть только один заказ в системе
			if (!$model) {
				$model = new Order();// нулевый
				if ($save) $model -> save(); // Если не записывать, то к заказу невозможно будет привязать другие объекты
			}
			*/
			// TODO: чтобы сделать проверку на начатый заказ - необходимы реквизиты!!
			$model = new Order();// нулевый

		}

		$model -> load($condition, '');
		if ($save) $model -> save();
		return $model -> assign(array_merge($params));
	}

	public static function priceRange6Description() {
		return '
		<code>public function priceRange6() </code><br>
		Возвращает вилку цены за 6 месяцев с учетом как общей вилки, так и локальной
		';
	}

	public function priceRange6() {
		$address = $this -> owner;
		$min0 = $address -> portal() -> getOptions('address.price.6min');
		$max0 = $address -> portal() -> getOptions('address.price.6max');

		$min = $address -> price6min;
		$max = $address -> price6max;
		if ($min0 + $max0 > 0) {
			$min = max($min0, $min);
			$max = max($max0, $min, min($max0, $max));
		}
		return [$min, $max];
	}


	public static function price6Description() {
		return '
		<code>public function price6() </code><br>
		Возвращает цену за 6 месяцев с учетом вилки
		';
	}

	public function price6() {
		$address = $this -> owner;
		
		$price = $address -> price6;	
		if ($price == 0) return 0; // Если установлена нулевая цена, то ее и оставляем
		
		$range = $this -> priceRange6();		
		

		if($range[0] + $range[1] > 0) {
			$price = $price < $range[0]?$range[0]:($price > $range[1]?$range[1]:$price);
		}
		return $price;
	}


	public static function priceRange11Description() {
		return '
		<code>public function priceRange11() </code><br>
		Возвращает вилку цены за 11 месяцев с учетом как общей вилки, так и локальной
		';
	}

	public function priceRange11() {

		$address = $this -> owner;
		$min0 = $address -> portal() -> getOptions('address.price.11min');
		$max0 = $address -> portal() -> getOptions('address.price.11max');

		$min = $address -> price11min;
		$max = $address -> price11max;
		if ($min0 + $max0 > 0) {
			$min = max($min0, $min);
			$max = max($max0, $min, min($max0, $max));
		}
		return [$min, $max];
	}


	public static function price11Description() {
		return '
		<code>public function price11() </code><br>
		Возвращает цену за 6 месяцев с учетом вилки
		';
	}

	public function price11() {
		$address = $this -> owner;
		
		$price = $address -> price11;
		if ($price == 0) return 0; // Если установлена нулевая цена, то ее и оставляем
		
		$range = $this -> priceRange11();

		

		if($range[0] + $range[1] > 0) {
			$price = $price < $range[0]?$range[0]:($price > $range[1]?$range[1]:$price);
		}
		return $price;
	}

	public static function getServiceOptionsDescription()

	{
		return '
		<code>public static function getServiceOptions() {</code><br>
		Возвращает список опций, которые соответствуют справочнику адреса<br>
		То есть множество связанных опций с услугами из справочника<br>
		Возвращает список опицй<br>
		';
	}

	public function getServiceOptions() 
	{				
		$address = $this -> owner;
		$result = $address -> getServiceOptionsByList($this -> serviceIdList);
	}
	
	
	
	public static function hasOptionDescription() 

	{
		return '
		<code>public function hasOption($alias)</code><br/>
		Есть ли у Адреса опция с указанным алиасом<br/>
		Опция Новый всегда false, если не выполняется условие canNew()<br/>
		';
	}

	public function hasOption($alias)
    {
		$address = $this -> owner;
        $count =  $address->hasMany(Option::className(), ['id' => 'option_id'])
             ->viaTable('adr_address_option', ['address_id' => 'id']) -> andWhere(['alias' => $alias]) -> count();
		
		if ($alias == 'new') {
			
			if (!$address -> canNew())  {				
				if ($count) $address -> save(); // обновим опции, если условие уже не выполняется (для удаления опции Новый)
				$count = 0;
			};
		}
		return $count;
    }
	
	
	public static function getLikedDescription() 
	{
		return '
		<code>public function getLiked($limit = 3, $demo = true)</code><br/>
		Вернуть наиболее похожие адреса<br/>
		В порядке убывания приоритета:<br/>
		- Налоговая<br/>
		- Метро<br/>
		- Округ<br/>
		- Рейтинг<br/>	
		$limit - число адресов, которое необходимо подобрать (вернуть)<br/>
		Если $demo, то включены только те адреса, которые могут отображаться в каталоге<br/>
		Возвращает список адресов<br/>
		';
	}
	
	public function getLiked($limit = 3, $demo = true)
    {
		$address = $this -> owner;
		
		//$rate = Address::generateByTemplate("(nalog_id=%nalog_id%)*4 + 2*(metro_id=%metro_id%) + (okrug_id=%okrug_id%)", $address -> attributes);
		
		$rate = '1';
		if ($address -> nalog_id) $rate = $rate.Address::generateByTemplate("+(nalog_id=%nalog_id%)*4", $address -> attributes);
		if ($address -> metro_id) $rate = $rate.Address::generateByTemplate("+(metro_id=%metro_id%)*2", $address -> attributes);
		if ($address -> okrug_id) $rate = $rate.Address::generateByTemplate("+(okrug_id=%okrug_id%)", $address -> attributes);
		
		
		$query = Address::find()
			-> select(['*', '('.$rate.') as rate'])			
			-> from(Address::tableName())
			-> where(['not',['id' => $address -> id]]);
			
			
/*			
		if ($demo) {
			$statuses = [Address::statusByAlias('published') -> id]; // статусы, в которых оторажается адрес			
			$query
				-> andWhere(['demo' => 1]) // только адреса с флажком demo
				-> andWhere(['status_id' => $statuses]); // ограничение по статусам
		};			
	
*/

		$query -> where(Address::getToCatalogCondition());
			
		$query	-> orderBy(['rate' => SORT_DESC, 'rating' => SORT_DESC]);
		
		if ($limit > 0) $query -> limit($limit);

		return $query -> all();
    }
	

	public static function getCanPhotograperDescription() 
	{
		return '
		<code>public function getCanPhotograper()</code><br/>
		Можно ли для этого адреса заказать фотографа<br/>
		Условия:<br/>
		- Статус адреса "Новый" или "Ожидание оплаты"
		';
	}
	
	public function getCanPhotographer() 
	{
		$address = $this -> owner;
		
		$statuses = ['new', 'paymentwaiting']; // алиасы статусов адреса, в которых возможно заказать фотографа
		
		return in_array($address -> status -> alias, $statuses);
	}
	
	
	public static function getAdminTabItemsDescription() {
		return
		'
		Возвращает список разделов для редактирования адреса в админке, в формате пунктов меню
		';
	}
	
	public function getAdminTabItems() {
		$address = $this -> owner;		
		
		$result = [];
		
		$result[] = [
					'label' => 'Основные данные',
					'url' => Url::to(['/admin/address/update/','id' => $address -> id, 'tab' => 'base']),
					'tab' => 'base'
		];
		
		$result[] = [
					'label' => 'Опции',
					'url' => Url::to(['/admin/address/update/','id' => $address -> id, 'tab' => 'options']),
					'tab' => 'base'
		];
			
		if (trim($address -> address) > '') { // Местоположение можно изменить только после указания адреса
			$result[] = [
					'label' => 'Местоположение',
					'url' => Url::to(['/admin/address/update/','id' => $address -> id, 'tab' => 'map']),
					'tab' => 'map'
			];
		};
			
		$result[] = [
					'label' => 'SEO',
					'url' => Url::to(['/admin/address/update/','id' => $address -> id, 'tab' => 'seo']),
					'tab' => 'seo'
		];
		
		$result[] = [
					'label' => 'Документы',
					'url' => Url::to(['/admin/address/files/','id' => $address -> id, 'tab' => 'files']),
					'tab' => 'files'
		];
		
		$result[] = [
					'label' => 'Фотографии',
					'url' => Url::to(['/admin/address/update','id' => $address -> id, 'tab' => 'photos']),
					'tab' => 'files'
		];
		
		
		return $result;
	}
	
	
	public static function getAdminMenuItemsDescription() {
		return
		'
		Возвращает пункты меню для адреса в ЛКА, в зависимости от текущего состояния адреса
		';
	}

	public function getAdminMenuItems() {
		$address = $this -> owner;		
		$sections = [];

		
		if ($address -> status -> alias == 'confirmwating') {
			$menuItems[] = [
				'label' => 'Подготовка адреса',
				'url' => Url::to(['/admin/address/preparation', 'id' => $address -> id]),
			];
		}
		
		
		if ($address -> status -> alias == 'preparation') {
			$menuItems[] = [
				'label' => 'Готов к публикации',
				'url' => Url::to(['/admin/address/ready', 'id' => $address -> id]),
			];
		}
		
		$menuItems[] = [
				'label' => $address -> demo?'Запретить показ':'Разрешить отображение',
				'url' => Url::to(['/admin/address/demo', 'id' => $address -> id, 'value' => $value = !$address -> demo]),
		];
		
		

		
		$menuItems[] = [
			'label' => 'Приоритет',
			'items' => [
				[
					'label' => 'Максимальный',
					'url' => Url::to(['/admin/address/priority', 'id' => $address -> id, 'priority' => 9999]),
				],
				
				[
					'label' => 'Минимальный',
					'url' => Url::to(['/admin/address/priority', 'id' => $address -> id, 'priority' => 0]),
				],
			]
		];
		

		
		$menuItems[] = [
			'label' => 'Карточка адреса',
			'url' => Url::to(['/admin/address/view/', 'id' => $address -> id])
		];
		
		if (count($menuItems) > 0) {
			$menuItems[] = '<li class="divider"></li>';
		}
		
		$menuItems[] = [
			'label' => 'Заказы',
			'url' => Url::to(['/admin/order/', 'address_id' => $address -> id])
		];
				
		
		
		$menuItems[] = [
			'label' => 'Статистика по заказам',
			 
				'items' => [					
						['label' => 'Владельцам', 'url' => Url::to(['/admin/stat/orders', 'address_id' => $address -> id])],
						['label' => 'Порталу', 'url' => Url::to(['/admin/stat/portal-orders', 'address_id' => $address -> id])],					
				]
			
			
		];
		
		
		if (count($menuItems) > 0) {
			$menuItems[] = '<li class="divider"></li>';
		};
		
		
		
		$menuItems[] = [
			'label' => 'Изменить',
			'items' => $this -> getAdminTabItems()
		];
		
/*
		$menuItems[] = [
			'label' => 'Удалить',
			'url' => Url::to(['/admin/address/delete/', 'id' => $address -> id])
		];
*/		
		
		return $menuItems;

	
	}
	
	public static function getOwnerMenuItemsDescription() {
		return
		'
		Возвращает пункты меню для адреса в ЛКВ, в зависимости от текущего состояния адреса
		';
	}

	public function getOwnerMenuItems() {
		$address = $this -> owner;		
		$sections = [];

		
		
		if (in_array($address -> status -> alias, ['new','paymentwaiting', 'partpayment', 'confirmwating', 'preparation', 'ready'])) {
			$sections['basic'] = 'Основные параметры';
		}
		
		if (!in_array($address -> status->alias, ['blocked','confirmwating'])) {			
			$sections['services'] = 'Справочник услуг';						
		}
		

		$menuItems =[];
		
		
		
		if ($address -> status -> alias == 'published') {
			$menuItems[] = [
				'label' => Icon::show('eye-slash', ['style'=>'margin-left:-16px; font-size: 8pt;']).' Снять с публикации',
				'encodeLabels' => true, 
				'url' => Url::to(['/owner/address/unpublicate/'.$address -> id]),
			];
		}
		
		if ($address -> status -> alias == 'ready') {
			$menuItems[] = [
				'label' => Icon::show('eye', ['style'=>'margin-left:-16px; font-size: 8pt;']).' Опубликовать',
				'encodeLabels' => true, 
				'url' => Url::to(['/owner/address/publicate/'.$address -> id]),
			];
		}
		
		if (!in_array($address -> status -> alias, ['published', 'blocked','confirmwating'])) {
			$sections['publication'] = 'Параметры для публикации';
		}
		
		if (in_array($address -> status -> alias, ['ready', 'published'])) {
			$sections['photo'] = 'Отображение адреса';			
			$sections['documents'] = 'Документы адреса';
		}
		
		//if (in_array($address -> status -> alias, ['paymentwaiting', 'partpayment'])) {
			
		if ($address -> getInvoiceCount()) {
		
			$menuItems[] = [
			
				'label' => ' Счета',
				'encodeLabels' => true, 
				'url' => Url::toRoute(['document', 'id' => $address -> id]),
			];
		}
		
		foreach($sections as $alias => $title) {
		
			$menuItems[] = [
				'label' => $title,
				'url' => Url::toRoute(['update', 'id' => $address -> id, 'tab' => $alias]),
			];
		};
		
		if ($address -> getCanPhotographer()) {
			$menuItems[] = [
			
				'label' => Icon::show('camera', ['style'=>'margin-left:-16px; font-size: 8pt;']).' Заказ фотографа',
				'encodeLabels' => true, 
				'url' => Url::toRoute(['update', 'id' => $address -> id, 'tab' => 'photographer']),
			];
		}		
		
		if (count($menuItems) > 0) {
			$menuItems[] = '<li class="divider"></li>';
		}
		
		$menuItems[] = [
			'label' => 'Карточка адреса',
			'url' => Url::to(['/address/'.$address -> id])
		];
		
		
		return $menuItems;
	}
}
?>