<?php
/*
*	Предок для справочников адресов
*/
namespace common\models;

use Yii;
use yii\base\Behavior;
use yii\helpers\Url;
use yii\helpers\Inflector;

use InvalidArgumentException;

class AddressListBehavior extends Behavior {
	
	
	public static function getDeletableDescription() {
		return '
		Возможно ли модель удалить. Возможно, если она не связана ни с одним адресом или с другими объектами.
		';
	}
	
	
	public function getDeletable() {
		$model = $this -> owner;
		return ($model -> getAddresses() -> count() == 0);
		
	}
	
	public static function getAdminMenuItemsDescription() {
		return
		'
		Возвращает пункты меню для модели, в Админке, в зависимости от текущего состояния модели.
		';
	}

	public function getAdminMenuItems() {
		$model = $this -> owner;		
		$sections = [];
		$conrollerId = Inflector::camel2id($model -> baseClassName());
		
		$menuItems[] = [
			'label' => 'Изменить',
			'url' => Url::to(['/admin/'.$conrollerId.'/update/', 'id' => $model -> id])
		];
		
		/*
		if ($this -> editable) {
			if (count($menuItems) > 0) {
				$menuItems[] = '<li class="divider"></li>';
			};		

			$menuItems[] = [
				'label' => 'Удалить',
				'url' => Url::to(['/admin/'.$conrollerId.'/delete/', 'id' => $model -> id]),								
			];
		
		}
		*/
		return $menuItems;

	
	}
	
	
}