<?php
namespace common\models;

use yii\base\Behavior;

class ServiceBehavior extends Behavior
{
			
		
	public static function invoiceIsAgreeDescription() {
		return 
		'
		<code>public function invoiceIsAgree($invoice) </code><br/>
		Проверка согласованности услуги со счетом.<br/>
		Плательщик должен соответствовать заказчику. А получатель владелце.<br/>
		';
	}
	
	// проверяем, согласуется ли услуга со счетом
	public function invoiceIsAgree($invoice) {		
		$owner_id = $this -> owner_id; // тот, кому платят (владелец счета), соответствует владелец услуги
		$recipient_id = $this -> recipient_id; // тот, кто платит (получатель счета), соответствует заказчик услуги (заказа)
		
		return ($invoice -> owner_id == $owner_id) && ($invoice -> recipient_id == $recipient_id);
			
	}
	
	public static function getDurationValueDescription() {
		return 
		'
		<code>public function durationValue() </code><br/>
		Продолжительность оказания услуги в месяцах, если это услуга длительная.<br/>		
		';
	}
	
	
	public function getDurationValue() {
		$service = $this -> owner;
		if ($service -> stype == Service::STYPE_ONCE) {
			return 1;
		} else {
			if ($service -> date_start && $service->date_fin) {				
				$value = (int)((new DateTime($service -> date_fin)) -> diff(new DateTime($service -> date_start)) -> format('m'));
			} else {							
				if ($service -> duration == Service::DURATION_11) $value = 11; else $value = 6;				
			}
			
			return $value;
		}
	}
	

}

?>