<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;

use backend\models\DocumentType;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DocumentTypeSearch represents the model behind the search form about `\backend\models\DocumentType`.
 */
class DocumentTypeSearch extends Model {
	
	
	
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DocumentType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'name'=>SORT_ASC				
			]
     ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
		
		
			

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
