<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_metro".
 *
 * @property integer $id
 * @property string $alias
 * @property string $name
 * @property string $title
 * @property string $meta_description
 * @property string $seo_text
 * @property string $h1
 *
 * @property Address[] $addresses
 */
class Metro extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_metro'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Список станций метро', 
			'imp' => 'Станция метро', 
			'rodp' => 'Станции метро', 
			'vinp' => 'Станцию метро', 
			'model' => 'Metro', 
			'parent' => 'objects', 
			'caption' => 'м.%name%', 
			'comment' => 'Справочник станций метро Москвы, может быть изменен с помощью администратора сайта.', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['show' => '0', 'label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'visible' => '0', ], 
			'alias' => ['label' => 'Код', 'filter' => 'value', 'readonly' => '1', 'type' => 'string', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'name' => ['label' => 'Название станции', 'filter' => 'like', 'readonly' => '1', 'type' => 'string', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'title' => ['label' => 'Поле Title', 'show' => '1', 'visible' => '0', 'type' => 'string', 'required' => '0', ], 
			'meta_description' => ['label' => 'Поле Meta Description', 'show' => '1', 'visible' => '0', 'type' => 'string', 'required' => '0', ], 
			'seo_text' => ['label' => 'SEO Текст', 'show' => '1', 'visible' => '0', 'comment' => 'Для разбивки на колонки использовать &ltp&gt', 'type' => 'text', 'required' => '0', ], 
			'h1' => ['label' => 'h1', 'visible' => '0', 'type' => 'string', 'required' => '0', 'show' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	

	// Создать объект по указанному псевдониму, если такой псевдоним существует.
	public static function byAlias($value) 
	{
		return self::findOne(['alias' => $value]);
	}







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'alias', 'name'], 'required'],
            [['id'], 'integer'],
            [['seo_text'], 'string'],
            [['alias', 'name'], 'string', 'max' => 45],
            [['title', 'meta_description', 'h1'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['alias'], 'unique'],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses() {
        return $this->hasMany(Address::className(), ['metro_id' => 'id']);
    }



    public static function captionTemplate(){

        return "м.%name%";
    }





}