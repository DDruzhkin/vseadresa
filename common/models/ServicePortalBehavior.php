<?php
namespace common\models;

use yii\base\Behavior;
use common\models\Entity;
use common\models\User;
use common\models\Porder;

class ServicePortalBehavior extends AddressListBehavior {
	
	public function orderDescription() {
		return 
		'
		<code>public function order($params = [], $model = NULL, $save = false)</code></br>
		Редактирует или создает (если не существует) портальный заказ на основе услуги портала. </br>
		В $params принимаются три параметра customer_r_id или customer_id, price</br>
		Если не указан customer_r_id, то используются реквизиты по умолчанию у пользователя customer_id</br>
		Если не указан customer_id, то используются текущий залогиненный пользователь</br>
		Метод возвращает объект заказа.</br>
		';
	}
	
	public function getDeletable() {
		$model = $this -> owner;
		return ($model -> getPorders() -> count() == 0) && ($model -> getRecipient() -> count() == 0);
		
	}
	
	
	public function order($params = [], $model = NULL, $save = false) {
		
		if (!$model) $model = new Porder();		
		$model -> service_id = $this -> owner -> id;

		if (isset($params['customer_r_id'])) {
			$model -> customer_r_id = $params['customer_r_id'];
		} else {
			if (isset($params['customer_r'])) {
				$model -> customer_r_id = $params['customer_r_id'];
			};
		}
		
		if (isset($params['price'])) $model -> summa = $params['price'];

		$model -> integrate();
		if ($save) $model -> save();
		return $model;

	}
}