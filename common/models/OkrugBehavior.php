<?php

namespace common\models;

use Yii;
use yii\base\Behavior;
use yii\helpers\Url;

use InvalidArgumentException;

class OkrugBehavior extends AddressListBehavior {
	
	
	
	public function getDeletable() {
		$model = $this -> owner;
		
		return parent::getDeletable() && ($model -> getNalogs() -> count() == 0) && ($model -> getRegions() -> count() == 0);
		
	}
}