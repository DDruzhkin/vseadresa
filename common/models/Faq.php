<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_faq".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $question
 * @property string $answer
 * @property string $role
 * @property string $h1
 * @property string $title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $seo_text
 * @property integer $enabled
 */
class Faq extends \common\models\EntityExt 
{
	
	/* Константы модели */
	const ROLE_CUSTOMER = 'Заказчик';
	const ROLE_OWNER = 'Владелец';
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_faq'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [
			'role' => ['Заказчик' => 'Заказчик', 'Владелец' => 'Владелец', ],  

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Частые вопросы и ответы', 
			'model' => 'Faq', 
			'comment' => 'Часто задаваемые вопросы', 
			'parent' => 'objects', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'created_at' => ['label' => 'Дата добавления', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'show' => '1', 'visible' => '1', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'readonly' => '1', 'required' => '1', ], 
			'updated_at' => ['label' => 'Дата изменения', 'show' => '0', 'readonly' => '1', 'type' => 'timestamp', 'required' => '0', 'visible' => '0', ], 
			'question' => ['label' => 'Вопрос', 'type' => 'text', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'answer' => ['label' => 'Ответ', 'visible' => '0', 'type' => 'text', 'required' => '0', 'show' => '1', ], 
			'role' => ['label' => 'Для кого вопрос', 'visible' => '1', 'filter' => 'list', 'format' => 'checkboxlist', 'type' => 'set', 'required' => '0', 'show' => '1', ], 
			'h1' => ['show' => '1', 'visible' => '0', 'label' => 'H1', 'type' => 'string', 'required' => '0', ], 
			'title' => ['show' => '1', 'visible' => '0', 'label' => 'Title', 'type' => 'string', 'required' => '0', ], 
			'meta_description' => ['label' => 'Поле Meta Description', 'show' => '1', 'visible' => '0', 'type' => 'string', 'required' => '0', ], 
			'meta_keywords' => ['label' => 'Поле Meta Keywords', 'show' => '1', 'visible' => '0', 'type' => 'string', 'required' => '0', ], 
			'seo_text' => ['label' => 'SEO Текст', 'visible' => '0', 'show' => '1', 'type' => 'text', 'required' => '0', ], 
			'enabled' => ['label' => 'Активен', 'type' => 'boolean', 'visible' => '0', 'default' => 0, 'required' => '0', 'show' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
		
	return array_merge($result,
	[
		
		[
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'create_time',
			'updatedAtAttribute' => 'update_time',
			'value' => new Expression('NOW()'),			
			
		],
	]);
	
		
}
	







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['created_at', 'updated_at'], 'safe'],
            [['question'], 'required'],
            [['question', 'answer', 'seo_text'], 'string'],
            [['h1', 'title', 'meta_description', 'meta_keywords'], 'string', 'max' => 255],
            [['enabled'], 'string', 'max' => 1],
            [['role_as_array'],  'each', 'rule' => ['string']],
        ]);
    }
	

	
	
	
	



    public static function captionTemplate(){

        return "%title%";
    }





}