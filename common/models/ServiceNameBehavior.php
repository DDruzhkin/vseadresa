<?php

namespace common\models;

use Yii;
use yii\base\Behavior;
use yii\helpers\Url;

use InvalidArgumentException;

class ServiceNameBehavior extends AddressListBehavior {


	public function getDeletable() {
		$model = $this -> owner;
		return ($model -> getServicePrices() -> count() == 0);
		
	}
	


}