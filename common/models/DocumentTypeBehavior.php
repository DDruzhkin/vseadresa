<?php

namespace common\models;

use Yii;
use yii\base\Behavior;
use yii\helpers\Url;

use frontend\models\Document;
use InvalidArgumentException;
use kartik\mpdf\Pdf;

class DocumentTypeBehavior extends Behavior {
	
	
	public static function createDocumentDescription() {
		return '
		Создание документа текущего типа.		
		';
	}
	
	
	public function createDocument() {
		$document_type = $this -> owner;
		$result = new Document();
		$result -> doc_type_id = $document_type -> id;
		return $result;
	}
	
	
}