<?php

namespace common\models;


use Yii;
use common\models\EventManager;
use frontend\models\Address;


class EventManagerExt extends EventManager
{
	
	
	// Метод вызывается после проведения оплаты
	public function afterPayment($params) {
		
		if (!isset($params['payment'])) return false;

		$payment = $params['payment'];
		$invoice = $payment -> invoice;
		
		if ($invoice -> porder) {
			
			$porderAlias = $invoice -> porder -> service -> alias;

			switch ($porderAlias) {
				case 'photography': {					
					//$invoice -> status_id = Address::statusByAlias('confirmwating') -> id;
					//$invoice -> address -> save();
					$invoice -> address -> balance();
					break;
				}
				case 'subscribe': {					
					$invoice -> address -> setSubscribe($payment -> invoice); // устанавливаем подписку
					$invoice -> address -> save();
					break;
				}	
			}

		};

		/*
		if ($invoice -> order) {
			echo "order -> balance<br>";		
			$invoice -> order -> balance();
		}
		
		*/
	}
	
	public function __construct() {
		 $this -> linkEvent('afterPayment', [$this, 'afterPayment']);		 
	}	 
	
	
}