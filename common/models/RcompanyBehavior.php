<?php
namespace common\models;

use yii;
use yii\base\Behavior;
use yii\helpers\Url;
use common\models\EntityExt;

class RcompanyBehavior extends RequisiteBehavior
{


	public function getTitle() {
		$r = $this -> owner;			
		$result = $r -> name?$r -> name:$r -> full_name;
		return $result;
	}

	

	
	public function findEqual($attrs = false) {
		$attrs = [
			'inn', 
			'kpp',
			'ogrn',
			'name',
			'full_name',
			'address',
			'phone',
			'bank_bik',
			'bank_name',
			'bank_cor',
			'account',
			'account_owner',
			'tax_system_vat',
		];
		
		return parent::findEqual($attrs);
		
	}
	
	
	public function getHeaderText($sideName) {
		$r = $this -> owner;
		$params = $r -> attributes;		
		$params['sideName'] = $sideName;
		//
		$template = '
		<strong>{full_name}</strong> именуемое в дальнейшем «{sideName}», в лице {dogovor_face} {dogovor_signature}, действующ(его/ей) на основании {dogovor_osnovanie}
		';
		
		return EntityHelper::replaceContent($template, $params);
	}
	

	public function getFooterText() {
		$r = $this -> owner;
		$params = $r -> attributes;
		$template = '			
<strong>{full_name}</strong><br/>
{address}<br/>
ИНН {inn}, <br/>
ОГРН № {ogrn}<br/>
р/с {account}<br/>
в {bank_name}<br/>
к/с {bank_cor}<br/>
БИК {bank_bik}<br/>
		';
		return EntityHelper::replaceContent($template, $params);
	}	

	

}

?>