<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_reviews".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $status_changed_at
 * @property string $name
 * @property string $email
 * @property string $company
 * @property string $author_position
 * @property integer $status_id
 * @property string $content
 *
 * @property Status $status
 */
class Review extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_reviews'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Отзывы', 
			'rodp' => 'отзыва', 
			'vinp' => 'отзыв', 
			'imp' => 'отзыв', 
			'model' => 'Review', 
			'parent' => 'objects', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['visible' => '1', 'label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', ], 
			'created_at' => ['label' => 'Дата добавления', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'readonly' => '1', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'status_changed_at' => ['label' => 'Дата изменения статуса', 'show' => '0', 'comment' => 'Дата последнего изменения статуса. Выставляется автоматически', 'type' => 'timestamp', 'required' => '0', 'visible' => '0', ], 
			'name' => ['label' => 'Автор', 'filter' => 'like', 'visible' => '1', 'type' => 'string', 'required' => '1', 'show' => '1', ], 
			'email' => ['label' => 'e-mail', 'filter' => 'like', 'visible' => '1', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
			'company' => ['label' => 'Название организации', 'filter' => 'like', 'visible' => '1', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
			'author_position' => ['label' => 'Должность автора отзыва', 'show' => '1', 'visible' => '0', 'type' => 'string', 'required' => '0', ], 
			'status_id' => ['label' => 'Статус', 'visible' => '1', 'filter' => 'list', 'model' => 'Status', 'type' => 'ref', 'required' => '0', 'show' => '1', 'link' => '0', ], 
			'content' => ['label' => 'Текст отзыва', 'show' => '1', 'visible' => '0', 'type' => 'text', 'required' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
		
	return array_merge($result,
	[
		
		[
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'create_time',
			
			'value' => new Expression('NOW()'),			
			
		],
	]);
	
		
}
	

	public static function statusByAlias($alias) {
			return Status::findOne(["alias" => $alias, "table" => self::tableName()]);
	}
			
			






    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'name', 'content'], 'required'],
            [['id', 'status_id'], 'integer'],
            [['created_at', 'status_changed_at'], 'safe'],
            [['content'], 'string'],
            [['name', 'email', 'company', 'author_position'], 'string', 'max' => 50],
            [['id'], 'unique'],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus() {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }



    public static function captionTemplate(){

        return "%name%";
    }





}