<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;

use backend\models\Page;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PageSearch represents the model behind the search form about `\backend\models\Page`.
 */
class PageSearch extends Model {

	
	public $parent_id;	
	public $created_at;	
	public $h1;	
	public $menu;	
	public $slug;	
	public $type;	
	
	
	    /**

     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id'], 'integer'],
            [['created_at', 'h1', 'menu', 'slug', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Page::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'id'=>SORT_DESC				
			]
     ]

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		
		
		
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['parent_id' => $this -> parent_id > ""?(int)$this -> parent_id:""]);
						
					
				
				
						
				
				
					
				
		/* фильтры по daterangepicker */
		if ($this -> created_at) {
			list($dateFrom, $dateTo) = explode('-', $this -> created_at);
			$dateFrom = self::datetToSql($dateFrom);
			$dateTo = self::datetToSql($dateTo, 1);
			
			$query->andFilterWhere(['between', 'created_at', $dateFrom, $dateTo]);
		};
				
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'h1', $this->h1]);
				
						
				
				
			
		/* фильтры по set */
		if ($this->menu) {
			if($this->menu > '') {
				$this->menu = Page::getList('menu')[$this->menu];
				$query->andWhere('FIND_IN_SET(:menu,menu)>0',[':menu' => $this->menu]);
			}			
		}
					
				
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'slug', $this->slug]);
				
						
				
				
					
				
				
			


        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }

	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	

}
