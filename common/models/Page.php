<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_pages".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $h1
 * @property string $label
 * @property string $content
 * @property string $menu
 * @property string $title
 * @property string $meta_description
 * @property string $seo_text
 * @property string $slug
 * @property string $template
 * @property string $type
 * @property string $tag_str
 * @property integer $tag_int
 * @property integer $seo_id
 * @property integer $visible
 * @property integer $enabled
 *
 * @property Seo $seo
 * @property Page $parent
 * @property Page[] $pages
 */
class Page extends \common\models\EntityExt 
{
	
	/* Константы модели */
	const MENU_MAIN = 'main';
	const MENU_CONDITIONS = 'conditions';
	const TYPE_STATIC = 'static';
	const TYPE_POST = 'post';
	const TYPE_RUBRIC = 'rubric';
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_pages'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [
			'menu' => ['main' => 'main', 'conditions' => 'conditions', ],  
			'type' => ['static' => 'static', 'post' => 'post', 'rubric' => 'rubric', ],  

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Страницы', 
			'model' => 'Page', 
			'comment' => 'Страницы сайта', 
			'parent' => 'objects', 
			'caption' => '%h1%', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'parent_id' => ['label' => 'Раздел', 'filter' => 'list', 'show' => '1', 'visible' => '1', 'link' => '1', 'condition' => '[type=>TYPE_RUBRIC]', 'model' => 'Page', 'type' => 'ref', 'required' => '0', ], 
			'created_at' => ['label' => 'Дата добавления', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'show' => '1', 'visible' => '1', 'readonly' => '0', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'required' => '0', ], 
			'updated_at' => ['label' => 'Дата изменения', 'show' => '0', 'readonly' => '1', 'type' => 'timestamp', 'required' => '0', 'visible' => '0', ], 
			'h1' => ['label' => 'Заголовок H1', 'filter' => 'like', 'type' => 'string', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'label' => ['label' => 'Метка', 'comment' => 'Текст, который будет отображаться в виде ссылки на эту страницу', 'visible' => '0', 'content' => 'Текст ссылки на эту страницу, который используется для построения меню', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
			'content' => ['label' => 'Текст страницы', 'comment' => 'В тексте страницы могут быть использованы HTML теги', 'visible' => '0', 'type' => 'text', 'required' => '0', 'show' => '1', ], 
			'menu' => ['label' => 'Меню', 'comment' => 'Меню, в котором отображать страницу', 'format' => 'checkboxlist', 'filter' => 'list', 'default' => "", 'type' => 'set', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'title' => ['label' => 'Поле Title', 'show' => '1', 'visible' => '0', 'default' => "", 'type' => 'string', 'required' => '0', ], 
			'meta_description' => ['label' => 'Поле Meta Description', 'show' => '1', 'visible' => '0', 'default' => "", 'type' => 'string', 'required' => '0', ], 
			'seo_text' => ['label' => 'SEO-текст', 'visible' => '0', 'type' => 'text', 'required' => '0', 'show' => '1', ], 
			'slug' => ['label' => 'Псевдоним', 'comment' => 'Используется для генерации URL. Если оставить пустым, то будет автоматическисгенерировано.', 'filter' => 'like', 'requeried' => '0', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'template' => ['label' => 'Шаблон', 'comment' => 'Шаблон для отображения страницы - имя view', 'visible' => '0', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
			'type' => ['label' => 'Тип страницы', 'show' => '1', 'filter' => 'list', 'default' => 'post', 'type' => 'enum', 'required' => '0', 'visible' => '1', ], 
			'tag_str' => ['label' => 'дополнительное строковое поле, которое может быть использовано в произволных целях', 'show' => '0', 'type' => 'string', 'required' => '0', 'visible' => '0', ], 
			'tag_int' => ['label' => 'дополнительное целочисленное поле, которое может быть использовано в произволных целях', 'show' => '0', 'type' => 'integer', 'required' => '0', 'visible' => '0', ], 
			'seo_id' => ['label' => 'Seo-данные', 'visible' => '0', 'show' => '0', 'model' => 'Seo', 'type' => 'ref', 'required' => '0', 'link' => '0', ], 
			'visible' => ['label' => 'Показывать', 'type' => 'boolean', 'visible' => '0', 'default' => 1, 'required' => '1', 'show' => '1', ], 
			'enabled' => ['label' => 'Активна', 'type' => 'boolean', 'visible' => '0', 'default' => 1, 'required' => '1', 'show' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
		
	return array_merge($result,
	[
		
		[
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'create_time',
			'updatedAtAttribute' => 'update_time',
			'value' => new Expression('NOW()'),			
			
		],
	]);
	
		
}
	

	// Создать объект по указанному псевдониму, если такой псевдоним существует.
	public static function bySlug($value) 
	{
		return self::findOne(['slug' => $value]);
	}







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'h1'], 'required'],
            [['id', 'parent_id', 'tag_int', 'seo_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['content', 'seo_text', 'type'], 'string'],
            [['h1', 'title', 'meta_description'], 'string', 'max' => 255],
            [['label'], 'string', 'max' => 50],
            [['slug', 'template'], 'string', 'max' => 100],
            [['tag_str'], 'string', 'max' => 45],
            [['visible', 'enabled'], 'string', 'max' => 1],
            [['menu_as_array'],  'each', 'rule' => ['string']],
            [['id'], 'unique'],
            [['slug'], 'unique'],
            [['seo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Seo::className(), 'targetAttribute' => ['seo_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeo() {
        return $this->hasOne(Seo::className(), ['id' => 'seo_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent() {
        return $this->hasOne(Page::className(), ['id' => 'parent_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages() {
        return $this->hasMany(Page::className(), ['parent_id' => 'id']);
    }



    public static function captionTemplate(){

        return "%h1%";
    }





}