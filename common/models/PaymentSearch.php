<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;

use backend\models\Payment;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PaymentSearch represents the model behind the search form about `\backend\models\Payment`.
 */
class PaymentSearch extends Model {
	
	
	public $date;	
	public $payer_name;	
	public $invoice_date;	
	public $invoice_number;	
	public $recipient_name;	
	
	
	    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'payer_name', 'invoice_date', 'invoice_number', 'recipient_name'], 'safe'],
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'id'=>SORT_DESC				
			]
     ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
		
		
						
				
				
					
				
		/* фильтры по daterangepicker */
		if ($this -> date) {
			list($dateFrom, $dateTo) = explode('-', $this -> date);
			$dateFrom = self::datetToSql($dateFrom);
			$dateTo = self::datetToSql($dateTo, 1);
			
			$query->andFilterWhere(['between', 'date', $dateFrom, $dateTo]);
		};
				
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'payer_name', $this->payer_name]);
				
						
				
				
					
				
		/* фильтры по daterangepicker */
		if ($this -> invoice_date) {
			list($dateFrom, $dateTo) = explode('-', $this -> invoice_date);
			$dateFrom = self::datetToSql($dateFrom);
			$dateTo = self::datetToSql($dateTo, 1);
			
			$query->andFilterWhere(['between', 'invoice_date', $dateFrom, $dateTo]);
		};
				
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'invoice_number', $this->invoice_number]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'recipient_name', $this->recipient_name]);
				
			

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
