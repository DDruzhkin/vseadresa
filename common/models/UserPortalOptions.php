<?php
/*
*	Модель для работы с опицями пользователя
*/
namespace common\models;


use yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

class UserPortalOptions extends BaseUserOptions
{
	

	public static function attrMeta() {
		return [
			'common_seo_counters' => ['label' => 'Коды счетчиков', 'comment' => 'Код, который вставляется на кажду страницу сайта'],
			'common_list_countperpage' => ['label' => 'Число строк на странице', 'comment' => 'Число страниц на одной странице при разбивке списка в таблице на страницы'],
		
			'top_banner_show' => ['label' => 'показывать баннер', 'comment' => 'Показывать баннер на верху страницы'],
			'top_banner_value' => ['label' => 'Код баннера', 'comment' => 'Код баннера на верху страницы'],
		
			'home_stat_show' => ['label' => 'показывать', 'comment' => 'Показывать на главное странице статистику'],
			'home_stat_address_count_show' => ['label' => 'адресов в системе (показывать)', 'comment' => 'Показывать на главное странице число алресов'],
			'home_stat_address_count_value' => ['label' => 'адресов в системе (значение)', 'comment' => 'Значение числа адресов'],
			'home_stat_owner_count_show' => ['label' => 'владельцев в системе', 'comment' => 'Показывать на главное странице число владельцев'],
			'home_stat_owner_count_value' => ['label' => 'владельцев в системе (значение)', 'comment' => 'Значение числа владельцев'],
			'home_stat_contract_count_show' => ['label' => 'договоров в системе', 'comment' => 'Показывать на главное странице число договоров'],
			'home_stat_contract_count_value' => ['label' => 'договоров в системе (значение)', 'comment' => 'Значение числа договоров'],
			'home_reviews_show' => ['label' => 'отзывы показывать', 'comment' => 'Показывать на главное странице отзывы'],
			'home_reviews_count' => ['label' => 'количество отзывов', 'comment' => 'Максимальное количество отзывов, которое необходимо показывать на главное странице'],
			'home_faq_show' => ['label' => 'показывать ЧаВо', 'comment' => 'Показывать на главное странице список частых вопросов'],
			'home_faq_count' => ['label' => 'количество вопросов', 'comment' => 'Максимальное количество вопросов, которое необходимо показывать на главное странице'],
			'home_news_show' => ['label' => 'показывать новости', 'comment' => 'Показывать на главное странице последние новости'],
			'home_seo_title' => ['label' => 'Title', 'comment' => 'Значение Title для главной страницы'],
			'home_seo_h1' => ['label' => 'H1', 'comment' => 'Значение H1 для главной страницы'],
			'home_seo_description' => ['label' => 'Мета Description', 'comment' => 'Значение мета поля description для главной страницы'],

			'home_seo_text' => ['label' => 'SEO-Текст', 'comment' => 'SEO-текст на гланой страницы. Разделитель столбцов &lt;p&gt'],
					
			'address_price_6min' => ['label' => 'Минимальная цена за 6 месяцев', 'comment' => ''],
			'address_price_6max' => ['label' => 'Максимальная цена за 6 месяцев', 'comment' => ''],
			'address_price_11min' => ['label' => 'Минимальная цена за 11 месяцев', 'comment' => ''],
			'address_price_11max' => ['label' => 'Максимальная цена за 11 месяцев', 'comment' => ''],
			'address_list_seo_title' => ['label' => 'Title', 'comment' => 'Значение Title для списка адресов'],
			'address_list_seo_h1' => ['label' => 'H1', 'comment' => 'Значение H1 для списка адресов'],
			'address_list_seo_description' => ['label' => 'Мета Description', 'comment' => 'Значение мета поля description для списка адресов'],
			'address_list_seo_text' => ['label' => 'SEO-Текст', 'comment' => 'SEO-текст для списка адресов. Разделитель столбцов &lt;p&gt;'],
			'address_pricelist_seo_title' => ['label' => 'Title', 'comment' => 'Значение Title для прайс-листа'],
			'address_pricelist_seo_h1' => ['label' => 'H1', 'comment' => 'Значение H1 для прайс-листа'],
			'address_pricelist_seo_description' => ['label' => 'Мета Description', 'comment' => 'Значение мета поля description для прайс-листа'],
			
			
			
			'notifications_registration_enabled' => ['label' => 'Активно', 'comment' => 'Активно уведомление при регистрации нового пользователя'],
			'notifications_registration_email' => ['label' => 'Email', 'comment' => 'Email для получения уведомления при регистрации нового пользователя'],
			'notifications_newreview_enabled' => ['label' => 'Активно', 'comment' => 'Активно уведомление при добавлении нового отзыва'],
			'notifications_newreview_email' => ['label' => 'Email', 'comment' => 'Email для получения уведомления при добавлении нового отзыва'],
			
			
		];
	}
	
		

}	

?>