<?php

namespace common\models;

use Yii;
use common\models\EntityExt;

/**
 * This is the model class for table "adr_payments".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $date
 * @property integer $status
 * @property string $ptype
 * @property string $value
 * @property string $info
 *
 * @property User $user
 */
class Import extends \common\models\CObject
{


    public static function validateImportedFile($file){

        //$a = new Import();

        /*var_dump($a->captionTemplate());
        die();*/
        //$this->captionTemplate();


        $response["success"] = false;
        $response["message"] = 'unknown error';
        $availableMimeTypes = array(
            'application/vnd.ms-excel',
            'application/msexcel',
            'application/x-msexcel',
            'application/x-ms-excel',
            'application/x-excel',
            'application/x-dos_ms_excel',
            'application/xls',
            'application/x-xls',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        );
        $type = mime_content_type($file['tmp_name']);
        if(in_array($type, $availableMimeTypes)){
            if($file['size'] < 50 * 1024 * 1024){
                $response["success"] = true;
                $response["message"] = 'OK';
            }else{
                $response["message"] = 'too big';
            }
        }else{
            $response["message"] = 'wrong extension';
        }
        return $response;
    }

    public static function parseExcel($file){
        $inputFileType = \PHPExcel_IOFactory::identify($file['tmp_name']);
        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file['tmp_name']);
		
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	
            $arrayData[$worksheet->getTitle()] = $worksheet;
        };
		unset($objReader);
        return $arrayData;
    }
	
	public static function getData($file){
		$ppTypeId = DocumentType::byAlias('paymentorder') -> id;
		$invTypeId = DocumentType::byAlias('invoice') -> id;		
		$rIdList = array_keys(EntityExt::portal() -> requisiteList()); // Список реквизитов портала
		$result = [];
		$sheets = self::parseExcel($file);
		foreach ($sheets as $sheet) { 

			$highestRow = $sheet->getHighestRow();

			
			for ($row = 2; $row <= $highestRow; ++ $row) {
				$summ = $sheet -> getCellByColumnAndRow(1, $row) -> getValue(); // Сумма платежа
				if ($summ <= 0) continue;
				$dataRow = [];
				$pmDate = self::createDate($sheet -> getCellByColumnAndRow(0, $row)); // Дата платежа
				
				$invNumber = $sheet -> getCellByColumnAndRow(2, $row) -> getValue(); // Номер счета				
				$invDate = self::createDate($sheet -> getCellByColumnAndRow(3, $row)); // Дата счета				
				$ppNumber = $sheet -> getCellByColumnAndRow(4, $row) -> getValue(); // Номер платежки				
				$ppDate = self::createDate($sheet -> getCellByColumnAndRow(5, $row));	//  Дата платежки				
				
				$dataRow['pmDate'] = $pmDate;
				$dataRow['ppDate'] = $ppDate;
				$dataRow['summ'] = $summ;
				$dataRow['invNumber'] = $invNumber;
				$dataRow['invDate'] = $invDate;
				$dataRow['ppNumber'] = $ppNumber;				
				$dataRow['Payer'] = '';
				$dataRow['Recipient'] = '';	

				// Попробуем найти счет в базе с указанным номером и среди тех, которые выставлены порталом
				$inv = Document::find() -> andWhere(['doc_type_id' => $invTypeId, 'number' => $invNumber, 'owner_id' => $rIdList]) -> one(); /// TODO: Косяк! - не хочет искать по терминальному классу (Invoice)				
				////////////////////////////////////////////////////////////////////////////
				$dataRow['message'] = 'Ok.';
				$dataRow['messageType'] = 'message';
				
				if (!$pmDate) {					
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Не указана дата поступления платежа - платеж не будет загружен";
				}
				
	
				if ($inv) { // счет найдет
					// Сравниваем сумму счета и платежа
					if ((int)$inv -> summa < (int)$summ) {
						$dataRow['messageType'] = 'notice';
						$dataRow['message'] = "Плата больше счета.";
					} else if ((int)$inv -> summa > (int)$summ) {
						$dataRow['messageType'] = 'notice';
						$dataRow['message'] = "Частичная оплата.";
					};
					/// Платежка
					// Попробуем найти платежку в базе

					
					$pp = Document::find() -> andWhere(['doc_type_id' => $ppTypeId, 'number' => $ppNumber, 'owner_id' => $inv -> recipient_id]) -> one();
					if ($pp) { // Нашлась
						$dataRow['messageType'] = 'notice';
						$dataRow['message'] = trim($dataRow['message']." Платеж уже существует в системе - будет обновлен");
					};
					
					$dataRow['Payer'] = $inv -> recipient -> getTitle().'<br>ИНН:'.$inv -> recipient -> inn;	
					$dataRow['Recipient'] = $inv -> owner -> getTitle().'<br>ИНН:'.$inv -> owner -> inn;	
					
				} else {
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Счет не найден, платеж не будет загружен.";					
				};				
				

				
				////////////////////////////////////////////////////////////////////////////
				
				$result[] = $dataRow;
            }
        }
		return $result;
	}

	
	// Загрузка данных в систему. Возвращает этот же массив с комментариями по отработке
	public static function loadData($data) {
		$newCount = 0;
		$updatedCount = 0;
		if (!$data || !is_array($data))  {
			return false;
		} else {
			
			$ppTypeId = DocumentType::byAlias('paymentorder') -> id;
			$invTypeId = DocumentType::byAlias('invoice') -> id;		
			$rIdList = array_keys(EntityExt::portal() -> requisiteList()); // Список реквизитов портала
			$result = [];
			
			foreach($data as $dataRow) {
				$pmDate = $dataRow['pmDate'];
				$ppDate = $dataRow['ppDate'];
				$summ = $dataRow['summ'];
				$invNumber = $dataRow['invNumber'];
				$invDate = $dataRow['invDate'];
				$ppNumber = $dataRow['ppNumber'];	
				
				$inv = Document::find() -> andWhere(['doc_type_id' => $invTypeId, 'number' => $invNumber, 'owner_id' => $rIdList]) -> one(); 
				
				$dataRow['Recipient'] = $inv -> owner -> getTitle().'<br>ИНН:'.$inv -> owner -> inn;	
				$dataRow['Payer'] = $inv -> recipient -> getTitle().'<br>ИНН:'.$inv -> recipient -> inn;	
				$dataRow['message'] = 'Платеж добавлен';
				$dataRow['messageType'] = 'message';				
				// Попробуем найти счет в базе с указанным номером и среди тех, которые выставлены порталом
				
				$skip = false;
				////////////////////////////////////////////////////////////////////////////
							
				$dataRow['message'] = 'Платеж добавлен';
				$dataRow['messageType'] = 'message';
				
				if (!$pmDate) {					
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Не указана дата поступления платежа - платеж не загружен";
					$skip = true;
				}
				
	
				if ($inv) { // счет найдет
					// Сравниваем сумму счета и платежа
					if ((int)$inv -> summa < (int)$summ) {
						$dataRow['messageType'] = 'notice';
						$dataRow['message'] = "Плата больше счета";
					} else if ((int)$inv -> summa > (int)$summ) {
						$dataRow['messageType'] = 'notice';
						$dataRow['message'] = "Частичная оплата";
					};
					
				
					/// Платежка
					// Попробуем найти загружаемую платежку в базе. Идентификация по номеру и плательшику
					$pp = Document::find() -> andWhere(['doc_type_id' => $ppTypeId, 'number' => $ppNumber, 'owner_id' => $inv -> recipient_id]) -> one();
					if ($pp) { // Нашлась
						
						$dataRow['messageType'] = 'notice';
						$dataRow['message'] = trim('Платеж обновлен');
					};
					
				} else {
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Счет не найден, платеж не загружен.";					
					$skip = true;
				};				
				

			
			
				////////////////////////////////////////////////////////////////////////////
				
				$result[] = $dataRow;
				if ($skip ) continue;
				
				$rPortalId = $inv -> owner_id; // в платежке используются те же реквизиты портала, что и в счете
				
				/// Платежка
				// Попробуем найти платежку в базе
				$pp = Document::find() -> andWhere(['doc_type_id' => $ppTypeId, 'number' => $ppNumber, 'owner_id' => $inv -> recipient_id]) -> one();
				if (!$pp) { // Если не нашлась, то добавляем
					$pp = new Document();
					$pp -> doc_type_id = $ppTypeId;
					$pp -> number = $ppNumber;
					$pp -> owner_id = $inv -> recipient_id; // владелец платежки является плательщиком - это контрагент портала
					$pp -> recipient_id = $rPortalId;// получатель платежки является получателем платежа - это портал
					$newCount++;
				} else {
					$updatedCount++;
				}
				

				$pp -> created_at = $ppDate;
			
				$pp -> updated_at = $ppDate;
				$pp -> order_id = $inv -> order_id;
				$pp -> porder_id = $inv -> porder_id;
				$pp -> address_id = $inv -> address_id;
				$pp -> summa = $inv -> summa;

				$pp -> save();
							

				$inv_id = $inv -> id; 
				$inv = Invoice::findOne([$inv_id]) ;
				$inv -> payment($pp, ['date' => $pmDate, 'loaded_at' => Entity::dbNow()]); // создаем оплату по платежке
				
				
			}
			$message = '';
			if ($newCount > 0) {
				$message = $message.'Добавлено платежей: '.$newCount.'<br>';	
			};
			if ($updatedCount > 0) {
				$message = $message.'Обновлено платежей: '.$updatedCount.'<br>';	
			};
			
			
			if ($updatedCount + $newCount > 0) {
				Yii::$app->session->setFlash('success', $message);
			} else {
				Yii::$app->session->setFlash('error', "Ни одного платежа не закружено");
			}
			
			return $result;
			
		}		
	}
	
	protected static function createDate($cell, $format = 'd.m.Y') {
		
		$result = NULL;
		if ($cell) {
			$value = $cell->getValue();
			if(\PHPExcel_Shared_Date::isDateTime($cell)) {				
				$result = date($format, \PHPExcel_Shared_Date::ExcelToPHP($value)); 
			}

		}
		return $result;
	}
	
    public static function compareToDbData($sheets){

		$ppTypeId = DocumentType::byAlias('paymentorder') -> id;
		$invTypeId = DocumentType::byAlias('invoice') -> id;
		$rList = EntityExt::portal() -> requisiteList();// Список реквизитов портала
		$rIdList = array_keys($rList);
		$rPortalId = EntityExt::portal() -> default_profile_id; // Реквизиты портала, которые используются для платежок
		$hasErrors = false;
        $table = [];
		$count = 0;
		$messages = [];
        foreach ($sheets as $sheet) { //getting sheets		
			$highestRow = $sheet->getHighestRow();

			for ($row = 2; $row <= $highestRow; ++ $row) {
				
				$pmDate = self::createDate($sheet -> getCellByColumnAndRow(0, $row)); // Дата платежа
				$summ = $sheet -> getCellByColumnAndRow(1, $row) -> getValue(); // Сумма платежа
				$invNumber = $sheet -> getCellByColumnAndRow(2, $row) -> getValue(); // Номер счета				
				$ppNumber = $sheet -> getCellByColumnAndRow(4, $row) -> getValue(); // Номер платежки
								
				
				if (!$ppNumber) {					
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = 'Не указан номер платежного поручения - не загружено';
					continue;
				}
				
				if (!$pmDate) {					
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Не указана дата поступления платежа для ПП №".$ppNumber.' или не распознан распознан формат даты ('.$pmDate.') - не загружено';
					continue;
				}
				
				if (!$ppDate) {					
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Не указана дата для ПП №".$ppNumber.' или не распознан распознан формат даты ('.$pmDate.')- не загружено';
					continue;
				}

				// Попробуем найти счет в базе с указанным номером и среди тех, которые выставлены порталом
				$inv = Document::find() -> andWhere(['doc_type_id' => $invTypeId, 'number' => $invNumber, 'owner_id' => $rIdList]) -> one(); /// TODO: Косяк! - не хочет искать по терминальному классу (Invoice)
	
				if ($inv) { // счет найдет
					// Сравниваем сумму счета и платежа
					if ((int)$inv -> summa < (int)$summ) {
						$dataRow['messageType'] = 'notice';
						$dataRow['message'] = "плата больше счета";
					} else if ((int)$inv -> summa > (int)$summ) {
						$dataRow['messageType'] = 'notice';
						$dataRow['message'] = "Частичная оплата";
					};
					
					
				} else {
					$dataRow['messageType'] = 'notice';
					$dataRow['message'] = "Счет не найден, платежное поручение не будет загружено.";					
				};				
				
				/// Платежка
				// Попробуем найти платежку в базе
				$pp = Document::find() -> andWhere(['doc_type_id' => $ppTypeId, 'owner_id' => $inv -> recipient_id, 'recipient_id' => $rPortalId, 'number' => $ppNumber, 'owner_id' => $rIdList]) -> one();
				if ($pp) { // Нашлась
					$dataRow['messageType'] = 'notice';
					$dataRow['message'] = "Платежное поручение уже добавлено - будет обновлено";					
				};
				//////////////////////////
				
				
				
				$rPortalId = $inv -> owner_id; // в платежке используются те же реквизиты портала, что и в счете
				
				/// Платежка
				// Попробуем найти платежку в базе
				$pp = Document::find() -> andWhere(['doc_type_id' => $ppTypeId, 'number' => $ppNumber, 'owner_id' => $rIdList]) -> one();
				if (!$pp) { // Если не нашлась, то добавляем
					$pp = new Document();
					$pp -> doc_type_id = $ppTypeId;
					$pp -> number = $ppNumber;
					$pp -> owner_id = $inv -> recipient_id;
					$pp -> recipient_id = $rPortalId;
				};
				

				$pp -> created_at = $ppDate;
			
				$pp -> updated_at = $ppDate;
				$pp -> order_id = $inv -> order_id;
				$pp -> porder_id = $inv -> porder_id;
				$pp -> address_id = $inv -> address_id;
				$pp -> summa = $inv -> summa;

				$pp -> save();
							

				$inv_id = $inv -> id; 
				$inv = Invoice::findOne([$inv_id]) ;
				$inv -> payment($pp, ['date' => $pmDate, 'loaded_at' => Entity::dbNow()]); // создаем оплату по платежке

            }
        }
		
		
		Yii::$app->session->setFlash('success', "Загружено платежей: ".$count);
		
		return !$hasErrors;
		
        //return $table;
    }

    public static function compareToDbDataOutcome($sheets){
        $table = [];
        foreach ($sheets as $sheet) { //getting sheets
            if(count($sheet)){
                array_shift($sheet); // delete sheet title
            }

            foreach ($sheet as &$line){ // getting lines and selecting record from DB.documents foreach line.

                /**
                 *
                 *
                 * добавить user_id в таблицу adr_documents (так как запрос получается ОЧЕНЬ тяжелый)
                 */
                $line['query_result'] = \Yii::$app->db->createCommand( "SELECT 
                    
                    customer_id,
                    username
                FROM adr_orders 
                    LEFT JOIN adr_users 
                    ON adr_users.id = adr_orders.customer_id
                
                WHERE adr_orders.id= :order_id", [":order_id" => $line[2]])->queryOne();

                if($line['query_result'] != false){

                    /** Получаем все платежные поручения по этому заказу */
                    $line['payment_info'] = \Yii::$app->db->createCommand( "SELECT
 
                        adr_documents.date, number, adr_documents.id,  adr_payments.id as payment_id
                        
                    FROM adr_payments LEFT JOIN adr_documents ON adr_payments.payment_order_id = adr_documents.id  
                    
                    WHERE adr_documents.doc_type_id = '2' and adr_payments.value < 0 and adr_payments.order_id = :order_id", [":order_id" => $line['2']])->queryAll();
                }else{
                    $line['payment_info'] = false;
                }
                $table[] = $line;
            }
        }
        return $table;
    }

    /** Вставляем платёж и обновляем статус заказа */
    public static function importAndModify(){

        /*echo "<pre>", print_r($_POST);
        die();*/
        foreach($_POST['data'] as $data){

            /** Вставляем платёжное поручение в документы */
            $data['document']['doc_type_id'] = '2';
            $dump = Document::addBy($data['document']);
            /*Yii::$app->db->createCommand()
                ->insert('adr_documents', [
                    'date' => $data['pay_data'],
                    'number' => $data["pay_number"],
                    'order_id' => $data['order_id'],
                ])->execute();*/

            /** Получаем id  вставленной записи для поля  payment_order_id*/

            $insert_id = $dump->id;

            /**  Вставляем платёж */
            $data['payment']['payment_order_id'] = $insert_id;
            $data['payment']['ptype'] = 'По счету';
            Payment::addBy($data['payment']);
            /*Yii::$app->db->createCommand()
                ->insert('adr_payments', [
                    'date' => $data["payment_date"],
                    'value' => $data["sum"],
                    'ptype' => 'По счету',
                    'document_id' => $data["doc_id"],
                    'user_id' => $data["user"],
                    'payment_order_id' => $insert_id,
                ])->execute();*/

            /** Обновляем статус заказа */
            if($data['payment']['value'] >= $data['doc_sum']){
                $status = 8;
            }else{
                $status = 7;
            }
            $columns = array(
                "status_id" => $status
            );
            Yii::$app->db->createCommand()->update('adr_orders', $columns, 'id=:id', array(':id' => (int)$data['document']['order_id']))->execute();


        }
    }

    /** Вставляем платёж и обновляем статус заказа */
    public static function importAndModifyOutcome(){

       /* echo "<pre>", print_r($_POST);
        die();*/
        foreach($_POST['data'] as $data){

            /** Вставляем платёжное поручение в документы */
            $data['document']['doc_type_id'] = '2';
            $dump = Document::addBy($data['document']);
           /* Yii::$app->db->createCommand()
                ->insert('adr_documents', [
                    'date' => $data['pay_data'],
                    'number' => $data["pay_number"],
                    'order_id' => $data['order'],
                    'doc_type_id' => '2',
                ])->execute();*/

            /** Получаем id  вставленной записи для поля  payment_order_id*/
            $insert_id = $dump->id;


            /**  Вставляем платёж */
            $data['payment']['ptype'] = 'По счету';
            $data["payment"]["value"] = -1 * abs($data["payment"]["value"]);
            $data['payment']['payment_order_id'] = $insert_id;
            //echo "<pre>", print_r($data['payment']);
            Payment::addBy($data['payment']);
            /*Yii::$app->db->createCommand()
                ->insert('adr_payments', [

                    'date' => $data["payment_date"],
                    'value' => $sum,
                    'ptype' => 'По счету',
                    'order_id' => $data["order"],
                    'user_id' => $data["user_id"],
                    'payment_order_id' => $insert_id,
                ])->execute();*/

        }
        //die("TEST");
    }

}
