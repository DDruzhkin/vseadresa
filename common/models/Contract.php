<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

class Contract extends ActiveRecord
{
    const SCENARIO_REGISTER = 'register';
    const SCENARIO_CHANGE = 'change';
    const SCENARIO_PROLONGATION = 'prolongation';

    /**
     * Declares the name of the database table associated with this AR class.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'adr_contracts';
    }

    /**
     * Known fields.
     *
     * @var array
     */
    protected $knownFields = [
        'company_name',
        'company_full_name',
        'execution_officer_full_name',
        'execution_officer_envoy_ship',
        'execution_officer_recording_city',
        'execution_officer_recording_address',
    ];

    /**
     * Returns the attribute labels.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'company_name' => 'Сокращённое название организации',
            'company_full_name' => 'Полное название организации',
            'company_inn' => 'ИНН огранизации',
            'company_ogrn' => 'ОГРН',
            'company_kpp' => 'КПП',
            'execution_officer_full_name' => 'ФИО исполнительного лица',
            'execution_officer_envoy_ship' => 'Должность исполнительного лица',
            'execution_officer_recording_city' => 'Город регистрации исполнительного лица',
            'execution_officer_recording_address' => 'Адрес регистрации исполнительного лица',
        ];
    }

    /**
     * Returns a list of scenarios and the corresponding active attributes.
     *
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_REGISTER => $this->getRegisterFields(),
            self::SCENARIO_CHANGE => $this->getChangeFields(),
        ];
    }

    /**
     * Returns the validation rules for attributes.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [$this->getRegisterFields(), 'required', 'on' => self::SCENARIO_REGISTER],
            [$this->getChangeFields(), 'required', 'on' => self::SCENARIO_CHANGE],
            [['company_inn'], 'string', 'max' => 10],
            [['company_ogrn'], 'string', 'max' => 13],
            [['company_kpp'], 'string', 'max' => 9],
        ]);
    }

    /**
     * Get the register fields.
     *
     * @return array
     */
    protected function getRegisterFields()
    {
        return $this->knownFields;
    }

    /**
     * Get the change fields.
     *
     * @return array
     */
    protected function getChangeFields()
    {
        return array_merge($this->knownFields, [
            'company_inn',
            'company_ogrn',
            'company_kpp',
        ]);
    }

    /**
     * Returns a list of behaviors that this component should behave as.
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     *
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }

}