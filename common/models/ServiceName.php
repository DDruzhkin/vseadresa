<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;



/**
 * This is the model class for table "adr_service_names".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $stype
 * @property integer $to_address
 * @property string $description
 * @property integer $option_id
 * @property integer $undeleted
 * @property string $alias
 *
 * @property Option $option

 * @property ServicePrice[] $servicePrices
 */
class ServiceName extends \common\models\EntityExt 
{
	
	/* Константы модели */
	const STYPE_ONCE = 'Разовая';
	const STYPE_MONTHLY = 'Ежемесячная';
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_service_names'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [
			'stype' => ['Разовая' => 'Разовая', 'Ежемесячная' => 'Ежемесячная', ],  

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Справочник услуг', 
			'rodp' => 'услуги', 
			'vinp' => 'услугу', 
			'imp' => 'услуга', 
			'model' => 'ServiceName', 
			'parent' => 'objects', 
			'caption' => '[%code%] %name%', 
			'comment' => 'Справочник услуг, оказываемых владельцами адресов. Этот справочник меняется только администратором портала. Владельцы адресов могут свофримровать свой справочник услуг в рамках этого справочника.', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
		return [
			'id' => ['title' => 'Код услуги', 'show' => '1', 'label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'visible' => '1', ], 
			'code' => ['label' => 'Код', 'visible' => '1', 'filter' => 'value', 'mask' => '99', 'default' => '00', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
			'name' => ['label' => 'Название услуги', 'comment' => 'Название услуги,Список некоторых услуг владельцев, Предоставление юридического адреса, Почтовое обслуживание, Предоставление рабочего места', 'visible' => '1', 'filter' => 'like', 'default' => '', 'type' => 'string', 'required' => '1', 'show' => '1', ], 
			'stype' => ['label' => 'Тип услуги', 'content' => 'Услуга является разовой или ежемесячной', 'visible' => '1', 'filter' => 'list', 'comment' => 'Является ли услуга разовой или оказывается ежемесячно', 'default' => 'Ежемесячная', 'type' => 'enum', 'required' => '1', 'show' => '1', ], 
			'to_address' => ['type' => 'boolean', 'label' => 'Добавлять в адрес', 'visible' => '0', 'content' => 'Добавлять ли услугу в адрес по умолчанию', 'readonly' => '0', 'comment' => 'Добавлять ли эту услугу автоматически при добавлении нового адреса', 'required' => '0', 'show' => '1', ], 
			'description' => ['label' => 'Описание', 'visible' => '0', 'readonly' => '0', 'comment' => 'Подробное описание услуги, которое будет показано заказчику', 'type' => 'text', 'required' => '0', 'show' => '1', ], 

			'option_id' => ['label' => 'Связанная опция', 'visible' => '1', 'readonly' => '0', 'comment' => 'Опция, соответствующая услуге. Будет автоматически выставляться при добавлении услуги. А услуга автоматчиески добавляться при добавлении опции', 'model' => 'Option', 'type' => 'ref', 'required' => '0', 'show' => '1', 'link' => '0', ], 
			'undeleted' => ['label' => 'Неудаляемая', 'comment' => 'Услуга является неудаляемой, ее невозможно удалить из справочника', 'visible' => '0', 'readonly' => '1', 'type' => 'boolean', 'default' => '0', 'required' => '1', 'show' => '1', ], 
			'alias' => ['label' => 'Псевдоним', 'comment' => 'Внутренне имя услуги для привязки ее к участию в бизнес-логике', 'show' => '1', 'visible' => '0', 'readonly' => '1', 'type' => 'string', 'required' => '0', ], 

		];
	}
	/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	

	// Создать объект по указанному псевдониму, если такой псевдоним существует.
	public static function byAlias($value) 
	{
		return self::findOne(['alias' => $value]);
	}








    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'name'], 'required'],
            [['id', 'option_id'], 'integer'],
			[['to_address', 'undeleted'], 'safe'],
            [['stype', 'description'], 'string'],
            [['code'], 'string', 'max' => 9],
            [['name'], 'string', 'max' => 50],
//            [['to_address', 'undeleted'], 'string', 'max' => 1],
            [['alias'], 'string', 'max' => 45],
            [['id'], 'unique'],
            [['alias'], 'unique'],
			[['code'], 'unique'],
            [['option_id'], 'exist', 'skipOnError' => true, 'targetClass' => Option::className(), 'targetAttribute' => ['option_id' => 'id']],

        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption() {
        return $this->hasOne(Option::className(), ['id' => 'option_id']);

    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicePrices() {
        return $this->hasMany(ServicePrice::className(), ['service_name_id' => 'id']);
    }



    public static function captionTemplate(){

        return "[%code%] %name%";
    }





}