<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_notifications".
 *
 * @property integer $id
 * @property string $type
 * @property string $email
 * @property integer $user_id
 * @property string $created_at
 * @property integer $enabled
 * @property integer $status_id
 *
 * @property Status $status
 * @property User $user
 */
class Notification extends \common\models\EntityExt 
{
	
	/* Константы модели */
	const NT_USER_STATUS = 'user_status';
	const NT_ORDER_STATUS = 'order_status';
	const NT_ADDRESS_STATUS = 'address_status';
	const NT_REVIEW_STATUS = 'review_status';
	const NT_REGISTRATION = 'registration';
	const NT_QUICK_ORDER = 'quick_order';
	const NT_PAYMENT_CONFIRMATION = 'payment_confirmation';
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_notifications'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [
			'type' => ['user_status' => 'user_status', 'order_status' => 'order_status', 'address_status' => 'address_status', 'review_status' => 'review_status', 'registration' => 'registration', 'quick_order' => 'quick_order', 'payment_confirmation' => 'payment_confirmation', ],  

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Уведомления', 
			'model' => 'Notification', 
			'parent' => 'objects', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'type' => ['label' => 'Тип оповещения', 'type' => 'enum', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'email' => ['label' => 'Адрес, на который будут уходить оповещения', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'user_id' => ['label' => 'Пользователь, к которму относятся оповещения', 'model' => 'User', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 
			'created_at' => ['label' => 'Created At', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'readonly' => '1', 'type' => 'timestamp', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'enabled' => ['label' => 'Активно', 'type' => 'boolean', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'status_id' => ['label' => 'Статус', 'comment' => 'Статус, при переходе на который возникает уведомление', 'model' => 'Status', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
		
	return array_merge($result,
	[
		
		[
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'create_time',
			
			'value' => new Expression('NOW()'),			
			
		],
	]);
	
		
}
	

	public static function statusByAlias($alias) {
			return Status::findOne(["alias" => $alias, "table" => self::tableName()]);
	}
			
			






    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['type'], 'required'],
            [['type'], 'string'],
            [['user_id', 'status_id'], 'integer'],
            [['created_at'], 'safe'],
            [['email'], 'string', 'max' => 45],
            [['enabled'], 'string', 'max' => 1],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus() {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }



    public static function captionTemplate(){

        return "[%id%]";
    }



/* Возвращает число ссылок на модель */
			function hasLinks() {
				
				return 
					($this -> parent()?$this -> parent() -> hasLinks():0) ;
			}
			





}