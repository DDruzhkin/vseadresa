<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;



/**
 * This is the model class for table "adr_counters".
 *
 * @property integer $id
 * @property string $alias
 * @property integer $value
 * @property string $updated_at
 * @property string $template
 */
class BaseCounter extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_counters'; 
    }




	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Счетчики', 
			'model' => 'BaseCounter', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
		return [
			'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'alias' => ['label' => 'Псевдоним номера', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'value' => ['label' => 'Последний номер', 'default' => 0, 'type' => 'integer', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'updated_at' => ['label' => 'Дата получения последнего номера', 'readonly' => '1', 'type' => 'timestamp', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'template' => ['label' => 'шаблон генерации номера', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
				
		];
	}
	/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
		
	return array_merge($result,
	[
		
		[
			'class' => TimestampBehavior::className(),
			
			'updatedAtAttribute' => 'update_time',
						
			
		],
	]);
	
		
}
	







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['value'], 'integer'],
            [['updated_at'], 'safe'],
            [['alias', 'template'], 'string', 'max' => 50],
        ]);
    }
	

	
	
	
	



    public static function captionTemplate(){

        return "[%id%]";
    }





}