<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;

use backend\models\Faq;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * FaqSearch represents the model behind the search form about `\backend\models\Faq`.
 */
class FaqSearch extends Model {
	
	
	public $created_at;	
	public $role;	
	
	
	    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'role'], 'safe'],
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Faq::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'id'=>SORT_DESC				
			]
     ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
		
		
						
				
				
					
				
		/* фильтры по daterangepicker */
		if ($this -> created_at) {
			list($dateFrom, $dateTo) = explode('-', $this -> created_at);
			$dateFrom = self::datetToSql($dateFrom);
			$dateTo = self::datetToSql($dateTo, 1);
			
			$query->andFilterWhere(['between', 'created_at', $dateFrom, $dateTo]);
		};
				
				
						
				
				
			
		/* фильтры по set */
		if ($this->role) {
			if($this->role > '') {
				$this->role = Faq::getList('role')[$this->role];
				$query->andWhere('FIND_IN_SET(:role,role)>0',[':role' => $this->role]);
			}			
		}
					
				
				
			

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
