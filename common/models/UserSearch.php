<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;

use backend\models\User;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form about `\backend\models\User`.
 */
class UserSearch extends Model {
	
	
	public $id;	
	public $fullname;	
	public $username;	
	public $role;	
	public $status_id;	
	public $created_at;	
	public $email;	
	public $phone;	
	
	
	    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_id'], 'integer'],
            [['fullname', 'username', 'role', 'created_at', 'email', 'phone'], 'safe'],
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'id'=>SORT_DESC				
			]
     ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
		
		
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['id' => $this -> id > ""?(int)$this -> id:""]);
						
					
				
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'fullname', $this->fullname]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'username', $this->username]);
				
						
				
				
			
		/* фильтры по set */
		if ($this->role) {
			if($this->role > '') {
				$this->role = User::getList('role')[$this->role];
				$query->andWhere('FIND_IN_SET(:role,role)>0',[':role' => $this->role]);
			}			
		}
					
				
				
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['status_id' => $this -> status_id > ""?(int)$this -> status_id:""]);
						
					
				
				
						
				
				
					
				
		/* фильтры по daterangepicker */
		if ($this -> created_at) {
			list($dateFrom, $dateTo) = explode('-', $this -> created_at);
			$dateFrom = self::datetToSql($dateFrom);
			$dateTo = self::datetToSql($dateTo, 1);
			
			$query->andFilterWhere(['between', 'created_at', $dateFrom, $dateTo]);
		};
				
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'email', $this->email]);
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'phone', $this->phone]);
				
			

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
