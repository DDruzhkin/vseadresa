<?php
/*
*	Базовый (системный) класс управления уведомлениями
*/
namespace common\models;


use yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use common\models\EntityHelper;
use common\models\Status;
use yii\base\Exception;

class NotificationManager extends Model 
{	
	
	const NT_EMAIL = 1;	// отправка по email
	const NT_SMS = 2;	// отправка по sms
	
	
	// в массиве $params обязательно должны быть определены отправитель (email.from) получатель или несколько через запятую(email.to)
	// Отправляет сообщения
	// notification - идентификатор уведомления, совпадает с именем используемого шаблона для генерации текста сообщения
	public function notify($notification, $params) {
		
//		if (!$types || in_array(NT_EMAIL, $types)) {
			$to = ArrayHelper::getValue($params, 'email.to'); //if (!$to)throw new Exception('Не определен получатель');
			if ($to) {
				$subject = ArrayHelper::getValue($params, 'email.subject'); if (!$subject)$subject = 'Уведомление '.Yii::$app -> name;
                $toMails = explode(',', $to);

				try {
					foreach ($toMails as $to) {				
							
							EntityHelper::sendEmail($subject, $params, $to, $notification);					
					}
				
				} catch (Exception $e) {
						Yii::$app->session->setFlash('error', 'Ошибка при отправки уведомления на электронную почту. '.$e->getMessage());						
				}

			};
//		}
	}

	
	// Этот метод вызывается при смене статуса
	// Может быть вызван и переопределен под конкретные прикладные задачи
	// Если $oldStatusId = null - значит, модель создается
	public function notifyChangeStatus($oldStatusId, $newStatusId, $model, $actorId = null) {
/*
		$oldStatus = Status::findOne($oldStatusId);
		$newStatus = Status::findOne($newStatusId);

		var_dump($oldStatus);
		var_dump($newStatus -> attributes);
		exit;
		*/
	}
	
	
}