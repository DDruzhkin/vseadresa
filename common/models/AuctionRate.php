<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_auction_rates".
 *
 * @property integer $id
 * @property string $created_at
 * @property integer $auction_id
 * @property integer $address_id
 * @property integer $owner_id
 * @property double $amount
 *
 * @property Address $address
 * @property Auction $auction
 * @property User $owner
 * @property Auction[] $auctions
 */
class AuctionRate extends \common\models\EntityExt 
{
	
	/* Константы модели */
	const STEP = 100;
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_auction_rates'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Список ставок аукциона', 
			'imp' => 'Ставка аукциона', 
			'rodp' => 'Ставки аукциона', 
			'vinp' => 'Ставку аукциона', 
			'model' => 'AuctionRate', 
			'parent' => 'objects', 
			'caption' => 'Ставка аукциона %amount%', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'created_at' => ['label' => 'Дата ставки', 'type' => 'datetime', 'filter' => 'range', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'readonly' => '1', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'auction_id' => ['label' => 'Акуцион', 'filter' => 'like', 'model' => 'Auction', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 
			'address_id' => ['label' => 'Адрес', 'filter' => 'like', 'condition' => '[owner_id=>owner_id]', 'model' => 'Address', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 
			'owner_id' => ['label' => 'Владелец адреса', 'filter' => 'like', 'default' => 'currentUser()', 'model' => 'User', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 
			'amount' => ['label' => 'Размер ставки', 'format' => '[\'decimal\', 2]', 'title' => 'Размер ставки', 'mask' => '@decimal', 'filter' => 'list', 'default' => '0', 'type' => 'float', 'required' => '0', 'show' => '1', 'visible' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
		
	return array_merge($result,
	[
		
		[
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'create_time',
			
			'value' => new Expression('NOW()'),			
			
		],
	]);
	
		
}
	







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id'], 'required'],
            [['id', 'auction_id', 'address_id', 'owner_id'], 'integer'],
            [['created_at'], 'safe'],
            [['amount'], 'number'],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['auction_id'], 'exist', 'skipOnError' => true, 'targetClass' => Auction::className(), 'targetAttribute' => ['auction_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress() {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuction() {
        return $this->hasOne(Auction::className(), ['id' => 'auction_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner() {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuctions() {
        return $this->hasMany(Auction::className(), ['rate_id' => 'id']);
    }



    public static function captionTemplate(){

        return "Ставка аукциона %amount%";
    }





}