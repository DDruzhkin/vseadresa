<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;

use backend\models\ServiceName;
use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ServiceNameSearch represents the model behind the search form about `\backend\models\ServiceName`.
 */
class ServiceNameSearch extends Model {
	
	
	public $code;	
	public $name;	
	public $stype;	
	
	
	    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'stype'], 'safe'],
        ];
    }
	
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ServiceName::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=>[
			'defaultOrder'=>[
				'id'=>SORT_DESC				
			]
     ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		
		
		
						
				
			
		/* фильтры по значению, по списку для ссылки*/
		$query->andFilterWhere(['code' => $this->code]);				
					
				
				
						
				
				
					
				
				
		/* фильтры по like */
		$query->andFilterWhere(['like', 'name', $this->name]);
				
						
				
				
					
				
				
			

        //echo 'sql: '.$query->prepare(Yii::$app->db->queryBuilder)->createCommand()->sql;
        return $dataProvider;
    }
	
	public static function datetToSql($date, $addDays = 0) {
		$date = strtotime(trim($date)) + $addDays * 86400;
		$date = date("Y-m-d", $date);			
		return $date;
	}
	
}
