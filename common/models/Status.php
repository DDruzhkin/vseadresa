<?php

namespace common\models;


use yii;



/**
 * This is the model class for table "adr_status".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $table
 * @property integer $isdefault
 *
 * @property Address[] $addresses
 * @property Order[] $orders
 * @property Payment[] $payments
 * @property Review[] $reviews
 * @property Service[] $services
 * @property StatusChange[] $statusChanges
 * @property StatusChange[] $statusChanges0
 * @property StatusLinks[] $statusLinks
 * @property StatusLinks[] $statusLinks0
 * @property User[] $users
 */
class Status extends \common\models\EntityExt
{
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_status';
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Статусы',
			'model' => 'Status',
			'parent' => 'objects',

		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
		return [
			'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ],
			'name' => ['label' => 'Name', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ],
			'alias' => ['label' => 'Псевдоним', 'comment' => 'Внутренне имя услуги для привязки ее к участию в бизнес-логике', 'show' => '0', 'readonly' => '1', 'type' => 'string', 'required' => '0', 'visible' => '0', ],

			'table' => ['label' => 'Table', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ],
			'isdefault' => ['label' => 'Isdefault', 'default' => 0, 'type' => 'smallint', 'required' => '0', 'show' => '1', 'visible' => '1', ],

		];
	}
	/* Поведения*/
	public function behaviors(){

	$result = [];
		$ns = "common\models\\";
		$class = $ns.$this -> meta()['model'].'Behavior';

		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};

		return $result;


}






	// Значение статуса по умолчанию для указанного прикладного класса
	public static function getDefault($className) {
		return self::findByCondition(['table' => $className::tableName(), 'isdefault' => 1]) -> one();
	}

	// Список значений статусов, из которых может статус измениться в текуший
	public function getFrom() {
		return self::findBySql("select s.*  from {{%status_links}} sl, {{%status}} s where s.id = from_id AND to_id = :to", ["to" => $this -> id]);
	}

	// Список значений статусов, в которые может статус измениться из текуший
	public function getTo() {
		return self::findBySql("select s.*  from {{%status_links}} sl, {{%status}} s where s.id = to_id AND from_id = :from", ["from" => $this -> id]);
	}

	// Сохраняет изменения статуса в истории
	public static function saveToHistory($oldValue, $value, $object) {

		$actor = self::currentUser();

		if (gettype($oldValue) == 'object') $oldValue = $oldValue -> id;
		if (gettype($value) == 'object') $value = $value -> id;

		$obj = null;
		if (gettype($object) == 'object') {
			$obj = $object;
			$object = $object -> id;
			
		}


		if (!$oldValue && !$value || ($oldValue == $value) ) return false;
		
		
		$list = ['object_id' => $object];
		if ($oldValue) $list['status_old'] = $oldValue;
		if ($value) $list['status'] = $value;

		$list['actor_id'] = $actor;
		
		$command = Yii::$app->db->createCommand();

		$result = $command->insert('adr_status_changes', $list) -> execute();
		if (is_null($obj)) $obj = Entity::byId($object);
		Yii::$app -> notifier -> notifyChangeStatus($oldValue, $value, $obj, $actor);
		return $result;
   }







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id'], 'required'],
            [['id', 'isdefault'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 45],

            [['table'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ]);
    }








    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses() {
        return $this->hasMany(Address::className(), ['status_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Order::className(), ['status_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments() {
        return $this->hasMany(Payment::className(), ['status_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews() {
        return $this->hasMany(Review::className(), ['status_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices() {
        return $this->hasMany(Service::className(), ['status_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusChanges() {
        return $this->hasMany(StatusChange::className(), ['status_old' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusChanges0() {
        return $this->hasMany(StatusChange::className(), ['status' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusLinks() {
        return $this->hasMany(StatusLinks::className(), ['from_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusLinks0() {
        return $this->hasMany(StatusLinks::className(), ['to_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['status_id' => 'id']);
    }



    public static function captionTemplate(){

        return "%name%";
    }





}