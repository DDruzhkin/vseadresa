<?php
namespace common\models;

use yii;
use yii\base\Behavior;
use yii\helpers\Url;
use common\models\EntityExt;

class FaqBehavior extends Behavior
{

public static function getAdminMenuItemsDescription() {
		return
		'
		Возвращает пункты меню для FAQ в ЛКА, в зависимости от текущего состояния вопроса
		';
	}
	
	public function getAdminMenuItems() {
		$faq = $this -> owner;		
		$menuItems = [];
		$enabled = $faq -> enabled;
				
		$menuItems[] = [
			'label' => $enabled?'Скрыть':'Показать',				
			'url' => Url::to(['/admin/faq/to-'.($enabled?'dis':'en').'abled', 'id' => $faq -> id]),
		];
		
		
		if (count($menuItems)) $menuItems[] = '<li class="divider"></li>';
		$menuItems[] = [
			'label' => 'Изменить',				
			'url' => Url::to(['/admin/faq/update', 'id' => $faq -> id]),
		];
		
		/*
		$menuItems[] = [
			'label' => 'Просмотреть',				
			'url' => Url::to(['/admin/faq/view', 'id' => $faq -> id]),
		];
		*/
		return $menuItems;
		
		
		
	}	
}

?>