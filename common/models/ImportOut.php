<?php
// Импорт исходящих платежей
namespace common\models;

use Yii;
use common\models\EntityExt;

/**
 * This is the model class for table "adr_payments".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $date
 * @property integer $status
 * @property string $ptype
 * @property string $value
 * @property string $info
 *
 * @property User $user
 */
class ImportOut extends Import
{

	public static function getData($file){
		$ppTypeId = DocumentType::byAlias('paymentorder') -> id;
		$invTypeId = DocumentType::byAlias('invoice') -> id;		
		$rIdList = array_keys(EntityExt::portal() -> requisiteList()); // Список реквизитов портала
		$rPortalId = EntityExt::portal() -> default_profile_id;// Реквизиты портала по умолчанию
		$rPortal = Entity::byId($rPortalId);
		$result = [];
		$sheets = self::parseExcel($file);
        foreach ($sheets as $sheet) { //getting sheets
		/*
            if(count($sheet)){
                array_shift($sheet); // delete sheet title
            }
*/
			$highestRow = $sheet->getHighestRow();

			for ($row = 2; $row <= $highestRow; ++ $row) {
				$summ = $sheet -> getCellByColumnAndRow(1, $row) -> getValue(); // Сумма платежа				
				if ($summ <= 0) continue;
				$dataRow = [];
				
				$pmDate = self::createDate($sheet -> getCellByColumnAndRow(0, $row)); // Дата платежа								
				$ppNumber = $sheet -> getCellByColumnAndRow(2, $row) -> getValue(); // Номер платежки
				$ppDate = self::createDate($sheet -> getCellByColumnAndRow(3, $row)); // Дата платежного поручения
				$recipientINN = $sheet -> getCellByColumnAndRow(4, $row) -> getValue(); // ИНН получателя
				
				$dataRow['pmDate'] = $pmDate;				
				$dataRow['summ'] = $summ;								
				$dataRow['ppDate'] = $ppDate;
				$dataRow['ppNumber'] = $ppNumber;	
				$dataRow['recipientINN'] = $recipientINN;								
				$dataRow['recipient'] = '';	
				$dataRow['recipientId'] = '';	
				$dataRow['payer'] = $rPortal -> getTitle().'<br>ИНН:'.$rPortal -> inn;	
				$dataRow['message'] = 'Ok';
				$dataRow['messageType'] = 'message';
				
		
				//////////////////////////
				if (!$pmDate) {					
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Не указана дата поступления платежа для ПП №".$ppNumber.' или не распознана формат даты - не загружено';
					continue;
				}
				
				if (!$ppDate) {					
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Не указана дата для ПП №".$ppNumber.' или не распознана формат даты - не загружено';
					continue;
				}


				$recipient = Rip::findOne(['inn' => $recipientINN]);
				if (!$recipient) $recipient = Rcompany::findOne(['inn' => $recipientINN]);
				if (!$recipient) $recipient = Rperson::findOne(['inn' => $recipientINN]);
				
				if (!$recipient) {					
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Не найден контрагент с ИНН: ".$recipientINN.' для ПП №'.$ppNumber.' - не загружено';
					continue;
				}
				
				$dataRow['recipient'] = $recipient -> getTitle().'<br>ИНН:'.$recipient -> inn;					
				$dataRow['recipientId'] = $recipient -> id;
				
				/// Платежка
				// Попробуем найти платежку в базе
				// по номеру и плательщику (портал)
				$pp = Document::find() -> andWhere(['doc_type_id' => $ppTypeId, 'number' => $ppNumber, 'owner_id' => $rIdList]) -> one();
				if ($pp) { // Нашлась
					$dataRow['messageType'] = 'notice';
					$dataRow['message'] = "Платежное поручение уже добавлено - будет обновлено";										
				};
				//////////////////////////
				
				

				
				$result[] = $dataRow;
            }
        }
		return $result;
	}

	
	// Загрузка данных в систему. Возвращает этот же массив с комментариями по отработке
	public static function loadData($data) {
		
		$newCount = 0;
		$updatedCount = 0;
		if (!$data || !is_array($data))  {
			return false;
		} else {
			
			$ppTypeId = DocumentType::byAlias('paymentorder') -> id;
			$invTypeId = DocumentType::byAlias('invoice') -> id;		
			$rIdList = array_keys(EntityExt::portal() -> requisiteList()); // Список реквизитов портала
			$rPortalId = EntityExt::portal() -> default_profile_id;// Реквизиты портала по умолчанию
			$rPortal = Entity::byId($rPortalId);
			$result = [];
			
			foreach($data as $dataRow) {
				if (!is_array($dataRow)) continue;				
				$pmDate = $dataRow['pmDate'];
				$ppDate = $dataRow['ppDate'];
				$ppNumber = $dataRow['ppNumber'];	
				$summ = $dataRow['summ'];				
				$recipientId = (int)$dataRow['recipient'];								
		
				$dataRow['recipient'] = '';			
				$dataRow['payer'] = $rPortal -> getTitle().'<br>ИНН:'.$rPortal -> inn;	
				$dataRow['message'] = 'Платеж добавлен';
				$dataRow['messageType'] = 'message';
		
		
				////////////////////////////////////////////////////////////////////////////		
				
				
				if (!$pmDate) {					
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Не указана дата поступления платежа для ПП №".$ppNumber.' или не распознана формат даты - не загружено';
					continue;
				}
				
				if (!$ppDate) {					
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Не указана дата для ПП №".$ppNumber.' или не распознана формат даты - не загружено';
					continue;
				}


				/*
				$recipient = Rip::findOne(['inn' => $recipientINN]);
				if (!$recipient) $recipient = Rcompany::findOne(['inn' => $recipientINN]);
				if (!$recipient) $recipient = Rperson::findOne(['inn' => $recipientINN]);
				*/
				$recipient = Entity::byId($recipientId);
				
				if (!$recipient) {					
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Не найден контрагент с ИНН: ".$recipientINN.' для ПП №'.$ppNumber.' - не загружено';
					continue;
				}
				$dataRow['recipient'] = $recipient -> getTitle();
				
				
				/// Платежка
				// Попробуем найти платежку в базе
				// по номеру и плательщику (портал)
				$pp = Document::find() -> andWhere(['doc_type_id' => $ppTypeId, 'number' => $ppNumber, 'owner_id' => $rIdList]) -> one();
				if ($pp) { // Нашлась
					$dataRow['messageType'] = 'notice';
					$dataRow['message'] = "Платеж обновлен";
				};

				if (!$pp) { // Если не нашлась, то добавляем
					$pp = new Document();
					$pp -> doc_type_id = $ppTypeId;
					$pp -> number = $ppNumber;
					$pp -> owner_id = $rPortalId; // Владалец платежки(плательщик) - портал
					$pp -> recipient_id = $recipient -> id;	// получатель платежки (получатель платежа) - контрагент портала
					$newCount++;
										
				} else {
					$updatedCount++;
				}
				$pp -> created_at = $ppDate;			
				$pp -> updated_at = $ppDate;
				$pp -> summa = $summ;
				$pp -> save();
				
				$payment = $pp -> createPayment(['date' => $pmDate, 'loaded_at' => Entity::dbNow()]); // создаем оплату по платежке
				$payment -> invoice_id = null;
				$payment -> save();
				////////////////////////////////////////////////////////////////////////////
							
				
							
				if (!$pmDate) {					
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Не указана дата поступления платежа - платеж не загружен";
					$skip = true;
				}
				
			
				

			
			
				////////////////////////////////////////////////////////////////////////////
				
				$result[] = $dataRow;
				//if ($skip ) continue;
				

				
				
			}
			$message = '';
			if ($newCount > 0) {
				$message = $message.'Добавлено платежей: '.$newCount.'<br>';	
			};
			if ($updatedCount > 0) {
				$message = $message.'Обновлено платежей: '.$updatedCount.'<br>';	
			};
			
			
			if ($updatedCount + $newCount > 0) {
				Yii::$app->session->setFlash('success', $message);
			} else {
				Yii::$app->session->setFlash('error', "Ни одного платежа не закружено");
			}
			
			return $result;
			
		}		
	}
	
	
    public static function compareToDbData($sheets){

		$ppTypeId = DocumentType::byAlias('paymentorder') -> id; // Платежное поручение		
		$rPortalId = EntityExt::portal() -> default_profile_id;// Реквизиты портала по умолчанию
		$rPortal = Entity::byId($rPortalId);
				
		$hasErrors = false;
        $table = [];
		$count = 0;
		$messages = [];

        foreach ($sheets as $sheet) { //getting sheets
		/*
            if(count($sheet)){
                array_shift($sheet); // delete sheet title
            }
*/
			$highestRow = $sheet->getHighestRow();

			for ($row = 2; $row <= $highestRow; ++ $row) {
				
				$pmDate = self::createDate($sheet -> getCellByColumnAndRow(0, $row)); // Дата платежа				
				$summ = $sheet -> getCellByColumnAndRow(1, $row) -> getValue(); // Сумма платежа
				$ppDateNumber = $sheet -> getCellByColumnAndRow(2, $row) -> getValue(); // Дата платежного поручения
				$ppNumber = $sheet -> getCellByColumnAndRow(3, $row) -> getValue(); // Номер платежки
				$recipientINN = $sheet -> getCellByColumnAndRow(4, $row) -> getValue(); // ИНН получателя
								
				
				if (!$pmDate) {
					Yii::$app->session->setFlash('error', "Не указана дата поступления платежа. Платежное поручение не загружено");
					continue;
				}

				//////////////////////////
				if (!$pmDate) {					
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Не указана дата поступления платежа для ПП №".$ppNumber.' или не распознан формат даты - не загружено';
					continue;
				}

				$recipient = Rip::findOne(['inn' => $recipientINN]);
				if (!$recipient) $recipient = Rcompany::findOne(['inn' => $recipientINN]);
				if (!$recipient) $recipient = Rperson::findOne(['inn' => $recipientINN]);
				
				if (!$pmDate) {					
					$dataRow['messageType'] = 'error';
					$dataRow['message'] = "Не найден контрагент с ИНН: ".$recipientINN.' для ПП №'.$ppNumber.' - не загружено';
					continue;
				}
				
				/// Платежка
				// Попробуем найти платежку в базе
				// по номеру и плательщиком должен быть портал
				$pp = Document::find() -> andWhere(['doc_type_id' => $ppTypeId, 'number' => $ppNumber, 'owner_id' => $rIdList]) -> one();
				if ($pp) { // Нашлась
					$dataRow['messageType'] = 'notice';
					$dataRow['message'] = "Платежное поручение уже добавлено - будет обновлено";										
				};
				//////////////////////////
				
				
				
				
				/// Платежка
				// Попробуем найти платежку в базе
				$pp = Document::find() -> andWhere(['doc_type_id' => $ppTypeId, 'number' => $ppNumber, 'owner_id' => $rIdList]) -> one();
				if (!$pp) { // Если не нашлась, то добавляем
					$pp = new Document();
					$pp -> doc_type_id = $ppTypeId;
					$pp -> number = $ppNumber;
					$pp -> owner_id = $rPortalId;
					$pp -> recipient_id = $inv -> recipient_id;
				};
				

				$pp -> created_at = $ppDate;
			
				$pp -> updated_at = $ppDate;
				$pp -> order_id = $inv -> order_id;
				$pp -> porder_id = $inv -> porder_id;
				$pp -> address_id = $inv -> address_id;
				$pp -> summa = $inv -> summa;

				$pp -> save();
							

				$inv_id = $inv -> id; 
				$inv = Invoice::findOne([$inv_id]) ;
				$inv -> payment($pp, ['date' => $pmDate, 'loaded_at' => Entity::dbNow()]); // создаем оплату по платежке

            }
        }
		
		
		Yii::$app->session->setFlash('success', "Загружено платежей: ".$count);
		
		return !$hasErrors;
		
        //return $table;
    }

    public static function compareToDbDataOutcome($sheets){
        $table = [];
		
        foreach ($sheets as $sheet) { //getting sheets
			$sheet = $sheet -> toArray();
            if(count($sheet)){
                array_shift($sheet); // delete sheet title
            }

            foreach ($sheet as &$line){ // getting lines and selecting record from DB.documents foreach line.

                /**
                 *
                 *
                 * добавить user_id в таблицу adr_documents (так как запрос получается ОЧЕНЬ тяжелый)
                 */
                $line['query_result'] = \Yii::$app->db->createCommand( "SELECT 
                    
                    customer_id,
                    username
                FROM adr_orders 
                    LEFT JOIN adr_users 
                    ON adr_users.id = adr_orders.customer_id
                
                WHERE adr_orders.id= :order_id", [":order_id" => $line[2]])->queryOne();

                if($line['query_result'] != false){

                    /** Получаем все платежные поручения по этому заказу */
                    $line['payment_info'] = \Yii::$app->db->createCommand( "SELECT
 
                        adr_documents.date, number, adr_documents.id,  adr_payments.id as payment_id
                        
                    FROM adr_payments LEFT JOIN adr_documents ON adr_payments.payment_order_id = adr_documents.id  
                    
                    WHERE adr_documents.doc_type_id = '2' and adr_payments.value < 0 and adr_payments.order_id = :order_id", [":order_id" => $line['2']])->queryAll();
                }else{
                    $line['payment_info'] = false;
                }
                $table[] = $line;
            }
        }
        return $table;
    }

    /** Вставляем платёж и обновляем статус заказа */
    public static function importAndModify(){

        /*echo "<pre>", print_r($_POST);
        die();*/
        foreach($_POST['data'] as $data){

            /** Вставляем платёжное поручение в документы */
            $data['document']['doc_type_id'] = '2';
            $dump = Document::addBy($data['document']);
            /*Yii::$app->db->createCommand()
                ->insert('adr_documents', [
                    'date' => $data['pay_data'],
                    'number' => $data["pay_number"],
                    'order_id' => $data['order_id'],
                ])->execute();*/

            /** Получаем id  вставленной записи для поля  payment_order_id*/

            $insert_id = $dump->id;

            /**  Вставляем платёж */
            $data['payment']['payment_order_id'] = $insert_id;
            $data['payment']['ptype'] = 'По счету';
            Payment::addBy($data['payment']);
            /*Yii::$app->db->createCommand()
                ->insert('adr_payments', [
                    'date' => $data["payment_date"],
                    'value' => $data["sum"],
                    'ptype' => 'По счету',
                    'document_id' => $data["doc_id"],
                    'user_id' => $data["user"],
                    'payment_order_id' => $insert_id,
                ])->execute();*/

            /** Обновляем статус заказа */
            if($data['payment']['value'] >= $data['doc_sum']){
                $status = 8;
            }else{
                $status = 7;
            }
            $columns = array(
                "status_id" => $status
            );
            Yii::$app->db->createCommand()->update('adr_orders', $columns, 'id=:id', array(':id' => (int)$data['document']['order_id']))->execute();


        }
    }

    /** Вставляем платёж и обновляем статус заказа */
    public static function importAndModifyOutcome(){

       /* echo "<pre>", print_r($_POST);
        die();*/
        foreach($_POST['data'] as $data){

            /** Вставляем платёжное поручение в документы */
            $data['document']['doc_type_id'] = '2';
            $dump = Document::addBy($data['document']);
           /* Yii::$app->db->createCommand()
                ->insert('adr_documents', [
                    'date' => $data['pay_data'],
                    'number' => $data["pay_number"],
                    'order_id' => $data['order'],
                    'doc_type_id' => '2',
                ])->execute();*/

            /** Получаем id  вставленной записи для поля  payment_order_id*/
            $insert_id = $dump->id;


            /**  Вставляем платёж */
            $data['payment']['ptype'] = 'По счету';
            $data["payment"]["value"] = -1 * abs($data["payment"]["value"]);
            $data['payment']['payment_order_id'] = $insert_id;
            //echo "<pre>", print_r($data['payment']);
            Payment::addBy($data['payment']);
            /*Yii::$app->db->createCommand()
                ->insert('adr_payments', [

                    'date' => $data["payment_date"],
                    'value' => $sum,
                    'ptype' => 'По счету',
                    'order_id' => $data["order"],
                    'user_id' => $data["user_id"],
                    'payment_order_id' => $insert_id,
                ])->execute();*/

        }
        //die("TEST");
    }

}
