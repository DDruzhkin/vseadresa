<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_orders".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $status_changed_at
 * @property string $number
 * @property integer $status_id
 * @property string $duration
 * @property integer $customer_id
 * @property integer $customer_r_id
 * @property string $requisites
 * @property integer $address_id
 * @property integer $owner_id
 * @property string $date_start
 * @property string $date_fin
 * @property double $address_price
 * @property integer $is_quick
 * @property string $paid_at
 * @property integer $delivery_id
 * @property integer $delivery_type_id
 * @property string $delivery_contacts
 * @property string $delivery_address
 * @property integer $guarantee_id
 * @property integer $add_guarantee
 * @property string $payment_type
 * @property double $summa
 *
 * @property Document[] $documents
 * @property CObject $customerR
 * @property Porder $delivery
 * @property Delivery $deliveryType
 * @property User $owner
 * @property Porder $guarantee
 * @property Address $address
 * @property Status $status
 * @property User $customer
 * @property Service[] $services
 */
class Order extends \common\models\EntityExt 
{
	
	/* Константы модели */
	const PTYPE_NONE = 'не задан';
	const PTYPE_CACH = 'Наличные';
	const PTYPE_CARD = 'По карте';
	const PTYPE_BANK = 'По счету';
	const DURATION_ONCE = 'разово';
	const DURATION_6 = '6 месяцев';
	const DURATION_11 = '11 месяцев';
	const DTYPE_PICKUP = 'самовывоз';
	const DTYPE_DELIVERY = 'доставка';
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_orders'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [
			'duration' => ['6 месяцев' => '6 месяцев', '11 месяцев' => '11 месяцев', ],  
			'payment_type' => ['не задан' => 'не задан', 'по карте' => 'по карте', 'по счету' => 'по счету', ],  

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Заказы', 
			'imp' => 'Заказ', 
			'rodp' => 'Заказа', 
			'vinp' => 'Заказ', 
			'model' => 'Order', 
			'parent' => 'objects', 
			'caption' => '№%number% на сумму %summa_view%', 
			'comment' => 'Список всех заказов, сделанных на портале.', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'created_at' => ['label' => 'Создан', 'comment' => 'Дата создания заказа', 'visible' => '1', 'filter' => 'range', 'type' => 'datetime', 'format' => 'd.m.Y', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'readonly' => '1', 'required' => '1', 'show' => '1', ], 
			'updated_at' => ['label' => 'Дата изменения', 'show' => '0', 'readonly' => '1', 'type' => 'timestamp', 'required' => '0', 'visible' => '0', ], 
			'status_changed_at' => ['label' => 'Дата изменения статуса', 'show' => '0', 'comment' => 'Дата последнего изменения статуса. Выставляется автоматически', 'type' => 'timestamp', 'required' => '0', 'visible' => '0', ], 
			'number' => ['label' => 'Номер', 'filter' => 'value', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'status_id' => ['label' => 'Статус', 'filter' => 'list', 'vidible' => '1', 'model' => 'Status', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 
			'duration' => ['label' => 'Срок оказания услуг', 'comment' => 'Срок приобретения услуг заказа в месяцах.', 'visible' => '1', 'filter' => 'list', 'default' => '11 месяцев', 'type' => 'string', 'required' => '1', 'show' => '1', ],
			'customer_id' => ['label' => 'Заказчик', 'filter' => 'like', 'link' => '1', 'comment' => 'Пользователь, совершивший заказ', 'model' => 'User', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'customer_r_id' => ['label' => 'Реквизиты заказчика', 'filter' => 'like', 'link' => '1', 'comment' => 'Реквизиты заказчика, на которые будет оформлен заказ и необходимые документы', 'readonly' => '0', 'visible' => '0', 'condition' => '[user_id=>customer_id]', 'model' => 'CObject', 'type' => 'ref', 'required' => '0', 'show' => '1', ], 
			'requisites' => ['label' => 'Реквизиты', 'comment' => 'Реквизиты, на которые оформляется адрес', 'type' => 'text', 'show' => '1', 'visible' => '1', ],
            'requisite_company_name' => ['label' => 'Название компании', 'type' => 'text', 'show' => '1', 'visible' => '1'],
            'requisite_inn' => ['label' => 'ИНН', 'type' => 'text', 'show' => '1', 'visible' => '1',],
            'requisite_ogrn' => ['label' => 'ОГРН', 'type' => 'text', 'show' => '1', 'visible' => '1',],
            'requisite_kpp' => ['label' => 'КПП', 'type' => 'text', 'show' => '1', 'visible' => '1',],
            'requisite_bank_name' => ['label' => 'Наименование банка', 'type' => 'text', 'show' => '1', 'visible' => '1', ],
            'requisite_bank_id' => ['label' => 'БИК', 'type' => 'text', 'show' => '1', 'visible' => '1', ],
            'requisite_bank_account' => ['label' => 'Расчётный счёт', 'comment' => 'Реквизиты, на которые оформляется адрес', 'type' => 'text', 'show' => '1', 'visible' => '1', ],
            'requisite_bank_correspondent_account' => ['label' => 'Корреспондентский счёт', 'comment' => 'Реквизиты, на которые оформляется адрес', 'type' => 'text', 'show' => '1', 'visible' => '1', ],
            'company_name' => ['label' => 'Сокращённое название организации', 'type' => 'text', 'required' => '1', 'show' => '1', 'visible' => '1'],
            'company_full_name' => ['label' => 'Полное название организации', 'type' => 'text', 'required' => '1', 'show' => '1', 'visible' => '1'],
            'company_inn' => ['label' => 'ИНН огранизации', 'type' => 'text', 'show' => '1', 'visible' => '1', ],
            'company_ogrn' => ['label' => 'ОГРН', 'type' => 'text', 'show' => '1', 'visible' => '1', ],
            'company_kpp' => ['label' => 'КПП', 'type' => 'text', 'show' => '1', 'visible' => '1', ],
            'execution_officer_full_name' => ['label' => 'ФИО исполнительного лица', 'type' => 'text', 'required' => '1', 'show' => '1', 'visible' => '1', ],
            'execution_officer_envoy_ship' => ['label' => 'Должность исполнительного лица', 'type' => 'text', 'required' => '1', 'show' => '1', 'visible' => '1', ],
            'execution_officer_recording_city' => ['label' => 'Город регистрации исполнительного лица', 'type' => 'text', 'required' => '1', 'show' => '1', 'visible' => '1', ],
            'execution_officer_recording_address' => ['label' => 'Адрес регистрации исполнительного лица', 'type' => 'text', 'required' => '1', 'show' => '1', 'visible' => '1', ],
            'comment' => ['label' => 'Комментарий к заказу', 'type' => 'text', 'show' => '1', 'visible' => '1', ],
            'address_id' => ['label' => 'Адрес', 'comment' => 'Адрес, к которому относится закза. Заказать можно одну или несколько услуг, относящихся только к одному адресу', 'filter' => 'like', 'visible' => '1', 'link' => '1', 'condition' => '[owner_id=>owner_id]', 'model' => 'Address', 'type' => 'ref', 'required' => '1', 'show' => '1', ],
			'owner_id' => ['label' => 'Владелец', 'filter' => 'like', 'link' => '1', 'comment' => 'Владелец заказанного адреса', 'readonly' => '1', 'model' => 'User', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'date_start' => ['label' => 'Старт', 'comment' => 'Дата начала аренды адреса', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'show' => '1', 'visible' => '0', 'required' => '0', ], 
			'date_fin' => ['label' => 'Окончание', 'comment' => 'Дата окончания аренды адреса. Определяется датой старта и длительностью аренды.', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'show' => '1', 'visible' => '0', 'readonly' => '1', 'required' => '0', ], 
			'address_price' => ['label' => 'Стоимость адреса', 'comment' => 'Стоимость адреса за выбранный период', 'visible' => '1', 'readonly' => '1', 'format' => '[\'decimal\', 2]', 'type' => 'double', 'required' => '0', 'show' => '1', ], 
			'is_quick' => ['label' => 'Срочный', 'comment' => 'Заказ является срочным. Устанавливается автоматически при наличии услуги срочного оформления в заказе', 'visible' => '0', 'type' => 'boolean', 'default' => 0, 'required' => '1', 'show' => '1', ], 
			'paid_at' => ['label' => 'Заказ оплачен', 'show' => '0', 'comment' => 'Дата оплаты заказа', 'type' => 'timestamp', 'required' => '0', 'visible' => '0', ], 
			'delivery_id' => ['label' => 'Заказ на доставку', 'comment' => 'Заказ порталу на доставку документов', 'visible' => '0', 'readonly' => '1', 'link' => '1', 'model' => 'Porder', 'type' => 'ref', 'required' => '0', 'show' => '1', ], 
			'delivery_type_id' => ['label' => 'Способ доставки', 'visible' => '0', 'model' => 'Delivery', 'type' => 'ref', 'required' => '0', 'show' => '1', 'link' => '0', ], 
			'delivery_contacts' => ['label' => 'Контакты по доставке', 'comment' => 'контактная информация для связи по вопросу доставки', 'visible' => '0', 'type' => 'string', 'required' => '1', 'show' => '1', ], 
			'delivery_address' => ['label' => 'Место и время доставки', 'comment' => 'Желаемое место и время доставки документов', 'visible' => '0', 'type' => 'string', 'required' => '1', 'show' => '1', ], 
			'guarantee_id' => ['label' => 'Заказ на гарантию', 'comment' => 'Заказ на пр', 'visible' => '0', 'readonly' => '1', 'link' => '1', 'model' => 'Porder', 'type' => 'ref', 'required' => '0', 'show' => '1', ], 
			'add_guarantee' => ['show' => '0', 'label' => 'Add Guarantee', 'default' => '0', 'type' => 'tinyint', 'required' => '0', 'visible' => '0', ], 
			'payment_type' => ['label' => 'Способ оплаты', 'filter' => 'list', 'visible' => '1', 'default' => 'не задан', 'type' => 'enum', 'required' => '0', 'show' => '1', ], 
			'summa' => ['label' => 'Сумма заказа', 'comment' => 'Общая стоимость заказа', 'visible' => '1', 'readonly' => '1', 'format' => 'currency', 'mask' => '@decimal', 'default' => 0, 'type' => 'double', 'required' => '0', 'show' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
		
	return array_merge($result,
	[
		
		[
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'create_time',
			'updatedAtAttribute' => 'update_time',
			'value' => new Expression('NOW()'),			
			
		],
	]);
	
		
}
	

	// Создать объект по указанному псевдониму, если такой псевдоним существует.
	public static function byCreatedAt($value) 
	{
		return self::findOne(['created_at' => $value]);
	}

	public static function statusByAlias($alias) {
			return Status::findOne(["alias" => $alias, "table" => self::tableName()]);
	}
			
			






    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'requisites', 'address_id', 'delivery_contacts', 'delivery_address'], 'required'],
            [['id', 'status_id', 'customer_id', 'customer_r_id', 'address_id', 'owner_id', 'delivery_id', 'delivery_type_id', 'guarantee_id'], 'integer'],
            [['created_at', 'updated_at', 'status_changed_at', 'date_start', 'date_fin', 'paid_at'], 'safe'],
            [['duration', 'requisites','requisite_company_name', 'requisite_bank_id','requisite_bank_name','requisite_bank_account','requisite_bank_correspondent_account', 'payment_type', 'comment'], 'string'],
            [['address_price', 'summa'], 'number'],
            [['number'], 'string', 'max' => 45],
            [['is_quick', 'add_guarantee'], 'string', 'max' => 1],
            [['delivery_contacts', 'delivery_address'], 'string', 'max' => 150],
            [['id'], 'unique'],
            [['created_at'], 'unique'],
            [['customer_r_id'], 'exist', 'skipOnError' => true, 'targetClass' => CObject::className(), 'targetAttribute' => ['customer_r_id' => 'id']],
            [['delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Porder::className(), 'targetAttribute' => ['delivery_id' => 'id']],
            [['delivery_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Delivery::className(), 'targetAttribute' => ['delivery_type_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['guarantee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Porder::className(), 'targetAttribute' => ['guarantee_id' => 'id']],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['customer_id' => 'id']],

            [
                [
                    'company_name',
                    'company_full_name',
                    'execution_officer_full_name',
                    'execution_officer_envoy_ship',
                    'execution_officer_recording_city',
                    'execution_officer_recording_address',
                    //'requisite_company_name',
                    //'requisite_bank_name',
                    //'requisite_bank_id',
                    //'requisite_bank_account',
                    //'requisite_bank_correspondent_account',
                ], 'required'
            ],
            [['company_inn', 'requisite_inn'], 'string', 'min' => '10', 'max' => 10],
            [['company_ogrn', 'requisite_ogrn'], 'string', 'max' => 13],
            [['company_kpp', 'requisite_kpp'], 'string', 'max' => 9],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments() {
        return $this->hasMany(Document::className(), ['order_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerR() {
        return $this->hasOne(CObject::className(), ['id' => 'customer_r_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery() {
        return $this->hasOne(Porder::className(), ['id' => 'delivery_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryType() {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_type_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner() {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuarantee() {
        return $this->hasOne(Porder::className(), ['id' => 'guarantee_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress() {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus() {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer() {
        return $this->hasOne(User::className(), ['id' => 'customer_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices() {
        return $this->hasMany(Service::className(), ['order_id' => 'id']);
    }

    /**
     *
     *
     * @return yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::class, ['order_id' => 'id']);
    }



    public static function captionTemplate(){

        return "№%number% на сумму %summa_view%";
    }



/* Возвращает число ссылок на модель */
			function hasLinks() {
				
				return 
					($this -> parent()?$this -> parent() -> hasLinks():0)  + 
					$this -> getDocuments() -> count() + 
					$this -> getServices() -> count();
			}
			





}