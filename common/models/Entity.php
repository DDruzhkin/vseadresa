<?php
/* 
* Базовый класс для прикладных моделей, не предназначен для прикладной кастомизации.
* Для внесения изменений в модели через наследование необходимо использовать класс EntityExt
*/


namespace common\models;

use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\BaseJson;
use yii\helpers\Url;
use yii\web\UploadedFile;
use common\controllers\FilesController;
use common\models\UploadException;
use yii\base\Exception;


/**
 *
 * @property integer $id
 *
 */
class Entity extends \yii\db\ActiveRecord
{

    public $rootClassName = __CLASS__;
    protected $_parent = false;    // Родитель по данным
    const statusClassName = 'Status';// Имя класса статуса
    protected $lastDirtyAttributes; //Атрибуты измненные перед последним сохранением
    private $objects = []; // Пул объектов

    public $autoIntegrate = true; //Автоматическое согласование данных (вызов метода integrate) перед сохранением.

    public static function statusFieldName()
    {
        return strtolower(self::statusClassName) . "_id";
    }

    public static function statusClassName()
    {
        return self::ns() . self::statusClassName;
    }


    // Возвращает маску ввода поля input по алиасу.
    // Для расширения списка алиасов надо переопределять метод getMaskAliasList
    // Алиас может быть как с символом @ вначале, так и без
    public static function getMaskByAlias($alias)
    {
        if ($alias[0] == '@') $alias = substr($alias, 1);
        return (isset(get_called_class()::getMaskAliasList()[$alias])) ? get_called_class()::getMaskAliasList()[$alias] : false;
    }

    // Метод, который возвращает маски ввода поля input по алиасу.
    // Этот метод может быть переопределен в потомках

    public static function getMaskAliasList()
    {
        return [
            'phone' => '+7(999) 999-99-99',            // номер телефон (Россия)
            'account' => '999 99 999 9 9{11,13}',    // Номер банковского счета
            'pasport' => '99 99 999999',        // Номер российского паспорта
            'money' => '9{1,12}.{0,1}9{0,2}',        // деньги
        ];

    }


    // Пространство имен текущего класса
    public static function ns()
    {
        return StringHelper::dirname(self::className()) . '\\';
    }

    // Имя текущего класса без пространства имен
    public static function baseClassName()
    {
        return StringHelper::basename(self::className());
    }

    // id модели (класса)
    public static function modelId()
    {
        return Inflector::camel2id(get_called_class()::baseClassName());
    }

    public function hasAttribute($name)
    {
        return in_array($name, $this->attributes());
    }

    // Создает объект по id (с автоматическим опредлением класса) - работает только для наследников
    public static function byId($id, $ns = false)
    {
        $result = NULL;
        if (!$id) return $result;
        $obj = CObject::findOne($id);
        if (!$obj) throw new Exception('Объект с id=' . $id . ' не найден');
        $tableName = $obj->tablename;

        $comment = Yii::$app->db->createCommand(
            'SELECT table_comment
			FROM information_schema.tables
			WHERE table_name = :tname'
        )->bindValue(':tname', $tableName)
            ->queryScalar();


        $data = array();
        $lines = explode(";", $comment);
        foreach ($lines as $line) {
            $arr = explode(":", $line);
            if (count($arr) > 1) {
                $name = $arr[0];
                unset($arr[0]);
                $value = implode(':', $arr);
                $data[strtolower(trim(strtolower($name)))] = trim($value);
            }
        }

        if (!isset($data['model'])) throw new Exception('Класс для таблицы ' . $tableName . ' не найден');
        if (!$ns) $ns = self::ns();
        $result = ($ns . $data['model'])::findOne($id);
        return $result;
    }


    // Проверяет, есть ли ссылки на эту модель
    // Метод переопределяется в потомках - возможна автоматическая генераця метода
    public function hasLinks()
    {
        return 0;
    }


    // Если это имя атрибута с постфиксом _view, то возвращает изначальное имя атрибута, иначе возвращает то же имя $attrName
    protected static function getViewAttrName($attrName)
    {
        $result = $attrName;
        if (substr($attrName, -5) == '_view') {
            $result = substr($attrName, 0, -5);
        }
        return $result;
    }


    // Имя класса родителя по данным
    public static function parentClassName()
    {
        return false;
    }


    // Возвращает родителя, который хранит родительские данные
    public function parent()
    {
        $parentClassName = $this->parentClassName();
        if (!$this->_parent && $parentClassName) {

            if ($this->getIsNewRecord()) {
                if (!$this->_parent || ($this->_parent && !$this->_parent->getIsNewRecord()))
                    $this->_parent = new $parentClassName();
            }
            /*			else {
                            $this -> _parent = $this -> _parent = $parentClassName::findOne($this -> id);
                        }
                        */

        };
        if ($this->_parent && $this->_parent->id != $this->id) $this->_parent = $parentClassName::findOne($this->id);
// родитель всегда должены быть синхронизирован по id
        return $this->_parent;
    }


    public function getId()
    {
        return $this->getPrimaryKey();
    }

    // Клонирует модель на $model
    // Можно указать конкретный набор атрибутов, переносимый при клонировании
    public function cloneTo($model, $attrs = false)
    {
        if (!$attrs) $attrs = $this->attributes();

        foreach ($attrs as $name) {
            if (strtolower($name) == 'id') continue;
            $model[$name] = $this[$name];
        }
    }

    // Создает клона модели на $model
    // Можно указать конкретный набор атрибутов, переносимый на клона
    public function createClone($attrs = false)
    {
        $className = self::className();
        $model = new $className;
        $this->cloneTo($model);
        return $model;
    }


    // Возвращает массив списков возможных значений для атрибутов - переопределяетя в наследниках
    public static function attrLists()
    {
        return [
            //'role' => [1 => 'Администратор', 2 => 'Заказчик', 3 => 'Владелец'],
            //'form' => [1 => 'Физ.лицо', 2 => 'Юр.лицо', 3 => 'ИП'],
        ];
    }

    // Возвращает массив мета-данных класса - переопределяетя в наследниках
    public static function meta()
    {
        return [

        ];
    }

    // Возвращает массив мета-данных для атрибутов - переопределяетя в наследниках
    public static function attrMeta()
    {
        return [
        ];
    }


    // по имени файла картинки вернет полное имя файла, досутпное из web
    public static function getImage($fileName = '')
    {

        return Url::to(['files/images/' . self::modelId()]) . '/' . $fileName;
    }


    // Если не указан icon, то возвращает список иконок для атрибута в формате <имя файла> => <тег картинки или полный путь в зависимости от значения $format>
    // Если указан icon, то возвращает полный путь к изображению иконки
    // $format = 'img' - тег с картинкой, 'path' - только путь
    // $properties - свойства тега img
    public static function getIcon($attrName, $icon = false, $format = 'img', $properties = false)
    {
        $wpath = Url::to(['files/icons/options']) . '/'; // TODO: в эту процедуру необходимо передавать значение "options"
        $result = [];
        if (!($properties && isset($properties['width']))) $properties['width'] = '24px';
        $list = FilesController::getIconList('options'); // TODO: в ф-ю getIcon необходимо передавать значение 'options' (имя набора)


        foreach ($list as $fn) {
            $v = $wpath . $fn;
            if ($format == 'img') {
                $props = 'src="' . $v . '" alt="' . $fn . '"';
                foreach ($properties as $pName => $pValue) {
                    $props = $props . ' ' . $pName . '="' . $pValue . '"';
                }
                //$v = '<img width="24px" src="'.$v.'" alt="'.$fn.'"/>';
                $v = '<img ' . $props . '/>';
            }
            $result[$fn] = $v;
        }


        if ($icon) {
            if (isset($result[$icon])) {
                $result = $result[$icon];
            } else {
                $result = ''; // не найдена иконка
            }
        }

        return $result;
    }


    // Возвращает конфигурацию модели
    public static function config()
    {
        $config = include(Yii::getAlias('@common') . '/config/models.php');
        $cn = StringHelper::basename(self::className());
        $config = isset($config[$cn]) ? $config[$cn] : false;
        if (!$config) return false;
        return $config;
    }

    // Возвращает готовые пункты меню, подставляя все нужные значения
    protected static function getMenuItems($items, $data)
    {
        $result = [];
        foreach ($items as $item) {

            if (is_array($item)) {

                $url = $item['url'];
                $nit = [];
                foreach ($url as $it) {
                    $nit[] = self::generateByTemplate($it, $data);
                };
                $item['url'] = Url::to($nit);

            }
            $result[] = $item;
        }
        return $result;
    }

    // Возвращает в формате меню список обратных связей на объект
    public static function getBLinks($data)
    {
        $config = get_called_class()::config();
        $list = isset($config['blinks']) ? $config['blinks'] : false;
        if (!$list) return false;

        return get_called_class()::getMenuItems($list, $data);
    }


    // Возвращает шаблон прямой ссылки, сответствующую атрибуту. Если для атрибута для ссылки нет, то возврашает false
    public static function getFLinkTemplate($attrName)
    {
        $result = false;


        $mdata = get_called_class()::getMeta($attrName);
        if (isset($mdata['model']) && isset($mdata['link']) && (boolean)$mdata['link']) {

            $result = Inflector::camel2id($mdata['model']) . '/update/%' . $attrName . '%';
        }
        return $result;
    }

    // Возвращает в формате меню список прямых связей на объект
    public static function getFLinks($data)
    {
        $class = get_called_class();
        $config = $class::config();
        $attrs = $class::attrMeta();
        $list = [];
        foreach ($attrs as $attrName => $mdata) {
            $url = $class::getFLinkTemplate($attrName);
            if (!$url) continue;
            $list[] = ['label' => $mdata['label'], 'url' => [$url]];
        }

        //var_dump($data); exit;
        return $class::getMenuItems($list, $data);
    }

    // Возвращает меню для списка. Метод может быть переопределен. По умолчанию загружается из файла.
    // Это массив вида
    //	['label' => 'Заказы', 'url' => Url::to(['/order?OrderSearch[address_id]=%id%'])],
    //	['label' => 'Изменить', 'url' => Url::to(['address/update/%id%'])],
    public static function getListViewMenu($data)
    {
        $config = get_called_class()::config();

        $result = get_called_class()::getBLinks($data); // обратные ссылки

        $list = isset($config['listViewMenu']) ? $config['listViewMenu'] : false;


        if ($list) {
            $list = get_called_class()::getMenuItems($list, $data);
            if ($result) {
                $result[] = '<li class="divider"></li>';
                $result = array_merge($result, $list);
            } else $result = $list;
        }
        return $result;
    }

    public static function getObjectMenu($data)
    {
        $config = get_called_class()::config();
        $result = get_called_class()::getBLinks($data); // обратные ссылки

        $list = get_called_class()::getFLinks($data);
        if ($list) {
            if ($result) {
                $result[] = '<li class="divider"></li>';
                $result = array_merge($result, $list);
            } else $result = $list;
        }

        return $result;
    }


    // Возвращает условия из мета-описания(подставляем в генератор запросов yii) для указанного атрибута, если оно существует
    // условие вида [user_id => owner_id] преобразуется в массив. В качестве значения могут быть использованы атрибуты или константы модели
    public function getConditionFor($attrName)
    {

        $condition = get_called_class()::getMeta($attrName, 'condition');

        $result = NULL;
        if (preg_match('/.*\[\s*(.+)\s*\].*/', $condition, $result) > 0) {
            $condition = $result[1];
            $condition = explode('=>', $condition);
            if (count($condition) > 0) {
                $value = $this->__get($condition[1]); // Возьмем значение
                if ($value == NULL) { // Посмотрим среди констант
                    $value = ArrayHelper::getValue((new \ReflectionClass(static::class))->getConstants(), $condition[1], NULL);
                }

                $result = [$condition[0] => $value];
            }

            return $result; //получаем налогично, например, ['user_id' => $this -> owner_id]
        }

    }


    // справочники атрибутов - список возможных значений для атрибута (из AttrLists)

    public static function getList($attrName)
    {
        $result = NULL;
        $class = get_called_class();
        $attrName = $class::getViewAttrName($attrName);
        $lists = $class::attrLists();
        if (array_key_exists($attrName, $lists)) $result = $lists[$attrName];
        return $result;
    }


    // Возвращает мета-данные для атрибута $attrName по имени $name. Если $name = '' то возвращается весь массив; если $attrName = '', то возвращаются мета-данные для класса
    public static function getMeta($attrName = '', $name = '')
    {
        $result = NULL;
        $attrName = strtolower($attrName);
        $class = get_called_class();
        $attrName = $class::getViewAttrName($attrName);
        $AttrMeta = $class::AttrMeta();

        if ($attrName == '') {
            $result = $class::Meta();
        } elseif (array_key_exists($attrName, $AttrMeta)) {
            $result = $AttrMeta[$attrName];
        }

        if ($result && $name > '') {
            if (array_key_exists($name, $result)) {
                $result = $result[$name];
            } else $result = false;
        }
        return $result;
    }


    public function attributeLabels()
    {

        //$list = get_called_class()::attrMeta();
        $list = $this->attrMeta();
        $result = [];
        foreach ($list as $attrName => $data) {
            $value = $data['label'];
            $result[$attrName] = $value;
            $result[$attrName . '_view'] = $value;
        }


        return $result;

    }


    // Возвращает список объектов текущего класса в формате id=>caption.
    // Возможно задать шаблон для генерации caption, если не задан, то используется заданный для данного класса шаблон
    // Если $condition указан, то кэширование не производится
    public static function listAll($template = false, $condition = false, $orderBy = false)
    {
        $result = false;
        if (!$template) $template = get_called_class()::captionTemplate();
        $cache = Yii::$app->cache;
        $cached = $cache && !$condition && !$orderBy;
        $cached = false;
        $key = '';
        /*
        if ($orderBy) {
            foreach($orderBy as $field => $dir) {
                $key = $key.'_'.$field.'.'.($dir==SORT_ASC?'asc':'desc');
            };
            $key = '.'.$key;
        };
        */
        //$key = get_called_class().'.'.$template.'.list'.$key;
        //if ($cache) $result = $condition?false:$cache->get($key);


        if ($cached) {
            $result = $cache->getList(get_called_class(), $template);
        }

        if (($result === false) || (!is_array($result))) {

            $query = static::find();
            if ($condition) $query = $query->andWhere($condition);
            if ($orderBy && is_array($orderBy)) $query = $query->orderBy($orderBy);
            $list = $query->all();

            //$list = static::findAll($condition);

            $result = [];
            foreach ($list as $item) {
                //var_dump($item -> getCaption($template)); exit;
                $result[$item->id] = $item->getCaption($template);

            }

//			if (!$condition && !$orderBy && $cache) $cache->set($key, $result, $cache -> listDuration);
            if ($cached) {
                $cache->setList($result, get_called_class(), $template, $result);
            }


        };
        return $result;

    }



    // Возвращает значение списочного значения атрибута в виде строки, элемент каждого из которых представляется через шаблон, для разделения элементов исползуется $delimiter
    // Списочное значение (массив) необходимо передать аргументо $list
    // $attrName требуется только в случае, если имя шаблона передается в виде алиаса (с префксом @)
    public function getListValue($list, $template = false, $attrName = false, $delimiter = ' ')
    {

        if (!$template) return $list;
        if ($attrName && (strlen($template) > 1) && ($template[0] == '@')) {
            $name = substr($template, 1);
            $template = $this->getMeta($attrName, $name);
        };

        $result = [];
        foreach ($list->all() as $item) {
            $result[] = $item->getCaption($template);
        }
        $result = implode($delimiter, $result);
        return $result;
    }


    // Метод, приводящий данные к согласованию. (вызывается перед сохранением)
    // Метод автоматически (при сооответствующей настройке) вызываетя перед сохранением.
    // Но можно вызывать и принудительно, если требуется согласовать данные перед сохранением,
    // например, чтобы показать пользователю в форме до того, как он ее сохранит.
    // Методы согласования описываются в потомке (EntityExt). Формат имени должен быть таким function integrate[Имя модели](), например function integrateOrder() - метод согласования модели Order
    // Этот метод работает срузу для всех классов, то есть автоматически определяет (по мета-данным модели), какой текущий класс и вызывает соответствующий метод согласования.
    // То есть переопределять его в потомках не надо.
    // Параметр $saving говорит о том, что после согласования объект достоверно будет сохранен.
    // То есть в этом случае в процедуре согласования могут быть произведены согласования подчиненных объектов, например, при добавлении услуги в заказ -
    // изменяется сумма заказа с сохранением изменений. Внешние согласования реализовываются в отдельной проседуре с постфиксом Ext. Например, function integrateOrderExt() - метод внешнего согласования модели Order
    // Если методы согласования не описаны, то они игнорируются, ни вызова ни исключения ни происходит
    public function integrate($saving = false)
    {
        $mdata = $this->meta();

        $class = ArrayHelper::getValue($mdata, 'model', NULL);
        if ($class === NULL) return false;
        $methodName = 'integrate' . $class;
        if (!method_exists($this, $methodName)) return false;
        $this->{'integrate' . $class}(); // Вызов внутренних согласований
    }

    // Вызов этой процедуры для согласования данных перед удалением объекта
    // $real - реальное удаление. Если  $real = false - установлка enabled в false
    public function integrateDel($real = true)
    {

        $mdata = $this->meta();
        $class = ArrayHelper::getValue($mdata, 'model', NULL);
        if ($class === NULL) return false;
        $methodName = 'integrate' . $class . 'Del';
        if (!method_exists($this, $methodName)) return true;
        return $this->{$methodName}($real);

    }


    // внешние согласования (после сохранения)
    public function integrateExt()
    {

        $mdata = $this->meta();
        $class = ArrayHelper::getValue($mdata, 'model', NULL);
        if ($class === NULL) return false;
        if (!method_exists($this, 'integrate' . $class . 'Ext')) return false;
        $this->{'integrate' . $class . 'Ext'}(); // Вызов внешних согласований
    }


    public function beforeDelete()
    {

        if (!parent::beforeDelete()) {
            return false;
        }
        $result = true;
        if ($this->autoIntegrate) $result = $this->integrateDel();
        return $result;
    }


    // восстановить объект, просто enabled = 1;
    public function restore()
    {
        if (array_key_exists('enabled', $this->attributes)) {
            $this->enabled = 1;
            $this->save();
        }
    }


    /**
     * Returns the list of attribute names.
     * By default, this method returns all public non-static properties of the class.
     * You may override this method to change the default behavior.
     * @return array list of attribute names.
     */
    public function attributes()
    {
        $result = parent::attributes();
        if ($this->parent()) {
            $class = $this->parent();
            $result = self::toList($result, $class->attributes());
        }

        return $result;
    }


    /**
     * Sets the attribute values in a massive way.
     * @param array $values attribute values (name => value) to be assigned to the model.
     * @param bool $safeOnly whether the assignments should only be done to the safe attributes.
     * A safe attribute is one that is associated with a validation rule in the current [[scenario]].
     * @see safeAttributes()
     * @see attributes()
     */
    public function setAttributes($values, $safeOnly = true)
    {
        if ($this->parent()) {
            $this->parent()->setAttributes($values, $safeOnly);
        }
        parent::setAttributes($values, $safeOnly);

    }

    /*
        public function canGetProperty($name, $checkVars = true, $checkBehaviors = true)
        {
            if ($name == 'option_list') return false;
            return true;
        }

        public function canSetProperty($name, $checkVars = true, $checkBehaviors = true)
        {
            if ($name == 'option_list') return false;

            $fields = get_object_vars($this);
            return (in_array($name, parent::attributes()) //свой атрибут
                    || array_key_exists($name, $fields)  // свое поле
                    || (!$this -> parent() || $this -> parent() -> canSetProperty($name, $checkVars, $checkBehaviors))
                    ) ?
                true : parent::canSetProperty($name, $checkVars, $checkBehaviors);
        }
    */

    public function __set($name, $value)
    {
        //Yii::info($name, 'entity');

        if (strtolower(substr($name, -9)) == '_as_array') {
            $name = substr($name, 0, -9);
            return $this->setValueAsArray($name, $value);
        }

        // атрибуты для ManyToMany имеют больший приоритет, чем атрибуты модели
        $manyToMany = $this->getBehavior('ManyToMany');
        if ($manyToMany && $manyToMany->canSetProperty($name)) {
            $manyToMany->__set($name, $value);
            return true;
        }


        if (in_array($name, parent::attributes())) {

            $type = $this->getMeta($name, 'type');
            if (($type == 'time') || ($type == 'datetime') || ($type == 'date')) {
                if (gettype($value) == 'string') { // форматируем дату-время под формат mysql
                    $d = strtotime($value);
                } else $d = $value;
                $value = ($d === false) ? NULL : date('Y-m-d H:i:s', $d);
            };
            parent::__set($name, $value); // свой атрибут
            return true;
        } else {

            // поищем среди своих полей
            $fields = get_object_vars($this);
            if (array_key_exists($name, $fields)) { // свое поле
                //Yii::info($name, 'entity');
                parent::__set($name, $value);
                return true;
            } else {
                // посмотрим среди behavior
                $this->ensureBehaviors();
                foreach ($this->GetBehaviors() as $behavior) {
                    if ($behavior->canSetProperty($name)) {
                        $behavior->__set($name, $value);
                        return true;
                    }
                }
                if ($this->parent()) {
                    $this->parent()->__set($name, $value); // отправляем атрибут родителю по данным
                    return true;
                }
            }
        }
    }

    public $counter = 0;

    public function __get($name)
    {

        $result = NULL;
        $originName = $name;

        if (strtolower(substr($name, -9)) == '_as_array') {
            $name = substr($name, 0, -9);
            return $this->getValueAsArray($name);
        }


        $result = $this->getValueAsModel($name); // подпихиваем объект по ссылке. Например, если есть атрибут user_id - ссылка на пользователя, то user вернет объект по с id = user_id
        if (!IS_NULL($result)) return $result;


        $manyToMany = $this->getBehavior('ManyToMany');
        if ($manyToMany && $manyToMany->canSetProperty($name)) {
            return $manyToMany->__get($name);
        }


        $name = $this->getViewAttrName($name);// срезаем _view, если есть


        if ($name == 'id') { // ID берем у самого старшего предка
            $result = $this->getId();
        } else if ($name == 'attributes') {
            $result = parent::__get($name);
        } else {


            if (in_array($name, parent::attributes())) {
                $result = parent::__get($name); // свой атрибут

                //if ($name=='to_attribute') {var_dump($result); echo "!<br>";}
            } else if (in_array($name . '_id', parent::attributes())) {
                $result = parent::__get($name . '_id'); // свой атрибут без _id

            } else {

                // поищем среди своих полей
                $fields = get_object_vars($this);
                if (array_key_exists($name, $fields)) {
                    $result = parent::__get($name);
                } else {
                    // посмотрим среди behavior
                    $this->ensureBehaviors();
                    foreach ($this->GetBehaviors() as $behavior) {
                        if ($behavior->canGetProperty($name)) {
                            //return $behavior-> __get($name);
                            return $behavior->$name;
                        }
                    }

                    // отправляем атрибут родителю по данным
                    if ($result = $this->parent()) $result = $this->parent()->__get($name);
                }
            }
        }


        if ($originName != $name) { // значит, это атриубут с постфиксом view - приводим к соответствующему виду

            $result = $this->getViewValue($name, $result);

        }


        if ($result) {
            if ((gettype($result) == 'string') && (int)date('y', strtotime($result)) < 0) { // костыль на пустые даты
                $result = '';
            } else {
                // преобразовываем значение в зависимости от формата
                $type = $this->getMeta($name, 'type');
                if (($type == 'time') || ($type == 'datetime') || ($type == 'date')) {
                    $format = $this->getMeta($name, 'format');
                    if (!$format) {
                        switch ($type) {
                            case 'date':
                                {
                                    $format = 'd.m.Y';
                                    break;
                                };
                            case 'datetime':
                                {
                                    $format = 'd.m.Y H:i:s';
                                    break;
                                };
                            case 'time':
                                {
                                    $format = 'H:i:s';
                                    break;
                                };
                        }
                    }

                    if (gettype($result) == 'string') {
                        $d = strtotime($result);
                    } else $d = $result;


                    $result = date($format, $d);
                }
            }
        } else $result = NULL;

        /*
                $this -> counter++;
                echo get_called_class().".".$name." = ";
                var_dump($result);
                echo " [".$this -> counter."]<br>";
        */
        return $result;

    }


    // Возвращает модель по ссылке в значении атрибута $name,  если это ссылка на другую модель
    // По умолчанию используется пространство имен текущего класса, но можно задать свое через $nameSapce, например, backend\models
    // Если ссылка должна , но
    protected function getValueAsModel($name, $nameSapce = false)
    {
        $result = NULL;
        $v = NULL;
        $m = '';

        if ($this->getMeta($name . '_id')) {

            //if (!$nameSapce) $nameSapce = StringHelper::dirname($this->className());

            $id = $this->__get($name . '_id');
            if ($id) {

                /*
                    if(array_key_exists($id, $this -> objects)) {
                        $result = $this -> objects[$id];
                    } else {
*/

                $cache = Yii::$app->cache;
                //$cache = false;
                if ($cache) {
                    $result = $cache->getObject($id);
                } else $result = false;

                //var_dump($result); exit;
                //$result = false;

                //$result = false; // отключение кэша для отладки (раскоментировать)
                if (!$result) {


                    $mdata = $this->getMeta($name . '_id');
                    /*
                    if (!array_key_exists('model', $mdata)) {
                        var_dump($this -> id); echo "<br><br>";
                        var_dump($this -> baseClassName()); echo "<br><br>";
                        var_dump($name); echo "<br><br>";
                        var_dump($mdata);
                        exit;
                    };
                    */
                    $m = $mdata['model'];
                    if ($m == 'CObject') { // полно работает пока нет полноценной структуры наследования моделей
                        $result = $this->byId($id);
                    } else {

                        $class = '\\' . self::ns() . $m;
                        $result = $class::findOne($id);
                        if (is_null($result)) $result = false; // Если модель должна быть, но не найдена, то false
                    }
                    //$cache->set($key, $result, $cache -> listDuration);
                    if ($cache) {
                        $cache->setObject($result);
                    }
                };
//						$this -> objects[$id] = &$result;
//					}
            }
        }

        return $result;
    }

    // Для структурированных, но хранимых в строковом виде атрибутов (например, json) этот метод возвращает значение атрибута в виде ассоциативного массива
    public function getValueAsArray($name)
    {
        $value = $this->__get($name);
        $type = $this->getMeta($name, 'type');


        switch ($type) {
            case 'json':
                {
                    $value = json_decode($value, true);
                    break;
                }
            case 'set':
                {
                    $value = explode(',', $value);
                    break;
                }
        }

        return $value;
    }

    // Для структурированных, но хранимых в строковом виде атрибутов (например, json) этот метод устанавливает значение атрибута, преобразуя массив в строку
    protected function setValueAsArray($name, $value)
    {

        $type = $this->getMeta($name, 'type');
        /*
        if ($name == 'delivery_address') {
            var_dump($value); exit;
        };
        */
        switch ($type) {
            case 'json':
                {
                    $value = json_encode($value, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                    break;
                }
            case 'set':
                {
                    $value = $value > '' ? implode(',', $value) : '';
                    break;
                }
        }

        return $this->__set($name, $value);
    }

    // получить человеческий вид значения атрибута (имя атрибута без постфикса)
    public function getViewValue($attrName, $value)
    {

        if (strtolower(substr($attrName, -3)) != '_id') {
            if (in_array($attrName . '_id', $this->attributes())) { // возможно, это ссылка без _id - попробуем.
                $value = $this->__get($attrName . '_id');
                $result = $this->getViewValue($attrName . '_id', $value);
                return $result;
            }
        }

        $result = $value;


        $class = get_called_class();
        $meta = $class::getMeta($attrName);
        $type = $class::getMeta($attrName, 'type');
        $format = $class::getMeta($attrName, 'format');

        if ($format == 'currency') {
            $result = Yii::$app->formatter->asCurrency($result);
        } else {
            switch ($type) {
                case 'ref':
                    {


                        $cache = Yii::$app->cache;

                        if ($cache) {
                            $obj = $cache->getObject($value);
                        };
//						echo ($obj?$obj -> id:'null'); echo "<br><br>";
                        if ($obj === false) {
                            $model = '\\' . $class::ns() . $class::getMeta($attrName, 'model');
                            $obj = $model::open($value);
                            $cache->setObject($obj);
                        };
//						var_dump($obj -> id);// exit;

                        $result = $obj ? $obj->getCaption() : '';


                        /*
                        $key = $this -> id.'-'.$attrName;
                        $cache = Yii::$app->cache;
                        $result=$cache->get($key);
                        $result = false;// кэш отключен (раскоментировано)
                        if (!$result) {
                            $model = '\\'.$class::ns().$class::getMeta($attrName,'model');
                            $obj = $model::open($value);
                            $result = $obj? $obj -> getCaption():'';
                            $cache->set($key, $result, 100);
                        };
                        */
                        break;
                    };
                case 'date':
                    ;
                case 'time':
                    ;
                case 'datetime':
                    {

                        if ($result && $format) {
                            $result = strtotime($result);
                            $result = date($format, $result);

                        }
                    }
            }
        }
        return $result;
    }


    public function save($runValidation = false, $attributeNames = null, $internal = false, $save_history = true)
    {
        $result = true;
        if ($this->autoIntegrate) $this->integrate();
        $this->lastDirtyAttributes = $this->getDirtyAttributes();

        if (Yii:: $app->id != 'app-console') {
            $this->_updateImages(); // для консольных приложений не запускать
        }


        if (!$internal && ($this->tablename != $this->tableName())) {
            $this->tablename = $this->tableName();
        }


        $parent = $this->parent();
        if ($parent) {

            $result = $parent->save($runValidation, $attributeNames, true);

            $id = $parent->id;
            parent::__set('id', $id);
        }

        $statusFN = $this->statusFieldName();

        if ($statusFN && $this->isAttributeChanged($statusFN)) { // Статус изменен, сохраним в истории
            $oldStatus_id = array_key_exists($statusFN, $this->getOldAttributes()) ? $this->getOldAttributes()[$statusFN] : NULL;
            $status_id = $this->status_id;
            $clName = $this->statusClassName();
            // Сохраним дату изменения, еслие существуте поле status_changed_at
            if (in_array('status_changed_at', $this->attributes())) {
                $this->status_changed_at = self::dbNow();
            }

//Yii::info($clName, 'entity');
//Yii::info($oldStatus_id, 'entity');
//Yii::info($status_id, 'entity');
//Yii::info($clName && ($oldStatus_id || $status_id), 'entity');

            if ($save_history && $clName && ($oldStatus_id || $status_id)) $clName::saveToHistory($oldStatus_id, $status_id, $this);
        }
        $result = $result && parent::save($runValidation, $attributeNames);

        if ($this->autoIntegrate) $this->integrateExt();

        if (!$internal) {
            // Если сохраняемый объект есть в кеше, то обновим его
            $cache = Yii::$app->cache;
            if ($cache) $cache->saveObject($this);

        }
        return $result;

    }

    public function defSave($validation = true)
    {
        return parent::save($validation);
    }

    public static function open($id)
    {
        return static::findOne($id);
    }

    public static function findOne($condition)
    {
        /*
                $result = null;
                $id = false;


                $cache = Yii::$app->cache;
                $counter = $cache -> get('counter') + 1;
                $cache -> set('counter', $counter);
                echo '<br>'.$counter.'<br>';


                if ($counter > 5) {
                    $cache -> set('counter', 0);
                    exit;
                }

                if(is_array($condition)) {
                    if (count($condition) == 1) {
                        if (array_key_exists('id', $condition)) $id = (integer)$condition['id'];
                        if (array_key_exists(0, $condition)) $id = (integer)$condition[0];

                    }
                } else $id = $condition;

                        if ($id) { // Попытка получить объект из базы - тогда проверим, есть ли он в кеше


                                $cache = Yii::$app->cache;
                                //$cache = false;
                                if ($cache) {
                                    $result = $cache -> getObject($id);

                                };


                        }




                if (is_null($result)) {


                        $result = parent::findOne($condition);

                        if ($result) {
                            $cache = Yii::$app->cache;
                            $cache = false;
                            if ($cache) {
                                $result = $cache -> setObject($result, $result -> id);
                            };
                        }
                }
            */
        $result = parent::findOne($condition);
        return $result;
    }


    /*
    public function loadConfig() {

        $config = include(Yii::getAlias('@common').'/config/models.php');
        $cn = StringHelper::basename(self::className());
        $config = isset($config[$cn])?$config[$cn]:false;
        if (!$config) return;
        $this -> listViewMenu = isset($config['listViewMenu'])?$config['listViewMenu'];



    }
    */

    public function getArrayValue($attrName, $name)
    {
        $arr = $this->__get($attrName . '_as_array');
        return ArrayHelper::getValue($arr, $name);
    }

    public function setArrayValue($attrName, $name, $value, $save = false)
    {
        $arr = $this->__get($attrName . '_as_array');
        ArrayHelper::setValue($arr, $name, $value);
        $this->__set($attrName . '_as_array', $arr);
        if ($save) $this->save();
    }

    // id текущего пользователя
    public static function currentUser()
    {
        if (Yii::$app instanceof yii\console\Application) {
            return null;
        } else {
            return Yii::$app->user->id;
        }
    }


    // property currentUser
    public static function getCurrentUser()
    {
        return self::byId(Yii::$app->user->id);
    }


    public function init()
    {

        parent::init();


        // проставляем значения по умолчанию

        foreach (parent::attributes() as $name) {
            if ($name == 'id') {
                continue;
            }
            $value = $this->__get($name);
            if (($value !== NULL)) continue;
            $nvalue = $value;
            $val = $this->getMeta($name, 'default');

            if ($val !== NULL) {
                if (strtolower($val) == 'currentuser()') // функция текущего пользователя
                {
                    $nvalue = $this->currentUser();
                } elseif ((substr($val, 0, 1) == '{') && (substr($val, -1) == '}'))  // выражение
                {
                    $val = BaseJson::decode($val);
                    if (isset($val['expression'])) {
                        $expression = new Expression($val['expression']);
                        if ($expression) {
                            $nvalue = (new \yii\db\Query)->select($expression)->scalar();
                        };
                    }
                } else { // конкретное  значение
                    $nvalue = $val;
                }
            }


            if ($name == $this->statusFieldName()) {
                $nvalue = ($this->statusClassName())::getDefault($this);
                if ($nvalue) $nvalue = $nvalue->id;
            }

            //if ($value != $nvalue) $this -> __set($name, $nvalue -> id);
            if ($value != $nvalue) $this->__set($name, $nvalue);
        }


    }


    public function getCaption($template = false)
    {

        if (!$template) $template = $this->captionTemplate();


        $cache = Yii::$app->cache;

        if ($cache) {
            $result = $cache->getObjectCaption($this, $template);
        };


        if ($result === false) {
            //$attributes = $this -> attributes;
            $vars = EntityHelper::getTemplateVars($template);

            if ($vars) {
                $attrList = [];
                foreach ($vars as $varName) {
                    $attrList[$varName] = $this->__get($varName);
                }

                $result = $this->generateByTemplate($template, $attrList);

            } else $result = $template;
            if ($cache) {
                $cache->setObjectCaption($result, $this, $template);
            }
        }


        return $result;

    }


    // Возвращает шаблон в виде списка строк и полей через запятую, например для concat
    public static function getCaptionTemplateAsStringList()
    {
        $templ = get_called_class()::captionTemplate();

        $fl = 0;
        $result = '';
        for ($i = 0; $i < strlen($templ); $i++) {
            $ch = $templ[$i];
            if ($ch == '%') {
                $fl = (~$fl) & 1;
                if (($i != 0) && ($i != strlen($templ) - 1))
                    $result = $result . ($fl ? "'," : ",'");
            } else {
                if ($i == 0) $ch = "'" . $ch;
                if ($i == strlen($templ) - 1) $ch = $ch . "'";
                $result = $result . $ch;
            }
        }


        return $result;
    }

    // Возвращает ассоциативный массив объектов из массива прикладных объектов $objects, id -> title, где title сгенерирован шаблоном $template
    public static function objectsByTemplate($objects, $template = false)
    {
        $result = [];
        if ($objects && is_array($objects)) {
            foreach ($objects as $object) {
                $result[$object->id] = $object->getCaption($template);
            };
        };
        return $result;
    }


    //TODO: remove to Helper
    // the text generating by template and parameters
    // $currentClass = true - тогда учитываются особенностей полей текущего класса
    public static function generateByTemplate($template, $params, $currentClass = true)
    {
        $result = $template;
        foreach ($params as $name => $value) {

            if (!is_array($value)) {
                if ($currentClass) {
                    $format = self::getMeta($name, 'format');
                    if ($format == 'icon') {
                        $value = self::getIcon($name, $value);
                    }
                }
            }

            if (is_array($value)) {
                $value = implode(',', $value);
            }


            $result = str_replace('%' . $name . '%', $value, $result);
        };
        return trim($result);
    }


    public static function findByCaption($value)
    {
//TODO:
        preg_match_all('(%([-\w]+)%)', $templ, $attrList);
        $fields = $attrList[1];
        $cond = ['or'];
        foreach ($fields as $field) {
            $cond[] = array('like', $field, $this->owner_id);
        }
    }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

    /*
    public function __call($name, $params)	{
        //echo $name;

        if (method_exists($this, $name)){
            $rclass = new \ReflectionClass(get_class($this));
            $method = $rclass -> getMethod($name);
            return $method -> invoke($this, $params);
        } else {
            if ($this -> _parent) {
                $result = $this -> _parent -> __call($name, $params);
            } else throw new Exception ('Метод '.$name.' у '.get_class($this).' не найден');
        }
        return $result;
    }

    */

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public function __construct($config = [])
    {
        parent::__construct($config);
        /*
        $parentClass = get_parent_class($this);
        if ($parentClass != $this -> rootClassName) { // Если дальше еше предки есть
            $this -> _parent = new $parentClass();
        }

        */

    }


    // Ищет объект с атрибутами из массива $attrs, значения из которых должны быть равны как в $data
    // Если находится, то этот объект обновляется данными из $data
    // Если не находится и $create = true, то создается объект и инициализируется данными из $data
    // Если $attrs не указано, то ищется объект полностью соответствующий $data
    public static function addBy($data, $attrs = false, $create = true)
    {

        if ($attrs) $condition = self::extractParams($data, $attrs);
        else $condition = $data;

        $result = self::findOne($condition);

        if (!$result) {
            if (!$create) return NULL;
            $className = get_called_class();
            $result = new $className();

        }

        $result->load($data, '');

        $result->save();

        return $result;
    }


// TODO: отработать ситуцацию $condition = ['id' = <xxxx>]

    protected static function findByCondition($condition)
    {

        $query = static::find();

        if (!ArrayHelper::isAssociative($condition)) {
            // query by primary key
            $primaryKey = static::primaryKey();
            if (isset($primaryKey[0])) {
                $className = get_called_class();
                $tableName = $className::tableName();
                $condition = [$tableName . '.' . $primaryKey[0] => $condition];

            } else {
                throw new InvalidConfigException('"' . get_called_class() . '" must have a primary key.');
            }
        }

        $result = $query->andWhere($condition);


        $tables = self::getTables();

        $attr = self::getFields();


//		if ((array_key_exists('id', $attr)) AND (count($tables) > 1)) {

//		}

        $result->select($attr);

        $result->from($tables);
        $where = [];
        if (count($tables) > 1) {
            $firstId = $tables[0] . '.id';
            for ($i = 1; $i < count($tables); $i++) {
                $where[] = $firstId . '=' . $tables[$i] . '.id';

//				$where[$firstId] = '`'.$tables[$i].'.id`';
            }

        }

        foreach ($where as $w) {
            $result->andWhere($w);
        }

//if ($condition) {var_dump($condition); var_dump($result -> where); exit;}
        return $result;
    }




// Возвращаем список таблиц, задействованных в запросе по цепочке наследования
// 
    public static function getTables($tableName = false)
    {
        $result = [];
        if (!$tableName) {
            $className = get_called_class();
            $tableName = $className::tableName();
        }
        if ($tableName) {
            $mdata = self::getTableMetaData($tableName);
            if (array_key_exists('parent', $mdata)) {
                $pTableName = static::getDb()->tablePrefix . strtolower($mdata['parent']);
                $result = self::getTables($pTableName);
            }
            $result[] = $tableName;
        }

        return $result;
    }


//Возвращает полные наименования полей, соответствующие текущей модели //TODO: проверить!
    public static function getFields()
    {

        $result = [];

        $className = get_called_class();
        $tableName = $className::tableName();

        $mdata = self::getTableMetaData($tableName);
        if (array_key_exists('parent', $mdata)) {
            $pTableName = static::getDb()->tablePrefix . strtolower($mdata['parent']);
            $result = static::getTableFields($pTableName);
        }


        $result = ArrayHelper::merge($result, static::getTableFields($tableName));


        return $result;
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        if ($this->parent()) $this->parent()->loadDefaultValues($skipIfSet);
        return parent::loadDefaultValues($skipIfSet);
    }


    // Если у модели есть свойство enabled и указано $force = false, то объект физически не удаляется а устанавливается enabled = 0
    public function delete($force = true)
    {
        $result = true;

        if (!$force || array_key_exists('enabled', $this->attributes)) {


            if ($this->integrateDel(false)) {
                $this->enabled = 0;
                $this->save();
                //if ($this -> id= 1992) {echo 'del from entity'; var_dump($this -> enabled);};
            };
        } else { //TODO: Весь процесс удаления необходимо завернуть в транзакцию!
            $result = parent::delete();
            if ($this->parent) $result = $this->parent->delete();
            $result = $result && parent::delete();
        }
        return $result;
    }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function getTableFields($tableName, $full = true)
    {

        $result = false;

        $tableSchema = static::getDb()
            ->getSchema()
            ->getTableSchema($tableName);

        if ($tableSchema) {

            $fields = array_keys($tableSchema->columns);

            if ($full) {
                foreach ($fields as $name) {
                    $result[$name] = $tableName . '.' . $name;
                }
            }
        }

        return $result;
    }


// Вурнуть мета-данные таблицы (из комментария)
    public static function getTableMetaData($tableName)
    {

        $comment = Yii::$app->db->createCommand(
            'SELECT table_comment
			FROM information_schema.tables
			WHERE table_name = :tname'
        )->bindValue(':tname', $tableName)
            ->queryScalar();

        $data = array();
        $lines = explode(";", $comment);
        foreach ($lines as $line) {
            $arr = explode(":", $line);
            if (count($arr) > 1) $data[strtolower(trim(strtolower($arr[0])))] = trim($arr[1]);
        }
        return $data;

    }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Получиь текущее время базы данных
    public static function dbNow($format = false)
    {
        $result = (new \yii\db\Query)->select('NOW()')->scalar();
        if ($format) {

            $result = date($format, strtotime($result));
        }
        return $result;
    }


// получить объединение множеств элементов из двух массивов
    public static function toList($a, $b)
    {
        $result = [];

        foreach ($a as $v) {
            if (!in_array($v, $result)) $result[] = $v;
        }

        foreach ($b as $v) {
            if (!in_array($v, $result)) $result[] = $v;
        }
        return $result;

    }

// из массива $data отбирает только те свойства, имена которых присутствуют в $names
    public static function extractParams($data, $names)
    {
        $result = [];
        foreach ($names as $name) {
            if (array_key_exists($name, $data)) {
                $result[$name] = $data[$name];
            }
        }


        return $result;
    }


// Возвращает статус по алиасу в рамках текущей модели
    public static function status($alias)
    {
        $className = self::statusClassName();
        return $className::findOne(['alias' => $alias, 'table' => get_called_class()::tableName()]);
    }

// Возвращает список возможных статусов модели, если у модели существует поле Статуса
    public function getStatusList($template = false)
    {

        $className = self::statusClassName();
        $status = $className::findOne($this->__get(self::statusFieldName()));

        if ($status) {
            $list = $status->getTo()->all();
            $list[] = $status; // добавляем текущее знаечение
        } else {
            $list = [$className::getDefault($this)];
        }
        $result = [];
        foreach ($list as $item) {
            if (!$item) continue;
            $result[$item->id] = $item->getCaption($template);
        }

        return $result;
    }

//Полный аналог getListValues, но не статический метод для возможности создания зависимых списков (в контексте объекта) с выставлением условия

    public function getRelatedListValues($name, $template = false, $condition = false, $required = null)
    {

        $rc = $this->getConditionFor($name);

        if (is_array($rc)) {
            if (is_array($condition)) {
                $condition = ArrayHelper::merge($rc, $condition);
            } else {
                $condition = $rc;
            }
        }


        $modelName = self::ns() . $this->getMeta($name, 'model');
        $list = $modelName::listAll($template, $condition);

        if ((is_null($required) && (self::getMeta($name, 'required') == 0)) || ($required === false))
            $list = ArrayHelper::merge(['' => ''], $list);  // Если поле необязательное, то добавляем сверху пустой вариант
        return $list;
    }


// Возвращает список всех возможных значений атрибута $name в виде массива id => caption
    public static function getListValues($name, $template = false, $required = null)
    {
        $modelName = self::ns() . self::getMeta($name, 'model');
        $list = $modelName::listAll($template);

//	echo $template.'<br><br>';var_dump($list); 


        if ((is_null($required) && (self::getMeta($name, 'required') == 0)) || ($required === false))
            $list = ArrayHelper::merge(['' => ''], $list);  // Если поле необязательное, то добавляем сверху пустой вариант
        return $list;
    }

// Возвращает список существующих значений поля $name. Если значение является ссылкой то создается массив ключ => значение
    public static function getExistsValues($name, $template = false, $empty = false)
    {
        $list = self::find()->select($name)->distinct()->all();

        $result = [];
        if ($empty) $result[0] = '';
        $method = false;
        $first = true;
        foreach ($list as $item) {

            if ($first) {
                //echo $name.' -> '.yii\helpers\Inflector::camelize($name);
                //exit;
                $mName = 'get' . Inflector::camelize(substr($name, 0, -3));
                if (method_exists($item, $mName)) { // преобразуем имя поле в соответствующий метод, например status_id => getStatus()
                    $method = $mName;
                };
            }
            $val = $item->__get($name);
            if ($method) {
                $r = call_user_func([$item, $method]);
                $obj = $r->one();
                if ($obj) $result[$val] = $obj->getCaption($template);
            } else {
                $result[] = $val;
            }
        }

        return $result;
    }
////////// images ////////////////////////////////////////////////////////////////

// Сохраняет поля формата images, скачивая и удаляя то, что надо
    protected function _updateImages()
    {

        if (!Yii::$app->request->isPost) return false;

        $model = $this;
        //$formName = ucfirst(Yii::$app->controller->id);
        $formName = $this->baseClassName();

        $params = Yii::$app->request->post();


        if (!isset($params[$formName])) return false;
        $params = $params[$formName];

        // Получим имена всех свойств формта images
        $attrList = [];
        $attrs = $model->attrMeta();
        foreach ($attrs as $name => $meta) {
            if (isset($meta['format']) && ($meta['format'] == 'images')) {
                $attrList[] = $name;
            }
        }


        $newFiles = $this->_uploadImages($attrList); // скачаем файлы, которые были добавлены


        foreach ($attrList as $attrName) {

            $attrFiles = ArrayHelper::getValue($newFiles, $attrName . '_files', []);
            $files = $model->__get($attrName . '_as_array');
            $fileList = [];

            if (!($files && is_array($files))) continue;
            foreach ($files as $file) {

                $item = [];
                $fileName = ArrayHelper::getValue($file, 'name');
                $title = ArrayHelper::getValue($file, 'title', '');

                $status = ArrayHelper::getValue($file, 'status', 'ok');
                if ($status == 'deleted') {
                    $this->_removeImage($fileName);
                    continue;
                };

                if ($status == 'new') {
                    $fileName = ArrayHelper::getValue($attrFiles, $fileName, false);
                }

                if (!$fileName) continue;
                $item['name'] = $fileName;
                $item['title'] = $title;
                $fileList[] = $item;
            }
            //echo "!!!"; var_dump($fileList); exit;
            //$value = json_encode($fileList, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            //$model -> __set($attrName, $value);
            //var_dump($fileList); exit;

            $model->__set($attrName . '_as_array', $fileList);


        }

        return true;
    }


// удалить файл изображения из хранилища
    public static function _removeImage($fileName)
    {
        if ($fileName) {
            $fn = FilesController::getPath('images', self::modelId()) . $fileName;
            $fn = self::getImage($fileName);
            if (file_exists($fn)) {
                unlink($fn);
            }
        }
    }


// Загружает изображения на сервер
// возвращает массив с закаченными файлами, с соответствиями старых и новых имен для каждого свойства формта images
    protected function _uploadImages($attrs)
    {


        $model = $this;
        if (Yii::$app->request->isPost) {
            $formName = $this->baseClassName();
            $attrList = [];
            foreach ($attrs as $name) {
                $attrList[] = $name . '_files';
            }

            if (count($attrList) > 0) {
                // Закачивание файлов
                $result = [];
                $name = Inflector::camel2id($formName);
                $path = FilesController::getPath('images', $name);

                if (!file_exists($path)) {
                    mkdir($path, 0775, true);
                }

                // пройдемся по каждому свойству и сохраним файлы
                foreach ($attrList as $attrName) {
                    $files = UploadedFile::getInstancesByName($formName . '[' . $attrName . ']');
                    $item = [];
                    foreach ($files as $file) {
                        $ext = $file->extension;
                        $filename = md5(uniqid(rand(), true)) . '.' . $ext;
                        $file->saveAs($path . $filename);
                        if ($file->hasError) {
                            throw new UploadException($file->error);
                        }

                        $item[$file->name] = $filename;
                    }
                    $result[$attrName] = $item;

                }
                return $result;
            }


        }

    }

    //Вырезать текст
    public static function cutText($string, $lenght = 0)
    {
        if (mb_strlen($string, 'UTF-8') > $lenght) {
            $content = str_replace('\n', '', mb_substr($string,
                0, $lenght, 'UTF-8'));
            return $content . '…';
        } else {
            return str_replace('\n', '', $string);
        }
    }


}