<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "adr_options".
 *
 * @property integer $id
 * @property string $icon
 * @property string $alias
 * @property string $stype
 * @property string $name
 * @property string $label
 * @property string $title
 * @property string $meta_description
 * @property string $h1
 * @property string $seo_text
 *
 * @property AddressOption[] $addressOptions
 * @property ServiceName[] $serviceNames
 */
class Option extends \common\models\EntityExt 
{
	
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_options'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [
			'stype' => ['Опция адреса' => 'Опция адреса', ],  

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Список опций адресов', 
			'imp' => 'Опция', 
			'rodp' => 'Опции', 
			'vinp' => 'Опцию', 
			'model' => 'Option', 
			'caption' => '%name%', 
			'parent' => 'objects', 
			'comment' => 'Список опций адресов. Дополнительные иконки могу быть добавлены системным администратором.', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', 'visible' => '0', ], 
			'icon' => ['label' => 'Иконка', 'directory' => 'options', 'format' => 'icon', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'alias' => ['label' => 'Псевдоним', 'comment' => 'Внутренне имя опции для привязки ее к участию в бизнес-логике', 'readonly' => '0', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'stype' => ['label' => 'Тип параметра', 'show' => '0', 'filter' => 'list', 'default' => 'Опция адреса', 'type' => 'enum', 'required' => '0', 'visible' => '0', ], 
			'name' => ['label' => 'Название параметра', 'filter' => 'like', 'type' => 'string', 'required' => '1', 'show' => '1', 'visible' => '1', ], 
			'label' => ['label' => 'Подпись для меню', 'type' => 'string', 'required' => '0', 'show' => '1', 'visible' => '1', ], 
			'title' => ['label' => 'Поле Title', 'show' => '1', 'visible' => '0', 'type' => 'string', 'required' => '0', ], 
			'meta_description' => ['label' => 'Поле Meta Description', 'show' => '1', 'visible' => '0', 'type' => 'string', 'required' => '0', ], 
			'h1' => ['label' => 'h1', 'visible' => '0', 'type' => 'string', 'required' => '0', 'show' => '1', ], 
			'seo_text' => ['label' => 'SEO Текст', 'show' => '1', 'visible' => '0', 'comment' => 'Для разбивки на колонки использовать <p>', 'type' => 'text', 'required' => '0', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
	
		return $result;
		
		
}
	

	// Создать объект по указанному псевдониму, если такой псевдоним существует.
	public static function byAlias($value) 
	{
		return self::findOne(['alias' => $value]);
	}







    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['stype', 'seo_text'], 'string'],
            [['name'], 'required'],
            [['icon'], 'string', 'max' => 50],
            [['alias'], 'string', 'max' => 45],
            [['name', 'label'], 'string', 'max' => 100],
            [['title', 'meta_description', 'h1'], 'string', 'max' => 255],
            [['alias'], 'unique'],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressOptions() {
        return $this->hasMany(AddressOption::className(), ['option_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceNames() {
        return $this->hasMany(ServiceName::className(), ['option_id' => 'id']);
    }



    public static function captionTemplate(){

        return "%name%";
    }





}