<?php
/*
*	Прикладной класс управления уведомлениями
*/

namespace common\models;


use ReflectionClass;
use yii;
use yii\helpers\ArrayHelper;
use common\models\EntityHelper;
use common\models\Notification;


class NotificationManagerExt extends NotificationManager
{
    const REGISTRATION = 'registration';    // Регистрация
    const PASSWORD_RECOVERY = 'password_recovery';    // Восстановление пароля
	const ORDER_FORMATION24 = 'order_formation24';    // 
	const ORDER_FORMATION24_SUBJECT = 'Заказ на формировании более 24 часов';    // 
	const ORDER_FORMATION3 = 'order_formation3';    // 
	const ORDER_FORMATION3_SUBJECT = 'Срочный заказ на формировании более 3 часов';    // 
    const NEW_REVIEW = 'new_review';    // Новый отзыв
    const ORDER_STATUS = 'order_status';    // Смена статуса заказа
    const ADDRESS_STATUS = 'address_status';    // Смена статуса адреса
	const FORCE_NOTIFICATIONS = [Self::REGISTRATION, Self::ORDER_FORMATION3, Self::ORDER_FORMATION24]; // События, которые отправляются в любом случае без пользовательских настроек на адрес пользователя (кроме администратора)
	
	
	
	public function notify($notification, $params) {
		ArrayHelper::setValue($params, 'common', EntityHelper::getCommonOptions()); // добавление общих параметров в набор параметров	
		
		return parent::notify($notification, $params);
	}

	// Отправляет сообщения администраторам
	protected function notifyAdmins($notification, $params) {
		// получаем список адресов, подписанные на указанное уведомление
		$nname = $notification.'_admin';				
		if (!$this -> templateExists($nname)) return false;
		$emails = false;
		$list = Notification::find() -> andWhere(['nname' => $nname, 'enabled' => 1]) -> all();
		if ($list) {
			foreach($list as $item) {
				$emails[] = $item -> email;
			}
		};

		if (!$emails) return false;
		$emails = implode(',', $emails);
		$params['email.to'] = $emails;		
		return $this -> notify($nname, $params);		
	}	
	
	// Отправляет сообщения Владельцу
	// Должен быть задан $params['email.to'] или определен $params['this.email']
	protected function notifyOwner($notification, $params) {
		$nname = $notification.'_owner';				
	
		if (!$this -> templateExists($nname)) return false;
		
		// Надо проверить, активировано уведомление или нет, заодно посмотреть email
		$n = Notification::find() -> andWhere(['nname' => $nname, 'enabled' => 1, 'user_id' => $params['this']['id']]) -> all();
		
		if ($n && is_array($n) && count($n)) {
			/*
			if (!isset($params['email']['to']) || !trim($params['email']['to'])) {
				if (isset($params['this']['email'])) $params['email']['to'] = $params['this']['email'];
			}
			*/			
			$params['email']['to'] = $n -> email;
			
			return $this -> notify($nname, $params);		
		} else {
			// Если пользователь не активный и вызывается уведомление о регистрации, то пропускаем, email берем из настроек пользователя
			
			if (in_array($notification, self::FORCE_NOTIFICATIONS)) {
				
				if (!isset($params['email']['to']) || !trim($params['email']['to'])) {
					$params['email']['to'] = $params['this']['email'];
				}
			}
			

			return $this -> notify($nname, $params);		
		}
	}
	
	// Отправляет сообщения Заказчику
	// Должен быть задан $params['email.to'] или определен $params['this.email']
	protected function notifyCustomer($notification, $params) {
		$nname = $notification.'_customer';		
		if (!$this -> templateExists($nname)) return false;
		
		// Надо проверить, активировано уведомление или нет, заодно посмотреть email
		$n = Notification::find() -> andWhere(['nname' => $nname, 'enabled' => 1, 'user_id' => $params['this']['id']]) -> all();
		if ($n) {
			/*
			if (!isset($params['email']['to']) || !trim($params['email']['to'])) {
				if (isset($params['this']['email'])) $params['email']['to'] = $params['this']['email'];
			}
			*/			
			$params['email']['to'] = $n -> email;
			
			return $this -> notify($nname, $params);		
		} else {
			// Если пользователь не активный и вызывается уведомление о регистрации, то пропускаем, email берем из настроек пользователя
			if (in_array($notification, self::FORCE_NOTIFICATIONS)) {
				$u = User::findOne([$params['this']['id']]);
				if ($u) {
					$params['email']['to'] = $u -> email;
				}
			}
			
			return $this -> notify($nname, $params);		
		}
	
	/*
		if (!isset($params['email']['to']) || !trim($params['email']['to'])) {
			if (isset($params['this']['email'])) $params['email']['to'] = $params['this']['email'];
		}
//		echo __method__; exit;
		return $this -> notify($nname, $params);		
	*/
	
	
	}
	
	// Проверяем существование шаблона для уведолмения по идентификатору
	protected static function templateExists($nname) {
		$fn = Yii::getAlias('@storage/templates/mail/default/' . $nname.'.php');	
		return file_exists($fn);
	}
	

	
	// точка входа - вызывается извне
    public function notifyChangeStatus($oldStatusId, $newStatusId, $model = null, $actorId = null)
    {
		if ($oldStatusId == $newStatusId) return false; // Если статус не изменился, то просто выходим
        $isNew = is_null($oldStatusId);
        $params = [];
        $class = '';
        if (!is_null($model)) {
            $class = $model->baseClassName();
            $params = $model->attributes;
        }
        $params['class'] = $class;
		$params = ['this' => $params];
        switch ($class) {
            case 'User' :
                {
                    if ($isNew) {
                        $this->notifyRegistration($params);
                    } else $this->notifyStatusCustom($newStatusId, $params);

                    break;
                };
            case 'Review' :
                {
					
                    if ($isNew) {
                        $this->notifyNewReview($params);
                    }
					
                    break;
                };
            case 'Address' || 'Order':
                {
                    $this->notifyStatusCustom($newStatusId, $params);
                    break;
                };
            default :
                {
                    $this->notifyStatus($oldStatusId, $newStatusId, $params);
                }

        }

    }

	
	// Отправляет уведомления по заказу. Используется для вызова извне
	// обязательно определеить тему при вызове.
	// xparams - включатся с приоритетом в общий массив параметров для генерации текста
	public function notifyOrder($notification, $order, $subject, $xparams = false) {		
        $class = $order->baseClassName();
        $params = $order->attributes;        		
		$params = ['this' => $params];
		$params = ArrayHelper::merge($params, $xparams);		
		$params['class'] = $class;
		$params['email']['subject'] = $subject;				
		return  $this->sendOrder($notification, $params);
	}
	
    public function getNotifySubject()
    {
        return [
            'Order' => Yii::$app->name . '. Изменение статуса заказа № {this.number} :*{newStatus.name}*',
            'Address' => Yii::$app->name . '. Изменение статуса адреса ({this.address}): *{newStatus.name}*',
            'User' => Yii::$app->name . '. Изменение статуса Владельца адреса: *{newStatus.name}*'
        ];
    }


    public function notifyStatusCustom($newStatus, $params, $types = false)
    {
        $class = $params['this']['class'];

		$notification = strtolower($class).'_'.Status::findOne($newStatus)->alias; // идентификатор события
        if ($newStatus) {
            $newStatus = Status::findOne($newStatus);
            $params['newStatus'] = $newStatus->attributes;
        }

        ArrayHelper::setValue($params, 'email.subject', ArrayHelper::getValue($this->getNotifySubject(), $class));

	//	if (in_array($class, ['Address', 'Order', 'User']) && !isset($params['id'])) return false;
		
        switch ($class) {
            case 'Address' :
                {					
                    $this->sendAddress($notification, $params);
                    break;
                };
            case 'Order' :
                {
                    $this->sendOrder($notification, $params);
                    break;
                };
            case 'User' :
                {
                    $this->sendUser($notification, $params);
                    break;
                };
        }
    }

    public function sendUser($notification, $params)
    {

		$id = $params['this']['id'];
        $r = Rcompany::findOne(['user_id' => $id]);
        if ($r) {
            $params['this']['company'] = $r->name;
        }		
		$this -> notifyAdmins($notification, $params);
		
		if ($params['this']['role'] == User::ROLE_OWNER) $this -> notifyOwner($notification, $params);		
		if ($params['this']['role'] == User::ROLE_CUSTOMER) $this -> notifyCustomer($notification, $params);

    }


    public function sendOrder($notification, $params)
    {

        $params['address'] = Address::findOne($params['this']['address_id']) -> attributes;
	
		$owner_id = $params['this']['owner_id'];
		$owner = User::findOne($owner_id);
		
		$customer_id = $params['this']['customer_id'];
		$customer = User::findOne($customer_id);
				
		$params['owner'] = $owner;
		$params['customer'] = $customer;				
		$this -> notifyAdmins($notification, $params);
		$params['email']['to'] = $owner -> email;		
		$this -> notifyOwner($notification, $params, $owner);
		$params['email']['to'] = $customer -> email;
		$this -> notifyCustomer($notification, $params, $customer);
	
    }


    public function sendAddress($notification, $params)
    {

        $owner_id = $params['this']['owner_id'];
		$owner = User::findOne($owner_id);
		
		$params['owner'] = $owner;		
		
		$this -> notifyAdmins($notification, $params);
		
		$params['email']['to'] = $owner -> email;
		$this -> notifyOwner($notification, $params, $owner);		
		
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
    public function notifyStatus($oldStatus, $newStatus, $params, $types = false)
    {
        $classToNotification = [
            'Status' => [self::ORDER_STATUS, Notification::NT_ORDER_STATUS],
        ];

        $class = $params['this.class'];
        if (array_key_exists($class, $classToNotification)) $n = $classToNotification[$class];
        else return false;

        $notifications = Notification::find()->where(['type' => $n[1], 'status_id' => $newStatus, 'enabled' => 1])->all();

        if ($oldStatus) {
            $oldStatus = Status::findOne($oldStatus);
            $params['oldStatus'] = $oldStatus->attributes;
        }

        if ($newStatus) {
            $newStatus = Status::findOne($newStatus);
            $params['newStatus'] = $newStatus->attributes;
        }

        $user = Entity::currentUser();
        if ($user) {
            $user = User::findOne($user);
            $params['actor'] = $user->attributes;
        }


        if ($notifications) {
            foreach ($notifications as $notification) {
                $to = $notification->email;
                ArrayHelper::setValue($params, 'email.to', $to);

                $this->notify($n[0], $params, $types);
            }
        }
    }

*/
    public function notifyRegistration($params, $types = false)
    {	

		$notification = self::REGISTRATION;
		$params['email']['subject'] = 'Регистрация на сайте VseAdresa.pro';
		$this -> notifyAdmins(self::REGISTRATION, $params);
		if ($params['this']['role'] == User::ROLE_CUSTOMER) {
			$this -> notifyCustomer(self::REGISTRATION, $params);
		};
		if ($params['this']['role'] == User::ROLE_OWNER) {
			$this -> notifyOwner(self::REGISTRATION, $params);
		};
		
		
		return true;
		

    }

    public function notifyNewReview($params, $types = false)
    {

        // пользователю
        /*
        $email = trim(User::portal() -> getOptions('notifications.newreview.email'));
        if ((boolean)$email) {;
            $this -> notify(self::NEW_REVIEW, ArrayHelper::merge(['from' => $app->params['infoEmail'], 'to' => $email], $user), $types);
        }
        */
		
		$params['email']['subject'] = 'Новый отзыв на сайте VseAdresa.pro';
        // администратору
		
		return $this -> notifyAdmins(self::NEW_REVIEW, $params);
		/*
        if (User::portal()->getOptions('notifications.newreview.enabled')) {
            $to = trim(User::portal()->getOptions('notifications.newreview.email'));

            ArrayHelper::setValue($params, 'email.to', $to);
			ArrayHelper::setValue($params, 'email.subject', "Новый отзыв");

            if ((boolean)$to) {
                $this->notify(self::NEW_REVIEW, $params, $types);
            };
        }
		*/
    }
	
	public function notifyPasswordRecovery($user) {
		// пользователю
        
        $params = $user -> attributes;
		
        
        $this -> notify(self::PASSWORD_RECOVERY, ['user' => $params, 'email.to' => $user -> email, 'email.subject' => 'Восстановление пароля на сайте VseAdresa.pro']);
        
		
	}

}