<?php
/* генерируемый файл, редактировать его не надо */


namespace common\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;



/**
 * This is the model class for table "adr_users".
 *
 * @property integer $id
 * @property string $fullname
 * @property string $username
 * @property string $role
 * @property integer $status_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $status_changed_at
 * @property string $registration_ip
 * @property string $password
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $psw_recovery_code
 * @property string $email
 * @property string $phone
 * @property integer $role_admin
 * @property string $notifications
 * @property string $form_mode
 * @property string $date_visit
 * @property integer $used
 * @property integer $new_id
 * @property string $auth
 * @property string $psw_recovery_date
 * @property integer $_status_
 * @property string $notification_email
 * @property string $notification_enabled
 * @property integer $default_profile_id
 * @property string $options
 * @property integer $offerta_confirm
 * @property string $delivery_address
 * @property string $info
 * @property integer $priority
 *
 * @property Address[] $addresses
 * @property AuctionRate[] $auctionRates
 * @property Auction[] $auctions
 * @property Auth[] $auths
 * @property Notification[] $notifications0
 * @property Order[] $orders
 * @property Order[] $orders0
 * @property PhotoCalendar[] $photoCalendars
 * @property Porder[] $porders
 * @property Rcompany[] $rcompanies
 * @property Rip[] $rips
 * @property Rperson[] $rpeople
 * @property ServicePrice[] $servicePrices
 * @property Service[] $services
 * @property StatusChange[] $statusChanges
 * @property Status $status
 * @property User $new
 * @property User[] $users
 */
class User extends \common\models\EntityExt  implements IdentityInterface
{
	
	/* Константы модели */
	const ROLE_ADMIN = 'Администратор';
	const ROLE_CUSTOMER = 'Заказчик';
	const ROLE_OWNER = 'Владелец';
	const FM_AUTO = 'Автоматический';
	const FM_MANUL = 'Ручной';
	
	
	
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_users'; 
    }



    public static function parentClassName(){
		return '\common\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [
			'role' => ['Администратор' => 'Администратор', 'Заказчик' => 'Заказчик', 'Владелец' => 'Владелец', ],  
			'form_mode' => ['Автоматический' => 'Автоматический', 'Ручной' => 'Ручной', ],  
			'notification_enabled' => ['yes' => 'yes', 'no' => 'no', ],  

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Пользователи', 
			'model' => 'User', 
			'imp' => 'Пользователь', 
			'rodp' => 'Пользователя', 
			'vinp' => 'Пользователя', 
			'caption' => '%fullname% (%username%)', 
			'parent' => 'objects', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
			$parentAttrMeta = [];
			if (self::parentClassName()) {
				$parentAttrMeta = self::parentClassName()::attrMeta();
			};

			return ArrayHelper::merge(		
				$parentAttrMeta,
				[
								'id' => ['sbow' => 'true', 'visible' => '1', 'filter' => 'value', 'label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'required' => '1', 'show' => '0', ], 
			'fullname' => ['show' => '1', 'visible' => '1', 'label' => 'ФИО', 'filter' => 'like', 'type' => 'string', 'required' => '0', ], 
			'username' => ['label' => 'Логин', 'show' => '1', 'filter' => 'like', 'type' => 'string', 'required' => '1', 'visible' => '1', ], 
			'role' => ['show' => '1', 'visible' => '1', 'label' => 'Тип', 'filter' => 'list', 'format' => 'checkboxlist', 'type' => 'set', 'required' => '0', ], 
			'status_id' => ['label' => 'Состояние', 'format' => 'status', 'filter' => 'list', 'default' => '0', 'model' => 'Status', 'type' => 'ref', 'required' => '0', 'show' => '1', 'visible' => '1', 'link' => '0', ], 
			'created_at' => ['label' => 'Дата добавления', 'filter' => 'range', 'type' => 'date', 'format' => 'd.m.Y', 'show' => '1', 'visible' => '1', 'default' => '{"expression":"CURRENT_TIMESTAMP","params":[]}', 'readonly' => '1', 'required' => '0', ], 
			'updated_at' => ['label' => 'Дата изменения', 'show' => '0', 'format' => 'date', 'readonly' => '1', 'type' => 'timestamp', 'required' => '0', 'visible' => '0', ], 
			'status_changed_at' => ['label' => 'Дата изменения статуса', 'show' => '0', 'comment' => 'Дата последнего изменения статуса. Выставляется автоматически', 'type' => 'timestamp', 'required' => '0', 'visible' => '0', ], 
			'registration_ip' => ['show' => '0', 'label' => 'Registration Ip', 'type' => 'string', 'required' => '0', 'visible' => '0', ], 
			'password' => ['show' => '0', 'label' => 'Пароль', 'type' => 'string', 'required' => '0', 'visible' => '0', ], 
			'password_hash' => ['show' => '0', 'label' => 'Password Hash', 'type' => 'string', 'required' => '0', 'visible' => '0', ], 
			'password_reset_token' => ['show' => '0', 'label' => 'Password Reset Token', 'type' => 'string', 'required' => '0', 'visible' => '0', ], 
			'auth_key' => ['show' => '0', 'label' => 'Auth Key', 'type' => 'string', 'required' => '1', 'visible' => '0', ], 
			'psw_recovery_code' => ['show' => '0', 'label' => 'код восстановления пароля', 'type' => 'string', 'required' => '0', 'visible' => '0', ], 
			'email' => ['show' => '1', 'label' => 'Email', 'format' => 'email', 'filter' => 'like', 'mask' => '@email', 'type' => 'string', 'required' => '0', 'visible' => '1', ], 
			'phone' => ['label' => 'Телефон', 'show' => '1', 'filter' => 'like', 'mask' => '@phone', 'type' => 'string', 'required' => '0', 'visible' => '1', ], 
			'role_admin' => ['show' => '0', 'comment' => 'является админитратором портала', 'label' => 'Role Admin', 'type' => 'tinyint', 'required' => '0', 'visible' => '0', ], 
			'notifications' => ['label' => 'Уведомления', 'show' => '0', 'comment' => 'Информация о включенных/выключенных уведомлениях', 'type' => 'string', 'required' => '0', 'visible' => '0', ], 
			'form_mode' => ['label' => 'Перевод в Формирование', 'show' => '0', 'comment' => 'автоматический/ручной перевод заказов в статус Формирование', 'default' => 'Автоматический', 'type' => 'enum', 'required' => '0', 'visible' => '0', ], 
			'date_visit' => ['show' => '0', 'label' => 'Последний визит', 'visible' => '0', 'comment' => 'Время последнего визита пользователя', 'type' => 'date', 'format' => 'd.m.Y', 'required' => '0', ], 
			'used' => ['show' => '0', 'comment' => 'Флаг того, что пользователь задействован в финансовых операциях, то  есть если флаг установлен, то изменение его платежных реквизитов оказывается невозможным. Для их изменения требуется создание нового пользователя', 'label' => 'Used', 'type' => 'tinyint', 'required' => '0', 'visible' => '0', ], 
			'new_id' => ['show' => '0', 'comment' => 'Ссылка на новго пользователя, если были изменены реквизиты', 'label' => 'New ID', 'model' => 'User', 'type' => 'ref', 'required' => '0', 'visible' => '0', 'link' => '0', ], 
			'auth' => ['show' => '0', 'label' => 'Auth', 'type' => 'string', 'required' => '0', 'visible' => '0', ], 
			'psw_recovery_date' => ['show' => '0', 'label' => 'Psw Recovery Date', 'type' => 'datetime', 'required' => '0', 'visible' => '0', ], 
			'_status_' => ['show' => '0', 'label' => 'Status', 'type' => 'tinyint', 'required' => '0', 'visible' => '0', ], 
			'notification_email' => ['show' => '0', 'label' => 'Notification Email', 'type' => 'string', 'required' => '0', 'visible' => '0', ], 
			'notification_enabled' => ['show' => '0', 'label' => 'Notification Enabled', 'default' => 'yes', 'type' => 'enum', 'required' => '1', 'visible' => '0', ], 
			'default_profile_id' => ['label' => 'Основной профиль', 'show' => '0', 'type' => 'integer', 'required' => '0', 'visible' => '0', ], 
			'options' => ['label' => 'Опции', 'type' => 'json', 'show' => '0', 'required' => '0', 'visible' => '0', ], 
			'offerta_confirm' => ['label' => 'Подтверждение офферты', 'visible' => '0', 'type' => 'boolean', 'required' => '0', 'show' => '1', ], 
			'delivery_address' => ['label' => 'Адрес доставки', 'comments' => 'Адрес доставки документов на адреса', 'type' => 'json', 'show' => '0', 'required' => '0', 'visible' => '0', ], 
			'info' => ['visible' => '0', 'label' => 'Информация', 'comment' => 'Дополнительная информация о пользователе', 'type' => 'text', 'required' => '0', 'show' => '1', ], 
			'priority' => ['label' => 'Приоритет', 'visible' => '0', 'default' => 0, 'type' => 'integer', 'required' => '0', 'show' => '1', ], 

						
				]
			);
		}
		/* Поведения*/
	public function behaviors(){
	
	$result = [];
		$ns = "common\models\\";	
		$class = $ns.$this -> meta()['model'].'Behavior';
		
		if (class_exists($class)) {
			$result['ext'] = [
				'class' => $class,
			];
		};
		
	return array_merge($result,
	[
		
		[
			'class' => TimestampBehavior::className(),
			'createdAtAttribute' => 'create_time',
			'updatedAtAttribute' => 'update_time',
			'value' => new Expression('NOW()'),			
			
		],
	]);
	
		
}
	

	// Создать объект по указанному псевдониму, если такой псевдоним существует.
	public static function byUsername($value) 
	{
		return self::findOne(['username' => $value]);
	}

	public static function statusByAlias($alias) {
			return Status::findOne(["alias" => $alias, "table" => self::tableName()]);
	}
			
			

	public function getOptions($name) {
		return $this -> getArrayValue('options', $name);
	}
			
	public function setOptions($name, $value, $save = false) {
		$this -> setArrayValue('options', $name, $value, $save);
	}
			
	public function getDeliveryAddress($name) {
		return $this -> getArrayValue('delivery_address', $name);
	}
			
	public function setDeliveryAddress($name, $value, $save = false) {
		$this -> setArrayValue('delivery_address', $name, $value, $save);
	}
			





    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'username', 'auth_key'], 'required'],
            [['id', 'status_id', 'new_id', 'default_profile_id', 'priority'], 'integer'],
            [['created_at', 'updated_at', 'status_changed_at', 'date_visit', 'psw_recovery_date'], 'safe'],
            [['form_mode', 'notification_enabled', 'options', 'delivery_address', 'info'], 'string'],
            [['fullname'], 'string', 'max' => 255],
            [['username', 'registration_ip', 'password', 'password_reset_token', 'auth_key', 'email', 'phone', 'notifications', 'auth'], 'string', 'max' => 45],
            [['password_hash', 'notification_email'], 'string', 'max' => 100],
            [['psw_recovery_code'], 'string', 'max' => 32],
            [['role_admin', 'used', '_status_'], 'string', 'max' => 4],
            [['offerta_confirm'], 'string', 'max' => 1],
            [['role_as_array'],  'each', 'rule' => ['string']],
            [['username'], 'unique'],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['new_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['new_id' => 'id']],
        ]);
    }
	

	
	
	
	
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses() {
        return $this->hasMany(Address::className(), ['owner_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuctionRates() {
        return $this->hasMany(AuctionRate::className(), ['owner_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuctions() {
        return $this->hasMany(Auction::className(), ['owner_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuths() {
        return $this->hasMany(Auth::className(), ['user_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications0() {
        return $this->hasMany(Notification::className(), ['user_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Order::className(), ['owner_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders0() {
        return $this->hasMany(Order::className(), ['customer_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotoCalendars() {
        return $this->hasMany(PhotoCalendar::className(), ['user_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPorders() {
        return $this->hasMany(Porder::className(), ['customer_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRcompanies() {
        return $this->hasMany(Rcompany::className(), ['user_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRips() {
        return $this->hasMany(Rip::className(), ['user_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRpeople() {
        return $this->hasMany(Rperson::className(), ['user_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicePrices() {
        return $this->hasMany(ServicePrice::className(), ['owner_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices() {
        return $this->hasMany(Service::className(), ['owner_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusChanges() {
        return $this->hasMany(StatusChange::className(), ['actor_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus() {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNew() {
        return $this->hasOne(User::className(), ['id' => 'new_id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['new_id' => 'id']);
    }



    public static function captionTemplate(){

        return "%fullname% (%username%)";
    }



    public static function findIdentity($id)
    {
//        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]); 
	return static::findOne($id); 
    }


    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        //return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
	return static::findOne(['username' => $username]);
    }



    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
//            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }



    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }





}