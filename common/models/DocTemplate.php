<?php
/*
*	Модель для редактирования файла шаблона документа
*/


namespace common\models;

use backend\controllers\FilesController;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

 
class DocTemplate extends Model {
	const path = '@storage/templates/documents';
	
	public $name;
	public $content;
	
	public function rules()
	{
        return [
            [['name', 'content'], 'required'],
			[['content'], 'string'],
            [['name'], 'string', 'max' => 255],            
        ];
    }
	
	
	// Список шаблонов - он фиксирован
	public static function getList() {
		return [
			'contract' => 'Договор',
			'agreement' => 'Дополнительно соглашение',
			'invoice' => 'Счет',
			'pd4' => 'Квитанция ПД-4',
			
		];
		
	}
	
	public function getFileName() {
		return Yii::getAlias(self::path).'/'.$this -> name.'.html';
	}
	
	
	public static function open($name) {
		$result = new self();
		$result -> name = $name;
		$result -> loadFromFile();
		return $result;
	}
	
	public function loadFromFile() {
		if (!file_exists($this -> fileName)) {
          throw new NotFoundHttpException("Файл ".$this -> fileName." не найден");
        };
		
		$this -> content = file_get_contents($this -> fileName);
	}

	
	public function save()
    {
  
        $file = fopen($this -> fileName, 'w');
        fwrite($file, $this -> content);
        fclose($file);
		return true;
    }
	
}