<?php
/*
* 	Класс генерации номеров документов
*/
namespace common\models;


use yii;



/**
 * This is the model class for table "adr_counters".
 *
 * @property integer $id
 * @property string $alias
 * @property integer $count
 * @property string $date
 * @property string $template
 */
class Counter extends BaseCounter 
{
	

  
  	public static function byAlias($alias){
		$result = static::findOne(['alias' => $alias]);
		if (!$result) {
			$result = new self();
			$result -> alias = $alias;
			///$result -> save();
		}
		
		return $result;		
	}
	
	
	public function getValue() {
		$this -> save();
		if (!$this -> template) {
			$result = $this -> value;
		} else {
			$params['d'] = $this -> value;			
			$result = $this -> generateByTemplate($this -> template, $params);
		}		
		return $result;
	}
	
	public function beforeSave($insert) {
		$this -> value = $this -> value + 1;
		$this -> updated_at = (new \yii\db\Query)-> select('NOW()') -> scalar();			
		return parent::beforeSave($insert);		
	}
	
	
	public static function generateByTemplate($template, $params, $currentClass = true) {		
		$result = $template;
		foreach($params as $name => $value) 
		{
			$result = str_replace('%'.$name.'%', $value, $result);
		};
		return trim($result);
	}

}