<?php
namespace common\models;

use yii;
use yii\base\Behavior;
use yii\helpers\Url;
use common\models\EntityExt;

class RpersonBehavior extends RequisiteBehavior
{

	public function getTitle() {
		$r = $this -> owner;
		$res = $r -> lname.' ';
		if ($r->fname) $res = $res.mb_substr($r->fname, 0, 1).'.';
		if ($r->mname) $res = $res.mb_substr($r->mname, 0, 1).'.';
		return trim($res);
	}

	
	

	
	public function findEqual($attrs = false) {
		$attrs = [
			'inn', 						
			'fname',
			'mname',
			'lname',
			'bdate',
			'pasport_number',
			'pasport_date',
			'pasport_organ',
			'address',
			'phone',
			'bank_bik',
			'bank_name',
			'bank_cor',
			'account',
			'account_owner',
			'tax_system_vat',			
		];
		
		return parent::findEqual($attrs);
	}		
	
	

		
	public function getHeaderText($sideName) {
		$r = $this -> owner;
		$params = $r -> attributes;
//		$params['sv_date'] = Yii::$app -> formatter -> asDate(strtotime($params['sv_date']), 'd MMMM Y');
		$template = '
			<strong>Гражданин  {lname} {fname} {mname}</strong> паспорт {pasport_number}, выданный {pasport_date} {pasport_organ}, зарегистрированный по адресу {address}
		';
		
		return EntityHelper::replaceContent($template, $params);
	}
	

		
	public function getFooterText() {
		$r = $this -> owner;
		$params = $r -> attributes;
		$template = '			
<strong>Гражданин  {lname} {fname} {mname}</strong><br/>
паспорт: {pasport_number}, выданный {pasport_date} {pasport_organ}<br/>
{address}<br/>'.
($r -> inn?'ИНН {inn}, <br/>':'').
'р/с {account}<br/>
в {bank_name}<br/>
к/с {bank_cor}<br/>
БИК {bank_bik}<br/>
		';
		return EntityHelper::replaceContent($template, $params);
	}	


	
}

?>