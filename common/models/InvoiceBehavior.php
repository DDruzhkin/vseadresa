<?php
namespace common\models;

use yii\base\Behavior;
use common\models\Inovice;
use common\models\EntityExt;
use common\models\Payment;
use common\models\DocumentType;
use yii\helpers\ArrayHelper;

class InvoiceBehavior extends Behavior
{
	
	
	public static function paymentDescription() {
		return
		'
		<code>payment($payment_order, $params)</code>
		Оплата счета - создание платежа, изменение статуса счет<br>
		Необходимо передать платежное поручение, по котором осуществляется оплата
		Могут быть переданы параметры для платежного поручения в $params<br>
		';
	}	
	
	// Оплата счета - создание платежа, изменение статуса счета
	public function payment($payment_order, $params) {
//		echo 'payment<br>';
//		var_dump($payment_order);
		/*
		// попробуем платеж найти в базе по платежке
		$payment = Payment::find() -> andWhere(['payment_order_id' => $payment_order -> id]) -> one(); 

		if (!$payment) {
			$payment = new Payment();
		};						
		
		// Создаем платеж
		
		$payment -> date = $payment_order -> created_at;
		if ($payment_order -> doc_type_id == DocumentType::byAlias('paymentorder') -> id) {
			$payment -> ptype = Payment::PTYPE_BANK;
		}
		
		$payment -> value = $payment_order -> summa;
		$payment -> payment_order_id = $payment_order -> id;
		$payment -> invoice_id = $invoice -> id;
		$payment -> payer_id = $invoice -> recipient_id;
		$payment -> recipient_id = $invoice -> owner_id;		
		$payment -> confirmed = 1;
		$payment -> load($params, '');
		
		$payment -> save();
*/		

		$invoice = $this -> owner;		
		if ($payment_order) {
			$params['payer_id'] = $invoice -> recipient_id;// получатель счета является плательщиком
			$params['recipient_id'] = $invoice -> owner_id;// владелец счета является получателем
			$payment = $payment_order -> createPayment($params);
		
			//$invoice -> state = 1;
			
		};
		
		$invoice -> save();		
		$payment -> invoice_id = $invoice -> id;
		$payment -> save();
		$invoice -> balance();
		// изменяем статус заказа в зависимости от баланса
		//var_dump(($invoice -> order)?invoice -> number.' -> '.$invoice -> order -> balance():'');
		if ($invoice -> order) $invoice -> order -> balance();
		
		/*
		if (!is_null($invoice) && !is_null($invoice -> order)) {
			$invoice -> order -> changeStatus('paid', true);
			$invoice -> order -> changeStatus('formation', true);
		};
		*/
		
		// TODO: Надо также изменять статус портального заказа, если статус появится
		
	}
	
	
	public static function balanceDescription() {
		return
		'
		<code>balance($checkStatus = true; $save = false)</code>
		Вычисляет сумму оплаты по счету.<br>
		Если $checkStatus, то выставляет статус оплачен для счета.<br>
		Если ни одной оплаты не было, то статус счета не затрагивается.<br>
		Возвращает оплаченную сумму по счету.<br>
		Для изменения статуса этот метод должен вызываться в местах оплаты
		';
	}
	
	
	//TODO: Скопирована из Document - но должно работать наследование
	public function balance($checkStatus = true, $save = true) {
		$invoice = $this -> owner;
		$query = (new \yii\db\Query()) 
			-> select('SUM(p.value) as value')
			-> from(Payment::tableName().' p')			
			-> andWhere(['invoice_id' => $invoice -> id, 'confirmed' => 1]);
		
		$paied = ($query -> one())['value'];
//echo $paied.' - '.$invoice -> summa.'<br>'; 
		if ($checkStatus) {
			if ($invoice -> summa > $paied) { // не оплачен
				$invoice -> state = Invoice::STATE_NOPAID;				
			} else { // полная оплата
				$invoice -> state = Invoice::STATE_PAID;
			}			
		};
//var_dump($invoice -> number.' -> '.$invoice -> state);echo "<br>";		 exit;		

		if ($save) $invoice ->  save();
		
		return $paied;
		
	}
	
	
	
}
