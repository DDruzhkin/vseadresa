<?php
/*
*	Класс кэширования, расширяет MemCache
*/


namespace common\models;


class MemCache extends \yii\caching\MemCache
{
	
	public $id = ''; // Уникальный идентификатор при использовании одного сервера кэширования для разных копий системы.
	
	public $defaultDuration = 60; // Время кэширования по-умолчанию (в основном для короткого кеширования, может быть не использовано)
	public $listDuration = 600; // Время кэширования справочников. Если меньше нуля, то кеш списков отключается
	public $statDuration = 3600; // Время кэширования статистических значений. Если меньше нуля, то кеш статистических значений отключается
	
	
    protected function getValue($key)
    {
        return parent::getValue($this -> id.$key);
    }

    protected function getValues($keys)
    {
		$data = parent::getValues($keys);
        foreach($data as $name => $value) {
			$rdata[$this -> id.$name] = $value;
		}
		return $rdata;
    }

    
    protected function setValue($key, $value, $duration)
    {
        return parent::setValue($this -> id.$key, $value, $duration);
    }

    protected function setValues($data, $duration)
    {        
		foreach($data as $name => $value) {
			$rdata[$this -> id.$name] = $value;
		}
		
        return parent::setValues($rdata, $duration);
    }

    protected function addValue($key, $value, $duration)
    {        
        return parent::addValue($this -> id.$key, $value, $duration);
    }

    
    protected function deleteValue($key)
    {
        return parent::delete($this -> id.$key);
    }

	
	// получить список по имени класса и шаблону. Шаболон напрямую не применяется, а только для идентификации и для получения списка используемых полей
	public function getList($className, $template = false) {
//		echo 'get '.$className.' - '. $template.'<br>';
		if ($this -> listDuration < 0) return;		
		$lists = $this -> get($className.'.lists');
		if ($lists && in_array($template, $lists)) {
				return $this -> get($className.'.'.$template);				
		} else return false;
		
	}
	
	// установить список по имени класса и шаблону. Шаболон напрямую не применяется, а только для идентификации и для получения списка используемых полей
	public function setList($value, $className, $template = false) {
//		echo 'set '.$className.' - '. $template.'<br>';		
		if ($this -> listDuration < 0) return;		
		$lists = $this -> get($className.'.lists');
		$this -> set($className.'.'.$template, $value, $this -> listDuration);
		if (!$lists || !in_array($template, $lists)) {
			$lists[] = $template;
			$this -> set($className.'.lists', $lists, $this -> listDuration);
		};
			
	}

	protected static function getObjectKey($id) {
		return 'objects.'.$id;
	}
	
	// возвращает caption объекта через кеш
	public function getObjectCaption($object, $template = false) {
		$result = false;
		if ($this -> listDuration < 0 || !$object) return $result;
		$id = $object -> id;
		
		$key = 'caption.'.$this -> getObjectKey($id);			
		$resultCaptions = $this -> get($key);
//		echo '<br>get: '; var_dump($key); echo' - ';var_dump($resultCaptions);
		if ($resultCaptions && is_array($resultCaptions)) {
			if (array_key_exists($template, $resultCaptions)) $result = $resultCaptions[$template];
		}
		
		return $result;
	}
	
	public function setObjectCaption($value, $object, $template = false) {		
		if ($this -> listDuration < 0 || !$object) return false;
		$id = $object -> id;		
		$key = 'caption.'.$this -> getObjectKey($id);		
		$resultCaptions = $this -> get($key);
		$resultCaptions[$template] = $value;	
		$this -> set($key, $resultCaptions, $this -> listDuration);		
	
	}
	
	public function clearObjectCaption($object) {
		if ($this -> listDuration < 0 || !$object) return false;
		$id = $object -> id;
		$key = 'caption.'.$this -> getObjectKey($id);
				
		$this -> set($key, false, 1);
	}
	
	public function clearObject($id) {
		if ($this -> statDuration < 0	) return false;
		$this -> set(self::getObjectKey($id), null);
		
		
	}
	
	public function getObject($id) {
		
		if ($this -> statDuration < 0) return;		
		$result = $this -> get(self::getObjectKey($id));
		
		if ($result !== false && !is_null($result)) {			
			if (($result -> baseClassName() == 'CObject'))	{				
				$result = false;
			};			
						
		}
		
		return $result;
		
	}
	
	public function setObject($object) {
		
		if (
			($this -> statDuration < 0) || 
			!$object || 
			(gettype($object) != 'object') || 
			($object -> baseClassName() == 'CObject')
		) return false;
		
		
		$id = $object -> id;		
		$this -> set(self::getObjectKey($id), $object, $this -> statDuration);
		
		
		
	}
	
	// вызывается при сохранении объекта.
	// Обновляется или чистится кэш всех связанных структур
	public function saveObject($object) {
		if (!$object || gettype($object) != 'object') return false;
		//var_dump($object -> attributes); exit;
		// обновляем объект
		//$id = $object -> id;
		//$this -> set(self::getObjectKey($id), $object, $this -> listDuration);
		$this -> setObject($object);
		$this -> clearObjectCaption($object);
		
		// чистим кэш списков, связанных с этим объектом
		$className = $object::className();
		$lists = $this -> get($className.'.lists');
				
		
		if ($lists && is_array($lists)) {
			foreach($lists as $template) {
				//$this -> delete($className.'.'.$template);			
				$this -> set($className.'.'.$template, false, 1);			
			}
		}		
		
		//$this -> delete($className.'.lists');
		$this -> set($className.'.lists', false, 1);
	}
	
}
?>