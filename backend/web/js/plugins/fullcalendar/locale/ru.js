!function (e, o) {
    "object" == typeof exports && "object" == typeof module ? module.exports = o(require("moment"), require("fullcalendar")) : "function" == typeof define && define.amd ? define(["moment", "fullcalendar"], o) : "object" == typeof exports ? o(require("moment"), require("fullcalendar")) : o(e.moment, e.FullCalendar)
}("undefined" != typeof self ? self : this, function (e, o) {
    return function (e) {
        function o(t) {
            if (r[t]) return r[t].exports;
            var n = r[t] = {i: t, l: !1, exports: {}};
            return e[t].call(n.exports, n, n.exports, o), n.l = !0, n.exports
        }

        var r = {};
        return o.m = e, o.c = r, o.d = function (e, r, t) {
            o.o(e, r) || Object.defineProperty(e, r, {configurable: !1, enumerable: !0, get: t})
        }, o.n = function (e) {
            var r = e && e.__esModule ? function () {
                return e.default
            } : function () {
                return e
            };
            return o.d(r, "a", r), r
        }, o.o = function (e, o) {
            return Object.prototype.hasOwnProperty.call(e, o)
        }, o.p = "", o(o.s = 113)
    }({
        0: function (o, r) {
            o.exports = e
        }, 1: function (e, r) {
            e.exports = o
        }, 113: function (e, o, r) {
            Object.defineProperty(o, "__esModule", {value: !0}), r(114);
            var t = r(1);
            t.datepickerLocale("ru", "ru", {
                closeText: "Закрыть",
                prevText: "&#x3C;Пред",
                nextText: "След&#x3E;",
                currentText: "Сегодня",
                monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                monthNamesShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
                dayNames: ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
                dayNamesShort: ["вск", "пнд", "втр", "срд", "чтв", "птн", "сбт"],
                dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                weekHeader: "Нед",
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ""
            }), t.locale("ru", {
                buttonText: {month: "Месяц", week: "Неделя", day: "День", list: "Повестка дня"},
                allDayText: "Весь день",
                eventLimitText: function (e) {
                    return "+ ещё " + e
                },
                noEventsMessage: "Нет событий для отображения"
            })
        }, 114: function (e, o, r) {
            !function (e, o) {
                o(r(0))
            }(0, function (e) {
                var o = "янв._февр._мар._апр._ма._июн._июл._авг._сент._окт._нояб._дек.".split("_"),
                    r = "янв_февр_мар_апр_ма_июн_июл_авг_сент_окт_нояб_дек".split("_"),
                    t = [/^янв/i, /^февр/i, /^мар/i, /^апр/i, /^ма/i, /^июн/i, /^июл/i, /^авг/i, /^сент/i, /^окт/i, /^нояб/i, /^дек/i],
                    n = /^(Января|Февраля|Марта|Апреля|Мая|Июня|Июля_|Августа|Сентября|Октября|Ноября|Декабря|янв\.?|февр\.?|мар\.?|апр\.?|ма\.?|июн\.?|июл\.?|авг\.?|сент\.?|окт\.?|нояб\.?|дек\.?)/i;
                return e.defineLocale("ru", {
                    months:  "Января_Февраля_Марта_Апреля_Мая_Июня_Июля_Августа_Сентября_Октября_Ноября_Декабря".split("_"),
                    monthsShort: function (e, t) {
                        return e ? /-MMM-/.test(t) ? r[e.month()] : o[e.month()] : o
                    },
                    monthsRegex: n,
                    monthsShortRegex: n,
                    monthsStrictRegex: /^(январ|феврал|марта|апрел|ма|июн|июл|августа|сентябр|октябр|ноябр|декабр)/i,
                    monthsShortStrictRegex: /^(янв\.|февр?\.|мар[т.]|апр\.|ма[яй]|июн[ья.]|июл[ья.]|авг\.|сент?\.|окт\.|нояб?\.|дек\.)/i,
                    monthsParse: t,
                    longMonthsParse: t,
                    shortMonthsParse: t,
                    weekdays: "воскресенье_понедельник_вторник_среду_четверг_пятницу_субботу".split("_"),
                    weekdaysShort: "вс_пн_вт_ср_чт_пт_сб".split("_"),
                    weekdaysMin: "вс_пн_вт_ср_чт_пт_сб".split("_"),
                    weekdaysParseExact: !0,
                    longDateFormat: {
                        LT: "H:mm",
                        LTS: "H:mm:ss",
                        L: "DD/MM/YYYY",
                        LL: "D [de] MMMM [de] YYYY",
                        LLL: "D [de] MMMM [de] YYYY H:mm",
                        LLLL: "dddd, D [de] MMMM [de] YYYY H:mm"
                    },
                    calendar: {
                        sameDay: function () {
                            return "[hoy a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                        }, nextDay: function () {
                            return "[mañana a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                        }, nextWeek: function () {
                            return "dddd [a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                        }, lastDay: function () {
                            return "[ayer a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                        }, lastWeek: function () {
                            return "[el] dddd [pasado a la" + (1 !== this.hours() ? "s" : "") + "] LT"
                        }, sameElse: "L"
                    },
                    relativeTime: {
                        future: "en %s",
                        past: "hace %s",
                        s: "unos segundos",
                        ss: "%d segundos",
                        m: "un minuto",
                        mm: "%d minutos",
                        h: "una hora",
                        hh: "%d horas",
                        d: "un día",
                        dd: "%d días",
                        M: "un mes",
                        MM: "%d meses",
                        y: "un año",
                        yy: "%d años"
                    },
                    dayOfMonthOrdinalParse: /\d{1,2}º/,
                    ordinal: "%dº",
                    week: {dow: 1, doy: 4}
                })
            })
        }
    })
});