<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "adr_nalogs".
 *
 * @property integer $id
 * @property string $number
 * @property integer $okrug
 *
 * @property Address[] $addresses
 * @property Okrug $okrug0
 */
class Nalog extends \common\models\Nalog
{

}
