<?php

namespace backend\models;
use Yii;

/**
 * This is the model class for table "adr_status".
 *
 * @property integer $id
 * @property string $name
 * @property string $table
 *
 * @property Address[] $addresses
 * @property Document[] $documents
 * @property Order[] $orders
 */
class Status extends \common\models\Status
{

}
