<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "adr_regions".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Addresses[] $addresses
 */
class Region extends \common\models\Region
{

}
