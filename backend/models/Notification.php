<?php
namespace backend\models;

use Yii;

/**
 * This is the model class for table "adr_notifications".
 *
 * @property integer $id
 * @property integer $type
 * @property string $email
 * @property integer $user_id
 * @property integer $enabled
 *
 * @property Users $user
 */
class Notification extends \common\models\Notification
{

}