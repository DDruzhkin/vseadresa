<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "adr_service_names".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 *
 * @property ServicePrices[] $servicePrices
 */
class ServiceName extends \common\models\ServiceName
{

}
