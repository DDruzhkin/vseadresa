<?php

namespace backend\models;
use Yii;




/**
 * This is the model class for table "adr_services".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status_id
 * @property integer $address_id
 * @property integer $order_id
 * @property string $date
 * @property string $date_fin
 * @property string $price
 *
 * @property Address $address
 * @property Service $status
 * @property Service[] $services
 */
class Service extends \common\models\Service
{


}