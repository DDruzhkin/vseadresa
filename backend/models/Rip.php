<?php

namespace backend\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;



/**
 * This is the model class for table "adr_rip".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $form
 * @property integer $user_id
 * @property string $inn
 * @property string $kpp
 * @property string $ogrnip
 * @property string $fname
 * @property string $mname
 * @property string $lname
 * @property string $pasport_number
 * @property string $pasport_date
 * @property string $pasport_organ
 * @property string $address
 * @property string $bank_bik
 * @property string $account
 * @property string $account_owner
 *
 * @property User $user
 */
class Rip extends \common\models\Rip {
	
}