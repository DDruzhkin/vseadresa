<?php

namespace app\models;





/**
 * This is the model class for table "adr_options".
 *
 * @property integer $id
 * @property string $name
 * @property string $stype
 * @property string $icon
 */
class Parametr extends \yii\db\ActiveRecord 
{
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'adr_options'; 
    }



    public static function parentClassName(){
		return '\app\models\CObject';
    }



	/* Списки для перечисляемых типов атрибутов */
	public static function attrLists() {
		return [
			'stype' => [1 => 'Общий', ],  

		];
	}
	/* Метаданные класса */
	public static function meta() {
		return [
			'title' => 'Список параметров адресов', 
			'imp' => 'Параметр', 
			'rodp' => 'Параметра', 
			'vinp' => 'Параметр', 
			'model' => 'Parametr', 
			'parent' => 'objects', 
			'caption' => '%name%', 
			'comment' => 'Список параметров адресов. Дополнительные иконки могу быть добавлены системным администратором.', 
				
		];
	}
	/* Метаданные атрибутов*/
	public static function attrMeta() {
		return [
			'id' => ['label' => 'ID', 'readonly' => '1', 'type' => 'integer', 'show' => '0', 'visible' => '0', ], 
			'name' => ['label' => 'Название параметра', 'type' => 'string', 'show' => '1', 'visible' => '1', ], 
			'stype' => ['label' => 'Тип параметра', 'default' => '"Общий"', 'type' => 'enum', 'show' => '1', 'visible' => '1', ], 
			'icon' => ['label' => 'Иконка', 'type' => 'string', 'show' => '1', 'visible' => '1', ], 
				
		];
	}










    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['stype'], 'string'],
            [['name', 'icon'], 'string', 'max' => 50],
        ];
    }




    public static function captionTemplate(){

        return "%name%";
    }





}