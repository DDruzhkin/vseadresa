<?php

namespace backend\models;





/**
 * This is the model class for table "adr_reviews".
 *
 * @property integer $id
 * @property string $date
 * @property string $fullname
 * @property string $email
 * @property string $company
 * @property integer $status_id
 * @property string $content
 * @property integer $user_id
 *
 * @property Status $status
 * @property User $user
 */
class Review extends \common\models\Review {
}