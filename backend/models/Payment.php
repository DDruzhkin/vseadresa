<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "adr_payments".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $date
 * @property integer $status
 * @property string $ptype
 * @property string $value
 * @property string $info
 *
 * @property User $user
 */
class Payment extends \common\models\Payment
{

}
