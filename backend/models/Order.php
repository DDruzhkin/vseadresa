<?php

namespace backend\models;
use Yii;

/**
 * This is the model class for table "adr_orders".
 *
 * @property integer $id
 * @property string $date
 * @property string $date_mod
 * @property string $nubmer
 * @property integer $customer_id
 * @property integer $status_id
 * @property string $duration
 * @property integer $address_id
 * @property integer $delivery_type
 * @property string $delivery_address
 * @property string $delivery_contacts
 * @property integer $payment_type
 * @property double $pirce
 *
 * @property Document[] $documents
 * @property Address $address
 * @property Status $status
 * @property User $customer
 * @property Payment[] $payments
 */
class Order extends \common\models\Order
{

}
