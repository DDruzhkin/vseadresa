<?php
/* генерируемый файл, редактировать его не надо */


namespace backend\models;


/**
 * This is the model class for table "adr_photo_calendar".
 *
 * @property integer $id
 * @property integer $address_id
 * @property integer $user_id
 * @property string $date
 * @property integer $interval
 * @property integer $enabled
 *
 * @property User $user
 * @property Address $address
 */
class PhotoCalendar extends \common\models\PhotoCalendar
{
    const BUSY_FROM_USER = 0;
    const BUSY_FROM_BLOCKED = 1;

	public function getEnabled(){
	    return [
	        self::BUSY_FROM_USER => 'Занят пользователем',
	        self::BUSY_FROM_BLOCKED => 'Занят заблокирован',
        ];
    }
}