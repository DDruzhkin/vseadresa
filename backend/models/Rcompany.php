<?php

namespace backend\models;


use yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;



/**
 * This is the model class for table "adr_rcompany".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $form
 * @property integer $user_id
 * @property string $inn
 * @property string $kpp
 * @property string $ogrn
 * @property string $name
 * @property string $full_name
 * @property string $address
 * @property string $bank_bik
 * @property string $account
 * @property string $account_owner
 * @property string $dogovor_signature
 * @property string $dogovor_face
 * @property string $dogovor_osnovanie
 *
 * @property User $user
 */
class Rcompany extends \common\models\Rcompany 
{
  
}