<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "adr_okrugs".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Address[] $addresses
 * @property Nalog[] $nalogs
 */
class Okrug extends \common\models\Okrug
{

}
