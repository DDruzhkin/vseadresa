<?php

namespace backend\models;


use yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "adr_porders".
 *
 * @property integer $id
 * @property string $number
 * @property string $created_at
 * @property string $updated_at
 * @property integer $customer_id
 * @property integer $customer_r_id
 * @property integer $service_id
 * @property integer $recipient_id
 * @property double $summa
 */
class Porder extends \common\models\Porder
{

};