<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "adr_options".
 *
 * @property integer $id
 * @property string $name
 * @property integer $val_int
 * @property double $val_float
 * @property string $val_str
 * @property string $val_text
 * @property string $val_datetime
 * @property string $vtype
 * @property integer $user_id
 *
 * @property Users $user
 */
class Option extends \common\models\Option
{

}
