<?php
/* генерируемый файл, редактировать его не надо */


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


use yii\grid\GridView;
use yii\helpers\Html;


$this->title = 'Шаблоны';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-index">

    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

        <?= Html::a('Добавить шаблон', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'header' => Yii::t('app','Name'),
                'attribute' => 'name',

            ],

            [
                'header' => Yii::t('app','Type'),
                'attribute' => 'type',

            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => [],
                'header' => '',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'name' => $model['name']],
                            ['title' => Yii::t('app', 'Update')]);
                    },
                    'delete' => function ($url, $model) {
                        if($model['delete'] !== false) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'name' => $model['name']],
                                [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data' => [
                                        'confirm' => 'Вы уверены, что хотите удалить элемент?',
                                        'method' => 'post',
                                    ],
                                ]
                            );
                        }
                    },
                ]
            ],
        ],
    ]); ?>
</div>
