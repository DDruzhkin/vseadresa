<?php
if (YII_DEBUG) $startTime = microtime(true);

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model backend\models\Document */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="email-template-form">

    <?php if ($model->defaultFileList): ?>
        <div class="default-template-list">Имена шаблонов по умолчанию
            <strong>
                <?php foreach ($model->defaultFileList as $key => $value): ?>
                    <?= Html::a($value, '#', ['class' => 'default-template', 'data-name' => $value]); ?>
                <?php endforeach; ?>
            </strong>
        </div>
    <?php endif; ?>

    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

    <?= $form->field($model, 'name', ['options' => ['class' => 'clearfix']])->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'template', ['options' => ['class' => 'clearfix']])->dropDownList(ArrayHelper::map($model->viewFileList, 'name', 'name'), array(
        /*'onchange' => '$.post("' . Url::to(['/email-template/put-template-into-editor']) . '",{ name: $(this).val() },
                  function(data) {
                         CKEDITOR.instances["emailtemplate-content"].setData(data);
                  }
           );',
        'csrf' => true,*/
    )) ?>

    <?= $form->field($model, 'content')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>

<script>
    $('.default-template').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: '<?= Url::to(['/email-template/put-default-template-into-editor']) ?>',
            type: 'post',
            data: {name: $(this).attr('data-name')},
            success: function (data) {
                var obj = JSON.parse(data);
                $("#emailtemplate-name").val(obj.name);
                $("#emailtemplate-template").val(obj.view);
                CKEDITOR.instances["emailtemplate-content"].setData(obj.content);
            }
        });
    });

    CKEDITOR.replace('EmailTemplate[content]');

</script>