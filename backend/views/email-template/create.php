<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Document */

$this->title = 'Добавление шаблон';
$this->params['breadcrumbs'][] = ['label' => 'Список шаблонов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
