<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Order;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;


$data = [];

$list = $model -> getRelatedListValues('address_id');

$data[] = 
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('created_at_view', 'label'), 'hint'=>''.Order::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'number' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('number', 'label'), 'hint'=>''.Order::getMeta('number', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	];

	
$data[] = 	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'status_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('status_id', 'label'), 'hint'=>''.Order::getMeta('status_id', 'comment'), 'items' => $model -> getStatusList()],	
						'duration' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('duration', 'label'), 'items' => Order::getList('duration'), 'hint'=>''.Order::getMeta('duration', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	];

	
$data[] = 	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'customer_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('customer_id', 'label'), 'items' => Order::getListValues('customer_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('user/update/%customer_id%')]).'">'.Icon::show('link').'</a>'.Order::getMeta('customer_id', 'comment'), 'options'=>[]],
						'customer_r_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('customer_r_id', 'label'), 'items' => $model -> getRelatedListValues('customer_r_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('cobject/update/%customer_r_id%')]).'">'.Icon::show('link').'</a>'.Order::getMeta('customer_r_id', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	];

	
$data[] = 	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'address_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('address_id', 'label'), 'items' => $model -> getRelatedListValues('address_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('address/update/%address_id%')]).'">'.Icon::show('link').'</a>'.Order::getMeta('address_id', 'comment'), 'options'=>[]],
						'owner_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('owner_id_view', 'label'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('user/update/%owner_id%')]).'">'.Icon::show('link').'</a>'.Order::getMeta('owner_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	];

	
$data[] = 	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'date_start' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Order::getMeta('date_start', 'label'), 'hint'=>''.Order::getMeta('date_start', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
						'date_fin_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('date_fin_view', 'label'), 'hint'=>''.Order::getMeta('date_fin_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	];

	
$data[] = 	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'address_price_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('address_price_view', 'label'), 'hint'=>''.Order::getMeta('address_price_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'is_quick' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>Order::getMeta('is_quick', 'label'), 'hint'=>''.Order::getMeta('is_quick', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],]],
		
				],
			],				
		],				
	];


	
$data[] = 	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'delivery_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('delivery_id_view', 'label'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('porder/update/%delivery_id%')]).'">'.Icon::show('link').'</a>'.Order::getMeta('delivery_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'delivery_type_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('delivery_type_id', 'label'), 'items' => Order::getListValues('delivery_type_id'), 'hint'=>''.Order::getMeta('delivery_type_id', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	];


	
$data[] = 	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'delivery_contacts' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('delivery_contacts', 'label'), 'hint'=>''.Order::getMeta('delivery_contacts', 'comment'), 'options'=>[]],
						'delivery_address' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('delivery_address', 'label'), 'hint'=>''.Order::getMeta('delivery_address', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	];


	
$data[] = 	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'guarantee_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('guarantee_id_view', 'label'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('porder/update/%guarantee_id%')]).'">'.Icon::show('link').'</a>'.Order::getMeta('guarantee_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'payment_type' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Order::getMeta('payment_type', 'label'), 'items' => Order::getList('payment_type'), 'hint'=>''.Order::getMeta('payment_type', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	];


	
$data[] = 	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'summa_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Order::getMeta('summa_view', 'label'), 'hint'=>''.Order::getMeta('summa_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	];

$data[] = 
		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		];
		




/* @var $this yii\web\View */
/* @var $model backend\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Order::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
