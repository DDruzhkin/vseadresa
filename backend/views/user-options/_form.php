<?php
if (YII_DEBUG) $startTime = microtime(true);
use backend\models\User;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;


?>
<div class="user-form">
<hr/>
<?php
		
	$form = ActiveForm::begin();
?>
	echo $form->field($model, 'address_price_6_min')->textInput()->hint($model -> getMeta('address_price_6_min', 'comment')) ;
<?php
	ActiveForm::end();

	
	

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
