<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Porder;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'number' => ['type'=>Form::INPUT_TEXT, 'label'=>Porder::getMeta('number', 'label'), 'hint'=>''.Porder::getMeta('number', 'comment'), 'options'=>[]],
						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Porder::getMeta('created_at_view', 'label'), 'hint'=>''.Porder::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'service_name' => ['type'=>Form::INPUT_TEXT, 'label'=>Porder::getMeta('service_name', 'label'), 'hint'=>''.Porder::getMeta('service_name', 'comment'), 'options'=>[]],
		
				],
			],
				
		],		
	],
		
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'info' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Porder::getMeta('info', 'label'), 'hint'=>''.Porder::getMeta('info', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'service_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Porder::getMeta('service_id', 'label'), 'items' => Porder::getListValues('service_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('service-portal/update/%service_id%')]).'">'.Icon::show('link').'</a>'.Porder::getMeta('service_id', 'comment'), 'options'=>[]],
						'customer_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Porder::getMeta('customer_id', 'label'), 'items' => Porder::getListValues('customer_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('user/update/%customer_id%')]).'">'.Icon::show('link').'</a>'.Porder::getMeta('customer_id', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'customer_r_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Porder::getMeta('customer_r_id', 'label'), 'items' => $model -> getRelatedListValues('customer_r_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('cobject/update/%customer_r_id%')]).'">'.Icon::show('link').'</a>'.Porder::getMeta('customer_r_id', 'comment'), 'options'=>[]],
						'recipient_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Porder::getMeta('recipient_id', 'label'), 'items' => $model -> getRelatedListValues('recipient_id'), 'hint'=>''.Porder::getMeta('recipient_id', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'summa_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Porder::getMeta('summa_view', 'label'), 'hint'=>''.Porder::getMeta('summa_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'invoice_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Porder::getMeta('invoice_id_view', 'label'), 'hint'=>''.Porder::getMeta('invoice_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Porder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="porder-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Porder::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
