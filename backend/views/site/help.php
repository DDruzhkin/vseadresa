<style>
table.list {
	border: solid #555555 2px;
}
table.list tr.header td {
	background: #337ab7;
	color: #EEEEEE;
	
}

table.list tr td {
	border: solid #555555 1px;
	padding: 2px 5px;
	text-align: left;
}

table.list tr td.name {
	font-weight: bold;
}

table.list tr.odd {
	background: #FFFFEE;
}

table.list tr td.description {		
	font-style: italic ;
	font-size: 9pt;
	padding: 5px 5px 10px 5px;
}

</style>
<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\icons\Icon;
use yii\helpers\Url;

$ns = '\common\models\\';
if (!$modelName) $modelName = 'Address';




function getMetaDescription($attrName) {
	$descriptions = [
		'title' => 'Заголовок списка экземпляров модели, который, например, отображается в списке экземпляров модели в backend',
		'model' => 'Имя модели',
		'imp' => 'Заголовок модели в именительном падеже',
		'rodp' => 'Заголовок модели в родительном падеже',
		'vinp' => 'Заголовок модели в винительном падеже',
		'caption' => 'Шаблон поумолчанию для генерации заголовка экземпляра модели, в частности, которое доступно по методу модели <code>getCaption()</code>',
		'comment' => 'Комментарий к модели, который, например, отображается в списке экземпляров модели в backend',
		'sortattr' => 'Имя атрибута, по которому будет производится сортировка по умолчанию',
		'sortdir' => 'Направление сортировки по умолчанию. Может принимать два значения asc/desc',
	];
	return  isset($descriptions[$attrName])?$descriptions[$attrName]:' - ';
}

function renderAttributProperties($modelName, $attrName) {
	
	$descriptions = [
		'label' => 'Подпись к атрибуту. Может быть использована в формах',
		'comment' => 'Описание атрибута для пользователя. Может быть использован для хинтов в формах',
		'show' => 'Отображать или нет в интерфейсе. Например, если это атрибут имеет внутренний служебный характер, то такой атрибут скрывается с помощью этого свойства. Значение = [0,1]',
		'visible' => 'Отображать или скрыть атрибут при отображении списков экземпляров модели. Значение = [0,1]',
		'readonly' => 'Сделать ли контрол этого атрибута в форме только для чтения. Обычно это необходимо для атрибутов, которые заполняются автоматически. Значение = [0,1]',
		'required' => 'Является ли атрибут обязательным для заполнения. Значение = [0,1]',
		'type' => 'Тип атриубта',
		'format' => 'Формат вывода значения атрибута. Для даты, например, может быть использован формат d.m.Y; для денежных значений [decimal, 2]. Существует несколько специальных форматов: gps - две коородинаты gps координыты через запятую; images - список изображений, icon - иконка, checkboxlist - список чекбоксов',
		'filter' => 'Тип фильтра в списках. Если значение равно 1, то тип фильтра система выберет автоматически. Существую следующие типы фильтров: value - поиск по полному совпадению значения; list - выбор значения из списка существующих в базе; range - выбор диапазона; like - поиск по подстроке',
		'default' => 'Значение по умолчанию, которое присваивается при создании экземпляра модели. Досутпны следующие специальные значения: currentUser() - текуцщий пользователь, defaultStatus() - значение специальное для поля статуса - устанавливается значение статуса по умолчанию, определенное для данной модели.',
		'link' => 'Отображать или нет ссылку на объект под значением в форме. Работает только для атрибутов, являющихся ссылкой на объект.Значение = [0,1]',		
		'model' => 'Имя модели, на экземпляр которой ссылается значения этого атрибута',
	];
	
	
	
	$_list = $modelName::getMeta($attrName);	
	$list = [];
	foreach($descriptions as $name => $value){
		$value = ArrayHelper::getValue($_list, $name, NULL);		
		if ($value !== NULL) {
			$list[$name] = $value;
			unset($_list[$name]);
		};		
	}
	
	//$list = array_merge($list, $_list);
	foreach($_list as $name => $value) {
		$list[$name] = $value;
	}
	
	
	foreach($list as $name => $value){
		$hint = ArrayHelper::getValue($descriptions, $name, false);	
		$hint = $hint?(' <a href="#" title="'.$hint.'">'.Icon::show('question-circle-o').'</a>'):''; 
		if ($name == 'label') $value = '<b>'.$value.'</b>';
		if ($name == 'model') $value = '<a title="Перейтик к описанию модели" href="'.Url::to(['help/', 'modelname'=>$value]).'">'.$value.'</a>';
		echo $name.': '.$value.$hint; 
		echo "<br/>";
	}
	
}

?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'model-form', 'method' => 'post']); ?>
				<div>
				<label for="modelname">Введите имя модели с учетом регистра:<br/>
				<?=$ns?><input name="modelname" value="<?=$modelName?>"/>
				</label>
				<?= Html::submitButton('Go', ['class' => 'btn btn-primary']) ?>
				</div>	
				
                

            <?php ActiveForm::end(); ?>
        </div>
    </div>	
	<?php 	
	if ($modelName > ''):
	$modelName = $ns.$modelName;
	?>
	<div class="info">
	<hr/>
	<h2>Информация о модели <b><?=$modelName?></b></h2>
	<?php
	if(!@class_exists($modelName)) {
		echo '<div>Модель <b>'.$modelName.'</b> не найдена.</div>';
		//exit;
	}
	?>
	<UL>
		<li><a href="#inheritance">Наследование </a></li>
		<li><a href="#dbtable">Таблица модели</a></li>
		<li><a href="#metaproperties">Мета-свойства модели</a></li>
		<li><a href="#attributes">Атрибуты модели</a></li>
		<li><a href="#methods">Прикладные методы модели</a></li>
	</UL>
	
	<hr/>
	<?php
	///////////////////////////////////////////////////////////////////////////////
	echo '<div style="padding-bottom: 50px" id="inheritance"></div>';
	echo '<h3>Наследование</h3>';
	$path = [];
	
	$parent = $modelName;
	do {		
		$path[] = ($parent == $modelName)?"<b>".$parent."</b>":$parent;
		$parent = get_parent_class($parent);		
	} while($parent);
	
	
	
	$path =  '<table width="100%"><tr><td style="text-align:center">'.implode('</td></tr><tr><td style="text-align:center">'.Icon::show('arrow-down').'</td></tr><tr><td style="text-align:center">', $path).'</table>';
	
	echo $path;
	///////////////////////////////////////////////////////////////////////////////
	?>
	<div style="padding-bottom: 50px" id="dbtable"></div>
	<h3>Таблица</h3>
	<p>Имя таблицы базы данных, в которой хранятся значения атрибутов экземпляров модели</p>
	<p><table width="100%"><tr><td style="text-align:center"><b><?=$modelName::tableName()?></b></td></tr></table></p>
	<p>Значение доступно по статической функции: <code><?=$modelName?>::tableName();</code></p>
	
	
	<?php
	///////////////////////////////////////////////////////////////////////////////
	?>
	<div style="padding-bottom: 50px" id="metaproperties"></div>
	<h3>Мета-свойства модели</h3>
	<p>Ассоциативный массив со всеми мета-свойствами модели доступен по статической функции: <code><?=$modelName?>::meta();</code></p>
	<?php
	$meta = $modelName::meta();
	echo '<table class="list"><tr class="header"><td class="name">'.Icon::show('hand-o-right').'Имя</td><td>Значение</td></tr><tr class="header"><td colspan="2" class="description">'.Icon::show('arrow-up').'Описание</td><tr>';
	$odd = true;
	foreach($meta as $name => $value) {		
		if ($name == 'parent') continue;
		$odd = !$odd;
		echo '<tr'.($odd?' class="odd"':'').'><td class="name">'.Icon::show('hand-o-right').$name.'</td><td>'.$value.'</td></tr>';
		echo '<tr'.($odd?' class="odd"':'').'><td colspan="2" class="description">'.Icon::show('arrow-up').getMetaDescription($name).'</td><tr>';
	}
	echo '</table>';
	///////////////////////////////////////////////////////////////////////////////
	
	?>
	<div style="padding-bottom: 50px" id="attributes"></div>
	<h3>Атрибуты модели</h3>
	<p>Каждому атрибуту модели может соответствовать набор мета-свойств.</p>
	<p>Ассоциативный массив со всеми мета-свойствами конкретного атрибута доступны по статической функции <code><?=$modelName?>::getMeta('id');</code></p>
	<p>Эта же функия позволяет получит значение конкретного свойства атрибута <code><?=$modelName?>::getMeta('id', 'label');</code></p>
	<p>Если запрашиваемый атрибут или свойство атрибута не существует, то функция вернет <code>false</code></p>
	<?php
	//$properties = get_parent_class($modelName);	
	$meta = $modelName::attrMeta();
	
	$odd = true;
	echo '<table class="list"><tr class="header"><td colspan="2" class="name">'.Icon::show('hand-o-right').'Имя атрибута</td></tr><tr class="header"><td valign="top" style="text-align:right;" width="40px">'.Icon::show('arrow-up').'</td><td>Список мета-свойств атрибута</td><tr>';
	foreach($meta as $name => $mdata) {
		
		$odd = !$odd;
		echo '<tr'.($odd?' class="odd"':'').'><td colspan="2" class="name">'.Icon::show('hand-o-right').$name.'</td></tr>';
		echo '<tr'.($odd?' class="odd"':'').'><td valign="top" style="text-align:right;" width="40px">'.Icon::show('arrow-up').'</td><td>';
		renderAttributProperties($modelName, $name);
		echo '</td></td><tr>';
				
	}
	echo '</table>';
	
	///////////////////////////////////////////////////////////////////////////////
	
	?>
	<div style="padding-bottom: 50px" id="methods"></div>
	<h3>Методы прикладного расширения модели</h3>
	<p>Методы, которые определяются в контексте конкретного проекта</p>
	<p>Методы определяются через механизм Behavior</p>
	<p>Методы должны быть описаны в классе с именем формата [Имя модели]Behavior, например, для модели <a title="Перейтик к описанию модели" href="<?=Url::to(['help/', 'modelname'=>'ServicePrice'])?>">ServicePrice</a> создаетя класс ServicePriceBehavior в пространстве имен common\models.</p>
	<p>Описанные в этом классе методы автоматиччески подключаются к модели и становятся доступны в моделях как в frontend так и в backend.</p>	
	<p>Для отображения здесь каждому методу должен соответствовать дополнительный статический метод с постфиксом Description, который возвращает описание метода.</p>	
	<?php
		
		$has = false;
		$bclass = '\common\models\\'.$modelName::baseClassName().'Behavior';
		if (class_exists($bclass)) {
			$methods = get_class_methods($bclass);
			
			echo '<table class="list"><tr class="header"><td class="name">'.Icon::show('hand-o-right').'Имя метода</td></tr><tr class="header"><td class="description">'.Icon::show('arrow-up').'Описание</td><tr>';
			$odd = true;				
		
			//echo '<tr'.($odd?' class="odd"':'').'><td class="name">'.Icon::show('hand-o-right').$name.'</td></tr>';
			//echo '<tr'.($odd?' class="odd"':'').'><td class="description">'.Icon::show('arrow-up').getMetaDescription($name).'</td><tr>';
			
			foreach($methods as $method) {
				if(in_array($method.'Description', $methods)) {
					$odd = !$odd;					
					$m = $method.'Description';					
					$description = $bclass::$m();					
					echo '<tr'.($odd?' class="odd"':'').'><td class="name">'.Icon::show('hand-o-right').$method.'</td>';
					echo '<tr'.($odd?' class="odd"':'').'><td class="description">'.Icon::show('arrow-up').$description.'</td><tr>';
					$has = true;
				}
			}			
			echo '</table>';
		}
		
		if (!$has) echo "<p><b>Методы не найдены</b></p>";
		
	?>
	</div>	
	<?php endif?>
	
</div>
