<?php




/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Region;
use common\models\RegionSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Районы Москвы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Region::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить район', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Region::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'name',
				'label' => Region::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name'],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name'],				
				'footerOptions' => ['class' => 'col-name'],
				
				
				
			],
            ['attribute' => 'okrug_id',
				'label' => Region::getMeta('okrug_id', 'label'),
				'contentOptions' => ['class' => 'col-okrug_id'],
				'filterOptions' => ['class' => 'col-okrug_id'],
				'headerOptions' => ['class' => 'col-okrug_id'],				
				'footerOptions' => ['class' => 'col-okrug_id'],
				'filter' => Region::getExistsValues('okrug_id'),	
				'value' => function($data){ return $data['okrug_id_view'];},
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
