<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Region */

$this->title = 'Добавление района';
$this->params['breadcrumbs'][] = ['label' => \backend\models\Region::getMeta('','title'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
