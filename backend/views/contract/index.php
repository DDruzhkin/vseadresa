<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Document;
use common\models\DocumentSearch;
use common\models\Entity;
use common\models\Rip;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Список договоров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Document::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

		
		
		
        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					$stateTo = [
						Document::CONTRACT_STATE_DEACTIVE => [Document::CONTRACT_STATE_SIGNING], 
						Document::CONTRACT_STATE_SIGNING => [Document::CONTRACT_STATE_ACTIVE, Document::CONTRACT_STATE_DEACTIVE],
						Document::CONTRACT_STATE_ACTIVE => [Document::CONTRACT_STATE_DEACTIVE]
					];
		
					$id = $data['id'];
					$contract = Document::findOne([$id]);
					$items = [];
					if (!is_null($contract)) {
						$state = $contract -> state;
						
						$avStates = $stateTo[is_null($state)?0:$state];
						$item = false;
						foreach($avStates as $state) {
							switch($state) {
								case Document::CONTRACT_STATE_DEACTIVE: {
									$item = [
										'label' => 'Деактивировать',
										'url' => Url::toRoute(['deactivate', 'id' => $id])
									]; break;
								}
								case Document::CONTRACT_STATE_SIGNING: {
									$item = [
										'label' => 'На подписание',
										'url' => Url::toRoute(['tosigning', 'id' => $id])
									]; break;
								}
								case Document::CONTRACT_STATE_ACTIVE: {
									$item = [
										'label' => 'Активировать',
										'url' => Url::toRoute(['activate', 'id' => $id])
									]; break;
								}
							};
							$items[] = $item;
						}
					}
					
					//var_dump($items); exit;
					
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => $items
							],
						],
					]);
				}					
			],




            ['attribute' => 'doc_type_id',
				'label' => Document::getMeta('doc_type_id', 'label'),
				'contentOptions' => ['class' => 'col-doc_type_id '],
				'filterOptions' => ['class' => 'col-doc_type_id'],
				'headerOptions' => ['class' => 'col-doc_type_id '],				
				'footerOptions' => ['class' => 'col-doc_type_id '],
				'filter' => Document::getExistsValues('doc_type_id'),	
				'value' => function($data){ return $data['doc_type_id_view'];},
				
			],
			['attribute' => 'created_at',
				'label' => Document::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
			
            ['attribute' => 'number',
				'label' => Document::getMeta('number', 'label'),
				'contentOptions' => ['class' => 'col-number '],
				'filterOptions' => ['class' => 'col-number'],
				'headerOptions' => ['class' => 'col-number '],				
				'footerOptions' => ['class' => 'col-number '],
				
				
				
			],
            
            ['attribute' => 'owner_id',
				'label' => 'Со стороны портала',
				'contentOptions' => ['class' => 'col-owner_id '],
				'filterOptions' => ['class' => 'col-owner_id'],
				'headerOptions' => ['class' => 'col-owner_id '],				
				'footerOptions' => ['class' => 'col-owner_id '],
				
				'value' => 
					function($data)
					{ 
						//$value = Html::a($data['owner_id_view'],[Inflector::camel2id('CObject').'/update/'.$data['owner_id']]);
						$value = $data['owner_id'];
						
						$obj = Entity::byId($value);
						if (is_null($obj)) {
							$value = '';
						} else {
							switch($obj -> form) {
									case Rip::FORM_PERSON: {
										$value = $obj -> getCaption('%fname% %mname% %lname%');
										break;
									};
									case Rip::FORM_IP: {
										$value = $obj -> getCaption('ИП %fname% %mname% %lname%');
										break;
									};
									case Rip::FORM_COMPANY: {
										$value = $obj -> getCaption('%name%');
										break;
									};
							}
							
						}
						
						return $value;
					},
				'format' => 'raw',
			],
            ['attribute' => 'recipient_id',
				'label' => 'Владелец адреса',
				'contentOptions' => ['class' => 'col-recipient_id '],
				'filterOptions' => ['class' => 'col-recipient_id'],
				'headerOptions' => ['class' => 'col-recipient_id '],				
				'footerOptions' => ['class' => 'col-recipient_id '],
				
				'value' => 
			function($data)
			{ 
						$value = $data['recipient_id'];
						
						$obj = Entity::byId($value);
						if (is_null($obj)) {
							$value = '';
						} else {
							switch($obj -> form) {
									case Rip::FORM_PERSON: {
										$value = $obj -> getCaption('%fname% %mname% %lname%');
										break;
									};
									case Rip::FORM_IP: {
										$value = $obj -> getCaption('ИП %fname% %mname% %lname%');
										break;
									};
									case Rip::FORM_COMPANY: {
										$value = $obj -> getCaption('%name%');
										break;
									};
							}
							
						}
						
						
				return $value;
			},
				'format' => 'raw',
			],
            			
            ['attribute' => 'state',
				'label' => Document::getMeta('state', 'label'),
				'contentOptions' => ['class' => 'col-state '],
				'filterOptions' => ['class' => 'col-state'],
				'headerOptions' => ['class' => 'col-state '],				
				'footerOptions' => ['class' => 'col-state '],
				'value' => function($data) {
					$id = $data['id'];
					$document = Document::findOne([$id]);
					return $document ->contractState;
				}
				
				
				
			],

        //    ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
