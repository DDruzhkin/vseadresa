<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Option;
use common\models\OptionSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Список опций адресов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="option-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Option::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить Опцию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Option::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'icon',
				'label' => Option::getMeta('icon', 'label'),
				'contentOptions' => ['class' => 'col-icon '],
				'filterOptions' => ['class' => 'col-icon'],
				'headerOptions' => ['class' => 'col-icon '],				
				'footerOptions' => ['class' => 'col-icon '],
				
				'value' => function($data){ return $data -> getIcon('icon', $data -> icon, 'path');},
				'format' => 'image',
			],
            ['attribute' => 'alias',
				'label' => Option::getMeta('alias', 'label'),
				'contentOptions' => ['class' => 'col-alias '],
				'filterOptions' => ['class' => 'col-alias'],
				'headerOptions' => ['class' => 'col-alias '],				
				'footerOptions' => ['class' => 'col-alias '],
				
				
				
			],
            ['attribute' => 'name',
				'label' => Option::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				
				
				
			],
            ['attribute' => 'label',
				'label' => Option::getMeta('label', 'label'),
				'contentOptions' => ['class' => 'col-label '],
				'filterOptions' => ['class' => 'col-label'],
				'headerOptions' => ['class' => 'col-label '],				
				'footerOptions' => ['class' => 'col-label '],
				
				
				
			],
            ['attribute' => 'title',
				'label' => Option::getMeta('title', 'label'),
				'contentOptions' => ['class' => 'col-title hidden '],
				'filterOptions' => ['class' => 'col-title hidden'],
				'headerOptions' => ['class' => 'col-title hidden '],				
				'footerOptions' => ['class' => 'col-title hidden '],
				
				
				
			],
            ['attribute' => 'meta_description',
				'label' => Option::getMeta('meta_description', 'label'),
				'contentOptions' => ['class' => 'col-meta_description hidden '],
				'filterOptions' => ['class' => 'col-meta_description hidden'],
				'headerOptions' => ['class' => 'col-meta_description hidden '],				
				'footerOptions' => ['class' => 'col-meta_description hidden '],
				
				
				
			],
            ['attribute' => 'h1',
				'label' => Option::getMeta('h1', 'label'),
				'contentOptions' => ['class' => 'col-h1 hidden '],
				'filterOptions' => ['class' => 'col-h1 hidden'],
				'headerOptions' => ['class' => 'col-h1 hidden '],				
				'footerOptions' => ['class' => 'col-h1 hidden '],
				
				
				
			],
            ['attribute' => 'seo_text',
				'label' => Option::getMeta('seo_text', 'label'),
				'contentOptions' => ['class' => 'col-seo_text hidden '],
				'filterOptions' => ['class' => 'col-seo_text hidden'],
				'headerOptions' => ['class' => 'col-seo_text hidden '],				
				'footerOptions' => ['class' => 'col-seo_text hidden '],
				
				
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
