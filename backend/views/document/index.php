<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Document;
use common\models\DocumentSearch;
use common\models\Entity;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Список документов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Document::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить Документ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Document::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'id',
				'label' => Document::getMeta('id', 'label'),
				'contentOptions' => ['class' => 'col-id hidden '],
				'filterOptions' => ['class' => 'col-id hidden'],
				'headerOptions' => ['class' => 'col-id hidden '],				
				'footerOptions' => ['class' => 'col-id hidden '],
				
				
				
			],
            ['attribute' => 'created_at',
				'label' => Document::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'doc_type_id',
				'label' => Document::getMeta('doc_type_id', 'label'),
				'contentOptions' => ['class' => 'col-doc_type_id '],
				'filterOptions' => ['class' => 'col-doc_type_id'],
				'headerOptions' => ['class' => 'col-doc_type_id '],				
				'footerOptions' => ['class' => 'col-doc_type_id '],
				'filter' => Document::getExistsValues('doc_type_id'),	
				'value' => function($data){ return $data['doc_type_id_view'];},
				
			],
            ['attribute' => 'number',
				'label' => Document::getMeta('number', 'label'),
				'contentOptions' => ['class' => 'col-number '],
				'filterOptions' => ['class' => 'col-number'],
				'headerOptions' => ['class' => 'col-number '],				
				'footerOptions' => ['class' => 'col-number '],
				
				
				
			],
            ['attribute' => 'summa',
				'label' => Document::getMeta('summa', 'label'),
				'contentOptions' => ['class' => 'col-summa '],
				'filterOptions' => ['class' => 'col-summa'],
				'headerOptions' => ['class' => 'col-summa '],				
				'footerOptions' => ['class' => 'col-summa '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'owner_id',
				'label' => Document::getMeta('owner_id', 'label'),
				'contentOptions' => ['class' => 'col-owner_id '],
				'filterOptions' => ['class' => 'col-owner_id'],
				'headerOptions' => ['class' => 'col-owner_id '],				
				'footerOptions' => ['class' => 'col-owner_id '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['owner_id_view'],[Inflector::camel2id('CObject').'/update/'.$data['owner_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'recipient_id',
				'label' => Document::getMeta('recipient_id', 'label'),
				'contentOptions' => ['class' => 'col-recipient_id '],
				'filterOptions' => ['class' => 'col-recipient_id'],
				'headerOptions' => ['class' => 'col-recipient_id '],				
				'footerOptions' => ['class' => 'col-recipient_id '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['recipient_id_view'],[Inflector::camel2id('CObject').'/update/'.$data['recipient_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'info',
				'label' => Document::getMeta('info', 'label'),
				'contentOptions' => ['class' => 'col-info '],
				'filterOptions' => ['class' => 'col-info'],
				'headerOptions' => ['class' => 'col-info '],				
				'footerOptions' => ['class' => 'col-info '],
				
				
				
			],
            ['attribute' => 'state',
				'label' => Document::getMeta('state', 'label'),
				'contentOptions' => ['class' => 'col-state '],
				'filterOptions' => ['class' => 'col-state'],
				'headerOptions' => ['class' => 'col-state '],				
				'footerOptions' => ['class' => 'col-state '],
				
				
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
