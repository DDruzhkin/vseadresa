<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Document;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Document::getMeta('created_at_view', 'label'), 'hint'=>''.Document::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'doc_type_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Document::getMeta('doc_type_id', 'label'), 'items' => Document::getListValues('doc_type_id'), 'hint'=>''.Document::getMeta('doc_type_id', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'number' => ['type'=>Form::INPUT_TEXT, 'label'=>Document::getMeta('number', 'label'), 'hint'=>''.Document::getMeta('number', 'comment'), 'options'=>[]],
						'summa' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Document::getMeta('summa', 'label'), 'hint'=>''.Document::getMeta('summa', 'comment'), 'options'=>['clientOptions' => ['alias' =>  'decimal', 'groupSeparator' => ' ','autoGroup' => true],]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'owner_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Document::getMeta('owner_id_view', 'label'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('cobject/update/%owner_id%')]).'">'.Icon::show('link').'</a>'.Document::getMeta('owner_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'recipient_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Document::getMeta('recipient_id_view', 'label'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('cobject/update/%recipient_id%')]).'">'.Icon::show('link').'</a>'.Document::getMeta('recipient_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'info' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Document::getMeta('info', 'label'), 'hint'=>''.Document::getMeta('info', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'state' => ['type'=>Form::INPUT_TEXT, 'label'=>Document::getMeta('state', 'label'), 'hint'=>''.Document::getMeta('state', 'comment'), 'options'=>[]],

]




/* @var $this yii\web\View */
/* @var $model backend\models\Document */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="document-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Document::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
