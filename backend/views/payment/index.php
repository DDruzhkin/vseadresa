<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Payment;
use common\models\Entity;
use common\models\PaymentSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Платежи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Payment::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить Платеж', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Payment::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'date',
				'label' => Payment::getMeta('date', 'label'),
				'contentOptions' => ['class' => 'col-date date'],
				'filterOptions' => ['class' => 'col-date'],
				'headerOptions' => ['class' => 'col-date date'],				
				'footerOptions' => ['class' => 'col-date date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'date',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'ptype',
				'label' => Payment::getMeta('ptype', 'label'),
				'contentOptions' => ['class' => 'col-ptype '],
				'filterOptions' => ['class' => 'col-ptype'],
				'headerOptions' => ['class' => 'col-ptype '],				
				'footerOptions' => ['class' => 'col-ptype '],
				
				
				
			],
            ['attribute' => 'value',
				'label' => Payment::getMeta('value', 'label'),
				'contentOptions' => ['class' => 'col-value '],
				'filterOptions' => ['class' => 'col-value'],
				'headerOptions' => ['class' => 'col-value '],				
				'footerOptions' => ['class' => 'col-value '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'invoice_id',
				'label' => Payment::getMeta('invoice_id', 'label'),
				'contentOptions' => ['class' => 'col-invoice_id hidden '],
				'filterOptions' => ['class' => 'col-invoice_id hidden'],
				'headerOptions' => ['class' => 'col-invoice_id hidden '],				
				'footerOptions' => ['class' => 'col-invoice_id hidden '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['invoice_id_view'],[Inflector::camel2id('Document').'/update/'.$data['invoice_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'info',
				'label' => Payment::getMeta('info', 'label'),
				'contentOptions' => ['class' => 'col-info hidden '],
				'filterOptions' => ['class' => 'col-info hidden'],
				'headerOptions' => ['class' => 'col-info hidden '],				
				'footerOptions' => ['class' => 'col-info hidden '],
				
				
				
			],
            ['attribute' => 'loaded_at',
				'label' => Payment::getMeta('loaded_at', 'label'),
				'contentOptions' => ['class' => 'col-loaded_at hidden '],
				'filterOptions' => ['class' => 'col-loaded_at hidden'],
				'headerOptions' => ['class' => 'col-loaded_at hidden '],				
				'footerOptions' => ['class' => 'col-loaded_at hidden '],
				
				
				
			],
            ['attribute' => 'payer_name',
				'label' => Payment::getMeta('payer_name', 'label'),
				'contentOptions' => ['class' => 'col-payer_name '],
				'filterOptions' => ['class' => 'col-payer_name'],
				'headerOptions' => ['class' => 'col-payer_name '],				
				'footerOptions' => ['class' => 'col-payer_name '],
				
				
				
			],
            ['attribute' => 'invoice_date',
				'label' => Payment::getMeta('invoice_date', 'label'),
				'contentOptions' => ['class' => 'col-invoice_date date'],
				'filterOptions' => ['class' => 'col-invoice_date'],
				'headerOptions' => ['class' => 'col-invoice_date date'],				
				'footerOptions' => ['class' => 'col-invoice_date date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'invoice_date',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'invoice_number',
				'label' => Payment::getMeta('invoice_number', 'label'),
				'contentOptions' => ['class' => 'col-invoice_number '],
				'filterOptions' => ['class' => 'col-invoice_number'],
				'headerOptions' => ['class' => 'col-invoice_number '],				
				'footerOptions' => ['class' => 'col-invoice_number '],
				
				
				
			],
            ['attribute' => 'recipient_name',
				'label' => Payment::getMeta('recipient_name', 'label'),
				'contentOptions' => ['class' => 'col-recipient_name '],
				'filterOptions' => ['class' => 'col-recipient_name'],
				'headerOptions' => ['class' => 'col-recipient_name '],				
				'footerOptions' => ['class' => 'col-recipient_name '],
				
				
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
