<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AddressSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="address-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'gps') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'status_changed_at') ?>

    <?= $form->field($model, 'adr_index') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'status_id') ?>

    <?php // echo $form->field($model, 'owner_id') ?>

    <?php // echo $form->field($model, 'nalog_id') ?>

    <?php // echo $form->field($model, 'metro_id') ?>

    <?php // echo $form->field($model, 'region_id') ?>

    <?php // echo $form->field($model, 'okrug_id') ?>

    <?php // echo $form->field($model, 'demo') ?>

    <?php // echo $form->field($model, 'parameters') ?>

    <?php // echo $form->field($model, 'square') ?>

    <?php // echo $form->field($model, 'rating') ?>

    <?php // echo $form->field($model, 'price_type') ?>

    <?php // echo $form->field($model, 'options') ?>

    <?php // echo $form->field($model, 'photo_date') ?>

    <?php // echo $form->field($model, 'subscribe_to') ?>

    <?php // echo $form->field($model, 'subscribe_id') ?>

    <?php // echo $form->field($model, 'photo_order_id') ?>

    <?php // echo $form->field($model, 'quickable') ?>

    <?php // echo $form->field($model, 'info') ?>

    <?php // echo $form->field($model, 'price6') ?>

    <?php // echo $form->field($model, 'price11') ?>

    <?php // echo $form->field($model, 'price6min') ?>

    <?php // echo $form->field($model, 'price11min') ?>

    <?php // echo $form->field($model, 'price6max') ?>

    <?php // echo $form->field($model, 'price11max') ?>

    <?php // echo $form->field($model, 'photos') ?>

    <?php // echo $form->field($model, 'files') ?>

    <?php // echo $form->field($model, 'range') ?>

    <?php // echo $form->field($model, 'seo_id') ?>

    <?php // echo $form->field($model, 'h1') ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'meta_description') ?>

    <?php // echo $form->field($model, 'seo_text') ?>

    <?php // echo $form->field($model, 'enabled') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
