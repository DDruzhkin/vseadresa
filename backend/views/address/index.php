<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Address;
use backend\models\Option;
use common\models\AddressSearch;
use common\models\Entity;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Адреса';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Address::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить Адрес', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Address::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'id',
				'label' => Address::getMeta('id', 'label'),
				'contentOptions' => ['class' => 'col-id '],
				'filterOptions' => ['class' => 'col-id'],
				'headerOptions' => ['class' => 'col-id '],				
				'footerOptions' => ['class' => 'col-id '],
				
				
				
			],
            ['attribute' => 'gps',
				'label' => Address::getMeta('gps', 'label'),
				'contentOptions' => ['class' => 'col-gps hidden '],
				'filterOptions' => ['class' => 'col-gps hidden'],
				'headerOptions' => ['class' => 'col-gps hidden '],				
				'footerOptions' => ['class' => 'col-gps hidden '],
				
				
				
			],
            ['attribute' => 'created_at',
				'label' => Address::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'adr_index',
				'label' => Address::getMeta('adr_index', 'label'),
				'contentOptions' => ['class' => 'col-adr_index hidden '],
				'filterOptions' => ['class' => 'col-adr_index hidden'],
				'headerOptions' => ['class' => 'col-adr_index hidden '],				
				'footerOptions' => ['class' => 'col-adr_index hidden '],
				
				
				
			],
            ['attribute' => 'address',
				'label' => Address::getMeta('address', 'label'),
				'contentOptions' => ['class' => 'col-address '],
				'filterOptions' => ['class' => 'col-address'],
				'headerOptions' => ['class' => 'col-address '],				
				'footerOptions' => ['class' => 'col-address '],
				
				
				
			],
            ['attribute' => 'status_id',
				'label' => Address::getMeta('status_id', 'label'),
				'contentOptions' => ['class' => 'col-status_id '],
				'filterOptions' => ['class' => 'col-status_id'],
				'headerOptions' => ['class' => 'col-status_id '],				
				'footerOptions' => ['class' => 'col-status_id '],
				'filter' => Address::getExistsValues('status_id'),	
				'value' => function($data){ return $data['status_id_view'];},
				
			],
            ['attribute' => 'owner_id',
				'label' => Address::getMeta('owner_id', 'label'),
				'contentOptions' => ['class' => 'col-owner_id '],
				'filterOptions' => ['class' => 'col-owner_id'],
				'headerOptions' => ['class' => 'col-owner_id '],				
				'footerOptions' => ['class' => 'col-owner_id '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['owner_id_view'],[Inflector::camel2id('User').'/update/'.$data['owner_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'nalog_id',
				'label' => Address::getMeta('nalog_id', 'label'),
				'contentOptions' => ['class' => 'col-nalog_id '],
				'filterOptions' => ['class' => 'col-nalog_id'],
				'headerOptions' => ['class' => 'col-nalog_id '],				
				'footerOptions' => ['class' => 'col-nalog_id '],
				'filter' => Address::getExistsValues('nalog_id'),	
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['nalog_id_view'],[Inflector::camel2id('Nalog').'/update/'.$data['nalog_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'metro_id',
				'label' => Address::getMeta('metro_id', 'label'),
				'contentOptions' => ['class' => 'col-metro_id hidden '],
				'filterOptions' => ['class' => 'col-metro_id hidden'],
				'headerOptions' => ['class' => 'col-metro_id hidden '],				
				'footerOptions' => ['class' => 'col-metro_id hidden '],
				'filter' => Address::getExistsValues('metro_id'),	
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['metro_id_view'],[Inflector::camel2id('Metro').'/update/'.$data['metro_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'region_id',
				'label' => Address::getMeta('region_id', 'label'),
				'contentOptions' => ['class' => 'col-region_id hidden '],
				'filterOptions' => ['class' => 'col-region_id hidden'],
				'headerOptions' => ['class' => 'col-region_id hidden '],				
				'footerOptions' => ['class' => 'col-region_id hidden '],
				'filter' => Address::getExistsValues('region_id'),	
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['region_id_view'],[Inflector::camel2id('Region').'/update/'.$data['region_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'okrug_id',
				'label' => Address::getMeta('okrug_id', 'label'),
				'contentOptions' => ['class' => 'col-okrug_id '],
				'filterOptions' => ['class' => 'col-okrug_id'],
				'headerOptions' => ['class' => 'col-okrug_id '],				
				'footerOptions' => ['class' => 'col-okrug_id '],
				'filter' => Address::getExistsValues('okrug_id'),	
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['okrug_id_view'],[Inflector::camel2id('Okrug').'/update/'.$data['okrug_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'demo',
				'label' => Address::getMeta('demo', 'label'),
				'contentOptions' => ['class' => 'col-demo hidden '],
				'filterOptions' => ['class' => 'col-demo hidden'],
				'headerOptions' => ['class' => 'col-demo hidden '],				
				'footerOptions' => ['class' => 'col-demo hidden '],
				
				
				
			],
            ['attribute' => 'square',
				'label' => Address::getMeta('square', 'label'),
				'contentOptions' => ['class' => 'col-square hidden '],
				'filterOptions' => ['class' => 'col-square hidden'],
				'headerOptions' => ['class' => 'col-square hidden '],				
				'footerOptions' => ['class' => 'col-square hidden '],
				
				
				
			],
            ['attribute' => 'options',
				'label' => Address::getMeta('options', 'label'),
				'contentOptions' => ['class' => 'col-options hidden '],
				'filterOptions' => ['class' => 'col-options hidden'],
				'headerOptions' => ['class' => 'col-options hidden '],				
				'footerOptions' => ['class' => 'col-options hidden '],
				
				'value' => 
				function($data){ 
					$model = Address::findone($data['id']);
					if ($model) {
						return $model -> getOptions('@listitemtemplate');
					}
				},
				'format' => 'raw',
			],
            ['attribute' => 'photo_date',
				'label' => Address::getMeta('photo_date', 'label'),
				'contentOptions' => ['class' => 'col-photo_date hidden '],
				'filterOptions' => ['class' => 'col-photo_date hidden'],
				'headerOptions' => ['class' => 'col-photo_date hidden '],				
				'footerOptions' => ['class' => 'col-photo_date hidden '],
				
				
				
			],
            ['attribute' => 'subscribe_to',
				'label' => Address::getMeta('subscribe_to', 'label'),
				'contentOptions' => ['class' => 'col-subscribe_to hidden date'],
				'filterOptions' => ['class' => 'col-subscribe_to hidden'],
				'headerOptions' => ['class' => 'col-subscribe_to hidden date'],				
				'footerOptions' => ['class' => 'col-subscribe_to hidden date'],
				
				
				
			],
            ['attribute' => 'subscribe_id',
				'label' => Address::getMeta('subscribe_id', 'label'),
				'contentOptions' => ['class' => 'col-subscribe_id hidden '],
				'filterOptions' => ['class' => 'col-subscribe_id hidden'],
				'headerOptions' => ['class' => 'col-subscribe_id hidden '],				
				'footerOptions' => ['class' => 'col-subscribe_id hidden '],
				
				'value' => function($data){ return $data['subscribe_id_view'];},
				
			],
            ['attribute' => 'quickable',
				'label' => Address::getMeta('quickable', 'label'),
				'contentOptions' => ['class' => 'col-quickable hidden '],
				'filterOptions' => ['class' => 'col-quickable hidden'],
				'headerOptions' => ['class' => 'col-quickable hidden '],				
				'footerOptions' => ['class' => 'col-quickable hidden '],
				
				
				
			],
            ['attribute' => 'info',
				'label' => Address::getMeta('info', 'label'),
				'contentOptions' => ['class' => 'col-info hidden '],
				'filterOptions' => ['class' => 'col-info hidden'],
				'headerOptions' => ['class' => 'col-info hidden '],				
				'footerOptions' => ['class' => 'col-info hidden '],
				
				
				
			],
            ['attribute' => 'price_first_6',
				'label' => Address::getMeta('price_first_6', 'label'),
				'contentOptions' => ['class' => 'col-price6 hidden '],
				'filterOptions' => ['class' => 'col-price6 hidden'],
				'headerOptions' => ['class' => 'col-price6 hidden '],				
				'footerOptions' => ['class' => 'col-price6 hidden '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'price_first_11',
				'label' => Address::getMeta('price_first_11', 'label'),
				'contentOptions' => ['class' => 'col-price11 hidden '],
				'filterOptions' => ['class' => 'col-price11 hidden'],
				'headerOptions' => ['class' => 'col-price11 hidden '],				
				'footerOptions' => ['class' => 'col-price11 hidden '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'price6min',
				'label' => Address::getMeta('price6min', 'label'),
				'contentOptions' => ['class' => 'col-price6min hidden '],
				'filterOptions' => ['class' => 'col-price6min hidden'],
				'headerOptions' => ['class' => 'col-price6min hidden '],				
				'footerOptions' => ['class' => 'col-price6min hidden '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'price11min',
				'label' => Address::getMeta('price11min', 'label'),
				'contentOptions' => ['class' => 'col-price11min hidden '],
				'filterOptions' => ['class' => 'col-price11min hidden'],
				'headerOptions' => ['class' => 'col-price11min hidden '],				
				'footerOptions' => ['class' => 'col-price11min hidden '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'price6max',
				'label' => Address::getMeta('price6max', 'label'),
				'contentOptions' => ['class' => 'col-price6max hidden '],
				'filterOptions' => ['class' => 'col-price6max hidden'],
				'headerOptions' => ['class' => 'col-price6max hidden '],				
				'footerOptions' => ['class' => 'col-price6max hidden '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'price11max',
				'label' => Address::getMeta('price11max', 'label'),
				'contentOptions' => ['class' => 'col-price11max hidden '],
				'filterOptions' => ['class' => 'col-price11max hidden'],
				'headerOptions' => ['class' => 'col-price11max hidden '],				
				'footerOptions' => ['class' => 'col-price11max hidden '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'photos',
				'label' => Address::getMeta('photos', 'label'),
				'contentOptions' => ['class' => 'col-photos hidden '],
				'filterOptions' => ['class' => 'col-photos hidden'],
				'headerOptions' => ['class' => 'col-photos hidden '],				
				'footerOptions' => ['class' => 'col-photos hidden '],
				
				
				
			],
            ['attribute' => 'files',
				'label' => Address::getMeta('files', 'label'),
				'contentOptions' => ['class' => 'col-files hidden '],
				'filterOptions' => ['class' => 'col-files hidden'],
				'headerOptions' => ['class' => 'col-files hidden '],				
				'footerOptions' => ['class' => 'col-files hidden '],
				
				
				
			],
            ['attribute' => 'range',
				'label' => Address::getMeta('range', 'label'),
				'contentOptions' => ['class' => 'col-range hidden '],
				'filterOptions' => ['class' => 'col-range hidden'],
				'headerOptions' => ['class' => 'col-range hidden '],				
				'footerOptions' => ['class' => 'col-range hidden '],
				
				
				
			],
            ['attribute' => 'h1',
				'label' => Address::getMeta('h1', 'label'),
				'contentOptions' => ['class' => 'col-h1 hidden '],
				'filterOptions' => ['class' => 'col-h1 hidden'],
				'headerOptions' => ['class' => 'col-h1 hidden '],				
				'footerOptions' => ['class' => 'col-h1 hidden '],
				
				
				
			],
            ['attribute' => 'title',
				'label' => Address::getMeta('title', 'label'),
				'contentOptions' => ['class' => 'col-title hidden '],
				'filterOptions' => ['class' => 'col-title hidden'],
				'headerOptions' => ['class' => 'col-title hidden '],				
				'footerOptions' => ['class' => 'col-title hidden '],
				
				
				
			],
            ['attribute' => 'meta_description',
				'label' => Address::getMeta('meta_description', 'label'),
				'contentOptions' => ['class' => 'col-meta_description hidden '],
				'filterOptions' => ['class' => 'col-meta_description hidden'],
				'headerOptions' => ['class' => 'col-meta_description hidden '],				
				'footerOptions' => ['class' => 'col-meta_description hidden '],
				
				
				
			],
            ['attribute' => 'seo_text',
				'label' => Address::getMeta('seo_text', 'label'),
				'contentOptions' => ['class' => 'col-seo_text hidden '],
				'filterOptions' => ['class' => 'col-seo_text hidden'],
				'headerOptions' => ['class' => 'col-seo_text hidden '],				
				'footerOptions' => ['class' => 'col-seo_text hidden '],
				
				
				
			],
            ['attribute' => 'enabled',
				'label' => Address::getMeta('enabled', 'label'),
				'contentOptions' => ['class' => 'col-enabled '],
				'filterOptions' => ['class' => 'col-enabled'],
				'headerOptions' => ['class' => 'col-enabled '],				
				'footerOptions' => ['class' => 'col-enabled '],
				
				
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
