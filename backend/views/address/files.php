<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;

use common\models\Address;

$this->title = "Документы";
$this->params['breadcrumbs'][] = ['label' => 'Адреса', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model -> address, 'url' => ['update', 'id' => $model -> id]];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="address-view">

    <h1><?= Html::encode($this->title) ?></h1>

</div>

<?php	

?>
<div style="clear:both"></div>
<?php 
$form = ActiveForm::begin(['id' => 'address-files-form', 'action' => Url::to(['address/files', 'id' => '9291']), 'method' => 'post', 'options' => ['enctype' => 'multipart/form-data']]);

if (is_array($files) && count($files)) {
?>
<table class="table table-striped table-bordered"><thead><tr><th>Номер</th><th>Дата</th><th>Название</th><th></th><th style="text-align:center;">Удалить</th></tr></thead>
<?php
$index = 1;
foreach($files as $file) {
//	var_dump($file); exit;
		$number = ArrayHelper::getValue($file, 'number');
		$date = ArrayHelper::getValue($file, 'date');
		if ($date) {
			$date = Yii::$app -> formatter -> asDate(strtotime($date));
		}
		$fn = ArrayHelper::getValue($file, 'filename');
		echo  '
		<tr>
		<td>
		'.$form->field($model, 'numbers['.$index.']')->textInput(['value' => $number]) -> label(false).'
		</td>
		<td>'.$form->field($model, 'dates['.$index.']')->textInput(['value' => $date]) -> label(false).'</td>
		<td>'.$form->field($model, 'names['.$index.']')->textInput(['value' => ArrayHelper::getValue($file, 'name')]) -> label(false).'</td>		
		<td style="text-align:center;">'.
			$form->field($model, 'filenames['.$index.']')->hiddenInput(['value' => $fn])->label(false).
			Html::a(Icon::show('download'), Url::to(['/files/documents/uploaded/'.$fn]), ['title' => 'Скачать файл']).		
		'
		</td>
		<td style="text-align:center; padding-top:20px;"><input type="checkbox" name="'.Address::baseClassName().'[removes]['.$index.']"/></td>
		</tr>
		';	
	$index++;
}

?>

</table>
	<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary'])?>
<?php
} else {
	echo "Нет документов";
}
?>

<h2> Добавить документы</h2>

    <?= $form->field($model, 'files[]')->fileInput(['multiple' => true]) -> label(false) ?> 
 
<?php ActiveForm::end() ?>



<?php
$script = <<< JS
	$('#address-files-form input[type=file]').change(function(){		
		$('#address-files-form').submit();
	});

JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>

