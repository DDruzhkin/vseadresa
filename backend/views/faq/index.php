<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Faq;
use common\models\FaqSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Частые вопросы и ответы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Faq::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Faq::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'created_at',
				'label' => Faq::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'question',
				'label' => Faq::getMeta('question', 'label'),
				'contentOptions' => ['class' => 'col-question '],
				'filterOptions' => ['class' => 'col-question'],
				'headerOptions' => ['class' => 'col-question '],				
				'footerOptions' => ['class' => 'col-question '],
				
				
				
			],
            ['attribute' => 'answer',
				'label' => Faq::getMeta('answer', 'label'),
				'contentOptions' => ['class' => 'col-answer hidden '],
				'filterOptions' => ['class' => 'col-answer hidden'],
				'headerOptions' => ['class' => 'col-answer hidden '],				
				'footerOptions' => ['class' => 'col-answer hidden '],
				
				
				
			],
            ['attribute' => 'role',
				'label' => Faq::getMeta('role', 'label'),
				'contentOptions' => ['class' => 'col-role '],
				'filterOptions' => ['class' => 'col-role'],
				'headerOptions' => ['class' => 'col-role '],				
				'footerOptions' => ['class' => 'col-role '],
				'filter' => Faq::getList('role'),	
				
				
			],
            ['attribute' => 'h1',
				'label' => Faq::getMeta('h1', 'label'),
				'contentOptions' => ['class' => 'col-h1 hidden '],
				'filterOptions' => ['class' => 'col-h1 hidden'],
				'headerOptions' => ['class' => 'col-h1 hidden '],				
				'footerOptions' => ['class' => 'col-h1 hidden '],
				
				
				
			],
            ['attribute' => 'title',
				'label' => Faq::getMeta('title', 'label'),
				'contentOptions' => ['class' => 'col-title hidden '],
				'filterOptions' => ['class' => 'col-title hidden'],
				'headerOptions' => ['class' => 'col-title hidden '],				
				'footerOptions' => ['class' => 'col-title hidden '],
				
				
				
			],
            ['attribute' => 'meta_description',
				'label' => Faq::getMeta('meta_description', 'label'),
				'contentOptions' => ['class' => 'col-meta_description hidden '],
				'filterOptions' => ['class' => 'col-meta_description hidden'],
				'headerOptions' => ['class' => 'col-meta_description hidden '],				
				'footerOptions' => ['class' => 'col-meta_description hidden '],
				
				
				
			],
            ['attribute' => 'meta_keywords',
				'label' => Faq::getMeta('meta_keywords', 'label'),
				'contentOptions' => ['class' => 'col-meta_keywords hidden '],
				'filterOptions' => ['class' => 'col-meta_keywords hidden'],
				'headerOptions' => ['class' => 'col-meta_keywords hidden '],				
				'footerOptions' => ['class' => 'col-meta_keywords hidden '],
				
				
				
			],
            ['attribute' => 'seo_text',
				'label' => Faq::getMeta('seo_text', 'label'),
				'contentOptions' => ['class' => 'col-seo_text hidden '],
				'filterOptions' => ['class' => 'col-seo_text hidden'],
				'headerOptions' => ['class' => 'col-seo_text hidden '],				
				'footerOptions' => ['class' => 'col-seo_text hidden '],
				
				
				
			],
            ['attribute' => 'enabled',
				'label' => Faq::getMeta('enabled', 'label'),
				'contentOptions' => ['class' => 'col-enabled hidden '],
				'filterOptions' => ['class' => 'col-enabled hidden'],
				'headerOptions' => ['class' => 'col-enabled hidden '],				
				'footerOptions' => ['class' => 'col-enabled hidden '],
				
				
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
