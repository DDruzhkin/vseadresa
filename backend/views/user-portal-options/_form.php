<?php
if (YII_DEBUG) $startTime = microtime(true);
use backend\models\User;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;


?>
<style>


#properties ul li {
	clear:both;
	list-style: none;
}
#properties ul li.col-2 div.form-group {
	width: 45%;
	float: left;
	margin-right: 20px;
}
#properties ul.level1 span.caption {
	font-size: 16pt;
}

#properties ul.level2 span.caption {
	font-size: 14pt;
}

</style>
<div class="user-form">

<?php
		

	$form = ActiveForm::begin();
?>
<div id="properties">
<ul class="level1">


	<li><span class="caption">Главная страница</span>	
		<ul class="level2">	
			<li><span class="caption">Статистика</span>
				<ul class="level3">				
					<li class="col-1">
					<?=$form->field($model, 'home_stat_show')->checkBox()->hint($model -> getMeta('home_stat_show', 'comment'))?>
					</li>
					<li class="col-2">
					<?=$form->field($model, 'home_stat_address_count_show')->checkBox()->hint($model -> getMeta('home_stat_address_count_show', 'comment'))?>
					<?=$form->field($model, 'home_stat_address_count_value')->textInput(['type'=>'number'])->hint($model -> getMeta('home_stat_address_count_value', 'comment'))?>					
					</li>
					<li class="col-2">
					<?=$form->field($model, 'home_stat_owner_count_show')->checkBox()->hint($model -> getMeta('home_stat_owner_count_show', 'comment'))?>
					<?=$form->field($model, 'home_stat_owner_count_value')->textInput(['type'=>'number'])->hint($model -> getMeta('home_stat_owner_count_value', 'comment'))?>					
					</li>
					<li class="col-2">
					<?=$form->field($model, 'home_stat_contract_count_show')->checkBox()->hint($model -> getMeta('home_stat_contract_count_show', 'comment'))?>
					<?=$form->field($model, 'home_stat_contract_count_value')->textInput(['type'=>'number'])->hint($model -> getMeta('home_stat_contract_count_value', 'comment'))?>					
					</li>
				</ul>	
			</li>	
			<li><span class="caption">Отзывы</span>
				<ul class="level3">		
					<li class="col-2">				
					<?=$form->field($model, 'home_reviews_show')->checkBox()->hint($model -> getMeta('home_reviews_show', 'comment'))?>
					<?=$form->field($model, 'home_reviews_count')->textInput(['type'=>'number'])->hint($model -> getMeta('home_reviews_count', 'comment'))?>					
					</li>
				</ul>	
			</li>	
			<li><span class="caption">Частые вопросы</span>
				<ul class="level3">		
					<li class="col-2">				
					<?=$form->field($model, 'home_faq_show')->checkBox()->hint($model -> getMeta('home_faq_show', 'comment'))?>
					<?=$form->field($model, 'home_faq_count')->textInput(['type'=>'number'])->hint($model -> getMeta('home_faq_count', 'comment'))?>					
					</li>
				</ul>	
			</li>	
			<li><span class="caption">Новости</span>
				<ul class="level3">		
					<li class="col-1">				
					<?=$form->field($model, 'home_news_show')->checkBox()->hint($model -> getMeta('home_news_show', 'comment'))?>					
					</li>
				</ul>	
			</li>
			
			<li><span class="caption">SEO</span>
				<ul class="level3">		
					<li class="col-1">				
					<?=$form->field($model, 'home_seo_title')->textInput()->hint($model -> getMeta('home_seo_title', 'comment'))?>					
					</li>
					<li class="col-1">				
					<?=$form->field($model, 'home_seo_h1')->textInput()->hint($model -> getMeta('home_seo_h1', 'comment'))?>					
					</li>					
					<li class="col-1">				
					<?=$form->field($model, 'home_seo_description')->textInput()->hint($model -> getMeta('home_seo_description', 'comment'))?>					
					</li>
					<li class="col-1">				
					<?=$form->field($model, 'home_seo_text')->textarea(['rows' => '6'])->hint($model -> getMeta('home_seo_text', 'comment'))?>
					</li>
					
				</ul>	
			</li>
			
			
		</ul>				
	</li>	

	<li><span class="caption">Баннеры</span>	
		<ul class="level2">	
			<li><span class="caption">Верхний баннер</span>
				<ul class="level3">				
					<li class="col-1">
						<?=$form->field($model, 'top_banner_show')->checkBox()->hint($model -> getMeta('top_banner_show', 'comment'))?>
					</li>
					<li class="col-1">
						<?=$form->field($model, 'top_banner_value')->textarea(['rows' => '6'])->hint($model -> getMeta('top_banner_value', 'comment'))?>
					</li>
				</ul>
			</li>
		</ul>	
	</li>		
	
	<li><span class="caption">Адреса</span>
		<ul class="level2">
			<li><span class="caption">Цены</span>
				<ul class="level3">
					<li class="col-2"><?=$form->field($model, 'address_price_6min')->textInput()->hint($model -> getMeta('address_price_6min', 'comment'))?>
					<?=$form->field($model, 'address_price_6max')->textInput()->hint($model -> getMeta('address_price_6max', 'comment'))?></li>
					<li class="col-2"><?=$form->field($model, 'address_price_11min')->textInput()->hint($model -> getMeta('address_price_11min', 'comment'))?>
					<?=$form->field($model, 'address_price_11max')->textInput()->hint($model -> getMeta('address_price_11max', 'comment'))?></li>
				</ul>	
			</li>
			<li><span class="caption">Список адресов</span>
				<ul class="level3">
					<li class="col-1"><?=$form->field($model, 'address_list_seo_title')->textInput()->hint($model -> getMeta('address_list_seo_title', 'comment'))?></li>
					<li class="col-1"><?=$form->field($model, 'address_list_seo_h1')->textInput()->hint($model -> getMeta('address_list_seo_h1', 'comment'))?></li>
					<li class="col-1"><?=$form->field($model, 'address_list_seo_description')->textInput()->hint($model -> getMeta('address_list_seo_description', 'comment'))?></li>
					<li class="col-1"><?=$form->field($model, 'address_list_seo_text')->textarea(['rows' => '6'])->hint($model -> getMeta('address_list_seo_text', 'comment'))?></li>
				
				</ul>	
			</li>			
			<li><span class="caption">Прайс-лист адресов</span>
				<ul class="level3">
					<li class="col-1"><?=$form->field($model, 'address_pricelist_seo_title')->textInput()->hint($model -> getMeta('address_pricelist_seo_title', 'comment'))?></li>
					<li class="col-1"><?=$form->field($model, 'address_pricelist_seo_h1')->textInput()->hint($model -> getMeta('address_pricelist_seo_h1', 'comment'))?></li>
					<li class="col-1"><?=$form->field($model, 'address_pricelist_seo_description')->textInput()->hint($model -> getMeta('address_pricelist_seo_description', 'comment'))?></li>
				</ul>	
			</li>			
			
		</ul>	
	</li>
	
	
	
	
	
	<li><span class="caption">Уведомления</span>
		<ul class="level2">
			<li><span class="caption">Регистрация нового пользователя</span>
				<ul class="level3">
					<li class="col-2">				
					<?=$form->field($model, 'notifications_registration_enabled')->checkBox()->hint($model -> getMeta('notifications_registration_enabled', 'comment'))?>
					<?=$form->field($model, 'notifications_registration_email')->textInput(['type'=>'email'])->hint($model -> getMeta('notifications_registration_email', 'comment'))?>					
					</li>
					
					<li class="col-2">				
					<?=$form->field($model, 'notifications_newreview_enabled')->checkBox()->hint($model -> getMeta('notifications_newreview_enabled', 'comment'))?>
					<?=$form->field($model, 'notifications_newreview_email')->textInput(['type'=>'email'])->hint($model -> getMeta('notifications_newreview_email', 'comment'))?>					
					</li>
				</ul>	
			</li>
	</li>
</ul>	
</div>
<hr style="clear:both"/>
<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
<hr/>
<?php
	ActiveForm::end();

	
	

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
