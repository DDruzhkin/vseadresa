<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = 'Параметры портала';
$this->params['breadcrumbs'][] = ['label' => 'Параметры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this -> title;
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
	echo $this->render('_form', [
        'model' => $model,
		'user' => $user,
    ]) 
	
	?>		
	
<!--
<?php
// Example of data.
$tree = [
	
	['title' => 'Адреса', 'folder' => true, 'children' => [
		['title' => 'Цены', 'folder' => true, 'children' => [
			[
				'title' => 'Для срока 6 месяцев', 'folder' => true, 
				'fields' =>['address.prices.6.min', 'address.prices.6.max'],				
				
			],
			['title' => 'Для срока 11 месяцев', 'key' => 1, 'id' => 'f11', 'folder' => true, 'fields' =>['address.prices.11.min', 'address.prices.11.max']]
		]]
	]]
];



?>
<table width="100%" border="1">
<tr><td width="50%" style="text-align:left;">
<?php
echo \wbraganca\fancytree\FancytreeWidget::widget([
	'options' =>[
		'source' => $tree,
		'extensions' => ['dnd'],
		'dnd' => [
			'preventVoidMoves' => true,
			'preventRecursiveMoves' => true,
			'autoExpandMS' => 400,			
		],
		
	]
]);
?>
</td>
<td width="50%">

</td></tr>
</table>
-->
	
	
	
    <?php
/*	$this->render('_form', [
        'model' => $model,
    ]) 
*/	
	?>

</div>
