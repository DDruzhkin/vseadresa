<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use kartik\nav\NavX;
use kartik\icons\Icon;

Icon::map($this); 
AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'VseAdresa.Pro ',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    if (Yii::$app->user->isGuest) {

       $menuItems = [

       ];
    } else {
       $menuItems = [
	[
	
		'label' => 'Управление',
		'items' => [

			[
				'label' => 'Порталом', 
				'items' => [					
					['label' => 'Справочник услуг', 'url' => Url::to(['service-portal/'])],
					['label' => 'Реквизиты', 'url' => Url::to(['requisites/'])],					
					['label' => 'Параметры', 'url' => Url::to(['options/'])],					
					['label' => 'Шаблоны писем', 'url' => Url::to(['/email-template'])],					
				],
			],

			[
				'label' => 'Адресами', 
				'items' => [
					['label' => 'Список адресов',	'url' => Url::to(['address/'])],
					['label' => 'Параметры адресов',	'url' => Url::to(['option/'])],					
					['label' => 'Добавить адрес',	'url' => Url::to(['address/create'])],					
					
				]
			],
			
			[
				'label' => 'Документами',
                'items' => [
					['label' => 'Список Договоров', 'url' => Url::to(['contract/'])],
                    ['label' => 'Список Документов', 'url' => Url::to(['document/'])],
                    ['label' => 'Типы документов', 'url' => Url::to(['document-type/'])],
                ],
			],
			[
				'label' => 'Пользователями',
                'items' => [
                    ['label' => 'Список', 'url' => Url::to(['user/'])],
                    ['label' => 'Добавить', 'url' => Url::to(['user/create'])],
					'<li class="divider"></li>',
					['label' => 'Физические лица', 'url' => ['rperson/']],
					['label' => 'ИП', 'url' => ['rip/']],
					['label' => 'Юридические лица', 'url' => ['rcompany/']],
				],
			],
			[
				'label' => 'Контентом',
                'items' => [
                    ['label' => 'Отзывы', 'url' => Url::to(['review/'])],
					['label' => 'ЧаВо', 'url' => Url::to(['faq/'])],
					['label' => 'Страницы', 'url' => Url::to(['page/'])],
					['label' => 'SEO', 'url' => Url::to(['seo/'])],
					['label' => 'Email Шаблоны', 'url' => Url::to(['email-template/'])],
				],
			],
			[
				'label' => 'Аукционами', 
				'items' => [					
					['label' => 'Акционы', 'url' => Url::to(['/auction'])],					
					['label' => 'Ставки', 'url' => Url::to(['/auction-rate'])],					
				],
			],

		],
    ],
	

	[
		'label' => 'Заказы и расчеты',
                'items' => [
                    ['label' => 'Список заказов', 'url' => Url::to(['order/'])],
                    ['label' => 'Платежи', 'url' => Url::to(['payment/'])],
					[
						'label' => 'Загрузка платежей', 
						'items' => [
							['label' => 'Поступивших', 'url' => Url::to(['import/'])],
							['label' => 'Исходящих', 'url' => Url::to(['import/outcome'])],  
						],
					]
                ],
        ],
		[
                'label' => 'Услуги',
                'items' => [
					
                    
                    ['label' => 'Услуги владельцев',
						'items' => [
							['label' => 'Справочник', 'url' => Url::to(['service-name/'])],
							['label' => 'Цены', 'url' => Url::to(['service-price/'])],  
							['label' => 'Заказанные услуги', 'url' => Url::to(['service/'])],
						],	
					],

					['label' => 'Услуги портала',
						'items' => [
							['label' => 'Справочник', 'url' => Url::to(['service-portal/'])],
							['label' => 'Заказанные', 'url' => Url::to(['porder/'])],
						],	
					],
                    
					
                ],


        ],
		
	[

                'label' => 'Справочники',
                'items' => [
					['label' => 'Налоговые инспекции', 'url' => Url::to(['nalog/'])],				
                    ['label' => 'Округи', 'url' => Url::to(['okrug/'])],
                    ['label' => 'Районы', 'url' => Url::to(['region/'])],
                    ['label' => 'Метро', 'url' => Url::to(['metro/'])],                    
					'<li class="divider"></li>',
					['label' => 'Способы доставки',	'url' => Url::to(['delivery/'])],		
					['label' => 'Опции',	'url' => Url::to(['option/'])],		
					['label' => 'Типы документов', 'url' => Url::to(['document-type/'])],
					'<li class="divider"></li>',
					['label' => 'Услуги портала', 'url' => Url::to(['service-portal/'])],										
                    ['label' => 'Услуги владельцев', 'url' => Url::to(['service-name/'])],					
                ],

	],
	


	];





  /*

        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';

*/
    }
	
	echo NavX::widget([
    'options' => ['class' => 'nav nav-pills'],
    'items' => $menuItems,
    'encodeLabels' => false
	]);
	/*
	
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
	*/
	
    NavBar::end();
    ?>

    <div class="container">

	
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy;VseAdresa.Pro <?= date('Y') ?></p>

        
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
