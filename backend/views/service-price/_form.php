<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\ServicePrice;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'code_view' => ['type'=>Form::INPUT_TEXT, 'label'=>ServicePrice::getMeta('code_view', 'label'), 'hint'=>''.ServicePrice::getMeta('code_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'stype' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>ServicePrice::getMeta('stype', 'label'), 'items' => ServicePrice::getList('stype'), 'hint'=>''.ServicePrice::getMeta('stype', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'service_name_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>ServicePrice::getMeta('service_name_id', 'label'), 'items' => ServicePrice::getListValues('service_name_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('service-name/update/%service_name_id%')]).'">'.Icon::show('link').'</a>'.ServicePrice::getMeta('service_name_id', 'comment'), 'options'=>[]],
						'name' => ['type'=>Form::INPUT_TEXT, 'label'=>ServicePrice::getMeta('name', 'label'), 'hint'=>''.ServicePrice::getMeta('name', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>ServicePrice::getMeta('created_at_view', 'label'), 'hint'=>''.ServicePrice::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'to_address' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>ServicePrice::getMeta('to_address', 'label'), 'hint'=>''.ServicePrice::getMeta('to_address', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'owner_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>ServicePrice::getMeta('owner_id_view', 'label'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('user/update/%owner_id%')]).'">'.Icon::show('link').'</a>'.ServicePrice::getMeta('owner_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'recipient_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>ServicePrice::getMeta('recipient_id', 'label'), 'items' => $model -> getRelatedListValues('recipient_id'), 'hint'=>''.ServicePrice::getMeta('recipient_id', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'address_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>ServicePrice::getMeta('address_id', 'label'), 'items' => $model -> getRelatedListValues('address_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('address/update/%address_id%')]).'">'.Icon::show('link').'</a>'.ServicePrice::getMeta('address_id', 'comment'), 'options'=>[]],
		
				],
			],
				
		],		
	],
		
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'description' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>ServicePrice::getMeta('description', 'label'), 'hint'=>''.ServicePrice::getMeta('description', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'type' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>ServicePrice::getMeta('type', 'label'), 'items' => ServicePrice::getList('type'), 'hint'=>''.ServicePrice::getMeta('type', 'comment'), 'options'=>[]],
						'price' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>ServicePrice::getMeta('price', 'label'), 'hint'=>''.ServicePrice::getMeta('price', 'comment'), 'options'=>['mask' => ServicePrice::getMaskByAlias("money"),]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'price6' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>ServicePrice::getMeta('price6', 'label'), 'hint'=>''.ServicePrice::getMeta('price6', 'comment'), 'options'=>['mask' => ServicePrice::getMaskByAlias("money"),]],
						'price11' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>ServicePrice::getMeta('price11', 'label'), 'hint'=>''.ServicePrice::getMeta('price11', 'comment'), 'options'=>['mask' => ServicePrice::getMaskByAlias("money"),]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'enabled' => ['type'=>Form::INPUT_TEXT, 'label'=>ServicePrice::getMeta('enabled', 'label'), 'hint'=>''.ServicePrice::getMeta('enabled', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\ServicePrice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-price-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => ServicePrice::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
