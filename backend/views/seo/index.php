<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Seo;
use common\models\SeoSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'SEO-данные';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Seo::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Seo::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'title',
				'label' => Seo::getMeta('title', 'label'),
				'contentOptions' => ['class' => 'col-title '],
				'filterOptions' => ['class' => 'col-title'],
				'headerOptions' => ['class' => 'col-title '],				
				'footerOptions' => ['class' => 'col-title '],
				
				
				
			],
            ['attribute' => 'description',
				'label' => Seo::getMeta('description', 'label'),
				'contentOptions' => ['class' => 'col-description hidden '],
				'filterOptions' => ['class' => 'col-description hidden'],
				'headerOptions' => ['class' => 'col-description hidden '],				
				'footerOptions' => ['class' => 'col-description hidden '],
				
				
				
			],
            ['attribute' => 'keywords',
				'label' => Seo::getMeta('keywords', 'label'),
				'contentOptions' => ['class' => 'col-keywords hidden '],
				'filterOptions' => ['class' => 'col-keywords hidden'],
				'headerOptions' => ['class' => 'col-keywords hidden '],				
				'footerOptions' => ['class' => 'col-keywords hidden '],
				
				
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
