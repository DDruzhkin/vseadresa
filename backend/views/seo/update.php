<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Seo */

$this->title = 'Редактирование  ' . $model->getCaption();
$this->params['breadcrumbs'][] = ['label' => \backend\models\Seo::getMeta('','title'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this -> title;
?>
<div class="seo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'tab' => $tab  
    ]) ?>

</div>
