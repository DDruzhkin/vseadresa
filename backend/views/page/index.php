<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Page;
use common\models\Entity;

use common\models\PageSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Страницы';

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Page::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Page::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'parent_id',
				'label' => Page::getMeta('parent_id', 'label'),
				'contentOptions' => ['class' => 'col-parent_id '],
				'filterOptions' => ['class' => 'col-parent_id'],
				'headerOptions' => ['class' => 'col-parent_id '],				
				'footerOptions' => ['class' => 'col-parent_id '],
				'filter' => Page::getExistsValues('parent_id'),	
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['parent_id_view'],[Inflector::camel2id('Page').'/update/'.$data['parent_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'created_at',
				'label' => Page::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],

            ['attribute' => 'h1',
				'label' => Page::getMeta('h1', 'label'),
				'contentOptions' => ['class' => 'col-h1 '],
				'filterOptions' => ['class' => 'col-h1'],
				'headerOptions' => ['class' => 'col-h1 '],				
				'footerOptions' => ['class' => 'col-h1 '],
				
				
				
			],
            ['attribute' => 'label',
				'label' => Page::getMeta('label', 'label'),
				'contentOptions' => ['class' => 'col-label hidden '],
				'filterOptions' => ['class' => 'col-label hidden'],
				'headerOptions' => ['class' => 'col-label hidden '],				
				'footerOptions' => ['class' => 'col-label hidden '],
				
				
				
			],
            ['attribute' => 'content',
				'label' => Page::getMeta('content', 'label'),
				'contentOptions' => ['class' => 'col-content hidden '],
				'filterOptions' => ['class' => 'col-content hidden'],
				'headerOptions' => ['class' => 'col-content hidden '],				
				'footerOptions' => ['class' => 'col-content hidden '],
				
				
				
			],
            ['attribute' => 'menu',
				'label' => Page::getMeta('menu', 'label'),
				'contentOptions' => ['class' => 'col-menu '],
				'filterOptions' => ['class' => 'col-menu'],
				'headerOptions' => ['class' => 'col-menu '],				
				'footerOptions' => ['class' => 'col-menu '],
				'filter' => Page::getList('menu'),	
				
				
			],
            ['attribute' => 'title',
				'label' => Page::getMeta('title', 'label'),
				'contentOptions' => ['class' => 'col-title hidden '],
				'filterOptions' => ['class' => 'col-title hidden'],
				'headerOptions' => ['class' => 'col-title hidden '],				
				'footerOptions' => ['class' => 'col-title hidden '],
				
				
				
			],
            ['attribute' => 'meta_description',
				'label' => Page::getMeta('meta_description', 'label'),
				'contentOptions' => ['class' => 'col-meta_description hidden '],
				'filterOptions' => ['class' => 'col-meta_description hidden'],
				'headerOptions' => ['class' => 'col-meta_description hidden '],				
				'footerOptions' => ['class' => 'col-meta_description hidden '],
				
				
				
			],
            ['attribute' => 'seo_text',
				'label' => Page::getMeta('seo_text', 'label'),
				'contentOptions' => ['class' => 'col-seo_text hidden '],
				'filterOptions' => ['class' => 'col-seo_text hidden'],
				'headerOptions' => ['class' => 'col-seo_text hidden '],				
				'footerOptions' => ['class' => 'col-seo_text hidden '],
				
				
				
			],
            ['attribute' => 'slug',
				'label' => Page::getMeta('slug', 'label'),
				'contentOptions' => ['class' => 'col-slug '],
				'filterOptions' => ['class' => 'col-slug'],
				'headerOptions' => ['class' => 'col-slug '],				
				'footerOptions' => ['class' => 'col-slug '],
				
				
				
			],
            ['attribute' => 'template',
				'label' => Page::getMeta('template', 'label'),
				'contentOptions' => ['class' => 'col-template hidden '],
				'filterOptions' => ['class' => 'col-template hidden'],
				'headerOptions' => ['class' => 'col-template hidden '],				
				'footerOptions' => ['class' => 'col-template hidden '],
				
				
				
			],
            ['attribute' => 'type',
				'label' => Page::getMeta('type', 'label'),
				'contentOptions' => ['class' => 'col-type '],
				'filterOptions' => ['class' => 'col-type'],
				'headerOptions' => ['class' => 'col-type '],				
				'footerOptions' => ['class' => 'col-type '],
				'filter' => Page::getList('type'),	
				
				
			],
            ['attribute' => 'visible',
				'label' => Page::getMeta('visible', 'label'),
				'contentOptions' => ['class' => 'col-visible hidden '],
				'filterOptions' => ['class' => 'col-visible hidden'],
				'headerOptions' => ['class' => 'col-visible hidden '],				
				'footerOptions' => ['class' => 'col-visible hidden '],
				
				
				
			],

            ['attribute' => 'enabled',
				'label' => Page::getMeta('enabled', 'label'),
				'contentOptions' => ['class' => 'col-enabled hidden '],
				'filterOptions' => ['class' => 'col-enabled hidden'],
				'headerOptions' => ['class' => 'col-enabled hidden '],				
				'footerOptions' => ['class' => 'col-enabled hidden '],
				
				
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
