<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Review;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Review::getMeta('created_at_view', 'label'), 'hint'=>''.Review::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'name' => ['type'=>Form::INPUT_TEXT, 'label'=>Review::getMeta('name', 'label'), 'hint'=>''.Review::getMeta('name', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'email' => ['type'=>Form::INPUT_TEXT, 'label'=>Review::getMeta('email', 'label'), 'hint'=>''.Review::getMeta('email', 'comment'), 'options'=>[]],
						'company' => ['type'=>Form::INPUT_TEXT, 'label'=>Review::getMeta('company', 'label'), 'hint'=>''.Review::getMeta('company', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'author_position' => ['type'=>Form::INPUT_TEXT, 'label'=>Review::getMeta('author_position', 'label'), 'hint'=>''.Review::getMeta('author_position', 'comment'), 'options'=>[]],
						'status_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Review::getMeta('status_id', 'label'), 'hint'=>''.Review::getMeta('status_id', 'comment'), 'items' => $model -> getStatusList()],	
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'content' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Review::getMeta('content', 'label'), 'hint'=>''.Review::getMeta('content', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Review */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Review::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
