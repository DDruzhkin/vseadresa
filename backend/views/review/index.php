<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Review;
use common\models\ReviewSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Отзывы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Review::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить отзыв', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Review::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'created_at',
				'label' => Review::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'name',
				'label' => Review::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				
				
				
			],
            ['attribute' => 'email',
				'label' => Review::getMeta('email', 'label'),
				'contentOptions' => ['class' => 'col-email '],
				'filterOptions' => ['class' => 'col-email'],
				'headerOptions' => ['class' => 'col-email '],				
				'footerOptions' => ['class' => 'col-email '],
				
				
				
			],
            ['attribute' => 'company',
				'label' => Review::getMeta('company', 'label'),
				'contentOptions' => ['class' => 'col-company '],
				'filterOptions' => ['class' => 'col-company'],
				'headerOptions' => ['class' => 'col-company '],				
				'footerOptions' => ['class' => 'col-company '],
				
				
				
			],
            ['attribute' => 'author_position',
				'label' => Review::getMeta('author_position', 'label'),
				'contentOptions' => ['class' => 'col-author_position hidden '],
				'filterOptions' => ['class' => 'col-author_position hidden'],
				'headerOptions' => ['class' => 'col-author_position hidden '],				
				'footerOptions' => ['class' => 'col-author_position hidden '],
				
				
				
			],
            ['attribute' => 'status_id',
				'label' => Review::getMeta('status_id', 'label'),
				'contentOptions' => ['class' => 'col-status_id '],
				'filterOptions' => ['class' => 'col-status_id'],
				'headerOptions' => ['class' => 'col-status_id '],				
				'footerOptions' => ['class' => 'col-status_id '],
				'filter' => Review::getExistsValues('status_id'),	
				'value' => function($data){ return $data['status_id_view'];},
				
			],
            ['attribute' => 'content',
				'label' => Review::getMeta('content', 'label'),
				'contentOptions' => ['class' => 'col-content hidden '],
				'filterOptions' => ['class' => 'col-content hidden'],
				'headerOptions' => ['class' => 'col-content hidden '],				
				'footerOptions' => ['class' => 'col-content hidden '],
				
				
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
