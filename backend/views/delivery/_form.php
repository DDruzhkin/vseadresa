<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Delivery;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'alias' => ['type'=>Form::INPUT_TEXT, 'label'=>Delivery::getMeta('alias', 'label'), 'hint'=>''.Delivery::getMeta('alias', 'comment'), 'options'=>[]],
						'name' => ['type'=>Form::INPUT_TEXT, 'label'=>Delivery::getMeta('name', 'label'), 'hint'=>''.Delivery::getMeta('name', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'description' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Delivery::getMeta('description', 'label'), 'hint'=>''.Delivery::getMeta('description', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'price' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Delivery::getMeta('price', 'label'), 'hint'=>''.Delivery::getMeta('price', 'comment'), 'options'=>['mask' => Delivery::getMaskByAlias("money"),]],
						'delivery_requirement' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>Delivery::getMeta('delivery_requirement', 'label'), 'hint'=>''.Delivery::getMeta('delivery_requirement', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Delivery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Delivery::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
