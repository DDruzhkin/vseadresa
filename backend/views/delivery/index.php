<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Delivery;
use common\models\DeliverySearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Способы доставки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Delivery::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить способ доставки', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Delivery::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'alias',
				'label' => Delivery::getMeta('alias', 'label'),
				'contentOptions' => ['class' => 'col-alias '],
				'filterOptions' => ['class' => 'col-alias'],
				'headerOptions' => ['class' => 'col-alias '],				
				'footerOptions' => ['class' => 'col-alias '],
				
				
				
			],
            ['attribute' => 'name',
				'label' => Delivery::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				
				
				
			],
            ['attribute' => 'description',
				'label' => Delivery::getMeta('description', 'label'),
				'contentOptions' => ['class' => 'col-description hidden '],
				'filterOptions' => ['class' => 'col-description hidden'],
				'headerOptions' => ['class' => 'col-description hidden '],				
				'footerOptions' => ['class' => 'col-description hidden '],
				
				
				
			],
            ['attribute' => 'price',
				'label' => Delivery::getMeta('price', 'label'),
				'contentOptions' => ['class' => 'col-price '],
				'filterOptions' => ['class' => 'col-price'],
				'headerOptions' => ['class' => 'col-price '],				
				'footerOptions' => ['class' => 'col-price '],
				
				
				
			],
            ['attribute' => 'delivery_requirement',
				'label' => Delivery::getMeta('delivery_requirement', 'label'),
				'contentOptions' => ['class' => 'col-delivery_requirement hidden '],
				'filterOptions' => ['class' => 'col-delivery_requirement hidden'],
				'headerOptions' => ['class' => 'col-delivery_requirement hidden '],				
				'footerOptions' => ['class' => 'col-delivery_requirement hidden '],
				
				
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
