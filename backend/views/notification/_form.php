<?php

use backend\models\Notification;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'type' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Notification::getMeta('type', 'label'), 'items' => Notification::getList('type'), 'hint'=>''.Notification::getMeta('type', 'comment'), 'options'=>[]],
						'email' => ['type'=>Form::INPUT_TEXT, 'label'=>Notification::getMeta('email', 'label'), 'hint'=>''.Notification::getMeta('email', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'user_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Notification::getMeta('user_id', 'label'), 'items' => Notification::getListValues('user_id'), 'hint'=>''.Notification::getMeta('user_id', 'comment'), 'options'=>[]],
						'date' => ['type'=>Form::INPUT_TEXT, 'label'=>Notification::getMeta('date', 'label'), 'hint'=>''.Notification::getMeta('date', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'order_id' => ['type'=>Form::INPUT_TEXT, 'label'=>Notification::getMeta('order_id', 'label'), 'hint'=>''.Notification::getMeta('order_id', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Notification */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notification-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Notification::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	





$form = ActiveForm::begin();
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

?>

</div>
