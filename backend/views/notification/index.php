<?php




/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Notification;
use common\models\NotificationSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Уведомления';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Notification::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Notification::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'type',
				'label' => Notification::getMeta('type', 'label'),
				'contentOptions' => ['class' => 'col-type'],
				'filterOptions' => ['class' => 'col-type'],
				'headerOptions' => ['class' => 'col-type'],				
				'footerOptions' => ['class' => 'col-type'],
				
				
				
			],
            ['attribute' => 'email',
				'label' => Notification::getMeta('email', 'label'),
				'contentOptions' => ['class' => 'col-email'],
				'filterOptions' => ['class' => 'col-email'],
				'headerOptions' => ['class' => 'col-email'],				
				'footerOptions' => ['class' => 'col-email'],
				
				
				
			],
            ['attribute' => 'user_id',
				'label' => Notification::getMeta('user_id', 'label'),
				'contentOptions' => ['class' => 'col-user_id'],
				'filterOptions' => ['class' => 'col-user_id'],
				'headerOptions' => ['class' => 'col-user_id'],				
				'footerOptions' => ['class' => 'col-user_id'],
				
				'value' => function($data){ return $data['user_id_view'];},
				
			],
            ['attribute' => 'date',
				'label' => Notification::getMeta('date', 'label'),
				'contentOptions' => ['class' => 'col-date'],
				'filterOptions' => ['class' => 'col-date'],
				'headerOptions' => ['class' => 'col-date'],				
				'footerOptions' => ['class' => 'col-date'],
				
				
				
			],
            ['attribute' => 'order_id',
				'label' => Notification::getMeta('order_id', 'label'),
				'contentOptions' => ['class' => 'col-order_id'],
				'filterOptions' => ['class' => 'col-order_id'],
				'headerOptions' => ['class' => 'col-order_id'],				
				'footerOptions' => ['class' => 'col-order_id'],
				
				
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
