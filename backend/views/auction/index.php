<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Auction;
use common\models\AuctionSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Список аукционов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auction-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Auction::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить Аукцион', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Auction::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'date',
				'label' => Auction::getMeta('date', 'label'),
				'contentOptions' => ['class' => 'col-date date'],
				'filterOptions' => ['class' => 'col-date'],
				'headerOptions' => ['class' => 'col-date date'],				
				'footerOptions' => ['class' => 'col-date date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'date',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'address_id',
				'label' => Auction::getMeta('address_id', 'label'),
				'contentOptions' => ['class' => 'col-address_id '],
				'filterOptions' => ['class' => 'col-address_id'],
				'headerOptions' => ['class' => 'col-address_id '],				
				'footerOptions' => ['class' => 'col-address_id '],
				
				'value' => function($data){ return $data['address_id_view'];},
				
			],
            ['attribute' => 'owner_id',
				'label' => Auction::getMeta('owner_id', 'label'),
				'contentOptions' => ['class' => 'col-owner_id '],
				'filterOptions' => ['class' => 'col-owner_id'],
				'headerOptions' => ['class' => 'col-owner_id '],				
				'footerOptions' => ['class' => 'col-owner_id '],
				
				'value' => function($data){ return $data['owner_id_view'];},
				
			],
            ['attribute' => 'status_id',
				'label' => Auction::getMeta('status_id', 'label'),
				'contentOptions' => ['class' => 'col-status_id '],
				'filterOptions' => ['class' => 'col-status_id'],
				'headerOptions' => ['class' => 'col-status_id '],				
				'footerOptions' => ['class' => 'col-status_id '],
				'filter' => Auction::getExistsValues('status_id'),	
				'value' => function($data){ return $data['status_id_view'];},
				
			],
            ['attribute' => 'rate_id',
				'label' => Auction::getMeta('rate_id', 'label'),
				'contentOptions' => ['class' => 'col-rate_id '],
				'filterOptions' => ['class' => 'col-rate_id'],
				'headerOptions' => ['class' => 'col-rate_id '],				
				'footerOptions' => ['class' => 'col-rate_id '],
				'filter' => Auction::getExistsValues('rate_id'),	
				'value' => function($data){ return $data['rate_id_view'];},
				
			],
            ['attribute' => 'rate_count',
				'label' => Auction::getMeta('rate_count', 'label'),
				'contentOptions' => ['class' => 'col-rate_count '],
				'filterOptions' => ['class' => 'col-rate_count'],
				'headerOptions' => ['class' => 'col-rate_count '],				
				'footerOptions' => ['class' => 'col-rate_count '],
				'filter' => Auction::getExistsValues('rate_count'),	
				
				
			],
            ['attribute' => 'amount',
				'label' => Auction::getMeta('amount', 'label'),
				'contentOptions' => ['class' => 'col-amount '],
				'filterOptions' => ['class' => 'col-amount'],
				'headerOptions' => ['class' => 'col-amount '],				
				'footerOptions' => ['class' => 'col-amount '],
				'filter' => Auction::getExistsValues('amount'),	
				
				'format' => ['decimal', 2],
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
