<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\PhotoCalendar;
use common\models\PhotoCalendarSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Календарь фотографа';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photo-calendar-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\PhotoCalendar::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\PhotoCalendar::getListViewMenu($data),
							],
						],
					]);
				}
			],


            ['attribute' => 'address_id',
				'label' => PhotoCalendar::getMeta('address_id', 'label'),
				'contentOptions' => ['class' => 'col-address_id '],
				'filterOptions' => ['class' => 'col-address_id'],
				'headerOptions' => ['class' => 'col-address_id '],
				'footerOptions' => ['class' => 'col-address_id '],

				'value' => function($data){ return $data['address_id_view'];},

			],
            ['attribute' => 'user_id',
				'label' => PhotoCalendar::getMeta('user_id', 'label'),
				'contentOptions' => ['class' => 'col-user_id '],
				'filterOptions' => ['class' => 'col-user_id'],
				'headerOptions' => ['class' => 'col-user_id '],
				'footerOptions' => ['class' => 'col-user_id '],

				'value' => function($data){ return $data['user_id_view'];},

			],
            ['attribute' => 'date',
				'label' => PhotoCalendar::getMeta('date', 'label'),
				'contentOptions' => ['class' => 'col-date date'],
				'filterOptions' => ['class' => 'col-date'],
				'headerOptions' => ['class' => 'col-date date'],
				'footerOptions' => ['class' => 'col-date date'],



			],
            ['attribute' => 'interval',
				'label' => PhotoCalendar::getMeta('interval', 'label'),
				'contentOptions' => ['class' => 'col-interval '],
				'filterOptions' => ['class' => 'col-interval'],
				'headerOptions' => ['class' => 'col-interval '],
				'footerOptions' => ['class' => 'col-interval '],
                'value' => function($data)
                    {
                        return ArrayHelper::getValue(\frontend\models\PhotoCalendar::getInterval(),$data->interval);
                    },


			],
            ['attribute' => 'enabled',
				'label' => PhotoCalendar::getMeta('enabled', 'label'),
				'contentOptions' => ['class' => 'col-enabled '],
				'filterOptions' => ['class' => 'col-enabled'],
				'headerOptions' => ['class' => 'col-enabled '],
				'footerOptions' => ['class' => 'col-enabled '],
                'value' => function($data)
                {
                    return ArrayHelper::getValue($data->getEnabled(),intval($data->enabled));
                },


			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
