<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Metro */

$this->title = 'Редактирование Станции метро ' . $model->getCaption();
$this->params['breadcrumbs'][] = ['label' => \backend\models\Metro::getMeta('','title'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this -> title;
?>
<div class="metro-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'tab' => $tab  
    ]) ?>

</div>
