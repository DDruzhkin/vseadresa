<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Metro;
use common\models\MetroSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Список станций метро';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metro-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Metro::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить Станцию метро', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Metro::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'alias',
				'label' => Metro::getMeta('alias', 'label'),
				'contentOptions' => ['class' => 'col-alias '],
				'filterOptions' => ['class' => 'col-alias'],
				'headerOptions' => ['class' => 'col-alias '],				
				'footerOptions' => ['class' => 'col-alias '],
				
				
				
			],
            ['attribute' => 'name',
				'label' => Metro::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				
				
				
			],
            ['attribute' => 'title',
				'label' => Metro::getMeta('title', 'label'),
				'contentOptions' => ['class' => 'col-title hidden '],
				'filterOptions' => ['class' => 'col-title hidden'],
				'headerOptions' => ['class' => 'col-title hidden '],				
				'footerOptions' => ['class' => 'col-title hidden '],
				
				
				
			],
            ['attribute' => 'meta_description',
				'label' => Metro::getMeta('meta_description', 'label'),
				'contentOptions' => ['class' => 'col-meta_description hidden '],
				'filterOptions' => ['class' => 'col-meta_description hidden'],
				'headerOptions' => ['class' => 'col-meta_description hidden '],				
				'footerOptions' => ['class' => 'col-meta_description hidden '],
				
				
				
			],
            ['attribute' => 'seo_text',
				'label' => Metro::getMeta('seo_text', 'label'),
				'contentOptions' => ['class' => 'col-seo_text hidden '],
				'filterOptions' => ['class' => 'col-seo_text hidden'],
				'headerOptions' => ['class' => 'col-seo_text hidden '],				
				'footerOptions' => ['class' => 'col-seo_text hidden '],
				
				
				
			],
            ['attribute' => 'h1',
				'label' => Metro::getMeta('h1', 'label'),
				'contentOptions' => ['class' => 'col-h1 hidden '],
				'filterOptions' => ['class' => 'col-h1 hidden'],
				'headerOptions' => ['class' => 'col-h1 hidden '],				
				'footerOptions' => ['class' => 'col-h1 hidden '],
				
				
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
