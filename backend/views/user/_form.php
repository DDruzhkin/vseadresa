<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\User;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'fullname' => ['type'=>Form::INPUT_TEXT, 'label'=>User::getMeta('fullname', 'label'), 'hint'=>''.User::getMeta('fullname', 'comment'), 'options'=>[]],
						'username' => ['type'=>Form::INPUT_TEXT, 'label'=>User::getMeta('username', 'label'), 'hint'=>''.User::getMeta('username', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'role_as_array' => ['type'=>Form::INPUT_CHECKBOX_LIST, 'label'=>User::getMeta('role', 'label'), 'items' => User::getList('role'), 'hint'=>''.User::getMeta('role', 'comment'), 'options'=>['multiple' => true]],
						'status_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>User::getMeta('status_id', 'label'), 'hint'=>''.User::getMeta('status_id', 'comment'), 'items' => $model -> getStatusList()],	
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>User::getMeta('created_at_view', 'label'), 'hint'=>''.User::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'email' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>User::getMeta('email', 'label'), 'hint'=>''.User::getMeta('email', 'comment'), 'options'=>['clientOptions' => ['alias' =>  'email'],]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'phone' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>User::getMeta('phone', 'label'), 'hint'=>''.User::getMeta('phone', 'comment'), 'options'=>['mask' => User::getMaskByAlias("phone"),]],
						'offerta_confirm' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>User::getMeta('offerta_confirm', 'label'), 'hint'=>''.User::getMeta('offerta_confirm', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'info' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>User::getMeta('info', 'label'), 'hint'=>''.User::getMeta('info', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'priority' => ['type'=>Form::INPUT_TEXT, 'label'=>User::getMeta('priority', 'label'), 'hint'=>''.User::getMeta('priority', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => User::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
