<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\ServicePortal;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'name' => ['type'=>Form::INPUT_TEXT, 'label'=>ServicePortal::getMeta('name', 'label'), 'hint'=>''.ServicePortal::getMeta('name', 'comment'), 'options'=>[]],
						'alias' => ['type'=>Form::INPUT_TEXT, 'label'=>ServicePortal::getMeta('alias', 'label'), 'hint'=>''.ServicePortal::getMeta('alias', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'for' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>ServicePortal::getMeta('for', 'label'), 'items' => ServicePortal::getList('for'), 'hint'=>''.ServicePortal::getMeta('for', 'comment'), 'options'=>[]],
						'price' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>ServicePortal::getMeta('price', 'label'), 'hint'=>''.ServicePortal::getMeta('price', 'comment'), 'options'=>['mask' => ServicePortal::getMaskByAlias("money"),]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'price_rules' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>ServicePortal::getMeta('price_rules', 'label'), 'hint'=>''.ServicePortal::getMeta('price_rules', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'undeleted_view' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>ServicePortal::getMeta('undeleted_view', 'label'), 'hint'=>''.ServicePortal::getMeta('undeleted_view', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],'readonly'=>'true', ]],
						'recipient_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>ServicePortal::getMeta('recipient_id', 'label'), 'items' => $model -> getRelatedListValues('recipient_id'), 'hint'=>''.ServicePortal::getMeta('recipient_id', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\ServicePortal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-portal-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => ServicePortal::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
