<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ServicePortal */

$this->title = "услуга ".$model->getCaption();
$this->params['breadcrumbs'][] = ['label' => 'Service Portals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$attrList = $model -> getAttributes(); 
$attributes = [];
	foreach ($attrList as $colName => $value) {		
		$type = $model -> getMeta($colName, 'type');
		$show = (int)$model -> getMeta($colName, 'show');
		if (!$show) continue;
		$attributes[] = $colName."_view" . ($type == 'text'?':ntext':'');
	}


?>
<div class="service-portal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

</div>
