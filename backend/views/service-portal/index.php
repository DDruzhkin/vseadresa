<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\ServicePortal;
use common\models\ServicePortalSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Справочник услуг портала';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-portal-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\ServicePortal::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить услугу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\ServicePortal::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'id',
				'label' => ServicePortal::getMeta('id', 'label'),
				'contentOptions' => ['class' => 'col-id '],
				'filterOptions' => ['class' => 'col-id'],
				'headerOptions' => ['class' => 'col-id '],				
				'footerOptions' => ['class' => 'col-id '],
				
				
				
			],
            ['attribute' => 'name',
				'label' => ServicePortal::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				
				
				
			],
            ['attribute' => 'alias',
				'label' => ServicePortal::getMeta('alias', 'label'),
				'contentOptions' => ['class' => 'col-alias hidden '],
				'filterOptions' => ['class' => 'col-alias hidden'],
				'headerOptions' => ['class' => 'col-alias hidden '],				
				'footerOptions' => ['class' => 'col-alias hidden '],
				
				
				
			],
            ['attribute' => 'for',
				'label' => ServicePortal::getMeta('for', 'label'),
				'contentOptions' => ['class' => 'col-for '],
				'filterOptions' => ['class' => 'col-for'],
				'headerOptions' => ['class' => 'col-for '],				
				'footerOptions' => ['class' => 'col-for '],
				'filter' => ServicePortal::getList('for'),	
				
				
			],
            ['attribute' => 'price',
				'label' => ServicePortal::getMeta('price', 'label'),
				'contentOptions' => ['class' => 'col-price '],
				'filterOptions' => ['class' => 'col-price'],
				'headerOptions' => ['class' => 'col-price '],				
				'footerOptions' => ['class' => 'col-price '],
				
				
				'format' => ['decimal', 2],
			],
            ['attribute' => 'price_rules',
				'label' => ServicePortal::getMeta('price_rules', 'label'),
				'contentOptions' => ['class' => 'col-price_rules hidden '],
				'filterOptions' => ['class' => 'col-price_rules hidden'],
				'headerOptions' => ['class' => 'col-price_rules hidden '],				
				'footerOptions' => ['class' => 'col-price_rules hidden '],
				
				
				
			],
            ['attribute' => 'undeleted',
				'label' => ServicePortal::getMeta('undeleted', 'label'),
				'contentOptions' => ['class' => 'col-undeleted hidden '],
				'filterOptions' => ['class' => 'col-undeleted hidden'],
				'headerOptions' => ['class' => 'col-undeleted hidden '],				
				'footerOptions' => ['class' => 'col-undeleted hidden '],
				
				
				
			],
            ['attribute' => 'recipient_id',
				'label' => ServicePortal::getMeta('recipient_id', 'label'),
				'contentOptions' => ['class' => 'col-recipient_id hidden '],
				'filterOptions' => ['class' => 'col-recipient_id hidden'],
				'headerOptions' => ['class' => 'col-recipient_id hidden '],				
				'footerOptions' => ['class' => 'col-recipient_id hidden '],
				
				'value' => function($data){ return $data['recipient_id_view'];},
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
