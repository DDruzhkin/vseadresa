<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Rcompany;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Rcompany::getMeta('created_at_view', 'label'), 'hint'=>''.Rcompany::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'form' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Rcompany::getMeta('form', 'label'), 'items' => Rcompany::getList('form'), 'hint'=>''.Rcompany::getMeta('form', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'user_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Rcompany::getMeta('user_id', 'label'), 'items' => Rcompany::getListValues('user_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('user/update/%user_id%')]).'">'.Icon::show('link').'</a>'.Rcompany::getMeta('user_id', 'comment'), 'options'=>[]],
						'inn' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rcompany::getMeta('inn', 'label'), 'hint'=>''.Rcompany::getMeta('inn', 'comment'), 'options'=>['mask' => '9999999999',]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'kpp' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rcompany::getMeta('kpp', 'label'), 'hint'=>''.Rcompany::getMeta('kpp', 'comment'), 'options'=>['mask' => '999999999',]],
						'ogrn' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rcompany::getMeta('ogrn', 'label'), 'hint'=>''.Rcompany::getMeta('ogrn', 'comment'), 'options'=>['mask' => '9999999999999',]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'name' => ['type'=>Form::INPUT_TEXT, 'label'=>Rcompany::getMeta('name', 'label'), 'hint'=>''.Rcompany::getMeta('name', 'comment'), 'options'=>[]],
						'full_name' => ['type'=>Form::INPUT_TEXT, 'label'=>Rcompany::getMeta('full_name', 'label'), 'hint'=>''.Rcompany::getMeta('full_name', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'address' => ['type'=>Form::INPUT_TEXT, 'label'=>Rcompany::getMeta('address', 'label'), 'hint'=>''.Rcompany::getMeta('address', 'comment'), 'options'=>[]],
						'phone' => ['type'=>Form::INPUT_TEXT, 'label'=>Rcompany::getMeta('phone', 'label'), 'hint'=>''.Rcompany::getMeta('phone', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'bank_bik' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rcompany::getMeta('bank_bik', 'label'), 'hint'=>''.Rcompany::getMeta('bank_bik', 'comment'), 'options'=>['mask' => '999999999',]],
						'bank_name' => ['type'=>Form::INPUT_TEXT, 'label'=>Rcompany::getMeta('bank_name', 'label'), 'hint'=>''.Rcompany::getMeta('bank_name', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'bank_cor' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rcompany::getMeta('bank_cor', 'label'), 'hint'=>''.Rcompany::getMeta('bank_cor', 'comment'), 'options'=>['mask' => '99999999999999999999',]],
						'account' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rcompany::getMeta('account', 'label'), 'hint'=>''.Rcompany::getMeta('account', 'comment'), 'options'=>['mask' => Rcompany::getMaskByAlias("account"),]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'account_owner' => ['type'=>Form::INPUT_TEXT, 'label'=>Rcompany::getMeta('account_owner', 'label'), 'hint'=>''.Rcompany::getMeta('account_owner', 'comment'), 'options'=>[]],
						'eq_login' => ['type'=>Form::INPUT_TEXT, 'label'=>Rcompany::getMeta('eq_login', 'label'), 'hint'=>''.Rcompany::getMeta('eq_login', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'eq_password' => ['type'=>Form::INPUT_TEXT, 'label'=>Rcompany::getMeta('eq_password', 'label'), 'hint'=>''.Rcompany::getMeta('eq_password', 'comment'), 'options'=>[]],
						'dogovor_signature' => ['type'=>Form::INPUT_TEXT, 'label'=>Rcompany::getMeta('dogovor_signature', 'label'), 'hint'=>''.Rcompany::getMeta('dogovor_signature', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'dogovor_face' => ['type'=>Form::INPUT_TEXT, 'label'=>Rcompany::getMeta('dogovor_face', 'label'), 'hint'=>''.Rcompany::getMeta('dogovor_face', 'comment'), 'options'=>[]],
						'dogovor_osnovanie' => ['type'=>Form::INPUT_TEXT, 'label'=>Rcompany::getMeta('dogovor_osnovanie', 'label'), 'hint'=>''.Rcompany::getMeta('dogovor_osnovanie', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'tax_system_vat' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>Rcompany::getMeta('tax_system_vat', 'label'), 'hint'=>''.Rcompany::getMeta('tax_system_vat', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Rcompany */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rcompany-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Rcompany::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
