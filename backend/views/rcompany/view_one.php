<?php
use kartik\icons\Icon;
use yii\helpers\Url;

$isDefault = ($model -> id == $model -> user -> default_profile_id);

$template = '
<div class="card">	
<div class="header">'
.($isDefault?
	'<a title="Основные реквизиты">'.Icon::show('star').'</a>'
	:'<a title="Сделать основынми реквизитами" href="'.URL::to(['/user/default-profile/'.$model -> user -> id.'/'.$model -> id]).'">'.Icon::show('star-o').'</a>'
)
.'<span class="caption">Юридическое лицо</span>'
.'<a title="Изменить реквизиты" style="float:right" href="'.URL::to(['/'.$model -> modelId().'/update/'.$model -> id]).'">'.Icon::show('edit').'</a>
</div>
<div class="value title">%full_name%</div>
<div class="undervalue">Полное наименование организации</div>
<div class="value>
</div><div class="undervalue"></div>
'
.(($model -> inn)?'<div class="value">%inn%</div><div class="undervalue">ИНН организации</div>':'')
.(($model -> kpp)?'<div class="value">%kpp%</div><div class="undervalue">КПП</div>':'')
.(($model -> ogrn)?'<div class="value">%ogrn%</div><div class="undervalue">ОГРН</div>':'')
.(($model -> dogovor_signature)?'<div class="value">%dogovor_signature%</div><div class="undervalue">подписант</div>':'')
.(($model -> dogovor_face)?'<div class="value">%dogovor_face%</div><div class="undervalue">в лице кого подписывается договор</div>':'')
.(($model -> dogovor_osnovanie)?'<div class="value">%dogovor_osnovanie%</div><div class="undervalue">на основании чего действует подписант</div>':'')



.(($model -> address)?'<div class="value">%address%</div><div class="undervalue">адрес регистрации</div>':'')
.(($model -> bank_bik)?'<div class="value">%bank_bik%</div><div class="undervalue">БИК банка</div>':'')
.(($model -> account)?'<div class="value">%account%</div><div class="undervalue">номер счета</div>':'')
.(($model -> account && $model -> account_owner)?'<div class="value">%account_owner%</div><div class="undervalue">владелец счета</div>':'')
.(($model -> eq_login && $model -> eq_password)?'<div class="value">%eq_login%</div><div class="undervalue" style="color:red;">Подключен эквайринг</div>':'')
.'</div>	
';
echo $model -> generateByTemplate($template, $model -> attributes);

?>