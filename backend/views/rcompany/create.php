<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Rcompany */

$this->title = 'Добавление Реквизиты юрлица';
$this->params['breadcrumbs'][] = ['label' => \backend\models\Rcompany::getMeta('','title'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rcompany-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
