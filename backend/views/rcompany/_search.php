<?php
/* генерируемый файл, редактировать его не надо */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RcompanySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rcompany-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'form') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'inn') ?>

    <?php // echo $form->field($model, 'kpp') ?>

    <?php // echo $form->field($model, 'ogrn') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'full_name') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'bank_bik') ?>

    <?php // echo $form->field($model, 'bank_name') ?>

    <?php // echo $form->field($model, 'bank_cor') ?>

    <?php // echo $form->field($model, 'account') ?>

    <?php // echo $form->field($model, 'account_owner') ?>

    <?php // echo $form->field($model, 'eq_login') ?>

    <?php // echo $form->field($model, 'eq_password') ?>

    <?php // echo $form->field($model, 'dogovor_signature') ?>

    <?php // echo $form->field($model, 'dogovor_face') ?>

    <?php // echo $form->field($model, 'dogovor_osnovanie') ?>

    <?php // echo $form->field($model, 'tax_system_vat') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
