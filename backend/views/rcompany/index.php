<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Rcompany;
use common\models\Entity;
use common\models\RcompanySearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Реквизиты юрлиц';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rcompany-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Rcompany::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить Реквизиты юрлица', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Rcompany::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'id',
				'label' => Rcompany::getMeta('id', 'label'),
				'contentOptions' => ['class' => 'col-id '],
				'filterOptions' => ['class' => 'col-id'],
				'headerOptions' => ['class' => 'col-id '],				
				'footerOptions' => ['class' => 'col-id '],
				
				
				
			],
            ['attribute' => 'created_at',
				'label' => Rcompany::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'form',
				'label' => Rcompany::getMeta('form', 'label'),
				'contentOptions' => ['class' => 'col-form hidden '],
				'filterOptions' => ['class' => 'col-form hidden'],
				'headerOptions' => ['class' => 'col-form hidden '],				
				'footerOptions' => ['class' => 'col-form hidden '],
				'filter' => Rcompany::getList('form'),	
				
				
			],
            ['attribute' => 'user_id',
				'label' => Rcompany::getMeta('user_id', 'label'),
				'contentOptions' => ['class' => 'col-user_id '],
				'filterOptions' => ['class' => 'col-user_id'],
				'headerOptions' => ['class' => 'col-user_id '],				
				'footerOptions' => ['class' => 'col-user_id '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['user_id_view'],[Inflector::camel2id('User').'/update/'.$data['user_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'inn',
				'label' => Rcompany::getMeta('inn', 'label'),
				'contentOptions' => ['class' => 'col-inn '],
				'filterOptions' => ['class' => 'col-inn'],
				'headerOptions' => ['class' => 'col-inn '],				
				'footerOptions' => ['class' => 'col-inn '],
				
				
				
			],
            ['attribute' => 'kpp',
				'label' => Rcompany::getMeta('kpp', 'label'),
				'contentOptions' => ['class' => 'col-kpp hidden '],
				'filterOptions' => ['class' => 'col-kpp hidden'],
				'headerOptions' => ['class' => 'col-kpp hidden '],				
				'footerOptions' => ['class' => 'col-kpp hidden '],
				
				
				
			],
            ['attribute' => 'ogrn',
				'label' => Rcompany::getMeta('ogrn', 'label'),
				'contentOptions' => ['class' => 'col-ogrn hidden '],
				'filterOptions' => ['class' => 'col-ogrn hidden'],
				'headerOptions' => ['class' => 'col-ogrn hidden '],				
				'footerOptions' => ['class' => 'col-ogrn hidden '],
				
				
				
			],
            ['attribute' => 'name',
				'label' => Rcompany::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				
				
				
			],
            ['attribute' => 'full_name',
				'label' => Rcompany::getMeta('full_name', 'label'),
				'contentOptions' => ['class' => 'col-full_name hidden '],
				'filterOptions' => ['class' => 'col-full_name hidden'],
				'headerOptions' => ['class' => 'col-full_name hidden '],				
				'footerOptions' => ['class' => 'col-full_name hidden '],
				
				
				
			],
            ['attribute' => 'address',
				'label' => Rcompany::getMeta('address', 'label'),
				'contentOptions' => ['class' => 'col-address '],
				'filterOptions' => ['class' => 'col-address'],
				'headerOptions' => ['class' => 'col-address '],				
				'footerOptions' => ['class' => 'col-address '],
				
				
				
			],
            ['attribute' => 'phone',
				'label' => Rcompany::getMeta('phone', 'label'),
				'contentOptions' => ['class' => 'col-phone hidden '],
				'filterOptions' => ['class' => 'col-phone hidden'],
				'headerOptions' => ['class' => 'col-phone hidden '],				
				'footerOptions' => ['class' => 'col-phone hidden '],
				
				
				
			],
            ['attribute' => 'bank_bik',
				'label' => Rcompany::getMeta('bank_bik', 'label'),
				'contentOptions' => ['class' => 'col-bank_bik hidden '],
				'filterOptions' => ['class' => 'col-bank_bik hidden'],
				'headerOptions' => ['class' => 'col-bank_bik hidden '],				
				'footerOptions' => ['class' => 'col-bank_bik hidden '],
				
				
				
			],
            ['attribute' => 'bank_name',
				'label' => Rcompany::getMeta('bank_name', 'label'),
				'contentOptions' => ['class' => 'col-bank_name hidden '],
				'filterOptions' => ['class' => 'col-bank_name hidden'],
				'headerOptions' => ['class' => 'col-bank_name hidden '],				
				'footerOptions' => ['class' => 'col-bank_name hidden '],
				
				
				
			],
            ['attribute' => 'bank_cor',
				'label' => Rcompany::getMeta('bank_cor', 'label'),
				'contentOptions' => ['class' => 'col-bank_cor hidden '],
				'filterOptions' => ['class' => 'col-bank_cor hidden'],
				'headerOptions' => ['class' => 'col-bank_cor hidden '],				
				'footerOptions' => ['class' => 'col-bank_cor hidden '],
				
				
				
			],
            ['attribute' => 'account',
				'label' => Rcompany::getMeta('account', 'label'),
				'contentOptions' => ['class' => 'col-account hidden '],
				'filterOptions' => ['class' => 'col-account hidden'],
				'headerOptions' => ['class' => 'col-account hidden '],				
				'footerOptions' => ['class' => 'col-account hidden '],
				
				
				
			],
            ['attribute' => 'account_owner',
				'label' => Rcompany::getMeta('account_owner', 'label'),
				'contentOptions' => ['class' => 'col-account_owner hidden '],
				'filterOptions' => ['class' => 'col-account_owner hidden'],
				'headerOptions' => ['class' => 'col-account_owner hidden '],				
				'footerOptions' => ['class' => 'col-account_owner hidden '],
				
				
				
			],
            ['attribute' => 'eq_login',
				'label' => Rcompany::getMeta('eq_login', 'label'),
				'contentOptions' => ['class' => 'col-eq_login hidden '],
				'filterOptions' => ['class' => 'col-eq_login hidden'],
				'headerOptions' => ['class' => 'col-eq_login hidden '],				
				'footerOptions' => ['class' => 'col-eq_login hidden '],
				
				
				
			],
            ['attribute' => 'eq_password',
				'label' => Rcompany::getMeta('eq_password', 'label'),
				'contentOptions' => ['class' => 'col-eq_password hidden '],
				'filterOptions' => ['class' => 'col-eq_password hidden'],
				'headerOptions' => ['class' => 'col-eq_password hidden '],				
				'footerOptions' => ['class' => 'col-eq_password hidden '],
				
				
				
			],
            ['attribute' => 'dogovor_signature',
				'label' => Rcompany::getMeta('dogovor_signature', 'label'),
				'contentOptions' => ['class' => 'col-dogovor_signature hidden '],
				'filterOptions' => ['class' => 'col-dogovor_signature hidden'],
				'headerOptions' => ['class' => 'col-dogovor_signature hidden '],				
				'footerOptions' => ['class' => 'col-dogovor_signature hidden '],
				
				
				
			],
            ['attribute' => 'dogovor_face',
				'label' => Rcompany::getMeta('dogovor_face', 'label'),
				'contentOptions' => ['class' => 'col-dogovor_face hidden '],
				'filterOptions' => ['class' => 'col-dogovor_face hidden'],
				'headerOptions' => ['class' => 'col-dogovor_face hidden '],				
				'footerOptions' => ['class' => 'col-dogovor_face hidden '],
				
				
				
			],
            ['attribute' => 'dogovor_osnovanie',
				'label' => Rcompany::getMeta('dogovor_osnovanie', 'label'),
				'contentOptions' => ['class' => 'col-dogovor_osnovanie hidden '],
				'filterOptions' => ['class' => 'col-dogovor_osnovanie hidden'],
				'headerOptions' => ['class' => 'col-dogovor_osnovanie hidden '],				
				'footerOptions' => ['class' => 'col-dogovor_osnovanie hidden '],
				
				
				
			],
            ['attribute' => 'tax_system_vat',
				'label' => Rcompany::getMeta('tax_system_vat', 'label'),
				'contentOptions' => ['class' => 'col-tax_system_vat '],
				'filterOptions' => ['class' => 'col-tax_system_vat'],
				'headerOptions' => ['class' => 'col-tax_system_vat '],				
				'footerOptions' => ['class' => 'col-tax_system_vat '],
				
				
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
