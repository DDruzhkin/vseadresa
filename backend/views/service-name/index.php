<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\ServiceName;
use common\models\ServiceNameSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Справочник услуг';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-name-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\ServiceName::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить услугу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\ServiceName::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'id',
				'label' => ServiceName::getMeta('id', 'label'),
				'contentOptions' => ['class' => 'col-id '],
				'filterOptions' => ['class' => 'col-id'],
				'headerOptions' => ['class' => 'col-id '],				
				'footerOptions' => ['class' => 'col-id '],
				
				
				
			],
            ['attribute' => 'code',
				'label' => ServiceName::getMeta('code', 'label'),
				'contentOptions' => ['class' => 'col-code '],
				'filterOptions' => ['class' => 'col-code'],
				'headerOptions' => ['class' => 'col-code '],				
				'footerOptions' => ['class' => 'col-code '],
				
				
				
			],
            ['attribute' => 'name',
				'label' => ServiceName::getMeta('name', 'label'),
				'contentOptions' => ['class' => 'col-name '],
				'filterOptions' => ['class' => 'col-name'],
				'headerOptions' => ['class' => 'col-name '],				
				'footerOptions' => ['class' => 'col-name '],
				
				
				
			],
            ['attribute' => 'stype',
				'label' => ServiceName::getMeta('stype', 'label'),
				'contentOptions' => ['class' => 'col-stype '],
				'filterOptions' => ['class' => 'col-stype'],
				'headerOptions' => ['class' => 'col-stype '],				
				'footerOptions' => ['class' => 'col-stype '],
				'filter' => ServiceName::getList('stype'),	
				
				
			],
            ['attribute' => 'to_address',
				'label' => ServiceName::getMeta('to_address', 'label'),
				'contentOptions' => ['class' => 'col-to_address hidden '],
				'filterOptions' => ['class' => 'col-to_address hidden'],
				'headerOptions' => ['class' => 'col-to_address hidden '],				
				'footerOptions' => ['class' => 'col-to_address hidden '],
				
				
				
			],
            ['attribute' => 'description',
				'label' => ServiceName::getMeta('description', 'label'),
				'contentOptions' => ['class' => 'col-description hidden '],
				'filterOptions' => ['class' => 'col-description hidden'],
				'headerOptions' => ['class' => 'col-description hidden '],				
				'footerOptions' => ['class' => 'col-description hidden '],
				
				
				
			],
            ['attribute' => 'option_id',
				'label' => ServiceName::getMeta('option_id', 'label'),
				'contentOptions' => ['class' => 'col-option_id '],
				'filterOptions' => ['class' => 'col-option_id'],
				'headerOptions' => ['class' => 'col-option_id '],				
				'footerOptions' => ['class' => 'col-option_id '],
				
				'value' => function($data){ return $data['option_id_view'];},
				
			],
            ['attribute' => 'undeleted',
				'label' => ServiceName::getMeta('undeleted', 'label'),
				'contentOptions' => ['class' => 'col-undeleted hidden '],
				'filterOptions' => ['class' => 'col-undeleted hidden'],
				'headerOptions' => ['class' => 'col-undeleted hidden '],				
				'footerOptions' => ['class' => 'col-undeleted hidden '],
				
				
				
			],
            ['attribute' => 'alias',
				'label' => ServiceName::getMeta('alias', 'label'),
				'contentOptions' => ['class' => 'col-alias hidden '],
				'filterOptions' => ['class' => 'col-alias hidden'],
				'headerOptions' => ['class' => 'col-alias hidden '],				
				'footerOptions' => ['class' => 'col-alias hidden '],
				
				
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
