<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Импорт исходящих платежей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="import-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php



    $i=0; //count records for insert
    if(count($data)) : ?>
        <?php //echo "<pre>", print_r($data); die(); ?>
        <form action="<?php echo Yii::$app->urlManager->createUrl(['import/create-outcome']);?>" method="POST">
            <input type="hidden" name="_csrf-backend" value="<?=Yii::$app->request->getCsrfToken()?>" />
            <table class="table">
                <th>Дата платежа</th>
                <th>Сумма</th>
                <th>Номер заказа</th>
                <th>Контрагент</th>
                <?php foreach ( $data as $line): ?>
                    <?php
                    $class = 'regular';
                    $save = true;
                    $query = $line["query_result"];
                    $payment_info = $line["payment_info"];
                   // echo "<pre>", print_r($line); //die();
                    if($query == false || (int)$line[2] == 0 || (int)$query['customer_id'] == 0){
                        $save = false;
                        $class = 'not-found';
                    }

                    /** Cмотрим по другим платежам (по этому заказу
                     *  сравниваем даты и номера поручений
                     */
                    if(is_array($payment_info) && count($payment_info) > 0){
                        //$save = true;
                        foreach($payment_info as $payment){

                            if($payment['date'] == date("Y-m-d H:i:s", strtotime($line[4])) && $payment['number'] == (int)trim($line[3])){
                                $save = false; // комбинация с одинаковым пользователем, одинаковой датой и номером - найдена
                                $class = 'same-detected';
                            }
                        }
                    }

                    ?>
                    <tr class="<?=$class;?>">
                        <?php if($save): ?>
                            <input type="hidden" name="data[<?=$i?>][payment][date]" value="<?=date("Y-m-d H:i:s", strtotime($line[0])); ?>"/>
                            <input type="hidden" name="data[<?=$i?>][payment][value]" value="<?=(int)$line[1];?>"/>
                            <input type="hidden" name="data[<?=$i?>][document][order_id]" value="<?=(int)$line[2];?>"/>
                            <input type="hidden" name="data[<?=$i?>][payment][order_id]" value="<?=(int)$line[2];?>"/>
                            <input type="hidden" name="data[<?=$i?>][document][number]" value="<?=(int)$line[3];?>"/>
                            <input type="hidden" name="data[<?=$i?>][document][date]" value="<?=date("Y-m-d H:i:s", strtotime($line[4]));?>"/>
                            <input type="hidden" name="data[<?=$i?>][payment][user_id]" value="<?=$query['customer_id'];?>"/>

                        <?php endif; ?>
                        <td>
                            <?=date("Y-m-d H:i:s", strtotime($line[0])); ?>

                        </td>
                        <td>
                            <?=$line[1];?>
                        </td>
                        <td>
                            <?=$line[2];?>
                        </td>
                        <td><?php
                            if(isset($query['username'])){
                                echo $query['username'];
                            }else{
                                echo "-";
                            }
                            ?></td>

                        <?php if(!empty($class)){
                            switch ($class){
                                case 'not-found': echo '<td>Платеж не найден</td>'; break;
                                case 'same-detected': echo '<td>Cчёт оплачен</td>'; break;
                            }
                        } ?>

                        <?php $i++; ?>

                    </tr>
                <?php endforeach; ?>
            </table>
            <button type="submit" class="btn btn-success">Сохранить</button>
        </form>
    <?php else: ?>
        <form action="" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label for="file">Excel файл</label>
                <input type="file" name="excel" class="form-control" id="file">
                <input type="hidden" name="_csrf-backend" value="<?=Yii::$app->request->getCsrfToken()?>" />
            </div>
            <button type="submit" class="btn btn-success">Загрузить</button>
        </form>
    <?php endif; ?>
</div>
