<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Rip;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('created_at_view', 'label'), 'hint'=>''.Rip::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'form' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Rip::getMeta('form', 'label'), 'items' => Rip::getList('form'), 'hint'=>''.Rip::getMeta('form', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'user_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Rip::getMeta('user_id', 'label'), 'items' => Rip::getListValues('user_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('user/update/%user_id%')]).'">'.Icon::show('link').'</a>'.Rip::getMeta('user_id', 'comment'), 'options'=>[]],
						'inn' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rip::getMeta('inn', 'label'), 'hint'=>''.Rip::getMeta('inn', 'comment'), 'options'=>['mask' => '999999999999',]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'ogrnip' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rip::getMeta('ogrnip', 'label'), 'hint'=>''.Rip::getMeta('ogrnip', 'comment'), 'options'=>['mask' => '999999999999999',]],
						'fname' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('fname', 'label'), 'hint'=>''.Rip::getMeta('fname', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'mname' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('mname', 'label'), 'hint'=>''.Rip::getMeta('mname', 'comment'), 'options'=>[]],
						'lname' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('lname', 'label'), 'hint'=>''.Rip::getMeta('lname', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'pasport_number' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rip::getMeta('pasport_number', 'label'), 'hint'=>''.Rip::getMeta('pasport_number', 'comment'), 'options'=>['mask' => Rip::getMaskByAlias("pasport"),]],
						'pasport_date' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Rip::getMeta('pasport_date', 'label'), 'hint'=>''.Rip::getMeta('pasport_date', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'pasport_organ' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('pasport_organ', 'label'), 'hint'=>''.Rip::getMeta('pasport_organ', 'comment'), 'options'=>[]],
						'sv_seriya' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('sv_seriya', 'label'), 'hint'=>''.Rip::getMeta('sv_seriya', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'sv_nomer' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('sv_nomer', 'label'), 'hint'=>''.Rip::getMeta('sv_nomer', 'comment'), 'options'=>[]],
						'sv_date' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Rip::getMeta('sv_date', 'label'), 'hint'=>''.Rip::getMeta('sv_date', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'address' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('address', 'label'), 'hint'=>''.Rip::getMeta('address', 'comment'), 'options'=>[]],
						'phone' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('phone', 'label'), 'hint'=>''.Rip::getMeta('phone', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'bank_bik' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rip::getMeta('bank_bik', 'label'), 'hint'=>''.Rip::getMeta('bank_bik', 'comment'), 'options'=>['mask' => '999999999',]],
						'bank_name' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('bank_name', 'label'), 'hint'=>''.Rip::getMeta('bank_name', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'bank_cor' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rip::getMeta('bank_cor', 'label'), 'hint'=>''.Rip::getMeta('bank_cor', 'comment'), 'options'=>['mask' => '99999999999999999999',]],
						'account' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rip::getMeta('account', 'label'), 'hint'=>''.Rip::getMeta('account', 'comment'), 'options'=>['mask' => Rip::getMaskByAlias("account"),]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'account_owner' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('account_owner', 'label'), 'hint'=>''.Rip::getMeta('account_owner', 'comment'), 'options'=>[]],
						'eq_login' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('eq_login', 'label'), 'hint'=>''.Rip::getMeta('eq_login', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'eq_password' => ['type'=>Form::INPUT_TEXT, 'label'=>Rip::getMeta('eq_password', 'label'), 'hint'=>''.Rip::getMeta('eq_password', 'comment'), 'options'=>[]],
						'tax_system_vat' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\checkbox\CheckboxX', 'label'=>Rip::getMeta('tax_system_vat', 'label'), 'hint'=>''.Rip::getMeta('tax_system_vat', 'comment'), 'options'=>['pluginOptions' => ['threeState' => false],]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Rip */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rip-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Rip::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
