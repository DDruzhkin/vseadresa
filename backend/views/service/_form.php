<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Service;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'code_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Service::getMeta('code_view', 'label'), 'hint'=>''.Service::getMeta('code_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Service::getMeta('created_at_view', 'label'), 'hint'=>''.Service::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'service_price_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Service::getMeta('service_price_id_view', 'label'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('service-price/update/%service_price_id%')]).'">'.Icon::show('link').'</a>'.Service::getMeta('service_price_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'order_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Service::getMeta('order_id_view', 'label'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('order/update/%order_id%')]).'">'.Icon::show('link').'</a>'.Service::getMeta('order_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'date_start' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Service::getMeta('date_start', 'label'), 'hint'=>''.Service::getMeta('date_start', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
						'date_fin_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Service::getMeta('date_fin_view', 'label'), 'hint'=>''.Service::getMeta('date_fin_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'address_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Service::getMeta('address_id_view', 'label'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('address/update/%address_id%')]).'">'.Icon::show('link').'</a>'.Service::getMeta('address_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'owner_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Service::getMeta('owner_id_view', 'label'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('user/update/%owner_id%')]).'">'.Icon::show('link').'</a>'.Service::getMeta('owner_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'stype_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Service::getMeta('stype_view', 'label'), 'hint'=>''.Service::getMeta('stype_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'duration_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Service::getMeta('duration_view', 'label'), 'hint'=>''.Service::getMeta('duration_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'price_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Service::getMeta('price_view', 'label'), 'hint'=>''.Service::getMeta('price_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'summa_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Service::getMeta('summa_view', 'label'), 'hint'=>''.Service::getMeta('summa_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'invoice_id_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Service::getMeta('invoice_id_view', 'label'), 'hint'=>''.Service::getMeta('invoice_id_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'nds' => ['type'=>Form::INPUT_TEXT, 'label'=>Service::getMeta('nds', 'label'), 'hint'=>''.Service::getMeta('nds', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Service */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Service::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
