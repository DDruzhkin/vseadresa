<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Rperson;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'created_at_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Rperson::getMeta('created_at_view', 'label'), 'hint'=>''.Rperson::getMeta('created_at_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'form_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Rperson::getMeta('form_view', 'label'), 'hint'=>''.Rperson::getMeta('form_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'user_id' => ['type'=>Form::INPUT_DROPDOWN_LIST, 'label'=>Rperson::getMeta('user_id', 'label'), 'items' => Rperson::getListValues('user_id'), 'hint'=>'<a title="Перейти" href="'.Url::to([$model -> getCaption('user/update/%user_id%')]).'">'.Icon::show('link').'</a>'.Rperson::getMeta('user_id', 'comment'), 'options'=>[]],
						'inn' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rperson::getMeta('inn', 'label'), 'hint'=>''.Rperson::getMeta('inn', 'comment'), 'options'=>['mask' => '999999999999',]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'fname' => ['type'=>Form::INPUT_TEXT, 'label'=>Rperson::getMeta('fname', 'label'), 'hint'=>''.Rperson::getMeta('fname', 'comment'), 'options'=>[]],
						'mname' => ['type'=>Form::INPUT_TEXT, 'label'=>Rperson::getMeta('mname', 'label'), 'hint'=>''.Rperson::getMeta('mname', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'lname' => ['type'=>Form::INPUT_TEXT, 'label'=>Rperson::getMeta('lname', 'label'), 'hint'=>''.Rperson::getMeta('lname', 'comment'), 'options'=>[]],
						'bdate' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Rperson::getMeta('bdate', 'label'), 'hint'=>''.Rperson::getMeta('bdate', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'pasport_number' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rperson::getMeta('pasport_number', 'label'), 'hint'=>''.Rperson::getMeta('pasport_number', 'comment'), 'options'=>['mask' => Rperson::getMaskByAlias("pasport"),]],
						'pasport_date' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\kartik\date\DatePicker', 'label'=>Rperson::getMeta('pasport_date', 'label'), 'hint'=>''.Rperson::getMeta('pasport_date', 'comment'), 'options'=>['type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose'=>true, 'format' => 'dd.mm.yyyy'], ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'pasport_organ' => ['type'=>Form::INPUT_TEXT, 'label'=>Rperson::getMeta('pasport_organ', 'label'), 'hint'=>''.Rperson::getMeta('pasport_organ', 'comment'), 'options'=>[]],
						'address' => ['type'=>Form::INPUT_TEXT, 'label'=>Rperson::getMeta('address', 'label'), 'hint'=>''.Rperson::getMeta('address', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'phone' => ['type'=>Form::INPUT_TEXT, 'label'=>Rperson::getMeta('phone', 'label'), 'hint'=>''.Rperson::getMeta('phone', 'comment'), 'options'=>[]],
						'eq_login' => ['type'=>Form::INPUT_TEXT, 'label'=>Rperson::getMeta('eq_login', 'label'), 'hint'=>''.Rperson::getMeta('eq_login', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'eq_password' => ['type'=>Form::INPUT_TEXT, 'label'=>Rperson::getMeta('eq_password', 'label'), 'hint'=>''.Rperson::getMeta('eq_password', 'comment'), 'options'=>[]],
						'bank_bik' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rperson::getMeta('bank_bik', 'label'), 'hint'=>''.Rperson::getMeta('bank_bik', 'comment'), 'options'=>['mask' => '999999999',]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'bank_name' => ['type'=>Form::INPUT_TEXT, 'label'=>Rperson::getMeta('bank_name', 'label'), 'hint'=>''.Rperson::getMeta('bank_name', 'comment'), 'options'=>[]],
						'bank_cor' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rperson::getMeta('bank_cor', 'label'), 'hint'=>''.Rperson::getMeta('bank_cor', 'comment'), 'options'=>['mask' => '99999999999999999999',]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'account' => ['type'=>Form::INPUT_WIDGET, 'widgetClass'=>'\yii\widgets\MaskedInput', 'label'=>Rperson::getMeta('account', 'label'), 'hint'=>''.Rperson::getMeta('account', 'comment'), 'options'=>['mask' => Rperson::getMaskByAlias("account"),]],
						'account_owner' => ['type'=>Form::INPUT_TEXT, 'label'=>Rperson::getMeta('account_owner', 'label'), 'hint'=>''.Rperson::getMeta('account_owner', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Rperson */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rperson-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Rperson::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
