<?php
use kartik\icons\Icon;
use yii\helpers\Url;
if (!isset($module)) $module = '';
$isDefault = ($model -> id == $model -> user -> default_profile_id);
$template = '
<div class="card">	
<div class="header">'
.($isDefault?
	'<a title="Основные реквизиты">'.Icon::show('star').'</a>'	
	:'<a title="Сделать основынми реквизитами" href="'.URL::to([$module.'/'.'user/default-profile/'.$model -> user -> id.'/'.$model -> id]).'">'.Icon::show('star-o').'</a>'
)
.'<span class="caption">Физическое лицо</span>'
.'<a title="Изменить реквизиты" style="float:right" href="'.URL::to([$module.'/'.$model -> modelId().'/update/'.$model -> id]).'">'.Icon::show('edit').'</a>
</div>
<div class="value title">%fname% %mname% %lname%</div>
<div class="undervalue">ФИО</div>
<div class="value>
</div><div class="undervalue"></div>
'
.(($model -> inn)?'<div class="value">%inn%</div><div class="undervalue">ИНН физического лица</div>':'')
.(($model -> pasport_number || $model -> pasport_date || $model -> pasport_organ)?'<div class="value">%pasport_number% ':'')
.(($model -> pasport_date || $model -> pasport_organ)?'выдан %pasport_date% %pasport_organ%':'')
.(($model -> pasport_number || $model -> pasport_date || $model -> pasport_organ)?'</div><div class="undervalue">паспорт гражданина РФ</div>':'')
.(($model -> address)?'<div class="value">%address%</div><div class="undervalue">адрес регистрации</div>':'')
.(($model -> bank_bik)?'<div class="value">%bank_bik%</div><div class="undervalue">БИК банка</div>':'')
.(($model -> account)?'<div class="value">%account%</div><div class="undervalue">номер счета</div>':'')
.(($model -> account && $model -> account_owner)?'<div class="value">%account_owner%</div><div class="undervalue">владелец счета</div>':'')
.(($model -> eq_login && $model -> eq_password)?'<div class="value">%eq_login%</div><div class="undervalue" style="color:red;">Подключен эквайринг</div>':'')
.'</div>	
';

echo $model -> generateByTemplate($template, $model -> attributes);
?>	