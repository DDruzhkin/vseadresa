<?php
/* генерируемый файл, редактировать его не надо */





/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use backend\models\Rperson;
use common\models\Entity;
use common\models\RpersonSearch;
use kartik\daterange\DateRangePicker;
use kartik\icons\Icon;
use kartik\nav\NavX;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

$this->title = 'Реквизиты физлица';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rperson-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="content">
	<?= \backend\models\Rperson::getMeta('','comment')	?>
	</div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить Реквизиты физлица', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,


		'rowOptions' => function ($model, $key, $index, $grid) {
			$color = $index % 2 == 0 ? '#DDDDFF' : '#FFFFFF';
			return ['style' => 'background-color:'.$color.';'];
		},

        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/
			[
				'class' => 'yii\grid\Column',
				'content' => function($data) {
					return NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'encodeLabels' => false,
						'items' => [
							[
								'label' => Icon::show('bars'), 'url' => '#',
								'items' => backend\models\Rperson::getListViewMenu($data),
							],
						],
					]);
				}					
			],


            ['attribute' => 'id',
				'label' => Rperson::getMeta('id', 'label'),
				'contentOptions' => ['class' => 'col-id '],
				'filterOptions' => ['class' => 'col-id'],
				'headerOptions' => ['class' => 'col-id '],				
				'footerOptions' => ['class' => 'col-id '],
				
				
				
			],
            ['attribute' => 'created_at',
				'label' => Rperson::getMeta('created_at', 'label'),
				'contentOptions' => ['class' => 'col-created_at date'],
				'filterOptions' => ['class' => 'col-created_at'],
				'headerOptions' => ['class' => 'col-created_at date'],				
				'footerOptions' => ['class' => 'col-created_at date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'created_at',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'form',
				'label' => Rperson::getMeta('form', 'label'),
				'contentOptions' => ['class' => 'col-form '],
				'filterOptions' => ['class' => 'col-form'],
				'headerOptions' => ['class' => 'col-form '],				
				'footerOptions' => ['class' => 'col-form '],
				'filter' => Rperson::getList('form'),	
				
				
			],
            ['attribute' => 'user_id',
				'label' => Rperson::getMeta('user_id', 'label'),
				'contentOptions' => ['class' => 'col-user_id '],
				'filterOptions' => ['class' => 'col-user_id'],
				'headerOptions' => ['class' => 'col-user_id '],				
				'footerOptions' => ['class' => 'col-user_id '],
				
				'value' => 
			function($data)
			{ 
				$value = Html::a($data['user_id_view'],[Inflector::camel2id('User').'/update/'.$data['user_id']]);
				return $value;
			},
				'format' => 'raw',
			],
            ['attribute' => 'inn',
				'label' => Rperson::getMeta('inn', 'label'),
				'contentOptions' => ['class' => 'col-inn '],
				'filterOptions' => ['class' => 'col-inn'],
				'headerOptions' => ['class' => 'col-inn '],				
				'footerOptions' => ['class' => 'col-inn '],
				
				
				
			],
            ['attribute' => 'fname',
				'label' => Rperson::getMeta('fname', 'label'),
				'contentOptions' => ['class' => 'col-fname '],
				'filterOptions' => ['class' => 'col-fname'],
				'headerOptions' => ['class' => 'col-fname '],				
				'footerOptions' => ['class' => 'col-fname '],
				
				
				
			],
            ['attribute' => 'mname',
				'label' => Rperson::getMeta('mname', 'label'),
				'contentOptions' => ['class' => 'col-mname '],
				'filterOptions' => ['class' => 'col-mname'],
				'headerOptions' => ['class' => 'col-mname '],				
				'footerOptions' => ['class' => 'col-mname '],
				
				
				
			],
            ['attribute' => 'lname',
				'label' => Rperson::getMeta('lname', 'label'),
				'contentOptions' => ['class' => 'col-lname '],
				'filterOptions' => ['class' => 'col-lname'],
				'headerOptions' => ['class' => 'col-lname '],				
				'footerOptions' => ['class' => 'col-lname '],
				
				
				
			],
            ['attribute' => 'bdate',
				'label' => Rperson::getMeta('bdate', 'label'),
				'contentOptions' => ['class' => 'col-bdate hidden date'],
				'filterOptions' => ['class' => 'col-bdate hidden'],
				'headerOptions' => ['class' => 'col-bdate hidden date'],				
				'footerOptions' => ['class' => 'col-bdate hidden date'],
				'filter' => DateRangePicker::widget([
						'model' => $searchModel,
						'attribute' => 'bdate',
						
						'presetDropdown'=>true,
						'convertFormat' => true,
						'hideInput'=>true,
						'pluginOptions'=>[							
							'locale'=>['format'=>'d.m.Y']
						]
						
					]),
				
				
			],
            ['attribute' => 'pasport_number',
				'label' => Rperson::getMeta('pasport_number', 'label'),
				'contentOptions' => ['class' => 'col-pasport_number '],
				'filterOptions' => ['class' => 'col-pasport_number'],
				'headerOptions' => ['class' => 'col-pasport_number '],				
				'footerOptions' => ['class' => 'col-pasport_number '],
				
				
				
			],
            ['attribute' => 'pasport_date',
				'label' => Rperson::getMeta('pasport_date', 'label'),
				'contentOptions' => ['class' => 'col-pasport_date hidden date'],
				'filterOptions' => ['class' => 'col-pasport_date hidden'],
				'headerOptions' => ['class' => 'col-pasport_date hidden date'],				
				'footerOptions' => ['class' => 'col-pasport_date hidden date'],
				
				
				
			],
            ['attribute' => 'pasport_organ',
				'label' => Rperson::getMeta('pasport_organ', 'label'),
				'contentOptions' => ['class' => 'col-pasport_organ '],
				'filterOptions' => ['class' => 'col-pasport_organ'],
				'headerOptions' => ['class' => 'col-pasport_organ '],				
				'footerOptions' => ['class' => 'col-pasport_organ '],
				
				
				
			],
            ['attribute' => 'address',
				'label' => Rperson::getMeta('address', 'label'),
				'contentOptions' => ['class' => 'col-address '],
				'filterOptions' => ['class' => 'col-address'],
				'headerOptions' => ['class' => 'col-address '],				
				'footerOptions' => ['class' => 'col-address '],
				
				
				
			],
            ['attribute' => 'phone',
				'label' => Rperson::getMeta('phone', 'label'),
				'contentOptions' => ['class' => 'col-phone hidden '],
				'filterOptions' => ['class' => 'col-phone hidden'],
				'headerOptions' => ['class' => 'col-phone hidden '],				
				'footerOptions' => ['class' => 'col-phone hidden '],
				
				
				
			],
            ['attribute' => 'eq_login',
				'label' => Rperson::getMeta('eq_login', 'label'),
				'contentOptions' => ['class' => 'col-eq_login hidden '],
				'filterOptions' => ['class' => 'col-eq_login hidden'],
				'headerOptions' => ['class' => 'col-eq_login hidden '],				
				'footerOptions' => ['class' => 'col-eq_login hidden '],
				
				
				
			],
            ['attribute' => 'eq_password',
				'label' => Rperson::getMeta('eq_password', 'label'),
				'contentOptions' => ['class' => 'col-eq_password hidden '],
				'filterOptions' => ['class' => 'col-eq_password hidden'],
				'headerOptions' => ['class' => 'col-eq_password hidden '],				
				'footerOptions' => ['class' => 'col-eq_password hidden '],
				
				
				
			],
            ['attribute' => 'bank_bik',
				'label' => Rperson::getMeta('bank_bik', 'label'),
				'contentOptions' => ['class' => 'col-bank_bik hidden '],
				'filterOptions' => ['class' => 'col-bank_bik hidden'],
				'headerOptions' => ['class' => 'col-bank_bik hidden '],				
				'footerOptions' => ['class' => 'col-bank_bik hidden '],
				
				
				
			],
            ['attribute' => 'bank_name',
				'label' => Rperson::getMeta('bank_name', 'label'),
				'contentOptions' => ['class' => 'col-bank_name hidden '],
				'filterOptions' => ['class' => 'col-bank_name hidden'],
				'headerOptions' => ['class' => 'col-bank_name hidden '],				
				'footerOptions' => ['class' => 'col-bank_name hidden '],
				
				
				
			],
            ['attribute' => 'bank_cor',
				'label' => Rperson::getMeta('bank_cor', 'label'),
				'contentOptions' => ['class' => 'col-bank_cor hidden '],
				'filterOptions' => ['class' => 'col-bank_cor hidden'],
				'headerOptions' => ['class' => 'col-bank_cor hidden '],				
				'footerOptions' => ['class' => 'col-bank_cor hidden '],
				
				
				
			],
            ['attribute' => 'account',
				'label' => Rperson::getMeta('account', 'label'),
				'contentOptions' => ['class' => 'col-account hidden '],
				'filterOptions' => ['class' => 'col-account hidden'],
				'headerOptions' => ['class' => 'col-account hidden '],				
				'footerOptions' => ['class' => 'col-account hidden '],
				
				
				
			],
            ['attribute' => 'account_owner',
				'label' => Rperson::getMeta('account_owner', 'label'),
				'contentOptions' => ['class' => 'col-account_owner hidden '],
				'filterOptions' => ['class' => 'col-account_owner hidden'],
				'headerOptions' => ['class' => 'col-account_owner hidden '],				
				'footerOptions' => ['class' => 'col-account_owner hidden '],
				
				
				
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
