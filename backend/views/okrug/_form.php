<?php
/* генерируемый файл, редактировать его не надо */


if (YII_DEBUG) $startTime = microtime(true);
use backend\models\Okrug;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\icons\Icon;
use kartik\nav\NavX;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$data = [

	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'alias_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Okrug::getMeta('alias_view', 'label'), 'hint'=>''.Okrug::getMeta('alias_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'name_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Okrug::getMeta('name_view', 'label'), 'hint'=>''.Okrug::getMeta('name_view', 'comment'), 'options'=>['readonly'=>'true', ]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'abr_view' => ['type'=>Form::INPUT_TEXT, 'label'=>Okrug::getMeta('abr_view', 'label'), 'hint'=>''.Okrug::getMeta('abr_view', 'comment'), 'options'=>['readonly'=>'true', ]],
						'title' => ['type'=>Form::INPUT_TEXT, 'label'=>Okrug::getMeta('title', 'label'), 'hint'=>''.Okrug::getMeta('title', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>2,								
					'attributes' => [			
		
						'meta_description' => ['type'=>Form::INPUT_TEXT, 'label'=>Okrug::getMeta('meta_description', 'label'), 'hint'=>''.Okrug::getMeta('meta_description', 'comment'), 'options'=>[]],
						'h1' => ['type'=>Form::INPUT_TEXT, 'label'=>Okrug::getMeta('h1', 'label'), 'hint'=>''.Okrug::getMeta('h1', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


	
	
	[
        'attributes' => [		
			'address_detail' => [ 						
				'columns'=>1,								
					'attributes' => [			
		
						'seo_text' => ['type'=>Form::INPUT_TEXTAREA, 'label'=>Okrug::getMeta('seo_text', 'label'), 'hint'=>''.Okrug::getMeta('seo_text', 'comment'), 'options'=>[]],
		
				],
			],				
		],				
	],


		[
			'attributes' => [
				'actions'=>[    // embed raw HTML content
                    'type'=>Form::INPUT_RAW, 
                    'value'=>  '<div style="text-align: right; margin-top: 20px">' .                         
                        Html::submitButton('Сохранить', ['class'=>'btn btn-primary']) . 
                        '</div>'
                ],
			]
		]
		
	

]




/* @var $this yii\web\View */
/* @var $model backend\models\Okrug */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="okrug-form">


<?php

		echo NavX::widget([
						'options'=>['class'=>'nav nav-context'],
						'items' => [
							[
								'label' => 'Связи', 'url' => '#',
								'items' => Okrug::getObjectMenu($model -> attributes),
							],
						],
					]);

echo "<hr/>";	



$form = ActiveForm::begin([]);
echo FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'rows' => $data
	]);
	
ActiveForm::end();

if (YII_DEBUG)  {
	$finTime = microtime(true); 
	$delta = $finTime - $startTime; 
	echo $delta . ' сек.'; 
}
?>

</div>
