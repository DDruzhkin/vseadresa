<?php
namespace backend\Page;

use \Codeception\Actor;

class AddressManagement
{
    public static $URL = '/address';

    protected $tester;

    public function __construct(Actor $I)
    {
        $this->tester = $I;
    }

    public static function open(Actor $I) {
        $I->amOnPage(self::$URL);
        return new AddressManagement($I);
    }

    public function publish($id) {
        $this->edit($id)->setStatus('Ожидание оплаты')->save()
            ->setStatus('Ожидание подтверждения')->save()
            ->setStatus('Подготовка адреса')->save()
            ->allowPublish()->setStatus('Готов к публикации')->save()
            ->setStatus('Опубликован')->save();
    }

    public function allowPublish() {
        $I = $this->tester;
        $I->click(['css' => '.field-address-demo .cbx-icon']);
        return $this;
    }

    function setStatus($status) {
        $I = $this->tester;
        $I->selectOption(['css' => '#address-status_id'], ['text' => $status]);
        return $this;
    }

    function save() {
        $I = $this->tester;
        $I->click(['css' => 'button.btn-primary']);
        $I->wait(1);
        $I->waitForElement(['css' => 'div.alert-success']);
        return $this;
    }

    private function dropDownMenu($id) {
        $this->tester->click(['css' => "tr[data-key='$id'] a.dropdown-toggle"]);
        return $this;
    }

    private function edit($id) {
        $I = $this->tester;
        $this->dropDownMenu($id);
        $I->click(['css' => "tr[data-key='$id'] ul.dropdown-menu a[href^='/address/update/']"]);
        return $this;
    }
}
