<?php
namespace backend\Page;

use \Codeception\Actor;;

class AgreementManagement
{
    public static $URL = '/contract';

    protected $tester;

    public function __construct(Actor $I)
    {
        $this->tester = $I;
    }

    public function open() {
        $I = $this->tester;
        $I->amOnPage(self::$URL);
    }

    public function getState($id) {
        $I = $this->tester;
        $I->expect('Есть договор с ID: '.$id);
        return $I->grabTextFrom(['css' => "tr[data-key='$id'] td.col-state"]);
    }

    public function sign($id) {
        $I = $this->tester;
        $this->dropDownMenu($id);
        $menuUrl = $I->grabAttributeFrom(['css' => "tr[data-key='$id'] ul.dropdown-menu a[tabindex='-1']"], 'href');
        $I->assertContains('/contract/tosigning', $menuUrl);
        $I->click(['css' => "tr[data-key='$id'] ul.dropdown-menu a[tabindex='-1']"]);
    }

    public function activate($id) {
        $I = $this->tester;
        $this->dropDownMenu($id);
        $menuUrl = $I->grabAttributeFrom(['css' => "tr[data-key='$id'] ul.dropdown-menu a[tabindex='-1']"], 'href');
        $I->assertContains('/contract/activate', $menuUrl);
        $I->click(['css' => "tr[data-key='$id'] ul.dropdown-menu a[tabindex='-1']"]);
    }

    private function dropDownMenu($id) {
        $this->tester->click(['css' => "tr[data-key='$id'] a.dropdown-toggle"]);
    }
}
