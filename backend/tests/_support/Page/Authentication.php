<?php
namespace backend\Page;

use \Codeception\Actor;

class Authentication
{
    public static $URL = '/site/login';
    public static $LOGIN_TIMEOUT = 3;

    protected $tester;

    public function __construct(Actor $I)
    {
        $this->tester = $I;
    }

    public static function open(Actor $I)
    {
        $I->amOnPage(self::$URL);
        return new Authentication($I);
    }

    public function login($user, $passwd)
    {
        $I = $this->tester;
        $I->waitForElement(['css' => '#loginform-username'], self::$LOGIN_TIMEOUT);
        $I->fillField(['css' => '#loginform-username'], $user);
        $I->fillField(['css' => '#loginform-password'], $passwd);
        $I->checkOption(['css' => '#loginform-rememberme']);
        $I->click(['css' => "button[name='login-button']"]);
        $I->waitForElement(['css' => 'div.jumbotron h1'], self::$LOGIN_TIMEOUT);
    }
}
