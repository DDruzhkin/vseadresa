<?php

namespace backend\controllers;

use Yii;
use backend\models\Payment;
use backend\models\Import;
use backend\models\PaymentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class ImportController extends PaymentController
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        // ...set `$this->enableCsrfValidation` here based on some conditions...
        // call parent method that will check CSRF if such property is true.

            # code...
            //$this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $data = [];
		$res = false;
        if(Yii::$app->request->isPost){
            /** Request method is POST, check file*/
            if(!isset($_FILES['excel'])) return;
            $response = Import::validateImportedFile($_FILES['excel']);  /** Validating file */
            if($response['success']){
                $sheets = Import::parseExcel($_FILES['excel']); /** Parsing file and get array with result */								
                //$data = Import::compareToDbData($sheets); /** Comparing to db data and returns array for table to display */
				$res = Import::compareToDbData($sheets); /** Comparing to db data and returns array for table to display */
            }

        }
	
        return ($res)? $this->redirect(array('payment/index')):$this->render('index', array("data" => []));


    }
    public function actionOutcome()
    {
        $data = [];
        if(Yii::$app->request->isPost){
	
			return "Функция временно откючена";
            /** Request method is POST, check file*/
			
            if(!isset($_FILES['excel'])) return;
            $response = Import::validateImportedFile($_FILES['excel']);  /** Validating file */

            if($response['success']){
                $sheets = Import::parseExcel($_FILES['excel']); /** Parsing file and get array with result */

                $data = Import::compareToDbDataOutcome($sheets); /** Comparing to db data and returns array for table to display */
            }
        }
        return $this->render('outcome', array("data" => $data));


    }



    /**
     * Creates a new Payment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       // echo "<pre>", print_r($_POST); die();
        if(isset($_POST['data'])){
            Import::importAndModify();
        }

        $this->redirect(array('payment/index'));

    }

    public function actionCreateOutcome()
    {

        if(isset($_POST['data'])){
            Import::importAndModifyOutcome();
        }

        $this->redirect(array('payment/index'));

    }


}
