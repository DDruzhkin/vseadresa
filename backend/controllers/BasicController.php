<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Response;
use common\models\User;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;

class BasicController extends \common\controllers\EntityController{
	
	public $title;
	public $h1;	
	
	public function beforeAction($action)
	{			
		$result = parent::beforeAction($action);
		if ($result) {
			
			if ((Yii::$app->user->isGuest || !User::getCurrentUser() -> checkRole(User::ROLE_ADMIN))) 				
					throw new ForbiddenHttpException ('Доступ возможен только Администраторам');

		};
		
		return $result;
				
		
	}

	
}

?>