<?php
/* генерируемый файл, редактировать его не надо */


namespace backend\controllers;

use Yii;
use backend\models\PhotoCalendar;
use common\models\PhotoCalendarSearch;
use backend\controllers\BasicController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PhotoCalendarController implements the CRUD actions for PhotoCalendar model.
 */
class PhotoCalendarController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PhotoCalendar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PhotoCalendarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PhotoCalendar model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PhotoCalendar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PhotoCalendar();

        if (Yii::$app->request->get() && isset(Yii::$app->request->get()['data'])) {
            $data = Yii::$app->request->get()['data'];
            $model->load($data, '');
            $model->integrate();
        };


        if ($model->load(Yii::$app->request->post())) {
            // PhotoCalendar::deleteAll(['date' => $model->date]);
            //PhotoCalendar::deleteAll(["DATE_FORMAT(date, '%d.%m.%Y')" => $model->date, 'interval' => intval($model->interval)]);

            $calendarData = [
                'user_id' => $model->user_id,
                'date' => $model->date,
                'enabled' => 0
            ];

            foreach ($model->interval as $value) {
                $photoCalendar = new PhotoCalendar();
                $calendarData['interval'] = $value;
                $photoCalendar->setAttributes($calendarData);
                $photoCalendar->save();
            }

            Yii::$app->session->setFlash('success', "Данные  сохранены");
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PhotoCalendar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $tab = isset($_GET['tab']) ? $_GET['tab'] : false;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $calendarData = [
                'user_id' => $model->user_id,
                'date' => $model->date,
                'enabled' => 0
            ];

            foreach ($model->interval as $value) {
                $photoCalendar = new PhotoCalendar();
                $calendarData['interval'] = $value;
                $photoCalendar->setAttributes($calendarData);
                $photoCalendar->save();
            }

            Yii::$app->session->setFlash('success', "Данные  сохранены");
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        };


        return $this->render('update', [
            'model' => $model,
            'tab' => $tab
        ]);
    }

    /**
     * Deletes an existing PhotoCalendar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PhotoCalendar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PhotoCalendar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PhotoCalendar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}
