<?php
/* генерируемый файл, редактировать его не надо */


namespace backend\controllers;

use Yii;
use backend\models\Document;
use common\models\ContractSearch;
use backend\controllers\BasicController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

/**
 * DocumentController implements the CRUD actions for Document model.
 */
class ContractController extends BasicController
{
    /**
     * @inheritdoc
     */
/*	 
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
*/
    /**
     * Lists all Document models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContractSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	

	public function actionTosigning($id) {
		$model = $this->findModel($id);
		$model -> state = Document::CONTRACT_STATE_SIGNING;		
		$model -> save();
		return $this -> redirect(Url::toRoute(['index']));
	}

	public function actionActivate($id) {
		$model = $this->findModel($id);
		$model -> state = Document::CONTRACT_STATE_ACTIVE;		
		$model -> save();
		return $this -> redirect(Url::toRoute(['index']));
	}
	
	public function actionDeactivate($id) {
		$model = $this->findModel($id);
		$model -> state = Document::CONTRACT_STATE_DEACTIVE;		
		$model -> save();
		return $this -> redirect(Url::toRoute(['index']));
	}

    /**
     * Displays a single Document model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Document model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Document the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Document::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}
