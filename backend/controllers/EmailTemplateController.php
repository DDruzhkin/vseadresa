<?php

namespace backend\controllers;

use backend\models\EmailTemplate;
use common\controllers\EntityController;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * DocumentController implements the CRUD actions for Document model.
 */
class EmailTemplateController extends EntityController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EmailTemplate models.
     * @return mixed
     */

    public function actionIndex()
    {

        $model = new EmailTemplate();
        $allModels = [];

        foreach($model->fileList as $value){
            $allModels[$value] = ['name' => $value,'delete' => true, 'type' => Yii::t('app','Custom')];
        }

        foreach($model->defaultFileList as $value){
            if(!isset($allModels[$value])){
                $allModels[$value] = ['name' => $value, 'delete' => false, 'type' => Yii::t('app','Default')];
            }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new EmailTemplate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EmailTemplate();

        $app = Yii::$app;
        $request = $app->request;
        $session = $app->session;

            if ($model->load($request->post()) && $model->validate()) {
            if ($request->isPost) {
                if ($model->save()) {
                    $session->setFlash('success', "Данные шаблона сохранены");
                }
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EmailTemplate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @par am integer $name
     * @return mixed
     */
    public function actionUpdate($name)
    {

        $app = Yii::$app;
        $request = $app->request;
        $session = $app->session;

        $model = $this->findModel($name);

        $model->oldAttributes = $model->getAttributes();

        if ($model->load($request->post()) && $model->validate()) {
            if ($request->isPost) {
                if ($model->save()) {
                    $session->setFlash('success', "Данные шаблона сохранены");
                }
            }
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing EmailTemplate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $name
     * @return mixed
     */
    public function actionDelete($name)
    {
        $this->findModel($name)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EmailTemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $name
     * @return EmailTemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    protected function findModel($name)
    {
        if (($model = EmailTemplate::findOne($name)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'pageWasNotFound'));
        }
    }


    public function actionPutDefaultTemplateIntoEditor()
    {
        $app = Yii::$app;
        $request = $app->request;
        $name = $request->post('name');

        if ($request->isAjax) {
            $path = Yii::getAlias('@storage/templates/mail/default/' . $name . EmailTemplate::FILE_EXTENSION);
            if (file_exists($path)) {
                $fileData = EmailTemplate::getFileData($path);

                return json_encode([
                    'name' => $name,
                    'content' => $fileData['content'],
                    'view' => $fileData['view']
                ]);

            }
        }
        return false;
    }
}
