<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use common\models\LoginForm;
use common\models\Address;
use common\models\Entity;
use frontend\models\User;
use frontend\models\Document;



/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'testing', 'pay'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'help'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],            
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {		
        return $this->render('index');
    }

	
	public function payPost($cmd, $params) {
		$url = 'https://3dsec.sberbank.ru/payment/'.$cmd;
		
		$client = new Client();
		$response = $client->createRequest()
			->setMethod('POST')
			->setUrl($url)
			->setData($params)
			->send();

		if ($response->isOk) {
			return $response -> data;			
		} else {
			var_dump($response -> statusCode); echo "<br>";
		}
	}
	
	public function payRegOrder($params) {
		return $this -> payPost('rest/register.do',
		[
			'amount' => isset($params['amount'])?$params['amount']:1,
			'currency' => 643,
			'language' => 'ru',
			'orderNumber'=> $params['number'],
			'userName' => 'best-ur-adres-api',
			'password' => 'best-ur-adres',
			'returnUrl' => $params['returnUrl']
		]);
		
	}
	

	public function actionPay() {

		$result = $this -> payRegOrder([
			'number' => 24, 
			'amount' => 1100000, 
			'returnUrl' => 'http://mgportal.steigenhaus.com',
			'jsonParams' => '{"email" => "m.gribov@mail.ru"}'
		]);
		
		return $this -> redirect($result['formUrl']);
		
		
		/*
		$result = $this -> payPost('paymentSberPay.do',[
		'orderId' => '5f67f82b-a420-7a46-5f67-f82b04b04b8c',
		'phone' => '+79163305455',
		'checkoutType' => 1,		
		]);
		*/
		var_dump($result);
		exit;
	
		
		$result = $this -> payPost('register.do',
		[
			'amount' => 6,
//			'currency' => 643,
			'language' => 'ru',
			'orderNumber'=> 6,
			'userName' => 'best-ur-adres-api',
			'password' => 'best-ur-adres',
			'returnUrl' => 'http://mgportal.steigenhaus.com'
		]);
		
//		var_dump($result);
		exit;
		
		$fields = array(
					'__VIEWSTATE'=>urlencode($state),
					'__EVENTVALIDATION'=>urlencode($valid),
					'btnSubmit'=>urlencode('Submit')
				);

		
		print $result;
	}
	
	
	
    public function actionTesting() {
	
	
	$contract = User::getCurrentUser() -> ownerContracting();
	$contract -> save();
	echo $contract -> id;
	var_dump($contract -> attributes);
	exit;
	
	$model = \frontend\models\Delivery::findOne([3816]);
	echo $model -> price_view;
	
	
	exit;
	/////////////
	$model = \frontend\models\Porder::findOne([4045]);	
	$model -> bill();
	echo "invoice: "; var_dump($model -> invoice-> attributes);
	exit;
	
	$model = \frontend\models\Invoice::findOne([4043]);	
	
	var_dump($model -> attributes);
	
	exit;
	
//		var_dump(mail("m.gribov@mail.ru", "My Subject", "Line 1\nLine 2\nLine 3"));
//		exit;
		
/*		$model = new \common\models\Invoice();
		//$model = new \common\models\Document();
		//$model -> integrate();
		$model -> info = 'Тестовый счет';
		//var_dump($model -> parent() -> hasMethos('getServices'));		
		//var_dump($model -> parent() -> __call('getServices', []));
				
		
		//var_dump($model -> getServices());
		
		$model -> save();
		var_dump($model -> attributes);
	
*/
	
		/* генеация счетов у заказов 
		
		$model = \common\models\Order::findOne(2475);
		$invoices = $model -> bill();
		foreach($invoices as $invoice) {
			//var_dump($invoice);
			if ($invoice) echo $invoice -> info; else 'empty';
			echo '<br>';
		}
	*/	
		
		
		/*  генеация счетов у портальных заказов */
		$model = \common\models\Porder::findOne(2381);		
		$invoice = $model -> bill();
		var_dump($invoice -> attributes);
		
		
		exit;
		echo 'price 6: '.$model -> price6.'<br>';
		echo 'range 6: ['.$model -> price6min.','.$model -> price6max.']<br>';
		var_dump($model -> priceRange6()); echo '<br>';
		echo $model -> price6(); echo '<br><br>';
		echo 'price 11: '.$model -> price11.'<br>';
		echo 'range 11: ['.$model -> price11min.','.$model -> price11max.']<br>';
		var_dump($model -> priceRange11());echo '<br>';
		echo $model -> price11(); echo '<br><br>';
		
		
		exit;
		
		$model -> setOptions('dir1.dir2.dir3','value123', true);
		echo "------------------------------------------------------------------------<br/>";
		var_dump($model -> getOptions('dir1.dir2'));
		exit;
		
		
		$startTime = microtime(true);
		/*
		$c = \common\models\Counter::byAlias('order') -> getValue();
		//$c = $counter -> getValue();
		var_dump($c);
		exit;
		*/

		$service = \backend\models\ServicePortal::byAlias('delivary');
		$order = $service -> order();
		echo 'service_name: '.$order -> service_name; echo "<br>";
		echo 'service_id: '.$order -> service_id; echo "<br>";
		echo 'customer_id: '.$order -> customer_id; echo "<br>";
		echo 'customer_r_id: '.$order -> customer_r_id; echo "<br>";
		echo 'recipient_id: '.$order -> recipient_id; echo "<br>";

		var_dump($order -> getRelatedListValues('recipient_id'));


		$order -> save();
	

/*

		$model = \backend\models\ServicePortal::byId(1455);
		var_dump($model); 
			
		exit;
*/		

/*
		$cache = Yii::$app->cache;							

		$user = \common\models\User::getPortalUser();
		var_dump($user -> requisiteList());
		
		echo "<br><br>";

		$key = 'ttt';
		$cache->set($key, $user, 100);
		$result=$cache->get($key);	
		
		var_dump($result -> requisiteList());

*/


	$model = \common\models\Address::findOne(1188);	
	var_dump($model -> owner -> requisiteList());echo "<br><br>";
/*
	//$owner = $model -> owner;
	//$owner = \backend\models\User::findOne(1095);	
	
	//var_dump($model -> owner -> requisiteList());
	var_dump($model -> owner -> requisiteList());
	echo "<br><br>";
*/
	
	
	var_dump($model -> owner -> info); echo "<br><br>";
	$model -> owner -> info = "Какая-то дополнительная информация update 5";
	var_dump($model -> owner -> info); echo "<br><br>";	
	//$v = $model -> owner;
	//$v1 = &$v;
	//$v2 = &$v;
	$v1 = $model -> owner;
	$v2 = $model -> owner;
	$v1 -> info = '!!!!!';
	echo $v1 -> info.' - '.$v2 -> info;
	$model -> owner -> save();
	echo "<br><br>";
	var_dump($model -> owner -> info);
	exit;
	//$model -> square
	exit;


	/*
	$model = \backend\models\Address::findOne(1188);	
	$i = 500;
	while ($i > 0) {
		$v = $model -> nalog_view;
		echo $v;
		$i--;
	}
	echo "<br>".(microtime(true) - $startTime);
	*/

	/*
	$list = $model -> getServicePrices() -> all();
	foreach($list as $item){
		echo $item -> getCaption()."<br>\n";
	}
	var_dump($list);
	

		
	$v = $model -> nalog_view;
	echo "<br>--------------<br>";
	var_dump($v);
	*/	


	exit;
			
	$sp = \backend\models\ServicePrice::findone(953);
		
	$s = $sp -> createService(['order_id' => 1303]);
	var_dump($s -> attributes);
	$s ->save();	
	/*
	var_dump($sp -> attributes);
	echo "<p>";	
	$values = Entity::extractParams($sp -> attributes,['code', 'stype', 'owner_id', 'address_id']);
	$values['service_preices_id'] = $sp -> id;
	var_dump($values); echo "<p>";
	foreach($values as $name => $value) {
		$s -> __set($name, $value);
	}
	
	echo "<p>";ihjjhkhjughf
	
	var_dump($s -> attributes);
	//$s -> save();
	*/
	
	var_dump($s -> attributes);
	
	
	exit;
		$cache = Yii::$app->cache;
		$key = 'Mem';
		//$cache->delete($key);
		$data=$cache->get($key);
		if ($data === false) {			
			$data='My First Memcached Data';
			$cache->set($key, $data, 10);
			echo "setting<br>";
		}		
		echo $data;
		
		exit;
	
	
	$model = \backend\models\Address::findOne(1097);	
	$menu = Address::getObjectMenu($model -> attributes);
	
	
	
	var_dump($model -> attributes);
	
	exit;
	$status = new \backend\models\Status();
	$data = ['name' => "Опубликован", 'table' => 'adr_review'];
	\backend\models\Status::addBy($data, ['name', 'table']);
	
		
	//$status -> save();
	
	exit;
	$user = new \backend\models\User();
	$user -> login = 'rrrr';
	if (!$user -> save()) echo "not saved!";
var_dump($user -> id);

exit;
	$ad = Address::find() -> one();
	$ad  -> status_id = 930;
	$ad  -> save();

	var_dump($ad);



exit;

	var_dump(Yii::$classMap); exit;

		$user = \common\models\User::findOne(948);

var_dump($user);
exit;


//		$status = \common\models\Status::getDefault('\common\models\Order');

		echo '*'.$status -> name."*<br/>";

//		$list = Yii::$app->db->createCommand("select to_id from {{%status_links}} where from_id = :from", ["from" => 832]) -> queryAll();
//		$list = $status -> findBySql("select s.*  from {{%status_links}} sl, {{%status}} s where s.id = to_id AND from_id = :from", ["from" => 832]) -> all();


		echo "<br/>to: <br/>";
		$list = $status -> getTo() -> all();		

		foreach($list as $item) {
			var_dump($item -> attributes);echo "<br/>";
		}

		echo "<br/>from: <br/>";

		$list = $status -> getFrom() -> all();		

		foreach($list as $item) {
			var_dump($item -> attributes);echo "<br/>";
		}



		exit;


    }


    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
	
	public function actionHelp() {
				
		/*
		$parms = Yii::$app->request->post();
		$modelName = false;
		if (isset($parms['modelname'])) {
			$modelName = $parms['modelname'];
			
		}
		*/
		
		$modelName = Yii::$app->request->post('modelname', false);
		if (!$modelName) $modelName = Yii::$app->request->get('modelname', false);
		return $this->render('help', [
                'modelName' => $modelName,
        ]);
		
		
	}
}
