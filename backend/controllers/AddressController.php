<?php
/* генерируемый файл, редактировать его не надо */

	
namespace backend\controllers;

use Yii;
use backend\models\Address;
use common\models\AddressSearch;
use backend\controllers\BasicController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AddressController implements the CRUD actions for Address model.
 */
class AddressController extends BasicController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Address models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AddressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	
public function actionFiles()
	{
		$get = Yii::$app->request->get();
		$id = $get['id'];
		
		$model = $this->findModel($id);	
			
		$files = $model -> files_as_array;		
		if (Yii::$app->request->isPost){			
			$post = Yii::$app->request->post();
			$data =  $post[Address::baseClassName()];
			
			$numbers = isset($data['numbers'])?$data['numbers']:[];			
			$dates = isset($data['dates'])?$data['dates']:[];			
			$names = isset($data['names'])?$data['names']:[];
			$removes = isset($data['removes'])?$data['removes']:[];			
			$filenames = isset($data['filenames'])?$data['filenames']:[];
			
			$files = [];
			foreach($numbers as $no => $value) {
				if (isset($removes[$no])) {
					//  файл удаляем физически
					
					FilesController::removeFile('documents', 'uploaded', $filenames[$no]);
					
					
				} else {
					$files[] = [
						'number' => ArrayHelper::getValue($numbers, $no), 
						'date' => ArrayHelper::getValue($dates, $no), 
						'name' => ArrayHelper::getValue($names, $no), 
						'filename' => ArrayHelper::getValue($filenames, $no)
					];
				}
			}
            $newFiles = FilesController::uploadFiles('Address[files]');
			if (is_array($newFiles) && count($newFiles)) {
				$loaded = 0;			
				foreach($newFiles as $name => $data) {
					if ($data['error'] > 0) {
						Yii::$app->session->setFlash('error', "Ошибка загрузки файла: ".$name);
					} else {
	//					Yii::$app->session->setFlash('success', "Файл загружен: ".$name);
						$loaded++;
						$files[] = ['date' => Entity::dbNow(), 'name' => $name, 'filename' => $data['filename']];
					}				
				}
						
				Yii::$app->session->setFlash('success', "Загружено файлов: ".$loaded);
			} else Yii::$app->session->setFlash('success', "Изменения сохранены");
			
			$model -> files_as_array = $files;			
			$model -> save();
		}
		
		
		return $this->render('files', [
			'files' => $files,
            'model' => $model,
			
        ]);
	}		
			
	
	
    /**
     * Displays a single Address model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Address model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Address();
		
		if (Yii::$app->request->get() && isset(Yii::$app->request->get()['data'])) {
				$data = Yii::$app->request->get()['data'];
				$model->load($data, '');
				$model->integrate();
		};


        if ($model->load(Yii::$app->request->post()) && $model->save()) {		
			Yii::$app->session->setFlash('success', "Данные Адреса сохранены");
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Address model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$tab = isset($_GET['tab'])?$_GET['tab']:false;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {           
			if ($model->save()) {                       
				Yii::$app->session->setFlash('success', "Данные Адреса сохранены");
				//return $this->redirect(['view', 'id' => $model->id]);
			}
		};
           
           
		return $this->render('update', [
			'model' => $model,
			'tab' => $tab               
		]);
    }

    /**
     * Deletes an existing Address model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Address model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Address the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Address::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не найдена.');
        }
    }
}
