<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserOptions;
use common\controllers\EntityController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserOptionsController extends BasicController
{


    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $username = Yii::$app->request->get('name');

		/*
        if ($model->load(Yii::$app->request->post())) {           
			if ($model->save()) {                       
				//return $this->redirect(['view', 'id' => $model->id]);
			}
		};
         */

		
			$user = User::findOne(['username' => $username]);
			$model = new UserOptions();
			
           
		return $this->render('update', [
			'model' => $model,
			'user' => $user,

		]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


}
